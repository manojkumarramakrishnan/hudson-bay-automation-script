package hbConfig;

public interface HBCInterface {	
	
		int timeout = 90;
		
		public String promoBannerr = "//*[@class='sc-bnrsliderwgt-wrapper pdt-main-container']";
		public String signupEmailss = "//*[@class='sk_ftsignupblk']";
		public String promocarousell = "//*[@id='hdPromoPagination']//*[contains(@class,'swiper-pagination-bullet')]";
		public String headerr = "//*[@id='tabHeader']";
		public String homepagee = "//*[@id='home']";
//		public String heroBannerr = "//*[@id='skPageLayoutCell_1_id-1']";
//		public String heroBannerr = "//*[@id='skPageLayoutCell_10_id-1']";
		
		public String heroBannerr = "//*[@widgettype='hbcHeroWidget']";
		public String ProductCarousell = "//*[@widgettype='HBCProductCarousel']";
		public String advertisementBannerr = "//*[@class='skHDHeroWidContainer']";
		public String footerBottomCopyrightt = "//*[@class='sk_footerBottomPanel']";
		public String footerBottomm = "//*[@class='sk_footerBottomWrapper']";
		public String footerSocialIconn = "//*[@class='sk_footerBottomIconCont']";
		public String footerContainerr = "//*[@class='tabFooter sktabFooterContainer']";
		public String footerTopp = "//*[@class='footer-top-wrapper']";
		public String footerTopTitlee = "//*[@class='sk_footerTitleCont']";		
		public String footerSubTopp = "//*[@class='sk_footerSubCont']";
		public String footerSubTopCalll = "//*[@id='sk_fcall']";
		public String footerSubTopEmaill = "//*[@id='sk_fmail']";
		public String footerMenuListt = "//*[@class='footer-menu-list']";
		public String footerMenuListActivee = "//*[@class='footer-menu-list active']";
		public String footerMenuWrapperDomm = "//*[@class='footer-menu-list active']//*[@class='footer-menu-wrapper']";
		public String footerMenuWrapperr = "//*[@class='footer-menu-wrapper']";
		public String footerSocialFBb = "//*[@class='sk_footerBottomIcon icn-fb']";
		public String footerSocialPinn = "//*[@class='sk_footerBottomIcon icn-pin']";
		public String footerSocialInstaa = "//*[@class='sk_footerBottomIcon icn-insta']";
		public String footerSocialTwitterr = "//*[@class='sk_footerBottomIcon icn-tw']";
		public String footerSocialYouu = "//*[@class='sk_footerBottomIcon icn-youtube']";		
		public String hamburgerr = "//*[@class='sk_shop sk_shop']";
		public String logoo = "//*[@class='logo_icon_wrapper']";
		public String searchIconn = "//*[@id='search_icon']";
		public String bagIconn = "//*[@id='bag_icon']";
		public String prodQuantityContt = "//*[@class='sk_cartQty']";		
		public String prodQuantyy = "//*[@class='quantity-info']//*[@id='qty_1']//*[@selected='selected']";
		public String prodQuantyIncc = "//*[@class='sk_incrementCont sk_changeQtyIcn']";
		public String prodQuantyDecc = "//*[@class='sk_decrementCont sk_changeQtyIcn']";
		
		public String loadingBarrr = "//*[@class='skMob_ui_loadingBar']";
		public String homepageDomm= "//*[contains(@class,'pancakeMobileView')]";


		public String shoppingBagPageDomm = "//*[@skpagename='skhbcshoppingcartpage']";
		public String shoppingCartPagee = "//*[@id='ShopCartPagingDisplay']";	
		public String shoppingBagPageProdTitlee = "//*[@class='item-info']//*[@class='pro-name']";
		public String shoppingBagPageProdImagee = "//*[@class='pro-pic']//img";
		public String shoppingBagPageProdAvaill = "//*[@class='sk_prodDesCont']//p";
		public String shoppingBagPageProdColorr = "//*[@class='attribute-container'][1]//dd";
		public String shoppingBagPageProdSizee = "//*[@class='attribute-container'][2]//dd";
		public String shoppingBagPageProdRemovee = "//*[@class='sk_prodDesCont']//*[@class='remove']";
		public String shoppingBagPageProdRegSalePricee= "//*[@class='item-price salePriceAvail']//*[@class='price']";
		public String shoppingBagPageProdRegularPricee= "//*[@class='item-price']//*[@class='price']";
		public String shoppingBagPageProdRegSaleSalePricee= "//*[@class='item-price salePriceAvail']//*[@class='price sale']//*[@class='offer-price-container']";
		public String shoppingBagPageProdQuickWindoww = "//*[@skpagename='skhbcCartQuickInfo']";
		public String shoppingBagPageProdQuickWindowClosee = "//*[contains(@widgetid,'dijit_Dialog')]//*[@title='Cancel']//*[@class='closeText']";
		public String shoppingBagPageShopNoww = "//*[@class='allbutton lineh']";
		public String PDPATBoverlayViewBagg = "//*[@class='skMob_ATBPopupBtn skMob_ViewCartBtn']";
		public String shoppingBagZipcodeEmptyAlertt = "//*[@id='ErrorMessage_postalCode']";
		public String shoppingBagZipcodeInvalidAlertt = "//*[@id='label-applyPostalCode']";
		public String shoppingBageEstimatedShippingg= "(//*[@id='estChargeDisplayArea']//*[@class='tit tooltipHelp'])[1]";
		public String shoppingBageEstimatedShippingPricee= "(//*[@id='estChargeDisplayArea']//*[@class='price'])[1]";
		public String shoppingBageEstimatedTaxx= "(//*[@id='estChargeDisplayArea']//*[@class='tit tooltipHelp'])[2]";
		public String shoppingBageEstimatedTaxPricee= "(//*[@id='estChargeDisplayArea']//*[@class='price'])[2]";
		
		



		public String shoppingBagProdd = "//*[@class='item-info']";
		public String shoppingBagshopNowbuttonn ="(//*[@class='shopping-left']//a)[2]";
		
		public String shoppingBagPageTitlee ="//*[@class='subtit shopping-bag-header-text']";
		public String shoppingCartCheckoutt = "(//*[@id='shopcartCheckout'])[1]";
		public String shoppingBagPaypall = "(//*[@id='paypalbutonBopis'])[1]";
		public String shoppingCartCheckoutt2 = "(//*[@id='shopcartCheckout'])[2]";
		public String shoppingBagPaypall2 = "(//*[@id='paypalbutonBopis'])[2]";
		public String shoppingBagPostalCodee = "//*[@id='postalCode']";
		public String shoppingBagShippingMethodDropdownn = "//*[@id='hbcshppingmethod']";
		public String shoppingBagShippingMethodApplyy = "//*[@id='estShipCharge']";
		public String shoppingBagShippingAmountTaxSectionn = "//*[@id='estChargeDisplayArea']";
		public String shoppingBagTotalAmountSectionn = "//*[@id='estimateTotalCharges']";

		
		
		
		public String herobannerActivee = "//*[contains(@class,'swiper-slide swiper-slide-active')]";
		//public String maskk = "//*[@class='sk_mobContainerMask showPanCakeMask']"; 
		public String maskk = "//*[contains(@class,'showPanCakeMask')]";
		public String bagIconCountt = "//*[@id='bag_count']";
		public String pancakeMenuu = "//*[@class='sk_mobPanCakeBrowseMenu']";
		public String searchBarr = "//*[@class='sk_mobContentSearchTab']";
		public String searchBoxx = "//*[@class='sk_mobContentSearchInput']";
		public String searchBoxConfirmm = "//*[@class='sk_mobContentSearch skMob_hasCancel sk_mobSearchOpen']";
		public String searchXClosee = "//*[@id='sk_mobSearchForm']//*[@class='sk_mobCross']";
		public String searchXCrosss = "//*[@class='sk_mobSearchForm showCross']";
		public String searchCancell = "//*[@id='sk_mobContentSearch']//*[@class='sk_mobCancelButton']";
		public String searchSuggestDomm = "//*[@class='suggestions']";
		public String searchSuggestionn = "//*[@class='suggestedItem-searchsuggestion']";
		public String searchSuggestItemm = "//*[@class='searchSuggestions']//*[@class='suggestedItems']";
		public String searchRecentHistoryy = "//*[@class='skMobsuggestedItem_container']//*[@class='recentSearchLabel']";
		public String searchRecentHistoryClearAlll = "//*[@class='recentSearchLabel']/span";
		public String searchNoResultss = "//*[@id='noSearchResultTerm']";
		
		//public String plpPagee = "//*[@class='productlistTab sk_sideFilter sk_sideFilterEnabled']";
		//public String plpChanelPagee ="//*[@class='productlistTab sk_sideFilter sk_sideFilterEnabled sk_chanelProductList']";
		public String plpPagee = "//*[contains(@class,'productlistTab sk_sideFilter sk_sideFilterEnabled')]";		
		

		public String plpHeaderTitlee = "//*[@class='skMob_prodCatTitle']";
		public String recentSearchListt = "(//*[@class='recentSuggestion']//*[@class='suggestedItems'])";		
		public String pancakeAdditionalContactt = "//*[@class='additional_links_items']//*[@link='contact']";
		public String pancakeDynamicCategoryy = "//*[@class='sk_mobCategory']";
		public String pancakeStaticCategoryy = "//*[@class='additional_links_items']";
		public String pancakeStaticColorr = "//*[@class='sk_additionalLinksItem ']//*[@class='sk_additionalLinksItemTxt']";
		public String pancakeCategoryActivee = "//*[contains(@class,'sk_menuOpen skMob_currentSelItem')]//*[@class='sk_mobCategoryItemTxt']";
		public String pancakeSubCategoryArroww = "//*[contains(@class,'sk_menuOpen skMob_currentSelItem')]//*[@class='sk_mobCategoryItemLink']";	
		public String pancakemenuBackButtonn = "//*[@class='sk_backBtn open']";
		public String pdpPagee = "//*[@id='id_pdpContainer']";
		public String pancakeStaticWelcomee = "//*[@link='signIn']//*[@class='sk_additionalLinksItemTxt']";
		public String pancakeStorePagee = "//*[@link='store']//*[@class='sk_additionalLinksItemTxt']";		
		public String pancakeStaticSignOutt = "//*[@link='SignOut']//*[@class='sk_additionalLinksItemTxt']";
		public String pancakeStaticViewFlyerr = "//*[@link='flyer']//*[@class='sk_additionalLinksItemTxt']";
		public String pancakeStaticContactUss = "//*[@link='contact']//*[@class='sk_additionalLinksItemTxt']";
		public String pancakeStaticWishlistt = "//*[@link='wishlist']//*[@class='sk_additionalLinksItemTxt']";
		public String contactUsPagee = "//*[@id='contactBlock']//h1";

		public String signInPagee = "//*[@skpagename='sk_hbcMyAccountPage']";
		public String emailFieldd = "//*[@id='email1']";
		public String VerifyemailFieldd = "//*[@id='verifyemail']";
		public String pwdFieldd = "//*[@id='Password']";
		public String verifypwdFieldd = "//*[@id='verifypassword']";
		public String signInButtonn = "//*[@id='WC_AccountDisplay_links_2_label']";
		public String startRegistrationn = "//*[@id='WC_AccountDisplay_links_3']";
		
		public String registrationRewardNumberr = "//*[@id='profileInfoContanier']//*[@id='reward-no']//*[@id='rewardPointStartWithID']";
		public String registrationCountryy = "//*[@id='_country_1']";
		public String registrationStatee = "//*[@id='stateDiv1']//*[@name='state']";
		public String registrationSubmitt = "//*[@id='submitButton_reg']";

		public String registrationEmaillabell = "//*[@for='email1']";
		public String registrationVerifyEmaillabell = "//*[@for='verifyemail']";
		public String registrationPwdlabell = "//*[@for='Password']";
		public String registrationVerifyPwdlabell = "//*[@for='verifypassword']";
		public String registrationPrefLanglabell = "//*[@for='preferredLanguage']";
		public String registrationRewardAlertt = "//*[@id='ErrorMessage_userField1']";
		public String registrationCityAlertt = "//*[@id='ErrorMessage_city']";
		public String registrationZipAlertt = "//*[@id='ErrorMessage_po']";

		public String registrationUkProvincee = "//*[@id='ukstate']//*[@name='state']";
		public String registrationUSStatee = "//*[@id='forstates']//*[@class='drop_down_country']";

		
		
		
		//public String myAccountPagee = "//*[@class='myAccountWraper']";	
		public String myAccountPageNamee = "//*[@class='my-account-summary']//span";	

		public String pancakeStaticlanguagee = "//*[@link='language']";
		public String defaultlanguagee = "//*[@link='language']//*[@class='sk_additionalLinksItemTxt']";
		public String otherlanguagee = "//*[@class='sk_additionalLinksItemTxt sk_lang sk_lang_french']";
		public String plpSubHeaderr = "//*[@class='skMob_subHeaderContainer ']";
		public String plpItemContt = "//*[@class='skMob_ProductCount ']";
		public String back_To_topp = "//*[@class='skMob_backToTop skMob_showBackToTop']";
		public String back_To_top_disabledd = "//*[@class='skMob_backToTop']";				
		public String Swatchess = "//*[@class='sk_prdswatch']";		
		public String MoreSwatchess = "//*[@class='sk_prdsmorewatch']";		
		public String PDPProdNamee = "//*[@class='prdName']";
		public String FilterCountt = "//*[@class='skMob_filterCount']";
		public String plpGridd = "//*[@id='sk_id_layout_grid']";
		public String plpProductss = "//*[@class='skMob_productList']";
		public String pdpPageProductImagee = "//*[@id='id_pdpMasterImageCont']//*[contains(@class,'slide-active')]//img";
		public String pdpImageSelectedd = "//*[contains(@class,'swiper-slide selected')]//img";		
		//public String pdpcollectionImageSelectedd = "//*[contains(@class,'swiper-slide-next selected')]//img";	
		public String pdpcollectionImageSelectedd = "//*[contains(@class,'swiper-slide selected swiper-slide-next')]//img";	
		public String pdpcollectionDetailsSectionn = "//*[@class='pdp_Description expandDetails']//*[@id='detial_main_content']";		

		public String pdpPageSwatchess ="//*[@class='pdpAvailColors']";
		public String pdpPageSwatchesListt ="//*[@class='pdpAvailColors']//img";
		public String pdpPageSwatchSelectedColorr ="//*[@class='selectedColor']";
		public String pdpPageActiveColorr ="//*[@id='pdpActiveColor']";
		
		public String pdpWishListIconn = "//*[@class='skMob_favIcon']";
		public String pdpWishListSuccessOverlayy = "//*[@id='sk_customPopup_wishlist_Success']";
		public String pdpWishListSuccessOkButtonn = "//*[@id='msgContainerOKBtn']";
		public String pdpWishListSuccessCloseButtonn = "//*[@id='msgContainerCloseBtn']";
		public String pdpWishListProductListt = "//*[@class='pro_list']//li";		
		public String WishListProductImagee = "//*[@class='pro_img']//img";		
		public String WishListProductTitlee = "//*[@class='sk_prdtDetails']//a[contains(@class,'tit')]";
		public String WishListProductTitlee2 = "//*[@class='sk_prdtDetails']//*[@class='info']//a";				
		public String WishListProductSizee = "//*[@class='select-info']//span[4]";
		public String WishListProductColorr = "//*[@class='select-info']//span[6]";
		public String WishListProductPricee = "//*[contains(@class,'pro_price')][1]";
		public String WishListAddToCartt = "(//*[@class='allbutton'])[1]";	
		public String myWishlistEmailWishh = "//*[@class='sk_emailUrWLBtn']";
		public String myWishlistAddAllItemss = "//*[@id='addAllItemsToBagBtn']";
		public String myWishlistCloseIconn = "//*[@class='removewish']";				
		public String WishListEmptyWishh = "//*[@class='my_account_wishlist_container']";
		
		public String WishListEmailWishpagee = "//*[@class='sk_myAccPopUp']";
		public String WishListEmailWishpageclosee = "//*[@class='sk_emailWLClsIcn']";
		public String WishListEmailWishpageToEmaill = "//*[@id='SendWishListForm_Recipient_Email']";
		public String WishListEmailWishpageFromNamee = "//*[@id='SendWishListForm_Sender_Name']";
		public String WishListEmailWishpageFromEmaill = "//*[@id='SendWishListForm_Sender_Email']";
		public String WishListEmailWishpageAdditionalMsgg = "//*[@id='wishlist_message']";
		public String WishListEmailWishpageSendButtonn = "//*[@id='wishlistsubmitButton']";
		public String WishListEmailWishpageToEmailAlertt = "//*[@id='ErrorMessage_SendWishListForm_Recipient_Email']";
		public String WishListEmailWishpageFromNameAlertt = "//*[@id='ErrorMessage_SendWishListForm_Sender_Name']";
		public String WishListEmailWishpageCloseDomm = "//*[@class='sk_myAccPopUp sk_hide']";		
		public String WishListEmailWishpageSendEmailAlertt = "//*[@id='ErrorMessageText']";
		//public String WishListProdd = "//*[@class='wishitem']";

		
		

		public String pdppageSwatchColorSelectedd ="//*[@class='pdpAvailColors']//*[contains(@class,'skMob_available selected')]";
		public String pdppageSizee ="//*[@class='pdpActiveSize isSizeAvailable']";
		public String pdppageZoomIconn ="//*[@class='sk_zoomIcon']";
		public String pdppageZoomContainerr ="//*[@class='swiper-zoom-container']";
		public String pdppageZoomMaskk ="//*[@class='sk_zoomPopupMask']";		
		public String pdppageSizeInfoo ="//*[@class='sk_sizeInfo']";
		public String pdppageSizeInfoExpandd ="//*[@class='pdp_Description expandDetails'][@id='sizinginfo']";
		public String pdppageSizeInfoExpandDOMM ="//*[@detailid='sizinginfo'][@style='display: block;']";
		public String pdppageSizeInfoExpandClss ="//*[@itemprop='sizinginfo']//*[@class='pdp_DetailsLink']";
		public String pdppageSizeContainerr ="//*[@id='pdpSizeContainer']";
		public String pdppageSizeContainerListt ="(//*[contains(@class,'pdpSizeSelect pdpSelectEle')])";
		public String pdppageSizeSelectedd ="(//*[@id='pdpSizeContainer']//*[contains(@class,'skMob_available selected')])";
		public String pdppageSizeSelectedDefaultt ="(//*[@id='pdpSizeContainer']//*[contains(@class,'skMob_available selected')])";
		public String pdppageQuantityContainerr ="(//*[@id='pdpQtyContainer'])";
		public String pdppageQuantityPluss ="(//*[@id='id_skMob_quantityPlus'])";
		public String pdppageQuantityMinuss ="(//*[@id='id_skMob_quantityMinus'])";
		public String pdppageQuantityTextt ="(//*[@class='skMob_quantityText '])";
		public String pdppageDetailss ="(//*[@itemprop='detail'])";
		public String pdppageDetailsRewardd ="//*[@class='desc_categoryid desc_RewardPoint']";
		public String pdppageDetailsExpandd ="//*[@class='pdp_Description expandDetails'][@itemprop='detail']";
		public String pdppageDetailsCollapsedd ="//*[@class='pdp_Description']";		
		public String pdppageDetailsExpandDOMM ="//*[@class='descriptionDetails pdp_DetailsCont'][@detailid='detail']";
		public String pdppageDetailsExpandClss ="//*[@itemprop='detail']//*[@class='pdp_DetailsLink']";
		public String pdppageDescriptionAccordss ="//*[contains(@class,'pdp_Description')]";
		public String pdppageAccordsExpandd ="(//*[@class='descriptionDetails pdp_DetailsCont'][@style='display: block;'])";
		public String pdppageAccordsExpandDoMm ="//*[@class='pdp_Description expandDetails']//*[@class='descriptionDetails pdp_DetailsCont']";
		public String pdppageAccordd ="//*[@class='descriptionDetails pdp_DetailsCont']";		
		public String pdppageNoRatingReviewContnrr ="//*[@class='pdpRatingCont']";
		public String pdppageRatingReviewContnrr ="//*[@class='pdpRatingCont ']";
		public String pdppageRatingCountt ="//*[@class='pdpReatingCount']";
		public String pdppageReviewCountt ="//*[@class='sk_proReviewCount']";		
		public String pdppagePriceContainerr ="//*[@class='pdpPriceContent']";
		public String pdppagePriceSalee ="//*[@pricetype='Sale']//*[@class='skmob_priceValue ']";
		public String pdppagePriceRegularr ="//*[@pricetype='Reg']//*[@class='skmob_priceValue ']";
		public String pdppageATBb ="//*[@class='pdpAddtoBagBtn']";
		public String pdppageATBOverlayy ="//*[@class='sk_mobPdtAddToBagCont sk_mobBag sk_QuickViewAnimIn']";
		public String pdppageATBOverlayClosee ="//*[@class='sk_mobCloseImg']";
		public String pdppageShareIconss ="//*[@class='shareImgs']";
		public String pdppageShareFBb ="//*[@id='share_FB']";
		public String pdppageShareTwitterr ="//*[@id='share_Twitter']";
		public String pdppageShareGpluss ="//*[@id='share_Google']";		
		public String pdppageIframeVideoo ="//*[@class='pdp-desc-wrapper']//iframe[1]";		
		public String pdppageIframeRelatedVideoo ="//*[@class='pdp-desc-wrapper']//iframe[2]";	
		public String pdppageVideoo ="//*[@id='videoview_videoPlayer']";
		public String pdppageVideoContainerr ="//*[@id='videoview_container']";
		public String videoPlayPausee ="//*[@id='videoview_playPauseButton']";
		public String videoSocialSharee ="//*[@id='videoview_socialShare']";
		public String videoEmailSharee ="//*[@id='videoview_emailShare']";
		public String videoEmbedSharee ="//*[@id='videoview_embedShare']";
		public String videoLinkSharee ="//*[@id='videoview_linkShare']";
		public String videoTwitterSharee ="//*[@id='videoview_twitterShare']";
		public String videoFacebookSharee ="//*[@id='videoview_facebookShare']";
		public String videoFullScreenn ="//*[@id='videoview_fullScreenButton']";
		public String relatedVideoss ="//*[@id='player']";
		public String relatedVideoplaypausee ="//*[@class='ytp-icon ytp-icon-large-play-button-hover']";
		public String pdpWebIdd ="//*[@class='desc_categoryid']";

		public String footerSectionsDomm ="//*[@skpagename='skhbmobilefitpage']";

		
		
		public String ATBAlertt ="//*[@class='sk_tabAlertPopUp']";
		public String ATBAlertOkk ="//*[@class='sk_trueButton sk_tabAlertOkBtn']";

	
		
		public String callToOrderr ="//*[@class='skpdp_addinfrm']/a[1]";
		public String collectionProductContianerr ="//*[@class='skMob_pdpBundleContainer']";
		public String collectionProductSelectYourItemss ="//*[@class='skMob_pdpBundleContainer']//*[@class='skChooseItemsBelow']";

		
		public String collectionProductListt ="//*[@class='skMob_pdpBundleItemCont']";
				
		public String socialIconsListss = "(//*[@class='sk_footerBottomIconCont']//a)";
		public String MenuListss = "(//*[@class='footer-menu-list active']//*[@class='link-data'])";
		public String advertiseBannerr = "(//*[@class='skHDHeroWidContainer'])";
		public String productCarouselListt = "//*[@widgettype='HBCProductCarousel']//*[@class='caoruselItemCont']";
		public String pancakeMenulistt = "(//*[@class='sk_mobCategoryItem '])";
		public String pancakeMenuActivelistt = "(//*[contains(@class,'sk_menuOpen skMob_currentSelItem')])";
		public String pancakeStaticListt = "(//*[@class='sk_additionalLinksItem '])";
		public String pancakeSub_SubListt = "//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]//*[@id='sk_mobCategoryItem_id_3_3'][1]//*[@id='sk_mobCategoryItem_id_4_4']";
		public String plpItemContListt = "//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_addToCompareCont']";

		public String RefineHeaderr = "//*[@class='skMob_titleCont']";
		public String sortt = "//*[@class='sk_sort']";
		public String Refinewindoww = "//*[@id='skMob_sideFilterContainer_id']";
		public String RefinewindowApplyy = "//*[@id='skMob_filterDone_id']";
		public String RefinewindowClearAlll = "//*[@id='skMob_clearAllFilter_id']";

		public String RefinewindowClosee = "//*[@class='skMob_sideFilterPopUpCloseIcon']";
		public String Filterr = "//*[@class='skMob_filterView_icn']";
		
		public String sortByy ="//*[@class='skRes_soryBy_txt']";
		public String sortByOptionn ="//*[@class='skRes_sortedValue_txt']";
		public String sortByArroww ="//*[@class='skRes_sortSelectArw']";
		public String sortByOpenDomm ="//*[@class='skRes_sortOptionsCont']";		
		public String sortByOpenn ="//*[@class='skRes_sortBox']";		
		public String sortOptionss ="//*[@id='sk_sortOptionContainer_id']";
		public String sortOptionSelectedd ="//*[contains(@class,'skMob_sortOptionSelected')]";
		public String sortOptionNOTSelectedd ="//*[@class='sk_sortOptionContainer']//*[@class='skRes_sortOption']";
		public String sortOptionNOTSelecteddd ="//*[@class='sk_sortOptionContainer ']//*[@class='skRes_sortOption']";

		public String sortOptionSelectetickk ="//*[contains(@class,'skMob_sortOptionSelected')]//*[@class='sk_sortCustomChkbox']//span";
//		public String sortOptionBoldd ="//*[@class='sk_sortOptionContainer  skMob_sortOptionSelected']//*[@class='skRes_sortOption']";
		public String sortOptionBoldd ="//*[contains(@class,'skMob_sortOptionSelected')]//*[@class='skRes_sortOption']";

		public String loadingbarr ="//*[@class='skMob_ui_loadingBar']";
		public String filterOptionSelectetickk ="(//*[contains(@class,'skMob_filterItemSelected')])";
		public String filterOptionSelectedd="//*[@class='skMob_filterItems skMob_filterItemSelected']";

		public String FilterAttributess ="//*[@class='skTab_filterContentScrollerContainer']";
		
		public String checkoutButtonn ="//*[@id='shopcartCheckout']";
		public String shippingPaymentPagee ="//*[@skpagename='skhbcorderbillingpage']";
		public String checkoutNavTitlee = "//*[@id='checkoutTitle']";
		public String checkoutNavTabss = "//*[@class='breadcrumbs']";
		public String checkoutNavTabb1 = "//*[@id='checkoutTitle']/div[1]";
		public String checkoutNavTabb2 = "//*[@id='checkoutTitle']/div[3]";
		public String checkoutNavTabb3 = "//*[@id='checkoutTitle']/div[5]";
		public String checkoutCurrNavTitlee = "//*[@id='checkoutTitle']//div[@class='current']";
		
		public String shipandPaypageShipSecc = "//*[@id='shipping']";
		public String shipandPaypagePaySecc = "//*[@id='paymentSection1']";
		
		public String shippingAlertt = "//*[@id='label-shippingdropDown']";
		public String paymentAlertt = "//*[@id='label-billingdropdown_1']";
		public String shipandPaypageShipEditLinkk = "//*[@id='shipping']//*[@class='address-link']//a";
		public String shipandPaypagePayEditLinkk = "//*[@id='billingAddress1']//*[@id='editBillingAddressLink_1']//a";
		public String shipandPaypageShipCreateAddLinkk = "//*[@id='shipping']//*[@id='newShippingAddressLink']//a";
		public String shipandPaypagePayCreateAddLinkk = "//*[@id='billingAddress1']//*[@id='createNewBillingAddress_1']//a";
		
		public String shipandPaypageShipShipCreateEditAddresss = "//*[contains(@class,'dijitDialogFixed dijitDialog')]";
		public String shipandPaypageShipCreateEditAddressOverlayy = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]";
		public String shipandPaypageShipShipCreateEditAddressCloseLinkk = "//*[@wairole='dialog']//*[@class='closeText']";
		public String shipandPaypageShipShipCreateEditAddressCancelBtnn = "//*[@id='cancelButton_label']";
		public String shipandPaypageShipShipCreateEditAddressOkBtnn = "//*[@id='submitButton_label']";		
		public String shipandPaypageShipAddressDispp = "//*[@id='shipping']//p";
		public String shipandPaypageShipAddressColorDispp = "//*[@id='shipping']//*[@class='address_info']";
		
		

		public String shipandPaypageShipShipCreateEditAddresssFNamee = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='fname']";
		public String shipandPaypageShipShipCreateEditAddresssLNamee = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='lname']";
		public String shipandPaypageShipShipCreateEditAddresssStAddresss = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='address']";
		public String shipandPaypageShipShipCreateEditAddresssStAddresss2 = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='address1']";
		public String shipandPaypageShipShipCreateEditAddresssCtyy = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='city']";
		public String shipandPaypageShipShipCreateEditAddresssCountryy = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='_country_1']";
		public String shipandPaypageShipShipCreateEditAddresssStatee = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='province']";
		public String shipandPaypageShipShipCreateEditAddresssPayStatee = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='stateDiv1']//select";
		public String shipandPaypageShipShipCreateEditAddresssPayUKStatee = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='ukstate'][contains(@style,'block')]//*[@id='widget_WC__ShoppingCartAddressEntryForm_billing_address_form_state_1']";
		public String shipandPaypageShipShipCreateEditAddresssPOcodee = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='po']";
		public String shipandPaypageShipShipCreateEditAddresssNonUsUkPOcodee = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='nonUS1'][contains(@style,'block')]//*[@id='po1']";
 		public String shipandPaypageShipShipCreateEditAddresssPHH1 = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='bphone1']";
		public String shipandPaypageShipShipCreateEditAddresssPHH2 = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='bphone2']";
		public String shipandPaypageShipShipCreateEditAddresssPHH3 = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='bphone3']";
		public String shipandPaypageShipShipCreateEditAddresssPHH4 = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='bphone4']";
		public String shipandPaypageShipShipCreateEditAddresssUKPhonee = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]//*[@class='address-form']//*[@id='widget_ukphone']";

		
		
		public String shipandPaypageShipShipCreateEditAddresssFnameLabell = "//*[@for='fname']";
		public String shipandPaypageShipShipCreateEditAddresssLNameLabell = "//*[@for='lname']";
		public String shipandPaypageShipShipCreateEditAddresssStAddressLabell = "//*[@for='address']";
		public String shipandPaypageShipShipCreateEditAddresssCtyLabell = "//*[@for='city']";
		public String shipandPaypageShipShipCreateEditAddresssCountryLabell = "//*[@class='address-form']//li[5]/label";
		public String shipandPaypageShipShipCreateEditAddresssStateLabell = " //*[@for='province'][@id='nonUS']";
		public String shipandPaypageShipShipCreateEditAddressStateLabell = " //*[@for='province']";
		public String shipandPaypageShipShipCreateEditAddresssPOcodeLabell = "//*[@for='po']";
		public String shipandPaypageShipShipCreateEditAddresssNonUsPOcodeLabell = "//*[@for='po1']";
		public String shipandPaypageShipShipCreateEditAddresssPHLabell = "//*[@for='phone']";
		public String shipandPaypageShipShipCreateEditAddresssEmailLabell = "//*[@for='email']";
		public String shipandPaypageShipShipCreateEditAddresssUKContryLabell = "//*[@id='ukstate']//*[@id='nonUS']";
		public String shipandPaypageShipShipCreateEditAddresssPHUKLabell = "//*[@for='ukphone']";

		
		
		
		public String bStateStatee = "//*[@class='billing-address']//*[@id='forstates']";
		public String addressVerificationoverlayy = "//*[@class='ui-dialog ui-widget ui-widget-content ui-corner-all'][@aria-labelledby='ui-dialog-title-QAS_Dialog']";
		public String addressVerificationEditt = "//*[@class='QAS_EditLink']//a";
		public String bpostalCodeee = "//*[@id='po']";
		public String checkoutBPOAlertt = "//*[contains(@id,'ErrorMessage_po')]";
		public String checkoutSPHAlertt1 = "//*[@id='ErrorMessage_bphone1']";
		public String checkoutSPHAlertt3 = "//*[@id='ErrorMessage_bphone3']";
		public String shipandPaypageShipShipToNamee = "//*[@id='shipping']//*[@class='drop_down shipto']";
		public String shipandPaypageShipPayToNamee = "//*[@id='billingAddress1']//*[@class='drop_down shipto']";

		public String addressuseasEnteredd = "//*[@id='QAS_AcceptOriginal']";
		public String progressBarWidgett = "//*[@widgetid='ajaxblock_dialog']";
		public String shipandPaypagee = "//*[@id='mainContents']";

		public String shipandPaypagePayAddressDispp = "//*[@id='billingInfo']";
		public String shipandPaypageShipShipCreateEditEmaill = "//*[@id='email1']";
		public String shipandPaypageShipShipCreateEditTermsandCondd = "//*[@class='ifreceive']";

		public String shipandPaypageShipShipCreateEditTermsandCondChkboxx = "//*[@class='ifreceive']//*[@name='ifreceive']";

		public String paymentOptionsPayByCardd = "//*[@class='pay_pannel sk_payByCreditCardPay']";
		public String paymentOptionspaybycardChkkBoxx = "//*[@class='payByCardCheckImg']";	
		public String paymentOptionspaybycardChkkBoxSelectedd = "//*[@class='payByCardCheckImg sk_selected']";		

		
		public String paymentOptionsCreditCardd = "//*[@class='pay_pannel sk_creditCardPay']";
		public String paymentOptionsCreditCardChkkBoxx = "//*[@class='creditCardCheckImg']";
		public String paymentOptionsCreditCardSecEnablee = "//*[contains(@class,'pay-by-credit-card')]";
		public String paymentOptionsCreditCardSelectedd = "//*[@class='pay_pannel sk_creditCardPay sk_selected']";
		public String paymentOptionsPaypall = "//*[@class='pay_pannel sk_payPalPay']";
		public String paymentOptionsPaypalChkkBoxx = "//*[@class='paypalCheckImg']";
		public String paymentOptionsWhatIspaypall = "//*[@class='pay_pannel sk_payPalPay sk_selected']//a";
		public String paymentOptionsPaypalSelectedd = "//*[@class='pay_pannel sk_payPalPay sk_selected']";
		public String paymentOptionsCreditCardCardSelDropDownn = "//*[contains(@class,'pay-by-credit-card')]//*[@id='payMethodId_1']";
		public String paymentOptionsCreditCardCardSelDropDownLabell= "//*[@class='type']//span";
		public String payPalLogoo= "//*[@id='paypalLogo']";
		public String payPalLoadingSpinnerr= "//*[@id='preloaderSpinner']";
		public String payPalPageCancell= "//*[@id='defaultCancelLink']//a";

		
		
		
		public String paymentOptionsGiftCardd = "//*[@class='pay_pannel sk_giftCardPay']";
		public String paymentOptionsGiftCardChkkBoxx = "//*[@class='giftCardCheckImg']";

		public String paymentOptionsRewardCardFieldLabell = "//*[@class='payment-pannel']//*[@id='reward-no']";
		public String paymentOptionsRewardCardFieldd1 = "//*[@class='payment-pannel']//*[@id='reward-no']//*[@id='rewardPointStartWithID']";
		public String paymentOptionsRewardCardFieldd2 = "//*[@class='payment-pannel']//*[@id='reward-no']//*[@id='widget_userField1']//*[@id='userField1']";
		public String paymentOptionsRewardCardFieldAlertt = "//*[@id='ErrorMessage_userField1']";
		public String paymentOptionsPromoCodee = "//*[@id='widget_promoCode']//*[@id='promoCode']";
		public String paymentOptionsPromoCodeApplyy = "//*[@id='WC_PromotionCodeDisplay_links_1']";
		public String paymentOptionsPromoCodeApplyAlertt = "//*[@id='promoCodeErrMsg']";
		public String paymentOptionsPromoCodeApplyInvalidAlertt = "//*[@id='promoCode_errorlabel']";
		public String paymentOptionsShippingMethoddropdownn = "//*[@class='shipiing-taxes']//*[@id='hbcshppingmethod1']";
		
		public String loadingGaugeDomm = "//*[@id='ajaxblock']";
		//paymentOptionsShippingMethoddropdownDomm
		
		public String paymentOptionsEstimatedSubtotall = "//*[@class='sub-total']//span[2]";
		public String paymentOptionsEstimatedSubtotalPricee = "//*[@class='sub-total']//span[1]";
		public String paymentOptionsShippingAmountlabell = "//*[@class='sk_formatted']";
		public String paymentOptionsShippingAmountPricee = "//*[@class='right sk_formatted']//*[@class='price'][1]";		
		public String paymentOptionsPSTLabell = "//*[@class='normal tooltipHelp'][3]//span";
		public String paymentOptionsPSTPricee = "//*[@class='price'][3]";
		public String paymentOptionsGSTHSTLabell = "//*[@class='normal tooltipHelp'][4]//span";
		public String paymentOptionsGSTHSTPricee = "//*[@class='price'][4]";
		public String paymentOptionsOrderTotall = "//*[@class='order-total']//*[@id='estimateTotalCharges']";
		public String paymentOptionsOrderTotalLabell = "//*[@class='order-total']//*[@class='normal']";
		
		
		public String paymentOptionsCreditCardImagess = "//*[@id='CreditCardList']//li";
		public String paymentOptionsCreditCardAmountToPaylabell = "//*[@class='pay']//span[1]";
		public String paymentOptionsCreditCardAmountToPayPricee = "//*[@class='pay']//*[@class='price']";
		public String paymentOptionsCreditCardNumberFieldd = "//*[@class='open']//*[@id='account1_1']";		
		public String paymentOptionsCreditCardNumberFieldAlertt = "//*[@id='ErrorMessage_account1_1']";		
		public String paymentOptionsCreditCardSecurityCodeFieldd = "//*[@class='open']//*[@id='cc_cvc_1']";		
		public String paymentOptionsCreditCardSecurityCodeFieldAlertt = "//*[@id='ErrorMessage_cc_cvc_1']";		
		public String paymentOptionsCreditCardExpMonthFieldd = "//*[@class='open']//*[@id='expMonth']";		
		public String paymentOptionsCreditCardExpMonthFieldAlertt = "//*[@id='ErrorMessage_expMonth']";	
		public String paymentOptionsCreditCardExpYearFieldd = "//*[@class='open']//*[@id='expYear']";		
		public String paymentOptionsCreditCardExpYearFieldAlertt = "//*[@id='ErrorMessage_expYear']";	

		//public String paymentOptionsApplyGiftCardd = "//*[@id='label-PaybyGiftCard']";
		public String paymentOptionsApplyGiftCardNumberFieldd = "//*[@id='GCcardNumber']";	
		public String paymentOptionsApplyGiftCardPinFieldd = "//*[@id='pin']";
		public String paymentOptionsApplyGiftCardApplyy = "//*[@id='apply']";
		public String paymentOptionsAddAnotherGiftCardd = "//*[@class='add-another-card']//a";
		public String secondGiftCardd = "//*[@class='cardinfo'][contains(@style,'height')]";
		public String paymentOptionReviewOrderr = "//*[@id='shippingBillingPageNext']";

		public String ReviewandSubmitPagee = "//*[@class='checkout-review-submit the-bay']";
		public String ReviewandSubmitShippingAddresss = "//*[@class='pmt-ship-addr']";
		public String ReviewandSubmitShippingAddressEditt = "//*[@id='editShipAddressLink']";		
		public String ReviewandSubmitShippingMethodd = "//*[@class='pmt-ship-method']";
		public String ReviewandSubmitShippingMethodEditt = "//*[@class='pmt-ship-method']//*[@class='edit']";
		public String ReviewandSubmitBillingAddresss = "//*[@class='pmt-bill-addr']";
		public String ReviewandSubmitBillingAddressEditt = "//*[@class='pmt-bill-addr']//*[@class='edit']";
		public String ReviewandSubmitBillingMethodd = "//*[@class='pmt-method']";
		public String ReviewandSubmitBillingMethodEditt = "//*[@class='pmt-method']//*[@class='edit']";
		public String ReviewandSubmitCheckoutSummaryy = "//*[@class='checkout-summary']";
		public String ReviewandSubmitPlaceorderbuttonn = "//*[@id='placeOrderButton']";
		
		public String shippingandPaymentt = "//*[@class='breadcrumbs']//a[2]";
		public String ReviewandSubmitShippingAddressDetailss = "//*[@class='pmt-ship-addr']//p";
		public String ReviewandSubmitShippingMethodSelectedd = "//*[@class='pmt-ship-method']//p";
		public String ReviewandSubmitBillingAddressDetailss = "//*[@class='pmt-bill-addr']//p";
		public String ReviewandSubmitBillingMethodDetailss = "//*[@class='pmt-method']//p";
		
		public String ReviewandSubmitBillingAddressNameDetailss = "//*[@class='pmt-bill-addr']//p//*[@class='name']";

		public String chkoutAddressPagee = "//*[@class='address-pannel']";
		public String bContactNoo = "//*[@class='billingTelephone']";
		public String AddressSubmitt = "//*[@id='submitButton']";		
		
		

		public String ReviewandSubmitOrderTotall = "//*[@class='order-total']//span";


		public String placeOrderr = "//*[@id='placeOrderButton']";		
		public String confirmOrderPagee = "//*[@skpagename='sk_hbcOrderConfirmPage']";		
		
		public String confirmThanksSectionn = "//*[@id='thanks-for-order']";		
		public String confirmShippingInfoSectionn = "//*[@id='shipping']";		
		public String confirmPaymentInfoSectionn = "//*[@id='payment']";		
		public String confirmPaymentDetailSectionn = "//*[@class='payment']";		
		public String confirmRewardDetailSectionn = "//*[@class='left hbc-reward-title']";		
		public String confirmsubTotalSectionn = "//*[@class='sub-total']";		
		public String confirmPromotionSectionn = "//*[@id='promotions']";	
		public String confirmShippingTaxesSectionn = "//*[@id='shiptaxes']";	
		public String confirmOrderTotalSectionn = "//*[@class='order-total']";	
		public String confirmThanksSectionOrdernumberr = "//*[@id='thanks-for-order']//strong[1]";		

		
		public String addressVerificationInfoTextt = "//*[@class='QAS_Header ui-state-highlight']";
		public String addressVerificationAddressEditLinkk = "//*[@class='QAS_EditLink']//a";
		public String addressSuggestionListt = "//*[@class='QAS_MultPick']";
		public String addressSuggestionAddressListt = "//*[@class='QAS_MultPick']//tr";
		public String addressSuggestionDeliveryWarningg = "//*[@class='QAS_DeliverableWarning']";
		public String addressSuggestionClosee = "//*[@class='ui-icon ui-icon-closethick']";
		public String addressSuggestedBtnn = "//*[@id='QAS_RefineBtn']";
		public String userEnteredAddresss = "//*[@class='QAS_RightDetails']//td";
		public String addressVerificationbuildingTextboxx = "//*[@id='QAS_RefineText']";
		public String addressVerificationSuggestedAddresss = "//*[@class='QAS_PromptData']//td";
		public String addressVerificationPotentialMatchesTextt = "//*[@class='QAS_ShowPick']//a";
		public String addressVerificationPotentialMatchesAddresss = "//*[@class='QAS_Pick']";
		public String addressVerificationPotentialMatchesAddressListt = "//*[@class='QAS_Pick']//tr";
		public String reviewOrderGeneralErrorr = "//*[@id='ErrorMessage_General']";

		public String myAccountPagee = "//*[@skpagename='sk_hbcMyAccountPage']";		
		public String myAccountPageTitlee = "//*[@id='main_content']//*[@class='subtit']";
		public String myAccountPageWelcomee = "//*[@class='my-account-summary']//h3[1]";
		public String myAccountPageMyProfileTxtt = "//*[@class='my-account-summary']//h3[2]";
		public String myAccountPageMyProfileEditt = "//*[@class='my-account-summary']//*[@class='left']//a";
		public String myAccountPageMyProfileDetailss = "//*[@class='myprofile']";		
		public String myAccountPageMenuListt = "//*[@class='subleftmenu']//li";
		public String myAccountPageMyProfilee = "//*[@class='subleftmenu']//li[1]//a";
		public String myAccountPageAddressbookk = "//*[@class='subleftmenu']//li[2]//a";
		public String myAccountPageNotificationPreff = "//*[@class='subleftmenu']//li[3]//a";
		public String myAccountPageMyOrderss = "//*[@class='subleftmenu']//li[4]//a";
		public String myAccountPageMyWishlistt = "//*[@class='subleftmenu']//li[5]//a";
		public String myAccountPageSignOutt = "//*[@class='sk_signOut']";			
		public String myAccountPageAddressDropDownn = "//*[@class='sk_selectEl']//*[@id='addressId']";
		public String myAccountPageAddressRemoveButtonn = "//*[@id='WC_AddressBookForm_links_2']";
		public String myAccountMyProfilepagee = "//*[@id='myAcctChngProfile']";
		public String myAccountAddressBookpagee = "//*[@id='WC_AddressBookForm_div_7']";
		public String myAccountNotifPreferncePagee = "//*[@id='notification']";
		public String myAccountMyOrdersPagee = "//*[@id='searchByOrderNumber']";
		public String myAccountpageBackk = "//*[@class='sk_myAccLinkEl']";

		
		
		public String myPageFnamee = "//*[@id='firstName']";
		public String myPageLnamee = "//*[@id='lastName']";
		public String myPageEmaill = "//*[@id='email1']";
		public String myPageEmailAlertt = "//*[@class='profileEmailAlert']";		
		public String myPagePasswordd = "//*[@id='logonPassword_old']";
		public String myPageVerifyPasswordd = "//*[@id='logonPasswordVerify_old']";
		public String myPageRewardPointIdd = "//*[@class='pro-address']//*[@id='rewardPointStartWithID']";
		public String myPageRewardCardNumm = "//*[@id='userField1']";
		public String myPageStAddress = "//*[@id='address1']";
		public String myPage2StAddress = "//*[@id='address2']";
		public String myPageCityy= "//*[@id='city']";
		public String myPageCountryy= "//*[@id='_country']";
		public String myPageStatee= "//*[@id='state']";
		public String myPageZipcodee= "//*[@id='zipCode']";
		public String myPagePhoneFieldd= "//*[@class='billingTelephone']";
		public String myPagePhoneExtt= "//*[@id='phoneext']";
		public String myPagePrefLangg= "//*[@class='preflang']//*[@id='preferredLanguage']";
		public String myPageBirthmonthh= "//*[@id='birth_month']";
		public String myPageBirthDatee= "//*[@id='birth_date']";
		public String myPageGenderFemalee= "(//*[@class='sk_radioBtn'])[1]";
		public String myPageGenderMalee= "(//*[@class='sk_radioBtn'])[2]";
		public String myPagePrivacyy= "//*[@class='privacy']//a";
		public String myPageUpdatee= "//*[@id='profsubmitButton']";		
		public String myPageFnameAlertt = "//*[@id='ErrorMessage_firstName']";
		public String myPageLnameAlertt = "//*[@id='ErrorMessage_lastName']";
		public String myPagePasswordAlertt = "//*[@id='ErrorMessage_logonPassword_old']";
		public String myPageVerifyPasswordAlertt = "//*[@id='ErrorMessage_logonPasswordVerify_old']";
		public String myPageStAddressAlertt = "//*[@id='ErrorMessage_address1']";
		public String myPageStCityAlertt = "//*[@id='ErrorMessage_city']";
		public String myPageZipAlertt = "//*[@id='ErrorMessage_zipCode']";
		public String myPagePhoneAlertt = "//*[@id='ErrorMessage_pphone']";
		public String myPageRewardAlertt = "//*[@id='ErrorMessage_userField1']";
		
		
		public String myAddressBookPageAddressAddNewButtonn = "//*[@id='WC_AddressBookForm_links_1']";
		public String myAddressBookPageCountryy= "//*[@id='country']";
		public String myAddressBookPagePhoneFieldd= "(//*[@class='billingTelephone'])[1]";
		public String myAddressBookPageUpdatee= "//*[@id='WC_AddressBookForm_links_3']";
		//AddNew Address
		public String myAddressBookNewFnamee = "//*[@id='firstName1']";
		public String myAddressBookNewLnamee = "//*[@id='lastName1']";
		public String myAddressBookNewStAddress = "//*[@id='address11']";
		public String myAddressBookNew2StAddress = "//*[@id='address2']";
		public String myAddressBookNewCityy= "//*[@id='city1']";
		public String myAddressBookNewStatee= "//*[@id='state1']";
		public String myAddressBookNewZipcodee= "//*[@id='zipCode1']";
		public String myAddressBookPageNewCountryy= "//*[@id='country1']";
		public String myAddressBookPageNewPhoneFieldd= "(//*[@class='billingTelephone'])[2]";
		public String myAddressBookPageNewPhoneExtt= "//*[@id='phoneext1']";
		public String myAddressBookPageNewCancell= "(//*[@class='button_footer_line']//*[@class='allbutton'])[2]";
		public String myAddressBookPageNewSubmitt= "//*[@id='WC_AddressBookForm_links_4']";
		public String myAddressBookSuccessAddAlertt = "//*[@id='MessageArea']";
		public String myAddressBookSuccessRemoveAlertt = "//*[@id='ErrorMessageText']";
		public String myAddressBookFnameAlertt = "//*[@id='ErrorMessage_firstName1']";
		public String myAddressBookLnameAlertt = "//*[@id='ErrorMessage_lastName1']";		
		public String myAddressBookStAddressAlertt = "//*[@id='ErrorMessage_address11']";
		public String myAddressBookStCityAlertt = "//*[@id='ErrorMessage_city1']";
		public String myAddressBookZipAlertt = "//*[@id='ErrorMessage_zipCode1']";

		public String myOrdersOrdernumberr = "//*[@id='orderNumber']";
		public String myOrdersPostalnumberr = "//*[@id='postalNumber']";
		public String myOrdersGetStatusButtonn = "//*[@id='submitButton']";
		public String myOrdersGetStatusCSSButtonn = "//*[@widgetid='submitButton']//*[contains(@class,'dijitButtonNode')]";		
		public String myOrdersCustServiceNumberr = "//*[@id='customerNo']";		
		public String myOrdersOrdernumberAlertt = "//*[@id='ErrorMessage_orderNumber']";
		public String myOrdersPostalnumberAlertt = "//*[@id='ErrorMessage_postalNumber']";

		
		
		public String chanelLandingg = "//*[@id='chanel']";
		public String chanelHeaderr = "//*[@id='chanelHeader']";
		public String chanelPromoBannerr = "//*[contains(@class,'swiper-slide')]//img";
		public String chanelPromoBannerDotss = "//*[@class='swiper-pagination swiper-pagination-clickable swiper-pagination-bullets']//*[contains(@class,'swiper-pagination-bullet')]";
		public String chanelCategoryy = "//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']";
		public String chanelCategoryOpenStatee = "//*[contains(@class,'sk_categorymenuopen')]";
		public String chanelCategoryOpenSectionCloseArroww = "//*[contains(@class,'sk_categorymenuopen skMob_currentSelItem')]//*[@class='sk_mobCategoryMenuItemLink skmob-header-sprite']";
		public String chanelPLPHeaderr = "//*[@class='sk_chanelheader']";
		public String chanelPLPProductCountt = "//*[@class='skMob_ProductCount prodCountChanelHt']";

		
		public String chanelPLPswatchContainerr = "//*[@class='prd_color_count']";

		
		public String chanelDescriptionn = "//*[@widgettype='global_textEditorWidget']//*[@id='chanelDescription']";
		public String chanelHeroBannerr = "//*[@widgettype='hbcHeroWidget']//img";

		

		
		public String brandPagee = "//*[@id='brands']";
		public String brandPageTitlee = "//*[@class='sk_brandsTitleCont']";
		public String brandPagedropdownn = "//*[@class='sk_brandsCategories']";
		public String brandPageAlphaSectionn = "//*[@class='sk_brandsNameCont']";
		public String brandPageNameSectionn = "//*[@class='sk_brandsLinksMainCont']";
		public String brandpageAlphabetss = "//*[@class='sk_brandsName available']";
		public String brandpageAlphabetsActivee = "//*[@class='sk_brandsLinksCont active']//*[@class='sk_brandsLinkName']";
		public String brandpageDropDownSelectionn = "//*[@class='sk_brandsLinksCont active']//*[@class='sk_brandsLinkInner']//div";
		public String brandpageAlphabetUnavaill = "//*[@class='sk_brandsNameCont']//*[@class='sk_brandsName']";
		public String brandPageHashh = "//*[@char='hash']";

		
		
		
		public String loginPageUsernamee = "//*[@id='WC_CheckoutLogon_FormInput_logonId']";
		public String loginPagePasswordd = "//*[@id='WC_CheckoutLogon_FormInput_logonPassword']";
		public String loginPageSignInbuttonn = "//*[@id='signInAndCheckoutbtn']";
		public String loginPageForgotPwdd = "//*[@class='forgotPSW txtPad']";
		public String loginPageContinueShoppingg = "//*[@class='continue_shopping']";
		public String loginPageClosee = "//*[@id='checkout_popup']//*[@class='closeText']";

		public String  invalidUnamePwdAlertt= "//*[@id='MessageArea']";

		public String loginPageSignInbuttoncolorr = "//*[@widgetid='signInAndCheckoutbtn']//*[contains(@dojoattachevent,'_onButtonClick')]";
		public String loginPageGuestchkoutbuttoncolorr = "//*[@widgetid='guestShopperContinue']//*[contains(@dojoattachevent,'_onButtonClick')]";


		public String  forgotPwdDefaultTxtt= "//*[@class='intrduc']";
		public String  forgotPwdSuccessAlertt= "//*[@class='strong']";
		public String  forgotPwdCntnueLoginPagee= "//*[@id='WC_PasswordResetDisplay_Link_1']";


		
		public String  notificationPrefEmaill= "//*[@class='myprofile']//li[1]";
		public String  notificationPrefFnamee= "//*[@class='myprofile']//li[2]";
		public String  notificationPrefLnamee= "//*[@class='myprofile']//li[3]";
		public String  notificationPrefInterestedSecc= "//*[@class='proinfo-check proinfo2']";
		public String  notificationPrefupdatee= "//*[@id='notisubmitButton']";

		public String  notificationPrefInterestedSecLabell= "//*[@class='proinfo-check proinfo2']//*[@class='normal']";
		public String  notificationPrefInterestedSecCheckBoxlabell= "//*[@class='proinfo-check proinfo2']//*[@class='check-like']//label";
		public String  notificationPrefInterestedSecCheckBoxx= "//*[@class='proinfo-check proinfo2']//*[@class='check-like']//*[@type='CHECKBOX']";
		public String  notificationPrefInterestedSelectedChckk= "//*[@class='checkbox checked']";

		
		
		
		
		
		
		
		
		
		
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////
		
		public String noSearchRess = "//*[@class='skMob_noSearchFound skMob_isSearch setToTop']";
		public String noPrdtFoundd = "//*[@class='pdp_not_found']";
		
		public String pdpPageWrapperr = "//*[@id='id_skStudio_Wrapper']//*[@id='id_pdpWrapper']";
		public String pdpSuccessIdd = "//*[@id='sk_addtobagSuccess_id']";
		public String pdpcheckOutBtnn = "//*[contains(@class,'skMob_ATBCheckoutBtn')]";		
		public String guestSignInPopupp = "//*[@id='checkout_popup']";
		public String guestContinueBtnn = "//*[@id='guestShopperContinue']";
		public String bpostalCodee = "//*[@id='nonUS1'][contains(@style,'block')]//*[@id='po1']";

		public String gccheckoutbillingAddressTitlee = "//*[@class='addresses']//span";
		public String gccheckoutshippingAddressTitlee = "//*[@class='shippingAddress']//span";
		public String firstNamee = "//*[@id='fname']";
		public String lastNamee = "//*[@id='lname']";
		public String staddresss = "//*[@id='address']";
		public String staddresss2 = "//*[@id='address2']";
		public String cityy = "//*[@id='city']";
		public String bcountrySelectionn = "//*[@class='billing-address']//*[@name='country']";
		public String bStatee = "//*[@class='billing-address']//*[@id='forstates'][contains(@style,'block')]//*[@name='state']";
		//public String bukStatee = "//*[@class='billing-address']//*[@id='ukstate'][contains(@style,'block')]//*[@name='state']";
		public String bukStatee = "//*[@class='billing-address']//*[@id='ukstate'][contains(@style,'block')]//*[@id='widget_WC__ShoppingCartAddressEntryForm_billing_address_form_state_1']";
		public String bukStateEnterr = "//*[@class='billing-address']//*[@id='ukstate'][contains(@style,'block')]//*[@id='WC__ShoppingCartAddressEntryForm_billing_address_form_state_1']";
		
		
		public String bcapostalCodee = "//*[@id='po1']";
		public String UspostalCodee = "//*[@id='po']";

		//public String bContactNoo = "//*[@class='billingTelephone']";
		public String bContactNooo = "//*[@id='fornonUK'][contains(@style,'block')]//*[@class='billingTelephone']";
		//public String bextCodee = "//*[@id='bphone4']";
		public String bextCodee = "//*[@id='fornonUK'][contains(@style,'block')]//*[@id='bphone4']";
		public String brewardssIdd = "//*[@id='rewardPointStartWithID']";
		//public String brewardsscardNumm = "//*[@id='userField1']";
		public String brewardsscardNumm = "//*[@id='reward-no']//*[@id='userField1']";
		public String emaill = "//*[@id='email']";
		public String checkoutAddressButtonss = "//*[@id='checkoutAddressButtons']";
		public String checkoutSameAsBillingAddresss = "//*[@id='lable-SameShippingAndBillingAddress']";
		public String checkoutBFirstNameAlertt = "//*[@id='ErrorMessage_fname']";
		public String checkoutSFirstNameAlertt = "//*[@id='ErrorMessage_fname2']";
		public String checkoutBLastNameAlertt = "//*[@id='ErrorMessage_lname']";
		public String checkoutSLastNameAlertt = "//*[@id='ErrorMessage_lname2']";
		public String checkoutBStaddressAlertt = "//*[@id='ErrorMessage_address']";
		public String checkoutSStaddressAlertt = "//*[@id='ErrorMessage_address4']";
		public String checkoutBCityAlertt = "//*[@id='ErrorMessage_city']";
		public String checkoutSCityAlertt = "//*[@id='ErrorMessage_city2']";
		public String checkoutBUKStateAlertt = "//*[@id='ErrorMessage_WC__ShoppingCartAddressEntryForm_billing_address_form_state_1']";
		/*public String checkoutBPOAlertt = "//*[@id='ErrorMessage_po']";
		public String checkoutScaPOAlertt = "//*[@id='ErrorMessage_po3']";*/
		public String checkoutBcaPHAlertt = "//*[@id='ErrorMessage_bphone']";
		public String checkoutScaPHAlertt = "//*[@id='ErrorMessage_sphone']";
		
		public String registerEmailAlertt = "//*[@id='ErrorMessage_email1']";
		public String registerVerifyEmailAlertt = "//*[@id='ErrorMessage_verifyemail']";
		public String registerPwdAlertt = "//*[@id='ErrorMessage_Password']";
		public String registerVerifyPwdAlertt = "//*[@id='ErrorMessage_verifypassword']";
		public String registercheckIconn = "//*[@for='wlike'][contains(@class,'checked')]//*[@class='sk_checkIcon']";
		public String registerPostalAlertt = "//*[@id='ErrorMessage_po1']";

		
		

		public String checkoutBcaEmailAlertt = "//*[@id='ErrorMessage_email']";
		public String termsandCondd = "//*[@class='ifreceive']//a";
		public String termsandConddChkBoxx = "//*[@class='ifreceive']//*[@for='ifreceive']";
		public String emailDisclaimerTxtt = "//*[@class='checkout-address-email-message']";
		
		public String checkoutshipAddressFormm = "//*[@id='shipping-address-form']";
		
		public String sfirstNamee = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@id='fname2']";
		public String slastNamee = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@id='lname2']";
		public String sstaddresss = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@id='address4']";
		public String sstaddresss2 = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@id='address5']";
		public String scityy = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@id='city2']";
		public String scountrySelectionn = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@id='country2']";
		public String sStatee = "//*[@class='shipping-address']//*[@id='forstates'][contains(@style,'block')]//*[@name='state']";
		public String spostalCodee = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@id='po3']";
		public String sContactNoo = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@class='shippingTelephone']";
		public String sextCodee = "//*[@class='shipping-address']//*[@id='shipping-address-form'][contains(@style,'block')]//*[@id='sphone4']";
		
		//public String shipandPaypagee = "//*[@id='QAS_AcceptOriginal']";
		
		public String shipandPaypageBillSecc = "//*[@id='editBillingAddressLink_1']";
		
		
		//public String shipandPaypageShipEditAddressOverlayy = "//*[@class='dijitDialogUnderlayWrapper']";
		//public String shipandPaypageShipShipEditAddresss = "//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]";
		
		public String checkoutBcaRWDAlertt = "//*[@id='ErrorMessage_userField1']";		
		public String shipandPaypageShipShipCreateEditAddresssPayUsStateLabell = "//*[@id='forstates'][contains(@style,'block')]//*[@id='forUS']";
		public String shipandPaypageShipShipCreateEditAddresssPayNonUsStateLabell = "//*[@id='forstates'][contains(@style,'block')]//*[@id='nonUS']";
		public String shipandPaypageShipShipCreateEditTermsandCondChkBoxx = "//*[@class='ifreceive']//*[@id='fax2']";
		public String buspostalCodee = "//*[@id='forUS1'][contains(@style,'block')]//*[@id='po']";
		public String paymentOptionsApplyGiftCardd = "//*[@class='giftCardCheckImg']";
		public String paymentOptionsApplyGiftCardLabell = "//*[@id='label-PaybyGiftCard']";
		public String reviewthankyouu = "//*[@id='thanks-for-order']//*[@class='thankyou']";
		
		public String reviewTabHBCTextt = "//*[@class='checkout-summary']//*[@class='hbc-points']";
		public String reviewshippingamounttxtt = "(//*[@class='order-cost-summary']//dt)[3]";
		public String reviewshippingamountt = "(//*[@class='order-cost-summary']//dd)[3]";
		public String reviewpsttxtt = "(//*[@class='order-cost-summary']//dt)[4]";
		public String reviewpstamountt = "(//*[@class='order-cost-summary']//dd)[4]";
		public String reviewgsttxtt = "(//*[@class='order-cost-summary']//dt)[5]";
		public String reviewgstamountt = "(//*[@class='order-cost-summary']//dd)[5]";
		public String revieworderDetailsTextt = "//*[@id='thanks-for-order']//strong";
		public String reviewBillinSummarySecc = "//*[@class='billing_summary']";
		public String reviewOrderCreateAccountt = "//*[@class='action-button wdtAuto']//a";
		public String reviewOrderCreateAccountPagee = "//*[@id='register-account']";


}
