package hbConfig;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HBConstants 
{
	 public static String filename = new SimpleDateFormat("dd-MM-yyyy").format(new Date())+".html";
	 
	 public static String currentDir = System.getProperty("user.dir");
	 
	
		public static String searchKey = "searchKey";
		public static String pdpUrl = "pdpUrl";
		public static String key = "key";
		
		//PLP Page Stream comparison offset is 100 & PDP Page Stream comparison offset is 0	


		/*public static String mobileSiteURL = "http://hbcstage.skavaone.com/
		public static String plpURL = "http://hbcstage.skavaone.com/skavastream/core/v5/thebay/search?campaignId=1&domain=www.thebay.com&storeid=10701&catalogid=10652&language=-24&offset=100&search=searchKey";
		public static String plpStream = "http://hbcstage.skavaone.com/skavastream/core/v5/thebay/search?campaignId=1&domain=www.thebay.com&storeid=10701&catalogid=10652&language=-24&offset=0&search=searchKey";
		public static String pdpStream = "http://hbcstage.skavaone.com/skavastream/core/v5/thebay/product?campaignId=1&url=pdpUrl";
		public static String suggestion = "http://hbcstage.skavaone.com/skavastream/core/v5/thebay/searchsuggestion?campaignId=1&search=key&name=MC_10601_CatalogEntry_en_CA&url=http%3A%2F%2Fwcssearch.hbc.com%3A3737%2Fsolr%2FMC_10601_CatalogEntry_en_CA&storeid=10701&include=true&productid=&type=ajax&domain=www.thebay.com";
		public static String repFileLoc = "E:\\HBC\\HBCComplete\\Reports\\H&B Automation Test Report Stage "+filename;	*/
		
		/*public static String mobileSiteURL = "http://mpreprod.thebay.com/";
		public static String plpURL = "http://mpreprod.thebay.com/skavastream/core/v5/thebay/search?campaignId=1&domain=www.thebay.com&storeid=10701&catalogid=10652&language=-24&offset=100&sort=&selectedFacets=&search=searchKey&locale=en_us";
		public static String plpStream = "http://mpreprod.thebay.com/skavastream/core/v5/thebay/search?campaignId=1&domain=www.thebay.com&storeid=10701&catalogid=10652&language=-24&offset=0&sort=&selectedFacets=&search=searchKey&locale=en_us";
		public static String pdpStream = "http://mpreprod.thebay.com/skavastream/core/v5/thebay/product?campaignId=1&url=pdpUrl";
		public static String suggestion = "http://mpreprod.thebay.com/skavastream/core/v5/thebay/searchsuggestion?campaignId=1&search=key&name=MC_10601_CatalogEntry_en_CA&url=http%3A%2F%2Fwcssearch.hbc.com%3A3737%2Fsolr%2FMC_10601_CatalogEntry_en_CA&storeid=10701&include=true&productid=&type=ajax&domain=www.thebay.com";
		public static String repFileLoc = "E:\\HBC\\HBCComplete\\Reports\\H&B Automation Test Report Preprod "+filename;*/
				
		 public static String mobileSiteURL = "http://m.thebay.com/";
		 public static String plpURL = "http://m.thebay.com/skavastream/core/v5/thebay/search?campaignId=1&domain=www.thebay.com&storeid=10701&catalogid=10652&language=-24&offset=100&sort=&selectedFacets=&search=searchKey&locale=en_us";
		 public static String plpStream = "http://m.thebay.com/skavastream/core/v5/thebay/search?campaignId=1&domain=www.thebay.com&storeid=10701&catalogid=10652&language=-24&offset=0&sort=&selectedFacets=&search=searchKey&locale=en_us";
		 public static String pdpStream = "http://m.thebay.com/skavastream/core/v5/thebay/product?campaignId=1&url=pdpUrl";
		 public static String suggestion = "http://m.thebay.com/skavastream/core/v5/thebay/searchsuggestion?campaignId=1&search=key&name=MC_10601_CatalogEntry_en_CA&url=http%3A%2F%2Fwcssearch.hbc.com%3A3737%2Fsolr%2FMC_10601_CatalogEntry_en_CA&storeid=10701&include=true&productid=&type=ajax&domain=www.thebay.com";
		 public static String repFileLoc = "E:\\HBC\\HBCComplete\\Reports\\H&B Automation Test Report Prod "+filename;
			 

		 /*public static String mobileSiteURL = "http://mperf.thebay.com/";
		 public static String mobileSiteURL = "http://mstage.thebay.com/";*/
	 
	 
		public static String commonExcelFile = currentDir+"\\ExcelFiles\\commonExcelData.xls";
		//public static String commonExcelFile = currentDir+"\\ExcelFiles\\commonExcelData_fr.xls";		
		public static String extentRepConfigfile = currentDir+"\\ExtentReportConfig.xml";
		public static String chromeexePath = currentDir+"\\lib\\chromedriver.exe";

}

