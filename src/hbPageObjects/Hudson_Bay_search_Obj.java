package hbPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbConfig.HBConstants;
import hbPages.Hudson_Bay_Search;

public class Hudson_Bay_search_Obj extends Hudson_Bay_Search implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_search_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}	
			
			
	
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;	
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
			
	@FindBy(xpath = ""+pancakeMenuu+"")
	WebElement pancakeMenu;
		
	@FindBy(xpath = ""+maskk+"")
	WebElement pancakeMask;
	
	@FindBy(xpath = ""+searchRecentHistoryy+"")
	WebElement searchRecentHistory;	
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+searchXClosee+"")
	WebElement searchXClose;
	
	@FindBy(xpath = ""+searchRecentHistoryClearAlll+"")
	WebElement searchRecentHistoryClearAll;	
	
	@FindBy(xpath = ""+searchBoxConfirmm+"")
	WebElement searchBoxConfirm;
	
	@FindBy(xpath = ""+searchSuggestionn+"")
	WebElement searchSuggestion;
	
	@FindBy(xpath = ""+searchSuggestItemm+"")
	WebElement searchSuggestItem;	
		
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+plpHeaderTitlee+"")
	WebElement plpHeaderTitle;
	
	@FindBy(xpath = ""+plpSubHeaderr+"")
	WebElement plpSubHeader;
	
	@FindBy(xpath = ""+plpItemContt+"")
	WebElement plpItemCont;
	
	@FindBy(xpath = ""+plpProductss+"")
	WebElement plpProducts;
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;
	
	@FindBy(xpath = ""+Filterr+"")
	WebElement Filter;
	
	@FindBy(xpath = ""+sortt+"")
	WebElement sort;
	
	@FindBy(xpath = ""+sortByy+"")
	WebElement sortBy;
	
	@FindBy(xpath = ""+Refinewindoww+"")
	WebElement Refinewindow;	
	
	@FindBy(xpath = ""+RefinewindowClosee+"")
	WebElement RefinewindowClose;
	
	@FindBy(xpath = ""+RefinewindowClearAlll+"")
	WebElement RefinewindowClearAll;		

	@FindBy(xpath = ""+RefinewindowApplyy+"")
	WebElement RefinewindowApply;	
	
	@FindBy(xpath = ""+loadingbarr+"")
	WebElement loadingbar;
	
	@FindBy(xpath = ""+FilterCountt+"")
	WebElement FilterCount;
	
	@FindBy(xpath = ""+back_To_topp+"")
	WebElement back_To_top;
	
	@FindBy(xpath = ""+Swatchess+"")
	WebElement Swatches;
		
	@FindBy(xpath = ""+MoreSwatchess+"")
	WebElement MoreSwatches;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+sortOptionSelectedd+"")
	WebElement sortOptionSelected;
	
	@FindBy(xpath = ""+sortOptionSelectetickk+"")
	WebElement sortOptionSelectetick;	
	
	@FindBy(xpath = ""+sortOptionBoldd+"")
	WebElement sortOptionBold;	
	
	@FindBy(xpath = ""+sortByArroww+"")
	WebElement sortByArrow;	
		
	@FindBy(xpath = ""+sortByOpenDomm+"")
	WebElement sortByOpenDom;	
	
	@FindBy(xpath = ""+sortByOpenn+"")
	WebElement sortByOpen;
	
	@FindBy(xpath = ""+PDPProdNamee+"")
	WebElement PDPProdName;	
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+loadingBarrr+"")
	WebElement loadingBarr;	
	
	
		
	@FindBy(how = How.XPATH,using = ""+recentSearchListt+"")
	List<WebElement> recentSearchList;
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	@FindBy(how = How.XPATH,using = ""+FilterAttributess+"")
	List<WebElement> FilterAttributes;
	
	@FindBy(how = How.XPATH,using = ""+sortOptionss+"")
	List<WebElement> sortOptionsList;
	
	@FindBy(how = How.XPATH,using = ""+sortOptionSelectedd+"")
	List<WebElement> sortOptionSelectedList;
	
	@FindBy(how = How.XPATH,using = ""+sortOptionNOTSelectedd+"")
	List<WebElement> sortOptionNOTSelected;
	
	@FindBy(how = How.XPATH,using = ""+sortOptionNOTSelecteddd+"")
	List<WebElement> sortOptionNOTSelectedddd;	
	
	
	String keyword = null;
	
	String refCount = "";
	
	String plpCount = "";
	
	int plp_Pagesize = 0;


	/*HUDSONBAY-387/388 Verify that Search icon & search text box Should be displayed as per the creative and fit to the screen*/
	public void searchCreative()
		{
			ChildCreation("HUDSONBAY-387 Verify that Search icon & search text box Should be displayed as per the creative and fit to the screen");
			boolean flag = false;
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					log.add("Search Icon displayed");
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						Pass("Search icon & search text box displayed as per the creative and fit to the screen",log);
						flag = true;
					}
					else
					{
						Fail("Search icon & search text box not displayed as per the creative");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}			
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-388 Verify that while selecting the search icon at the header,the search field/text box should be enabled");
			if(flag==true)
			{
				Pass("Selecting the search icon at the header,the search field/text box enabled");
			}
			else
			{
				Fail("Selecting the search icon at the header,the search field/text box not enabled");
			}			
		}
	
	/*HUDSONBAY-394 Verify that while enter into search field,the 'X' cancel option should be shown as default*/
	public void searchXEnabled()
		{
			ChildCreation("HUDSONBAY-394 Verify that while enter into search field,the 'X' cancel option should be shown as default");
			boolean flag = false;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchXClose));			
				if(HBCBasicfeature.isElementPresent(searchXClose))
				{
					Pass("Entering the keywords in the search field, the 'X' cancel option shown as default");
					flag = true;
				}
				else
				{
					Fail("Entering the keywords in the search field, the 'X' cancel option shown as default");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-409 Verify that 'X' close icon should be shown as default, when no keyword is present inside the text field.");
			if(flag==true)
			{
				Pass("'X' close icon shown as default, when no keyword is present inside the text field");
			}
			else
			{
				Fail("'X' close icon not shown as default, when no keyword is present inside the text field");
			}
		}
	
	/*HUDSONBAY-395 Verify that while selecting the X button in the search text box, the search box should be closed.*/
	public void searchCancel()
		{
			ChildCreation("HUDSONBAY-395 Verify that while selecting the X button in the search text box, the search box should be closed.");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchXClose));			
				if(HBCBasicfeature.isElementPresent(searchXClose))
				{
					action.moveToElement(searchXClose).click().build().perform();
					if(!HBCBasicfeature.isElementPresent(searchBoxConfirm))
					{
						Pass("Selecting the X button in the search text box, the search box is closed");
					}
					else
					{
						Fail("Selecting the X button in the search text box, the search box is not closed");
					}	
				}	
				else
				{
					Fail("Search cancel button is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-885 Verify that while selecting the search icon at the header,after selecting the pancake, the search field/text box should be enabled*/
	public void searchEnableAfterPancakeClick()
		{
			ChildCreation("HUDSONBAY-885 Verify that while selecting the search icon at the header,after selecting the pancake, the search field/text box should be enabled");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					if(HBCBasicfeature.isElementPresent(searchBoxConfirm))
					{
						log.add("Search Box displayed");
						wait.until(ExpectedConditions.visibilityOf(hamburger));			
						if(HBCBasicfeature.isElementPresent(hamburger))
						{
							boolean pgload = false;
							do
							{
								try
								{
									((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);	
									if(HBCBasicfeature.isElementPresent(pancakeMenu))
									{
										log.add("Pancake Menu displayed");
										pgload = true;
										break;
									}									
								}
								catch(Exception e)
								{
									continue;
								}								
							}
							while(pgload!=true);						
							if(HBCBasicfeature.isElementPresent(pancakeMenu))
							{
								((JavascriptExecutor) driver).executeScript("arguments[0].click();", pancakeMask);			
								Thread.sleep(500);
								log.add("Pancake menu closed");
								if(HBCBasicfeature.isElementPresent(searchIcon))
								{
									Pass("While selecting the search icon at the header,after selecting the pancake, the search field/text box enabled",log);
								}
								else
								{
									Fail("While selecting the search icon at the header,after selecting the pancake, the search field/text box not enabled",log);
								}
							}
							else
							{
								Fail("Pancake Menu not displayed");
							}
						}
						else
						{
							Fail("Hamburger menu not displayed");
						}
					}
					else
					{
						Fail("SearchBox not displayed");
					}	
				}	
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	
	/*HUDSONBAY-393/396/397 Verify that user able to enter keywords in the search field/textbox */
	public void searchKeyword() throws Exception
		{
			ChildCreation("HUDSONBAY-393 Verify that user able to enter keywords in the search field/textbox.");
			String key = HBCBasicfeature.getExcelVal("HB393", sheet, 1);
			String[]split = key.split("\n");
			Random r = new Random();
			int size = split.length;
			int streamLength = 0, siteLength = 0;
			boolean flag = false;
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					HBCBasicfeature.jsclick(searchIcon, driver);
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						int i = r.nextInt(size);
						keyword = split[i];
						//	keyword = "handbag";
						Thread.sleep(500);
						action.moveToElement(searchBox).sendKeys(keyword).build().perform();
						log.add("The actual keyword is "+keyword);
						Thread.sleep(200);
						String expected = searchBox.getAttribute("value");	
						log.add("The expected keyword is "+expected);
						if(expected.equals(keyword))
						{
							Pass("User able to enter keywords in the search field/textbox",log);
							flag = true;
							Thread.sleep(500);
						}
						else
						{
							Fail("User not able to enter keywords in the search field/textbox",log);
						}
					}
					else
					{
						Fail("Search Box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}		
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-396 Verify that while entering the valid search keyword in the search field,corresponding search suggestion should be displayed");
			if(flag==true)
			{
				flag = false;
/*				HttpURLConnection conn = null;
*/				ArrayList<String> siteResp = new ArrayList<>();
				ArrayList<String> streamResp = new ArrayList<>();
				ArrayList<Integer> result = new ArrayList<Integer>();
				try
				{
					//Getting Site response	
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(searchSuggestion));			
					if(HBCBasicfeature.isElementPresent(searchSuggestion))
					{
						Thread.sleep(500);
						wait.until(ExpectedConditions.visibilityOf(searchSuggestItem));			
						if(HBCBasicfeature.isElementPresent(searchSuggestItem))
						{
							List<WebElement> list = driver.findElements(By.xpath("//*[@class='searchSuggestions']//*[@class='suggestedItems']"));
							Thread.sleep(1000);
							int ssize = list.size();
							siteLength = ssize;
							for(int i=0;i<ssize;i++)
							{
								String suggest = driver.findElement(By.xpath("(//*[@class='searchSuggestions']//*[@class='suggestedItems'])["+(i+1)+"]")).getText();
								siteResp.add(suggest);								
							}	
							
							//Getting Stream response				
							String SearchStream = HBConstants.suggestion.replace("key", keyword);
							/*URL curl = new URL(SearchStream);
							conn  = (HttpURLConnection) curl.openConnection();
							if(conn.getResponseCode()==200)
							{
								String classicResponse = IOUtils.toString(new URL(SearchStream));*/
								
								driver.navigate().to(SearchStream);
								String classicResponse = driver.findElement(By.xpath("/html/body/pre")).getText();
								Thread.sleep(1000);
								driver.navigate().back();
								JSONObject jobj = new JSONObject(classicResponse);
								JSONObject classicResultsObj = jobj.getJSONObject("properties");
								JSONArray classicResultArray = classicResultsObj.getJSONArray("suggestion");
								JSONObject resultObj = classicResultArray.getJSONObject(0);
								JSONArray resultArray = resultObj.getJSONArray("value");
								//	System.out.println(resultArray.length());
								streamLength = resultArray.length();
								for(int j=0; j<resultArray.length();j++)	
								{
									String prtname = resultArray.getString(j);	
									//	System.out.println(prtname);
									streamResp.add(prtname);
								}
								for (String temp : streamResp)
								{
									result.add(siteResp.contains(temp) ? 1 : 0);
								//	System.out.println(result);
									log.add("Compared result is: "+result);
								}
								
								if(!result.contains(0))
							  	{
							  		log.add("The search suggestions for the keyword (Response from classicSite): " + streamResp.toString());
							  		log.add("The search suggestions for the keyword (Response from MobileSite): " + siteResp.toString());
							  		Pass("Entering the valid search keyword in the search field,corresponding search suggestion displayed based on the stream response",log);
							  		flag = true;
							  	}
							  	else
							  	{
							  		log.add("The search suggestions for the keyword (Response from classicSite): " + streamResp.toString());
							  		log.add("The search suggestions for the keyword (Response from MobileSite): " + siteResp.toString());
							  		Fail("Entering the valid search keyword in the search field,corresponding search suggestion not displayed based on the stream response",log);
							  	}								
							}
							else
							{
								Fail("Response not getting from stream");
							}
						}
						else
						{
							Fail("Search suggestion items are not displayed");
						}
					/*}
					else
					{
						Fail("Search suggestion is not displayed");
					}			*/		
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}				
			}
			else
			{
				Skip("Not able to enter keywords in the search field");
			}
			
			ChildCreation("HUDSONBAY-397 Verify that while entering the valid search keyword in the search field, count of search suggestion as per the API should be displayed");
			if(flag==true)
			{
				log.add("The Search suggestion count is: "+siteLength);
				log.add("Stream response count is: "+streamLength);
				if(streamLength==siteLength)
				{
					Pass("Entering the valid search keyword in the search field, count of search suggestion displayed as per the API",log);
				}
				else
				{
					Fail("Entering the valid search keyword in the search field, count of search suggestion not displayed as per the API",log);
				}
			}
			else
			{
				Skip("Not able to enter keywords in the search field");
			}			
		}
		
	/*HUDSONBAY-401 Verify that while clearing the search keyword using the backspace button in the virtual keypad,the blank drop-down should not get displayed*/
	public void searchClear()
		{
			ChildCreation("HUDSONBAY-401 Verify that while clearing the search keyword using the backspace button in the virtual keypad,the blank drop-down should not get displayed");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));	
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					HBCBasicfeature.jsclick(searchIcon, driver);
					action.moveToElement(searchBox).sendKeys(keyword).build().perform();
					Thread.sleep(200);
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						String var = searchBox.getAttribute("value");
						if(!var.isEmpty())
						{
							log.add("The search keyword "+var);
							int len = var.length();
							do
							{			
								searchBox.sendKeys(Keys.BACK_SPACE);
								len--;					
							}
							while(len>=0);
							if(searchBox.getAttribute("value").isEmpty())
							 {
								log.add("The entered search keyword is cleared");
								Thread.sleep(1000);
								if(!HBCBasicfeature.isElementPresent(searchSuggestion))
								{
									Pass("While clearing the search keyword ,the blank drop-down not get displayed",log);
									searchXClose.click();
								}
								else
								{
									Fail("While clearing the search keyword ,the blank drop-down get displayed",log);
									searchXClose.click();
								}
							 }
							else
							{
								Fail("Search box is not empty");
							}
						}
						else
						{
							Fail("SearchBox is empty");
						}			
					}
					else
					{
						Fail("search box is not displayed");
					}
				}
				else
				{
					Fail("search Icon is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-405 Verify that default inline text("Search/Rechercher") should be shown in the search field.*/
	public void searchInlineText() throws Exception
		{			
			ChildCreation("HUDSONBAY-405 Verify that default inline text('Search/Rechercher') should be shown in the search field.");
			String text = HBCBasicfeature.getExcelVal("HB405", sheet, 1);
			boolean flag = false;
			try
			{				
				wait.until(ExpectedConditions.visibilityOf(searchBox));			
				if(HBCBasicfeature.isElementPresent(searchBox))
				{
					String inline = searchBox.getAttribute("placeholder");
					log.add("Default Inline text is: "+inline);
					if(inline.equalsIgnoreCase(text))
					{
						Pass("Default inline text('Search/Rechercher') shown below the search field.",log);
						flag = true;
					}
					else
					{
						Fail("Default inline text('Search/Rechercher') not shown below the search field.",log);
					}
				}
				else
				{
					Fail("SearchBox not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-406 Verify that default inline text('Please Enter to search') should be shown below the search field on after enterting the keyword.");			
			if(flag==true)
			{
				Pass("Default inline text shown below the search field on after enterting the keyword.");
			}
			else
			{
				Fail("Default inline text not shown below the search field on after enterting the keyword");
			}
		}
	
	/*HUDSONBAY-399/415/435 Verify that while selecting the search keyword in the search suggestions dropdown,the search results page should be displayed*/
	public void searchSuggestionSelection() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-399 Verify that while selecting the search keyword in the search suggestions dropdown,the search results page should be displayed");
			boolean flag = false;
			String selectedSuggestion = ""; 
			String plpTitle = "";
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.clear();
						searchBox.sendKeys(keyword);
						Thread.sleep(1500);
						wait.until(ExpectedConditions.visibilityOf(searchSuggestion));			
						if(HBCBasicfeature.isElementPresent(searchSuggestion))
						{
							wait.until(ExpectedConditions.visibilityOf(searchSuggestItem));			
							if(HBCBasicfeature.isElementPresent(searchSuggestItem))
							{
								List<WebElement> list = driver.findElements(By.xpath("//*[@class='searchSuggestions']//*[@class='suggestedItems']"));
								Thread.sleep(1000);
								int size = list.size();
								Random r = new Random();
								int i = r.nextInt(size);
								//i=0;
								WebElement suggest = driver.findElement(By.xpath("(//*[@class='searchSuggestions']//*[@class='suggestedItems'])["+(i+1)+"]"));
								selectedSuggestion = suggest.getText();
								log.add("The selected suggestion is: "+selectedSuggestion);
								suggest.click();
								wait.until(ExpectedConditions.visibilityOf(plpPage));
								if(HBCBasicfeature.isElementPresent(plpPage))
								{
									plpTitle = plpHeaderTitle.getText();
									log.add("The search result page title is: "+plpTitle);
									if(selectedSuggestion.equalsIgnoreCase(plpTitle))
									{
										Pass("while selecting the search keyword in the search suggestions dropdown,the search results page displayed",log);
										flag = true;
									}
									else
									{
										Fail("while selecting the search keyword in the search suggestions dropdown,the search results page not displayed",log);
									}
								}
								else
								{
									Fail("PLP page not displayed");
								}
							}
							else
							{
								Fail("Search suggestion items are not displayed");
							}
						}
						else
						{
							Fail("Search suggestion is not displayed");
						}								
					}
					else
					{
						Fail("Search box is not displayed");
					}						
				}
				else
				{
					Fail("Search icon is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-415 Verify that while selecting the search keyword in the search suggestions dropdown,the search results page should be displayed");
			if(flag == true)
			{
				Pass("while selecting the search keyword in the search suggestions dropdown,the search results page displayed");
			}
			else
			{
				Fail("while selecting the search keyword in the search suggestions dropdown,the search results page not displayed");
			}
			
			ChildCreation("HUDSONBAY-435 Verify that while selecting the search keyword in the search suggestion dropdown,the particular category the search results page should be displayed with all the available products");
			if(flag == true)
			{
				if(selectedSuggestion.equalsIgnoreCase(plpTitle))
				{
					Pass("while selecting the search keyword in the search suggestion dropdown,the particular category the search results page displayed with all the available products",log);
				}
				else
				{
					Fail("while selecting the search keyword in the search suggestion dropdown,the particular category the search results page not displayed with all the available products",log);
				}
			}
			else
			{
				Skip("While selecting suggestion PLP page not displayed");
			}
			
			
		}
	
	/*HUDSONBAY-412 Verify that searched string should be maintained in search box when returning from search results page.*/
	public void searchStringMaintain()
		{
			ChildCreation("HUDSONBAY-412 Verify that searched string should not be maintained in search box when returning from search results page.");
			Actions action = new Actions(driver);
			try
			{
				String searchedKeyword  = keyword;
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{		
						String Keyword = searchBox.getText();
						if(Keyword.isEmpty())
						{
							Pass("Searched string not maintained in search box when returning from search results page");
							searchXClose.click();
						}
						else
						{
							if(searchedKeyword.equalsIgnoreCase(Keyword))
							{
								Fail("Searched string maintained in search box when returning from search results page");
								searchXClose.click();
							}
						}
					}
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-404 Verify that while entering into the search box,previous visited search keywords should be displayed in search history.*/
	public void searchKeywordHistory() throws Exception
		{
			ChildCreation("HUDSONBAY-404 Verify that while entering into the search box,previous visited search keywords should be displayed in search history.");
			String key = HBCBasicfeature.getExcelVal("HB404", sheet, 1);
			String split[] = key.split("\n");
			ArrayList<String> searchedKeywords =new ArrayList<String>();
			try
			{
				Actions action = new Actions(driver);
				int size = split.length;
				for(int i=0;i<size;i++)
				{					
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.clear();						
						searchBox.sendKeys(split[i]);
						searchBox.sendKeys(Keys.ENTER);
						searchedKeywords.add(split[i]);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(plpPage));			
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							continue;
						}											
					}
				}
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchRecentHistory));			
					if(HBCBasicfeature.isElementPresent(searchRecentHistory))
					{
						int lsize = recentSearchList.size();
						boolean flag = false;
						for(int i=1;i<=lsize;i++)
						{
							String suggest = driver.findElement(By.xpath("(//*[@class='recentSuggestion']//*[@class='suggestedItems'])["+i+"]")).getText();
							log.add("Search History keyword is: "+suggest);
							if(searchedKeywords.contains(suggest))
							{
								log.add("Searched keyword present in the search history");
								flag = true;
								continue;
							}
							else
							{
								Fail("Entering into the search box,previous visited search keywords not displayed in search history.",log);
								searchXClose.click();
							}
						}
						if(flag==true)
						{
							Pass("Entering into the search box,previous visited search keywords not displayed in search history",log);
							searchXClose.click();
						}
					}
					else
					{
						Fail("Recent search history container is not displayed");
					}
				}
				else
				{
					Fail("search Icon is not displayed");
				}
				
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-407 Verify that while entering into the search box,last two previous visited search keywords should be displayed in search history.*/
	public void searchKeywordHistoryCount() throws Exception
		{
			ChildCreation("HUDSONBAY-407 Verify that while entering into the search box,last two previous visited search keywords should be displayed in search history.");
			String key = HBCBasicfeature.getExcelVal("HB404", sheet, 1);
			String split[] = key.split("\n");
			int size = split.length;
			ArrayList<String> searchHistory =new ArrayList<String>();
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchRecentHistory));			
					if(HBCBasicfeature.isElementPresent(searchRecentHistory))
					{
						int size_1 = recentSearchList.size();
						log.add("Previously visited keywords: "+size_1);
						boolean flag = false;
						for(int j=1;j<=size_1;j++)
						{
							String suggestion = driver.findElement(By.xpath("(//*[@class='recentSuggestion']//*[@class='suggestedItems'])["+j+"]")).getText();
							searchHistory.add(suggestion);
						}
						log.add("The last two previous visited search keywords are: "+searchHistory);
						for(int k=0;k<size;k++)
						{
							String searched = split[k];
							if(searchHistory.contains(searched))
							{								
								log.add("Previously visited keyword "+ searched +" displayed");
								flag = true;						
							}
							else
							{
								continue;
							}
						}
						if(flag==true)
						{
							Pass("last two previous visited search keywords displayed in search history",log);
							searchRecentHistoryClearAll.click();
							searchXClose.click();
						}
						else
						{
							Fail("last two previous visited search keywords displayed in search history",log);
							searchRecentHistoryClearAll.click();
							searchXClose.click();
						}
					}
					else
					{
						Fail("Recent search history container is not displayed");
					}
				}	
				else
				{
					Fail("Search Icon is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-410 Verify that on entering minimum of 4 characters only , search suggestion should be shown.*/
	public void searchMinimumChar() throws Exception
		{
			ChildCreation("HUDSONBAY-410 Verify that on entering minimum of 4 characters only , search suggestion should be shown.");
			String Key = HBCBasicfeature.getExcelVal("HB410", sheet, 1);
			try
			{
				WebDriverWait w1 = new  WebDriverWait(driver,10);
				String chr = "null";
				int ctr = 0;
				String[] split  = Key.split("\n");
				int size = split.length;
				boolean flag = false;
				for(int i=0;i<size;i++)
				{	
					ctr++;
					wait.until(ExpectedConditions.visibilityOf(searchIcon));			
					if(HBCBasicfeature.isElementPresent(searchIcon))
					{	
						searchIcon.click();
						wait.until(ExpectedConditions.visibilityOf(searchBox));			
						if(HBCBasicfeature.isElementPresent(searchBox))
						{											
							searchBox.clear();
							chr = split[i];
							searchBox.sendKeys(split[i]);
							try
							{
								w1.until(ExpectedConditions.visibilityOf(searchSuggestion));
								if(HBCBasicfeature.isElementPresent(searchSuggestion))
								{									
									log.add("Search suggestion displayed for the character: "+chr);
									searchBox.clear();
									searchXClose.click();
									flag =true;									
								}					
							}
							catch (Exception e)
							{							
								log.add("Search suggestion not displayed for the character: "+chr);
								searchBox.clear();
								searchXClose.click();
								continue;
							}									
						}
						else
						{
							Fail("Search Box not displayed");
						}
					}
					else
					{
						Fail("Search Icon not displayed");
					}
				}					
				if(flag==true)
				{
					log.add("Characters displayed for suggestion is: "+ctr);
					Pass("Minimum character for search suggestion as per the classic site",log);
				}
				else
				{
					Fail("Minimum character for search suggestion not as per the classic site",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}	
	
	/*HUDSONBAY-411 Verify that on entering the HTML tags in the input field, it should be treated as a string.*/
	public void searchHtmlTags() throws Exception
		{
			ChildCreation("HUDSONBAY-411 Verify that on entering the HTML tags in the input field, it should be treated as a string.");
			String Key = HBCBasicfeature.getExcelVal("HB411", sheet, 1);
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{							
						searchBox.clear();
						searchBox.sendKeys(Key);
						log.add("Searched HTML keyword is: "+Key);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(1000);
						try
						{							
							wait.until(ExpectedConditions.visibilityOf(plpPage));			
							if(HBCBasicfeature.isElementPresent(plpPage))
							{
								log.add("PLP Page displayed");
								Pass("on entering the HTML tags in the input field, it treated as a string",log);
							}
						}
						catch(Exception e)
						{
							log.add("PLP page not displayed");
							Fail("On Entering the HTML tags in the input field, it  not treated as a string",log);
							searchBox.clear();
							searchXClose.click();
						}
					}							
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-416 Verify that total items available count (XXX items) should be displayed below the category title in search results page*/
	public void searchPageItemCount()
		{
			ChildCreation("HUDSONBAY-416 Verify that total items available count (XXX items) should be displayed below the category title in search results page");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{							
						searchBox.clear();
						searchBox.sendKeys(keyword);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(500);
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							wait.until(ExpectedConditions.visibilityOf(plpItemCont));
							if(HBCBasicfeature.isElementPresent(plpItemCont))
							{
								String val = plpItemCont.getText();
								String[] split = val.split(" ");
								String count = split[0]; 
								String text = split[1];
								Thread.sleep(500);
								if(count.matches(".*\\d+.*"))
								{		
									Pass("Product count displayed is :"+count);							
								}
								else
								{
									Fail("Product count not displayed",log);
								}					
								if(text.equalsIgnoreCase("Items"))
								{
									Pass("Total items available count (XXX items) displayed below the category title in product list page"+plpItemCont.getText(),log);
								}
								else
								{
									Fail("Total items available count (XXX items) not displayed below the category title in product list page"+plpItemCont.getText(),log);
								}
							}
							else
							{
								Fail("PLP Item count not displayed");		
							}
						}
						else
						{
							Fail("PLP Page not displayed");		
						}
					}
					else
					{
						Fail("Search Box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-417 Verify that user should be able to scroll the product list page vertically*/
	public void SearchPageVerticalScroll()
		{
			ChildCreation("HUDSONBAY-417 Verify that user should be able to scroll the product list page vertically");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					HBCBasicfeature.scrolldown(footerContainer, driver);
					Pass("User able to scroll the product list page vertically");
				}
				else
				{
					Fail("PLP Page not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
		}
	
	/*HUDSONBAY-418 Verify that while scrolling the products in the product list page,the products should be displayed with the lazy loading */
	public void searchPageLazyLoad()
		{
			ChildCreation("HUDSONBAY-418 Verify that while scrolling the products in the product list page,the products should be displayed with the lazy loading ");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpProducts));
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					int bfre_size = plpItemContList.size();
					log.add("Before scrolling size is "+bfre_size);
					if(bfre_size>=100)
					{		
							HBCBasicfeature.scrolldown(footerContainer, driver);
							boolean loaded = false;
							do
							{	
								int size = plpItemContList.size();
								if(size>bfre_size)
								{
									loaded=true;
								}
								else
								{
									loaded=false;
								}
								
							}while(!loaded==true);
							
						Thread.sleep(1000);
						int aftr_size =  plpItemContList.size();						
						log.add("After lazy loading size is "+aftr_size);
						if(aftr_size>bfre_size)
						{
							Pass("while scrolling the products in the product list page,the products displayed with the lazy loading",log);
						}
						else
						{
							Fail("while scrolling the products in the product list page,the products not displayed with the lazy loading",log);
						}						
					}
					else
					{
						Skip("Product displayed is less than 100, so no lazy loading, product size is "+bfre_size);
					}				
				}
				else
				{
					Fail("Products are not displayed in the PLP page");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-419 Verify that "sort" and "filter" options should be displayed below the header */
	public void search_Sort_Filter()
		{
			ChildCreation("HUDSONBAY-419 Verify that 'sort' and 'filter' options should be displayed below the header ");
				try
				{						
					wait.until(ExpectedConditions.visibilityOf(sort));
					HBCBasicfeature.scrollup(sort, driver);
					if(HBCBasicfeature.isElementPresent(sort))
					{
						Pass("Sort option displayed below the header");
					}
					else
					{
						Fail("Sort option not displayed below the header");
					}
					wait.until(ExpectedConditions.visibilityOf(Filter));
					if(HBCBasicfeature.isElementPresent(Filter))
					{
						Pass("Filter option displayed below the header");
					}
					else
					{
						Fail("Filter option not displayed below the header");
					}				
				}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-420 Verify that separation line should be displayed between "sort" and "filter" options*/
	public void search_Sort_separationLine()
		{
			ChildCreation("HUDSONBAY-420 Verify that separation line should be displayed between 'sort' and 'filter' options");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					
					WebElement separation = driver.findElement(By.cssSelector(".sk_sort"));
					String separationLine = ((JavascriptExecutor)driver).executeScript("return window.getComputedStyle(arguments[0], '::after').getPropertyValue('border-right');",separation).toString().substring(12);
					//System.out.println(separationLine);
					String item = separationLine.replace("1px solid", "");
					Color colorhxcnvt = Color.fromString(item);
					String hexCode = colorhxcnvt.asHex();
					if(hexCode.contains("#cccccc"))
					{
						Pass("Separation line displayed between 'sort' and 'filter' options");
					}
					else
					{
						Fail("Separation line not displayed between 'sort' and 'filter' options");
					}					
				}
				else
				{
					Fail("Sort and Filter Container not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-421 Verify that selected "sort" and "filter" options count should be displayed as per the creative*/
	public void sort_Filter_Count()
		{
			ChildCreation("HUDSONBAY-421 Verify that selected 'sort' and 'filter' options count should be displayed as per the creative");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(FilterCount));
				if(HBCBasicfeature.isElementPresent(FilterCount))
				{
					int count = Integer.parseInt(FilterCount.getText());
					if(count>=0)
					{
						Pass("'sort' and 'filter' options count displayed as per the creative");
					}
					else
					{
						Fail("'sort' and 'filter' options count not displayed as per the creative");
					}
				}
				else
				{
					Fail("Filter count not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-422 Verify that listed product should contains product Image,title,color,availability, description,price and special offers details*/
	public void searchPage_Details()
		{
			ChildCreation("HUDSONBAY-422 Verify that listed product should contains product Image,title,color,availability, description,price and special offers details");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				HBCBasicfeature.scrollup(plpHeaderTitle, driver);
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					System.out.println("Please wait for while...checking all the products for Image, title, color, etc..");				
					int size = plpItemContList.size()/2;
					plp_Pagesize = size;
					for(int i=1;i<=size;i++)
					{							
						WebElement image = 	driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"]//*[@class='skMob_productImgDiv']"));
						wait.until(ExpectedConditions.visibilityOf(image));
						if(HBCBasicfeature.isElementPresent(image))
						{		
							continue;						
						}
						else
						{
							Fail("Product Image not displayed for each product");	
						}
					}
					Pass("Product Image displayed for each product");
					
					for(int i=1;i<=size;i++)
					{							
						WebElement product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(product_BrandTitle));
						if(HBCBasicfeature.isElementPresent(product_BrandTitle))
						{
							continue;
						}
						else
						{
							Fail("Product Brand Title not displayed for each product");	
						}
					}
					Pass("Product Brand Title displayed for each product");
					
					for(int i=1;i<=size;i++)
					{							
						WebElement product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(product_Title));
						if(HBCBasicfeature.isElementPresent(product_Title))
						{
							continue;
						}
						else
						{
							Fail("Product Title not displayed for each product");	
						}
					}
					Pass("Product Title displayed for each product");				
					
					for(int i=1;i<=size;i++)
					{							
						WebElement product_Color = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_color_items'])["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(product_Color));
						if(HBCBasicfeature.isElementPresent(product_Color))
						{
							continue;
						}
						else
						{
							Fail("Product Color not displayed for each product");	
						}
					}
					Pass("Product Color displayed for each product");										
				}
				else
				{
					Fail("Plp Page not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-423 Verify that product ratings should be displayed for each product, with black shaded as per the classic site*/
	public void product_rating()
		{
			ChildCreation("HUDSONBAY-423 Verify that product ratings should be displayed for each product, with black shaded as per the classic site");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					int size = plpItemContList.size()/2;
					int ctr = 0;
					boolean flag = false;
					for(int i=1;i<=size;i++)
					{
						try
						{
							WebElement ratingContainer = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='skMob_productDetails']//*[@class='skMob_rating_reviews ']"));
							if(HBCBasicfeature.isElementPresent(ratingContainer))
							{
								WebElement ProductRating = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='skMob_productDetails']//*[@id='skMob_fullRating']"));
								if(ProductRating.isDisplayed())
								{
									flag = true;
									ctr++;
								}
							}
							else
							{
								Fail("The Product Rating container is not displayed.");
								continue;
							}
						}
						catch (Exception e)
						{
							continue;
						}						
					}
					if(flag==true)
					{
						Pass("Product ratings displayed for products "+ctr);
					}
					else
					{
						Fail("Product ratings not displayed for any of the products");
					}
				}
				else
				{
					Fail("PLP page is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}	

	/*HUDSONBAY-424 Verify that when there is no rating ,empty rating star should not be shown as per the classic site*/
	public void product_noRating()
	{
		ChildCreation("HUDSONBAY-424 Verify that when there is no rating ,empty rating star should not be shown as per the classic site ");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(plpPage));
			if(HBCBasicfeature.isListElementPresent(plpItemContList))
			{
				int ctr = 0;
				int size = plp_Pagesize;
				System.out.println("Please wait for while...checking all the products when there is no rating ,empty rating star should not be shown..");
				for(int i=1;i<=size;i++)
				{
					try
					{
						WebElement ratingContainer = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='skMob_productDetails']//*[@class='skMob_rating_reviews ']"));
						if(HBCBasicfeature.isElementPresent(ratingContainer))
						{
							continue;
						}
						else
						{
							Fail("The Product Rating container is not displayed.");
							continue;
						}
					}
					catch (Exception e)
					{
						try
						{
							String ProductRating = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='skMob_productDetails']//*[@id='skMob_fullRating']")).getAttribute("title");
							Fail("Product Rating star is displayed"+ProductRating);
						}
						catch (Exception e1)
						{								
							ctr++;													
						}							
					}						
				}
				if(ctr>=1)
				{
					log.add("The rating star is not shown for " +ctr+ " products");
					Pass("Product has no rating ,empty rating star not shown as per the classic site",log);
				}
				else
				{
					Skip("Displayed products has Rating container");
				}
			}
			else
			{
				Fail("PLP page is not displayed");
			}					
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-427  Verify that color swatches should be displayed below the ratings star in the product list page*/
	public void searchPage_Swatches()
		{
			ChildCreation("HUDSONBAY-427 Verify that color swatches should be displayed below the ratings star in the product list page");
			try
			{
				Random r  = new Random();
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					int size = plpItemContList.size();
					int i = r.nextInt(size);
					if(i==0)
					{
						i=i+1;
					}
					boolean swatch  = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='sk_prdswatch']")).isDisplayed();
					if(swatch==true)
					{
						Pass("color swatches displayed below the ratings star in the product list page");
					}
					else
					{
						Fail("color swatches not displayed below the ratings star in the product list page");
					}
				}
				else
				{
					Fail("PLP page not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-428 Verify that "+" icon should be shown near the color swatches when more than 4 color swatches is available for the product*/
	public void searchPage_SwatchMore() throws Exception
		{			
			String key = HBCBasicfeature.getExcelVal("HB428", sheet, 1);
			ChildCreation("HUDSONBAY-428 Verify that "+" icon should be shown near the color swatches when more than 4 color swatches is available for the product");
			boolean swag = false;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{	
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", searchIcon);		
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{						
						searchBox.clear();
						searchBox.sendKeys(key);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							int plp_size = plpItemContList.size();
							int j = 0;
							boolean flag = false;
							do
							{
								for(j=1;j<=plp_size;j++)
								{
									List<WebElement> swatch  = driver.findElements(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+j+"])//*[@class='sk_prdswatch']"));
									int prodSize = swatch.size();
									if(prodSize>=4)
									{			
										try
										{
											swag = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+j+"])//*[@class='sk_prdsmorewatch']")).isDisplayed();								
											flag = true;			
											break;
										}							
										catch (Exception e)
										{
											continue;
										}
									}
									else
									{
										continue;
									}								
								}
							}
							while(!flag==true);
							if(swag==true)
							{
								Pass(" '+' icon shown near the color swatches when more than 4 color swatches is available for the product");
							}
							else
							{
								Fail(" '+' icon not displayed near the color swatches");
							}
						}						
						else
						{
							Fail("PLP page not displayed");
						}
					}			
					else
					{
						Fail("SearchBox not displayed");
					}				
				}
				else
				{
					Fail("SearchIcon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-429 Verify that on tapping the '+' icon near the color swatches, it should navigates to its corresponding PDP page*/
	public void swatches_to_PDP()
		{
			ChildCreation("HUDSONBAY-429 Verify that on tapping the '+' icon near the color swatches, it should navigates to its corresponding PDP page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(Swatches));
				if(HBCBasicfeature.isElementPresent(MoreSwatches))
				{
					MoreSwatches.click();
					wait.until(ExpectedConditions.visibilityOf(PDPPage));
					if(HBCBasicfeature.isElementPresent(PDPPage))
					{
						Pass("On tapping the '+' icon near the color swatches, navigates to its corresponding PDP page");
						driver.navigate().back();
					}
					else
					{
						Fail("On tapping the '+' icon near the color swatches, not navigates to its corresponding PDP page");
						driver.navigate().back();
					}
				}
				else
				{
					Fail("'+' icon near the color swatches not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	
	/*HUDSONBAY-430 Verify that "Back to Top" icon should be displayed as per the creative  in the search results page*/
	public void Search_back_To_topCreative()
		{
			ChildCreation("HUDSONBAY-430 Verify that 'Back to Top' icon should be displayed as per the creative  in the search results page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(back_To_top));
				if(HBCBasicfeature.isElementPresent(back_To_top))
				{				
					Pass("'Back to Top' icon displayed as per the creative");
				}
				else
				{
					Fail("'Back to Top' icon not displayed as per the creative");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-431 Verify that "Back to Top" button/icon should be shown while scrolling the products in the product list page*/
	public void search_back_To_topScroll()
		{
			ChildCreation("HUDSONBAY-431 Verify that 'Back to Top' button/icon should be shown while scrolling the products in the product list page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					int size = plpItemContList.size();
					if(size>6)
					{
						Thread.sleep(500);
						HBCBasicfeature.scrolldown(footerContainer, driver);
						wait.until(ExpectedConditions.visibilityOf(back_To_top));
						if(HBCBasicfeature.isElementPresent(back_To_top))
						{
							Pass("Back to top button present");
						}
						else
						{
							Fail("Back to top button present");
						}
					}
					else
					{
						Skip("Not more than 6 products present, so 'back to top' button not displayed");
						HBCBasicfeature.scrolldown(footerContainer, driver);
					}				
				}
				else
				{
					Fail("PLP page not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-432 Verify that "Back to Top" button/icon should be displayed in the right side of the product list page*/
	public void search_back_To_topRightSide()
		{
			ChildCreation("HUDSONBAY-432 Verify that 'Back to Top' button/icon should be displayed in the right side of the product list page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(back_To_top));
				if(HBCBasicfeature.isElementPresent(back_To_top))
				{				
					String val =  back_To_top.getCssValue("right");
					if(val.contains("px"))
					{
						Pass("Back to Top' button/icon displayed in the right side of the product list page");
					}
					else
					{
						Fail("Back to Top' button/icon not displayed in the right side");
					}						
				}
				else
				{
					Fail("Back to Top' button/icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}		
		}
	
	/*HUDSONBAY-434 Verify that while selecting the "Back to Top" icon then page should be scroll to top of the page*/
	public void search_back_To_top()
		{
			ChildCreation("HUDSONBAY-434 Verify that while selecting the 'Back to Top' icon then page should be scroll to top of the page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(back_To_top));
				if(HBCBasicfeature.isElementPresent(back_To_top))
				{
					back_To_top.click();					
					Thread.sleep(1000);					
					if(!HBCBasicfeature.isElementPresent(back_To_top))
					{
						Pass("While selecting the 'Back to Top' icon then page scrolled to top of the page");
					}
					else
					{
						Fail("While selecting the 'Back to Top' icon then page not scrolled to top of the page");
					}					
				}
				else
				{
					Fail("Back to top not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-433 Verify that "Back to Top" icon should not be displayed in the top of the page*/
	public void search_back_To_topDisabled()
		{
			ChildCreation("HUDSONBAY-433 Verify that Back to Top icon should not be displayed in the top of the page");
			try
			{
				HBCBasicfeature.scrolldown(footerContainer, driver);
				wait.until(ExpectedConditions.visibilityOf(back_To_top));
				if(HBCBasicfeature.isElementPresent(back_To_top))
				{
					back_To_top.click();	
					Thread.sleep(1000);
					if(!HBCBasicfeature.isElementPresent(back_To_top))
					{
						Pass("Back to Top icon not displayed in the top of the page");
					}
					else
					{
						Fail("Back to Top icon displayed in the top of the page");
					}					
				}
				else
				{
					Fail("Back to top not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}

	/*HUDSONBAY-436 Verify that if the product has multiple price, the regular and the sale price should be displayed*/
	public void procuct_RegularSalePrice()
		{
			ChildCreation("HUDSONBAY-436 Verify that if the product has multiple price, the regular and the sale price should be displayed");
			try
			{
				int reg = 0 , sal = 0, regSal = 0;
				System.out.println("Please wait for while...checking all the products has multiple price, the regular and the sale price should be displayed..");
				for(int i=1;i<=plp_Pagesize;i++)
				{	
					try
					{						
						WebElement regularPrice = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_pdtPriceCont'])["+i+"]//*[@pricetype='Reg']"));
						wait.until(ExpectedConditions.visibilityOf(regularPrice));
						if(HBCBasicfeature.isElementPresent(regularPrice))
						{							
							try
							{
								WebElement salePrice = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_pdtPriceCont'])["+i+"]//*[@pricetype='Sale']"));
								wait.until(ExpectedConditions.visibilityOf(salePrice));
								if(HBCBasicfeature.isElementPresent(salePrice))
								{
									regSal++;
									continue;
								}
							}
							catch(Exception e)
							{
								reg++;
								continue;
							}							
						}
						else
						{
							Fail("Regular Price not available for the products");	
						}
					}
					catch(Exception e)
					{
						WebElement salePrice = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_pdtPriceCont'])["+i+"]//*[@pricetype='Sale']"));
						wait.until(ExpectedConditions.visibilityOf(salePrice));
						if(HBCBasicfeature.isElementPresent(salePrice))
						{
							sal++;
							continue;
						}
						else
						{
							Fail("Sale Price not available for the products");
						}
					}
				}
				log.add("The total products in PLP page is: "+plp_Pagesize);
				log.add("Total Products has both Regular price and sale price: "+regSal);
				log.add("Total Products has only Regular price: "+reg);
				log.add("Total Products has only sale price: "+sal);
				if(regSal>=1)
				{
					Pass("Both Regular Price and Sale Price displayed for the products",log);	
				}
				else
				{
					Pass("Sale price alone displayed for the products",log);
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/* HUDSONBAY-437 Verify that if the product has any offers, the offer details are displayed (Eg: "HUDSON'S BAY EXCLUSIVE")*/
	public void productSplOffer()
		{
			ChildCreation(" HUDSONBAY-437 Verify that if the product has any offers, the offer details are displayed (Eg: 'HUDSON'S BAY EXCLUSIVE')");
			try
			{
				int ctr = 0;
				boolean flag = false;
				System.out.println("Please wait for while...checking all the products has any offers, the offer details are displayed..");
				for(int i=1;i<=plp_Pagesize;i++)
				{	
					try
					{
						WebElement product_offer = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_buyInfo skMob_extendedSizes'])["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(product_offer));
						if(HBCBasicfeature.isElementPresent(product_offer))
						{							
							ctr++;
							flag = true;
							String offer = product_offer.getText();
							log.add("The product has offer: "+offer);
							continue;
						}
						else
						{
							Fail("Product special offer not displayed for products");	
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
				if(flag==true)
				{
					log.add("Total Products has special offers is: "+ctr);
					Pass("Product special offer displayed for each product",log);	
				}
				else
				{
					Fail("Special offers not displayed for any of the displayed product");
				}									
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}

	/*HUDSONBAY-439 Verify that broken images should not be shown in the product list page */
	public void brokenImage()
		{
			ChildCreation("HUDSONBAY-439 Verify that broken images should not be shown in the product list page");		
			int ctr = 0;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
/*					plp_Pagesize = plpItemContList.size();
*/					System.out.println("Please wait for while...checking all the products for any broken Image..");
					for(int i=1;i<=plp_Pagesize;i++)
					{
						Thread.sleep(200);
						WebElement image = 	driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv']//img)["+i+"]"));	
						Thread.sleep(1000);
						HBCBasicfeature.scrolldown(image, driver);						
						int resp = HBCBasicfeature.imageBroken(image, log);
						if(resp==200)
						{
							ctr++;
							continue;
						}
						else
						{
							log.add("Image broken for the product"+i);
							continue;
						}
					}
					if(plp_Pagesize==ctr)
					{
						Pass("Broken images not be shown in the product list page",log);
					}
					else
					{
						Fail("Broken images shown in the product list page",log);
					}
				}
				else
				{
					Fail("PLP page not dispalyed");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-441 Verify that while selecting the sort/filter options then "Refine" page should be displayed*/
	public void searchRefinePage()
		{
			ChildCreation("HUDSONBAY-441 Verify that while selecting the sort/filter options then 'Refine' page should be displayed");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", sort);		
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						Pass("On selecting Sort option then 'Refine' page displayed ");
						RefinewindowClose.click();						
					}
					else
					{
						Fail("On selecting Sort option then 'Refine' page not displayed ");
					}					
				}
				else
				{
					Fail("Sort option not displayed");
				}
				
				wait.until(ExpectedConditions.visibilityOf(Filter));
				if(HBCBasicfeature.isElementPresent(Filter))
				{
					action.moveToElement(Filter).click().build().perform();
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						Pass("On selecting Filter option then 'Refine' page displayed ");
						//RefinewindowClose.click();
					}
					else
					{
						Fail("On selecting Filter option then 'Refine' page not displayed ");
					}					
				}
				else
				{
					Fail("Filter option not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
		}
	
	/*HUDSONBAY-443 Verify that header should be hidden when the sort/filter drawer is in enabled state*/
	public void searchRefine_HeaderDisabled()
		{
			ChildCreation("HUDSONBAY-443 Verify that header should be hidden when the sort/filter drawer is in enabled state");
			try
			{				
				wait.until(ExpectedConditions.visibilityOf(Refinewindow));
				if(HBCBasicfeature.isElementPresent(Refinewindow))
				{
					try
					{	
						logo.click();
						Fail("Header not hidden when the sort/filter drawer is in enabled state");
					}
					catch (Exception e)
					{
						Pass("Header not displayed when the sort/filter drawer is in enabled state");
					}
				}
				else
				{
					Fail("Refine window not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}		
		}
	
	/*HUDSONBAY-445 Verify that Close "X" icon should be shown in the top left corner of the drawer*/
	public void searchRefine_close()
		{
			ChildCreation("HUDSONBAY-445 Verify that Close X icon should be shown in the top left corner of the drawer");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(RefinewindowClose));
				if(HBCBasicfeature.isElementPresent(RefinewindowClose))
				{
					Pass("Close X icon shown in the top left corner of the drawer");
				}
				else
				{
					Fail("Close X icon not shown in the top left corner of the drawer");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-446 Verify while selecting the Close "X" icon then the sort/filter drawer should be closed(i.e. moves from top to bottom)*/
	public void searchRefine_CloseClick()
		{
			ChildCreation("HUDSONBAY-446 Verify while selecting the Close X icon then the sort/filter drawer should be closed(i.e. moves from top to bottom)");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(RefinewindowClose));
				if(HBCBasicfeature.isElementPresent(RefinewindowClose))
				{
					RefinewindowClose.click();
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						Fail("Selecting the Close X icon then the sort/filter drawer not closed");
					}
					else
					{
						Pass("Selecting the Close X icon then the sort/filter drawer closed");
					}					
				}
				else
				{
					Fail("Refine close icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}	
	
	/*HUDSONBAY-456 Verify that "clear All" button should be shown in the top right corner of the drawer*/
	public void searchRefine_ClearAll() throws Exception
		{
			ChildCreation("HUDSONBAY-456 Verify that 'clear All' button should be shown in the top right corner of the drawer");
			String clear= HBCBasicfeature.getExcelVal("HB456", sheet, 1);
			try
			{
				wait.until(ExpectedConditions.visibilityOf(Refinewindow));
				if(HBCBasicfeature.isElementPresent(RefinewindowClearAll))
				{
					String text = RefinewindowClearAll.getText();
					if(text.equalsIgnoreCase(clear))
					{
						log.add("Text displayed is: "+text);
						Pass("'clear All' button shown in the top right corner of the drawer",log);					
					}
					else
					{
						Fail("'clear All' button not shown in the top right corner of the drawer",log);
					}
				}
				else
				{
					Fail("clear All button not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-447 Verify that on selecting the filter button the different types of filter attributes and the values should be displayed*/
	public void filterOptions() throws Exception
		{
			ChildCreation("HUDSONBAY-447 Verify that on selecting the filter button the different types of filter attributes and the values should be displayed");
			String key = HBCBasicfeature.getExcelVal("HB447", sheet, 2);
			String[] filter = key.split("\n");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(Refinewindow));
				if(HBCBasicfeature.isElementPresent(Refinewindow))
				{
					wait.until(ExpectedConditions.visibilityOf(sortBy));
					if(HBCBasicfeature.isElementPresent(sortBy))
					{
						String actual_txt = sortBy.getText();
						if(filter[0].equalsIgnoreCase(actual_txt))
						{
							Pass("Sort By option displayed",log);
						}						
					}
					else
					{
						Fail("Sort By option not displayed",log);
					}
					
					if(HBCBasicfeature.isListElementPresent(FilterAttributes))
					{
						int size = FilterAttributes.size();
						for(int i=0;i<size;i++)
						{
							String attribute = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]")).getText();
							//System.out.println(attribute);
							//System.out.println(filter[(i+1)]);
							if(filter[(i+1)].equalsIgnoreCase(attribute))
							{
								Pass("Filter attribute "+ attribute +" displayed");
							}
							else
							{
								Fail("Filter attribute "+ attribute +" not displayed");
							}							
						}
					}
					else
					{
						Fail("Filter attributes not displayed");
					}					
				}
				else
				{
					Fail("Sort & Filter window not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-448 Verify that user can't able to select multiple "Sort By" options of his choice in the refine panel*/
	public void sortOption_MultipleSelection()
		{
			ChildCreation("HUDSONBAY-362 Verify that user can't able to select multiple 'Sort By' options of his choice in the refine panel");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(sortByOpen));
				if(HBCBasicfeature.isListElementPresent(sortOptionsList))
				{
					int size = sortOptionsList.size();
					for(int i=1;i<=size;i++)
					{
						WebElement sortOptions = driver.findElement(By.xpath("//*[@id='sk_sortOptionContainer_id']["+i+"]"));
						sortOptions.click();
						wait.until(ExpectedConditions.attributeContains(loadingBarr, "style", "none"));
						Thread.sleep(1000);
					}
					int act_size = sortOptionSelectedList.size();
					if(act_size==1)
					{
						String selectedSort = sortOptionSelected.getText();
						log.add("The selected Sort option is: "+selectedSort);
						Pass("User can't able to select multiple 'Sort By' options of his choice in the refine panel",log);
					}
					else
					{
						log.add("Totall selected sort options are: "+act_size);
						Fail("User can able to select multiple 'Sort By' options of his choice in the refine panel",log);
					}					
				}
				else
				{
					Fail("Sort options not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
		}
	
	/*HUDSONBAY-449 Verify that blue tick mark should be shown for selected filters at the right corner in the "Refine" panel*/
	public void sortOptionBlueTick()
		{
			ChildCreation("HUDSONBAY-449 Verify that blue tick mark should be shown for selected filters at the right corner in the 'Refine' panel");
					try
					{
						wait.until(ExpectedConditions.visibilityOf(sortByOpen));
						if(HBCBasicfeature.isListElementPresent(sortOptionsList))
						{
							String selectedSort = sortOptionSelected.getText().replaceAll("\\s+", "");
							log.add("Selected sort option is: "+selectedSort);
							if(HBCBasicfeature.isElementPresent(sortOptionSelectetick))
							{
								String tickOption = sortOptionSelectetick.getAttribute("class").substring(19).replace("sk_selectedSort", "").trim();
								log.add("Selected Tick mark sort option is: "+tickOption);
								if(selectedSort.equalsIgnoreCase(tickOption))
								{
									Pass("Blue tick mark shown for selected filters at the right corner in the 'Refine' panel",log);
								}
								else
								{
									Fail("Blue tick mark not shown for selected filters at the right corner in the 'Refine' panel",log);
								}
							}
							else
							{
								Fail("Tick Mark not displayed");
							}						
						}
						else
						{
							Fail("Sort options are not displayed");
						}
						
					}
					catch (Exception e)
					{
						System.out.println(e.getMessage());
						Exception("There is something wrong. Please Check." + e.getMessage());
					}			
				}
	
	/*HUDSONBAY-450 Verify that selected "Sort By" filter options should be highlighted in bold*/
	public void sortOptionBold() throws Exception
				{
					ChildCreation("HUDSONBAY-450 Verify that selected 'Sort By' filter options should be highlighted in bold");
					String expctd = HBCBasicfeature.getExcelVal("HB450", sheet, 3);
					try
					{
						wait.until(ExpectedConditions.visibilityOf(sortByOpen));
						if(HBCBasicfeature.isElementPresent(sortOptionSelectetick))
						{
							//sortOptionSelectetick.click();
							wait.until(ExpectedConditions.attributeContains(loadingBarr, "style", "none"));
							if(HBCBasicfeature.isElementPresent(sortOptionBold))
							{
								String clr = sortOptionBold.getCssValue("color");
								String act_Color = HBCBasicfeature.colorfinder(clr);
								log.add("The Actual color is: "+act_Color);
								log.add("The Expected color is: "+expctd);
								Thread.sleep(500);
								if(act_Color.equalsIgnoreCase(expctd))
								{
									Pass("Selected 'Sort By' filter options highlighted in bold",log);
								}
								else
								{
									Fail("Selected 'Sort By' filter options not highlighted in bold",log);
								}							
							}
							else
							{
								Fail("Sort option is not bold");
							}
						}
						else
						{
							Fail("Sort option not has tick mark");
						}
						
					}
					catch (Exception e)
					{
						System.out.println(e.getMessage());
						Exception("There is something wrong. Please Check." + e.getMessage());
					}
				}
	
	/*HUDSONBAY-455 Verify that unselected sort by options should be shown in greyed out color*/
	public void sortOptionUnselected() throws Exception
	{
		ChildCreation("HUDSONBAY-455 Verify that unselected sort by options should be shown in greyed out color");
		String expctd = HBCBasicfeature.getExcelVal("HB455", sheet, 3);
		WebElement sortOption = null;
		try
		{
			log.add("The Expected color is: "+expctd);
			wait.until(ExpectedConditions.visibilityOf(sortByOpen));
			if(HBCBasicfeature.isListElementPresent(sortOptionNOTSelected)||HBCBasicfeature.isListElementPresent(sortOptionNOTSelectedddd))
			{	
				int size = 0; 			
				if(HBCBasicfeature.isListElementPresent(sortOptionNOTSelected))
				{
					size = sortOptionNOTSelected.size();
				}
				else if(HBCBasicfeature.isListElementPresent(sortOptionNOTSelectedddd))
				{
					size = sortOptionNOTSelectedddd.size();
				}
				else
				{
					Fail("Changes in unselected sort by options DOM ");
				}
				for(int i=1;i<=size;i++)
				{
					try
					{
						sortOption = driver.findElement(By.xpath("(//*[@class='sk_sortOptionContainer ']//*[@class='skRes_sortOption'])["+i+"]"));
					}
					catch(Exception e)
					{
						sortOption = driver.findElement(By.xpath("(//*[@class='sk_sortOptionContainer']//*[@class='skRes_sortOption'])["+i+"]"));
					}
					Thread.sleep(500);
					String sortOptionClr = sortOption.getCssValue("color");
					String act_Color = HBCBasicfeature.colorfinder(sortOptionClr);
					log.add("The Actual color is: "+act_Color);
					Thread.sleep(200);
					if(act_Color.equalsIgnoreCase(expctd))
					{
						Pass("unselected 'Sort By' filter option "+sortOption.getText()+" shown in greyed out color",log);
					}
					else
					{
						Fail("unselected 'Sort By' filter option "+sortOption.getText()+" not shown in greyed out color",log);
					}							
				}
			}
			else
			{
				Fail("Not selected sort by options not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-452 Verify that Down arrow/Up arrow should be shown for Open/Close Refine By options */
	public void upDownArrow()
		{
			ChildCreation("HUDSONBAY-452 Verify that Down arrow/Up arrow should be shown for Open/Close Refine By options ");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(sortBy));
				if(HBCBasicfeature.isElementPresent(sortBy))
				{
					log.add("Sort option displayed");
					if(HBCBasicfeature.isElementPresent(sortByArrow))
					{
						Pass("Down arrow/Up arrow displayed near SortBy option", log);
					}	
					else
					{
						Fail("Down arrow/Up arrow not displayed near SortBy option", log);
					}
				}
				else
				{
					Fail("Sort option not displayed");
				}
				
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					int size = FilterAttributes.size();
					for(int i=0;i<size;i++)
					{
						WebElement arrow = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]")).getText().replace(":","");
						if(HBCBasicfeature.isElementPresent(arrow))
						{
							Pass("Down arrow/Up arrow shown for Refine By option: "+filter_Name);
						}
						else
						{
							Fail("Down arrow/Up arrow not shown for Refine By option: "+filter_Name);
						}
					}						
				}
				else
				{
					Fail("Filter options not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	
	/*HUDSONBAY-454/468 Verify that Down arrow should be shown for the closed refine by option*/
	public void downArrowClosedState()
		{
			WebDriverWait wait = new WebDriverWait(driver, 5);
			ChildCreation("HUDSONBAY-454 Verify that Down arrow should be shown for the closed refine by option");
			boolean flag = false;
			try
			{
				//Down Arrow for SortBy option
				try
				{
					wait.until(ExpectedConditions.attributeContains(sortByOpenDom, "style", "none"));	
				}
				catch(Exception e)
				{
					sortByArrow.click();
					wait.until(ExpectedConditions.attributeContains(sortByOpenDom, "style", "none"));	
				}
				if(HBCBasicfeature.isElementPresent(sortByArrow))
				{
					Pass("Down arrow displayed near SortBy option");
				}
				else
				{
					Fail("Down arrow not displayed near SortBy option");
				}			
				
				//Down Arrow for Filter options
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					int size = FilterAttributes.size();
					for(int i=0;i<size;i++)
					{
						flag = false;
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(filter, "style", "display: none"));	
						}
						catch(Exception e)
						{
							WebElement arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							arrow_clk.click();
							wait.until(ExpectedConditions.attributeContains(filter, "style", "display: none"));	
						}
						WebElement arrow = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						Thread.sleep(500);
						String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");
						if(HBCBasicfeature.isElementPresent(arrow))
						{
							Pass("Down arrow shown for Refine By option: "+filter_Name);
							flag = true;
							HBCBasicfeature.scrolldown(arrow, driver);
							arrow.click();
						}
						else
						{
							Fail("Down arrow not shown for Refine By option: "+filter_Name);
						}
					}
				}
				else
				{
					Fail("Filter options not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-468 Verify that initially all the filter/sort options should be in collapsed state");
			if(flag==true)
			{
				Pass("Initially all the filter/sort options in collapsed state");
			}
			else
			{
				Fail("Initially all the filter/sort options not in collapsed state");
			}
			
		}
	
	/*HUDSONBAY-451 Verify that color difference needs to be shown as per the creative in sort/filter page*/
	public void colorDifference() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-451 Verify that color difference needs to be shown as per the creative in sort/filter page");
			String Color = HBCBasicfeature.getExcelVal("HB451", sheet, 3);
			String split[] = Color.split("\n");		
			log.add("Actual sort/filter header background color is: "+split[0]);
			log.add("Actual sort/filter option background color is: "+split[1]);
			int i = 0;
			try
			{
				Random r = new Random();
				int size = FilterAttributes.size();
				i = r.nextInt(size);					
				String filter = driver.findElement(By.xpath("(//*[@class='skMob_filterContentWrapper  skMob_filterOpen'])["+(i+1)+"]")).getCssValue("background-color");
				String expctdFilterColor  = HBCBasicfeature.colorfinder(filter);
				log.add("Filter background Color is: "+expctdFilterColor);				
				if(split[0].equalsIgnoreCase(expctdFilterColor))
				{
					String filterOptionColor = driver.findElement(By.xpath("(//*[@class='skMob_filterItemContentWrapper'])["+(i+1)+"]")).getCssValue("background-color");
					String expctdFilterOptionColor = HBCBasicfeature.colorfinder(filterOptionColor);
					log.add("Filter options background Color is: "+expctdFilterOptionColor);
					if(split[1].equalsIgnoreCase(expctdFilterOptionColor))
					{
						Pass("color difference shown as per the creative in filter page",log);
					}
					else
					{
						Fail("color difference not shown as per the creative in filter page",log);
					}
				}
				else
				{
					Fail("color difference not shown as per the creative in filter page",log);
				}				
				
				String sortColor = driver.findElement(By.xpath("//*[@class='skRes_sortSelected']")).getCssValue("background-color");
				String expctdSortColor = HBCBasicfeature.colorfinder(sortColor);
				log.add("Sort background Color displayed is: "+expctdSortColor);
				if(split[0].equalsIgnoreCase(expctdSortColor))
				{					
					String sortOptionBG = sortByOpenDom.getCssValue("background-color");
					String expctdSortoptionColor  = HBCBasicfeature.colorfinder(sortOptionBG);
					log.add("Sort option background color is: "+expctdSortoptionColor);
					if(split[1].equalsIgnoreCase(expctdSortoptionColor))
					{
						Pass("color difference shown as per the creative in Sort page: ",log);
					}
					else
					{
						Fail("color difference not shown as per the creative in Sort page",log);
					}
					
				}
				else
				{
					Fail("color difference not shown as per the creative in Sort page: ",log);
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}	
		
	/*HUDSONBAY-453 Verify that Up arrow should be shown for the opened refine by option*/
	public void upArrowOpenedState()
		{
			WebDriverWait wait = new WebDriverWait(driver, 5);
			ChildCreation("HUDSONBAY-453 Verify that Up arrow should be shown for the opened refine by option");
			try
			{
				//Up Arrow for SortBy option
				try
				{
					wait.until(ExpectedConditions.attributeContains(sortByOpenDom, "style", "block"));	
				}
				catch(Exception e)
				{
					sortByArrow.click();
					wait.until(ExpectedConditions.attributeContains(sortByOpenDom, "style", "block"));	
				}
				if(HBCBasicfeature.isElementPresent(sortByArrow))
				{
					Pass("Up arrow displayed near SortBy option");
					sortByArrow.click();
				}
				else
				{
					Fail("Up arrow not displayed near SortBy option");
				}			
				
				//Up Arrow for Filter options
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					int size = FilterAttributes.size();
					for(int i=0;i<size;i++)
					{
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}
						catch(Exception e)
						{
							WebElement arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							HBCBasicfeature.scrolldown(arrow_clk, driver);
							Thread.sleep(500);
							arrow_clk.click();
						}												
						wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						WebElement arrow = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						Thread.sleep(500);
						String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
						if(HBCBasicfeature.isElementPresent(arrow))
						{
							Pass("Up arrow shown for Refine By option: "+filter_Name);
							HBCBasicfeature.scrolldown(arrow, driver);
							Thread.sleep(500);
							arrow.click();
						}
						else
						{
							Fail("Up arrow not shown for Refine By option: "+filter_Name);
						}						
					}
				}
				else
				{
					Fail("Filter options not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-463/460/461 Verify that "View All" option should be selected in default in the sort/filter page*/
	public void FilterViewAll() throws Exception
		{
		boolean child = false;
			ArrayList<Integer> filterlist = new ArrayList<>();
			WebDriverWait wait = new WebDriverWait(driver, 5);
			String viewall = HBCBasicfeature.getExcelVal("HB460", sheet, 1);
			ChildCreation("HUDSONBAY-463 Verify that 'View All' option should be selected in default in the sort/filter page");
			try
			{
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					int size = FilterAttributes.size();
					for(int i=0;i<size;i++)
					{
						child = false;
						WebElement arrow_clk = null ;
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}
						catch(Exception e)
						{
							arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							HBCBasicfeature.scrolldown(arrow_clk, driver);
							arrow_clk.click();
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}
						Thread.sleep(500);
						String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
						String expctd = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[@class='skMob_filterItems  skMob_filterItemSelected  ']")).getText();
						Thread.sleep(500);
						if(expctd.equalsIgnoreCase(viewall))
						{
							Pass(""+expctd+" option selected in default for refine option: "+filter_Name);
							List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
							filterlist.add(filterList.size());		// For HB461					
							HBCBasicfeature.scrolldown(arrow_clk, driver);							
							arrow_clk.click();
							child = true;
						}
						else
						{
							log.add("Selected default option is: "+expctd);
							Fail("'View All' option not selected in default for refine option: "+filter_Name,log);
						}						
					}
				}
				else
				{
					Fail("Filter options not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-460 Verify that 'View All' filter option should be shown under Refine by options, when the product has more number of sort by options");
			if(child==true)
			{
				Pass("'View All' filter option shown under Refine by options, when the product has more number of sort by option");
			}
			else
			{
				Fail("'View All' filter option not shown under Refine by options");
			}	
			
			ChildCreation("HUDSONBAY-461 Verify that all the filter options should be shown, when the user selects 'View All' option");
			if(child==true)
			{
				int size = filterlist.size();
				for(int m = 0;m<size;m++)
				{
					if(filterlist.get(m)>1)
					{
						log.add("The filter options shown for the filter is: "+filterlist.get(m));
						Pass("All the filter options shown, when the user selects 'View All' option",log);
					}
					else
					{
						log.add("The filter options shown for the filter is: "+filterlist.get(m));
						Fail("All the filter options not shown, when the user selects 'View All' option",log);
					}
				}
			}
			else
			{
				Fail("'View All' filter option not shown under Refine by options");
			}			
		}	
	
	/*HUDSONBAY-464/380/449  Verify that on selecting some filter options in the sort/filter then View All option should be disabled*/
	public void FilterViewAllDisabled() throws java.lang.Exception
	{
		boolean filterslct = false;
		int i =0, j=0;
		WebElement filterSelect = null;
		String currentFilter ="";
		String filter_Name ="";
		WebDriverWait wait = new WebDriverWait(driver, 10);
		String viewall = HBCBasicfeature.getExcelVal("HB460", sheet, 1);
		ChildCreation("HUDSONBAY-464 Verify that on selecting some filter options in the sort/filter then View All option should be disabled");
			try
			{
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					Random r = new Random();
					int size = FilterAttributes.size();
					i = r.nextInt(size);					
						WebElement arrow_clk = null ;
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}
						catch(Exception e)
						{
							arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							HBCBasicfeature.scrolldown(arrow_clk, driver);
							arrow_clk.click();
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}	
						Thread.sleep(500);
						filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
						Thread.sleep(500);
						log.add("The Selected Filter is: "+filter_Name);
						Thread.sleep(1000);
						String expctd = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
						Thread.sleep(1000);
						log.add("The default selected option is: "+expctd);
						Thread.sleep(1000);
						if(expctd.equalsIgnoreCase(viewall))
						{
							log.add("View All option selected as default");	
							Thread.sleep(500);
							List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
							int ssize = filterList.size();
							j = r.nextInt(ssize);
							if(j==0)
							{
								j=j+1;
							}
							filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
							HBCBasicfeature.scrolldown(filterSelect, driver);
							filterSelect.click();
							wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
							Thread.sleep(1000);
							arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							arrow_clk.click();
							currentFilter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
							log.add("The current selected filter option is: "+currentFilter);                                                                                              
							if(currentFilter.equalsIgnoreCase(expctd))
							{
								Fail("on selecting some filter options in the sort/filter then View All option not disabled",log);
							}
							else
							{
								Pass("on selecting some filter options in the sort/filter then View All option  disabled",log);
								filterslct = true;
							}							
						}
						else
						{
							Fail("View All option not selected as default",log);
						}						
				}
				else
				{
					Fail("Filter Attributes not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-449 Verify that blue tick mark should be shown for selected filters in the right corner of it");
			if(filterslct==true)
			{
				filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
				log.add("The Selected Filter is: "+filter_Name);
				Thread.sleep(500);
				currentFilter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
				log.add("The current selected filter option is: "+currentFilter);   
				Thread.sleep(500);
				WebElement filterTick = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]//*[@class='cls_skCustomChkboxSpan']"));
				if(HBCBasicfeature.isElementPresent(filterTick))
				{					
					Pass("Blue tick mark shown for selected filters in the right corner of it",log);
				}
				else
				{
					Fail("Blue tick mark not shown for selected filters in the right corner of it",log);
				}					
			}		
			else
			{
				Skip("Filter option not selected");
			}							
			ChildCreation("HUDSONBAY-380 Verify that selected filter option should be deselected on tapping the selected filter option");
			if(filterslct==true)
			{
				Thread.sleep(1000);
				filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
				Thread.sleep(500);
				HBCBasicfeature.scrolldown(filterSelect, driver);
				if(HBCBasicfeature.isElementPresent(filterSelect))
				{
					filterslct = false;
					String selected = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
					log.add("The selected filter option is: "+selected);      
					filterSelect.click();
					wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
					String current = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
					log.add("The current selected filter option is: "+current); 
					Thread.sleep(500);
					if(current.equalsIgnoreCase(selected))
					{
						Fail("Selected filter option not deselected on tapping the selected filter option",log);						
					}
					else
					{
						Pass("Selected filter option deselected on tapping the selected filter option",log);
						filterslct = true;						
					}					
				}
				else
				{
					Fail("Selected filter option is not displayed");
				}
			}
			else
			{
				Skip("Filter option not selected");
			}
		}
	
	/*HUDSONBAY-459/462/467 Verify that product count should be updated based on applied filter as per the classic site*/
	public void Filter_ProductCount() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-459 Verify that product count should be updated based on applied filter as per the classic site");
			Random r = new Random();
			ArrayList<String> filters  = new ArrayList<String>();
			ArrayList<String> filtersUpdate  = new ArrayList<String>();
			ArrayList<Integer> result = new ArrayList<Integer>();
			WebElement arrow_clk = null;
			String slct = HBCBasicfeature.getExcelVal("HB459", sheet, 1);
			int i = 0, j =0, k=0, l = 0;
			boolean opt = false;			
			String filter_Name ="";
			String current ="";
			try
			{				
				//Selecting PLP Items count				
				if(HBCBasicfeature.isElementPresent(plpItemCont))
				{
					String val = plpItemCont.getText();
					String[] split = val.split(" ");
					plpCount = split[0]; 
				}
				else
				{
					Skip("Product count not displayed");
				}		
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", sort);		
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));	
					if(HBCBasicfeature.isListElementPresent(FilterAttributes))
					{
						int size = FilterAttributes.size();
						i = r.nextInt(size);	
						Thread.sleep(500);
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");
						log.add("The Selected Filter is: "+filter_Name);
						Thread.sleep(500);
						arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						HBCBasicfeature.scrolldown(arrow_clk, driver);
						arrow_clk.click();
						wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						Thread.sleep(500);
						List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
						int ssize = filterList.size();
						j = r.nextInt(ssize);
						if(j==0)
						{
							j=j+1;
						}
						l = j;
						WebElement filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
						Thread.sleep(500);						
						HBCBasicfeature.scrolldown(filterSelect, driver);
						filterSelect.click();
						wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
						arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						HBCBasicfeature.scrolldown(arrow_clk, driver);
						arrow_clk.click();
						Thread.sleep(500);
						current = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
						log.add("The current selected filter option is: "+current);  
						filters.add(current);
						wait.until(ExpectedConditions.visibilityOf(RefinewindowApply));
						if(HBCBasicfeature.isElementPresent(RefinewindowApply))
						{
							HBCBasicfeature.scrollup(RefinewindowApply, driver);
							RefinewindowApply.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
							wait.until(ExpectedConditions.visibilityOf(plpItemCont));
							if(HBCBasicfeature.isElementPresent(plpItemCont))
							{
								String val = plpItemCont.getText();
								refCount = FilterCount.getText();	//For comparing HB462
								String[] split = val.split(" ");
								String aftrPlpCount = split[0]; 
								log.add("Before count: "+plpCount);
								log.add("After count: "+aftrPlpCount);
								Thread.sleep(500);
								if(!aftrPlpCount.contentEquals(plpCount))
								{
									Pass("Product count updated based on applied filter as per the classic site",log);
									opt = true;
								}
								else
								{
									Fail("Product count not updated based on applied filter as per the classic site",log);
								}								
							}
							else
							{
								Fail("PLP Item count not displayed");
							}							
						}
						else
						{
							Fail("Apply button not displayed");
						}						
					}
					else
					{
						Fail("Filter attributes not displayed");
					}
				}
				else
				{
					Fail("Sort option not sdisplayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
			ChildCreation("HUDSONBAY-462 Verify that selected refine by options count (filtered count) should be shown near the refine by option as X selected");
			if(opt==true)
			{
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", sort);		
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));					
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");
						log.add("The Selected Filter is: "+filter_Name);
						String filter_Count = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentCount']")).getText();
						Thread.sleep(500);
						String[] split = filter_Count.split(" ");
						String count = split[0]; 
						String selected = split[1];
						log.add("The displayed count is: "+count);
						log.add("The displayed text is: "+selected);
						if(count.contains(refCount) && selected.equalsIgnoreCase(slct))
						{
							Pass("Selected refine by options count (filtered count) shown near the refine by option as X selected",log);	
						}
						else
						{
							Fail("Selected refine by options count (filtered count) shown near the refine by option as X selected",log);
						}
					}
					else
					{
						Fail("Refine window not displayed");
					}
				}
				else
				{
					Fail("Sort option not displayed");
				}
			}
			else
			{
				Skip("Filter option not selected");
			}
						
			
			ChildCreation("HUDSONBAY-467 Verify that selected filter options should be maintained with previously selected filters");
			if(opt==true)
			{	
				Actions act = new Actions(driver);
				arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
				HBCBasicfeature.scrolldown(arrow_clk, driver);
				act.moveToElement(arrow_clk).click().build().perform();
				Thread.sleep(500);
				List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
				int ssize = filterList.size();
				j = r.nextInt(ssize);
				if(j==l||j==0)
				{
					j=j+1;
				}
				WebElement filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
				Thread.sleep(500);						
				HBCBasicfeature.scrolldown(filterSelect, driver);
				filterSelect.click();
				wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
				Thread.sleep(1000);
				arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
				HBCBasicfeature.scrolldown(arrow_clk, driver);
				act.moveToElement(arrow_clk).click().build().perform();
				Thread.sleep(500);
				String filt= driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]")).getText();
				Thread.sleep(200);
				filters.add(filt);
				wait.until(ExpectedConditions.visibilityOf(RefinewindowApply));
				if(HBCBasicfeature.isElementPresent(RefinewindowApply))
				{
					HBCBasicfeature.scrollup(RefinewindowApply, driver);
					RefinewindowApply.click();
					wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
					Thread.sleep(1000);
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", sort);		
					arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
					HBCBasicfeature.scrolldown(arrow_clk, driver);
					arrow_clk.click();
					Thread.sleep(1000);
					List<WebElement> appliedFilters = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]"));
					for(k=1;k<=appliedFilters.size();k++)
					{
						String FilterNames = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]["+k+"]")).getText();
						Thread.sleep(500);
						filtersUpdate.add(FilterNames);
					}
					for (String temp : filtersUpdate)
					{
						result.add(filters.contains(temp) ? 1 : 0);
						//System.out.println(result);
						log.add("Compared result is: "+result);
					}
					
					if(!result.contains(0))
				  	{
						log.add("The previously selected filters are: "+filters);
				  		log.add("The current displayed filters are: "+filtersUpdate);
				  		Pass("Selected filter options maintained with previously selected filters",log);
				  		
				  	}
				  	else
				  	{	
				  		log.add("The previously selected filters are: "+filters);
				  		log.add("The current displayed filters are: "+filtersUpdate);
				  		Fail("Selected filter options not maintained with previously selected filters",log);				  		
				  	}						
				}
				else
				{
					Fail("Refine apply not displayed");
				}				
			}
			else
			{
				Skip("Filter option not selected");
			}					
		}
	
	/*HUDSONBAY-457 Verify that sort/filter options should be cleared and default options should be selected on tapping the "Clear All" link*/
	public void filterClearAll() throws Exception
		{
			ChildCreation("HUDSONBAY-457 Verify that sort/filter options should be cleared and default options should be selected on tapping the 'Clear All' link");
			WebDriverWait wait = new WebDriverWait(driver, 5);
			String viewall = HBCBasicfeature.getExcelVal("HB460", sheet, 1);
			try
			{
				if(HBCBasicfeature.isElementPresent(RefinewindowClearAll))
				{
					HBCBasicfeature.scrollup(RefinewindowClearAll, driver);
					RefinewindowClearAll.click();
					wait.until(ExpectedConditions.visibilityOf(plpPage));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", sort);		
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));	
					if(HBCBasicfeature.isListElementPresent(FilterAttributes))
					{
						int size = FilterAttributes.size();
						for(int i=0;i<size;i++)
						{
							WebElement arrow_clk = null ;
							WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
							try
							{
								wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
							}
							catch(Exception e)
							{
								arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
								HBCBasicfeature.scrolldown(arrow_clk, driver);
								arrow_clk.click();
								wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
							}
							Thread.sleep(500);
							String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
							String expctd = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[@class='skMob_filterItems  skMob_filterItemSelected  ']")).getText();
							Thread.sleep(500);
							if(expctd.equalsIgnoreCase(viewall))
							{
								Pass(""+expctd+" option selected in default after clearall all the filters for option: "+filter_Name);
								HBCBasicfeature.scrolldown(arrow_clk, driver);							
								arrow_clk.click();
							}
							else
							{
								log.add("Selected default option is: "+expctd);
								Fail("'View All' option not selected in default after clearall all the filters for refine option: "+filter_Name,log);
							}						
						}
					}
					else
					{
						Fail("Filter options not displayed");
					}
				}
				else
				{
					Fail("Clear All option not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
		}
		
	/*HUDSONBAY-465 Verify that on tapping the "X" close icon after selecting the filter options, PLP page should not be updated based on the selected filter options*/
	public void Filter_not_selected()
		{
			ChildCreation("HUDSONBAY-465 Verify that on tapping the 'X' close icon after selecting the filter options, PLP page should not be updated based on the selected filter options");
			int i =0;
			WebElement filterSelect = null;
			WebDriverWait wait = new WebDriverWait(driver, 5);			
				try
				{
					if(HBCBasicfeature.isElementPresent(sort))
					{
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", sort);		
						wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));
						if(HBCBasicfeature.isListElementPresent(FilterAttributes))
						{
							Random r = new Random();
							int size = FilterAttributes.size();
							i = r.nextInt(size);					
							WebElement arrow_clk = null ;
							WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
							try
							{
								wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
							}
							catch(Exception e)
							{
								arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
								HBCBasicfeature.scrolldown(arrow_clk, driver);
								arrow_clk.click();
								wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
							}	
							String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
							log.add("The Selected Filter is: "+filter_Name);
							Thread.sleep(1000);
							List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
							int ssize = filterList.size();
							int j = r.nextInt(ssize);
							if(j==0)
							{
								j=j+1;
							}
							filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
							HBCBasicfeature.scrolldown(filterSelect, driver);
							filterSelect.click();
							wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
							Thread.sleep(500);
							String current = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
							log.add("The current selected filter option is: "+current);      
							wait.until(ExpectedConditions.visibilityOf(RefinewindowClose));
							if(HBCBasicfeature.isElementPresent(RefinewindowClose))
							{
								log.add("X close icon displayed");
								HBCBasicfeature.scrollup(RefinewindowClose, driver);
								RefinewindowClose.click();
								wait.until(ExpectedConditions.visibilityOf(plpItemCont));
								if(HBCBasicfeature.isElementPresent(plpItemCont))
								{
									String val = plpItemCont.getText();
									String[] split = val.split(" ");
									String aftrPlpCount = split[0]; 
									log.add("Before count: "+plpCount);
									log.add("After count: "+aftrPlpCount);
									Thread.sleep(500);
									if(aftrPlpCount.contentEquals(plpCount))
									{
										Pass("'X' close icon after selecting the sort/filter options, PLP page not updated based on the selected filter options",log);
									}
									else
									{
										Fail("'X' close icon after selecting the sort/filter options, PLP page updated",log);
									}								
								}
								else
								{
									Fail("PLP Item count not displayed");
								}							
							}
							else
							{
								Fail("'X' close icon not displayed");
							}				
						}
						else
						{
							Fail("Filter Attributes not displayed");
						}	
					}
					else
					{
						Fail("Sort option not displayed");
					}
				}	
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-413 Verify that on tapping the valid product/Web ID and tapping the search button, respective PDP page should be shown*/
	public void searchPage_TO_PDP()
		{
			ChildCreation("HUDSONBAY-413 Verify that on tapping the valid product/Web ID and tapping the search button, respective PDP page should be shown");
			try
			{
				if(HBCBasicfeature.isElementPresent(plpPage))
				{				
					if(HBCBasicfeature.isListElementPresent(plpItemContList))
					{
						int size = plpItemContList.size();
						Random r  = new Random();
						int i = r.nextInt(size);
						WebElement product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
						wait.until(ExpectedConditions.visibilityOf(product));
						HBCBasicfeature.scrolldown(product, driver);
						if(HBCBasicfeature.isElementPresent(product))
						{
							String product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+(i+1)+"]")).getText();
							String product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+(i+1)+"]")).getText();
							String PLPtitle = product_BrandTitle +" "+ product_Title;
							log.add("The selected product title is: "+PLPtitle);
							Thread.sleep(1000);
							WebElement product_Clk = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])["+(i+1)+"]"));		
							product_Clk.click();
							wait.until(ExpectedConditions.visibilityOf(PDPPage));
							if(HBCBasicfeature.isElementPresent(PDPPage))
							{
								String PDPtitle = PDPProdName.getText();
								log.add("The PDP page title is: "+PDPtitle);
								if(PDPtitle.equalsIgnoreCase(PLPtitle))
								{
									Pass("On tapping the valid product/Web ID and tapping the search button, respective PDP page shown",log);
								}
								else
								{
									Fail("On tapping the valid product/Web ID and tapping the search button, respective PDP page not shown",log);
								}
							}
							else
							{
								Fail("PDP Page not displayed");
							}
						}
						else
						{
							Fail("Selected product not displayed");
						}					
					}
					else
					{
						Fail("PLP products are not dispalyed");
					}
				}
				else
				{
					Fail("PLP Page not displayed");
				}
				driver.navigate().back();
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
		}
	
	}
