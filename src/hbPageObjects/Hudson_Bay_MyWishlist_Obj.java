package hbPageObjects;

import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_MyWishlist;

public class Hudson_Bay_MyWishlist_Obj extends Hudson_Bay_MyWishlist implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_MyWishlist_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath=""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+bagIconCountt+"")
	WebElement bagIconCount;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;	
	
	@FindBy(xpath = ""+shoppingBagPageDomm+"")
	WebElement shoppingBagPageDom;	
	
	@FindBy(xpath = ""+shoppingBagProdd+"")
	WebElement shoppingBagProd;
			
	@FindBy(xpath = ""+shoppingBagPageProdTitlee+"")
	WebElement shoppingBagPageProdTitle;	
	
	@FindBy(xpath = ""+shoppingBagshopNowbuttonn+"")
	WebElement shoppingBagshopNowbutton;		
		
	@FindBy(xpath = ""+pdpWishListIconn+"")
	WebElement pdpWishListIcon;	
	
	@FindBy(xpath = ""+pdpWebIdd+"")
	WebElement pdpWebId;	
	
	@FindBy(xpath = ""+PDPProdNamee+"")
	WebElement PDPProdName;	
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+homepagee+"")
	WebElement homepage;	
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;	
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+pancakeStaticWelcomee+"")
	WebElement pancakeStaticWelcome;
	
	@FindBy(xpath = ""+pancakeStaticWishlistt+"")
	WebElement pancakeStaticWishlist;	
		
	@FindBy(xpath = ""+signInPagee+"")
	WebElement signInPage;
	
	@FindBy(xpath = ""+emailFieldd+"")
	WebElement emailField;
	
	@FindBy(xpath = ""+pwdFieldd+"")
	WebElement pwdField;
	
	@FindBy(xpath = ""+signInButtonn+"")
	WebElement signInButton;
	
	@FindBy(xpath = ""+myAccountPagee+"")
	WebElement myAccountPage;
	
	@FindBy(xpath = ""+myAccountpageBackk+"")
	WebElement myAccountpageBack;	
	
	@FindBy(xpath = ""+myAccountPageMyProfileTxtt+"")
	WebElement myAccountPageMyProfileTxt;
	
	@FindBy(xpath = ""+myAccountPageMyProfileDetailss+"")
	WebElement myAccountPageMyProfileDetails;
	
	@FindBy(xpath = ""+myAccountPageMyWishlistt+"")
	WebElement myAccountPageMyWishlist;
	
	@FindBy(xpath = ""+myWishlistEmailWishh+"")
	WebElement myWishlistEmailWish;
	
	@FindBy(xpath = ""+WishListEmailWishpagee+"")
	WebElement WishListEmailWishpage;
	
	@FindBy(xpath = ""+WishListEmailWishpageclosee+"")
	WebElement WishListEmailWishpageclose;
	
	@FindBy(xpath = ""+WishListEmailWishpageCloseDomm+"")
	WebElement WishListEmailWishpageCloseDom;	
	
	@FindBy(xpath = ""+WishListEmailWishpageToEmaill+"")
	WebElement WishListEmailWishpageToEmail;
	
	@FindBy(xpath = ""+WishListEmailWishpageFromNamee+"")
	WebElement WishListEmailWishpageFromName;
	
	@FindBy(xpath = ""+WishListEmailWishpageFromEmaill+"")
	WebElement WishListEmailWishpageFromEmail;
	
	@FindBy(xpath = ""+WishListEmailWishpageAdditionalMsgg+"")
	WebElement WishListEmailWishpageAdditionalMsg;
	
	@FindBy(xpath = ""+WishListEmailWishpageSendButtonn+"")
	WebElement WishListEmailWishpageSendButton;	
	
	@FindBy(xpath = ""+WishListEmailWishpageSendEmailAlertt+"")
	WebElement WishListEmailWishpageSendEmailAlert;	
			
	@FindBy(xpath = ""+pdpWishListSuccessOverlayy+"")
	WebElement pdpWishListSuccessOverlay;
	
	@FindBy(xpath = ""+pdpWishListSuccessOkButtonn+"")
	WebElement pdpWishListSuccessOkButton;
	
	@FindBy(xpath = ""+WishListProductImagee+"")
	WebElement WishListProductImage;
	
	@FindBy(xpath = ""+WishListProductSizee+"")
	WebElement WishListProductSize;
	
	@FindBy(xpath = ""+WishListProductTitlee+"")
	WebElement WishListProductTitle;
	
	@FindBy(xpath = ""+WishListProductTitlee2+"")
	WebElement WishListProductTitle2;	
	
	@FindBy(xpath = ""+WishListEmptyWishh+"")
	WebElement WishListEmptyWish;	
	
	@FindBy(xpath = ""+WishListProductColorr+"")
	WebElement WishListProductColor;	
	
	@FindBy(xpath = ""+WishListProductPricee+"")
	WebElement WishListProductPrice;	
	
	@FindBy(xpath = ""+myWishlistCloseIconn+"")
	WebElement myWishlistCloseIcon;	
	
	@FindBy(xpath = ""+WishListAddToCartt+"")
	WebElement WishListAddToCart;	
	
	@FindBy(xpath = ""+myWishlistAddAllItemss+"")
	WebElement myWishlistAddAllItems;
	
	@FindBy(xpath = ""+WishListEmailWishpageToEmailAlertt+"")
	WebElement WishListEmailWishpageToEmailAlert;
	
	@FindBy(xpath = ""+WishListEmailWishpageFromNameAlertt+"")
	WebElement WishListEmailWishpageFromNameAlert;	
	
	@FindBy(how = How.XPATH,using = ""+shoppingBagProdd+"")
	List<WebElement> shoppingBagProdListt;
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	@FindBy(how = How.XPATH,using = ""+pdpWishListProductListt+"")
	List<WebElement> pdpWishListProductList;		
	
	@FindBy(xpath = ""+addressVerificationoverlayy+"")
	WebElement addressVerificationoverlay;
	
	@FindBy(xpath = ""+addressuseasEnteredd+"")
	WebElement addressuseasEntered;
	
	
	boolean MyWishlist, MyAccount = false;
	
	String PDPtitle = "" , pdpID = "";
	
	//Add Product to Wishlist
	public void addProductToWishlist() throws Exception
	{
		String Key = HBCBasicfeature.getExcelVal("wishKey", sheet, 1);
		wait.until(ExpectedConditions.visibilityOf(searchIcon));
		if(HBCBasicfeature.isElementPresent(searchIcon))
		{
			searchIcon.click();
			wait.until(ExpectedConditions.visibilityOf(searchBox));
			if(HBCBasicfeature.isElementPresent(searchBox))
			{
				searchBox.sendKeys(Key);
				searchBox.sendKeys(Keys.ENTER);
				boolean pgLoad = false;
				do
				{				
					try
					{
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(plpPage))		
						{
							pgLoad=true;	
							break;		
						}
					}
					catch (Exception e)
					{
						pgLoad=false;
						driver.navigate().refresh();	
						continue;
					}
				}							
				while(!pgLoad==true); 							
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					Pass("PLP page displayed");
					Thread.sleep(500);
					int size = plpItemContList.size();
					Random r  = new Random();
					int i = r.nextInt(size);
					WebElement product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
					HBCBasicfeature.scrolldown(product, driver);
					Thread.sleep(1000);
					product.click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(PDPPage));
					if(HBCBasicfeature.isElementPresent(PDPPage))
					{
						Pass("PDP page displayed");
						PDPtitle = PDPProdName.getText();
						pdpID = pdpWebId.getText().substring(9);
						if(HBCBasicfeature.isElementPresent(pdpWishListIcon))
						{
							Pass("Wishlist Icon displayed");
							HBCBasicfeature.jsclick(pdpWishListIcon, driver);
							wait.until(ExpectedConditions.attributeContains(pdpWishListSuccessOverlay, "style", "absolute"));
							if(HBCBasicfeature.isElementPresent(pdpWishListSuccessOkButton))
							{
								pdpWishListSuccessOkButton.click();
								Pass("Wishlist 'View Wishlist' button displayed");
								wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
								Thread.sleep(1000);
								if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
								{
									int sizee = pdpWishListProductList.size();
									if(sizee>0)
									{
										Pass("Product added to Wishlist page");
									}
									else									
									{
										Fail("Product not added to Wishlist page");
									}									
								}
								else
								{
									Fail("Products not displayed in Mywishlist page");
								}								
							}
							else
							{
								Fail("Wishlist success overlay 'View Wishlist' button not displayed");
							}							
						}
						else
						{
							Fail("Wishlist Icon not displayed");
						}						
					}
					else
					{
						Fail("PDP page not displayed");
					}
				}
				else
				{
					Fail("PLP page not displayed");
				}
			}
			else
			{
				Fail("SearchBox not displayed");
			}
		}
		else
		{
			Fail("Search Icon not displayed");
		}
	}
	
	//Remove Products from ShoppingBag
	public void removeProdFromBag() throws Exception
	{	
		ChildCreation("Remove Products from ShoppingBag");		
		wait.until(ExpectedConditions.visibilityOf(bagIcon));
		if(HBCBasicfeature.isElementPresent(bagIcon))
		{
			HBCBasicfeature.jsclick(bagIcon, driver);		
			wait.until(ExpectedConditions.visibilityOf(shoppingBagPageDom));
			if(HBCBasicfeature.isElementPresent(shoppingBagPageDom))
			{					
				if(HBCBasicfeature.isListElementPresent(shoppingBagProdListt))
				{
					int i =0;
					boolean flag = false;
					do
					{
						i=1;
						WebElement removeProd = driver.findElement(By.xpath("(//*[@class='item-info']//*[@class='remove'])["+i+"]"));
						removeProd.click();
						Thread.sleep(3000);
						wait.until(ExpectedConditions.attributeContains(shoppingBagPageDom, "style", "visible"));
						try
						{
							Thread.sleep(1000);
							int Val = Integer.parseInt(bagIconCount.getText());
							if(Val==0)
							{
								Pass("Products removed from ShoppinBag successfully");
								flag = true;
								break;
							}
							else
							{
								continue;
							}
						}
						catch(Exception e)
						{
							continue;
						}
					}
					while(flag==false);				
				}
				else
				{
					Pass("No products displayed in shoppingBag");
				}			
			}
			else
			{
				Fail("Shopping Bag page not displayed");
			}		
		}
		else
		{
			Fail("Shopping Bag Icon not displayed");
		}
	}	
	
	//HUDSONBAY-1941 Verify that on tapping 'X' close icon the added product should be removed from the 'My Wishlist' page
	public void MyWishlistRemoveProduct() throws Exception
	{
		ChildCreation("HUDSONBAY-1941 Verify that on tapping 'X' close icon the added product should be removed from the 'My Wishlist' page");
		Actions action = new Actions(driver);
		try
		{
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{
					try
					{
						action.moveToElement(hamburger).click().build().perform();
						if(HBCBasicfeature.isElementPresent(pancakeStaticWishlist))
						{
							flag = true;
							break;
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
				while(flag!=true);					
				if(HBCBasicfeature.isElementPresent(pancakeStaticWishlist))
				{
					HBCBasicfeature.scrolldown(pancakeStaticWishlist, driver);
					pancakeStaticWishlist.click();														
					wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
					if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
					{
						int sizee = pdpWishListProductList.size();
						int i=0;
						do
						{
							i=1;
							WebElement remove = driver.findElement(By.xpath("(//*[@class='pro_list']//*[@class='removewish'])["+i+"]"));
							remove.click();
							Thread.sleep(3000);
							wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
							sizee = pdpWishListProductList.size();
						}
						while(sizee!=0);
						if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
						{
							Fail("Products not removed from Wishlist successfully on tapping 'X' close icon");
						}
						else
						{
							Pass("Products removed from Wishlist successfully on tapping 'X' close icon");
						}
					}
					else
					{
						Pass("Products not displayed in Mywishlist page");
					}								
				}
				else
				{
					Fail("Pancake WelcomesignIn option not displayed");
				}
			}
			else
			{
				Fail("Hamburger menuu not displayed");
			}	
		}
		catch(Exception e)
		{
			System.out.println("HBC-1941" +e.getMessage());
			Exception("HBC-1941 There is something wrong. Please Check." + e.getMessage());
		}
	}
	
	/*SignIn page to MyAccount-MyWishlist Page*/
	public void signin() throws java.lang.Exception
	{
		ChildCreation("SignIn page to MyWishlist Page");
		String email = HBCBasicfeature.getExcelVal("login", sheet, 3);
		String pwd = HBCBasicfeature.getExcelVal("login", sheet, 1);
		try
		{
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{					
					try
					{						
						do
						{
							try
							{
								HBCBasicfeature.jsclick(hamburger, driver);
								Thread.sleep(2000);
								if(HBCBasicfeature.isElementPresent(pancakeStaticWishlist))
								{
									flag = true;
									break;
								}
							}
							catch(Exception e)
							{
								continue;
							}
						}
						while(flag!=true);		
						if(HBCBasicfeature.isElementPresent(pancakeStaticWishlist))
						{
							flag = false;
							HBCBasicfeature.scrolldown(pancakeStaticWishlist, driver);
							pancakeStaticWishlist.click();
							try
							{
								wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
								flag = true;
								break;
							}
							catch(Exception e)
							{
								flag = false;
								continue;
							}
						}
						else
						{
							continue;
						}
					}
					catch(Exception e)
					{
						driver.navigate().refresh();
						continue;
					}
				}
				while(flag!=true);	
				wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
				if(HBCBasicfeature.isElementPresent(emailField))
				{	
					emailField.sendKeys(email);								
					log.add("Entered Email ID is: "+ email);
					if(HBCBasicfeature.isElementPresent(pwdField))
					{	
						pwdField.sendKeys(pwd);									
						log.add("Entered Password is: "+ pwd);
						if(HBCBasicfeature.isElementPresent(signInButton))
						{
							signInButton.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
							if(HBCBasicfeature.isElementPresent(WishListEmptyWish)||HBCBasicfeature.isElementPresent(myWishlistEmailWish))
							{
								Pass("My Wishlist page displayed",log);
								MyWishlist = true;
							}
							else
							{
								Fail("My Wishlist page not dispalyed",log);
							}									
						}
						else
						{
							Fail("SignIn button not displayed");
						}
					}
					else
					{
						Fail("Password Field not displayed");
					}
				}
				else
				{
					Fail("Email Field not displayed");
				}			
			}
			else
			{
				Fail("Hamburger menuu not displayed");
			}				
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}	
		
	/*HUDSONBAY-1932 Verify that "Your wish list is empty....." text should be shown when the Wishlist is empty*/
	public void MyWishlistEmptyText() throws Exception 
	{
		ChildCreation("HUDSONBAY-1932  Verify that 'Your wish list is empty.....' text should be shown when the Wishlist is empty");
		if(MyWishlist == true)
		{
			String expectedTxt = HBCBasicfeature.getExcelVal("HBC1932", sheet, 1);
			try
			{				
				if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
				{
					int size = pdpWishListProductList.size();
					if(size>0)
					{
						Fail("Product displayed in the wishlist Page");
					}
				}
				else
				{
					if(HBCBasicfeature.isElementPresent(WishListEmptyWish))
					{
						Pass("Wishlist is empty");
						String actualTxt = WishListEmptyWish.getText();
						log.add("Actual Empty wishlist text is: "+actualTxt);
						log.add("Expected Empty wishlist text is: "+expectedTxt);
						Thread.sleep(200);
						if(actualTxt.equalsIgnoreCase(expectedTxt))
						{
							Pass("'Your wish list is empty.....' text shown when the Wishlist is empty",log);
						}
						else
						{
							Fail("'Your wish list is empty.....' text not shown when the Wishlist is empty",log);
						}
					}
					else
					{
						Fail("Empty Wishlist text not displayed");
					}				
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1932" +e.getMessage());
				Exception("HBC-1932 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1933 Verify that "< Account" link should be shown in the left side of the page and it should be highlighted in blue color as per the creative*/
	/*HUDSONBAY-1938 Verify that "< My Account" text link should be shown in the left top corner of the page*/
	public void MyWishlistPageBackArrow() 
	{
		ChildCreation("HUDSONBAY-1933 Verify that '< Account' link should be shown in the left side of the page and it should be highlighted in blue color as per the creative");
		if(MyWishlist == true)
		{
			boolean HBC1938 = false;
			try
			{
				String expectedTxt = HBCBasicfeature.getExcelVal("HBC2005", sheet, 3);
				String expectedColor = HBCBasicfeature.getExcelVal("HBC2005", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					Pass("'< Account'link is displayed");	
					String actualTxt = myAccountpageBack.getText();
					String Color = myAccountpageBack.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(Color);
					log.add("The Actual text is: "+actualTxt);
					log.add("The Expected text is: "+expectedTxt);
					log.add("The Actual Color is: "+actualColor);
					log.add("The Expected Color is: "+expectedColor);					
					if(actualTxt.equalsIgnoreCase(expectedTxt)&&actualColor.equalsIgnoreCase(expectedColor))
					{
						Pass("'< Account' link shown in the left side of the page and it highlighted in blue color as per the creative",log);
						HBC1938 = true;
					}
					else
					{
						Fail("'< Account' link not shown in the left side of the page and not as per the creative",log);
					}				
				}
				else
				{
					Fail("'< Account' link not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1933" +e.getMessage());
				Exception("HBC-1933 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-1938 Verify that '< My Account' text link should be shown in the left top corner of the page");
			if(HBC1938 == true)
			{
				Pass("'< My Account' text link shown in the left top corner of the page");
			}
			else
			{
				Fail("'< My Account' text link not shown in the left top corner of the page");
			}
		}
		else
		{
			Skip("MyWishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1934 Verify that on tapping "< Account" link, it should navigates to "My Account" page*/
	/*HUDSONBAY-1939 Verify that on tapping "< Account" link, it should navigates to "My Account" page*/	
	public void MyWishlistBackArrowClick() 
	{
		ChildCreation("HUDSONBAY-1934 Verify that on tapping '< Account' link, it should navigates to 'My Account' page");
		if(MyWishlist == true)
		{
			boolean HBC1939 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					HBCBasicfeature.jsclick(myAccountpageBack, driver);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileTxt))
					{
						Pass("on tapping '< Account' link, navigates to 'My Account' page");
						HBC1939 = true;
						MyWishlist=false;				
					}
					else
					{
						Fail("on tapping '< Account' link, not navigates to 'My Account' page");
					}					
					if(HBCBasicfeature.isElementPresent(myAccountPageMyWishlist))
					{						
						HBCBasicfeature.jsclick(myAccountPageMyWishlist, driver);		
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
						if(HBCBasicfeature.isElementPresent(WishListEmptyWish)||HBCBasicfeature.isElementPresent(myWishlistEmailWish))
						{
							Pass("Navigated back to My Wishlist page ");
							MyWishlist = true;
						}
						else
						{
							Fail("My Wishlist page not dispalyed");
						}
					}
					else
					{
						Fail("My Wishlist section not displayed");
					}
				}
				else
				{
					Fail("'< Account' link not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1934" +e.getMessage());
				Exception("HBC-1934 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-1939 Verify that on tapping '< My Account' text link should navigate to 'My Account' page");
			if(HBC1939==true)
			{
				Pass("on tapping '< Account' link, navigates to 'My Account' page");
			}
			else
			{
				Fail("on tapping '< Account' link, navigates to 'My Account' page");
			}			
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1935 Verify that added products should be shown in the Wishlist page*/
	public void MyWishlistAddProducts() 
	{
		ChildCreation("HUDSONBAY-1935 Verify that added products should be shown in the Wishlist page");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
				{
					int size = pdpWishListProductList.size();
					if(size>0)
					{
						Pass("Product already displayed");
					}
				}
				else			
				{
					addProductToWishlist();
					Pass("Since no prducts in the wishlist page, Product now added successfully");
				}			
				
				if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
				{
					int size = pdpWishListProductList.size();
					if(size==1)
					{
						Pass("Product displayed in wishlist Page");
						if(HBCBasicfeature.isElementPresent(WishListProductTitle))
						{
							Thread.sleep(500);
							String wishProdTitle1 = WishListProductTitle.getText();
							String wishProdTitle2 = WishListProductTitle2.getText();
							log.add("Product Brand Title displayed in wishlist is: "+wishProdTitle1);
							log.add("Product Title displayed in wishlist is: "+wishProdTitle2);
							log.add("Selected Product to add wishlist is: "+PDPtitle);
							String title = wishProdTitle1 + " " + wishProdTitle2;
							Thread.sleep(500);
							if(wishProdTitle1.equalsIgnoreCase(PDPtitle)||wishProdTitle2.equalsIgnoreCase(PDPtitle)||title.equalsIgnoreCase(PDPtitle.trim()))
							{
								Pass("Added products shown in the Wishlist page",log);
							}
							else
							{
								Fail("Added products not shown in the Wishlist page",log);
							}							
						}
						else
						{
							Fail("Product Title is not displayed");
						}
					}
					else
					{
						Fail("No Products displayed in wishlist page "+size);
					}
				}
				else
				{
					Fail("No Products displayed in wishlist page");
				}						
			}
			catch (Exception e)
			{
				System.out.println("HBC-1935" +e.getMessage());
				Exception("HBC-1935 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1937 Verify that "My Wishlist" page should display "Product Image", "Product Title", "Brand", "Size", "Color", "Price", "Add to Cart" button, "Email your Wishlist" button, "Add All Items to Bag" button*/
	public void MyWishlistProductDetails() 
	{
		ChildCreation("HUDSONBAY-1937 Verify that 'My Wishlist' page should display 'Product Image', 'Product Title', 'Brand', 'Size', 'Color', 'Price', 'Add to Cart' button, 'Email your Wishlist' button, 'Add All Items to Bag' button");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
				{
					int size = pdpWishListProductList.size();
					if(size==1)
					{
						Pass("Product displayed in Wishlist");
						if(HBCBasicfeature.isElementPresent(WishListProductImage))
						{
							int resp = HBCBasicfeature.imageBroken(WishListProductImage, log);
							if(resp==200)
							{
								Pass("My Wishlist' page displays 'Product Image'");
							}
							else
							{
								Fail("'Product Image' not displayed");
							}
						}							
						else
						{
							Fail("'Product Image' not displayed");
						}						
						if(HBCBasicfeature.isElementPresent(WishListProductTitle))
						{
							Pass("'Product Title' diplayed in My Wishlist page is: " +WishListProductTitle.getText() );
						}
						else
						{
							Fail("'Product Title' not displayed in My Wishlist page");
						}
						if(HBCBasicfeature.isElementPresent(WishListProductTitle2))
						{
							Pass("'Product Brand' diplayed in My Wishlist page is: "+WishListProductTitle2.getText() );
						}
						else
						{
							Fail("'Product Brand' not displayed in My Wishlist page");
						}
						if(HBCBasicfeature.isElementPresent(WishListProductSize))
						{
							Pass("'Product size' diplayed in My Wishlist page is: "+WishListProductSize.getText() );
						}
						else
						{
							Fail("'Product Size' not displayed in My Wishlist page");
						}
						if(HBCBasicfeature.isElementPresent(WishListProductColor))
						{
							Pass("'Product color' diplayed in My Wishlist page is: "+WishListProductColor.getText() );
						}
						else
						{
							Fail("'Product color' not displayed in My Wishlist page");
						}
						if(HBCBasicfeature.isElementPresent(WishListProductPrice))
						{
							Pass("'Product price' diplayed in My Wishlist page is: "+WishListProductPrice.getText() );
						}
						else
						{
							Fail("'Product price' not displayed in My Wishlist page");
						}
						if(HBCBasicfeature.isElementPresent(WishListAddToCart))
						{
							Pass("'Add To Cart' button diplayed in My Wishlist page and the button name is: "+WishListAddToCart.getText() );
						}
						else
						{
							Fail("'Add To Cart' button not displayed in My Wishlist page");
						}
						if(HBCBasicfeature.isElementPresent(myWishlistEmailWish))
						{
							Pass("'Email your Wishlist' button diplayed in My Wishlist page and the button name is: "+myWishlistEmailWish.getText() );
						}
						else
						{
							Fail("'Email your Wishlist' not displayed in My Wishlist page");
						}
						if(HBCBasicfeature.isElementPresent(myWishlistAddAllItems))
						{
							Pass("'Add All Items to Bag' button diplayed in My Wishlist page and the button name is: "+myWishlistAddAllItems.getText() );
						}
						else
						{
							Fail("'Add All Items to Bag' not displayed in My Wishlist page");
						}						
					}
					else
					{
						Fail("More than one product displayed "+ size);
					}
				}
				else					
				{
					Fail("Product not displayed in wishlist page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1937" +e.getMessage());
				Exception("HBC-1937 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1940 Verify that "X" close icon should be shown in the top right corner of each product*/
	public void MyWishlistCloseIcon() 
	{
		ChildCreation("HUDSONBAY-1940 Verify that 'X' close icon should be shown in the top right corner of each product");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myWishlistCloseIcon))
				{
					Pass("'X' close icon shown in the top right corner of each product");
				}
				else
				{
					Fail("'X' close icon not shown in the top right corner of each product");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1940" +e.getMessage());
				Exception("HBC-1940 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1942 Verify that on tapping "Product Image" or "Product Title", the respective PDP should be shown*/
	public void MyWishlistToPDP() 
	{
		ChildCreation("HUDSONBAY-1942 Verify that on tapping 'Product Image' or 'Product Title', the respective PDP should be shown");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(WishListProductImage))
				{
					int resp = HBCBasicfeature.imageBroken(WishListProductImage, log);
					if(resp==200)
					{
						Pass("My Wishlist' page displays 'Product Image'");
						WishListProductImage.click();
						wait.until(ExpectedConditions.visibilityOf(PDPPage));
						if(HBCBasicfeature.isElementPresent(PDPPage))
						{
							String pdpProdID = pdpWebId.getText().substring(8);
							log.add("Product ID from PDP page is: "+pdpID);
							log.add("Current Product ID from PDP page is: "+pdpProdID);
							Thread.sleep(200);							
							if(pdpID.trim().equalsIgnoreCase(pdpProdID.trim()))
							{
								Pass("on tapping 'Product Image' the respective PDP page shown ",log);
								driver.navigate().back();
							}
							else
							{
								Fail("on tapping 'Product Image' the respective PDP page not shown",log);
								driver.navigate().back();
							}
						}
						else
						{
							Fail("PDP page not displayed");
						}
						
					}
					else
					{
						Fail("'Product Image' not displayed");
					}
				}							
				else
				{
					Fail("'Product Image' not displayed");
				}
				
				wait.until(ExpectedConditions.visibilityOf(WishListProductTitle));
				if(HBCBasicfeature.isElementPresent(WishListProductTitle))
				{					
					Pass("My Wishlist' page displays 'Product Title'");
					WishListProductTitle.click();
					wait.until(ExpectedConditions.visibilityOf(PDPPage));
					if(HBCBasicfeature.isElementPresent(PDPPage))
					{
						String pdpProdID = pdpWebId.getText().substring(8);
						log.add("Product ID from PLP page is: "+pdpID);
						log.add("Current Product ID from PDP page is: "+pdpProdID);
						Thread.sleep(200);							
						if(pdpID.trim().equalsIgnoreCase(pdpProdID.trim()))
						{
							Pass("on tapping 'Product Title' the respective PDP page shown ",log);
							driver.navigate().back();
							wait.until(ExpectedConditions.visibilityOf(WishListProductTitle));
						}
						else
						{
							Fail("on tapping 'Product Image' the respective PDP page not shown",log);
							driver.navigate().back();
							wait.until(ExpectedConditions.visibilityOf(WishListProductTitle));
						}
					}
					else
					{
						Fail("PDP page not displayed");
					}					
				}
				else
				{
					Fail("'Product Image' not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1942" +e.getMessage());
				Exception("HBC-1942 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1945 Verify that on tapping "Email Your Wishlist" button Email Your Wishlist page will be shown*/
	public void MyWishlistEmailYourWishlist() 
	{
		ChildCreation("HUDSONBAY-1945 Verify that on tapping 'Email Your Wishlist' button Email Your Wishlist page will be shown");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myWishlistEmailWish))
				{
					Pass("'Email your Wishlist' button diplayed");
					HBCBasicfeature.jsclick(myWishlistEmailWish, driver);
					wait.until(ExpectedConditions.visibilityOf(WishListEmailWishpage));
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpage))
					{
						Pass("on tapping 'Email Your Wishlist' button Email Your Wishlist page shown");
					}
					else
					{
						Fail("on tapping 'Email Your Wishlist' button Email Your Wishlist page not shown");
					}
				}
				else
				{
					Fail("'Email your Wishlist' not displayed in My Wishlist page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1945" +e.getMessage());
				Exception("HBC-1945 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1946 Verify that "Email Your Wishlist" page should contain "To:Email Address"field, "From:Your Name" field, "Additional Message" textbox with "Send Wish List" button*/
	public void MyWishlistEmailYourWishlistCreative() 
	{
		ChildCreation("HUDSONBAY-1946 Verify that 'Email Your Wishlist' page should contain 'To:Email Address' field, 'From:Your Name' field, 'Additional Message' textbox with 'Send Wish List' button");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(WishListEmailWishpage))
				{
					Pass("Email your wishlist page is displayed");
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpageToEmail))
					{
						Pass("'To:Email Address' field is displayed");
					}
					else
					{
						Fail("'To:Email Address' field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpageFromName))
					{
						Pass("'From:Your Name' field is displayed");
					}
					else
					{
						Fail("'From:Your Name' field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpageFromEmail))
					{
						Pass("'From: Email Address' field is displayed");
					}
					else
					{
						Fail("'From: Email Address' field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpageAdditionalMsg))
					{
						Pass("'Additional Message' field is displayed");
					}
					else
					{
						Fail("'Additional Message' field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpageSendButton))
					{
						Pass("'Send Wish List' button is displayed");
					}
					else
					{
						Fail("'Send Wish List' button is not displayed");
					}
				}
				else
				{
					Fail("Email your wishlist page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1946" +e.getMessage());
				Exception("HBC-1946 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1947 Verify that "From:Your Name" field should accept characters and special characters*/
	public void MyWishlistYourNameField() 
	{
		ChildCreation("HUDSONBAY-1947 Verify that 'From:Your Name' field should accept characters and special characters");
		if(MyWishlist == true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1947", sheet, 1);
				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageFromName))
				{
					Pass("'From:Your Name' field is displayed");
					WishListEmailWishpageFromName.clear();
					WishListEmailWishpageFromName.sendKeys(cellVal);
					log.add("The value read from excel file is:  "+cellVal.toString());
					if(WishListEmailWishpageFromName.getAttribute("value").isEmpty())
					{
						Fail("'From:Your Name' field is empty");
					}
					else  if(WishListEmailWishpageFromName.getAttribute("value").equalsIgnoreCase(cellVal))
					{
						Pass("'From:Your Name' field accept characters and special characters",log);
					}
					else
					{
						Fail("From:Your Name' field accept characters and special characters but not matches",log);
					}
					WishListEmailWishpageFromName.clear();

				}
				else
				{
					Fail("'From:Your Name' field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1947" +e.getMessage());
				Exception("HBC-1947 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1948 Verify that "From:Your Name" field should accept maxium of 30 characters*/
	public void MyWishlistYourNameFieldLimit() 
	{
		ChildCreation("HUDSONBAY-1948 Verify that 'From:Your Name' field should accept maxium of 30 characters");
		if(MyWishlist == true)
		{
			try
			{
				String cellVal[] = HBCBasicfeature.getExcelVal("HBC1948", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageFromName))
				{
					Pass("'From:Your Name' field is displayed");
					for(int i=0;i<cellVal.length;i++)
					{
						WishListEmailWishpageFromName.clear();
						log.add("The value read from excel file is:  "+cellVal[i].toString());
						WishListEmailWishpageFromName.sendKeys(cellVal[i]);
						Thread.sleep(500);
						if(WishListEmailWishpageFromName.getAttribute("value").isEmpty())
						{
							Fail("'From:Your Name' field is empty");
						}
						else  if(WishListEmailWishpageFromName.getAttribute("value").length()<=30)
						{
							Pass("'From:Your Name' field accept maxium of 30 characters",log);
						}
						else
						{
							Fail("From:Your Name' field accept characters but exceeding the desired limit",log);
						}
						WishListEmailWishpageFromName.clear();
					}
					
				}
				else
				{
					Fail("'From:Your Name' field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1948" +e.getMessage());
				Exception("HBC-1948 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1949 Verify that on tapping "Send Wish list" button without entering details in email and name field, alert needs to be shown as per the desktop site*/
	public void MyWishlistSendWishAlert() 
	{
		ChildCreation("HUDSONBAY-1949 Verify that on tapping 'Send Wish list' button without entering details in email and name field, alert needs to be shown as per the desktop site");
		if(MyWishlist == true)
		{
			try
			{
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1949", sheet, 1);
				String expectedNAlert = HBCBasicfeature.getExcelVal("HBC1949", sheet, 2);

				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageToEmail))
				{
					Pass("'To:Email Address' field is displayed");
					WishListEmailWishpageToEmail.clear();
					WishListEmailWishpageFromName.click();
					wait.until(ExpectedConditions.attributeContains(WishListEmailWishpageToEmailAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpageToEmailAlert))
					{
						Pass("on tapping 'Send Wish list' button without entering details in email field, alert shown ");
						String actualAlert = WishListEmailWishpageToEmailAlert.getText();
						log.add("Actual alert displayed is: "+actualAlert);
						log.add("Expected alert is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("Alert shown as per the desktop site",log);
						}
						else
						{
							Fail("Alert not shown as per the desktop site",log);
						}
					}
					else
					{
						Fail("Alert not displayed");
					}
				}
				else
				{
					Fail("'To:Email Address' field is not displayed");
				}
				
				
				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageFromName))
				{
					Pass("'To:Your name' field is displayed");
					WishListEmailWishpageFromName.clear();
					WishListEmailWishpageToEmail.click();
					wait.until(ExpectedConditions.attributeContains(WishListEmailWishpageFromNameAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpageFromNameAlert))
					{
						Pass("on tapping 'Send Wish list' button without entering details in To:Your name field, alert shown ");
						String actualAlert = WishListEmailWishpageFromNameAlert.getText();
						log.add("Actual alert displayed is: "+actualAlert);
						log.add("Expected alert is: "+expectedNAlert);
						if(actualAlert.equalsIgnoreCase(expectedNAlert))
						{
							Pass("Alert shown as per the desktop site",log);
						}
						else
						{
							Fail("Alert not shown as per the desktop site",log);
						}
					}
					else
					{
						Fail("Alert not displayed");
					}
				}
				else
				{
					Fail("'To:Your name' field is not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1949" +e.getMessage());
				Exception("HBC-1949 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1951 Verify that "Additional Message" field should accept characters, numbers and special characters*/
	public void MyWishlistAdditionalMessageField() 
	{
		ChildCreation("HUDSONBAY-1951 Verify that 'Additional Message' field should accept characters, numbers and special characters");
		if(MyWishlist == true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1947", sheet, 1);
				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageAdditionalMsg))
				{
					Pass("'Additional Message' field is displayed");
					WishListEmailWishpageAdditionalMsg.clear();
					WishListEmailWishpageAdditionalMsg.sendKeys(cellVal);
					log.add("The value read from excel file is:  "+cellVal.toString());
					if(WishListEmailWishpageAdditionalMsg.getAttribute("value").isEmpty())
					{
						Fail("'Additional Message' field is empty");
					}
					else  if(WishListEmailWishpageAdditionalMsg.getAttribute("value").equalsIgnoreCase(cellVal))
					{
						Pass("'Additional Message' field accept characters, numbers and special characters",log);
					}
					else
					{
						Fail("Additional Message field accept characters and special characters, numbers but not matches",log);
					}
					WishListEmailWishpageAdditionalMsg.clear();
				}
				else
				{
					Fail("'From:Your Name' field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1951" +e.getMessage());
				Exception("HBC-1951 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}

	/*HUDSONBAY-1952 Verify that "Additional Message" field should accept maxium of 140 characters*/
	public void MyWishlistAdditionalMessageFieldLimit() 
	{
		ChildCreation("HUDSONBAY-1952 Verify that 'Additional Message' field should accept maxium of 140 characters");
		if(MyWishlist == true)
		{
			try
			{
				String cellVal[] = HBCBasicfeature.getExcelVal("HBC1952", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageAdditionalMsg))
				{
					Pass("'From:Your Name' field is displayed");
					for(int i=0;i<cellVal.length;i++)
					{
						WishListEmailWishpageAdditionalMsg.clear();
						log.add("The value read from excel file is:  "+cellVal[i].toString());
						WishListEmailWishpageAdditionalMsg.sendKeys(cellVal[i]);
						Thread.sleep(500);
						if(WishListEmailWishpageAdditionalMsg.getAttribute("value").isEmpty())
						{
							Fail("'Additional Message' field is empty");
						}
						else  if(WishListEmailWishpageAdditionalMsg.getAttribute("value").length()<=140)
						{
							Pass("'Additional Message' field accept maxium of 140 characters",log);
						}
						else
						{
							Fail("Additional Message field accept characters but exceeding the desired limit",log);
						}
						WishListEmailWishpageAdditionalMsg.clear();
					}					
				}
				else
				{
					Fail("'Additional Message' field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1952" +e.getMessage());
				Exception("HBC-1952 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1953 Verify that "X" close icon should be shown in the top right corner of the page*/
	public void MyWishlistEmailWishCloseIcon() 
	{
		ChildCreation("HUDSONBAY-1953 Verify that 'X' close icon should be shown in the top right corner of the page");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageclose))
				{
					Pass("'X' close icon shown in the top right corner of the page");
				}
				else
				{
					Fail("'X' close icon not shown in the top right corner of the page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1953" +e.getMessage());
				Exception("HBC-1953 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1950 Verify that email should be send successfully and no alert should be shown on leaving the "Additional Message" textbox empty*/
	public void MyWishlistEmailSendSuccess() 
	{
		ChildCreation("HUDSONBAY-1950 Verify that email should be send successfully and no alert should be shown on leaving the 'Additional Message' textbox empty");
		if(MyWishlist == true)
		{
			try
			{
				String emailId = HBCBasicfeature.getExcelVal("HBC1950", sheet, 1);
				String userName = HBCBasicfeature.getExcelVal("HBC1950", sheet, 2);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1950", sheet, 3);

				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageToEmail)&&HBCBasicfeature.isElementPresent(WishListEmailWishpageFromName))
				{
					Pass("'To:Email Address' & 'From: Your Name' field is displayed");
					WishListEmailWishpageToEmail.clear();
					WishListEmailWishpageToEmail.sendKeys(emailId);
					log.add("The value read from excel file is:  "+emailId.toString());
					WishListEmailWishpageFromName.clear();
					WishListEmailWishpageFromName.sendKeys(userName);
					log.add("The value read from excel file is:  "+userName.toString());
					if(HBCBasicfeature.isElementPresent(WishListEmailWishpageAdditionalMsg))
					{
						WishListEmailWishpageAdditionalMsg.clear();
						Thread.sleep(200);
						boolean res = WishListEmailWishpageAdditionalMsg.getAttribute("value").isEmpty();
						if(res)
						{
							Pass("'Additional Message' textbox is empty");
							if(HBCBasicfeature.isElementPresent(WishListEmailWishpageSendButton))
							{
								Pass("'Send Wish List' button is displayed");
								HBCBasicfeature.jsclick(WishListEmailWishpageSendButton, driver);
								wait.until(ExpectedConditions.visibilityOf(WishListEmailWishpageSendEmailAlert));
								if(HBCBasicfeature.isElementPresent(WishListEmailWishpageSendEmailAlert))
								{
									Pass("Email sent successfully and alert is displayed",log);
									String actualAlert = WishListEmailWishpageSendEmailAlert.getText();
									log.add("Actual alert displayed is: "+actualAlert);
									log.add("Expected alert is: "+expectedAlert);
									Thread.sleep(200);
									if(actualAlert.equalsIgnoreCase(expectedAlert))
									{
										Pass("Email send successfully and expected alert is displayed",log);
									}
									else
									{
										Fail("Email send successfully and expected alert is not matches",log);
									}
								}
								else
								{
									Fail("Email send success alert is not displayed");
								}
							}
							else
							{
								Fail("'Send Wish List' button is not displayed");
							}
						}
						else
						{
							Fail("'Additional Message' textbox is not empty");
						}
					}
					else
					{
						Fail("'Additional Message' textbox is not displayed");
					}
				}
				else
				{
					Fail("'To:Email Address' field & 'From: Your Name' is not displayed");					
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1950" +e.getMessage());
				Exception("HBC-1950 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}

	/*HUDSONBAY-1954 Verify that on tapping the "X" close icon the "Email Your Wishlist" page should be closed*/
	public void MyWishlistEmailWishCloseIconClick() 
	{
		ChildCreation("HUDSONBAY-1954 Verify that on tapping the 'X' close icon the 'Email Your Wishlist' page should be closed");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(WishListEmailWishpageclose))
				{
					Pass("'X' close icon shown in the top right corner of the page");
					HBCBasicfeature.jsclick(WishListEmailWishpageclose, driver);
					Thread.sleep(1000);
					if(!HBCBasicfeature.isElementPresent(WishListEmailWishpage))
					{
						Pass("on tapping the 'X' close icon the 'Email Your Wishlist' page closed");
					}
					else
					{
						Fail("on tapping the 'X' close icon the 'Email Your Wishlist' page not closed");
					}
				}
				else
				{
					Fail("'X' close icon not shown in the top right corner of the page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1954" +e.getMessage());
				Exception("HBC-1954 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}

	/*HUDSONBAY-1941 Verify that on tapping "X" close icon the added product should be removed from the "My Wishlist" page
	public void MyWishlistRemoveProduct() 
	{
		ChildCreation("HUDSONBAY-1941 Verify that on tapping 'X' close icon the added product should be removed from the 'My Wishlist' page");
		if(MyWishlist == true)
		{
			try
			{
				if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
				{
					int size = pdpWishListProductList.size();
					if(size>0)
					{
						Pass("Product displayed in wishlist Page");
						if(HBCBasicfeature.isElementPresent(myWishlistCloseIcon))
						{
							Pass(" X Close Icon displayed");
							HBCBasicfeature.jsclick(myWishlistCloseIcon, driver);
							wait.until(ExpectedConditions.visibilityOf(WishListEmptyWish));
							if(HBCBasicfeature.isElementPresent(WishListEmptyWish))
							{
								Pass("on tapping 'X' close icon the added product removed from the 'My Wishlist' page");
							}
							else
							{
								Fail("on tapping 'X' close icon the added product not removed from the 'My Wishlist' page");
							}
						}
						else
						{
							Fail("Remove button not displayed");
						}						
					}
					else
					{
						Fail("No products displayed");
					}
				}
				else
				{
					Fail("Product not displayed in wishlist Page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1941" +e.getMessage());
				Exception("HBC-1941 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	*/
	/*HUDSONBAY-1943 Verify that on tapping "Add to Cart" button, the respective product will be added to the cart successfully*/
	public void MyWishlistAddToCart() throws Exception 
	{
		ChildCreation("HUDSONBAY-1943 Verify that on tapping 'Add to Cart' button, the respective product will be added to the cart successfully");
		if(MyWishlist == true)
		{
			String expectedAlert = HBCBasicfeature.getExcelVal("HBC1943", sheet, 1);
			String expectedAlertt = HBCBasicfeature.getExcelVal("HBC1943", sheet, 2);
			try
			{
				if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
				{
					int size = pdpWishListProductList.size();
					if(size>0)
					{
						Pass("Product already displayed in wishlist page");
					}
				}
				else			
				{
					addProductToWishlist();
					Pass("Since no prducts in the wishlist page, Product now added successfully");
				}			
				
				if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
				{
					int size = pdpWishListProductList.size();
					if(size==1)
					{
						Pass("Product displayed in wishlist Page");
						if(HBCBasicfeature.isElementPresent(WishListProductTitle))
						{
							Thread.sleep(500);
							String wishProdTitle1 = WishListProductTitle.getText();
							String wishProdTitle2 = WishListProductTitle2.getText();
							if(HBCBasicfeature.isElementPresent(WishListAddToCart))
							{								
								Pass("AddToCart button is displayed");
								String beforeCount = bagIconCount.getText();
								WishListAddToCart.click();
								Thread.sleep(500);
								wait.until(ExpectedConditions.visibilityOf(WishListEmailWishpageSendEmailAlert));
								if(HBCBasicfeature.isElementPresent(WishListEmailWishpageSendEmailAlert))
								{
									String actualAlert = WishListEmailWishpageSendEmailAlert.getText();
									log.add("The actual alert is: "+actualAlert);
									log.add("The expected alert is: "+expectedAlert);
									if(actualAlert.equalsIgnoreCase(expectedAlert)||actualAlert.equalsIgnoreCase(expectedAlertt))
									{
										Pass("Item successfully added to cart alert displayed",log);
										String afterCount = bagIconCount.getText();
										log.add("Before shopping Bag count is: "+beforeCount);
										log.add("After shopping Bag count is: "+afterCount);
										if(beforeCount==afterCount)
										{
											Fail("Shopping Bag count not changed after add the item to cart",log);
										}
										else
										{
											Pass("Shopping Bag count changed after add the item to cart",log);
											if(HBCBasicfeature.isElementPresent(bagIcon))
											{
												Pass("Shopping bag Icon displayed");
												HBCBasicfeature.jsclick(bagIcon, driver);
												wait.until(ExpectedConditions.attributeContains(shoppingBagPageDom, "style", "visible"));
												if(HBCBasicfeature.isElementPresent(shoppingBagPageDom))
												{
													Pass("Shopping Bag page displayed");
													if(HBCBasicfeature.isElementPresent(shoppingBagPageProdTitle))
													{
														String prodTitle = shoppingBagPageProdTitle.getText();
														log.add("Product Title from shoppingBag Page is: "+prodTitle);														
														log.add("Product Brand Title displayed in wishlist is: "+wishProdTitle1);
														log.add("Product Title displayed in wishlist is: "+wishProdTitle2);
														Thread.sleep(500);
														if(wishProdTitle1.equalsIgnoreCase(prodTitle)||wishProdTitle2.equalsIgnoreCase(prodTitle))
														{
															Pass(" on tapping 'Add to Cart' button, the respective product added to the cart successfully",log);	
															removeProdFromBag();
														}
														else
														{
															Fail("on tapping 'Add to Cart' button, the respective product not added to the cart successfully",log);
															removeProdFromBag();
														}
													}
													else
													{
														Fail("shopping bagpage product title is not displayed");
													}														
												}
												else
												{
													Fail("Shopping Bag page not displayed");
												}												
											}
											else
											{
												Fail("Shopping bag Icon not displayed");
											}
										}									
									}
									else
									{
										Fail("Respective alert not displayed",log);
									}									
								}
								else
								{
									Fail("Added to cart success alert is not displayed");
								}
							}
							else
							{
								Fail("Add To Cart button not displayed");
							}
						}
						else
						{
							Fail("Wishlist product title not displayed");
						}
					}
					else
					{
						Fail("More than one product displayed");
					}
				}
				else
				{
					Fail("Wishlist porduct list is not displayed");
				}							
			}
			catch (Exception e)
			{
				System.out.println("HBC-1943" +e.getMessage());
				Exception("HBC-1943 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	/*HUDSONBAY-1944 Verify that on tapping "Add All Items to Bag" button, all the products in the "My Wishlist" page will be added to the cart successfully*/
	public void MyWishlistAddAllItemsToBag() throws Exception 
	{
		ChildCreation("HUDSONBAY-1944 Verify that on tapping 'Add All Items to Bag' button, all the products in the 'My Wishlist' page will be added to the cart successfully");
		if(MyWishlist == true)
		{
			Actions action = new Actions(driver);
			String expectedAlert = HBCBasicfeature.getExcelVal("HBC1943", sheet, 1);		
			String expectedAlertt = HBCBasicfeature.getExcelVal("HBC1943", sheet, 2);
			try
			{
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					boolean flag = false;
					do
					{
						try
						{
							action.moveToElement(hamburger).click().build().perform();
							if(HBCBasicfeature.isElementPresent(pancakeStaticWishlist))
							{
								flag = true;
								break;
							}
						}
						catch(Exception e)
						{
							continue;
						}
					}
					while(flag!=true);					
					if(HBCBasicfeature.isElementPresent(pancakeStaticWishlist))
					{
						HBCBasicfeature.scrolldown(pancakeStaticWishlist, driver);
						pancakeStaticWishlist.click();														
						wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
						if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
						{
							int size = pdpWishListProductList.size();
							if(size>0)
							{
								Pass("Product already displayed in wishlist page");
							}
						}
						else			
						{
							addProductToWishlist();
							Pass("Since no prducts in the wishlist page, Product now added successfully");
						}
						Thread.sleep(500);
						if(HBCBasicfeature.isListElementPresent(pdpWishListProductList))
						{
							int size = pdpWishListProductList.size();
							if(size==1)
							{
								Pass("Product displayed in wishlist Page");
								if(HBCBasicfeature.isElementPresent(WishListProductTitle))
								{
									Thread.sleep(500);
									String wishProdTitle1 = WishListProductTitle.getText();
									String wishProdTitle2 = WishListProductTitle2.getText();	
									if(HBCBasicfeature.isElementPresent(myWishlistAddAllItems))
									{								
										Pass("'Add All Items to Bag' button is displayed");
										String beforeCount = bagIconCount.getText();
										myWishlistAddAllItems.click();
										Thread.sleep(500);
										wait.until(ExpectedConditions.visibilityOf(WishListEmailWishpageSendEmailAlert));
										if(HBCBasicfeature.isElementPresent(WishListEmailWishpageSendEmailAlert))
										{
											String actualAlert = WishListEmailWishpageSendEmailAlert.getText();
											log.add("The actual alert is: "+actualAlert);
											log.add("The expected alert is: "+expectedAlert);
											if(actualAlert.equalsIgnoreCase(expectedAlert)&&actualAlert.equalsIgnoreCase( expectedAlertt))
											{
												Pass("All Items successfully added to cart alert displayed",log);
												String afterCount = bagIconCount.getText();
												log.add("Before shopping Bag count is: "+beforeCount);
												log.add("After shopping Bag count is: "+afterCount);
												if(beforeCount==afterCount)
												{
													Fail("Shopping Bag count not changed after add the item to cart",log);
												}
												else
												{
													Pass("Shopping Bag count changed after add the item to cart",log);
													if(HBCBasicfeature.isElementPresent(bagIcon))
													{
														Pass("Shopping bag Icon displayed");
														HBCBasicfeature.jsclick(bagIcon, driver);
														wait.until(ExpectedConditions.attributeContains(shoppingBagPageDom, "style", "visible"));
														if(HBCBasicfeature.isElementPresent(shoppingBagPageDom))
														{
															Pass("Shopping Bag page displayed");
															if(HBCBasicfeature.isElementPresent(shoppingBagPageProdTitle))
															{
																String prodTitle = shoppingBagPageProdTitle.getText();
																log.add("Product Title from shoppingBag Page is: "+prodTitle);														
																log.add("Product Brand Title displayed in wishlist is: "+wishProdTitle1);
																log.add("Product Title displayed in wishlist is: "+wishProdTitle2);
																Thread.sleep(500);
																if(wishProdTitle1.equalsIgnoreCase(prodTitle)||wishProdTitle2.equalsIgnoreCase(prodTitle))
																{
																	Pass(" on tapping 'Add All Items to Bag' button, all the product added to the cart successfully",log);	
																}
																else
																{
																	Fail("on tapping 'Add All Items to Bag' button, all the product not added to the cart successfully",log);
																}
															}
															else
															{
																Fail("shopping bagpage product title is not displayed");
															}														
														}
														else
														{
															Fail("Shopping Bag page not displayed");
														}												
													}
													else
													{
														Fail("Shopping bag Icon not displayed");
													}
												}									
											}
											else
											{
												Fail("Respective alert not displayed",log);
											}									
										}
										else
										{
											Fail("Added All Items to Bag alert is not displayed");
										}
									}
									else
									{
										Fail("Add All Items to Bag button not displayed");
									}
								}
								else
								{
									Fail("Wishlist product title not displayed");
								}
							}
							else
							{
								Fail("More than one product displayed");
							}
						}
						else
						{
							Fail("Wishlist porduct list is not displayed");
						}							
					}
					else
					{
						Fail("Wishlist option not dispalyed");
					}
				}
				else
				{
					Fail("Hamburger Menu not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1944" +e.getMessage());
				Exception("HBC-1944 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Wishlist Page not displayed");
		}
	}
	
	

}

