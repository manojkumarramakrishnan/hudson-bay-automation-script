package hbPageObjects;

import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_BrandLanding;

public class Hudson_Bay_BrandLanding_Obj extends Hudson_Bay_BrandLanding implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_BrandLanding_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
			//Footer:
			@FindBy(xpath = ""+signupEmailss+"")
			WebElement signupEmails;
		
			@FindBy(xpath = ""+footerContainerr+"")
			WebElement footerContainer;	
			
			@FindBy(xpath = ""+footerTopp+"")
			WebElement footerTop;	
			
			@FindBy(xpath = ""+footerTopTitlee+"")
			WebElement footerTopTitle;
			
			@FindBy(xpath = ""+footerSubTopp+"")
			WebElement footerSubTop;
			
			@FindBy(xpath = ""+footerSubTopCalll+"")
			WebElement footerSubTopCall;
			
			@FindBy(xpath = ""+footerSubTopEmaill+"")
			WebElement footerSubTopEmail;
			
			@FindBy(xpath = ""+footerMenuListt+"")
			WebElement footerMenuList;
			
			@FindBy(xpath = ""+footerBottomm+"")
			WebElement footerBottom;
			
			@FindBy(xpath = ""+footerSocialIconn+"")
			WebElement footerSocialIcon;
			
			@FindBy(xpath = ""+footerBottomCopyrightt+"")
			WebElement footerBottomCopyright;
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+homepageDomm+"")
	WebElement homepageDom;	
	
	@FindBy(xpath = ""+maskk+"")
	WebElement mask;
	
	@FindBy(xpath = ""+brandPagee+"")
	WebElement brandPage;
	
	@FindBy(xpath = ""+brandPageTitlee+"")
	WebElement brandPageTitle;
	
	@FindBy(xpath = ""+brandPagedropdownn+"")
	WebElement brandPagedropdown;
	
	@FindBy(xpath = ""+brandPageAlphaSectionn+"")
	WebElement brandPageAlphaSection;
		
	@FindBy(xpath = ""+brandPageNameSectionn+"")
	WebElement brandPageNameSection;	
	
	@FindBy(xpath = ""+brandpageAlphabetsActivee+"")
	WebElement brandpageAlphabetsActive;	
	
	@FindBy(xpath = ""+brandPageHashh+"")
	WebElement brandPageHash;	
	
		
	@FindBy(how = How.XPATH,using = ""+brandpageAlphabetUnavaill+"")
	List<WebElement> brandpageAlphabetUnavail;	
	
	@FindBy(how = How.XPATH,using = ""+brandpageDropDownSelectionn+"")
	List<WebElement> brandpageDropDownSelection;	
	
	@FindBy(how = How.XPATH,using = ""+pancakeMenulistt+"")
	List<WebElement> pancakeMenulist;	
	
	@FindBy(how = How.XPATH,using = ""+brandpageAlphabetss+"")
	List<WebElement> brandpageAlphabets;	
	
	boolean brandLandPage = false;
		
	/*HUDSONBAY-3035 Verify that Brands option should be shown in the Pancake menu*/
	public void BrandOption() throws Exception 
	{
		ChildCreation("HUDSONBAY-3035 Verify that Brands option should be shown in the Pancake menu");		
		String Expected = HBCBasicfeature.getExcelVal("HB3035", sheet, 1);
		try
		{
			wait.until(ExpectedConditions.visibilityOf(homepageDom));
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				HBCBasicfeature.jsclick(hamburger, driver);
				wait.until(ExpectedConditions.visibilityOf(mask));
				if(HBCBasicfeature.isListElementPresent(pancakeMenulist))
				{
					int size = pancakeMenulist.size();
					for(int i=0;i<size;i++)
					{
						String pancakeMenu = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@class='sk_mobCategoryItemTxt']")).getText();
						Thread.sleep(200);
						if(pancakeMenu.equalsIgnoreCase(Expected))
						{
							log.add("Expected Pancake menu is: "+Expected);
							log.add("Actual pancake menu is: "+pancakeMenu);
							Pass(" Brands option shown in the Pancake menu",log);
							break;
						}
						else
						{
							continue;
						}
					}
				}
				else
				{
					Fail("Pancake menu list not displayed");
				}
			}
			else
			{
				Fail("Hamburger menu not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println("HBC-3035" +e.getMessage());
			Exception("HBC-3035 There is something wrong. Please Check." + e.getMessage());
		}		
	}
	
	/*HUDSONBAY-3036 Verify that on tapping the Brands option in the Pancake menu, the Brand Landing Page should be shown*/
	public void BrandPage() throws Exception 
	{
		ChildCreation("HUDSONBAY-3036 Verify that on tapping the Brands option in the Pancake menu, the Brand Landing Page should be shown");		
		String Expected = HBCBasicfeature.getExcelVal("HB3035", sheet, 1);
		try
		{
			wait.until(ExpectedConditions.visibilityOf(mask));
			if(HBCBasicfeature.isListElementPresent(pancakeMenulist))
			{
				int size = pancakeMenulist.size();
				for(int i=0;i<size;i++)
				{
					String pancakeMenu = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@class='sk_mobCategoryItemTxt']")).getText();
					Thread.sleep(200);
					if(pancakeMenu.equalsIgnoreCase(Expected))
					{
						Pass("Brands option displayed in pancake menu");
						WebElement selectedCategory = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
						if(HBCBasicfeature.isElementPresent(selectedCategory))
						{
							selectedCategory.click();
							wait.until(ExpectedConditions.attributeContains(brandPage, "style", "100%"));
							if(HBCBasicfeature.isElementPresent(brandPage))
							{
								Pass("On tapping the Brands option in the Pancake menu, the Brand Landing Page shown");
								brandLandPage = true;
								break;
							}
							else
							{
								Fail("On tapping the Brands option in the Pancake menu, the Brand Landing Page not shown");
							}
						}
						else
						{
							Fail("Selected category not displayed");
						}
					}
					else
					{
						Fail("Brands menu not displayed");
					}
				}
			}
			else
			{
				Fail("Pancake menu list not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println("HBC-3036" +e.getMessage());
			Exception("HBC-3036 There is something wrong. Please Check." + e.getMessage());
		}		
	}
	
	/*HUDSONBAY-3037 Verify that Brand Landing Page should be shown as per the creative*/
	public void BrandpageCreative() 
	{
		ChildCreation("HUDSONBAY-3037 Verify that Brand Landing Page should be shown as per the creative");
		if(brandLandPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(brandPageTitle))
				{
					Pass("Brand Landing Page title displayed");
				}
				else
				{
					Fail("Brand Landing Page title not displayed");
				}
				if(HBCBasicfeature.isElementPresent(brandPagedropdown))
				{
					Pass("Brand Landing Page DropDown displayed");
				}
				else
				{
					Fail("Brand Landing Page DropDown not displayed");
				}
				if(HBCBasicfeature.isElementPresent(brandPageAlphaSection))
				{
					Pass("Brand Landing Page Brand Alphates selection section displayed");
				}
				else
				{
					Fail("Brand Landing Page Brand Alphates selection section not displayed");
				}
				if(HBCBasicfeature.isElementPresent(brandPageNameSection))
				{
					Pass("Brand Landing Page Brand names section displayed");
				}
				else
				{
					Fail("Brand Landing Page Brand names section not displayed");
				}
				if(HBCBasicfeature.isElementPresent(footerTop))
				{
					Pass("Footer top panel is displayed");	
					wait.until(ExpectedConditions.visibilityOf(footerTopTitle));
					if(HBCBasicfeature.isElementPresent(footerTopTitle))
					{
						Pass("Footer top panel title is displayed");	
					}
					else
					{
						Fail("Footer top panel title is not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerSubTop));
					if(HBCBasicfeature.isElementPresent(footerSubTop))
					{
						Pass("Footer sub top panel is displayed");	
						wait.until(ExpectedConditions.visibilityOf(footerSubTopCall));
						if(HBCBasicfeature.isElementPresent(footerSubTopCall))
						{
							Pass("Footer call option is displayed");		
						}
						else
						{
							Fail("Footer Call option is not displayed");						
						}
						wait.until(ExpectedConditions.visibilityOf(footerSubTopEmail));
						if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
						{
							Pass("Footer Email option is displayed");	
						}
						else
						{
							Fail("Footer email option is not displayed");						
						}
					}
					else
					{
						Fail("Footer sub top panel is not displayed");						
					}
				}
				else
				{
					Fail("Footer top panel is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					Pass("Footer Menu List option is displayed");
				}
				else
				{
					Fail("Footer menu list is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerBottom));
				if(HBCBasicfeature.isElementPresent(footerBottom))
				{
					Pass("Footer bottom panel is displayed");
					wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
					if(HBCBasicfeature.isElementPresent(footerSocialIcon))
					{
						Pass("Footer social icon options are displayed");
					}
					else
					{
						Fail("Footer social icon options are not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{
						log.add("Footer displayed as per the creative and fit to the screen");
						Pass("Footer copyright section is displayed",log);										
					}
					else
					{
						log.add("Footer is not displayed as per the creative and fit to the screen");
						Fail("Footer copyright section is not displayed",log);	
					}
				}
				else
				{
					Fail("Footer bottom panel is not displayed");						
				}			
			}
			catch (Exception e)
			{
				System.out.println("HBC-3037" +e.getMessage());
				Exception("HBC-3037 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-3038 Verify that Dropdown should be shown below "SHOP OUR ONLINE BRANDS" title*/
	public void BrandPageDropdown() 
	{
		ChildCreation("HUDSONBAY-3038 Verify that Dropdown should be shown below 'SHOP OUR ONLINE BRANDS' title");
		if(brandLandPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(brandPagedropdown))
				{
					Pass("Brand Landing Page DropDown displayed");					
				}
				else
				{
					Fail("Brand Landing Page DropDown not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-3038" +e.getMessage());
				Exception("HBC-3038 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-3040 Verify that "Select Category" option should be shown by default in the dropdown menu*/
	public void BrandPageDropdownDefault() throws Exception 
	{
		ChildCreation("HUDSONBAY-3040 Verify that 'Select Category' option should be shown by default in the dropdown menu");
		if(brandLandPage == true)
		{
			String[] expected = HBCBasicfeature.getExcelVal("HBC3038", sheet, 1).split("\n");
			try
			{
				if(HBCBasicfeature.isElementPresent(brandPagedropdown))
				{
					Pass("Brand Landing Page DropDown displayed");
					Select sel = new Select(brandPagedropdown);
					String option = sel.getFirstSelectedOption().getText();
					log.add("Default selected option is: "+option);
					log.add("Expected selected option is: "+expected[0]);
					Thread.sleep(200);
					if(option.equalsIgnoreCase(expected[0]))
					{
						Pass("'Select Category' option shown by default in the dropdown menu",log);						
					}
					else
					{
						Fail("'Select Category' option not shown by default in the dropdown menu",log);						
					}
				}
				else
				{
					Fail("Brand Landing Page DropDown not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-3040" +e.getMessage());
				Exception("HBC-3040 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-3039 Verify that all the required options should be shown in the Dropdown Menu*/
	public void BrandDropDownOptions() throws Exception 
	{
		ChildCreation("HUDSONBAY-3039 Verify that all the required options should be shown in the Dropdown Menu");
		if(brandLandPage == true)
		{
			String[] expected = HBCBasicfeature.getExcelVal("HBC3038", sheet, 1).split("\n");
			try
			{
				if(HBCBasicfeature.isElementPresent(brandPagedropdown))
				{
					Pass("Brand Landing Page DropDown displayed");
					Select sel = new Select(brandPagedropdown);
					int size = sel.getOptions().size();
					for(int i=0;i<size;i++)
					{
						String option = sel.getOptions().get(i).getText();
						for(int j=0;j<=expected.length;j++)
						{
							if(option.equalsIgnoreCase(expected[j]))
							{
								Pass(option+ " option shown in the Dropdown Menu");
								break;
							}
							else
							{
								continue;
							}
						}
					}
				}
				else
				{
					Fail("Brand Landing Page DropDown not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-3039" +e.getMessage());
				Exception("HBC-3039 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
		
	/*HUDSONBAY-3041 Verify that on tapping the dropdown menu, the dropdown menu should be enabled with all the required options*/
	public void BrandDropDownClick() 
	{
		ChildCreation("HUDSONBAY-3041 Verify that on tapping the dropdown menu, the dropdown menu should be enabled with all the required options");
		if(brandLandPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(brandPagedropdown))
				{
					brandPagedropdown.click();
					Pass("on tapping the dropdown menu, the dropdown menu enabled with all the required options");
					Thread.sleep(500);
					brandPagedropdown.click();
				}
				else
				{
					Fail("Dropdown menu not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-3041" +e.getMessage());
				Exception("HBC-3041 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-3045 Verify that on selecting the options in dropdown, the respective brands alphabets should be shown*/
	public void BrandDropDownSelection() 
	{
		ChildCreation("HUDSONBAY-3045 Verify that on selecting the options in dropdown, the respective brands alphabets should be shown");
		if(brandLandPage == true)
		{
			Random r = new Random();
			int i = 0;
			try
			{
				if(HBCBasicfeature.isListElementPresent(brandpageDropDownSelection))
				{
					int bfreSize = brandpageDropDownSelection.size();
					if(HBCBasicfeature.isElementPresent(brandPagedropdown))
					{
						Select sel = new Select(brandPagedropdown);
						int size = sel.getOptions().size();
						boolean flag = false;
						do
						{
							i = r.nextInt(size);
							if(i==0||i==1)
							{
								continue;
							}
							else
							{
								break;
							}
						}while(flag!=true);						
						WebElement option = sel.getOptions().get(i);
						String optionName = option.getText();
						log.add("Selected drop down option is: "+optionName);
						option.click();
						wait.until(ExpectedConditions.attributeContains(brandPage, "style", "100%"));
						if(HBCBasicfeature.isListElementPresent(brandpageDropDownSelection))
						{
							int aftrSize = brandpageDropDownSelection.size();
							log.add("Displayed Brands size by default is: "+bfreSize);				
							log.add("Displayed Brands size after changing the options is: "+aftrSize);		
							if(bfreSize!=aftrSize)
							{
								Pass("On selecting the options in dropdown, the respective brands alphabets shown",log);
							}
							else
							{
								Fail("On selecting the options in dropdown, the respective brands alphabets not shown",log);								
							}
						}
						else
						{
							Pass("No Brands displayed for the selected dropdown option",log);
						}
					}
					else
					{
						Fail("Dropdown menu not displayed");
					}				
				}
				else
				{
					Fail("Dropdown Alphabets not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-3045" +e.getMessage());
				Exception("HBC-3045 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	
	
		
	/*HUDSONBAY-3042 Verify that alphabetic characters should be shown below the dropdown menu*/
	public void BrandAlphabetSection() throws Exception 
	{
		ChildCreation("HUDSONBAY-3042 Verify that alphabetic characters should be shown below the dropdown menu");
		if(brandLandPage == true)
		{
			String[] expected = HBCBasicfeature.getExcelVal("HBC3042", sheet, 1).split("\n");
			try
			{
				if(HBCBasicfeature.isElementPresent(brandPageAlphaSection))
				{
					Pass("Brand Landing Page Brand Alphates selection section displayed");					
					if(HBCBasicfeature.isListElementPresent(brandpageAlphabets))
					{
						int size = brandpageAlphabets.size();
						Pass("Total size of Alphabets displayed is: "+size);
						for(int i=1;i<=size;i++)
						{
							String actual = driver.findElement(By.xpath("//*[@class='sk_brandsName available']["+i+"]")).getText();
							int ssize = expected.length;
							//Pass("Total size of Expected Alphabets displayed is: "+ssize);
							for(int j=0;j<ssize;j++)
							{
								String expectd = expected[j];
								if(actual.equalsIgnoreCase(expectd))
								{
									Pass("Alphabetic character shown below the dropdown menu is: "+actual);
									break;
								}
								else
								{
									continue;
								}
							}
						}
					}
					else
					{
						Fail("Alphabetic characters not displayed ");
					}					
				}
				else
				{
					Fail("Brand Landing Page Brand Alphates selection section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-3042" +e.getMessage());
				Exception("HBC-3042 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-3043 Verify that alphabet "A" related brands should be shown by default*/
	public void BrandAlphabetDefault() throws Exception 
	{
		ChildCreation("HUDSONBAY-3043 Verify that alphabet 'A' related brands should be shown by default");
		if(brandLandPage == true)
		{
			String[] expected = HBCBasicfeature.getExcelVal("HBC3042", sheet, 1).split("\n");
			try
			{
				if(HBCBasicfeature.isElementPresent(brandpageAlphabetsActive))
				{
					String actual = brandpageAlphabetsActive.getText();
					log.add("Actual brands shown by default is: "+actual);
					log.add("Expected brands shown by default is: "+expected[0]);
					if(actual.equalsIgnoreCase(expected[0]))
					{
						Pass("Alphabet 'A' related brands shown by default",log);
					}
					else
					{
						Fail("Alphabet 'A' related brands not shown by default",log);
					}				
				}
				else
				{
					Fail("Brand alphabets not selected by default");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1955" +e.getMessage());
				Exception("HBC-1955 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-3044 Verify that on tapping any alphabets, the brands related to that alphabet is shown*/
	public void BrandAlphabetsSelect() 
	{
		ChildCreation("HUDSONBAY-3044 Verify that on tapping any alphabets, the brands related to that alphabet is shown");
		if(brandLandPage == true)
		{
			try
			{
				Random r  = new Random();
				if(HBCBasicfeature.isListElementPresent(brandpageAlphabets))
				{
					int size = brandpageAlphabets.size();
					int i = r.nextInt(size);
					WebElement actual = driver.findElement(By.xpath("//*[@class='sk_brandsName available']["+(i+1)+"]"));
					String alpha = actual.getText();							
					if(HBCBasicfeature.isElementPresent(actual))
					{
						log.add("Alphabet will be select is: "+alpha);
						actual.click();
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(brandpageAlphabetsActive))
						{
							String selected = brandpageAlphabetsActive.getText();
							log.add("Actual brand alphabet selected is: "+selected);
							if(alpha.equalsIgnoreCase(selected))
							{
								Pass("On tapping any alphabets, the brands related to that alphabet is shown",log);
							}
							else
							{
								Fail("On tapping any alphabets, the brands related to that alphabet is not shown",log);
							}				
						}
						else
						{
							Fail("Brand alphabets not selected");
						}						
					}
					else
					{
						Fail("selected Alphabet is not displayed");
					}
				}
				else
				{
					Fail("Brand Alphabets not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-3044" +e.getMessage());
				Exception("HBC-3044 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-3046 Verify that when the particular alphabet is not available, then that alphabet is unclickable and it is shown in grayed out color*/
	public void BrandProductNotAvailable() throws Exception 
	{
		ChildCreation("HUDSONBAY-3046 Verify that when the particular alphabet is not available, then that alphabet is unclickable and it is shown in grayed out color");
		if(brandLandPage == true)
		{
			Random r  = new Random();
			String expected = HBCBasicfeature.getExcelVal("HBC3046", sheet, 1);
			try
			{
				if(HBCBasicfeature.isListElementPresent(brandpageAlphabetUnavail))
				{
					Pass("Not available product is displayed");
					int size = brandpageAlphabetUnavail.size();
					int i = r.nextInt(size);
					WebElement alphabet = driver.findElement(By.xpath("//*[@class='sk_brandsNameCont']//*[@class='sk_brandsName']["+(i+1)+"]"));
					String alphaColor = alphabet.getCssValue("color");
					String color = HBCBasicfeature.colorfinder(alphaColor);
					log.add("Actual color is: "+color);
					log.add("Expected color is: "+expected);
					if(color.equalsIgnoreCase(expected))
					{
						Pass("when the particular alphabet is not available, then that alphabet is shown in grayed out color",log);
					}
					else
					{
						Fail("when the particular alphabet is not available, then that alphabet is not shown in grayed out color",log);
					}					
					/*wait.until(ExpectedConditions.visibilityOf(brandpageAlphabetsActive));
					if(HBCBasicfeature.isElementPresent(brandpageAlphabetsActive))
					{
						String actual = brandpageAlphabetsActive.getText();
						log.add("Actual brands shown by default is: "+actual);
						alphabet.click();
						Thread.sleep(500);
						String afterSelect = brandpageAlphabetsActive.getText();
						log.add("Brands shown after selection is: "+afterSelect);
						if(actual.equalsIgnoreCase(afterSelect))
						{
							Pass("when the particular alphabet is not available, then that alphabet is unclickable ",log);
						}
						else
						{
							Fail("when the particular alphabet is not available, then that alphabet is clickable",log);
						}				
					}
					else
					{
						Fail("Brand alphabets not selected by default");
					}*/
				}
				else
				{
					Skip("Selected DropDown option has products available for all the alphabets");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-3046" +e.getMessage());
				Exception("HBC-3046 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-3047 Verify that when the brand name is starts with the number, then it shows in the #*/
	public void BrandHashOption() 
	{
		ChildCreation("HUDSONBAY-3047 Verify that when the brand name is starts with the number, then it shows in the #");
		if(brandLandPage == true)
		{
			Random r  = new Random();
			try
			{
				if(HBCBasicfeature.isElementPresent(brandPagedropdown))
				{
					Pass("Brand Landing Page DropDown displayed");
					Select sel = new Select(brandPagedropdown);
					sel.getOptions().get(1).click();		
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(brandPageHash))
					{
						Pass("# option displayed");
						brandPageHash.click();
						Thread.sleep(500);
						if(HBCBasicfeature.isElementPresent(brandpageAlphabetsActive))
						{
							String actual = brandpageAlphabetsActive.getText();
							log.add("Current selected option is: "+actual);
							if(HBCBasicfeature.isListElementPresent(brandpageDropDownSelection))
							{
								int size = brandpageDropDownSelection.size();
								int i = r.nextInt(size);
								String brand = driver.findElement(By.xpath("//*[@class='sk_brandsLinksCont active']//*[@class='sk_brandsLinkInner']//div["+(i+1)+"]")).getText();
								log.add("Selected Brand name is: "+brand);
								String txt = brand.substring(0, 1);
								if(txt.matches(".*\\d+.*"))
								{
									Pass("Brand name is starts with the number",log);
								}
								else
								{
									Fail("Brand name is not starts with the number",log);
								}
							}
							else
							{
								Fail("Brand names not displayed");
							}
						}
						else
						{
							Fail("Selected alphabet brand name not displayed");
						}
					}
					else
					{
						Fail("# option not displayed");
					}
				}
				else
				{
					Fail("Brand Page DropDown option not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-3047" +e.getMessage());
				Exception("HBC-3047 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Brand Landing Page not displayed");
		}
	}
	
}

