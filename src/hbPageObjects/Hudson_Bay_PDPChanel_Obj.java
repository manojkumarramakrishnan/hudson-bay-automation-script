package hbPageObjects;

import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_PDPChanel;

public class Hudson_Bay_PDPChanel_Obj extends Hudson_Bay_PDPChanel implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_PDPChanel_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}	
	
	
	@FindBy(xpath=""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
		
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath=""+chanelLandingg+"")
	WebElement chanelLanding;	
	
	@FindBy(xpath=""+chanelHeaderr+"")
	WebElement chanelHeader;
	
	@FindBy(xpath=""+chanelPLPHeaderr+"")
	WebElement chanelPLPHeader;	
	
	@FindBy(xpath=""+chanelPLPProductCountt+"")
	WebElement 	chanelPLPProductCount;	
	
	@FindBy(xpath=""+chanelDescriptionn+"")
	WebElement chanelDescription;
	
	@FindBy(xpath=""+chanelHeroBannerr+"")
	WebElement chanelHeroBanner;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+PDPProdNamee+"")
	WebElement PDPProdName;		

	@FindBy(xpath = ""+pdpPageProductImagee+"")
	WebElement pdpPageProductImage;		
	
	@FindBy(xpath = ""+	pdppageSizeContainerr+"")
	WebElement 	pdppageSizeContainer;
	
	@FindBy(xpath = ""+pdpcollectionImageSelectedd+"")
	WebElement pdpcollectionImageSelected;	
	
	@FindBy(xpath = ""+pdpWishListIconn+"")
	WebElement pdpWishListIcon;		
	
	@FindBy(xpath = ""+pdpPageSwatchess+"")
	WebElement pdpPageSwatchesContainer;
	
	@FindBy(xpath = ""+pdpPageActiveColorr+"")
	WebElement pdpPageActiveColor;	
		
	@FindBy(xpath = ""+pdppageSizee+"")
	WebElement pdppageSize;
		
	@FindBy(xpath = ""+	pdppageQuantityContainerr+"")
	WebElement 	pdppageQuantityContainer;
	
	@FindBy(xpath = ""+	pdppageQuantityPluss+"")
	WebElement 	pdppageQuantityPlus;
	
	@FindBy(xpath = ""+	pdppageQuantityMinuss+"")
	WebElement 	pdppageQuantityMinus;
	
	@FindBy(xpath = ""+	pdppageQuantityTextt+"")
	WebElement 	pdppageQuantityText;
	
	@FindBy(xpath = ""+	pdppageDetailss+"")
	WebElement 	pdppageDetails;
	
	@FindBy(xpath = ""+	pdpcollectionDetailsSectionn+"")
	WebElement 	pdpcollectionDetailsSection;
	
	@FindBy(xpath = ""+	pdppageATBb+"")
	WebElement 	pdppageATB;
	
	@FindBy(xpath = ""+	ATBAlertt+"")
	WebElement 	ATBAlert;
	
	@FindBy(xpath = ""+	ATBAlertOkk+"")
	WebElement 	ATBAlertOk;
		
	@FindBy(xpath = ""+	pdppageATBOverlayy+"")
	WebElement 	pdppageATBOverlay;
	
	@FindBy(xpath = ""+	loadingbarr+"")
	WebElement 	loadingbar;
	
	@FindBy(xpath = ""+pdppageATBOverlayClosee+"")
	WebElement 	pdppageATBOverlayClose;
	
	@FindBy(xpath = ""+pdppageShareIconss+"")
	WebElement 	pdppageShareIcons;
	
	@FindBy(xpath = ""+pdppageShareFBb+"")
	WebElement 	pdppageShareFB;
	
	@FindBy(xpath = ""+pdppageShareTwitterr+"")
	WebElement 	pdppageShareTwitter;
	
	@FindBy(xpath = ""+pdppageShareGpluss+"")
	WebElement 	pdppageShareGplus;
	
	@FindBy(xpath = ""+pdppageSizeSelectedDefaultt+"")
	WebElement 	pdppageSizeSelectedDefault;
	
	@FindBy(xpath = ""+pdppageSwatchColorSelectedd+"")
	WebElement 	pdppageSwatchColorSelected;
		
	@FindBy(xpath = ""+collectionProductSelectYourItemss+"")
	WebElement 	collectionProductSelectYourItems;
	
	@FindBy(xpath = ""+chanelCategoryOpenSectionCloseArroww+"")
	WebElement 	chanelCategoryOpenSectionCloseArrow;
	
	@FindBy(xpath = ""+chanelCategoryOpenStatee+"")
	WebElement 	chanelCategoryOpenState;	
	
	@FindBy(xpath = ""+pdppageDetailsExpandd+"")
	WebElement 	pdppageDetailsExpand;
	
	@FindBy(xpath = ""+pdppageDetailsExpandClss+"")
	WebElement 	pdppageDetailsExpandCls;
	
	@FindBy(xpath = ""+pdpWebIdd+"")
	WebElement 	pdpWebId;
	
	@FindBy(xpath = ""+pdppageDetailsRewardd+"")
	WebElement 	pdppageDetailsReward;
	
	@FindBy(how = How.XPATH,using  = ""+pdppageDescriptionAccordss+"")
	List<WebElement> pdppageDescriptionAccords;	
	
	@FindBy(how = How.XPATH,using  = ""+pdppageSizeContainerListt+"")
	List<WebElement> pdppageSizeContainerList;
	
	@FindBy(how = How.XPATH,using  = ""+chanelPLPswatchContainerr+"")
	List<WebElement> chanelPLPswatchContainer;	
	
	@FindBy(xpath = ""+	pdppagePriceSalee+"")
	WebElement 	pdppagePriceSale;

	@FindBy(xpath = ""+	pdppagePriceRegularr+"")
	WebElement 	pdppagePriceRegular;
	
	@FindBy(xpath = ""+sortt+"")
	WebElement sort;
	
	@FindBy(xpath = ""+Filterr+"")
	WebElement Filter;
	
	@FindBy(xpath = ""+FilterCountt+"")
	WebElement FilterCount;
	
	@FindBy(xpath = ""+pdpPageSwatchSelectedColorr+"")
	WebElement 	pdpPageSwatchSelectedColor;
	
		
	@FindBy(how = How.XPATH,using  = ""+chanelCategoryy+"")
	List<WebElement> chanelCategory;	
	
	@FindBy(how = How.XPATH,using  = ""+chanelPromoBannerr+"")
	List<WebElement> chanelPromoBanner;	
	
	@FindBy(how = How.XPATH,using  = ""+chanelPromoBannerDotss+"")
	List<WebElement> chanelPromoBannerDots;	
	
	@FindBy(how = How.XPATH,using  = ""+pdppageDetailsExpandd+"")
	List<WebElement> pdppageDetailsExpandList;		
	
	@FindBy(how = How.XPATH,using  = ""+collectionProductListt+"")
	List<WebElement> collectionProductList;	
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	@FindBy(how = How.XPATH,using  = ""+pdpPageSwatchesListt+"")
	List<WebElement> pdpPageSwatchesList;
	
		//Footer:
		@FindBy(xpath = ""+signupEmailss+"")
		WebElement signupEmails;
	
		@FindBy(xpath = ""+footerContainerr+"")
		WebElement footerContainer;	
		
		@FindBy(xpath = ""+footerTopp+"")
		WebElement footerTop;	
		
		@FindBy(xpath = ""+footerTopTitlee+"")
		WebElement footerTopTitle;
		
		@FindBy(xpath = ""+footerSubTopp+"")
		WebElement footerSubTop;
		
		@FindBy(xpath = ""+footerSubTopCalll+"")
		WebElement footerSubTopCall;
		
		@FindBy(xpath = ""+footerSubTopEmaill+"")
		WebElement footerSubTopEmail;
		
		@FindBy(xpath = ""+footerMenuListt+"")
		WebElement footerMenuList;
		
		@FindBy(xpath = ""+footerBottomm+"")
		WebElement footerBottom;
		
		@FindBy(xpath = ""+footerSocialIconn+"")
		WebElement footerSocialIcon;
		
		@FindBy(xpath = ""+footerBottomCopyrightt+"")
		WebElement footerBottomCopyright;

		String PLPtitle = "";	

		boolean chanel = false, chanelPlp = false,  chanelPDP = false, chanelPLPSwatches = false;
	

	//HUDSONBAY-703	Verify that Chanel Landing page should be as per the creative and fit to the Screen in both potrait and Landscape
	public void ChanelLandingPageCreative() throws Exception
		{
			ChildCreation("HUDSONBAY-703 Verify that Chanel Landing page should be as per the creative and fit to the Screen in both potrait and Landscape");	
			String Key = HBCBasicfeature.getExcelVal("chanel", sheet, 1);
			try
			{	
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(Key);
						searchBox.sendKeys(Keys.ENTER);
						wait.until(ExpectedConditions.attributeContains(chanelLanding, "style", "100%"));
						if(HBCBasicfeature.isElementPresent(chanelLanding))
						{
							Pass("Chanel Landing page displayed");		
							chanel = true;
							if(HBCBasicfeature.isElementPresent(chanelHeader))
							{
								Pass("Chanel Header is displayed");
							}
							else
							{
								Fail("Chanel Header is not displayed");
							}
							if(HBCBasicfeature.isListElementPresent(chanelPromoBanner))
							{
								Pass("Chanel Promo Banner is displayed");
							}
							else
							{
								Fail("Chanel Promo Banner is not displayed");
							}							
							if(HBCBasicfeature.isListElementPresent(chanelPromoBannerDots))
							{
								Pass("Chanel Promo Banner Dots are displayed");
							}
							else
							{
								Fail("Chanel Promo Banner Dots are not displayed");
							}
							if(HBCBasicfeature.isListElementPresent(chanelCategory))
							{
								int size = chanelCategory.size();
								log.add("Actual sections/categories size is: "+size);
								log.add("Expected sections/categories size is: "+5);
								if(size==5)
								{
									Pass("Chanel sections/categories are displayed",log);
								}
								else
								{
									Fail("Chanel sections/categories size not matches",log);
								}
							}
							else
							{
								Fail("Chanel sections/categories are not displayed");
							}
							if(HBCBasicfeature.isElementPresent(chanelDescription))
							{
								boolean res = chanelDescription.getText().isEmpty();
								log.add("Displayed chanel description is: "+chanelDescription.getText());
								if(res==false)
								{
									Pass("Chanel Description is displayed",log);
								}
								else
								{
									Fail("Chanel Description is empty ",log);
								}
							}
							else
							{
								Fail("Chanel Description is not displayed");
							}
							if(HBCBasicfeature.isElementPresent(chanelHeroBanner))
							{
								HBCBasicfeature.scrolldown(chanelHeroBanner, driver);
								Pass("Chanel Hero Banner is displayed");
							}
							else
							{
								Fail("Chanel Hero Banner is not displayed");
							}
						}
						else
						{
							Fail("Chanel Landing page not dispalyed");
						}		
						if(HBCBasicfeature.isElementPresent(footerTop))
						{
							HBCBasicfeature.scrolldown(footerTop, driver);
							Pass("Footer top panel is displayed");	
							if(HBCBasicfeature.isElementPresent(footerTopTitle))
							{
								Pass("Footer top panel title is displayed");	
							}
							else
							{
								Fail("Footer top panel title is not displayed");						
							}
							if(HBCBasicfeature.isElementPresent(footerSubTop))
							{
								Pass("Footer sub top panel is displayed");	
								if(HBCBasicfeature.isElementPresent(footerSubTopCall))
								{
									Pass("Footer call option is displayed");		
								}
								else
								{
									Fail("Footer Call option is not displayed");						
								}
								if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
								{
									Pass("Footer Email option is displayed");	
								}
								else
								{
									Fail("Footer email option is not displayed");						
								}
							}
							else
							{
								Fail("Footer sub top panel is not displayed");						
							}
						}
						else
						{
							Fail("Footer top panel is not displayed");						
						}
						if(HBCBasicfeature.isElementPresent(footerMenuList))
						{
							Pass("Footer Menu List option is displayed");
						}
						else
						{
							Fail("Footer menu list is not displayed");						
						}
						if(HBCBasicfeature.isElementPresent(footerBottom))
						{
							Pass("Footer bottom panel is displayed");
							wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
							if(HBCBasicfeature.isElementPresent(footerSocialIcon))
							{
								Pass("Footer social icon options are displayed");
							}
							else
							{
								Fail("Footer social icon options are not displayed");						
							}
							if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
							{
								log.add("Footer displayed as per the creative and fit to the screen");
								Pass("Footer copyright section is displayed",log);										
							}
							else
							{
								log.add("Footer is not displayed as per the creative and fit to the screen");
								Fail("Footer copyright section is not displayed",log);	
							}
						}
						else
						{
							Fail("Footer bottom panel is not displayed");						
						}	
						HBCBasicfeature.scrollup(header, driver);
					}
					else
					{
						Fail("Search Box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println("HUDSONBAY-703" +e.getMessage());
				Exception("HUDSONBAY-703 There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	
	/*HUDSONBAY-704 Verify that Promo banner should be displayed below the header in Chanel landing page as per the creative.*/
	public void ChanelPromoBanner() 
	{
		ChildCreation("HUDSONBAY-704 Verify that Promo banner should be displayed below the header in Chanel landing page as per the creative.");
		if(chanel == true)
		{
			try
			{
				if(HBCBasicfeature.isListElementPresent(chanelPromoBanner))
				{
					Pass("Chanel Promo Banner is displayed");
					int size = chanelPromoBanner.size();
					log.add("Promo banner size is: "+size);
					if(HBCBasicfeature.isListElementPresent(chanelPromoBannerDots))
					{
						Pass("Chanel Promo Banner carousel dots displayed ");
						int ssize = chanelPromoBannerDots.size();
						log.add("Promo banner carousel size is: "+ssize);
						if(size==ssize)
						{
							for(int i = 1;i<=ssize;i++)
							{
								WebElement carouslClick = driver.findElement(By.xpath("//*[@class='swiper-pagination swiper-pagination-clickable swiper-pagination-bullets']//*[contains(@class,'swiper-pagination-bullet')]["+i+"]"));
								carouslClick.click();
								Thread.sleep(500);
								WebElement promoBanner = driver.findElement(By.xpath("//*[@class='swiper-slide swiper-slide-active']//img"));
								int response = HBCBasicfeature.imageBroken(promoBanner, log);
								Thread.sleep(500);
								if(response==200)
								{
									Pass("Promo banner "+i+" displayed  with valid response",log);
								}
								else
								{
									Fail("Promo banner "+i+" not displayed",log);
								}							
							}
						}
						else
						{
							Fail("Promo banner and Promo banner carousel sizes are not matches",log);
						}
					}
					else
					{
						Fail("Chanel Promo Banner Carousels are not displayed");
					}			
				}
				else
				{
					Fail("Chanel Promo Banner is not displayed");
				}							
			}
			catch (Exception e)
			{
				System.out.println("HBC-704" +e.getMessage());
				Exception("HBC-704 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-709 Verify that Carousels should be shown in the Chanel landing page if more than one banner is shown*/
	public void ChanelCarousels() 
	{
		ChildCreation("HUDSONBAY-709 Verify that Carousels should be shown in the Chanel landing page if more than one banner is shown");
		if(chanel == true)
		{
			try
			{
				if(HBCBasicfeature.isListElementPresent(chanelPromoBanner))
				{
					Pass("Chanel Promo Banner is displayed");
					int size = chanelPromoBanner.size();
					log.add("Promo banner size is: "+size);
					if(size>1)
					{
						if(HBCBasicfeature.isListElementPresent(chanelPromoBannerDots))
						{
							Pass("Chanel Promo Banner carousel dots displayed in the Chanel landing page if more than one banner is shown ");
						}
						else
						{
							Fail("Chanel Promo Banner Carousels are not displayed in the Chanel landing page if more than one banner is shown");
						}		
					}
					else
					{
						log.add(" Promo Banner size is "+size+" not more than one");
						if(HBCBasicfeature.isListElementPresent(chanelPromoBannerDots))
						{
							Fail(" Carousels shown in the Chanel landing page if more than one banner is shown");
						}
						else
						{
							Pass(" Carousels not shown in the Chanel landing page if not more than one banner is shown");
						}						
					}
				}
				else
				{
					Fail("Chanel Promo Banner is not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println("HBC-709" +e.getMessage());
				Exception("HBC-709 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-706 Verify that Chanel logo should be displayed below the header.*/
	public void ChanelLogo() 
	{
		ChildCreation("HUDSONBAY-706 Verify that Chanel logo should be displayed below the header");
		if(chanel == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC706", sheet, 1);
				if(HBCBasicfeature.isElementPresent(chanelHeader))
				{
					String actual = chanelHeader.getText();
					log.add("Actual string displayed is: "+actual);
					log.add("Expected string is: "+expected);
					if(actual.equals(expected))
					{
						Pass("Chanel Header is displayed",log);
					}
					else
					{
						Fail("Chanel Header string not matches",log);
					}					
				}
				else
				{
					Fail("Chanel Header is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-706" +e.getMessage());
				Exception("HBC-706 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}
	
	
	/*HUDSONBAY-707 Verify that Chanel description should be shown in the Chanel landing page as per the creative*/
	public void ChanelDescription() 
	{
		ChildCreation("HUDSONBAY-707 Verify that Chanel description should be shown in the Chanel landing page as per the creative");
		if(chanel == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(chanelDescription))
				{
					boolean res = chanelDescription.getText().isEmpty();
					log.add("Displayed chanel description is: "+chanelDescription.getText());
					if(res==false)
					{
						Pass("Chanel Description is displayed in the Chanel landing page as per the creative",log);
					}
					else
					{
						Fail("Chanel Description is empty ",log);
					}
				}
				else
				{
					Fail("Chanel Description is not displayed in the Chanel landing page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-707" +e.getMessage());
				Exception("HBC-707 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-708 Verify that Chanel Hero banners should be shown in the Chanel landing page as per the creative*/
	public void ChanelHeroBanners() 
	{
		ChildCreation("HUDSONBAY-708 Verify that Chanel Hero banners should be shown in the Chanel landing page as per the creative");
		if(chanel == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(chanelHeroBanner))
				{
					HBCBasicfeature.scrolldown(chanelHeroBanner, driver);
					int response = HBCBasicfeature.imageBroken(chanelHeroBanner, log);
					if(response==200)
					{
						Pass("Chanel Hero Banner is displayed in the Chanel landing page as per the creative",log);
					}
					else
					{
						Fail("Chanel Hero Banner response not matches",log);
					}
				}
				else
				{
					Fail("Chanel Hero Banner is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-708" +e.getMessage());
				Exception("HBC-708 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}	
	
	/*HUDSONBAY-717 Verify that Accordions(sections/categories) should be displayed below the product notes in Chanel landing page*/
	public void ChanelAccordians() 
	{
		ChildCreation("HUDSONBAY-717 Verify that Accordions(sections/categories) should be displayed below the product notes in Chanel landing page");
		if(chanel == true)
		{
			try
			{
				String[] expectedAccords = HBCBasicfeature.getExcelVal("HBC717", sheet, 1).split("\n");
				if(HBCBasicfeature.isListElementPresent(chanelCategory))
				{
					int size = chanelCategory.size();
					log.add("Actual sections/categories size is: "+size);
					log.add("Expected sections/categories size is: "+5);
					if(size==5)
					{
						Pass("Chanel sections/categories are displayed",log);
						for(int i=1;i<=size;i++)
						{
							String accordName = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']//*[@class='sk_mobCategoryMenuItemTxt'])["+i+"]")).getText(); 
							int ssize = expectedAccords.length;
							for(int j=0;i<ssize;i++)
							{
								if(accordName.equalsIgnoreCase(expectedAccords[j]))
								{						
									Pass("Accordions(sections/categories) "+accordName+" displayed below the product notes in Chanel landing page");
								}
								else
								{
									Fail("Accordions(sections/categories) "+accordName+" not displayed below the product notes in Chanel landing page");
								}
							}
						}
					}
					else
					{
						Fail("Chanel sections/categories size not matches",log);
					}
				}
				else
				{
					Fail("Chanel sections/categories are not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-717" +e.getMessage());
				Exception("HBC-717 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}
		
	
	/*HUDSONBAY-724 Verify that all the accordion in the Chanel Landing page should be in collapsed state by default*/
	public void ChanelAccordianNotOpenStateDefault() 
	{
		ChildCreation("HUDSONBAY-724 Verify that all the accordion in the Chanel Landing page should be in collapsed state by default");
		if(chanel == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(chanelCategoryOpenState))
				{
					String openAccord = driver.findElement(By.xpath("//*[contains(@class,'sk_categorymenuopen skMob_currentSelItem')]//*[@class='sk_mobCategoryMenuItemTxt']")).getText();
					log.add("Accordian in opened state is: "+openAccord);
					Fail("Some Accordian in opened state by default",log);
				}
				else
				{
					Pass("All the accordion in the Chanel Landing page in collapsed state by default");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-724" +e.getMessage());
				Exception("HBC-724 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}
		
	/*HUDSONBAY-725 Verify that accordion should be closed on tapping the opened state accordion again or tapping the collapsed state accordion in Chanel landing page.*/
	public void ChanelOpenCloseAccordian() throws Exception 
	{
		ChildCreation("HUDSONBAY-725 Verify that accordion should be closed on tapping the opened state accordion again or tapping the collapsed state accordion in Chanel landing page.");
		if(chanel == true)
		{
			try
			{
				String[] expectedAccords = HBCBasicfeature.getExcelVal("HBC717", sheet, 1).split("\n");		
				Random r = new Random();
				Actions act = new Actions(driver);
				if(HBCBasicfeature.isListElementPresent(chanelCategory))
				{
					Pass("Chanel sections/categories are displayed");				
					int size = chanelCategory.size();
					int i = r.nextInt(size);
					String accordName = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']//*[@class='sk_mobCategoryMenuItemTxt'])["+(i+1)+"]")).getText(); 
					if(accordName.equalsIgnoreCase(expectedAccords[4]))
					{
						i = i-1;
						accordName = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']//*[@class='sk_mobCategoryMenuItemTxt'])["+(i+1)+"]")).getText(); 
					}
					log.add("Selected Accord Name is: "+accordName);
					WebElement accordClick = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+(i+1)+"]"));
					if(HBCBasicfeature.isElementPresent(accordClick))
					{
						act.moveToElement(accordClick).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(chanelCategoryOpenState));
						if(HBCBasicfeature.isElementPresent(chanelCategoryOpenState))
						{							
							Pass("Accordian in opened state");
							if(HBCBasicfeature.isElementPresent(chanelCategoryOpenSectionCloseArrow))
							{
								Pass("Open state accordian arrow  displayed");
								chanelCategoryOpenSectionCloseArrow.click();
								Thread.sleep(500);
								if(HBCBasicfeature.isElementPresent(chanelCategoryOpenState))
								{
									Fail("Accordion not closed on tapping the opened state accordion again");
								}
								else
								{
									Pass("Accordion closed on tapping the opened state accordion again");
								}							
							}
							else
							{
								Fail("Open state accordian arrow not displayed");
							}
						}
						else
						{
							Fail("Accordian not in opened state");
						}
					}
					else
					{
						Fail("selected accord is not displayed");
					}
				}
				else
				{
					Fail("Chanel sections/categories are not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-725" +e.getMessage());
				Exception("HBC-725 There is something wrong. Please Check." + e.getMessage());
			}		
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}
				
	/*HUDSONBAY-726 Verify that when selecting the Accordions(sections/cateogries) from Chanel landing page its sub categories/sections should be shown.*/
	/*HUDSONBAY-727 Verify that when selecting the sub(sections/cateogries) from Chanel landing page, corresponding Chanel PLP should be shown.*/
	public void ChanelOpenAccordianSubCatPLP() throws Exception 
	{
		ChildCreation("HUDSONBAY-726 Verify that when selecting the Accordions(sections/cateogries) from Chanel landing page its sub categories/sections should be shown");
		if(chanel == true)
		{
			Random r = new Random();
			Actions act = new Actions(driver);
			String accordName = ""; int i=0; WebElement accordClick = null, subCategory=null;
			boolean HBC726 = false, HBC727 = false;
			try
			{
				String[] expectedAccords = HBCBasicfeature.getExcelVal("HBC717", sheet, 1).split("\n");
				if(HBCBasicfeature.isListElementPresent(chanelCategory))
				{
					Pass("Chanel sections/categories are displayed");				
					int size = chanelCategory.size();
					i = r.nextInt(size);
					accordName = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']//*[@class='sk_mobCategoryMenuItemTxt'])["+(i+1)+"]")).getText(); 
					if(accordName.equalsIgnoreCase(expectedAccords[4]))
					{
						i = i-1;
						accordName = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']//*[@class='sk_mobCategoryMenuItemTxt'])["+(i+1)+"]")).getText(); 
					}
					log.add("Selected Accord Name is: "+accordName);
					accordClick = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+(i+1)+"]"));
					if(HBCBasicfeature.isElementPresent(accordClick))
					{
						act.moveToElement(accordClick).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(chanelCategoryOpenState));
						if(HBCBasicfeature.isElementPresent(chanelCategoryOpenState))
						{						
							String openAccord = driver.findElement(By.xpath("//*[contains(@class,'sk_categorymenuopen skMob_currentSelItem')]//*[@class='sk_mobCategoryMenuItemTxt']")).getText();
							log.add("Accordian in opened state is: "+openAccord);
							if(accordName.equalsIgnoreCase(openAccord))
							{
								Pass("Accordian expands On Tapping the closed state accordian",log);
								HBC726 = true;
							}
							else
							{
								Fail("Respective accordian not expanded",log);
							}
						}
						else
						{
							Fail("Accordian not expands On Tapping the closed state accordian",log);					
						}
					}
					else
					{
						Fail("Selected sections/categories not dispalyed");
					}
				}
				else
				{
					Fail("Chanel sections/categories are not displayed");
				}			
						
				ChildCreation("HUDSONBAY-726 Verify that when selecting the Accordions(sections/cateogries) from Chanel landing page its sub categories/sections should be shown.");
				if(HBC726 == true)
				{				
					if(HBCBasicfeature.isElementPresent(chanelCategoryOpenState))
					{						
						String openAccord = driver.findElement(By.xpath("//*[contains(@class,'sk_categorymenuopen skMob_currentSelItem')]//*[@class='sk_mobCategoryMenuItemTxt']")).getText();
						Pass("Accordian in opened state is: "+openAccord);
						Thread.sleep(1000);
						subCategory = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+(i+1)+"]//*[@class='sk_mobSubCategory']"));
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(subCategory, "style", "block"));
						if(HBCBasicfeature.isElementPresent(subCategory))
						{
							Pass("when selecting the Accordions(sections/cateogries) from Chanel landing page its sub categories/sections shown");
							HBC727 = true;
						}
						else
						{
							Fail("when selecting the Accordions(sections/cateogries) from Chanel landing page its sub categories/sections not shown");
						}
					}
					else
					{
						Fail("Accordian not expands On Tapping the closed state accordian",log);					
					}				
				}
				else
				{
					Skip("Accordians not in opened state ");
				}		
			
				ChildCreation("HUDSONBAY-727 Verify that when selecting the sub(sections/cateogries) from Chanel landing page, corresponding Chanel PLP should be shown");
				if(HBC727 == true)
				{
					if(HBCBasicfeature.isElementPresent(subCategory))
					{
						Pass("Sub(sections/cateogries) displayed");
						List<WebElement> subcat = driver.findElements(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+(i+1)+"]//*[@class='sk_mobSubCategory']//*[@id='sk_mobCategoryItem_id__1']"));
						int subSize = 	subcat.size();
						int j = r.nextInt(subSize);
						Thread.sleep(1000);
						WebElement subCatClick =  driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+(i+1)+"]//*[@class='sk_mobSubCategory']//*[@id='sk_mobCategoryItem_id__1']["+(j+1)+"]"));
						if(HBCBasicfeature.isElementPresent(subCatClick))
						{
							act.moveToElement(subCatClick).click().build().perform();
							Thread.sleep(1000);
							try
							{
								wait.until(ExpectedConditions.visibilityOf(plpPage));
								if(HBCBasicfeature.isElementPresent(plpPage))
								{							
									Pass("Selecting the sub(sections/cateogries) from Chanel landing page, corresponding Chanel PLP shown");
									chanelPlp = true;
								}
								else
								{
									Fail("Selecting the sub(sections/cateogries) from Chanel landing page, corresponding Chanel PLP not shown");
								}
							}
							catch(Exception e)							
							{
								WebElement subSubCat = driver.findElement(By.xpath("((//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+(i+1)+"]//*[@class='sk_mobSubCategory']//*[@id='sk_mobCategoryItem_id__1']["+(j+1)+"])//*[@class='sk_mobSubCategory ']"));
								wait.until(ExpectedConditions.attributeContains(subSubCat, "style", "block"));
								if(HBCBasicfeature.isElementPresent(subSubCat))
								{
									Pass("Sub Sub sections/cateogries displayed");
									List<WebElement> subsubcat = driver.findElements(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+(i+1)+"]//*[@class='sk_mobSubCategory']//*[@id='sk_mobCategoryItem_id__1']"));
									int subsubSize = 	subsubcat.size();
									int k = r.nextInt(subsubSize);
									Thread.sleep(1000);
									WebElement subsubCatClick =  driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+(i+1)+"]//*[@class='sk_mobSubCategory']//*[@id='sk_mobCategoryItem_id__1']["+(j+1)+"]//*[@class='sk_mobSubCategory ']//*[@id='sk_mobCategoryItem_id__2']["+(k+1)+"]"));
									if(HBCBasicfeature.isElementPresent(subsubCatClick))
									{
										act.moveToElement(subsubCatClick).click().build().perform();
										wait.until(ExpectedConditions.visibilityOf(plpPage));
										if(HBCBasicfeature.isElementPresent(plpPage))
										{							
											Pass("Selecting the Sub Sub(sections/cateogries) from Chanel landing page, corresponding Chanel PLP shown");
											chanelPlp = true;
										}
										else
										{
											Fail("Selecting the Sub Sub(sections/cateogries) from Chanel landing page, corresponding Chanel PLP not shown");
										}
									}
									else
									{
										Fail("Sub Sub(sections/cateogries) not displayed");
									}
								}
								else
								{
									Fail("Sub Sub(sections/cateogries) not displayed");
								}								
							}
						}
						else
						{
							Fail("sub(sections/cateogries) not displayed");
						}
					}
					else
					{
						Fail("Selected sections/categories's sub(sections/cateogries)  not dispalyed");
					}
				}
				else
				{
					Skip("Accordians not in opened state, pls chk HBC-726 ");
				}	
			}
			catch (Exception e)
			{
				System.out.println("HBC-726" +e.getMessage());
				Exception("HBC-726 There is something wrong. Please Check." + e.getMessage());
			}			
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}	
	
	/*HUDSONBAY-728 Verify that Chanel PLP page should be as per the creative and fit to the Screen in both potrait and Landscape*/
	public void ChanelPLPCreative() 
	{
		ChildCreation("HUDSONBAY-728 Verify that Chanel PLP page should be as per the creative and fit to the Screen in both potrait and Landscape");
		if(chanelPlp == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					Pass("Chanel PLP page displayed");
					if(HBCBasicfeature.isElementPresent(chanelPLPHeader))
					{
						Pass("Chanel PLP page header displayed");
					}
					else
					{
						Fail("Chanel PLP page header not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(chanelPLPProductCount))
					{
						Pass("Chanel PLP page product count displayed");
					}
					else
					{
						Fail("Chanel PLP page product count not displayed");
					}
					if(HBCBasicfeature.isElementPresent(sort))
					{
						Pass("Chanel PLP page sort option displayed");
					}
					else
					{
						Fail("Chanel PLP page sort option not displayed");
					}			
					if(HBCBasicfeature.isElementPresent(Filter))
					{
						Pass("Chanel PLP page sort option displayed");
					}
					else
					{
						Fail("Chanel PLP page sort option not displayed");
					}		
					if(HBCBasicfeature.isListElementPresent(plpItemContList))
					{
						int size = plpItemContList.size();
						log.add("Products displayed in chanel PLP page is: "+size);
						if(size>0)
						{
							Pass("Products displayed in chanel PLP page",log);
						}
						else
						{
							Fail("Products not displayed in chanel PLP page",log);
						}
					}
					else
					{
						Fail("Products not displayed in chanel PLP page");
					}
					if(HBCBasicfeature.isElementPresent(footerTop))
					{
						HBCBasicfeature.scrolldown(footerTop, driver);
						Pass("Footer top panel is displayed");	
						if(HBCBasicfeature.isElementPresent(footerTopTitle))
						{
							Pass("Footer top panel title is displayed");	
						}
						else
						{
							Fail("Footer top panel title is not displayed");						
						}
						if(HBCBasicfeature.isElementPresent(footerSubTop))
						{
							Pass("Footer sub top panel is displayed");	
							if(HBCBasicfeature.isElementPresent(footerSubTopCall))
							{
								Pass("Footer call option is displayed");		
							}
							else
							{
								Fail("Footer Call option is not displayed");						
							}
							if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
							{
								Pass("Footer Email option is displayed");	
							}
							else
							{
								Fail("Footer email option is not displayed");						
							}
						}
						else
						{
							Fail("Footer sub top panel is not displayed");						
						}
					}
					else
					{
						Fail("Footer top panel is not displayed");						
					}
					if(HBCBasicfeature.isElementPresent(footerMenuList))
					{
						Pass("Footer Menu List option is displayed");
					}
					else
					{
						Fail("Footer menu list is not displayed");						
					}
					if(HBCBasicfeature.isElementPresent(footerBottom))
					{
						Pass("Footer bottom panel is displayed");
						wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
						if(HBCBasicfeature.isElementPresent(footerSocialIcon))
						{
							Pass("Footer social icon options are displayed");
						}
						else
						{
							Fail("Footer social icon options are not displayed");						
						}
						if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
						{
							log.add("Footer displayed as per the creative and fit to the screen");
							Pass("Footer copyright section is displayed",log);										
						}
						else
						{
							log.add("Footer is not displayed as per the creative and fit to the screen");
							Fail("Footer copyright section is not displayed",log);	
						}
					}
					else
					{
						Fail("Footer bottom panel is not displayed");						
					}	
					HBCBasicfeature.scrollup(header, driver);
				}
				else
				{
					Fail("Chanel PLP page not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-728" +e.getMessage());
				Exception("HBC-728 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PLP page not displayed");
		}
	}
	
	
	/*HUDSONBAY-730 Verify that total items available count (XXX items) should be shown below the Category title in Chanel PLP page*/
	public void ChanelItemCount()
	{
		ChildCreation("HUDSONBAY-730 Verify that total items available count (XXX items) should be shown below the Category title in Chanel PLP page");
		chanelPlp = true;
		if(chanelPlp == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(chanelPLPProductCount))
				{
					Pass("Chanel PLP page product count displayed");
					String val = chanelPLPProductCount.getText().substring(8);
					String[] split = val.split(" ");
					String count = split[0];
					String text = split[1];
					Thread.sleep(500);
					log.add("Displayed available product count is: "+count);
					if(count.matches(".*\\d+.*"))
					{		
						Pass("Product count displayed",log);							
					}
					else
					{
						Fail("Product count not displayed",log);
					}	
					Thread.sleep(500);
					log.add("Displayed Item text is: "+text);					
					if(text.equalsIgnoreCase("Items")||text.equalsIgnoreCase("Item"))
					{
						Pass("Total items available count (XXX items) displayed below the category title in Chanel product list page",log);
					}
					else
					{
						Fail("Total items available count (XXX items) not displayed below the category title in Chanel product list page",log);
					}
				}
				else
				{
					Fail("Chanel PLP page product count not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-730" +e.getMessage());
				Exception("HBC-730 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PLP Page not displayed");
		}
	}
	
	/*HUDSONBAY-731 Verify that User able to scroll the page vertically*/
	public void ChanelPLPScroll() 
	{
		ChildCreation("HUDSONBAY-731 Verify that User able to scroll the page vertically");
		if(chanelPlp == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					HBCBasicfeature.scrolldown(footerContainer, driver);
					HBCBasicfeature.scrollup(header, driver);
					Pass("User able to scroll the product list page vertically");					
				}
				else
				{
					Fail("Chanel PLP Page not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-731" +e.getMessage());
				Exception("HBC-731 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PLP Page not displayed");
		}
	}
	
	/*HUDSONBAY-732 verify that lazy loading of the products should be shown when the user scroll the Chanel PLPpage*/
	public void ChanelPLPLazyLoad() 
	{
		ChildCreation("HUDSONBAY-732 verify that lazy loading of the products should be shown when the user scroll the Chanel PLPpage");
		if(chanelPlp == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(chanelPLPProductCount))
				{
					Pass("Chanel PLP page product count displayed");
					String val = chanelPLPProductCount.getText().substring(8);
					String[] split = val.split(" ");
					int count = Integer.parseInt(split[0]);					
					Thread.sleep(500);
					log.add("Displayed available product count is: "+count);
					if(count>100)
					{
						if(HBCBasicfeature.isListElementPresent(plpItemContList))
						{
							int bfre_size = plpItemContList.size();
							log.add("Before scrolling size is "+bfre_size);
							if(bfre_size>=100)
							{		
									HBCBasicfeature.scrolldown(footerContainer, driver);
									boolean loaded = false;
									do
									{	
										int size = plpItemContList.size();
										if(size>100)
										{
											loaded=true;
										}
										else
										{
											loaded=false;
										}
										
									}while(!loaded==true);
									
								Thread.sleep(1000);
								int aftr_size =  plpItemContList.size();						
								log.add("After lazy loading size is "+aftr_size);
								if(aftr_size>bfre_size)
								{
									Pass("while scrolling the products in the product list page,the products displayed with the lazy loading",log);
								}
								else
								{
									Fail("while scrolling the products in the product list page,the products not displayed with the lazy loading",log);
								}						
							}
							else
							{
								Pass("Product displayed is less than 100, so no lazy loading",log);
							}
						}
						else
						{
							Fail("Products are not displayed in the PLP page");
						}
					}
					else
					{
						Pass("Product displayed is less than 100, so no lazy loading",log);
					}
				}
				else
				{
					Fail("Products count not displayed in the PLP page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-732" +e.getMessage());
				Exception("HBC-732 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PLPpage not displayed");
		}
	}
	
	
	/*HUDSONBAY-733 Verify that "sort" and "filter" options are displayed below the header in Chanel PLP page.*/
	public void ChanelPLPSortFilter() 
	{
		ChildCreation("HUDSONBAY-733 Verify that 'sort' and 'filter' options should be displayed below the header");
		if(chanelPlp == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(sort))
				{
					Pass("Sort option displayed below the header");
				}
				else
				{
					Fail("Sort option not displayed below the header");
				}
				if(HBCBasicfeature.isElementPresent(Filter))
				{
					Pass("Filter option displayed below the header");
				}
				else
				{
					Fail("Filter option not displayed below the header");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-733" +e.getMessage());
				Exception("HBC-733 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PLP Page not displayed");
		}
	}
	
	/*HUDSONBAY-734 Verify that Chanel PDP page should be displayed as per the creative and fit to the Screen in both potrait and Landscape*/
	public void ChanelPLPToPDP() 
	{
		ChildCreation("HUDSONBAY-734 Verify that Chanel PDP page should be displayed as per the creative and fit to the Screen in both potrait and Landscape");
		if(chanelPlp == true)
		{
			try
			{
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					int size = plpItemContList.size();
					Random r  = new Random();
					int i = r.nextInt(size);
					WebElement product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
					wait.until(ExpectedConditions.visibilityOf(product));
					HBCBasicfeature.scrolldown(product, driver);
					if(HBCBasicfeature.isElementPresent(product))
					{
						String product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+(i+1)+"]")).getText();
						String product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+(i+1)+"]")).getText().trim();
						String PLPtitle = product_BrandTitle +" "+ product_Title;
						log.add("The selected product title is: "+PLPtitle);
						Thread.sleep(1000);
						WebElement product_Clk = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])["+(i+1)+"]"));		
						product_Clk.click();
						wait.until(ExpectedConditions.visibilityOf(PDPPage));
						if(HBCBasicfeature.isElementPresent(PDPPage))
						{
							chanelPDP = true;
							String PDPBrand_title = driver.findElement(By.xpath("//*[@class='sk_chanelTitle']")).getText();
							String PDPproduct_Title = driver.findElement(By.xpath("//*[@class='prdName']//h1[1]")).getText();
							String PDPtitle = PDPBrand_title +" "+ PDPproduct_Title;
							log.add("The PDP page title is: "+PDPtitle);
							if(PDPtitle.replaceAll("\\s+","").equalsIgnoreCase(PLPtitle.replaceAll("\\s+","")))
							{
								Pass("Selecting the product in the PLP page,it navigated to the corresponding PDP page",log);
							}
							else
							{
								Fail("Selecting the product in the PLP page,not navigated to the corresponding PDP page",log);
							}
						}
						else
						{
							Fail("PDP Page not displayed");
						}
					}
					else
					{
						Fail("Selected product not displayed");
					}					
				}
				else
				{
					Fail("PLP products are not dispalyed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-734" +e.getMessage());
				Exception("HBC-734 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel Landing Page not displayed");
		}
	}
	
	/*HUDSONBAY-735 Verify that user should able to scroll the Chanel PDP page vertically*/
	public void ChanelPDPScroll() 
	{
		ChildCreation("HUDSONBAY-735 Verify that user should able to scroll the Chanel PDP page vertically");
		if(chanelPDP == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(PDPPage))
				{
					HBCBasicfeature.scrolldown(footerContainer, driver);
					HBCBasicfeature.scrollup(header, driver);
					Pass("User able to scroll the PDPPage vertically");					
				}
				else
				{
					Fail("Chanel PDP Page not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-735" +e.getMessage());
				Exception("HBC-735 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PDP Page not displayed");
		}
	}	
	
	/*HUDSONBAY-736 Verify that header should be displayed in Chanel PDP as per the creative.*/
	public void ChanelPDPHeader() 
	{
		ChildCreation("HUDSONBAY-736 Verify that header should be displayed in Chanel PDP as per the creative.");
		if(chanelPDP == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC706", sheet, 1);
				String expectedbgColor = HBCBasicfeature.getExcelVal("HBC706", sheet, 2);
				String expectedtextColor = HBCBasicfeature.getExcelVal("HBC706", sheet, 3);
				if(HBCBasicfeature.isElementPresent(header))
				{
					HBCBasicfeature.scrollup(header, driver);
					if(HBCBasicfeature.isElementPresent(hamburger))
					{
						Pass("Hamburger menu displayed in the header");
					}
					else
					{
						Fail("Hamburger menu not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(logo))
					{
						Pass("Logo displayed in the header");
					}
					else
					{
						Fail("Logo not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(searchIcon))
					{
						Pass("Search icon displayed in the header");
					}
					else
					{
						Fail("Search icon not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(bagIcon))
					{
						Pass("Shoppin Bag icon displayed in the header");
					}
					else
					{
						Fail("Shoppin Bag icon not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(chanelPLPHeader))
					{
						Pass("Chanel PDP header displayed in the header");
						String actual = chanelPLPHeader.getText();
						String bColor = chanelPLPHeader.getCssValue("background").substring(0,12);
						String bgColor = HBCBasicfeature.colorfinder(bColor);
						String txtColor = chanelPLPHeader.getCssValue("color");
						String textColor = HBCBasicfeature.colorfinder(txtColor);
						Thread.sleep(500);
						log.add("Actual string displayed is: "+actual);
						log.add("Expected string is: "+expected);
						log.add("Actual Background color displayed is: "+bgColor);
						log.add("Expected Background color is: "+expectedbgColor);
						log.add("Actual Text color displayed is: "+textColor);
						log.add("Expected Text color is: "+expectedtextColor);
						Thread.sleep(500);
						if(actual.equals(expected)&&bgColor.equalsIgnoreCase(expectedbgColor)&&textColor.equalsIgnoreCase(expectedtextColor))
						{
							Pass("Chanel Header displayed in Chanel PDP as per the creative",log);
						}
						else
						{
							Fail("Chanel Header not displayed in Chanel PDP as per the creative",log);
						}											
					}
					else
					{
						Fail("Chanel PDP header not displayed in the header");
					}
				}
				else
				{
					Fail("Header not displayed in the home page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-736" +e.getMessage());
				Exception("HBC-736 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PDP Page not displayed");
		}
	}
	
	/*HUDSONBAY-737 Verify that Footer should be displayed in Chanel PDP as per the creative.*/
	public void ChanelPDPFooter() 
	{
		ChildCreation("HUDSONBAY-737 Verify that Footer should be displayed in Chanel PDP as per the creative.");
		if(chanelPDP == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(footerTop))
				{
					Pass("Footer top panel is displayed");	
					HBCBasicfeature.scrollup(footerTop, driver);
					if(HBCBasicfeature.isElementPresent(footerTopTitle))
					{
						Pass("Footer top panel title is displayed");	
					}
					else
					{
						Fail("Footer top panel title is not displayed");						
					}
					if(HBCBasicfeature.isElementPresent(footerSubTop))
					{
						Pass("Footer sub top panel is displayed");	
						if(HBCBasicfeature.isElementPresent(footerSubTopCall))
						{
							Pass("Footer call option is displayed");		
						}
						else
						{
							Fail("Footer Call option is not displayed");						
						}
						if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
						{
							Pass("Footer Email option is displayed");	
						}
						else
						{
							Fail("Footer email option is not displayed");						
						}
					}
					else
					{
						Fail("Footer sub top panel is not displayed");						
					}
				}
				else
				{
					Fail("Footer top panel is not displayed");						
				}
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					Pass("Footer Menu List option is displayed");
				}
				else
				{
					Fail("Footer menu list is not displayed");						
				}
				if(HBCBasicfeature.isElementPresent(footerBottom))
				{
					Pass("Footer bottom panel is displayed");
					wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
					if(HBCBasicfeature.isElementPresent(footerSocialIcon))
					{
						Pass("Footer social icon options are displayed");
					}
					else
					{
						Fail("Footer social icon options are not displayed");						
					}
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{
						log.add("Footer displayed as per the creative and fit to the screen");
						Pass("Footer copyright section is displayed",log);										
					}
					else
					{
						log.add("Footer is not displayed as per the creative and fit to the screen");
						Fail("Footer copyright section is not displayed",log);	
					}
				}
				else
				{
					Fail("Footer bottom panel is not displayed");						
				}			
				HBCBasicfeature.scrollup(header, driver);
			}
			catch (Exception e)
			{
				System.out.println("HBC-737" +e.getMessage());
				Exception("HBC-737 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PDP Page not displayed");
		}
	}	
	
	/*HUDSONBAY-738 Verify that product image should be displayed (without an image border) below the page header.*/
	public void ChanelPDPImage() 
	{
		ChildCreation("HUDSONBAY-738 Verify that product image should be displayed (without an image border) below the page header");
		if(chanelPDP == true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdpPageProductImage));
				HBCBasicfeature.scrollup(pdpPageProductImage, driver);
				if(HBCBasicfeature.isElementPresent(pdpPageProductImage))
				{
					Pass("The image container is present for the selected product.");
					WebElement imageContainer = driver.findElement(By.xpath("//*[@id='id_pdpMasterImageCont']"));
					List<WebElement> img = imageContainer.findElements(By.tagName("img"));
					log.add("Total Images present for the Product is: "+img.size());
					for(WebElement image : img)
					{
						int status = HBCBasicfeature.imageBroken(image,log);
						if(status==200)
						{
							Pass("The displayed image for the product is Valid Image and it is not broken.", log);
						}
						else
						{
							Fail("The displayed image for the product is Invaalid Image and it is broken.", log);
						}
					}
				}
				else
				{
					Fail("The image container is not found for the selected product.");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-738" +e.getMessage());
				Exception("HBC-738 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PDP Page not displayed");
		}
	}
	
	/*HUDSONBAY-741 Verify that by default the first image should be displayed in the Chanel PDP.*/
	public void ChanelPDPDefaultImage() 
	{
		ChildCreation("HUDSONBAY-741 Verify that by default the first image should be displayed in the Chanel PDP.");
		if(chanelPDP == true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdpcollectionImageSelected));
				if(HBCBasicfeature.isElementPresent(pdpcollectionImageSelected))
				{
					String position = pdpcollectionImageSelected.getAttribute("index");
					int pos = Integer.parseInt(position);
					log.add("The selected image position is: "+position);
					if(pos==1)
					{
						Pass("By default the first image displayed",log);
					}
					else
					{
						Fail("By default the first image not displayed",log);
					}					
				}
				else
				{
					Fail("Image not selected by defalut");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-741" +e.getMessage());
				Exception("HBC-741 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PDP Page not displayed");
		}
	}
	
	/*HUDSONBAY-743 Verify that "Favorite" icon should be displayed in the Chanel PDP page*/
	/*HUDSONBAY-744 Verify that 'Favourite' icon should be displayed in the top right corner of the Chanel PDP page*/
	public void ChanelPDPFavIcon() 
	{
		ChildCreation("HUDSONBAY-743 Verify that 'Favorite' icon should be displayed in the Chanel PDP page");
		if(chanelPDP == true)
		{
			boolean HBC744 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(pdpWishListIcon))
				{
					Pass("'Favorite' icon displayed in the Chanel PDP page");
					 HBC744 = true;
				}
				else
				{
					Fail("'Favorite' icon displayed in the Chanel PDP page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-743" +e.getMessage());
				Exception("HBC-743 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-744 Verify that 'Favourite' icon should be displayed in the top right corner of the Chanel PDP page");
			if( HBC744 == true)
			{
				Pass("'Favourite' icon displayed in the top right corner of the Chanel PDP page");
			}
			else
			{
				Fail("'Favourite' icon not displayed in the top right corner of the Chanel PDP page");
			}
		}
		else
		{
			Skip("Chanel PDP Page not displayed");
		}
	}
	
	/*HUDSONBAY-746 Verify that product name should be displayed below the secondary images in Chanel PDP.*/
	public void ChanelPDPProdName() 
	{
		ChildCreation("HUDSONBAY-746 Verify that product name should be displayed below the secondary images in Chanel PDP.");
		if(chanelPDP == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(PDPProdName))
				{
					String PDPtitle = PDPProdName.getText();
					log.add("The PDP page title is: "+PDPtitle);
					Pass("Product name displayed below the secondary images in Chanel PDP",log);					
				}
				else
				{
					Fail(" PDP page title is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-746" +e.getMessage());
				Exception("HBC-746 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PDP Page not displayed");
		}
	}
	
	/*HUDSONBAY-748 Verify that Accordions should be displayed below the product notes in Chanel PDP.*/
	public void ChanelPDPAccordians() 
	{
		ChildCreation("HUDSONBAY-748 Verify that Accordions should be displayed below the product notes in Chanel PDP.");
		if(chanelPDP == true)
		{
			try
			{
				if(HBCBasicfeature.isListElementPresent(pdppageDescriptionAccords))
				{
					int size = pdppageDescriptionAccords.size();
					log.add("Accordians present below theproduct notes is: "+size);
					for(int i=1;i<=size;i++)
					{
						String prodDesc = driver.findElement(By.xpath("(//*[contains(@class,'pdp_Description')]//h1)["+i+"]")).getText();
						Thread.sleep(200);
						boolean prodDescAccord = driver.findElement(By.xpath("//*[contains(@class,'pdp_Description')]["+i+"]//*[@class='pdp_DetailsLink']")).isDisplayed();
						if(prodDescAccord==true)
						{
							Pass("Accordion shown below the 'product notes'  is: "+prodDesc,log);
						}
						else
						{
							Fail("Accordian not displayed",log);
						}
					}					
				}
				else
				{
					Fail("Product Accordians not displayed below the ATB button");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-748" +e.getMessage());
				Exception("HBC-748 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Chanel PDP Page not displayed");
		}
	}
	
		/*HUDSONBAY-750 Verify that 1st Accordion(Details) should be displayed below the product notes in Chanel PDP..*/
		public void ChanelPDPDetailsAccordian() 
		{
			ChildCreation("HUDSONBAY-750 Verify that 1st Accordion(Details) should be displayed below the product notes in Chanel PDP.");
			if(chanelPDP == true)
			{
				boolean HBC747 = false;
				try
				{
					if(HBCBasicfeature.isListElementPresent(pdppageDescriptionAccords))
					{
						int size = pdppageDescriptionAccords.size();
						log.add("Accordians present below theproduct notes is: "+size);
						int cnt = 0;
						for(int i=1;i<=size;i++)
						{
							cnt++;
							String prodDesc = driver.findElement(By.xpath("(//*[contains(@class,'pdp_Description')]//h1)["+i+"]")).getText();
							log.add("Accordian name is: "+prodDesc);
							if(prodDesc.equalsIgnoreCase("Details")&&cnt==1)
							{
								Pass("1st Accordion(Details) displayed below the product notes in Chanel PDP",log);
								HBC747 = true;
							}
							else
							{
								Fail("1st Accordion(Details) not displayed below the product notes in Chanel PDP",log);
							}
						}					
					}
					else
					{
						Fail("Product Accordians not displayed below the ATB button");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-750" +e.getMessage());
					Exception("HBC-750 There is something wrong. Please Check." + e.getMessage());
				}
				
				ChildCreation("HUDSONBAY-747 Verify that product notes should be displayed below the product name in Chanel PDP..");
				if(HBC747 == true)
				{
					Pass("Product notes displayed below the product name in Chanel PDP");
				}
				else
				{
					Fail("Product notes not displayed below the product name in Chanel PDP");
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-753 Verify that all the accordion in the Chanel PDP page should be in expanded  state by default*/
		/*HUDSONBAY-756 Verify that 'X' icon should be shown, when the accordion is in enabled state in Chanel PDP*/
		public void ChanelAccordDefaultState() 
		{
			ChildCreation("HUDSONBAY-753 Verify that all the accordion in the Chanel PDP page should be in expanded  state by default");
			boolean HBC756 = false;
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
					{						
						Pass("Accordion in the Chanel PDP page in expanded state by default");
						if(HBCBasicfeature.isElementPresent(pdppageDetailsExpandCls))
						{
							HBC756 = true;
						}
					}					
					else
					{
						Fail("Accordion in the Chanel PDP page not in expanded state by default");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-753" +e.getMessage());
					Exception("HBC-753 There is something wrong. Please Check." + e.getMessage());
				}
				
				ChildCreation("HUDSONBAY-756 Verify that 'X' icon should be shown, when the accordion is in enabled state in Chanel PDP");
				if(HBC756==true)
				{
					Pass("'X' icon shown, when the accordion is in enabled state in Chanel PDP");
				}
				else
				{
					Fail("'X' icon not shown, when the accordion is in enabled state in Chanel PDP");
				}				
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
						
		/*HUDSONBAY-754 Verify that accordion should be closed on tapping the opened state accordion again or tapping the collapsed state accordion in Chanel PDP*/
		/*HUDSONBAY-755 Verify that "+" icon should be shown, when the accordion is in disabled state in Chanel PDP*/
		public void ChanelAccordClose() 
		{
			ChildCreation("HUDSONBAY-754 Verify that accordion should be closed on tapping the opened state accordion again or tapping the collapsed state accordion in Chanel PDP");
			if(chanelPDP == true)
			{
				boolean HBC755 = false;
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
					{						
						Pass("Accordion in the Chanel PDP page in expanded state by default");
						if(HBCBasicfeature.isElementPresent(pdppageDetailsExpandCls))
						{
							Pass("Expand/Collapse accordian displayed");
							pdppageDetailsExpandCls.click();
							Thread.sleep(500);
							if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
							{
								Fail("Accordion not closed on tapping the opened state accordion in Chanel PDP");
							}
							else
							{
								Pass("Accordion closed on tapping the opened state accordion again or tapping the collapsed state accordion in Chanel PDP");
								if(HBCBasicfeature.isElementPresent(pdppageDetailsExpandCls))
								{
									HBC755 = true;
								}
							}							
						}
						else
						{
							Fail("Expand/Collapse accordian not displayed");
						}						
					}					
					else
					{
						Fail("Accordion in the Chanel PDP page not in expanded state by default");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-754" +e.getMessage());
					Exception("HBC-754 There is something wrong. Please Check." + e.getMessage());
				}
				
				ChildCreation("HUDSONBAY-755 Verify that "+" icon should be shown, when the accordion is in disabled state in Chanel PDP");
				if(HBC755 == true)
				{
					Pass("'+' icon shown, when the accordion is in disabled state in Chanel PDP");
				}
				else
				{
					Fail("'+' icon not shown, when the accordion is in disabled state in Chanel PDP");
				}				
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-903 Verify that Web id should be shown in the details section.*/
		public void ChanelPDPWebID() 
		{
			ChildCreation("HUDSONBAY-903 Verify that Web id should be shown in the details section.");
			if(chanelPDP == true)
			{
				try
				{					
					if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
					{	
						Pass("Details Accordian in Expanded state");
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(pdppageDetailsExpandCls))
						{
							pdppageDetailsExpandCls.click();
							Thread.sleep(500);
							if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
							{
								Pass("Details Accordian in Expanded state");
							}
						}
					}
					if(HBCBasicfeature.isElementPresent(pdpWebId))
					{
						String[] webID = pdpWebId.getText().split(":");
						log.add("Displayed webID is: "+webID[1]);
						boolean res = webID[1].isEmpty();
						if(res==false)
						{
							Pass(" Web Id shown in the details section",log);
						}
						else
						{
							Fail(" Web Id not shown in the details section",log);
						}
					}
					else
					{
						Fail(" Web Id not shown in the details section");
					}
					
					
				}
				catch (Exception e)
				{
					System.out.println("HBC-903" +e.getMessage());
					Exception("HBC-903 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-758 Verify that HBC Rewards points should be displayed below the ADD to BAG button under details section.*/
		public void ChanelPDP_HBCRewards() 
		{
			ChildCreation("HUDSONBAY-758 Verify that HBC Rewards points should be displayed below the ADD to BAG button under details section.");
			if(chanelPDP == true)
			{
				try
				{						
					if(HBCBasicfeature.isElementPresent(pdppageDetailsReward))
					{
						String[] reward = pdppageDetailsReward.getText().split(":");
						log.add("Displayed HBC Rewards point is: "+reward[1]);
						boolean res = reward[1].isEmpty();
						if(res==false)
						{
							Pass(" HBC Rewards point shown under the details section",log);
						}
						else
						{
							Fail("HBC Rewards point  not shown under the details section",log);
						}
					}
					else
					{
						Fail(" HBC Rewards point  not shown in the details section");
					}
					
					
				}
				catch (Exception e)
				{
					System.out.println("HBC-758" +e.getMessage());
					Exception("HBC-758 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-759 Verify that "Sale" price, "Actual" price should be shown below the product image in Chanel PDP*/
		/*HUDSONBAY-760 Verify that Product has only one price then no original/sale should be shown in Chanel PDP*/
		public void ChanelPDPPrice() 
		{
			ChildCreation("HUDSONBAY-759 Verify that 'Sale' price, 'Actual' price should be shown below the product image in Chanel PDP");
			boolean HBC760 = false;
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppagePriceRegular)&&HBCBasicfeature.isElementPresent(pdppagePriceSale))
					{
						String salePrice = pdppagePriceSale.getText();
						String RegPrice = pdppagePriceRegular.getText();
						log.add("The Regular price is: "+RegPrice+" and The Sale price is: "+salePrice);
						Pass("'Sale' price, 'Actual' price shown below the product image in Chanel PDP",log);					
					}
					else if(HBCBasicfeature.isElementPresent(pdppagePriceRegular))
					{
						String RegPrice = pdppagePriceRegular.getText();
						log.add("The Regular price is: "+RegPrice);
						Pass("Regular Price alone displayed below the product image in Chanel PDP",log);
						HBC760 = true;
					}
					else
					{
						Fail("Regular Price/ 'Actual' price not shown below the product image in Chanel PDP ");
					}											
				}
				catch (Exception e)
				{
					System.out.println("HBC-759" +e.getMessage());
					Exception("HBC-759 There is something wrong. Please Check." + e.getMessage());
				}
				
				ChildCreation("HUDSONBAY-760 Verify that Product has only one price then no original/sale should be shown in Chanel PDP");
				if(HBC760==true)
				{
					if(HBCBasicfeature.isElementPresent(pdppagePriceRegular))
					{
						String RegPrice = pdppagePriceRegular.getText();
						log.add("The Regular price is: "+RegPrice);
						Pass("Product has only one price then no original/sale shown in Chanel PDP",log);
					}
					else
					{
						Fail("Product has only one price then no original/sale not shown in Chanel PDP",log);
					}
				}
				else
				{
					Skip("Product has both original/sale not shown in Chanel PDP");				
				}		
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*Chanel to PLP with swatches*/
		public void ChanelSwatch() throws Exception 
		{
			ChildCreation("Chanel to PLP with swatches");
			chanelPDP = true;
			if(chanelPDP == true)
			{
				String Key = HBCBasicfeature.getExcelVal("chanel", sheet, 1);
				String[] expectedAccords = HBCBasicfeature.getExcelVal("HBC717", sheet, 1).split("\n");
				String[] categories = HBCBasicfeature.getExcelVal("HBC717", sheet, 3).split("\n");

				String accordName = ""; int i=0,j=0,k=0; WebElement accordClick = null;
				Actions act = new Actions(driver);
				try
				{
					wait.until(ExpectedConditions.visibilityOf(searchIcon));
					if(HBCBasicfeature.isElementPresent(searchIcon))
					{
						searchIcon.click();
						wait.until(ExpectedConditions.visibilityOf(searchBox));
						if(HBCBasicfeature.isElementPresent(searchBox))
						{
							searchBox.sendKeys(Key);
							searchBox.sendKeys(Keys.ENTER);
							wait.until(ExpectedConditions.attributeContains(chanelLanding, "style", "100%"));
							if(HBCBasicfeature.isElementPresent(chanelLanding))
							{
								Pass("Chanel Landing page displayed");		
								if(HBCBasicfeature.isListElementPresent(chanelCategory))
								{
									Pass("Chanel sections/categories are displayed");				
									boolean flag = false;
									do
									{
										try
										{
											i++;
											accordName = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']//*[@class='sk_mobCategoryMenuItemTxt'])["+i+"]")).getText(); 
											Thread.sleep(200);
											if(accordName.equalsIgnoreCase(expectedAccords[2]))
											{
												flag = true;
												break;
											}
										}
										catch(Exception e)
										{
											continue;
										}
									}while(flag!=true);
									
									log.add("Selected Accord Name is: "+accordName);
									accordClick = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+i+"]"));
									if(HBCBasicfeature.isElementPresent(accordClick))
									{
										act.moveToElement(accordClick).click().build().perform();
										Thread.sleep(1000);
										wait.until(ExpectedConditions.visibilityOf(chanelCategoryOpenState));
										if(HBCBasicfeature.isElementPresent(chanelCategoryOpenState))
										{						
											Pass("Accordian expands On Tapping the closed state accordian",log);
											boolean subflag = false;
											do
											{
												try
												{
													j++;
													String subCatName = driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']["+i+"])//*[@id='sk_mobCategoryItem_id__1']["+j+"]//*[@class='sk_mobCategoryMenuItemTxt   ']")).getText(); 
													Thread.sleep(200);
													if(subCatName.equalsIgnoreCase(categories[0]))
													{
														subflag = true;
														break;
													}
												}
												catch(Exception e)
												{
													continue;
												}
											}while(subflag!=true);
											WebElement subCatClick =  driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+i+"]//*[@class='sk_mobSubCategory']//*[@id='sk_mobCategoryItem_id__1']["+j+"]"));
											if(HBCBasicfeature.isElementPresent(subCatClick))
											{
												act.moveToElement(subCatClick).click().build().perform();
												Thread.sleep(1000);
												WebElement subSubCat = driver.findElement(By.xpath("((//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+i+"]//*[@class='sk_mobSubCategory']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@class='sk_mobSubCategory ']"));
												wait.until(ExpectedConditions.attributeContains(subSubCat, "style", "block"));
												if(HBCBasicfeature.isElementPresent(subSubCat))
												{
													Pass("Sub Sub sections/cateogries displayed");	
													boolean subsubflag = false;
													do
													{
														try
														{
															k++;
															String subsubCatName = driver.findElement(By.xpath("((//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem']["+i+"])//*[@id='sk_mobCategoryItem_id__1']["+j+"]//*[@id='sk_mobCategoryItem_id__2']//*[@class='sk_mobCategoryMenuItemTxt   '])["+k+"]")).getText(); 
															Thread.sleep(200);
															if(subsubCatName.equalsIgnoreCase(categories[1]))
															{
																subsubflag = true;
																break;
															}
														}
														catch(Exception e)
														{
															continue;
														}
													}while(subsubflag!=true);
													WebElement subsubCatClick =  driver.findElement(By.xpath("(//*[@widgettype='hbcCategoryMenu']//*[@class='sk_mobCategoryMenuItem'])["+i+"]//*[@class='sk_mobSubCategory']//*[@id='sk_mobCategoryItem_id__1']["+j+"]//*[@class='sk_mobSubCategory ']//*[@id='sk_mobCategoryItem_id__2']["+k+"]"));
													if(HBCBasicfeature.isElementPresent(subsubCatClick))
													{
														act.moveToElement(subsubCatClick).click().build().perform();
														wait.until(ExpectedConditions.visibilityOf(plpPage));
														if(HBCBasicfeature.isElementPresent(plpPage))
														{							
															Pass("Selecting the Sub Sub(sections/cateogries) from Chanel landing page, corresponding Chanel PLP shown");
															if(HBCBasicfeature.isListElementPresent(chanelPLPswatchContainer))
															{
																Pass("Products displayed in Chanel PLP page has color Swatches");
																chanelPLPSwatches = true;
															}
															else
															{
																Skip("Products displayed in Chanel PLP page has no color Swatches");																
															}															
														}
														else
														{
															Fail("Selecting the Sub Sub(sections/cateogries) from Chanel landing page, corresponding Chanel PLP not shown");
														}
													}
													else
													{
														Fail("Sub Sub(sections/cateogries) not displayed to click");
													}
												}
												else
												{
													Fail("Sub Sub(sections/cateogries) not displayed");
												}
											}
											else
											{
												Fail("Sub (sections/cateogries) not displayed");
											}
										}
										else
										{
											Fail("Accordian not expands On Tapping the closed state accordian",log);					
										}
									}
									else
									{
										Fail("Selected sections/categories not dispalyed");
									}
								}
								else
								{
									Fail("Chanel sections/categories are not displayed");
								}			
								
							}
							else
							{
								Fail("Chanel Landing page not displayed");
							}
						}
						else
						{
							Fail("Search Box not displayed");
						}
					}
					else
					{
						Fail("Search Icon not displayed");
					}			
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-763 Verify that color swatches should be displayed below the product title in Chanel PDP*/
		public void ChanelPDPSwatches() throws Exception 
		{
			try
			{
				ChanelSwatch();
				ChildCreation("HUDSONBAY-763 Verify that color swatches should be displayed below the product title in Chanel PDP");
				if(chanelPLPSwatches == true)
				{
					if(HBCBasicfeature.isListElementPresent(plpItemContList))
					{
						int size = plpItemContList.size();
						for(int i=1;i<=size;i++)
						{
							boolean swatch  = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='sk_prdswatch']")).isDisplayed();
							if(swatch==true)
							{
								Pass("color swatches displayed in the product");
								WebElement product_Clk = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])"));		
								if(HBCBasicfeature.isElementPresent(product_Clk))
								{
									HBCBasicfeature.scrolldown(product_Clk, driver);
									Thread.sleep(500);
									product_Clk.click();
									wait.until(ExpectedConditions.visibilityOf(PDPPage));
									if(HBCBasicfeature.isElementPresent(PDPPage))
									{
										Pass("PDP page displayed");
										chanelPDP = true;
										if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
										{
											Pass("color swatches displayed below the product title in Chanel PDP");
											break;
										}
										else
										{
											Fail("color swatches not displayed below the product title in Chanel PDP");
										}
									}
									else
									{
										Fail("PDP page not displayed");
									}
								}
								else
								{
									Fail("Product not displayed in PLP page");
								}
							}
							else
							{
								continue;
							}
						}
					}
					else
					{
						Fail("Product not displayed in PLP page");
					}
				}
				else
				{
					Fail("Products in PLP not have swatches");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-763" +e.getMessage());
				Exception("HBC-763 There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
		/*HUDSONBAY-766 Verify that name of the color that is selected should be displayed in Chanel PDP*/
		public void ChanelPDPSelectedColor() 
		{
			ChildCreation("HUDSONBAY-766 Verify that name of the color that is selected should be displayed in Chanel PDP");
			if(chanelPDP == true)
			{
				try
				{
					Random r = new Random();
					int size = pdpPageSwatchesList.size();
					int i = r.nextInt(size);
					WebElement colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)["+(i+1)+"]"));
					colorSwatch.click();
					Thread.sleep(500);
					String selectedColor = colorSwatch.getAttribute("aria-label");
					log.add("The actual Tile color is: "+selectedColor);
					String displayedColor = pdpPageSwatchSelectedColor.getText();
					log.add("The Displayed Selected Tile color is: "+displayedColor);
					if(selectedColor.equalsIgnoreCase(displayedColor))
					{
						Pass("Name of the color that is selected in Chanel PDP is displayed",log);
					}
					else
					{
						Fail("Name of the color that is selected in Chanel PDP is not displayed",log);
					}				
				}
				catch (Exception e)
				{
					System.out.println("HBC-766" +e.getMessage());
					Exception("HBC-766 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		
		/*HUDSONBAY-767 Verify that on changing the color the image should be updated as per the color is selected in Chanel PDP*/
		public void ChanelPDPColorChange() 
		{
			ChildCreation("HUDSONBAY-767 Verify that on changing the color the image should be updated as per the color is selected in Chanel PDP");
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
					{
						Thread.sleep(500);
						int size = pdpPageSwatchesList.size();
						String activeColor = pdpPageProductImage.getAttribute("imgclrname");
						log.add("The current color of product is: "+activeColor);
						String activeUrl = pdpPageProductImage.getAttribute("src");
						log.add("The current Url of product is: "+activeUrl);
						for(int i=1;i<size;i++)
						{
							WebElement colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)["+(i+1)+"]"));
							colorSwatch.click();
							Thread.sleep(1000);
							String CurrentColor = pdpPageProductImage.getAttribute("imgclrname");
							log.add("After changed The color of product is: "+CurrentColor);
							String CurrentUrl = pdpPageProductImage.getAttribute("src");
							log.add("After changed The color of product is: "+CurrentUrl);
							if(activeColor.equalsIgnoreCase(CurrentColor))
							{
								Fail("Changing the color, respective color not changed",log);
							}
							else
							{
								Pass("Respective color changed",log);
								if(activeUrl.equalsIgnoreCase(CurrentUrl))
								{
									Fail("On changing the color, the image not updated",log);
								}
								else
								{
									Pass("On changing the color, the image gets updated",log);
								}
							}
						}
					}
					else
					{
						Fail("PDP swatches container not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-767" +e.getMessage());
					Exception("HBC-767 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-711 Verify that "Size Swatches" should be shown below the product price in the chanel pdp*/
		public void ChanelPDPSizeSwatches() 
		{
			ChildCreation("HUDSONBAY-711 Verify that 'Size Swatches' should be shown below the product price in the chanel pdp");
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageSizeContainer))
					{
						int size = pdppageSizeContainerList.size();
						for(int i=1;i<=size;i++)
						{
							String ssize = driver.findElement(By.xpath("//*[contains(@class,'pdpSizeSelect pdpSelectEle')]["+i+"]")).getText();
							log.add("The displayed size is: "+ssize);
						}
						Pass("Size container displayed below the product price in the chanel pdp",log);
					}
					else
					{
						Fail("Size container not displayed below the product price in the chanel pdp",log);
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-711" +e.getMessage());
					Exception("HBC-711 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-714 Verify that Selected size name string(eg : SELECT SIZE: XXXXXXXXX) should be shown near the size swatches/dropdown*/
		public void ChanelPDPSizeString() 
		{
			ChildCreation("HUDSONBAY-714 Verify that Selected size name string(eg : SELECT SIZE: XXXXXXXXX) should be shown near the size swatches/dropdown");
			if(chanelPDP == true)
			{
				try
				{
					String[] expected = HBCBasicfeature.getExcelVal("HBC816", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(pdppageSize))
					{
						Pass("PDP size container displayed");
						HBCBasicfeature.scrolldown(pdppageSize, driver);						
							try
							{
								String defTxt = pdppageSize.getText();
								String[] sizeName = defTxt.split(":");
								log.add("Actual Selected SIZE name String is: "+defTxt);
								log.add("Expected Selected SIZE name String is: "+expected[0]+" or "+expected[1]);
								log.add("Displayed SIZE is: "+sizeName[1]);
								if((sizeName[0].trim().contains(expected[0].trim())||sizeName[0].trim().contains(expected[1].trim()))&&!sizeName[1].isEmpty())
								{
									Pass("(SELECT SIZE: XXXXXXXXX) shown near the SIZE swatches/dropdown for the product",log);
								}
								else
								{
									Fail("(SELECT SIZE: XXXXXXXXX) not shown near the SIZE swatches/dropdown for the product",log);
								}
							}
							catch (Exception e)
							{
								Fail("SIZE swatches not displayed for the subProduct ");
							}
						}
						else
						{
							Fail("PDP size container not displayed");
						}
					}				
					catch (Exception e)
					{
						System.out.println("HBC-714" +e.getMessage());
						Exception("HBC-714 There is something wrong. Please Check." + e.getMessage());
					}
				}
				else
				{
					Skip("Chanel PDP Page not displayed");
				}
			}	
		
		/*HUDSONBAY-715 Verify that difference needs to be shown for selected/unselected color/size swatches*/
		public void ChanelColorSwatchesDiff() 
		{
			ChildCreation("HUDSONBAY-715 Verify that difference needs to be shown for selected/unselected color/size swatches");
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isListElementPresent(pdpPageSwatchesList))
					{
						int size = pdpPageSwatchesList.size();
						log.add("Total displayed color swatches is: "+size);						
						if(size>1)
						{														
							WebElement colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)[1]"));
							colorSwatch.click();
							String selected = pdppageSwatchColorSelected.getCssValue("border").substring(12);
							String selectedColor = HBCBasicfeature.colorfinder(selected);
							log.add("Selected color swatches default color is: "+selectedColor);
							colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)[2]"));
							colorSwatch.click();
							Thread.sleep(1000);
							String unselected = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)[1]")).getCssValue("border").substring(9);
							String unSelectedColor = HBCBasicfeature.colorfinder(unselected);
							log.add("Un Selected color swatches default color is: "+unSelectedColor);
							if(selectedColor.equalsIgnoreCase(unSelectedColor))
							{
								Fail("Difference not shown for selected/unselected color/size swatches",log);
							}
							else
							{
								Pass("Difference shown for selected/unselected color/size swatches",log);
							}
						}
						else
						{
							Skip("Not more than one Color swatched displayed");
						}					
					}
					else
					{
						Fail("Swatches not dispalyed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-715" +e.getMessage());
					Exception("HBC-715 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-771 Verify that Quantity increment(+) or decrement(-) buttons should be displayed below the product price in Chanel PDP.*/
		public void ChanelpdpQuantityContainer() 
		{
			ChildCreation("HUDSONBAY-771 Verify that Quantity increment(+) or decrement(-) buttons should be displayed below the product price in Chanel PDP.");
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
					{
						Pass("The Product Quantity container displayed");
						if(HBCBasicfeature.isElementPresent(pdppageQuantityMinus))
						{
							Pass(" Quantity decrement(-) button displayed below the product price");
						}
						else
						{
							Fail("Quantity decrement(-) button not displayed below the product price");
						}
						if(HBCBasicfeature.isElementPresent(pdppageQuantityPlus))
						{
							Pass(" Quantity increment(+) button displayed below the product price");
						}
						else
						{
							Fail(" Quantity increment(+) button not displayed below the product price");
						}
					}
					else
					{
						Fail("The Product Quantity container not displayed");
					}			
				}
				catch (Exception e)
				{
					System.out.println("HBC-771" +e.getMessage());
					Exception("HBC-771 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-772 Verify that Quantity should be incremented on selecting the Quantity increment button("+") in Chanel PDP*/
		public void ChanelpdpQuantityIncrement() 
		{
			ChildCreation("HUDSONBAY-772 Verify that Quantity should be incremented on selecting the Quantity increment button("+") in Chanel PDP");
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
					{
						String val = pdppageQuantityText.getAttribute("value");
						log.add("The current Quantity is: "+val);
						if(HBCBasicfeature.isElementPresent(pdppageQuantityPlus))
						{
							pdppageQuantityPlus.click();
							String valPlus = pdppageQuantityText.getAttribute("value");
							log.add("The updated Quantity is: "+valPlus);
							if(val.equals(valPlus))
							{
								Fail(" Quantity not incremented on selecting the Quantity increment button("+")",log);
							}
							else
							{
								Pass("Quantity incremented on selecting the Quantity increment button("+")",log);
							}
						}
						else
						{
							Fail("Quantity increment button("+") is displayed");
						}
					}
					else
					{
						Fail("Quantity Container not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-772" +e.getMessage());
					Exception("HBC-772 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-773 Verify that Quantity should be deccremented on selecting the Quantity decrement button("-") in Chanel PDP*/
		public void ChanelpdpQuantityDecrement() 
		{
			ChildCreation("HUDSONBAY-773 Verify that Quantity should be deccremented on selecting the Quantity decrement button('-') in Chanel PDP");
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
					{
						String val = pdppageQuantityText.getAttribute("value");
						log.add("The current Quantity is: "+val);
						if(HBCBasicfeature.isElementPresent(pdppageQuantityMinus))
						{
							pdppageQuantityMinus.click();
							String valPlus = pdppageQuantityText.getAttribute("value");
							log.add("The updated Quantity is: "+valPlus);
							if(val.equals(valPlus))
							{
								Fail(" Quantity not decremented on selecting the Quantity decrement button('-')",log);
							}
							else
							{
								Pass("Quantity decremented on selecting the Quantity decrement button('-')",log);
							}
						}
						else
						{
							Fail("Quantity decrement button('-') is displayed");
						}
					}
					else
					{
						Fail("Quantity Container not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-773" +e.getMessage());
					Exception("HBC-773 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-774 Verify that User can add a maximum of 5 at a time & 100 overall products based on the client requirements in Chanel PDP*/
		public void ChanelPDPMaximumQuantity() 
		{
			ChildCreation("HUDSONBAY-774 Verify that User can add a maximum of 5 at a time & 100 overall products based on the client requirements in Chanel PDP");
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
					{
						if(HBCBasicfeature.isElementPresent(pdppageQuantityPlus))
						{							
							int size = 6;
							for(int i = 1;i<=size;i++)
							{
								try
								{
									pdppageQuantityPlus.click();
								}
								catch(Exception e)
								{
									String valPlus = pdppageQuantityText.getAttribute("value").trim();
									int k = Integer.parseInt(valPlus);
									log.add("The updated Quantity is: "+valPlus);
									if(k==5)
									{
										Pass("User can able to add/update maximum of 5 products based on the client requirements and as per the classic site",log);
										break;
									}
									else
									{
										Fail("User not able to add/update maximum of 5 products based on the client requirements and as per the classic site",log);
									}			
								}
							}									
						}
						else
						{
							Fail("Quantity plus option not displayed");
						}						
					}
					else
					{
						Fail("PDP Quantity container not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-774" +e.getMessage());
					Exception("HBC-774 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-775  Verify that Add to bag button should be displayed as per the creative in Chanel PDP*/
		public void ChanelPDPAddToBag() throws Exception 
		{
			ChildCreation("HUDSONBAY-775  Verify that Add to bag button should be displayed as per the creative in Chanel PDP");
			if(chanelPDP == true)
			{
				String color = HBCBasicfeature.getExcelVal("HBC706", sheet, 2);
				try
				{
					wait.until(ExpectedConditions.visibilityOf(pdppageATB));
					HBCBasicfeature.scrolldown(pdppageATB, driver);
					if(HBCBasicfeature.isElementPresent(pdppageATB))
					{
						Pass("Add To Bag button displayed");
						String Colour = pdppageATB.getCssValue("background");					
						String bgColor = HBCBasicfeature.colorfinder(Colour.substring(0,12));		
						log.add("Actual color is: "+bgColor);
						log.add("Expected color is: "+color);
						Thread.sleep(500);
						if(bgColor.contains(color))
						{
							Pass("Add to bag button displayed as per the creative",log);
						}
						else
						{
							Fail("Add to bag button not displayed as per the creative",log);
						}
					}
					else
					{
						Fail("Add to bag button not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-775" +e.getMessage());
					Exception("HBC-775 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-776 Verify that Added to Bag success overlay should be shown on selecting the Add to bag button in Chanel PDP*/
		public void Chanelpdp_AddToBagClick() 
		{
			ChildCreation("HUDSONBAY-776 Verify that Added to Bag success overlay should be shown on selecting the Add to bag button in Chanel PDP");
			if(chanelPDP == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageATB))
					{
						HBCBasicfeature.scrollup(pdppageATB, driver);
						log.add("Add To Bag button displayed");
						pdppageATB.click();
						Thread.sleep(1500);
						try
						{
							WebDriverWait wait = new WebDriverWait(driver,10);
							wait.until(ExpectedConditions.visibilityOf(ATBAlert));
							if(HBCBasicfeature.isElementPresent(ATBAlert))
							{
								log.add("Limited quantities alert displayed");
								int val = 0;
								ATBAlertOk.click();
								do
								{
									pdppageQuantityMinus.click();	
									Thread.sleep(200);
									String valPlus = pdppageQuantityText.getAttribute("value");
									val = Integer.parseInt(valPlus);
								}
								while(val!=1);
								pdppageATB.click();
							}
						}
						catch (Exception e)
						{
							log.add("Limited quantities alert not displayed");						
						}					
						wait.until(ExpectedConditions.attributeContains(pdppageATBOverlay, "style", "block"));
						if(HBCBasicfeature.isElementPresent(pdppageATBOverlay))
						{
							Pass("Added to Bag success overlay shown on selecting the Add to bag button",log);
							pdppageATBOverlayClose.click();
						}
						else
						{
							Fail("Added to Bag success overlay not shown on selecting the Add to bag button",log);
						}
					}
					else
					{
						Fail("Add to bag button not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HBC-776" +e.getMessage());
					Exception("HBC-776 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}
		
		/*HUDSONBAY-777  Verify that social icons such as "Facebook", "Google+", "Twitter" and so on should be shown for hitting the like for the product in chanel PDP*/
		public void Chanelpdp_SocialIcons() 
		{
			ChildCreation("HUDSONBAY-777  Verify that social icons such as 'Facebook', 'Google+', 'Twitter' and so on should be shown for hitting the like for the product in chanel PDP");
			if(chanelPDP == true)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(pdppageShareIcons));
					HBCBasicfeature.scrolldown(pdppageShareIcons, driver);
					if(HBCBasicfeature.isElementPresent(pdppageShareIcons))
					{
						if(HBCBasicfeature.isElementPresent(pdppageShareTwitter))
						{
							Pass("Social Icon Twitter displayed");
						}
						else
						{
							Fail("Social Icon Twitter not displayed");
						}
						if(HBCBasicfeature.isElementPresent(pdppageShareGplus))
						{
							Pass("Social Icon Google+ displayed");
						}
						else
						{
							Fail("Social Icon Google+ not displayed");
						}
						if(HBCBasicfeature.isElementPresent(pdppageShareFB))
						{
							Pass("Social Icon FB+ displayed");
						}
						else
						{
							Fail("Social Icon FB not displayed");
						}
					}
					else
					{
						Fail("PDP share Icons container not displayed");
					}				
				}
				catch (Exception e)
				{
					System.out.println("HBC-777 " +e.getMessage());
					Exception("HBC-777  There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Chanel PDP Page not displayed");
			}
		}	
	}

	
