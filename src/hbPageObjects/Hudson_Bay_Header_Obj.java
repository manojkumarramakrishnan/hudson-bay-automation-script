package hbPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbConfig.HBConstants;
import hbPages.Hudson_Bay_Header;

public class Hudson_Bay_Header_Obj extends Hudson_Bay_Header implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_Header_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ""+promoBannerr+"")
	WebElement promoBanner;
	
	@FindBy(xpath = ""+homepageDomm+"")
	WebElement homepageDom;	
	
	@FindBy(xpath = ""+promocarousell+"")
	WebElement promocarousel;	

	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+maskk+"")
	WebElement mask;	
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath = ""+bagIconCountt+"")
	WebElement bagIconCount;
	
	@FindBy(xpath = ""+pancakeMenuu+"")
	WebElement pancakeMenu;
	
	@FindBy(xpath = ""+searchBarr+"")
	WebElement searchBar;
	
	@FindBy(xpath = ""+shoppingBagPageDomm+"")
	WebElement shoppingBagPageDom;
		
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+searchBoxConfirmm+"")
	WebElement searchBoxConfirm;	

	@FindBy(xpath = ""+searchXClosee+"")
	WebElement searchXClose;
	
	@FindBy(xpath = ""+searchXCrosss+"")
	WebElement searchXCross;	
		
	@FindBy(xpath = ""+searchSuggestionn+"")
	WebElement searchSuggestion;
	
	@FindBy(xpath = ""+searchSuggestItemm+"")
	WebElement searchSuggestItem;	
	
	@FindBy(xpath = ""+searchRecentHistoryy+"")
	WebElement searchRecentHistory;	
	
	@FindBy(xpath = ""+searchRecentHistoryClearAlll+"")
	WebElement searchRecentHistoryClearAll;	
	
	@FindBy(xpath = ""+searchNoResultss+"")
	WebElement searchNoResults;		
	
	
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+plpHeaderTitlee+"")
	WebElement plpHeaderTitle;
	
	@FindBy(how = How.XPATH,using = ""+recentSearchListt+"")
	List<WebElement> recentSearchList;
		
	
		
	ArrayList<String> searchKeywords =new ArrayList<String>();
	
	String keyword = null;
		
	/* HUDSONBAY-145 Verify that Promo banner should be displayed in the top of the page as per creative and fit to the screen*/
	public void PromoBanner()
		{
			ChildCreation("HUDSONBAY-145 Verify that Promo banner should be displayed in the top of the page as per creative and fit to the screen");
			try
			{
				//wait.until(ExpectedConditions.visibilityOf(promoBanner));
				if(HBCBasicfeature.isElementPresent(promoBanner))
				{
					Pass("Promo banner displayed in the top of the page");	
				}
				else
				{
					Fail("Promo banner not displayed in the top of the page");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}	
	
	/*HUDSONBAY-146 Verify that Promo banner contains carousel Images that should match with classic site/studio authored and able to scroll Horizontally */
	public void promoCarousel()
		{
			ChildCreation("HUDSONBAY-146 Verify that Promo banner contains carousel Images that should match with classic site/studio authored and able to scroll Horizontally");
			try
			{
				//wait.until(ExpectedConditions.visibilityOf(promocarousel));
				if(HBCBasicfeature.isElementPresent(promocarousel))
				{
					Pass("Promo banner carousel Images displayed in the top of the page");							
				}
				else
				{
					Fail("Promo banner carousel Images not displayed in the top of the page");
				}								
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-148 - Verify that promo banner at the header should be rotate with the periodical interval of time*/
	public void promochangetime()
	{
		ChildCreation("HUDSONBAY-148 - Verify that promo banner at the header should be rotate with the periodical interval of time.");
		try 
		{
		//	wait.until(ExpectedConditions.visibilityOf(promoBanner));
			if(HBCBasicfeature.isElementPresent(promoBanner))
			{
				List<WebElement> listlen = driver.findElements(By.xpath("//*[@id='hdPromoPagination']//*[contains(@class,'swiper-pagination-bullet')]"));
				if(listlen.size()>0)
				{
					for(int j = 0; j<listlen.size(); j++)
					{
						String promoName;
						long startTime ;
						startTime = System.currentTimeMillis();
						try
						{
							promoName=driver.findElement(By.xpath("//*[contains(@class,'swiper-slide sk_hdr_prmBnrInnerCell swiper-slide-active')]//*[@class='sk_promolink']//section")).getText();		
							System.out.println(promoName);
							long endTime = System.currentTimeMillis();
							long milliseconds = (endTime - startTime);
							long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(endTime - startTime);
							log.add("The banner promo " + promoName + " visible for " + timeSeconds + " Second / " + milliseconds +" milliseconds." );
							Thread.sleep(2000);
						}
						catch(Exception e)
						{
							promoName=driver.findElement(By.xpath("//*[contains(@class,'swiper-slide sk_hdr_prmBnrInnerCell swiper-slide-duplicate swiper-slide-active')]//*[@class='sk_promolink']//section")).getText();
							System.out.println(promoName);
							long endTime = System.currentTimeMillis();
							long milliseconds = (endTime - startTime);
							long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(endTime - startTime);
							log.add("The banner promo " + promoName + " visible for " + timeSeconds + " Second / " + milliseconds +" milliseconds." );
							Thread.sleep(3000);
						}
						
					}
					
					Pass("The banner changes promo rotates frequently",log);
				}
				else
				{
					Fail("The promo content list seems to be empty. Please Check.");
				}
			}
			else
			{
				Fail("The Promo message banner is not present at the top of the page.",log);
			}
		}
		catch (Exception e) 
		{
			Exception("There is something wrong. Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	/*HUDSONBAY-154 Verify that header contains hamburger, logo,search,shopping bag icons*/
	public void headerSection()
		{
			ChildCreation("HUDSONBAY-154 Verify that header contains hamburger, logo,search,shopping bag icons");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));			
				if(HBCBasicfeature.isElementPresent(header))
				{
					if(HBCBasicfeature.isElementPresent(hamburger))
					{
						Pass("Hamburger menu displayed in the header");
					}
					else
					{
						Fail("Hamburger menu not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(logo))
					{
						Pass("Logo displayed in the header");
					}
					else
					{
						Fail("Logo not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(searchIcon))
					{
						Pass("Search Icon displayed in the header");
					}
					else
					{
						Fail("Search Icon not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(bagIcon))
					{
						Pass("Shopping bag Icon displayed in the header");
					}
					else
					{
						Fail("Shopping bag Icon not displayed in the header");
					}
				}
				else
				{
					Fail("Header not displayed in the page");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-155 Verify that on tapping hamburger icon , pancake Menu page should be displayed*/
	public void hamburgerClick()
		{
			ChildCreation("HUDSONBAY-155 Verify that on tapping hamburger icon , pancake Menu page should be displayed");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(hamburger));			
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					action.moveToElement(hamburger).click().build().perform();
					Thread.sleep(1000);	
					wait.until(ExpectedConditions.visibilityOf(pancakeMenu));			
					if(HBCBasicfeature.isElementPresent(pancakeMenu))
					{
						Pass("Tapping hamburger Icon pancake menu displayed");
						driver.navigate().refresh();
						wait.until(ExpectedConditions.visibilityOf(hamburger));			
					}
					else
					{
						Fail("Tapping hamburger Icon pancake menu not displayed");
					}						
				}
				else
				{
					Fail("Hamburger icon not displayed in the header");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
		
	/*HUDSONBAY-157 Verify that on tapping search icon,search field should be enable with blinking cursor*/
	public void searchClick()
		{
			ChildCreation("HUDSONBAY-157 Verify that on tapping search icon,search field should be enable with blinking cursor");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBar));			
					if(HBCBasicfeature.isElementPresent(searchBar))
					{
						Pass("Tapping search Icon search field displayed");
					}
					else
					{
						Fail("Tapping search Icon search field not displayed");
					}						
				}
				else
				{
					Fail("searchIcon icon not displayed in the header");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}					
		}
	
	/*HUDSONBAY-158 Verify that on tapping shopping bag icon,Minicart should be displayed*/
	public void shoppingBagClick()
		{
			ChildCreation("HUDSONBAY-158 Verify that on tapping shopping bag icon,Minicart should be displayed");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(bagIcon));			
				if(HBCBasicfeature.isElementPresent(bagIcon))
				{
					action.moveToElement(bagIcon).click().build().perform();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(shoppingBagPageDom, "style", "visible"));			
					if(HBCBasicfeature.isElementPresent(shoppingBagPageDom))
					{
						Pass("Tapping Shopping Bag icon Minicart displayed");
					}
					else
					{
						Fail("Tapping Shopping Bag icon Minicart not displayed");
					}						
				}
				else
				{
					Fail("Shopping Bag icon not displayed in the header");
				}						
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-165 Verify that Search icon & search text box Should be displayed as per the creative and fit to the screen*/
	public void searchCreative()
		{
			ChildCreation("HUDSONBAY-165 Verify that Search icon & search text box Should be displayed as per the creative and fit to the screen");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchBox));			
				if(HBCBasicfeature.isElementPresent(searchBox))
				{
					Pass("Search icon & search text box displayed as per the creative and fit to the screen");
				}
				else
				{
					Fail("Search icon & search text box not displayed as per the creative");
				}						
			}			
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}	
	
	/*HUDSONBAY-183 Verify that default inline text("Type to search") should be shown below the search field.*/
	public void searchInlineText() throws Exception
		{
			String text = HBCBasicfeature.getExcelVal("HB183", sheet, 1);
			boolean flag = false;
			ChildCreation("HUDSONBAY-183 Verify that default inline text(Type to search') should be shown below the search field.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchBox));			
				if(HBCBasicfeature.isElementPresent(searchBox))
				{
					String inline = searchBox.getAttribute("placeholder");
					log.add("Default Inline text is: "+inline);
					if(inline.equalsIgnoreCase(text))
					{
						Pass("Default inline text(Type to search') shown below the search field.",log);
						flag = true;
					}
					else
					{
						Fail("Default inline text(Type to search') not shown below the search field.",log);
					}
				}
				else
				{
					Fail("SearchBox not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-184 Verify that default inline text('Please Enter to search') should be shown below the search field on after enterting the keyword.");
			if(flag==true)
			{
				Pass("Default inline text('Please Enter to search')shown below the search field on after enterting the keyword",log);
			}
			else
			{
				Fail("Default inline text('Please Enter to search')not shown below the search field on after enterting the keyword",log);
			}
		}
	
	/*HUDSONBAY-244 Verify that product count should be shown in the "Shopping Bag" icon in the header*/
	public void shoppingBagCount()
		{
			ChildCreation("HUDSONBAY-244 Verify that product count should be shown in the 'Shopping Bag' icon in the header");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(bagIcon));			
				if(HBCBasicfeature.isElementPresent(bagIcon))
				{
					wait.until(ExpectedConditions.visibilityOf(bagIconCount));			
					if(HBCBasicfeature.isElementPresent(bagIconCount))
					{
						int count = Integer.parseInt(bagIconCount.getText());
						log.add("The Product count shown in bag is: "+count);
						if(count>=0)
						{
							Pass("Product count shown in the 'Shopping Bag' icon in the header",log);
						}
						else
						{
							Fail("Product count not shown in the 'Shopping Bag' icon in the header",log);
						}
					}
					else
					{
						Fail("Shopping bag Icon count not displayed");
					}
				}
				else
				{
					Fail("Bag Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-166 Verify that while selecting the search icon at the header,the search field/text box should be enabled*/
	public void searchFields()
		{
			ChildCreation("HUDSONBAY-166 Verify that while selecting the search icon at the header,the search field/text box should be enabled");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchBox));			
				if(HBCBasicfeature.isElementPresent(searchBox))
				{
					Pass("Selecting the search icon at the header,the search field/text box is enabled");
				}
				else
				{
					Fail("Selecting the search icon at the header,the search field/text box is not enabled");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}	
	
	/*HUDSONBAY-167 Verify that while selecting the search icon at the header when its already open,the search field/text box should be disabled*/
	public void searchOpenClose()
		{
			ChildCreation("HUDSONBAY-167 Verify that while selecting the search icon at the header when its already open,the search field/text box should be disabled");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchBox));			
				if(HBCBasicfeature.isElementPresent(searchBoxConfirm))
				{
					action.moveToElement(searchXClose).click().build().perform();
					if(!HBCBasicfeature.isElementPresent(searchBoxConfirm))
					{
						Pass("Selecting the search icon at the header when its already open,the search field/text box is disabled");
					}
					else
					{
						Fail("Selecting the search icon at the header when its already open,the search field/text box is not disabled");
					}						
				}
				else
				{
					Fail("search box is not enabled");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}	
	
	/*HUDSONBAY-171 Verify that user able to enter keywords in the search field/text box.*/
	public void searchKeyword() throws Exception
		{
			String key = HBCBasicfeature.getExcelVal("HB171", sheet, 1);
			String[]split = key.split("\n");
			Random r = new Random();
			int size = split.length;
			ChildCreation("HUDSONBAY-171 Verify that user able to enter keywords in the search field/textbox.");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						int i = r.nextInt(size);
						keyword = split[i];
					//	keyword = "handbag";
						Thread.sleep(1000);
						action.moveToElement(searchBox).sendKeys(keyword).build().perform();
						log.add("The actual keyword is "+keyword);
						String expected = searchBox.getAttribute("value");	
						log.add("The expected keyword is "+expected);
						if(expected.equals(keyword))
						{
							Pass("User able to enter keywords in the search field/textbox",log);
						}
						else
						{
							Fail("User not able to enter keywords in the search field/textbox",log);
						}
					}
					else
					{
						Fail("Search box is not displayed");
					}
				}
				else
				{
					Fail("SearchIcon is not displayed in the header");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-172 Verify that while entering the keywords in the search field,the "X" cancel option should be enabled*/
	public void searchXEnabled()
		{
			ChildCreation("HUDSONBAY-172 Verify that while entering the keywords in the search field,the X cancel option should be enabled");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchXClose));			
				if(HBCBasicfeature.isElementPresent(searchXClose))
				{
					Pass("Entering the keywords in the search field, the 'X' cancel option enabled");
					Thread.sleep(1000);
				}
				else
				{
					Fail("Entering the keywords in the search field, the 'X' cancel option not enabled");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-173 Verify that while enter into search field,the 'X' cancel option should be shown as default*/
	public void searchXclear()
		{
			ChildCreation("HUDSONBAY-173 Verify that while enter into search field,the 'X' cancel option should be shown as default");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchXClose));			
				if(HBCBasicfeature.isElementPresent(searchXClose))
				{
					Pass("Entering the keywords in the search field, the 'X' cancel option shown as default");
				}
				else
				{
					Fail("Entering the keywords in the search field, the 'X' cancel option shown as default");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-174 Verify that while selecting the CANCEL button in the search text box, the search box should be closed.*/
	public void searchCancel()
		{
			ChildCreation("HUDSONBAY-174 Verify that while selecting the CANCEL button in the search text box, the search box should be closed.");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchXClose));			
				if(HBCBasicfeature.isElementPresent(searchXClose))
				{
					action.moveToElement(searchXClose).click().build().perform();
					if(!HBCBasicfeature.isElementPresent(searchBoxConfirm))
					{
						Pass("Selecting the CANCEL button in the search text box, the search box is closed");
						Thread.sleep(1000);
					}
					else
					{
						Fail("Selecting the CANCEL button in the search text box, the search box is not closed");
					}	
				}	
				else
				{
					Fail("Search cancel button is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
		
	/*HUDSONBAY-175/176 Verify that while entering the valid search keyword in the search field,corresponding search suggestion should be displayed based on the stream response*/
	public void searchSuggestion() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-176 Verify that while entering the valid search keyword in the search field, search suggestion should be displayed based on the stream response");
			String keyword = HBCBasicfeature.getExcelVal("HB175", sheet, 1);
			/*HttpURLConnection conn = null;*/
			ArrayList<String> siteResp = new ArrayList<>();
			ArrayList<String> streamResp = new ArrayList<>();
			ArrayList<Integer> result = new ArrayList<Integer>();
			boolean flag = false;
			try
			{
				//Getting Stream response
				
				/*String SearchStream = "http://hbcstage.skavaone.com/skavastream/core/v5/thebay/searchsuggestion?campaignId=1&search="+key+"&name=MC_10601_CatalogEntry_en_CA&url=http%3A%2F%2Fwcssearch.hbc.com%3A3737%2Fsolr%2FMC_10601_CatalogEntry_en_CA&storeid=10701&include=true&productid=&type=ajax&domain=www.thebay.com";
				URL curl = new URL(SearchStream);
				conn  = (HttpURLConnection) curl.openConnection();
				if(conn.getResponseCode()==200)
				{
					String classicResponse = IOUtils.toString(new URL(SearchStream));*/
					String SearchStream = HBConstants.suggestion.replace("key", keyword);
					driver.navigate().to(SearchStream);
					String classicResponse = driver.findElement(By.xpath("/html/body/pre")).getText();
					Thread.sleep(1000);
					driver.navigate().back();				
					JSONObject jobj = new JSONObject(classicResponse);
					JSONObject classicResultsObj = jobj.getJSONObject("properties");
					JSONArray classicResultArray = classicResultsObj.getJSONArray("suggestion");
					JSONObject resultObj = classicResultArray.getJSONObject(0);
					JSONArray resultArray = resultObj.getJSONArray("value");
				//	System.out.println(resultArray.length());
					for(int j=0; j<resultArray.length();j++)	
					{
						String prtname = resultArray.getString(j);	
					//	System.out.println(prtname);
						streamResp.add(prtname);
					}									
				/*}
				else
				{
					Fail("Response not getting from stream");
				}*/
				
				//Getting Site response
				
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.clear();
						log.add("Search keyword to be send is: "+keyword);
						searchBox.sendKeys(keyword);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(searchSuggestion));			
						if(HBCBasicfeature.isElementPresent(searchSuggestion))
						{
							wait.until(ExpectedConditions.visibilityOf(searchSuggestItem));			
							if(HBCBasicfeature.isElementPresent(searchSuggestItem))
							{
								List<WebElement> list = driver.findElements(By.xpath("//*[@class='searchSuggestions']//*[@class='suggestedItems']"));
								Thread.sleep(1000);
								int size = list.size();
								if(size>=1)
								{
									for(int i=0;i<size;i++)
									{
										String suggest = driver.findElement(By.xpath("(//*[@class='searchSuggestions']//*[@class='suggestedItems'])["+(i+1)+"]")).getText();
										siteResp.add(suggest);								
									}									
								}
								else
								{
									Fail("More than one Search Suggestion not shown");
								}					
							}
							else
							{
								Fail("Search suggestion items are not displayed");
							}
						}
						else
						{
							Fail("Search suggestion is not displayed");
						}								
					}
					else
					{
						Fail("Search box is not displayed");
					}						
				}
				else
				{
					Fail("Search icon is not displayed");
				}
				
				//Comparing Stream and Site responses

				
				for (String temp : streamResp)
				{
					result.add(siteResp.contains(temp) ? 1 : 0);
				//	System.out.println(result);
					log.add("Compared result is: "+result);
				}
				
				if(!result.contains(0))
			  	{
			  		log.add("The search suggestions for the keyword (Response from classicSite): " + streamResp.toString());
			  		log.add("The search suggestions for the keyword (Response from MobileSite): " + siteResp.toString());
			  		Pass("Entering the valid search keyword in the search field,corresponding search suggestion displayed based on the stream response",log);
			  		flag = true;
			  	}
			  	else
			  	{
			  		log.add("The search suggestions for the keyword (Response from classicSite): " + streamResp.toString());
			  		log.add("The search suggestions for the keyword (Response from MobileSite): " + siteResp.toString());
			  		Fail("Entering the valid search keyword in the search field,corresponding search suggestion not displayed based on the stream response",log);
			  	}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-175 Verify that while entering the valid search keyword in the search field,corresponding search suggestion should be displayed");
			if(flag==true)
			{
				Pass("Entering the valid search keyword in the search field,corresponding search suggestion should be displayed");
			}
			else
			{
				Fail("Entering the valid search keyword in the search field,corresponding search suggestion not displayed");
			}			
		}

	/*HUDSONBAY-180 Verify that while clearing the search keyword using the backspace button in the virtual keypad,the blank drop-down should not get displayed*/
	public void searchClear()
		{
			ChildCreation("HUDSONBAY-180 Verify that while clearing the search keyword using the backspace button in the virtual keypad,the blank drop-down should not get displayed");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchBox));			
				if(HBCBasicfeature.isElementPresent(searchBox))
				{
					log.add("The search keyword "+searchBox.getAttribute("value"));
					String var = searchBox.getAttribute("value");
					int len = var.length();
					do
					{			
						searchBox.sendKeys(Keys.BACK_SPACE);
						len--;					
					}
					while(len>=0);
					if(searchBox.getAttribute("value").isEmpty())
					 {
						log.add("The entered search keyword is cleared");
						if(!HBCBasicfeature.isElementPresent(searchSuggestion))
						{
							Pass("While clearing the search keyword ,the blank drop-down not get displayed",log);
						}
						else
						{
							Fail("While clearing the search keyword ,the blank drop-down get displayed",log);
						}
					 }
					else
					{
						Fail("Search box is not empty");
					}
				}
				else
				{
				Fail("search box is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-188 Verify that 'X' cancel icon should be shown as default, when no keyword is present inside the text field.*/
	public void searchXIcon()
		{
			ChildCreation("HUDSONBAY-188 Verify that 'X' cancel icon should be shown as default, when no keyword is present inside the text field");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchBox));			
				if(HBCBasicfeature.isElementPresent(searchBox))
				{				
					if(searchBox.getAttribute("value").isEmpty())
					{
						log.add("Search box field is empty");
						wait.until(ExpectedConditions.visibilityOf(searchXClose));			
						if(HBCBasicfeature.isElementPresent(searchXClose))
						{
							Pass("when no keyword is present inside the text field 'X' cancel icon displayed as default",log);
						}
						else
						{
							Fail("when no keyword is present inside the text field 'X' cancel icon is not displayed as default",log);
						}
					}
					else					
					{
						Fail("Search Box field is not empty");
					}					
				}
				else
				{
					Fail("Search Box is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-232 Verify that virtual keypad and search field/textbox should be closed on tapping the search icon in the header*/
	public void searchClose()
		{
			ChildCreation("HUDSONBAY-232 Verify that virtual keypad and search field/textbox should be closed on tapping the search icon in the header");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchBox));			
				if(HBCBasicfeature.isElementPresent(searchBoxConfirm))
				{	
					searchXClose.click();
					if(!HBCBasicfeature.isElementPresent(searchBoxConfirm))
					{
						Pass("Search field/textbox closed on tapping the search icon in the header");		
					}
					else
					{
						Fail("Search field/textbox not get closed on tapping the search icon in the header");	
					}
				}
				else
				{
					Fail("Search field/textbox not displayed");	
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-243 Verify that HBC logo should not be clickable on tapping from the home page. */
	public void logoClick()
		{
			ChildCreation("HUDSONBAY-243 Verify that HBC logo should not be clickable on tapping from the home page.");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(logo));			
				if(HBCBasicfeature.isElementPresent(logo))
				{	
					String currentUrl = driver.getCurrentUrl();
					log.add("The current homepage url is :"+currentUrl);
					action.moveToElement(logo).click().build().perform();				
					Thread.sleep(500);
					String expectedUrl = driver.getCurrentUrl();
					log.add("After clicked logo url is :"+expectedUrl);
					if(currentUrl.equals(expectedUrl))
					{
						Pass("HBC logo not clickable on tapping from the home page",log);
					}
					else
					{
						Fail("HBC logo clickable on tapping from the home page",log);
					}					
				}
				else
				{
					Fail("Logo not displayed on header");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-178 Verify that while selecting the search keyword in the search suggestions dropdown,the search results page should be displayed*/
	public void searchSuggestionSelection() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-178 Verify that while selecting the search keyword in the search suggestions dropdown,the search results page should be displayed");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.clear();
						searchBox.sendKeys(keyword);
						Thread.sleep(1500);
						wait.until(ExpectedConditions.visibilityOf(searchSuggestion));			
						if(HBCBasicfeature.isElementPresent(searchSuggestion))
						{
							Thread.sleep(1500);
							wait.until(ExpectedConditions.visibilityOf(searchSuggestItem));			
							if(HBCBasicfeature.isElementPresent(searchSuggestItem))
							{
								List<WebElement> list = driver.findElements(By.xpath("//*[@class='searchSuggestions']//*[@class='suggestedItems']"));
								Thread.sleep(1000);
								int size = list.size();
								Random r = new Random();
								int i = r.nextInt(size);
								//i=0;
								WebElement suggest = driver.findElement(By.xpath("(//*[@class='searchSuggestions']//*[@class='suggestedItems'])["+(i+1)+"]"));
								String selectedSuggestion = suggest.getText();
								searchKeywords.add(selectedSuggestion);
								log.add("The selected suggestion is: "+selectedSuggestion);
								suggest.click();
								wait.until(ExpectedConditions.visibilityOf(plpPage));
								if(HBCBasicfeature.isElementPresent(plpPage))
								{
									String plpTitle = plpHeaderTitle.getText();
									log.add("The search result page title is: "+plpTitle);
									if(selectedSuggestion.equalsIgnoreCase(plpTitle))
									{
										Pass("while selecting the search keyword in the search suggestions dropdown,the search results page displayed",log);
									}
									else
									{
										Fail("while selecting the search keyword in the search suggestions dropdown,the search results page not displayed",log);
									}
								}
								else
								{
									Fail("PLP page not displayed");
								}
							}
							else
							{
								Fail("Search suggestion items are not displayed");
							}
						}
						else
						{
							Fail("Search suggestion is not displayed");
						}								
					}
					else
					{
						Fail("Search box is not displayed");
					}						
				}
				else
				{
					Fail("Search icon is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-152 Verify the promo messages should be available only in home page only*/
	public void PLP_Promo_disabled()
		{
			ChildCreation("HUDSONBAY-152 Verify the promo messages should be available only in home page only");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					if(!HBCBasicfeature.isElementPresent(promoBanner))
					{
						Pass("Promo banner not displayed in the page");						
					}
					else
					{
						Fail("Promo banner displayed in the page");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-156 Verify that on tapping logo icon, it should navigate to home page*/
	public void logoClickTOHomePage()
		{
			ChildCreation("HUDSONBAY-156 Verify that on tapping logo icon, it should navigate to home page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(logo));
				if(HBCBasicfeature.isElementPresent(logo))
				{
					String Url = driver.getCurrentUrl();
					logo.click();
					wait.until(ExpectedConditions.visibilityOf(homepageDom));
					if(HBCBasicfeature.isElementPresent(homepageDom))
					{
						String currentUrl = driver.getCurrentUrl();
						if(Url!=currentUrl)
						{
							Pass("on tapping logo icon, navigated to home page");
						}
						else
						{
							Fail("on tapping logo icon,not navigated to home page");
						}
					}
					else
					{
						Fail("PromoBanner not displayed");
					}					
				}
				else
				{
					Fail("Logo not dispalyed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-182 Verify that while entering into the search box,previous visited search keywords should be displayed in search history.*/
	public void searchKeywordHistory() throws Exception
		{
			ChildCreation("HUDSONBAY-182 Verify that while entering into the search box,previous visited search keywords should be displayed in search history.");
			String key = HBCBasicfeature.getExcelVal("HB185", sheet, 1);
			String split[] = key.split("\n");
			ArrayList<String> searchedKeywords =new ArrayList<String>();
			try
			{
				Actions action = new Actions(driver);
				int size = split.length;
				for(int i=0;i<size;i++)
				{					
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.clear();						
						searchBox.sendKeys(split[i]);
						searchBox.sendKeys(Keys.ENTER);
						searchedKeywords.add(split[i]);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(plpPage));			
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							continue;
						}											
					}
				}
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchRecentHistory));			
					if(HBCBasicfeature.isElementPresent(searchRecentHistory))
					{
						int lsize = recentSearchList.size();
						boolean flag = false;
						for(int i=1;i<=lsize;i++)
						{
							String suggest = driver.findElement(By.xpath("(//*[@class='recentSuggestion']//*[@class='suggestedItems'])["+i+"]")).getText();
							log.add("Search History keyword is: "+suggest);
							if(searchedKeywords.contains(suggest))
							{
								log.add("Searched keyword present in the search history");
								flag = true;
								continue;
							}
							else
							{
								Fail("Entering into the search box,previous visited search keywords not displayed in search history.",log);
								searchXClose.click();
							}
						}
						if(flag==true)
						{
							Pass("Entering into the search box,previous visited search keywords not displayed in search history",log);
							searchXClose.click();
						}
					}
					else
					{
						Fail("Recent search history container is not displayed");
					}
				}
				else
				{
					Fail("search Icon is not displayed");
				}
				
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-185 Verify that while entering into the search box,last two previous visited search keywords should be displayed in search history.*/
	public void searchKeywordHistoryCount() throws Exception
		{
			ChildCreation("HUDSONBAY-185 Verify that while entering into the search box,last two previous visited search keywords should be displayed in search history.");
			String key = HBCBasicfeature.getExcelVal("HB185", sheet, 1);
			String split[] = key.split("\n");
			int size = split.length;
			ArrayList<String> searchHistory =new ArrayList<String>();
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchRecentHistory));			
					if(HBCBasicfeature.isElementPresent(searchRecentHistory))
					{
						int size_1 = recentSearchList.size();
						log.add("Previously visited keywords: "+size_1);
						boolean flag = false;
						for(int j=1;j<=size_1;j++)
						{
							String suggestion = driver.findElement(By.xpath("(//*[@class='recentSuggestion']//*[@class='suggestedItems'])["+j+"]")).getText();
							searchHistory.add(suggestion);
						}
						log.add("The last two previous visited search keywords are: "+searchHistory);
						for(int k=0;k<size;k++)
						{
							String searched = split[k];
							if(searchHistory.contains(searched))
							{								
								log.add("Previously visited keyword "+ searched +" displayed");
								flag = true;						
							}
							else
							{
								continue;
							}
						}
						if(flag==true)
						{
							Pass("last two previous visited search keywords displayed in search history",log);
							searchRecentHistoryClearAll.click();
							searchXClose.click();
						}
						else
						{
							Fail("last two previous visited search keywords displayed in search history",log);
							searchRecentHistoryClearAll.click();
							searchXClose.click();
						}
					}
					else
					{
						Fail("Recent search history container is not displayed");
					}
				}	
				else
				{
					Fail("Search Icon is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-189 Verify that minimum character for search suggestion should be as per the classic site*/
	public void searchMinimumChar() throws Exception
		{
			String Key = HBCBasicfeature.getExcelVal("HB189", sheet, 1);
			ChildCreation("HUDSONBAY-189 Verify that minimum character for search suggestion should be as per the classic site");
			try
			{
				String chr = "null";
				Actions action = new Actions(driver);
				String[] split  = Key.split("\n");
				int size = split.length;
				boolean flag = false;
				for(int i=0;i<size;i++)
				{							
					wait.until(ExpectedConditions.visibilityOf(searchIcon));			
					if(HBCBasicfeature.isElementPresent(searchIcon))
					{
						action.moveToElement(searchIcon).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(searchBox));			
						if(HBCBasicfeature.isElementPresent(searchBox))
						{											
							searchBox.clear();
							chr = split[i];
							searchBox.sendKeys(split[i]);
							Thread.sleep(2000);
								if(HBCBasicfeature.isElementPresent(searchSuggestion))
								{									
									log.add("Search suggestion displayed for the character: "+chr);
									searchBox.clear();
									searchXClose.click();
									flag =true;
									
								}														
								else
								{
									log.add("Search suggestion not displayed for the character: "+chr);
									searchBox.clear();
									searchXClose.click();
									continue;
								}									
							}
						else
						{
							Fail("Search Box not displayed");
						}
					}
					else
					{
						Fail("Search Icon not displayed");
					}
				}
				if(flag==true)
				{
					Pass("Minimum character for search suggestion as per the classic site",log);
				}
				else
				{
					Fail("Minimum character for search suggestion not as per the classic site",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-190 Verify that on entering the HTML tags in the input field, it should be treated as a string.*/
	public void searchHtmlTags() throws Exception
		{
			ChildCreation("HUDSONBAY-190 Verify that on entering the HTML tags in the input field, it should be treated as a string.");
			String Key = HBCBasicfeature.getExcelVal("HB190", sheet, 1);
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{							
						searchBox.clear();
						searchBox.sendKeys(Key);
						log.add("Searched HTML keyword is: "+Key);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(1000);
						try
						{							
							wait.until(ExpectedConditions.visibilityOf(plpPage));			
							if(HBCBasicfeature.isElementPresent(plpPage))
							{
								log.add("PLP Page displayed");
								Pass("on entering the HTML tags in the input field, it treated as a string",log);
							}
						}
						catch(Exception e)
						{
							log.add("PLP page not displayed");
							Fail("On Entering the HTML tags in the input field, it  not treated as a string",log);
							searchBox.clear();
							searchXClose.click();
						}
					}							
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-192 Verify that searched string should be maintained in search box when returning from search results page.*/
	public void searchStringMaintain()
		{
			ChildCreation("HUDSONBAY-192 Verify that searched string should not be maintained in search box when returning from search results page.");
			Actions action = new Actions(driver);
			try
			{
				String searchedKeyword  = keyword;
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{		
						String Keyword = searchBox.getText();
						if(Keyword.isEmpty())
						{
							Pass("Searched string not maintained in search box when returning from search results page");
							searchXClose.click();
						}
						else
						{
							if(searchedKeyword.equalsIgnoreCase(Keyword))
							{
								Fail("Searched string maintained in search box when returning from search results page");
								searchXClose.click();
							}
						}
					}
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-181 Verify that while selecting the enter button in virtual keyboard after entering valid keyword,the search results page should be displayed*/
	public void searchEnter() throws Exception
		{
			ChildCreation("HUDSONBAY-181 Verify that while selecting the enter button in virtual keyboard after entering valid keyword,the search results page should be displayed");
			String key = HBCBasicfeature.getExcelVal("HB175", sheet, 1);
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{											
						searchBox.clear();
						searchBox.sendKeys(key);
						searchBox.sendKeys(Keys.ENTER);
						wait.until(ExpectedConditions.visibilityOf(plpPage));			
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							Pass("Selecting the enter button in virtual keyboard after entering valid keyword,the search results page displayed");
							logo.click();
						}
						else
						{
							Fail("Selecting the enter button in virtual keyboard after entering valid keyword,the search results page displayed");
							logo.click();
						}
					}
					else
					{
						Fail("Search Box not displayed");
					}						
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-179 Verify that while entering the invalid search keyword in the search field,the "No suggested search for XXXX" alert message should be displayed*/
	public void searchNoResults() throws Exception
		{
			ChildCreation("HUDSONBAY-179 Verify that while entering the invalid search keyword in the search field,the 'No suggested search for XXXX' alert message should be displayed");
			String key = HBCBasicfeature.getExcelVal("HB179", sheet, 1);
			String expctd[] = key.split("\n");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{											
						searchBox.clear();
						searchBox.sendKeys(expctd[1]);
						Thread.sleep(200);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(500);
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(searchNoResults))
						{
							String actual = searchNoResults.getText();
							Thread.sleep(200);
							String defText =  actual.substring(0, 15);
							log.add("Displayed text is: "+defText);
							Thread.sleep(200);
							String disKey = actual.substring(16);
							log.add("Searched Keyword is: "+disKey);
							Thread.sleep(200);
							if(((expctd[0].trim()).equalsIgnoreCase(defText))&&((expctd[1].trim()).equalsIgnoreCase(disKey)))
							{
								Pass("Entering the invalid search keyword in the search field,the 'No suggested search for XXXX' alert message displayed",log);
							}
							else
							{
								Fail("Entering the invalid search keyword in the search field,the 'No suggested search for XXXX' alert message displayed",log);
							}
						}
						else
						{
							Fail("Search results displayed");
						}						
					}
					else
					{
						Fail("Search Box not displayed");
					}						
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	
	
	
}

