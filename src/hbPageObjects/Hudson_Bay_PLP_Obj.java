package hbPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbConfig.HBConstants;
//import hbConfig.HBConstants;
import hbPages.Hudson_Bay_PLP;

public class Hudson_Bay_PLP_Obj extends Hudson_Bay_PLP implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_PLP_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}	
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;	
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	//HUDSONBAY-255 To be changed
	@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id_1_0']")
	WebElement pancakemenu;
	
	@FindBy(xpath = "//*[@id='sk_mobCategoryItem_id_1_1'][2]")
	WebElement pancakemenuCategory;
	
	//To be changed
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+searchSuggestionn+"")
	WebElement searchSuggestion;
	
	@FindBy(xpath = ""+searchSuggestDomm+"")
	WebElement searchSuggestDom;
	
	@FindBy(xpath = ""+searchSuggestItemm+"")
	WebElement searchSuggestItem;	
	
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+plpSubHeaderr+"")
	WebElement plpSubHeader;
	
	@FindBy(xpath = ""+plpHeaderTitlee+"")
	WebElement plpHeaderTitle;
		
	@FindBy(xpath = ""+plpItemContt+"")
	WebElement plpItemCont;
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;
	
	@FindBy(xpath = ""+back_To_topp+"")
	WebElement back_To_top;
	
	@FindBy(xpath = ""+back_To_top_disabledd+"")
	WebElement back_To_top_disabled;
		
	@FindBy(xpath = ""+sortt+"")
	WebElement sort;
	
	@FindBy(xpath = ""+Refinewindoww+"")
	WebElement Refinewindow;	
	
	@FindBy(xpath = ""+RefinewindowApplyy+"")
	WebElement RefinewindowApply;		
	
	@FindBy(xpath = ""+RefinewindowClosee+"")
	WebElement RefinewindowClose;	
	
	@FindBy(xpath = ""+Filterr+"")
	WebElement Filter;
	
	@FindBy(xpath = ""+Swatchess+"")
	WebElement Swatches;
		
	@FindBy(xpath = ""+MoreSwatchess+"")
	WebElement MoreSwatches;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+PDPProdNamee+"")
	WebElement PDPProdName;	
	
	@FindBy(xpath = ""+FilterCountt+"")
	WebElement FilterCount;
	
	@FindBy(xpath = ""+plpGridd+"")
	WebElement plpGrid;
	
	@FindBy(xpath = ""+plpProductss+"")
	WebElement plpProducts;
		
	//Footer:
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+footerTopp+"")
	WebElement footerTop;	
	
	@FindBy(xpath = ""+footerTopTitlee+"")
	WebElement footerTopTitle;
	
	@FindBy(xpath = ""+footerSubTopp+"")
	WebElement footerSubTop;
	
	@FindBy(xpath = ""+footerSubTopCalll+"")
	WebElement footerSubTopCall;
	
	@FindBy(xpath = ""+footerSubTopEmaill+"")
	WebElement footerSubTopEmail;
	
	@FindBy(xpath = ""+footerMenuListt+"")
	WebElement footerMenuList;
	
	@FindBy(xpath = ""+footerBottomm+"")
	WebElement footerBottom;
	
	@FindBy(xpath = ""+footerSocialIconn+"")
	WebElement footerSocialIcon;
	
	@FindBy(xpath = ""+footerBottomCopyrightt+"")
	WebElement footerBottomCopyright;
	
	@FindBy(how = How.XPATH,using = ""+searchSuggestItemm+"")
	List<WebElement> searchSuggestItemList;

	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	@FindBy(how = How.XPATH,using = ""+pancakeMenulistt+"")
	List<WebElement> pancakeMenulist;	
	
	int plp_Pagesize = 0;
			
	/* HUDSONBAY-255 Verify that while selecting the sub-categories in the pancake menu,the selected category product list page should be displayed*/
	public void subcatToPlp() throws Exception
		{
			String key = HBCBasicfeature.getExcelVal("searchkey", sheet, 1);
			ChildCreation("HUDSONBAY-255 Verify that while selecting the sub-categories in the pancake menu,the selected category product list page should be displayed");
			try
			{	
					try
					{
						Thread.sleep(1000);
						wait.until(ExpectedConditions.elementToBeClickable(hamburger));	
						if(HBCBasicfeature.isElementPresent(hamburger))
						{
							hamburger.click();
							wait.until(ExpectedConditions.visibilityOf(pancakemenu));
							if(HBCBasicfeature.isElementPresent(pancakemenu))
							{
								pancakemenu.click();
								wait.until(ExpectedConditions.visibilityOf(pancakemenuCategory));
								if(HBCBasicfeature.isElementPresent(pancakemenuCategory))
								{
									pancakemenuCategory.click();
									boolean pgLoad1 = false;
									do
									{				
										try
										{
											Thread.sleep(1000);
											if(HBCBasicfeature.isElementPresent(plpPage))		
											{
												pgLoad1=true;	
												break;		
											}
										}
										catch (Exception e1)
										{
											pgLoad1=false;
											driver.navigate().refresh();	
											continue;
										}
									}
									while(!pgLoad1==true);
									if(HBCBasicfeature.isElementPresent(plpPage))
									{
										Pass("PLP Page Displayed");
									}
									else
									{
										Fail("Selecting the sub-categories in the pancake menu,the selected category PLP page not displayed");
									}							
								}
								else
								{
									Fail("Pancake Menu category not displayed");
								}
							}
							else
							{
								Fail("Pancake menu not displayed");
							}				
						}
						else
						{
							Fail("Hamburger not displayed");
						}			
					}
					catch (Exception e)
					{
						System.out.println(e.getMessage());
						Actions action = new Actions(driver);
						wait.until(ExpectedConditions.visibilityOf(searchIcon));			
						if(HBCBasicfeature.isElementPresent(searchIcon))
						{
							action.moveToElement(searchIcon).click().build().perform();
							wait.until(ExpectedConditions.visibilityOf(searchBox));			
							if(HBCBasicfeature.isElementPresent(searchBox))
							{											
								searchBox.clear();
								searchBox.sendKeys(key);
								searchBox.sendKeys(Keys.ENTER);
								wait.until(ExpectedConditions.visibilityOf(plpPage));			
								if(HBCBasicfeature.isElementPresent(plpPage))
								{
									Pass("Selecting the enter button in virtual keyboard after entering valid keyword,the search results page displayed");
								}
								else
								{
									Fail("Selecting the enter button in virtual keyboard after entering valid keyword,the search results page displayed");
								}
							}
							else
							{
								Fail("Search Box not displayed");
							}						
						}
						else
						{
							Fail("Search Icon not displayed");
						}				
					}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-256 Verify that total items available count (XXX items) should be displayed below the category title in product list page*/
	public void plpItemCount()
		{
			ChildCreation("HUDSONBAY-256 Verify that total items available count (XXX items) displayed below the category title in product list page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpSubHeader));
				if(HBCBasicfeature.isElementPresent(plpItemCont))
				{
					String val = plpItemCont.getText();
					String[] split = val.split(" ");
					String count = split[0]; 
					String text = split[1];
					Thread.sleep(500);
					if(count.matches(".*\\d+.*"))
					{		
						Pass("Product count displayed is :"+count);							
					}
					else
					{
						Fail("Product count not displayed",log);
					}					
					if(text.equalsIgnoreCase("Items"))
					{
						Pass("Total items available count (XXX items) displayed below the category title in product list page"+plpItemCont.getText(),log);
					}
					else
					{
						Fail("Total items available count (XXX items) not displayed below the category title in product list page"+plpItemCont.getText(),log);
					}
				}
				else
				{
					Fail("PLP Item count not displayed");		
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}	

	/*HUDSONBAY-257 Verify that user should be able to scroll the product list page vertically*/
	public void plpVerticalScroll()
		{
			ChildCreation("HUDSONBAY-257 Verify that user should be able to scroll the product list page vertically");
			boolean flag = false;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					HBCBasicfeature.scrolldown(footerContainer, driver);
					Pass("User able to scroll the product list page vertically");
					flag=true;
				}
				else
				{
					Fail("PLP Page not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}

			ChildCreation("HUDSONBAY-856 Verify that user should be able to scroll the PLP/search results page smoothly");		
			if(flag==true)
			{
				Pass("User able to scroll the PLP/search results page smoothly");
			}
			else
			{
				Fail("User not able to scroll the PLP/search results page smoothly");
			}
			
		}

	/*HUDSONBAY-275 Verify that while selecting the "Back to Top" icon then page should be scroll to top of the page*/
	public void plp_back_To_top()
		{
			ChildCreation("HUDSONBAY-275 Verify that while selecting the 'Back to Top' icon then page should be scroll to top of the page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(back_To_top));
				if(HBCBasicfeature.isElementPresent(back_To_top))
				{
					back_To_top.click();					
					Thread.sleep(1000);					
					if(!HBCBasicfeature.isElementPresent(back_To_top))
					{
						Pass("While selecting the 'Back to Top' icon then page scrolled to top of the page");
					}
					else
					{
						Fail("While selecting the 'Back to Top' icon then page not scrolled to top of the page");
					}					
				}
				else
				{
					Fail("Back to top not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-272 Verify that "Back to Top" button/icon should be shown while scrolling the products in the product list page*/
	public void plp_back_To_topScroll()
		{
			ChildCreation("HUDSONBAY-272 Verify that 'Back to Top' button/icon should be shown while scrolling the products in the product list page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					int size = plpItemContList.size();
					if(size>6)
					{
						Thread.sleep(500);
						HBCBasicfeature.scrolldown(footerContainer, driver);
						wait.until(ExpectedConditions.visibilityOf(back_To_top));
						if(HBCBasicfeature.isElementPresent(back_To_top))
						{
							Pass("Back to top button present");
						}
						else
						{
							Fail("Back to top button present");
						}
					}
					else
					{
						Pass("Not more than 6 products present, so 'back to top' button not displayed");
						HBCBasicfeature.scrolldown(footerContainer, driver);
					}				
				}
				else
				{
					Fail("PLP page not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-273 Verify that "Back to Top" button/icon should be displayed in the right side of the product list page*/
	public void plp_back_To_topRightSide()
		{
			ChildCreation("HUDSONBAY-273 Verify that 'Back to Top' button/icon should be displayed in the right side of the product list page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(back_To_top));
				if(HBCBasicfeature.isElementPresent(back_To_top))
				{				
					String val =  back_To_top.getCssValue("right");
					if(val.contains("px"))
					{
						Pass("Back to Top' button/icon displayed in the right side of the product list page");
					}
					else
					{
						Fail("Back to Top' button/icon not displayed in the right side");
					}						
				}
				else
				{
					Fail("Back to Top' button/icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}		
		}
	
	/*HUDSONBAY-271 Verify that "Back to Top" icon should be displayed as per the creative*/
	public void plp_back_To_topCreative()
		{
			ChildCreation("HUDSONBAY-271 Verify that 'Back to Top' icon should be displayed as per the creative");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(back_To_top));
				if(HBCBasicfeature.isElementPresent(back_To_top))
				{				
					Pass("'Back to Top' icon displayed as per the creative");
				}
				else
				{
					Fail("'Back to Top' icon not displayed as per the creative");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-274 Verify that "Back to Top" icon should not be displayed in the top of the page*/
	public void plp_back_To_topDisabled()
		{
			ChildCreation("HUDSONBAY-274 Verify that Back to Top icon should not be displayed in the top of the page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(back_To_top));
				if(HBCBasicfeature.isElementPresent(back_To_top))
				{
					back_To_top.click();	
					Thread.sleep(1000);
					if(!HBCBasicfeature.isElementPresent(back_To_top))
					{
						Pass("Back to Top icon not displayed in the top of the page");
					}
					else
					{
						Fail("Back to Top icon displayed in the top of the page");
					}					
				}
				else
				{
					Fail("Back to top not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-259 Verify that "sort" and "filter" options should be displayed below the header */
	public void plp_Sort_Filter()
		{
			ChildCreation("HUDSONBAY-259 Verify that 'sort' and 'filter' options should be displayed below the header ");
				try
				{						
					wait.until(ExpectedConditions.visibilityOf(sort));
					if(HBCBasicfeature.isElementPresent(sort))
					{
						Pass("Sort option displayed below the header");
					}
					else
					{
						Fail("Sort option not displayed below the header");
					}
					wait.until(ExpectedConditions.visibilityOf(Filter));
					if(HBCBasicfeature.isElementPresent(Filter))
					{
						Pass("Filter option displayed below the header");
					}
					else
					{
						Fail("Filter option not displayed below the header");
					}				
				}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
		
	/*HUDSONBAY-260 Verify that separation line should be displayed between "sort" and "filter" options*/
	public void plp_sort_separationLine()
		{
			ChildCreation("HUDSONBAY-260 Verify that separation line should be displayed between 'sort' and 'filter' options");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					
					WebElement separation = driver.findElement(By.cssSelector(".sk_sort"));
					String separationLine = ((JavascriptExecutor)driver).executeScript("return window.getComputedStyle(arguments[0], '::after').getPropertyValue('border-right');",separation).toString().substring(12);
					//System.out.println(separationLine);
					String item = separationLine.replace("1px solid", "");
					Color colorhxcnvt = Color.fromString(item);
					String hexCode = colorhxcnvt.asHex();
					if(hexCode.contains("#cccccc"))
					{
						Pass("Separation line displayed between 'sort' and 'filter' options");
					}
					else
					{
						Fail("Separation line not displayed between 'sort' and 'filter' options");
					}					
				}
				else
				{
					Fail("Sort and Filter Container not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-268 Verify that color swatches should be displayed below the ratings star in the product list page*/
	public void plp_Swatches()
		{
			ChildCreation("HUDSONBAY-268 Verify that color swatches should be displayed below the ratings star in the product list page");
			try
			{
				Random r  = new Random();
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					int size = plpItemContList.size();
					int i = r.nextInt(size);
					if(i==0)
					{
						i=i+1;
					}
					boolean swatch  = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='sk_prdswatch']")).isDisplayed();
					if(swatch==true)
					{
						Pass("color swatches displayed below the ratings star in the product list page");
					}
					else
					{
						Fail("color swatches not displayed below the ratings star in the product list page");
					}
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-269 Verify that "+" icon should be shown near the color swatches when more than 4 color swatches is available for the product*/
	public void plp_SwatchMore() throws Exception
		{			
			String key = HBCBasicfeature.getExcelVal("HB269", sheet, 1);
			ChildCreation("HUDSONBAY-269 Verify that "+" icon should be shown near the color swatches when more than 4 color swatches is available for the product");
			boolean swag = false;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{	
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", searchIcon);		
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{						
						searchBox.clear();
						searchBox.sendKeys(key);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							int plp_size = plpItemContList.size();
							int j = 0;
							boolean flag = false;
							do
							{
								for(j=1;j<=plp_size;j++)
								{
									List<WebElement> swatch  = driver.findElements(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+j+"])//*[@class='sk_prdswatch']"));
									int prodSize = swatch.size();
									if(prodSize>=4)
									{			
										try
										{
											swag = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+j+"])//*[@class='sk_prdsmorewatch']")).isDisplayed();								
											flag = true;			
											break;
										}							
										catch (Exception e)
										{
											continue;
										}
									}
									else
									{
										continue;
									}								
								}
							}
							while(!flag==true);
							if(swag==true)
							{
								Pass(" '+' icon shown near the color swatches when more than 4 color swatches is available for the product");
							}
							else
							{
								Fail(" '+' icon not displayed near the color swatches");
							}
						}						
						else
						{
							Fail("PLP page not displayed");
						}
					}			
					else
					{
						Fail("SearchBox not displayed");
					}				
				}
				else
				{
					Fail("SearchIcon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-270 Verify that on tapping the '+' icon near the color swatches, it should navigates to its corresponding PDP page*/
	public void swatches_to_PDP()
		{
			ChildCreation("HUDSONBAY-270 Verify that on tapping the '+' icon near the color swatches, it should navigates to its corresponding PDP page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(Swatches));
				if(HBCBasicfeature.isElementPresent(MoreSwatches))
				{
					MoreSwatches.click();
					wait.until(ExpectedConditions.visibilityOf(PDPPage));
					if(HBCBasicfeature.isElementPresent(PDPPage))
					{
						Pass("On tapping the '+' icon near the color swatches, navigates to its corresponding PDP page");
						driver.navigate().back();
					}
					else
					{
						Fail("On tapping the '+' icon near the color swatches, not navigates to its corresponding PDP page");
						driver.navigate().back();
					}
				}
				else
				{
					Fail("'+' icon near the color swatches not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-261 Verify that selected "sort" and "filter" options count should be displayed as per the creative*/
	public void sort_Filter_Count()
		{
			ChildCreation("HUDSONBAY-261 Verify that selected 'sort' and 'filter' options count should be displayed as per the creative");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(FilterCount));
				if(HBCBasicfeature.isElementPresent(FilterCount))
				{
					int count = Integer.parseInt(FilterCount.getText());
					if(count>=0)
					{
						Pass("'sort' and 'filter' options count displayed as per the creative");
					}
					else
					{
						Fail("'sort' and 'filter' options count not displayed as per the creative");
					}
				}
				else
				{
					Fail("Filter count not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-264 Verify that product ratings should be displayed for each product, with black shaded as per the classic site*/
	public void plp_rating()
		{
			ChildCreation("HUDSONBAY-264 Verify that product ratings should be displayed for each product, with black shaded as per the classic site");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					int size = plpItemContList.size();
					int ctr = 0;
					boolean flag = false;
					for(int i=1;i<=size;i++)
					{
						try
						{
							WebElement ratingContainer = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='skMob_productDetails']//*[@class='skMob_rating_reviews ']"));
							if(HBCBasicfeature.isElementPresent(ratingContainer))
							{
								WebElement ProductRating = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='skMob_productDetails']//*[@id='skMob_fullRating']"));
								if(ProductRating.isDisplayed())
								{
									flag = true;
									ctr++;
								}
							}
							else
							{
								Fail("The Product Rating container is not displayed.");
								continue;
							}
						}
						catch (Exception e)
						{
							continue;
						}						
					}
					if(flag==true)
					{
						Pass("Product ratings displayed for products "+ctr);
					}
					else
					{
						Fail("Product ratings not displayed for any of the products");
					}
				}
				else
				{
					Fail("PLP page is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-265 Verify that when there is no rating ,empty rating star should not be shown as per the classic site */
	public void plp_No_rating()
		{
			ChildCreation("HUDSONBAY-265 Verify that when there is no rating ,empty rating star should not be shown as per the classic site ");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					int ctr = 0;
					int size = plpItemContList.size();
					System.out.println("Please wait for while...checking all the products when there is no rating ,empty rating star should not be shown...");		
					for(int i=1;i<=size;i++)
					{
						try
						{
							WebElement ratingContainer = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='skMob_productDetails']//*[@class='skMob_rating_reviews ']"));
							if(HBCBasicfeature.isElementPresent(ratingContainer))
							{
								continue;
							}
							else
							{
								Fail("The Product Rating container is not displayed.");
								continue;
							}
						}
						catch (Exception e)
						{
							try
							{
								String ProductRating = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"])//*[@class='skMob_productDetails']//*[@id='skMob_fullRating']")).getAttribute("title");
								Fail("Product Rating star is displayed"+ProductRating);
							}
							catch (Exception e1)
							{								
								ctr++;													
							}							
						}						
					}
					if(ctr>=1)
					{
						log.add("The rating star is not shown for " +ctr+ " products");
						Pass("Product has no rating ,empty rating star not shown as per the classic site",log);
					}
					else
					{
						Skip("Displayed products has Rating container");
					}
				}
				else
				{
					Fail("PLP page is not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-281 Verify that grid view should be enabled by default on the product list page*/
	public void plpGridView()
		{
			ChildCreation("HUDSONBAY-281 Verify that grid view should be enabled by default on the product list page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpGrid))
				{
					Pass("Grid view enabled by default on the product list page");
				}
				else
				{
					Fail("Grid view not enabled by default on the product list page");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-254 Verify that product list page should be as displayed per the creative and fit to the screen in both potrait and Landscape*/
	public void PLPCreative()
		{
			ChildCreation("HUDSONBAY-254 Verify that product list page should be as displayed per the creative and fit to the screen in both potrait and Landscape");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					Pass("PLP page is displayed");
					wait.until(ExpectedConditions.visibilityOf(plpSubHeader));
					if(HBCBasicfeature.isElementPresent(plpSubHeader))
					{
						Pass("PLP page Subheader is displayed");
						wait.until(ExpectedConditions.visibilityOf(plpHeaderTitle));
						if(HBCBasicfeature.isElementPresent(plpHeaderTitle))
						{
							Pass("PLP header is displayed"+plpHeaderTitle.getText());
						}
						else
						{
							Fail("PLP header is not displayed");
						}
						wait.until(ExpectedConditions.visibilityOf(plpItemCont));
						if(HBCBasicfeature.isElementPresent(plpItemCont))
						{
							Pass("PLP Item count is displayed"+plpItemCont.getText());
						}
						else
						{
							Fail("PLP Item count is not displayed");
						}
						wait.until(ExpectedConditions.visibilityOf(sort));
						if(HBCBasicfeature.isElementPresent(sort))
						{
							Pass("PLP Sort option is displayed");
						}
						else
						{
							Fail("PLP Sort option is not displayed");
						}
						wait.until(ExpectedConditions.visibilityOf(sort));
						if(HBCBasicfeature.isElementPresent(sort))
						{
							Pass("PLP Sort option is displayed");
						}
						else
						{
							Fail("PLP Sort option is not displayed");
						}
						wait.until(ExpectedConditions.visibilityOf(Filter));
						if(HBCBasicfeature.isElementPresent(Filter))
						{
							Pass("PLP Filter option is displayed");
						}
						else
						{
							Fail("PLP Filter option is not displayed");
						}
						wait.until(ExpectedConditions.visibilityOf(FilterCount));
						if(HBCBasicfeature.isElementPresent(FilterCount))
						{
							Pass("PLP Filter Count is displayed");
						}
						else
						{
							Fail("PLP Filter Count is not displayed");
						}					
					}
					else
					{
						Fail("PLP subheader is not displayed");
					}
					wait.until(ExpectedConditions.visibilityOf(plpProducts));
					if(HBCBasicfeature.isListElementPresent(plpItemContList))
					{
						int size = plpItemContList.size();
						int ctr = 0;
						for(int i=1;i<=size;i++)
						{
							ctr++;
						}
						Pass("Total products displayed in the PLP page is: "+ctr);
					}
					else
					{
						Fail("Products not displayed in the PLP page");
					}
					HBCBasicfeature.scrolldown(footerContainer, driver);
				/*	wait.until(ExpectedConditions.visibilityOf(signupEmails));
					if(HBCBasicfeature.isElementPresent(signupEmails))
					{
						Pass("Signup for daily Emails option is displayed");	
					}
					else
					{
						Fail("Signup for daily Emails option is not displayed");						
					}			*/
					wait.until(ExpectedConditions.visibilityOf(footerTop));
					if(HBCBasicfeature.isElementPresent(footerTop))
					{
						Pass("Footer top panel is displayed");	
						wait.until(ExpectedConditions.visibilityOf(footerTopTitle));
						if(HBCBasicfeature.isElementPresent(footerTopTitle))
						{
							Pass("Footer top panel title is displayed");	
						}
						else
						{
							Fail("Footer top panel title is not displayed");						
						}
						wait.until(ExpectedConditions.visibilityOf(footerSubTop));
						if(HBCBasicfeature.isElementPresent(footerSubTop))
						{
							Pass("Footer sub top panel is displayed");	
							wait.until(ExpectedConditions.visibilityOf(footerSubTopCall));
							if(HBCBasicfeature.isElementPresent(footerSubTopCall))
							{
								Pass("Footer call option is displayed");		
							}
							else
							{
								Fail("Footer Call option is not displayed");						
							}
							wait.until(ExpectedConditions.visibilityOf(footerSubTopEmail));
							if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
							{
								Pass("Footer Email option is displayed");	
							}
							else
							{
								Fail("Footer email option is not displayed");						
							}
						}
						else
						{
							Fail("Footer sub top panel is not displayed");						
						}
					}
					else
					{
						Fail("Footer top panel is not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerMenuList));
					if(HBCBasicfeature.isElementPresent(footerMenuList))
					{
						Pass("Footer Menu List option is displayed");
					}
					else
					{
						Fail("Footer menu list is not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerBottom));
					if(HBCBasicfeature.isElementPresent(footerBottom))
					{
						Pass("Footer bottom panel is displayed");
						wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
						if(HBCBasicfeature.isElementPresent(footerSocialIcon))
						{
							Pass("Footer social icon options are displayed");
						}
						else
						{
							Fail("Footer social icon options are not displayed");						
						}
						wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
						if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
						{
							log.add("Footer displayed as per the creative and fit to the screen");
							Pass("Footer copyright section is displayed",log);										
						}
						else
						{
							log.add("Footer is not displayed as per the creative and fit to the screen");
							Fail("Footer copyright section is not displayed",log);	
						}
					}
					else
					{
						Fail("Footer bottom panel is not displayed");						
					}					
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-258 Verify that while scrolling the products in the product list page,the products should be displayed with the lazy loading */
	public void plpLazyLoad()
		{
			ChildCreation("HUDSONBAY-258 Verify that while scrolling the products in the product list page,the products should be displayed with the lazy loading ");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpProducts));
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					int bfre_size = plpItemContList.size();
					log.add("Before scrolling size is "+bfre_size);
					if(bfre_size>=100)
					{		
							HBCBasicfeature.scrolldown(footerContainer, driver);
							boolean loaded = false;
							do
							{	
								int size = plpItemContList.size();
								if(size>100)
								{
									loaded=true;
								}
								else
								{
									loaded=false;
								}
								
							}while(!loaded==true);
							
						Thread.sleep(1000);
						int aftr_size =  plpItemContList.size();						
						log.add("After lazy loading size is "+aftr_size);
						if(aftr_size>bfre_size)
						{
							Pass("while scrolling the products in the product list page,the products displayed with the lazy loading",log);
						}
						else
						{
							Fail("while scrolling the products in the product list page,the products not displayed with the lazy loading",log);
						}						
					}
					else
					{
						Pass("Product displayed is less than 100, so no lazy loading",log);
					}				
				}
				else
				{
					Fail("Products are not displayed in the PLP page");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-263 Verify that listed product should contains product Image,title,color,availability, description,price and special offers details*/
	public void PLPDetails()
		{
			ChildCreation("HUDSONBAY-263 Verify that listed product should contains product Image,title,color,availability, description,price and special offers details");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				HBCBasicfeature.scrollup(plpHeaderTitle, driver);
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					System.out.println("Please wait for while...checking all the products for Image, title, color, etc..");				
					plp_Pagesize = plpItemContList.size();
					int size = plp_Pagesize/2;
					for(int i=1;i<=size;i++)
					{							
						WebElement image = 	driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"]//*[@class='skMob_productImgDiv']"));
						wait.until(ExpectedConditions.visibilityOf(image));
						if(HBCBasicfeature.isElementPresent(image))
						{		
							continue;						
						}
						else
						{
							Fail("Product Image not displayed for each product");	
						}
					}
					Pass("Product Image displayed for each product");
					
					for(int i=1;i<=size;i++)
					{							
						WebElement product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(product_BrandTitle));
						if(HBCBasicfeature.isElementPresent(product_BrandTitle))
						{
							continue;
						}
						else
						{
							Fail("Product Brand Title not displayed for each product");	
						}
					}
					Pass("Product Brand Title displayed for each product");
					
					for(int i=1;i<=size;i++)
					{							
						WebElement product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(product_Title));
						if(HBCBasicfeature.isElementPresent(product_Title))
						{
							continue;
						}
						else
						{
							Fail("Product Title not displayed for each product");	
						}
					}
					Pass("Product Title displayed for each product");				
					
					for(int i=1;i<=size;i++)
					{							
						WebElement product_Color = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_color_items'])["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(product_Color));
						if(HBCBasicfeature.isElementPresent(product_Color))
						{
							continue;
						}
						else
						{
							Fail("Product Color not displayed for each product");	
						}
					}
					Pass("Product Color displayed for each product");										
				}
				else
				{
					Fail("Plp Page not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-333 Verify that broken images should not be shown in the product list page */
	public void brokenImage()
		{
			ChildCreation("HUDSONBAY-333 Verify that broken images should not be shown in the product list page");		
			int ctr = 0;
			try
			{
				HBCBasicfeature.scrollup(logo, driver);
				wait.until(ExpectedConditions.visibilityOf(plpPage));
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					plp_Pagesize = plpItemContList.size();
					for(int i=1;i<=plp_Pagesize;i++)
					{
						Thread.sleep(200);
						WebElement image = 	driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv']//img)["+i+"]"));	
						Thread.sleep(1000);
						HBCBasicfeature.scrolldown(image, driver);						
						int resp = HBCBasicfeature.imageBroken(image, log);
						if(resp==200)
						{
							ctr++;
							continue;
						}
						else
						{
							log.add("Image broken for the product"+i);
							continue;
						}
					}
					if(plp_Pagesize==ctr)
					{
						Pass("Broken images not be shown in the product list page",log);
					}
					else
					{
						Fail("Broken images shown in the product list page",log);
					}
				}
				else
				{
					Fail("PLP page not dispalyed");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-278 Verify that if the product has multiple price, the regular and the sale price should be displayed*/
	public void procuctRegularSalePrice()
		{
			ChildCreation("HUDSONBAY-278 Verify that if the product has multiple price, the regular and the sale price should be displayed");
			try
			{
				int reg = 0 , sal = 0, regSal = 0;
				System.out.println("Please wait for while...checking all the products for multiple price, the regular and the sale price..");			
				for(int i=1;i<=plp_Pagesize;i++)
				{	
					try
					{	
						HBCBasicfeature.scrollup(logo, driver);
						WebElement regularPrice = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_pdtPriceCont'])["+i+"]//*[@pricetype='Reg']"));
						wait.until(ExpectedConditions.visibilityOf(regularPrice));
						if(HBCBasicfeature.isElementPresent(regularPrice))
						{							
							try
							{
								WebElement salePrice = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_pdtPriceCont'])["+i+"]//*[@pricetype='Sale']"));
								wait.until(ExpectedConditions.visibilityOf(salePrice));
								if(HBCBasicfeature.isElementPresent(salePrice))
								{
									regSal++;
									continue;
								}
							}
							catch(Exception e)
							{
								reg++;
								continue;
							}							
						}
						else
						{
							Fail("Regular Price not available for the products");	
						}
					}
					catch(Exception e)
					{
						WebElement salePrice = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_pdtPriceCont'])["+i+"]//*[@pricetype='Sale']"));
						wait.until(ExpectedConditions.visibilityOf(salePrice));
						if(HBCBasicfeature.isElementPresent(salePrice))
						{
							sal++;
							continue;
						}
						else
						{
							Fail("Sale Price not available for the products");
						}
					}
				}
				log.add("The total products in PLP page is: "+plp_Pagesize);
				log.add("Total Products has both Regular price and sale price: "+regSal);
				log.add("Total Products has only Regular price: "+reg);
				log.add("Total Products has only sale price: "+sal);
				if(regSal>=1)
				{
					Pass("Both Regular Price and Sale Price displayed for the products",log);	
				}
				else
				{
					Pass("Sale price alone displayed for the products",log);
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/* HUDSONBAY-279 Verify that if the product has any offers, the offer details are displayed (Eg: "HUDSON'S BAY EXCLUSIVE")*/
	public void productSplOffer()
		{
			ChildCreation(" HUDSONBAY-279 Verify that if the product has any offers, the offer details are displayed (Eg: 'HUDSON'S BAY EXCLUSIVE')");
			try
			{
				int ctr = 0;
				System.out.println("Please wait for while...checking all the products for any offers, the offer details are displayed...");			
				for(int i=1;i<=plp_Pagesize;i++)
				{	
					try
					{
						WebElement product_offer = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_buyInfo skMob_extendedSizes'])["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(product_offer));
						if(HBCBasicfeature.isElementPresent(product_offer))
						{							
							ctr++;
							String offer = product_offer.getText();
							log.add("The product has offer: "+offer);
							continue;
						}
						else
						{
							Fail("Product special offer not displayed for each product");	
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
				log.add("Total Products has special offers is: "+ctr);
				Pass("Product special offer displayed for each product",log);						
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-280 Verify that while selecting the product in the PLP page,it should navigate to the corresponding PDP page */
	public void plp_TO_PDP()
		{
			ChildCreation("HUDSONBAY-280 Verify that while selecting the product in the PLP page,it should navigate to the corresponding PDP page ");
			try
			{
				if(HBCBasicfeature.isListElementPresent(plpItemContList))
				{
					int size = plpItemContList.size();
					Random r  = new Random();
					int i = r.nextInt(size);
					WebElement product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
					wait.until(ExpectedConditions.visibilityOf(product));
					HBCBasicfeature.scrolldown(product, driver);
					if(HBCBasicfeature.isElementPresent(product))
					{
						String product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+(i+1)+"]")).getText();
						String product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+(i+1)+"]")).getText();
						String PLPtitle = product_BrandTitle +" "+ product_Title;
						log.add("The selected product title is: "+PLPtitle);
						Thread.sleep(1000);
						WebElement product_Clk = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])["+(i+1)+"]"));		
						product_Clk.click();
						wait.until(ExpectedConditions.visibilityOf(PDPPage));
						if(HBCBasicfeature.isElementPresent(PDPPage))
						{
							String PDPtitle = PDPProdName.getText();
							log.add("The PDP page title is: "+PDPtitle);
							if(PDPtitle.equalsIgnoreCase(PLPtitle))
							{
								Pass("Selecting the product in the PLP page,it navigated to the corresponding PDP page",log);
							}
							else
							{
								Fail("Selecting the product in the PLP page,not navigated to the corresponding PDP page",log);
							}
						}
						else
						{
							Fail("PDP Page not displayed");
						}
					}
					else
					{
						Fail("Selected product not displayed");
					}					
				}
				else
				{
					Fail("PLP products are not dispalyed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
		}
	
	/*HUDSONBAY-276 Verify that on selecting the particular category the product list page should be displayed with all the available products belongs to that category*/
	public void category_To_PLP()
		{
			ChildCreation("HUDSONBAY-276 Verify that on selecting the particular category the product list page should be displayed with all the available products belongs to that category");
			Random r = new Random();
			ArrayList<String> title = new ArrayList<String>();
			int i = 0, j = 0, k = 0, m = 0 ;
			String selectedMenuName ="";
			String selectedCategoryName ="";
			String selectedSubCategoryName = "";
			String selectedSub_subCategoryName = "";
			boolean  plpTitleFlag = false;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(hamburger));
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					hamburger.click();
											
					/*********************Menu selection*********************/
					
					boolean catFlag = false, subcatFlag = false, subsubCatFlag = false;
					int size = pancakeMenulist.size();
					log.add("The Pancake menu size is "+size);
					i = r.nextInt(size);
					if(i==0)
					{
						i=i+1;
					}					
					WebElement selectedmenu = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
					Thread.sleep(500);
					selectedMenuName = selectedmenu.getText();
					title.add(selectedMenuName);
					Thread.sleep(1000);
					log.add("The selected Menu is: "+selectedMenuName);	
					try
					{
						//WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(i+1)+"_0']//*[@id='sk_mobCategoryItem_id__1']"));
						WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]"));
						if(HBCBasicfeature.isElementPresent(category))
						{
							log.add("The Pancake menu " +selectedMenuName+" has category");
							catFlag = true;
							HBCBasicfeature.scrolldown(selectedmenu, driver);
							selectedmenu.click();	
						}
					}
					catch(Exception e)
					{
						log.add("The Pancake menu " +selectedMenuName+" has no category");
						selectedmenu.click();	
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							Pass("On selecting the particular category the product list page displayed with all the available products belongs to that category",log);
							plpTitleFlag = true;
						}	
						else
						{
							Fail("On selecting the particular category the product list page not displayed with all the available products belongs to that category",log);

						}
					}
					/*********************Category selection*********************/
					
					if(catFlag == true)		
					{					
						//List<WebElement> categorySize = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(i+1)+"_0']//*[@id='sk_mobCategoryItem_id__1']"));
						List<WebElement> categorySize = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]"));
						int CatSize = categorySize.size();				
						log.add("The Pancake menu Category size is "+CatSize);
						
						j = r.nextInt(CatSize);
						WebElement selectedsubCategory = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+(j+1)+"]"));
						Thread.sleep(500);
						selectedCategoryName = selectedsubCategory.getText();
						Thread.sleep(500);
						title.add(selectedCategoryName);	
						log.add("The selected Category is: "+selectedCategoryName);	
						try
						{	
							//WebElement subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(i+1)+"_0']//*[@id='sk_mobCategoryItem_id__1']["+(j+1)+"])//*[@id='sk_mobCategoryItem_id__2']"));
							WebElement subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+(j+1)+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]"));
							if(HBCBasicfeature.isElementPresent(subCategory))
							{
								HBCBasicfeature.scrolldown(selectedsubCategory, driver);
								selectedsubCategory.click();	
								subcatFlag = true;
								log.add("The selected category " +selectedCategoryName+" has sub category");
								Thread.sleep(1000);
							}
						}
						catch(Exception e)
						{
							log.add("The selected category " +selectedCategoryName+" has no sub category");
							selectedsubCategory.click();	
							wait.until(ExpectedConditions.visibilityOf(plpPage));
							if(HBCBasicfeature.isElementPresent(plpPage))
							{
								Pass("On selecting the particular category the product list page displayed with all the available products belongs to that category",log);
								plpTitleFlag = true;
							}	
							else
							{
								Fail("On selecting the particular category the product list page not displayed with all the available products belongs to that category",log);
							}
						}
					}
					else
					{
						Skip("PLP Page already displayed");
					}
					

					/*************************SubCategory Selection*************************/
				
					if(subcatFlag == true)
					{
						//List<WebElement> SubcategorySize =  driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(i+1)+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']"));
						List<WebElement> SubcategorySize =  driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+(j+1)+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]"));
						int subCatSize = SubcategorySize.size();
						log.add("The respective category's sub category size is "+subCatSize);	
						
						k = r.nextInt(subCatSize);
						WebElement selectedsub_SubCategory = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+(j+1)+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+(k+1)+"]"));
						Thread.sleep(500);
						selectedSubCategoryName = selectedsub_SubCategory.getText();
						Thread.sleep(500);
						title.add(selectedSubCategoryName);	
						log.add("The selected Sub Category is: "+selectedSubCategoryName);	
	
						try
						{
							WebElement sub_SubCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+(j+1)+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+(k+1)+"]//*[contains(@id,'sk_mobSublevel_2')]"));
							if(HBCBasicfeature.isElementPresent(sub_SubCategory))
							{
								subsubCatFlag = true;
								HBCBasicfeature.scrolldown(selectedsub_SubCategory, driver);
								selectedsub_SubCategory.click();	
								log.add("The selected sub category " +selectedSubCategoryName+" has sub sub category");
								Thread.sleep(1000);
							}									
						}
						catch(Exception e)
						{
							log.add("The selected sub category " +selectedSubCategoryName+" has no sub category");
							selectedsub_SubCategory.click();	
							wait.until(ExpectedConditions.visibilityOf(plpPage));
							if(HBCBasicfeature.isElementPresent(plpPage))
							{
								Pass("On selecting the particular category the product list page displayed with all the available products belongs to that category",log);
								plpTitleFlag = true;
							}	
							else
							{
								Fail("On selecting the particular category the product list page not displayed with all the available products belongs to that category",log);
	
							}
						}
					}
					else
					{
						Skip("PLP Page already displayed");
					}
						
					/*************************SubSubCategory Selection*************************/
					
					if(subsubCatFlag==true)
					{					
						//List<WebElement> SubSubcategorySize =  driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(i+1)+"_0']//*[@id='sk_mobCategoryItem_id__1']["+(j+1)+"])//*[@id='sk_mobCategoryItem_id__2']["+(k+1)+"]//*[@id='sk_mobSublevel__2']//*[@id='sk_mobCategoryItem_id__3']"));
						List<WebElement> SubSubcategorySize =  driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+(j+1)+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+(k+1)+"]//*[contains(@id,'sk_mobSublevel_2')]//*[contains(@id,'sk_mobCategoryItem_id_3')]"));
						int subSubCatSize = SubSubcategorySize.size();
						log.add("The respective sub category's sub-sub category size is "+subSubCatSize);	
						
						m = r.nextInt(subSubCatSize);	
						WebElement selectedsub_sub_SubCategory = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+(j+1)+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+(k+1)+"]//*[contains(@id,'sk_mobSublevel_2')]//*[contains(@id,'sk_mobCategoryItem_id_3')]["+(m+1)+"]"));
						Thread.sleep(500);
						selectedSub_subCategoryName = selectedsub_sub_SubCategory.getText();
						Thread.sleep(500);
						title.add(selectedSub_subCategoryName);	
						log.add("The selected SubSub Category is: "+selectedSub_subCategoryName);						
						try
						{
							//WebElement sub_Sub_subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']["+k+"]//*[@id='sk_mobSublevel__2']//*[@id='sk_mobCategoryItem_id__3']["+m+"]//*[@id='sk_mobSublevel__3']"));
							WebElement sub_Sub_subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+(j+1)+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+(k+1)+"]//*[contains(@id,'sk_mobSublevel_2')]//*[contains(@id,'sk_mobCategoryItem_id_3')]["+(m+1)+"]//*[contains(@id,'sk_mobSublevel_3')]"));
							if(HBCBasicfeature.isElementPresent(sub_Sub_subCategory))
							{
								HBCBasicfeature.scrolldown(selectedsub_sub_SubCategory, driver);
								selectedsub_sub_SubCategory.click();
								log.add("The selected sub sub  category " +selectedSub_subCategoryName+" has sub sub sub category");
								Thread.sleep(1000);
							}
						}							
						catch(Exception e)
						{
							log.add("The selected sub_sub category " +selectedSub_subCategoryName+" has no sub category");
							selectedsub_sub_SubCategory.click();	
							wait.until(ExpectedConditions.visibilityOf(plpPage));
							if(HBCBasicfeature.isElementPresent(plpPage))
							{
								Pass("On selecting the particular category the product list page displayed with all the available products belongs to that category",log);
								plpTitleFlag = true;
							}	
							else
							{
								Fail("On selecting the particular category the product list page not displayed with all the available products belongs to that category",log);
	
							}
						}
					}
					else
					{
						Skip("PLP already displayed");
					}
				}
				else
				{
					Fail("Hamburger not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-277 Verify that category title should matches with the selected category");
			if(plpTitleFlag==true)
			{
				boolean flag = false;
				if(HBCBasicfeature.isElementPresent(plpHeaderTitle))
				{
					String plpTitle = plpHeaderTitle.getText();
					log.add("The PLP title is: "+plpTitle);
					for(int p=0;p<title.size();p++)
					{
						String catTitle = title.get(p);
						if(catTitle.equalsIgnoreCase(plpTitle))
						{
							flag = true;
						}			
					}
					if(flag==true)
					{
						Pass("Category title matches with the selected category",log);
					}
					else
					{
						Fail("Category title not matches with the selected category",log);
					}
				}
				else
				{
					Fail("PLP header is not displayed");
				}
			}
			else
			{
				Skip("Category title not selected");
			}			
		}
	
	/*HUDSONBAY-780 Verify that on loading more Products then the The products in PLP page should matched with the products in stream call.*/
	public void PLP_loadMoreProducts_comparingStream() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-780 Verify that on loading more Products then the The products in PLP page should matched with the products in stream call");
			String srchKey = HBCBasicfeature.getExcelVal("searchkey", sheet, 1);
			ArrayList<String> streamResp = new ArrayList<>();
			ArrayList<String> siteResp = new ArrayList<>();
			ArrayList<Integer> result = new ArrayList<Integer>();
			String id = "";
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{											
						searchBox.clear();
						searchBox.sendKeys(srchKey);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(500);
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							int bfre_size = plpItemContList.size();
							log.add("Site Resp: Before loading products the size is: "+bfre_size);
							Thread.sleep(500);
							HBCBasicfeature.scrolldown(footerBottomCopyright, driver);	
							boolean loaded = false;
							do
							{	
								int size = plpItemContList.size();
								if(size>100)
								{
									loaded=true;
								}
								else
								{
									loaded=false;
								}
								
							}while(!loaded==true);							
							Thread.sleep(500);
							int aftr_size =  plpItemContList.size();						
							log.add("Site Resp: After lazy loading products size is "+aftr_size);
							Thread.sleep(500);
							if(aftr_size>bfre_size)
							{
								log.add("loaded more products after lazy loading in PLP page");
								System.out.println("Getting product ID from site...");
								//Getting product ID from site
								
								for(int i=100;i<aftr_size;i++)
								{
									Thread.sleep(200);
									String product_id = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])["+(i+1)+"]//img")).getAttribute("identifier");		
									siteResp.add(product_id);
								}
								
								//Getting Stream ID value for the loaded products 
														
								String Stream = HBConstants.plpURL;
								String plpStream = Stream.replace("searchKey", srchKey);
								driver.navigate().to(plpStream);
								String response = driver.findElement(By.xpath("/html/body/pre")).getText();
								Thread.sleep(1000);
								driver.navigate().back();
								JSONObject jObj1 = new JSONObject(response);
								JSONObject classicResobj = jObj1.getJSONObject("children");
								JSONArray classicResArray = classicResobj.getJSONArray("products");
								Thread.sleep(200);
								int plsize = classicResArray.length();
								System.out.println("Getting product ID from StreamCall...");
								for(int i= 0;i<plsize;i++)
								{
									Thread.sleep(200);
									id = classicResArray.getJSONObject(i).getString("identifier");
									streamResp.add(id);
									//System.out.println(id); 	
								}
								
								//comparing Stream resp and site resp
								
								for (String temp : streamResp)
								{
									Thread.sleep(100);
									result.add(siteResp.contains(temp) ? 1 : 0);
									//System.out.println(result);
									//log.add("Compared result is: "+result);
								}
								if(!result.contains(0))
							  	{								  		
							  		Pass("On loading more Products then the The products in PLP page matched with the products in stream call",log);
							  	}
							  	else
							  	{								  	
							  		Fail("On loading more Products then the The products in PLP page not matched with the products in stream call",log);
							  	}										
							}								
							else
							{
								Fail("Not getting response from stream");
							}
						}
						else
						{
							Fail("After page loaded products not added");
						}
					}
					else
					{
						Fail("PLP page not displayed");
					}
				}
				else
				{
					Fail("SearchBox not dispalyed");
				}						
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
			
	}
