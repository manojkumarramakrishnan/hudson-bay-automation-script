package hbPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_Pancake;

public class Hudson_Bay_Pancake_Obj extends Hudson_Bay_Pancake implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
			
	public Hudson_Bay_Pancake_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	String selectedMenu = "";
	
	String SelectedSubSubCat ="";
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+homepageDomm+"")
	WebElement homepageDom;
		
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+pancakeMenuu+"")
	WebElement pancakeMenu;
		
	@FindBy(xpath = ""+maskk+"")
	WebElement pancakeMask;
		
	@FindBy(xpath = ""+pancakeAdditionalContactt+"")
	WebElement pancakeAdditionalContact;
	
	@FindBy(xpath = ""+pancakeDynamicCategoryy+"")
	WebElement pancakeDynamicCategory;
		
	@FindBy(xpath = ""+pancakeStaticCategoryy+"")
	WebElement pancakeStaticCategory;
		
	@FindBy(xpath = ""+pancakeStaticColorr+"")
	WebElement pancakeStaticColor;

	@FindBy(xpath = ""+pancakeCategoryActivee+"")
	WebElement pancakeCategoryActive;	
	
	/*@FindBy(xpath = "//*[contains(@class,'sk_menuOpen skMob_currentSelItem')]//*[@class='sk_mobCategoryItemElm']//*[@class='sk_mobCategoryItemTxt']")
	WebElement pancakeSubCategoryActive;	*/
	
	@FindBy(xpath = ""+pancakeSubCategoryArroww+"")
	WebElement pancakeSubCategoryArrow;	
			
	@FindBy(xpath = ""+pancakemenuBackButtonn+"")
	WebElement pancakemenuBackButton;
	
	@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id_8_0']")
	WebElement pancakemenuToys;
		
	@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id_1_0']")
	WebElement pancakeWomen;
	
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+plpHeaderTitlee+"")
	WebElement PLPPageTitle;
	
	@FindBy(xpath = ""+signInButtonn+"")
	WebElement signInButton;
	
	@FindBy(xpath = ""+signInPagee+"")
	WebElement signInPage;
	
	@FindBy(xpath = ""+emailFieldd+"")
	WebElement emailField;
	
	@FindBy(xpath = ""+pwdFieldd+"")
	WebElement pwdField;
	
	@FindBy(xpath = ""+myAccountPagee+"")
	WebElement myAccountPage;
	
	@FindBy(xpath = ""+myAccountPageNamee+"")
	WebElement myAccountPageName;	
	
	
	
	/**************Static Links xpaths*****************/
	
	@FindBy(xpath = ""+pancakeStaticWelcomee+"")
	WebElement pancakeStaticWelcome;
	
	@FindBy(xpath = ""+pancakeStaticViewFlyerr+"")
	WebElement pancakeStaticViewFlyer;
	
	@FindBy(xpath = ""+pancakeStaticlanguagee+"")
	WebElement pancakeStaticlanguage;
	
	@FindBy(xpath = ""+defaultlanguagee+"")
	WebElement defaultlanguage;
	
	@FindBy(xpath = ""+otherlanguagee+"")
	WebElement otherlanguage;
	
	@FindBy(xpath = ""+pancakeStaticContactUss+"")
	WebElement pancakeStaticContactUs;
	
	@FindBy(xpath = ""+contactUsPagee+"")
	WebElement contactUsPage;
	
	@FindBy(xpath = ""+pancakeStorePagee+"")
	WebElement pancakeStorePage;
	
	
	//HUDSONBAY-44
	//@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id_1_0']")
	@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id_2_0']")
	WebElement pancakemenuShoe;
	
	//@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id__1_1']")
	@FindBy(xpath = "(//*[@class='sk_mobCategoryItem sk_New Arrivals'])[2]")
	WebElement pancakemenushoeNewArrival;
	
	//@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id__2_152']")
	@FindBy(xpath = "(//*[@class='sk_mobCategoryItem sk_Women']//*[@id='sk_mobCategoryItemCont_id_2_2_1'])[1]")
	WebElement pancakesubmenuShoeWomenNewArrival;

	//To be changed
	
	
	/************Lists*************/
	
	@FindBy(how = How.XPATH,using = ""+pancakeMenulistt+"")
	List<WebElement> pancakeMenulist;	
	
	@FindBy(how = How.XPATH,using = ""+pancakeMenuActivelistt+"")
	List<WebElement> pancakeMenuActivelist;
	
	@FindBy(how = How.XPATH,using = ""+pancakeStaticListt+"")
	List<WebElement> pancakeStaticList;
	
	@FindBy(how = How.XPATH,using = ""+pancakeSub_SubListt+"")
	List<WebElement> pancakeSub_SubList;
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
		
	/*HUDSONBAY-40 Verify that Pancake icon Should be displayed at the header as per the creative */
	public void pancakeIcon()
		{
			ChildCreation("HUDSONBAY-40 Verify that Pancake icon Should be displayed at the header as per the creative ");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));			
				if(HBCBasicfeature.isElementPresent(header))
				{
					wait.until(ExpectedConditions.visibilityOf(hamburger));			
					if(HBCBasicfeature.isElementPresent(hamburger)) 
					{
						Pass("Pancake icon displayed in the header as per the creative");
					}
					else
					{
						Fail("Pancake icon not displayed in the header as per the creative");
					}
				}
				else
				{
					Fail("Header not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-41 Verify that while selecting the pancake icon,the pancake menu should render smoothly*/
	public void pancakeTap()
		{
			ChildCreation("HUDSONBAY-41 Verify that while selecting the pancake icon,the pancake menu should render smoothly");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(hamburger));			
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					Thread.sleep(1000);
					action.moveToElement(hamburger).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(pancakeMenu));			
					if(HBCBasicfeature.isElementPresent(pancakeMenu))
					{
						Pass("Tapping Pancake Icon pancake menu displayed");
					}
					else
					{
						Fail("Tapping Pancake Icon pancake menu not displayed");
					}						
				}
				else
				{
					Fail("Pancake icon not displayed in the header");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}		
		}

	/*HUDSONBAY-52 Verify that Pancake menu should display all the categories and its sub categories*/
	public void pancakeCategoriesSubCat()
		{
			ChildCreation("HUDSONBAY-52 Verify that Pancake menu should display all the categories and its sub categories");
			try
			{
				ArrayList<Integer> categoryId = new ArrayList<>();	
				//categoryId.addAll(Arrays.asList(1, 2, 3, 4, 5));
				boolean subcatpresent = false;
				Random r = new Random();
				String selectedMenuName ="";
				String selectedCategoryName ="";
				String selectedSubCategoryName = "";
				String selectedSub_subCategoryName = "";
				int i = 0, j = 0, k=0, m = 0 ;				
				int size = pancakeMenulist.size();
				log.add("The Pancake menu size is "+size);
				
				do
				{
					try
					{				
						
						/*********************Menu selection*********************/
						
						boolean Cat = false;
						do
						{								
							try
							{			
								boolean flag;						
								do
								{
									flag = false;
									i = r.nextInt(size);
									if(i==0)
									{
										i=i+1;
									}
									if(!categoryId.contains(i))
									{
										flag = true;	
									}
									else
									{
										flag=false;
										continue;
									}
								}
								while(!flag==true);							
								categoryId.add(i);
								WebElement selectedCategory = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
								Thread.sleep(1000);
								selectedMenuName = selectedCategory.getText();
								Thread.sleep(1000);
								log.add("The selected Menu is: "+selectedMenuName);	
								
								//WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']"));
								WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]"));
								if(HBCBasicfeature.isElementPresent(category))
								{
									Cat =true;
									selectedCategory.click();	
									break;
								}
							}
							catch(Exception e)
							{
								Cat =false;
								continue;
							}	
						}
						while(!Cat==true);				
						log.add("The Pancake menu " +selectedMenuName+" has category");
					
								/*********************Category selection*********************/
					
						
						
						//List<WebElement> categorySize = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']"));
						List<WebElement> categorySize = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]"));
						int CatSize = categorySize.size();				
						log.add("The Pancake menu category size is "+CatSize);	
							boolean flagcat = false;
							for(j=1	;j<=CatSize;j++)
							{							
								//WebElement selectedsubCategory = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"]"));
								WebElement selectedsubCategory = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+j+"]"));
								selectedCategoryName = selectedsubCategory.getText();
								try
								{	
									//WebElement subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']"));
									WebElement subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+j+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]"));
									if(HBCBasicfeature.isElementPresent(subCategory))
									{
										subcatpresent=true;
										flagcat = true;
										selectedsubCategory.click();	
										Pass("The selected category " +selectedCategoryName+" has sub category",log);
										Thread.sleep(1000);
										break;
									}
								}
								catch(Exception e)
								{
									//log.add("The selected category "+selectedCategoryName+" has no sub category");	
									continue;
								}							
							}	
							if(flagcat != true)
							{
								Pass("The selected category has no sub category");
								pancakemenuBackButton.click();	
							}
					}				
					catch (Exception e)
					{
						System.out.println(e.getMessage());
						Exception("There is something wrong. Please Check." + e.getMessage());
					}	
				}				
				while(!subcatpresent==true);
				
				/*************************SubCategory Selection*************************/
			
				//List<WebElement> SubcategorySize =  driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']"));
				List<WebElement> SubcategorySize =  driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+j+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]"));
				int subCatSize = SubcategorySize.size();
				log.add("The respective category's sub category size is "+subCatSize);	
				
				boolean status = false;							
				for(k=1	;k<=subCatSize;k++)
				{
					//WebElement selectedsub_SubCategory = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']["+k+"]"));
					WebElement selectedsub_SubCategory = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+j+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+k+"]"));
					selectedSubCategoryName = selectedsub_SubCategory.getText();
					try
					{
						//WebElement sub_SubCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']["+k+"]//*[@id='sk_mobSublevel__2']"));
						WebElement sub_SubCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+j+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+k+"]//*[contains(@id,'sk_mobSublevel_2')]"));
						if(HBCBasicfeature.isElementPresent(sub_SubCategory))
						{
							status=true;
							selectedsub_SubCategory.click();	
							Pass("The selected sub category " +selectedSubCategoryName+" has sub sub category",log);
							Thread.sleep(1000);
							break;
						}									
					}
					catch(Exception e)
					{
						//log.add("The selected sub category "+selectedSubCategoryName+" has no sub sub category");	
						continue;
					}
				}
				if(status!=true)
				{
					Pass("The selected sub category "+selectedSubCategoryName +" has no sub sub category");
					pancakemenuBackButton.click();	
				}
				else
				{				
					/*************************SubSubCategory Selection*************************/
									
					//List<WebElement> SubSubcategorySize =  driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']["+k+"]//*[@id='sk_mobSublevel__2']//*[@id='sk_mobCategoryItem_id__3']"));
					List<WebElement> SubSubcategorySize =  driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+j+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+k+"]//*[contains(@id,'sk_mobSublevel_2')]//*[contains(@id,'sk_mobCategoryItem_id_3')]"));
					int subSubCatSize = SubSubcategorySize.size();
					log.add("The respective sub category's sub-sub category size is "+subSubCatSize);	
				
					boolean flag = false;
					for(m=1	;m<=subSubCatSize;m++)
					{
						//WebElement selectedsub_sub_SubCategory = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']["+k+"]//*[@id='sk_mobSublevel__2']//*[@id='sk_mobCategoryItem_id__3']["+m+"]"));
						WebElement selectedsub_sub_SubCategory = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+j+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+k+"]//*[contains(@id,'sk_mobSublevel_2')]//*[contains(@id,'sk_mobCategoryItem_id_3')]["+m+"]"));
						selectedSub_subCategoryName = selectedsub_sub_SubCategory.getText();
						try
						{
							//WebElement sub_Sub_subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']["+k+"]//*[@id='sk_mobSublevel__2']//*[@id='sk_mobCategoryItem_id__3']["+m+"]//*[@id='sk_mobSublevel__3']"));
							WebElement sub_Sub_subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]["+j+"])//*[contains(@id,'sk_mobCategoryItem_id_2')]["+k+"]//*[contains(@id,'sk_mobSublevel_2')]//*[contains(@id,'sk_mobCategoryItem_id_3')]["+m+"]//*[contains(@id,'sk_mobSublevel_3')]"));
							if(HBCBasicfeature.isElementPresent(sub_Sub_subCategory))
							{
								flag = true;
								selectedsub_sub_SubCategory.click();
								Pass("The selected sub sub  category " +selectedSub_subCategoryName+" has sub sub sub category",log);
								Thread.sleep(1000);
								break;
							}
						}							
						catch(Exception e)
						{
							//log.add("The selected sub sub category " +selectedSub_subCategoryName+" has no sub sub sub category");	
							continue;
						}
					}				
					if(flag!=true)
					{
						Pass("The selected sub sub category has no further categories");
						pancakemenuBackButton.click();	
					}			
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/* HUDSONBAY-59 Verify that user should be able to select the all the options in the Pancake slider*/
	public void pancakeMenus()
		{
			ChildCreation(" HUDSONBAY-59 Verify that user should be able to select the all the options in the Pancake slider");
			try
			{
				String selectedCategoryName ="";
				WebElement selectedCategory;
				int size = pancakeMenulist.size();
				log.add("The Pancake menu size is "+size);
				for(int i=1;i<size;i++)
				{
					selectedCategory = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
					Thread.sleep(200);
					selectedCategoryName = selectedCategory.getText();
					try
					{						
						//WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']"));
						WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id_1_1']"));
						if(HBCBasicfeature.isElementPresent(category))
						{
							selectedCategory.click();	
							wait.until(ExpectedConditions.visibilityOf(pancakemenuBackButton));			
							if(HBCBasicfeature.isElementPresent(pancakemenuBackButton))
							{
								log.add("The selected Menu is"+selectedCategoryName);
								pancakemenuBackButton.click();
							}
						}
					}
					catch(Exception e)
					{
						selectedCategory.click();	
						log.add("The selected Menu is"+selectedCategoryName);
						if(HBCBasicfeature.isElementPresent(hamburger))
						{
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
						}
						else
						{
							driver.navigate().back();
							((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);							
						}
						continue;
					}	
				}
				Pass("user able to select the all the options in the Pancake slider",log);
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}	
	
	/*HUDSONBAY-42 Verify that on tapping the pancake icon when the pancake is in enabled state, the pancake should be closed*/
	public void pancakeClose()
		{
			ChildCreation("HUDSONBAY-42 Verify that on tapping the pancake icon when the pancake is in enabled state, the pancake should be closed");
			boolean childFlag = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(pancakeMenu))
				{
					log.add("The pancake menu is in enabled state");
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", pancakeMask);			
					Thread.sleep(2000);
					if(!HBCBasicfeature.isElementPresent(pancakeMask))
					{
						childFlag = true;
						Pass("Tapping the pancake icon when the pancake is in enabled state, the pancake closed",log);
					}
					else
					{
						Fail("Tapping the pancake icon when the pancake is in enabled state, the pancake closed",log);
					}						
				}
				else
				{
					Fail("Pancake Menu is not in enabled state");
				}	
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-51 Verify that on tapping the background mask, the pancake should be closed");
			if(childFlag)
			{
				Pass("Tapping the background mask, the pancake closed",log);
			}
			else
			{
				Fail("Tapping the background mask, the pancake closed",log);
			}
		}
	
	/*HUDSONBAY-50 Verify that background masking should be shown, when the pancake is in open state*/
	public void pancakeMasking()
		{
			ChildCreation("HUDSONBAY-50 Verify that background masking should be shown, when the pancake is in open state");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pancakeMenu));			
				if(HBCBasicfeature.isElementPresent(pancakeMenu))
				{
					log.add("The pancake menu is in open state");								
					wait.until(ExpectedConditions.visibilityOf(pancakeMask));			
					if(HBCBasicfeature.isElementPresent(pancakeMask))
					{
						Pass("Background masking shown, when the pancake is in open state",log);
					}
					else
					{
						Fail("Background masking not shown, when the pancake is in open state",log);
					}						
				}
				else
				{
					Fail("Pancake Menu is not in enabled state");
				}	
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}		
		}
	
	/*HUDSONBAY-45 Verify that scrolling should work fine in the pancake menu*/
	public void pancakeScrolling()
		{
			ChildCreation("HUDSONBAY-45 Verify that scrolling should work fine in the pancake menu");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pancakeMenu));			
				if(HBCBasicfeature.isElementPresent(pancakeMenu))
				{
					log.add("The pancake menu is in open state");								
					HBCBasicfeature.scrolldown(pancakeAdditionalContact, driver);		
					if(HBCBasicfeature.isElementPresent(pancakeAdditionalContact))
					{
						log.add("Scroll down is working fine");
					}
					else
					{
						log.add("Scroll down is not working fine");
					}
					Thread.sleep(2000);
					HBCBasicfeature.scrollup(pancakeWomen, driver);	
					Thread.sleep(2000);
					if(HBCBasicfeature.isElementPresent(pancakeWomen))
					{
						log.add("Scroll up is working fine");
						Pass("scrolling workin fine in the pancake menu",log);
					}
					else
					{
						log.add("Scroll up is not working fine");
					}
				}
				else
				{
					Fail("Pancake Menu is not in enabled state",log);
				}	
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}		
		}
	
	/*HUDSONBAY-53 Verify that Pancake menu should display two section(i.e. Dynamic Categories & Static Links) */
	public void pancakeSections()
		{
			ChildCreation("HUDSONBAY-53 Verify that Pancake menu should display two section(i.e. Dynamic Categories & Static Links)");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(hamburger));			
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);		
					wait.until(ExpectedConditions.visibilityOf(pancakeMenu));			
					if(HBCBasicfeature.isElementPresent(pancakeMenu))
					{								
						if(HBCBasicfeature.isElementPresent(pancakeDynamicCategory))
						{
							log.add("Pancake menu Dynamic Categories section is displayed");
						}
						else
						{
							log.add("Pancake menu Dynamic Categories section is not displayed");
						}
						HBCBasicfeature.scrolldown(pancakeStaticCategory, driver);
						if(HBCBasicfeature.isElementPresent(pancakeStaticCategory))
						{
							log.add("Pancake menu Static links section is displayed");
							Pass("Pancake menu displayed two section(i.e. Dynamic Categories & Static Links)",log);
						}
						else
						{
							log.add("Pancake menu Static links section is not displayed");
							Fail("Pancake menu not displayed two section(i.e. Dynamic Categories & Static Links)",log);
						}
					}
					else
					{
						Fail("Pancake Menu is not displayed");
					}
				}
				else
				{
					Fail("Hamburger option is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-54 Verify that font difference needs to be shown for two sections(i.e. Dynamic Categories & Static Links) in the pancake slider*/
	public void pancakeSectionsFont()
		{
			ChildCreation("HUDSONBAY-54 Verify that font difference needs to be shown for two sections(i.e. Dynamic Categories & Static Links) in the pancake slider");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pancakeMenu));			
				if(HBCBasicfeature.isElementPresent(pancakeMenu))
				{
					int size = pancakeMenulist.size();
					Random r = new Random();
					int i = r.nextInt(size);
					if(i==0)
					{
						i=i+1;
					}
					String dynamic_Color = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItemCont_id_"+i+"_0']//*[@class='sk_mobCategoryItemTxt']")).getCssValue("color");
					Color colorhxcnvt = Color.fromString(dynamic_Color);
					String dynamic_hexCode = colorhxcnvt.asHex();
					log.add("The Dynamice sections font is: "+dynamic_hexCode);
					wait.until(ExpectedConditions.visibilityOf(pancakeStaticColor));			
					if(HBCBasicfeature.isElementPresent(pancakeStaticColor))
					{
						String static_Color = pancakeStaticColor.getCssValue("color");
						Color colorhxcnvt1 = Color.fromString(static_Color);
						String static_hexCode = colorhxcnvt1.asHex();
						log.add("The static links font is: "+static_hexCode);
						if(dynamic_hexCode!=static_hexCode)
						{
							Pass("Font difference shown for two sections(i.e. Dynamic Categories & Static Links) in the pancake slider",log);
						}
						else
						{
							Fail("Font difference not shown for two sections(i.e. Dynamic Categories & Static Links) in the pancake slider",log);
						}
					}
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-56 Verify that on selecting any category in the pancake slider, the selected category name needs to be highlighted in bold and all its corresponding sub categories needs to be shown below*/
	public void pancakeMenuBold() throws Exception
		{
			ChildCreation("HUDSONBAY-56 Verify that on selecting any category in the pancake slider, the selected category name needs to be highlighted in bold and all its corresponding sub categories needs to be shown below");
			String bold = HBCBasicfeature.getExcelVal("HB56", sheet,1);
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(pancakeMenu));			
				if(HBCBasicfeature.isElementPresent(pancakeMenu))
				{	
					Random r  = new Random();	
					int size = pancakeMenulist.size();
					int i = r.nextInt(size);	
					if(i==0)
					{
						i=i+1;
					}					
					WebElement menu = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
					log.add("The selected category name is: "+menu.getText());
					action.moveToElement(menu).click().build().perform();
					Thread.sleep(2000);
					String menuColor = pancakeCategoryActive.getCssValue("font-family");
					log.add("The selected category font is: "+menuColor);
					if(menuColor.contains(bold))
					{
						Pass("On selecting any category in the pancake slider, the selected category name highlighted in bold",log);
						wait.until(ExpectedConditions.attributeContains(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']"), "style", "block"));
						boolean flag = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']")).getAttribute("style").contains("block");
						if(flag==true)
						{
							Pass("On selecting any category in the pancake slider, the selected category name highlighted in bold and corresponding sub categories displayed");
						}
						else
						{
							Fail("On selecting any category in the pancake slider, the selected category name highlighted in bold and corresponding sub categories are not displayed");
						}
					}
					else
					{
						Fail("On selecting any category in the pancake slider, the selected category name not highlighted in bold",log);
					}					
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}

	/*HUDSONBAY-57 Verify that on selecting any category in the pancake slider, all the other categories should be hidden and the "< Back" option should be shown with the selected category highlighted in bold*/
	public void pancakeMenuBack()
		{
			ChildCreation("HUDSONBAY-57 Verify that on selecting any category in the pancake slider, all the other categories should be hidden and the '< Back' option should be shown with the selected category highlighted in bold");
			try
			{	
				int size = pancakeMenuActivelist.size();
				if(size==1)
				{
					log.add("on selecting any category in the pancake slider all the other categories are hidden");
					wait.until(ExpectedConditions.visibilityOf(pancakemenuBackButton));			
					if(HBCBasicfeature.isElementPresent(pancakemenuBackButton))
					{
						Pass("on selecting any category in the pancake slider all the other categories are hidden and the '< Back' option shown with the selected category highlighted in bold",log);
					}
					else
					{
						Fail("on selecting any category in the pancake slider all the other categories are hidden and the '< Back' option shown with the selected category highlighted in bold",log);
					}
				}
				else
				{
					Fail("on selecting any category in the pancake slider all the other categories are not hidden");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-58 Verify that on selecting "< Back" icon, it should navigates to the previous level*/
	public void pancakeBackClick()
		{
			ChildCreation("HUDSONBAY-58 Verify that on selecting '< Back' icon, it should navigates to the previous level");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pancakeCategoryActive));			
				if(HBCBasicfeature.isElementPresent(pancakemenuBackButton))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", pancakemenuBackButton);		
					if(!HBCBasicfeature.isElementPresent(pancakeCategoryActive))
					{
						Pass("on selecting '< Back' icon, navigates to the previous level");
					}
					else
					{
						Fail("on selecting '< Back' icon, it not navigates to the previous level");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-247 Verify that Pancake menu should consists of all the categories(Dynamic links) as per the creative.*/
	public void pancakeDynamicMenu() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-247 Verify that Pancake menu should consists of all the categories(Dynamic links) as per the creative.");
			String list = HBCBasicfeature.getExcelVal("HB247", sheet, 1);
			String[] sheetContents = list.split("\n");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pancakeMenu));			
				if(HBCBasicfeature.isElementPresent(pancakeMenu))
				{
					boolean menuFound = true;
					int size= pancakeMenulist.size();
					for(int i=0;i<size-1;i++)
					{
						//String menuName = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItem ']["+(i+1)+"]//*[@type='category']//*[@class='sk_mobCategoryItemTxt']")).getText();
						String menuName = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItemCont_id_"+(i+1)+"_0']//*[@class='sk_mobCategoryItemTxt']")).getText();
						for(int j=0;j<sheetContents.length;j++)		
						{
							if(menuName.equalsIgnoreCase(sheetContents[j]))
							{
								log.add("The pancake menu category name is: "+menuName);
								menuFound = true;
								break;
							}
							else
							{
								menuFound = false;
								continue;
							}
						}
					}
					if(menuFound == true)
					{
						Pass("Pancake menu consists of all the categories(Dynamic links) as per the creative.", log);
					}
					else
					{
						Fail("Pancake menu not consists of all the categories(Dynamic links) as per the creative.", log);
					}				
				}
				else
				{
					Fail("Pancake menu list not available");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-248 Verify that Pancake menu should consists of all the categories(Static links) as per the creative.*/
	public void pancakeStaticMenu() throws Exception
		{
			String list = HBCBasicfeature.getExcelVal("HB248", sheet, 1);
			String lang = HBCBasicfeature.getExcelVal("HB248*", sheet, 1);
			String sheetContenets[] = list.split("\n");
			ChildCreation("HUDSONBAY-248 Verify that Pancake menu should consists of all the categories(Static links) as per the creative.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pancakeStaticCategory));			
				if(HBCBasicfeature.isElementPresent(pancakeStaticCategory))
				{
					HBCBasicfeature.scrolldown(pancakeStaticCategory, driver);
					boolean menuFound = true;
					int size = pancakeStaticList.size();
					for(int i=0;i<size;i++)
					{						
						String menuName = driver.findElement(By.xpath("//*[@class='sk_additionalLinksItem ']["+(i+1)+"]//*[@class='sk_additionalLinksItemTxt']")).getText();
						for(int j=0;j<=sheetContenets.length;j++)
						{
							if(menuName.equalsIgnoreCase(sheetContenets[j]))
							{
								log.add("The static menu name is: "+menuName);
								menuFound = true;
								break;							
							}
							else
							{
								menuFound = false;
								continue;
							} 
						}						
					}
					String frenchName = driver.findElement(By.xpath("//*[@class='sk_additionalLinksItemTxt sk_lang sk_lang_french']")).getText();
					if(frenchName.equalsIgnoreCase(lang))
					{
						log.add("The static menu name is:"+frenchName);
					}
					else
					{
						log.add("The static menu name not present is:"+frenchName);
					}
					if(menuFound == true)
					{
						Pass("Pancake menu consists of all the categories(Static links) as per the creative", log);
					}
					else
					{
						Fail("Pancake menu not consists of all the categories(Static links) as per the creative", log);
					}
				}
				else
				{
					Fail("Pancake menu list not available");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}

	/*HUDSONBAY-44 Verify that Pancake menu bar & all the menus in the pancake should work fine in all the pages*/
	public void pancakeWorkingFine()
		{
			ChildCreation("HUDSONBAY-44 Verify that Pancake menu bar & all the menus in the pancake should work fine in all the pages");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(pancakemenuShoe));
				if(HBCBasicfeature.isElementPresent(pancakemenuShoe))
				{
					pancakemenuShoe.click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(pancakemenushoeNewArrival));
					if(HBCBasicfeature.isElementPresent(pancakemenushoeNewArrival))
					{
						pancakemenushoeNewArrival.click();
						wait.until(ExpectedConditions.visibilityOf(pancakesubmenuShoeWomenNewArrival));
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(pancakesubmenuShoeWomenNewArrival))
						{	
							pancakesubmenuShoeWomenNewArrival.click();
							Thread.sleep(1000);
							boolean pgLoad = false;
							do
							{				
								try
								{
									if(HBCBasicfeature.isElementPresent(plpPage))		
									{
										pgLoad=true;	
										break;		
									}
								}
								catch (Exception e)
								{
									pgLoad=false;
									driver.navigate().refresh();	
									continue;
								}
							}
							while(!pgLoad==true);
							if(HBCBasicfeature.isElementPresent(plpPage))
							{
								log.add("PLP page displayed");
								HBCBasicfeature.jsclick(hamburger, driver);
								Thread.sleep(500);
								String selectedCategoryName ="";
								WebElement selectedCategory;
								int size = pancakeMenulist.size();
								log.add("The Pancake menu size is "+size);
								for(int i=1;i<size;i++)
								{
									selectedCategory = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
									Thread.sleep(500);
									selectedCategoryName = selectedCategory.getText();
									try
									{						
										//WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']"));
										WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]"));
										if(HBCBasicfeature.isElementPresent(category))
										{
											selectedCategory.click();	
											wait.until(ExpectedConditions.visibilityOf(pancakemenuBackButton));			
											if(HBCBasicfeature.isElementPresent(pancakemenuBackButton))
											{
												log.add("The selected Menu is"+selectedCategoryName);
												pancakemenuBackButton.click();
											}
										}
									}
									catch(Exception e)
									{
										selectedCategory.click();	
										log.add("The selected Menu is"+selectedCategoryName);
										if(HBCBasicfeature.isElementPresent(hamburger))
										{
										((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
										}
										else
										{
											driver.navigate().back();
											((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);							
										}
										continue;
									}	
								}
								Pass("Pancake menu bar & all the menus in the pancake working fine in the PLP pages",log);	
								((JavascriptExecutor) driver).executeScript("arguments[0].click();", pancakeMask);
					
								/*****PDP Page****/
								
								Random r = new Random();
								int Psize = plpItemContList.size();
								int i = r.nextInt(Psize);
								Thread.sleep(500);
								WebElement product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
								wait.until(ExpectedConditions.visibilityOf(product));
								HBCBasicfeature.scrolldown(product, driver);
								boolean pgload = false;
								do
								{
									try
									{
										if(HBCBasicfeature.isElementPresent(product))
										{
											product.click();
											wait.until(ExpectedConditions.visibilityOf(PDPPage));
											if(HBCBasicfeature.isElementPresent(PDPPage))
											{
												pgload= true;
												break;
											}
										}
									}
									catch(Exception e)
									{
										pgload= false;
										continue;
									}										
								}
								while(!pgload==true);
								if(HBCBasicfeature.isElementPresent(PDPPage))
									{
										Thread.sleep(1000);
										log.add("PDP page displayed");
										wait.until(ExpectedConditions.visibilityOf(hamburger));
										action.moveToElement(hamburger).click().build().perform();
										String selectedPDPCategoryName ="";
										WebElement selectedPDPCategory;
										int size1 = pancakeMenulist.size();
										log.add("The Pancake menu size is "+size1);
										for(int j=1;j<size1;j++)
										{
											selectedPDPCategory = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+j+"_0']"));
											Thread.sleep(500);
											selectedPDPCategoryName = selectedPDPCategory.getText();
											try
											{						
												//WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+j+"_0']//*[@id='sk_mobCategoryItem_id__1']"));
												WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+j+"_0']//*[contains(@id,'sk_mobCategoryItem_id_1')]"));
												if(HBCBasicfeature.isElementPresent(category))
												{
													selectedPDPCategory.click();	
													wait.until(ExpectedConditions.visibilityOf(pancakemenuBackButton));			
													if(HBCBasicfeature.isElementPresent(pancakemenuBackButton))
													{
														log.add("The selected Menu is"+selectedPDPCategoryName);
														pancakemenuBackButton.click();
													}
												}
											}
											catch(Exception e)
											{
												selectedPDPCategory.click();	
												log.add("The selected Menu is"+selectedPDPCategoryName);
												if(HBCBasicfeature.isElementPresent(hamburger))
												{
												((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
												}
												else
												{
													driver.navigate().back();
													((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);							
												}
												continue;
											}	
										}
										Pass("Pancake menu bar & all the menus in the pancake working fine in the PDP pages",log);	
									}
									else
									{
										Fail("PDP Page not displayed");
									}
								}								
							}
							else
							{
								Fail("PLP page not displayed");
							}							
						}
						else
						{
							Fail("Handbag menu New Arrival category not displayed");
						}
					}
					else
					{
						Fail("Handbag menu new arrival not displayed");
					}				
				}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-60/62/250/249 Verify that on selecting the sub categories under category, user should be able to view the next level with the sub category name highlighted in bold*/
	public void pancakeSubMenuBold() throws Exception
		{
			String bold = HBCBasicfeature.getExcelVal("HB56", sheet,1);
			//String split = BNBasicfeature.getExcelVal("HB60", sheet, 1);	
			//String[] id = split.split("\n");
						
			boolean flag = false;
			try
			{
				ChildCreation("HUDSONBAY-60 Verify that on selecting the sub categories under category, user should be able to view the next level with the sub category name highlighted in bold");
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(pancakeMenu));	
				if(HBCBasicfeature.isElementPresent(pancakemenuToys))
				{	
					action.moveToElement(pancakemenuToys).click().build().perform();
					log.add("The selected Menu is: "+pancakeCategoryActive.getText());					
					Thread.sleep(1000);
					
					//Category Baby of Menu Toys
					
					WebElement category = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]"));
					wait.until(ExpectedConditions.visibilityOf(category));	
					HBCBasicfeature.scrolldown(category, driver);
					Thread.sleep(500);
					category.click();
					Thread.sleep(500);
					log.add("The selected Category is: "+pancakeCategoryActive.getText());
					Thread.sleep(1000);
					
					//Sub Category BabyClothing of Menu Toys
					
					WebElement subCategory = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]"));
					wait.until(ExpectedConditions.visibilityOf(subCategory));						
					HBCBasicfeature.scrolldown(subCategory, driver);
					subCategory.click();
					Thread.sleep(1000);
					
					selectedMenu = pancakeCategoryActive.getText();
					log.add("The selected SubCategory is: "+selectedMenu);
									
					String menuStyle = pancakeCategoryActive.getCssValue("font-family");	
					log.add("The selected category font is: "+menuStyle);
					if(menuStyle.contains(bold))
					{
						log.add("On selecting any sub category under category, the selected category name highlighted in bold");
						Thread.sleep(1000);
						WebElement subCategoryView = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobSublevel_2_2'][@style='display: block;']"));
						wait.until(ExpectedConditions.visibilityOf(subCategoryView));			
						if(HBCBasicfeature.isElementPresent(subCategoryView))
						{
							flag = true;
							Pass("On selecting any sub category under category, the selected category name highlighted in bold and corresponding next level categories are displayed",log);
						}
						else
						{
							Fail("On selecting any sub category under category, the selected category name highlighted in bold and corresponding next level categories are not displayed",log);
						}						
					}
					else
					{
						Fail("On selecting any category in the pancake slider, the selected category name not highlighted in bold",log);
					}					
				}
				else
				{
					Fail("Pancake Menu JEWELLERY & ACCESSORIES not displayed");
				}
			}			
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			CategoryBackArrow();
			
			CategoryBackArrowClick();
			
			categoryBackground();
			
			if(flag==true)
			{
				ChildCreation("HUDSONBAY-62 Verify that on selecting the sub-sub categories under any sub category, user should be able to view the next level with the sub-sub category name highlighted in bold");
				try
				{
					WebElement subSubCategory = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]//*[@id='sk_mobCategoryItem_id_3_3'][1]"));
					wait.until(ExpectedConditions.visibilityOf(subSubCategory));					
					HBCBasicfeature.scrolldown(subSubCategory, driver);
					subSubCategory.click();
					Thread.sleep(500);
					SelectedSubSubCat = pancakeCategoryActive.getText();
					log.add("The selected Sub-SubCategory is: "+SelectedSubSubCat);
					Thread.sleep(1000);
					
					String menuStyle = pancakeCategoryActive.getCssValue("font-family");	
					log.add("The selected sub-sub category font is: "+menuStyle);
					if(menuStyle.contains(bold))
					{
						log.add("On selecting any sub-sub category under sub category, the selected sub category name highlighted in bold");
						Thread.sleep(1000);
						WebElement subCategoryView = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobSublevel_3_3'][@style='display: block;']"));
						wait.until(ExpectedConditions.visibilityOf(subCategoryView));			
						if(HBCBasicfeature.isElementPresent(subCategoryView))
						{
							Pass("On selecting any sub-sub category under sub category, the selected sub category name highlighted in bold and corresponding next level categories are displayed",log);
							//pancakeSubCategoryArrow.click();
						}
						else
						{
							Fail("On selecting any sub-sub category under sub category, the selected category name highlighted in bold and corresponding next level categories are not displayed",log);
							//pancakeSubCategoryArrow.click();
						}						
					}
					else
					{
						Fail("On selecting any sub-sub category in the sub category, the selected category name not highlighted in bold",log);
					}					
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}			
			}
			else
			{
				Skip("sub category not displayed");
			}
			//subCategoryBackground();			
			//SubCategoryBackArrow();			
		}
	
	/*HUDSONBAY-65 Verify that on selecting the sub menu in the sub-sub category, it should naviagtes to the PLP page & the pancake should be closed*/
	public void pancakeSubSubMenu_To_PLP()
		{
			ChildCreation("HUDSONBAY-65 Verify that on selecing the sub menu in the sub-sub category, it should naviagtes to the PLP page & the pancake should be closed");
			try
			{
				int size = pancakeSub_SubList.size();
				Random r = new Random();
				int i = r.nextInt(size);
				WebElement sub_subCat = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]//*[@id='sk_mobCategoryItem_id_3_3'][1]//*[@id='sk_mobCategoryItem_id_4_4']["+(i+1)+"]"));
				Thread.sleep(500);
				String title = sub_subCat.getText();
				log.add("The selected sub menu title: "+title);
				wait.until(ExpectedConditions.visibilityOf(sub_subCat));	
				HBCBasicfeature.scrolldown(sub_subCat, driver);
				Thread.sleep(500);
				sub_subCat.click();	
				boolean pgload = false;
				do
				{
					try
					{
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							pgload = true;
							break;
						}
					}
					catch(Exception e)
					{
						pgload = false;
						driver.navigate().refresh();
						continue;
					}
				}
				while(pgload!=true);
				wait.until(ExpectedConditions.visibilityOf(plpPage));			
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					log.add("PLP page displayed");
					String plptitle = PLPPageTitle.getText();
					log.add("The PLP page title: "+plptitle);
					if((title.equalsIgnoreCase(plptitle))&&(!HBCBasicfeature.isElementPresent(pancakeMask)))
					{
						Pass("on selecing the sub menu in the sub-sub category, naviagtes to the PLP page & the pancake closed",log);
					}
					else
					{
						Fail("on selecing the sub menu in the sub-sub category, not naviagtes to the PLP page & the pancake not closed",log);
					}
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/* HUDSONBAY-250 Verify that sub categories and sub-sub categories should be greyed out on tapping the categories/sub categories*/
	public void categoryBackground() throws Exception
		{
			ChildCreation("HUDSONBAY-250 Verify that sub categories and sub-sub categories should be greyed out on tapping the categories/sub categories");
			String actualColor = HBCBasicfeature.getExcelVal("HB250", sheet, 2);
			/*String split = BNBasicfeature.getExcelVal("HB60", sheet, 1);	
			String[] id = split.split("\n");*/
			try
			{				
				String background = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]")).getCssValue("background").replace("1px solid", "").substring(0,18);
				Thread.sleep(1000);
				//System.out.println(background);
				Color colorhxcnvt = Color.fromString(background);
				String color = colorhxcnvt.asHex();
				log.add("The Subcategories color is: "+color);
				Thread.sleep(500);
				if(color.equalsIgnoreCase(actualColor))
				{
					Pass("sub categories and sub-sub categories greyed out on tapping the categories/sub categories",log);
					Thread.sleep(1000);
					WebElement subCategory = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]"));
					wait.until(ExpectedConditions.visibilityOf(subCategory));						
					HBCBasicfeature.scrolldown(subCategory, driver);
					subCategory.click();
					//((JavascriptExecutor) driver).executeScript("arguments[0].click();", subCategory);	
				}
				else
				{
					Fail("sub categories and sub-sub categories not greyed out on tapping the categories/sub categories",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-250 Verify that sub categories and sub-sub categories should be greyed out on tapping the categories/sub categories*/
	public void subCategoryBackground() throws Exception
	{
		ChildCreation("HUDSONBAY-250 Verify that sub categories and sub-sub categories should be greyed out on tapping the categories/sub categories");
		String actualColor = HBCBasicfeature.getExcelVal("HB250", sheet, 2);
		/*String split = BNBasicfeature.getExcelVal("HB60", sheet, 1);	
		String[] id = split.split("\n");*/
		try
		{			
			Thread.sleep(500);
			String background = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]//*[@id='sk_mobCategoryItem_id_3_3'][1]//*[@id='sk_mobCategoryItem_id_4_4']")).getCssValue("background").replace("1px solid", "").substring(0, 18);	
			Color colorhxcnvt = Color.fromString(background);
			String color = colorhxcnvt.asHex();
			log.add("The SubSubcategories color is: "+color);
			Thread.sleep(500);
			if(color.equalsIgnoreCase(actualColor))
			{
				Pass("sub categories and sub-sub categories greyed out on tapping the categories/sub categories",log);
			}
			else
			{
				Fail("sub categories and sub-sub categories not greyed out on tapping the categories/sub categories",log);
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
		
	/*HUDSONBAY-61 Verify that on selecting the sub categories under any category, the category name should be shown with the "<" back arrow icon*/
	public void CategoryBackArrow() throws Exception
		{
			ChildCreation("HUDSONBAY-61 Verify that on selecting the sub categories under any category, the category name should be shown with the '<' back arrow icon ");
			String block = HBCBasicfeature.getExcelVal("HB61", sheet, 1);
			try
			{
				String categoryName =  pancakeCategoryActive.getText();
				if(categoryName.equalsIgnoreCase(selectedMenu))
				{
					log.add("the category name shown is: "+categoryName);
					Thread.sleep(1000);
					String backArrow = pancakeSubCategoryArrow.getCssValue("display");
					if(backArrow.contains(block))
					{
						log.add("The '<' arrow displayed");
						Pass("on selecting the sub categories under any category, the category name  shown with the '<' back arrow icon",log);
					}
					else
					{
						Fail("on selecting the sub categories under any category, the category name not shown with the '<' back arrow icon",log);
					}
				}
				else
				{
					Fail("Selected category is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-63 Verify that on selecting the sub-sub categories under sub category, the sub category name should be shown with the "<" back arrow icon*/
	public void SubCategoryBackArrow() throws Exception
		{
			ChildCreation("HUDSONBAY-63 Verify that on selecting the sub-sub categories under sub category, the sub category name should be shown with the '<' back arrow icon");
			/*String split = BNBasicfeature.getExcelVal("HB60", sheet, 1);	
			String[] id = split.split("\n");	*/
			String block = HBCBasicfeature.getExcelVal("HB61", sheet, 1);
			try
			{
				/*WebElement subSubCategory = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]//*[@id='sk_mobCategoryItem_id_3_3'][1]/a"));
				wait.until(ExpectedConditions.visibilityOf(subSubCategory));					
				HBCBasicfeature.scrolldown(subSubCategory, driver);
				subSubCategory.click();
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", subSubCategory);	
				Thread.sleep(1500);*/
				String categoryName =  pancakeCategoryActive.getText();
				if(categoryName.equalsIgnoreCase(SelectedSubSubCat))
				{
					log.add("the sub category name shown is: "+categoryName);
					Thread.sleep(500);
					String backArrow = pancakeSubCategoryArrow.getCssValue("display");
					if(backArrow.contains(block))
					{
						log.add("The '<' arrow displayed");
						Pass("on selecting the sub-sub categories under sub category, the sub category name  shown with the '<' back arrow icon",log);
					}
					else
					{
						Fail("on selecting the sub-sub categories under sub category, the sub category name not shown with the '<' back arrow icon",log);
					}
				}
				else
				{
					Fail("Selected category is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-64 Verify that on selecting the category name or the sub category name with the "<" back arrow icon, the user should navigates to its corresponding level*/
	public void CategoryBackArrowClick()
		{
			ChildCreation("HUDSONBAY-64 Verify that on selecing the category name or the sub category name with the '<' back arrow icon, the user should navigates to its corresponding level");
			try
			{
				Thread.sleep(1000);
				String previousActiveMneu = pancakeCategoryActive.getText();
				log.add("The current sub category name is: "+previousActiveMneu);
				wait.until(ExpectedConditions.visibilityOf(pancakeSubCategoryArrow));			
				if(HBCBasicfeature.isElementPresent(pancakeSubCategoryArrow))
				{
					pancakeSubCategoryArrow.click();
					String currentActiveMenu = pancakeCategoryActive.getText();
					Thread.sleep(500);
					log.add("After selecting the arrow, The current category name is: "+currentActiveMenu);
					if(previousActiveMneu==currentActiveMenu)
					{
						Fail("on selecing the category name or the sub category name with the '<' back arrow icon not navigates to its corresponding level",log);
						//pancakemenuBackButton.click();
					}
					else
					{
						Pass("on selecing the category name or the sub category name with the '<' back arrow icon navigates to the corresponding level",log);
						//pancakemenuBackButton.click();
					}
				}
				else
				{
					Fail("the '<' back arrow icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
		
	/*HUDSONBAY-249 Verify that sub-sub categories should be shown in Title case*/
	public void subCat_TiTleCase() throws Exception
		{
			ChildCreation("HUDSONBAY-249 Verify that sub-sub categories should be shown in Title case");
			/*String split = BNBasicfeature.getExcelVal("HB60", sheet, 1);	
			String[] id = split.split("\n");	*/
			try
			{
				int ctr = 0;				
				String text = driver.findElement(By.xpath("//*[@id='sk_mobSublevel_8_0']//*[@id='sk_mobCategoryItem_id_1_1'][2]//*[@id='sk_mobCategoryItem_id_2_2'][1]//*[@id='sk_mobCategoryItem_id_3_3'][1]//*[@id='sk_mobCategoryItem_id_4_4'][1]")).getText();
				log.add("The selected Sub-Subcategory is: "+text);
				String[] text1 = text.split("");
				for(String str:text1)
				{
					str.replace("", "//W//S");
					char c = str.charAt(0);
					int ascii = (int) c;
					Thread.sleep(500);
					if(ascii<97)
					{
						ctr++;
					}				       
				}						
				if(ctr==text1.length)
				{
					Pass("sub-sub categories shown in Title case",log);
				}
				else
				{
					Fail("sub-sub categories not shown in Title case",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
		}
	
	/*HUDSONBAY-70 Verify that Welcome Sign in link should be shown in pancake menu*/
	public void pancakeWelcomeSignIn() throws Exception
		{
			ChildCreation("HUDSONBAY-70 Verify that Welcome Sign in link should be shown in pancake menu");
			String expected = HBCBasicfeature.getExcelVal("HB70", sheet, 1);
			try
			{
				if(HBCBasicfeature.isElementPresent(pancakeMask))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", pancakeMask);
					Thread.sleep(500);
				}
				wait.until(ExpectedConditions.visibilityOf(hamburger));			
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
					wait.until(ExpectedConditions.visibilityOf(pancakeStaticCategory));			
					if(HBCBasicfeature.isElementPresent(pancakeStaticCategory))
					{
						HBCBasicfeature.scrolldown(pancakeStaticCategory, driver);
						wait.until(ExpectedConditions.visibilityOf(pancakeStaticWelcome));			
						if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
						{
							String actual = pancakeStaticWelcome.getText();
							if(actual.equalsIgnoreCase(expected))
							{
								Pass("Welcome Sign in link shown in pancake menu");
							}
							else
							{
								Fail("Welcome Sign in link not shown in pancake menu");
							}
						}
						else
						{
							Fail("Welcome sign link not displayed");
						}					
					}	
					else
					{
						Fail("Pancake static category section not displayed");
					}
				}
				else
				{
					Fail("Hamburger menu not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-67 Verify that As a guest user, able to view the welcome and a link to sign-in.*/
	public void WelcomeSignInLink() throws Exception
		{
			ChildCreation("HUDSONBAY-67 Verify that As a guest user, able to view the welcome and a link to sign-in.");
			String expected = HBCBasicfeature.getExcelVal("HB70", sheet, 1);
			try
			{				
				if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
				{
					String actual = pancakeStaticWelcome.getText();
					if(actual.equalsIgnoreCase(expected))
					{
						log.add("Welcome Sign in link shown in pancake menu");
						pancakeStaticWelcome.click();
						wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
						wait.until(ExpectedConditions.visibilityOf(signInButton));			
						if(HBCBasicfeature.isElementPresent(signInButton))
						{
							Pass("SignIn page displayed for guest user",log);
						}
						else
						{
							Fail("SignIn page not displayed for guest user",log);
						}						
					}
					else
					{
						Fail("Welcome Sign in link not shown in pancake menu");
					}
				}
				else
				{
					Fail("Welcome sign link not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-68 Verify that As a registered user,able to view the welcome message and a link to access registered account.*/
	public void WelcomeRegisteredLink() throws Exception
		{
			ChildCreation("HUDSONBAY-68 Verify that As a registered user,able to view the welcome message and a link to access registered account.");
			String email = HBCBasicfeature.getExcelVal("HB68", sheet, 4);
			String pwd = HBCBasicfeature.getExcelVal("HB68", sheet, 5);
			String wlcme = HBCBasicfeature.getExcelVal("HB68", sheet, 1);
			try
			{				
				wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
				if(HBCBasicfeature.isElementPresent(emailField))
				{	
					emailField.sendKeys(email);
					if(emailField.getAttribute("value").equalsIgnoreCase(email))
					{
						log.add("Enetered Email ID is: "+ email);
						if(HBCBasicfeature.isElementPresent(pwdField))
						{	
							pwdField.sendKeys(pwd);
							if(pwdField.getAttribute("value").equalsIgnoreCase(pwd))
							{
								log.add("Enetered Password is: "+ pwd);
								if(HBCBasicfeature.isElementPresent(signInButton))
								{
									signInButton.click();
									Thread.sleep(1000);
									wait.until(ExpectedConditions.visibilityOf(myAccountPage));			
									if(HBCBasicfeature.isElementPresent(myAccountPage))
									{	
										log.add("Logged In successfully");
										String name = myAccountPageName.getText();
										wait.until(ExpectedConditions.visibilityOf(hamburger));			
										if(HBCBasicfeature.isElementPresent(hamburger))
										{
											((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
											wait.until(ExpectedConditions.visibilityOf(pancakeStaticCategory));			
											if(HBCBasicfeature.isElementPresent(pancakeStaticCategory))
											{
												HBCBasicfeature.scrolldown(pancakeStaticCategory, driver);
												wait.until(ExpectedConditions.visibilityOf(pancakeStaticWelcome));			
												if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
												{	
													String[] actual = pancakeStaticWelcome.getText().split(",");	
													log.add("Displayed welcome text is: "+actual[0]);
													log.add("Displayed user name is: "+actual[1]);
													Thread.sleep(500);
													if((actual[0].equalsIgnoreCase(wlcme))&&((actual[1].trim()).equalsIgnoreCase(name)))
													{
														Pass("As a registered user,able to view the welcome message and a link to access registered account",log);
														((JavascriptExecutor) driver).executeScript("arguments[0].click();", pancakeMask);
													}
													else
													{
														Fail("As a registered user not able to view the welcome message and a link to access registered account",log);
														((JavascriptExecutor) driver).executeScript("arguments[0].click();", pancakeMask);
													}
												}
												else
												{
													Fail("Welcome link not displayed");
												}
											}
											else
											{
												Fail("static menus are not displayed");
											}
										}
										else
										{
											Fail("Hamburger menu not displayed");
										}
									}
									else
									{
										Fail("MyAccount page not displayed after signed in");
									}
								}
								else
								{
									Fail("SignIn button not displayed");
								}
							}
							else
							{
								Fail("Password field not displayed");
							}
						}
						else
						{
							Fail("Email field not displayed");
						}
					}					
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-71 Verify that View Flyer page should be shown on selecting the View Flyer link from the pancake menu.*/	
	public void pancakeViewFlyer() throws java.lang.Exception
	{
		String url = HBCBasicfeature.getExcelVal("HB71", sheet, 3);
		ChildCreation("HUDSONBAY-71 Verify that View Flyer page should be shown on selecting the View Flyer link from the pancake menu.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(hamburger));			
			if(HBCBasicfeature.isElementPresent(hamburger))
			{	
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
				wait.until(ExpectedConditions.visibilityOf(pancakeStaticViewFlyer));	
				if(HBCBasicfeature.isElementPresent(pancakeStaticViewFlyer))
				{
					HBCBasicfeature.scrolldown(pancakeStaticViewFlyer, driver);
					pancakeStaticViewFlyer.click();
					Thread.sleep(500);
					String act_url = driver.getCurrentUrl();
					if(act_url.contains(url))
					{
						Pass("View Flyer page shown on selecting the View Flyer link from the pancake menu");
					}
					else
					{
						Fail("View Flyer page not shown on selecting the View Flyer link from the pancake menu");
					}
				}
				else
				{
					Fail("View Flyer link not displayed");
				}
			}
			else
			{
				Fail("Hamburger menu not displayed");
			}			
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-69 Verify that on default Language English should be shown on launching the site initially.*/
	public void defaultlanguage()
		{
			ChildCreation("HUDSONBAY-69 Verify that on default Language English should be shown on launching the site initially.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(hamburger));			
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
					wait.until(ExpectedConditions.visibilityOf(pancakeStaticlanguage));			
					if(HBCBasicfeature.isElementPresent(pancakeStaticlanguage))
					{	
						String selectedLang = defaultlanguage.getText();
						log.add("The selected language is: "+selectedLang);
						String disabledLang = otherlanguage.getText();
						log.add("The Disabled language is: "+disabledLang);
						if(selectedLang != disabledLang)
						{
							Pass("Default Language "+selectedLang+" shown on launching the site initially",log);
						}
						else
						{
							Fail("Default Language "+selectedLang+" not shown on launching the site initially",log);
						}
					}
					else
					{
						Fail("Default Language not displayed");
					}
				}
				else
				{
					Fail("Hamburger menu not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-245/246 Verify that while selecting the language option English/French, French/English should be disabled in the pancake menu,the respective language home page should be displayed*/
	public void languageSelection() throws java.lang.Exception
		{
			String keyword = HBCBasicfeature.getExcelVal("HB245", sheet, 1);
			String color = HBCBasicfeature.getExcelVal("HB245", sheet, 2);		
			String url = HBCBasicfeature.getExcelVal("HB245", sheet, 3);		
			try
			{
				wait.until(ExpectedConditions.visibilityOf(hamburger));			
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
					wait.until(ExpectedConditions.visibilityOf(pancakeStaticlanguage));			
					if(HBCBasicfeature.isElementPresent(pancakeStaticlanguage))
					{	
						String selectedLang = defaultlanguage.getText();
						log.add("The selected language is: "+selectedLang);
						String disabledLang = otherlanguage.getText();
						log.add("The Disabled language is: "+disabledLang);
						
						if(selectedLang.equalsIgnoreCase(keyword))
						{
							ChildCreation("HUDSONBAY-246 Verify that while selecting the language option French, English should be disabled in the pancake menu,the respective language home page should be displayed");
							wait.until(ExpectedConditions.visibilityOf(otherlanguage));			
							if(HBCBasicfeature.isElementPresent(otherlanguage))
							{
								HBCBasicfeature.scrolldown(otherlanguage, driver);
								HBCBasicfeature.jsclick(otherlanguage, driver);
								Thread.sleep(500);
								wait.until(ExpectedConditions.visibilityOf(homepageDom));
								Thread.sleep(1500);
								String act_url = driver.getCurrentUrl();
								if(act_url.contains(url))
								{
									log.add("While changing language respective homepage is displayed");
									Thread.sleep(500);
									wait.until(ExpectedConditions.visibilityOf(hamburger));			
									((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
									HBCBasicfeature.scrolldown(otherlanguage, driver);
									String actcolor = otherlanguage.getCssValue("color").replace("1px solid", "");
									Color colorhxcnvt = Color.fromString(actcolor);
									String hexCode = colorhxcnvt.asHex();
									Thread.sleep(500);
									if(hexCode.contains(color))
									{
										Pass("Selecting the language option French, English disabled in the pancake menu,the respective language home page should be displayed",log);
										driver.navigate().back();
									}
									else
									{
										Fail("Selecting the language option French, English not disabled in the pancake menu",log);
										driver.navigate().back();
									}								
								}
								else
								{
									Fail("while changing language respective homepage not displayed");
								}
							}
							else
							{
								Fail("Other language option not displayed");
							}
						}
						else
						{
							ChildCreation("HUDSONBAY-245 Verify that while selecting the language option English, French should be disabled in the pancake menu,the respective language home page should be displayed");
							wait.until(ExpectedConditions.visibilityOf(otherlanguage));			
							if(HBCBasicfeature.isElementPresent(otherlanguage))
							{
								HBCBasicfeature.scrolldown(otherlanguage, driver);
								HBCBasicfeature.jsclick(otherlanguage, driver);
								wait.until(ExpectedConditions.visibilityOf(homepageDom));
								Thread.sleep(1000);		
								String act_url = driver.getCurrentUrl();
								if(act_url.contains(url))
								{
									log.add("Changed language homepage is displayed");
									((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
									String actcolor = otherlanguage.getCssValue("color").replace("1px solid", "");
									Color colorhxcnvt = Color.fromString(actcolor);
									String hexCode = colorhxcnvt.asHex();							
									if(hexCode.contains(color))
									{
										Pass("Selecting the language option English, French disabled in the pancake menu,the respective language home page should be displayed",log);
										driver.navigate().back();
									}
									else
									{
										Fail("Selecting the language option English, French not disabled in the pancake menu",log);
										driver.navigate().back();
									}								
								}
								else
								{
									Fail("while changing language respective homepage not displayed");
								}	
							}
							else
							{
								Fail("Other language option not displayed");
							}
						}				
					}
					else
					{
						Fail("Static category link Lanuage option not displayed");
					}
				}
				else
				{
					Fail("Hamburger menu not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}

	/*HUDSONBAY-49 Verify that while switching the language option(English to French/ French to English) in the pancake menu,the respective language home page should be displayed*/
	public void lanuageSwitching()
		{
			ChildCreation("HUDSONBAY-49 Verify that while switching the language option(English to French/ French to English) in the pancake menu,the respective language home page should be displayed");
			try
			{
				String selectedLang = defaultlanguage.getText();
				log.add("The current Website language is: "+selectedLang);
				String act_Url = driver.getCurrentUrl();
				wait.until(ExpectedConditions.visibilityOf(otherlanguage));			
				if(HBCBasicfeature.isElementPresent(otherlanguage))
				{
					HBCBasicfeature.scrolldown(otherlanguage, driver);
					Thread.sleep(500);
					HBCBasicfeature.jsclick(otherlanguage, driver);
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(homepageDom));
					String Changed_Url = driver.getCurrentUrl();
					if(act_Url!=Changed_Url)
					{
						wait.until(ExpectedConditions.visibilityOf(hamburger));			
						if(HBCBasicfeature.isElementPresent(hamburger))
						{
							((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
							Thread.sleep(1000);
							log.add("After switching the current Website language is: "+defaultlanguage.getText());
							Pass("Switching the language option(English to French/ French to English) in the pancake menu,the respective language home page displayed",log);
							driver.navigate().back();
						}
						else
						{
							Fail("Hamburger menu not displayed");
						}
					}
					else
					{
						Fail("switching the Language option respective site not displayed",log);
					}
				}
				else
				{
					Fail("Other language option not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-73 Verify that Contact us page should be shown on selecting the Contact us link from the pancake menu.*/
	public void pancakeContactUS() throws Exception
		{
			ChildCreation("HUDSONBAY-73 Verify that Contact us page should be shown on selecting the Contact us link from the pancake menu.");
			String list = HBCBasicfeature.getExcelVal("HB248", sheet, 1);
			String sheetContenets[] = list.split("\n");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(hamburger));			
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
					Thread.sleep(200);
					wait.until(ExpectedConditions.visibilityOf(pancakeStaticContactUs));			
					if(HBCBasicfeature.isElementPresent(pancakeStaticContactUs))
					{
						HBCBasicfeature.scrolldown(pancakeStaticContactUs, driver);
						String expctd = pancakeStaticContactUs.getText();
						if(expctd.equalsIgnoreCase(sheetContenets[7]))
						{
							log.add("Contact Us link displayed");
							pancakeStaticContactUs.click();
							Thread.sleep(200);
							wait.until(ExpectedConditions.visibilityOf(contactUsPage));
							if(HBCBasicfeature.isElementPresent(contactUsPage)) 	
							{
								Pass("Contact us page shown on selecting the Contact us link from the pancake menu",log);
								driver.navigate().back();
							}
							else
							{
								Fail("Contact us page not shown on selecting the Contact us link from the pancake menu",log);							
							}
						}
						else
						{
							Fail("Contact Us link text not matched");
						}
					}
					else
					{
						Fail("Contact Us link not displayed");
					}
				}
				else
				{
					Fail("Hamburger menu not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-66 Veriy that Find in store page should be shown on selecting the FIND IN STORE button from the pancake*/	
	public void pancakeFindInStores() throws java.lang.Exception
	{
		String url = HBCBasicfeature.getExcelVal("HB66", sheet, 3);
		ChildCreation("HUDSONBAY-66 Veriy that Find in store page should be shown on selecting the FIND IN STORE button from the pancake");
		try
		{
			String curUrl = driver.getCurrentUrl();
			wait.until(ExpectedConditions.visibilityOf(hamburger));			
			if(HBCBasicfeature.isElementPresent(hamburger))
			{	
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", hamburger);
				wait.until(ExpectedConditions.visibilityOf(pancakeStorePage));	
				if(HBCBasicfeature.isElementPresent(pancakeStorePage))
				{
					HBCBasicfeature.scrolldown(pancakeStorePage, driver);
					pancakeStorePage.click();
					Thread.sleep(500);
					String act_url = driver.getCurrentUrl();
					if(act_url.contains(url))
					{
						Pass("Find in store page shown on selecting the  FIND IN STORE link from the pancake menu");
					}
					else
					{
						Fail("Find in store page not shown on selecting the  FIND IN STORE link from the pancake menu");
					}
					driver.navigate().to(curUrl);
				}
				else
				{
					Fail(" FIND IN STORE link not displayed");
				}
			}
			else
			{
				Fail("Hamburger menu not displayed");
			}			
		}
		catch (Exception e)
		{
			System.out.println("HBC66"+e.getMessage());
			Exception(" HBC66 There is something wrong. Please Check." + e.getMessage());
		}			
	}

	
}

