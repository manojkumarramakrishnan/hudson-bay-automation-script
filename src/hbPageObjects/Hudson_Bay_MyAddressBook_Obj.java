package hbPageObjects;

import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_MyAddressBook;

public class Hudson_Bay_MyAddressBook_Obj extends Hudson_Bay_MyAddressBook implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_MyAddressBook_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+homepagee+"")
	WebElement homepage;	
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;	
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+pancakeStaticWelcomee+"")
	WebElement pancakeStaticWelcome;	
	
	@FindBy(xpath = ""+signInPagee+"")
	WebElement signInPage;
	
	@FindBy(xpath = ""+emailFieldd+"")
	WebElement emailField;
	
	@FindBy(xpath = ""+pwdFieldd+"")
	WebElement pwdField;
	
	@FindBy(xpath = ""+signInButtonn+"")
	WebElement signInButton;
	
	@FindBy(xpath = ""+myAccountPagee+"")
	WebElement myAccountPage;
	
	@FindBy(xpath = ""+myAccountPageMyProfileTxtt+"")
	WebElement myAccountPageMyProfileTxt;
	
	@FindBy(xpath = ""+myAccountPageMyProfileDetailss+"")
	WebElement myAccountPageMyProfileDetails;
	
	@FindBy(xpath = ""+myAccountPageAddressbookk+"")
	WebElement myAccountPageAddressbook;
	
	@FindBy(xpath = ""+myAccountAddressBookpagee+"")
	WebElement myAccountAddressBookpage;
	
	@FindBy(xpath = ""+myAccountpageBackk+"")
	WebElement myAccountpageBack;	
	
	@FindBy(xpath = ""+myAccountPageAddressDropDownn+"")
	WebElement myAccountPageAddressDropDown;	
	
	@FindBy(xpath = ""+myAccountPageAddressRemoveButtonn+"")
	WebElement myAccountPageAddressRemoveButton;
	
	@FindBy(xpath = ""+myAddressBookPageAddressAddNewButtonn+"")
	WebElement myAddressBookPageAddressAddNewButton;
		
	@FindBy(xpath = ""+myPageFnamee+"")
	WebElement myPageFname ;
	
	@FindBy(xpath = ""+myPageLnamee+"")
	WebElement myPageLname;
	
	@FindBy(xpath = ""+myPageEmaill+"")
	WebElement myPageEmail;
		
	@FindBy(xpath = ""+myPageStAddress+"")
	WebElement myPageStAddres;
	
	@FindBy(xpath = ""+myPage2StAddress+"")
	WebElement myPage2StAddres;
	
	@FindBy(xpath = ""+myPageCityy+"")
	WebElement myPageCity;
	
	@FindBy(xpath = ""+myAddressBookPageCountryy+"")
	WebElement myAddressBookPageCountry;
	
	@FindBy(xpath = ""+myPageStatee+"")
	WebElement myPageState;	
	
	@FindBy(xpath = ""+myPageZipcodee+"")
	WebElement myPageZipcode;	
	
	@FindBy(xpath = ""+myAddressBookPagePhoneFieldd+"")
	WebElement myAddressBookPagePhoneField;
	
	@FindBy(xpath = ""+myPagePhoneExtt+"")
	WebElement myPagePhoneExt ;
			
	@FindBy(xpath = ""+myAddressBookPageUpdatee+"")
	WebElement myAddressBookPageUpdate;
	
	@FindBy(xpath = ""+myAddressBookNewFnamee+"")
	WebElement myAddressBookNewFname;
	
	@FindBy(xpath = ""+myAddressBookNewLnamee+"")
	WebElement myAddressBookNewLname;
	
	@FindBy(xpath = ""+myAddressBookNewStAddress+"")
	WebElement myAddressBookNewStAddres;
	
	@FindBy(xpath = ""+myAddressBookNew2StAddress+"")
	WebElement myAddressBookNew2StAddres;
	
	@FindBy(xpath = ""+myAddressBookNewCityy+"")
	WebElement myAddressBookNewCity;
	
	@FindBy(xpath = ""+myAddressBookNewStatee+"")
	WebElement myAddressBookNewState;
		
	@FindBy(xpath = ""+myAddressBookNewZipcodee+"")
	WebElement myAddressBookNewZipcode;
	
	@FindBy(xpath = ""+myAddressBookPageNewCountryy+"")
	WebElement myAddressBookPageNewCountry;
	
	@FindBy(xpath = ""+myAddressBookPageNewPhoneFieldd+"")
	WebElement myAddressBookPageNewPhoneField;
	
	@FindBy(xpath = ""+myAddressBookPageNewPhoneExtt+"")
	WebElement myAddressBookPageNewPhoneExt;	
	
	@FindBy(xpath = ""+myAddressBookPageNewCancell+"")
	WebElement myAddressBookPageNewCancel;
	
	@FindBy(xpath = ""+myAddressBookPageNewSubmitt+"")
	WebElement myAddressBookPageNewSubmit;
	
	@FindBy(xpath = ""+myPageFnameAlertt+"")
	WebElement myPageFnameAlert;
	
	@FindBy(xpath = ""+myPageLnameAlertt+"")
	WebElement myPageLnameAlert;

	@FindBy(xpath = ""+myPageStAddressAlertt+"")
	WebElement myPageStAddressAlert;
	
	@FindBy(xpath = ""+myPageStCityAlertt+"")
	WebElement myPageStCityAlert;
	
	@FindBy(xpath = ""+myPageZipAlertt+"")
	WebElement myPageZipAlert;
	
	@FindBy(xpath = ""+myPagePhoneAlertt+"")
	WebElement myPagePhoneAlert;

	@FindBy(xpath = ""+myAddressBookSuccessAddAlertt+"")
	WebElement myAddressBookSuccessAddAlert;
	
	@FindBy(xpath = ""+myAddressBookSuccessRemoveAlertt+"")
	WebElement myAddressBookSuccessRemoveAlert;
	
	
	
	@FindBy(xpath = ""+addressVerificationoverlayy+"")
	WebElement addressVerificationoverlay;
	
	@FindBy(xpath = ""+addressuseasEnteredd+"")
	WebElement addressuseasEntered;
	
	
	boolean MyAddress, MyAccount = false;
	
	/*SignIn page to MyAccount-MyAddressBook Page*/
	public void signin() throws java.lang.Exception
	{
		ChildCreation("SignIn page to MyAccount-MyAddressBook Page");
		String email = HBCBasicfeature.getExcelVal("login", sheet, 4);
		String pwd = HBCBasicfeature.getExcelVal("login", sheet, 1);
		try
		{
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{					
					try
					{						
						do
						{
							try
							{
								HBCBasicfeature.jsclick(hamburger, driver);
								Thread.sleep(2000);
								if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
								{
									flag = true;
									break;
								}
							}
							catch(Exception e)
							{
								continue;
							}
						}
						while(flag!=true);		
						if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
						{
							flag = false;
							HBCBasicfeature.scrolldown(pancakeStaticWelcome, driver);
							pancakeStaticWelcome.click();
							try
							{
								wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
								flag = true;
								break;
							}
							catch(Exception e)
							{
								flag = false;
								continue;
							}
						}
						else
						{
							continue;
						}
					}
					catch(Exception e)
					{
						driver.navigate().refresh();
						continue;
					}
				}
				while(flag!=true);	
				wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
				if(HBCBasicfeature.isElementPresent(emailField))
				{	
					emailField.sendKeys(email);								
					log.add("Entered Email ID is: "+ email);
					if(HBCBasicfeature.isElementPresent(pwdField))
					{	
						pwdField.sendKeys(pwd);									
						log.add("Entered Password is: "+ pwd);
						if(HBCBasicfeature.isElementPresent(signInButton))
						{
							signInButton.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.visibilityOf(myAccountPage));			
							if(HBCBasicfeature.isElementPresent(myAccountPage))
							{	
								Pass("MyAccount Page displayed",log);
								if(HBCBasicfeature.isElementPresent(myAccountPageAddressbook))
								{	
									
									Pass("My AddressBook section displayed");
									HBCBasicfeature.jsclick(myAccountPageAddressbook, driver);		
									Thread.sleep(500);
									wait.until(ExpectedConditions.visibilityOf(myAccountAddressBookpage));
									if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
									{
										Pass("My AddressBook page displayed");
										MyAddress = true;
									}
									else
									{
										Fail("My AddressBook page not dispalyed");
									}
								}
								else
								{
									Fail("My AddressBook section not displayed");
								}								
							}
							else
							{
								Fail("MyAccount page not displayed",log);
							}
						}
						else
						{
							Fail("SignIn button not displayed");
						}
					}
					else
					{
						Fail("Password Field not displayed");
					}
				}
				else
				{
					Fail("Email Field not displayed");
				}			
			}
			else
			{
				Fail("Hamburger menuu not displayed");
			}				
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-2114 Verify that "Address Book" page should be displayed as per the creative*/
	public void MyAddressBookpageCreative(String tc) 
	{
		if(tc == "HBC2114")
		{
			ChildCreation("HUDSONBAY-2114 Verify that 'Address Book' page should be displayed as per the creative");	
		}
		else
		{
			ChildCreation("HUDSONBAY-2072 Verify that 'Address Book' page should be displayed as per the creative");
		}			
		if(MyAddress == true)
		{				
			try
			{				
				if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
				{
					Pass("My AddressBook Page displayed");
					if(HBCBasicfeature.isElementPresent(myAccountPageAddressDropDown))
					{
						Pass("Address DropDown is displayed");
					}
					else
					{
						Fail("Address DropDown is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAccountPageAddressRemoveButton))
					{
						Pass("Address Remove Button is displayed");
					}
					else
					{
						Fail("Address Remove Button is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPageAddressAddNewButton))
					{
						Pass("Address AddNew Button is displayed");
					}
					else
					{
						Fail("Address AddNew Button is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageFname))
					{
						Pass("FirstName field is displayed");
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageLname))
					{
						Pass("LastName field is displayed");
					}
					else
					{
						Fail("LastName field is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myPageStAddres))
					{
						Pass("Street Address field 1 is displayed");
					}
					else
					{
						Fail("Street Address field 1 is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPage2StAddres))
					{
						Pass("Street Address field 2 is displayed");
					}
					else
					{
						Fail("Street Address field 2 is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageCity))
					{
						HBCBasicfeature.scrolldown(myPageCity, driver);
						Pass("City field is displayed");
					}
					else
					{
						Fail("City field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
					{
						HBCBasicfeature.scrolldown(myAddressBookPageCountry, driver);
						Pass("Country dropdown field is displayed");						
					}
					else
					{
						Fail("Country dropdown field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						HBCBasicfeature.scrolldown(myPageState, driver);
						Pass("State/province dropdown field is displayed");
					}
					else
					{
						Fail("State/province dropdown field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						HBCBasicfeature.scrolldown(myPageZipcode, driver);
						Pass("PostalCode field is displayed");
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPagePhoneField)&&HBCBasicfeature.isElementPresent(myPagePhoneExt))
					{
						HBCBasicfeature.scrolldown(myAddressBookPagePhoneField, driver);
						Pass("Phone field & extension field is displayed");
					}
					else
					{
						Fail("Phone field & extension field is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myAddressBookPageUpdate))
					{
						Pass("Update Button is displayed");
					}
					else
					{
						Fail("Update Button is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageEmail))
					{
						Pass("Email field is displayed");
					}
					else
					{
						Fail("Email field is not displayed");
					}
					/*if(HBCBasicfeature.isElementPresent(signupEmails))
					{
						Pass("SignUp for emails button is displayed");
					}
					else
					{
						Fail("SignUp for emails Button is not displayed");
					}*/
					if(HBCBasicfeature.isElementPresent(footerContainer))
					{
						Pass("Footer container is displayed");
					}
					else
					{
						Fail("Footer container is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(myAccountpageBack))
					{
						HBCBasicfeature.scrollup(myAccountpageBack, driver);
						Pass("'< Account'link is displayed");
					}
					else
					{
						Fail("'< Account'link is not displayed");
					}			
				}
				else
				{
					Fail("My AddressBook page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2114" +e.getMessage());
				Exception("HBC-2114 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2115 Verify that "< Account" link should be shown in the left side of the page and it should be highlighted in blue color as per the creative*/
	/*HUDSONBAY-2127 Verify that "<Account" link should be shown in the left corner of the "Address Detail" page*/
	public void MyAddressBookBackArrow(String tc) 
	{
		if(tc == "HBC2115")
		{
			ChildCreation("HUDSONBAY-2115 Verify that '< Account' link should be shown in the left side of the page and it should be highlighted in blue color as per the creative");
		}
		else
		{
			ChildCreation("HUDSONBAY-2073 Verify that '< Account' link should be shown in the left side of the page and it should be highlighted in blue color as per the creative");
		}
		if(MyAddress == true)
		{
			boolean HBC2127 = false;
			try
			{
				String expectedTxt = HBCBasicfeature.getExcelVal("HBC2005", sheet, 1);
				String expectedColor = HBCBasicfeature.getExcelVal("HBC2005", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					Pass("'< Account'link is displayed");	
					String actualTxt = myAccountpageBack.getText();
					String Color = myAccountpageBack.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(Color);
					log.add("The Actual text is: "+actualTxt);
					log.add("The Expected text is: "+expectedTxt);
					log.add("The Actual Color is: "+actualColor);
					log.add("The Expected Color is: "+expectedColor);					
					if(actualTxt.equalsIgnoreCase(expectedTxt)&&actualColor.equalsIgnoreCase(expectedColor))
					{
						Pass("'< Account' link shown in the left side of the page and it highlighted in blue color as per the creative",log);
						HBC2127 =true;
					}
					else
					{
						Fail("'< Account' link not shown in the left side of the page and not as per the creative",log);
					}				
				}
				else
				{
					Fail("'< Account' link not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2115" +e.getMessage());
				Exception("HBC-2115 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2127 Verify that '<Account' link should be shown in the left corner of the 'Address Detail' page");
			if(HBC2127==true)
			{
				Pass("'<Account' link shown in the left corner of the 'Address book' page");
			}
			else
			{
				Fail("'<Account' link not shown in the left corner of the 'Address book' page");
			}
			
			ChildCreation("HUDSONBAY-2085 Verify that '<Account' link should be shown in the left corner of the 'Address Detail' page");
			if(HBC2127==true)
			{
				Pass("'<Account' link shown in the left corner of the 'Address book' page");
			}
			else
			{
				Fail("'<Account' link not shown in the left corner of the 'Address book' page");
			}			
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2116 Verify that on tapping "< Account" link, it should navigates to "My Account" page*/
	public void MyAddressBookBackArrowClick() 
	{
		ChildCreation("HUDSONBAY-2116 Verify that on tapping '< Account' link, it should navigates to 'My Account' page");
		if(MyAddress == true)
		{			
			boolean HBC2074 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					HBCBasicfeature.jsclick(myAccountpageBack, driver);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileTxt))
					{
						Pass("on tapping '< Account' link, navigates to 'My Account' page");
						HBC2074 = true;
						MyAddress=false;				
					}
					else
					{
						Fail("on tapping '< Account' link, not navigates to 'My Account' page");
					}					
					if(HBCBasicfeature.isElementPresent(myAccountPageAddressbook))
					{						
						HBCBasicfeature.jsclick(myAccountPageAddressbook, driver);									
						wait.until(ExpectedConditions.visibilityOf(myAccountAddressBookpage));
						if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
						{
							Pass("Navigated back to My AddressBook page ");
							MyAddress = true;
						}
						else
						{
							Fail("My AddressBook page not dispalyed");
						}
					}
					else
					{
						Fail("My AddressBook section displayed");
					}
				}
				else
				{
					Fail("'< Account' link not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2116" +e.getMessage());
				Exception("HBC-2116 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2074 Verify that on tapping '< Account' link, it should navigates to 'My Account' page");
			if(HBC2074 == true)
			{
				Pass("on tapping '< Account' link, navigates to 'My Account' page");
			}
			else
			{
				Fail("on tapping '< Account' link, not navigates to 'My Account' page");
			}	
			
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2117 Verify that "AddNew" button should be shown in the right corner below the Address Book title*/
	public void MyAddressBookAddNewButton() 
	{
		ChildCreation("HUDSONBAY-2117 Verify that 'AddNew' button should be shown in the right corner below the Address Book title");
		if(MyAddress == true)
		{
			boolean HBC2075 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(myAddressBookPageAddressAddNewButton))
				{
					Pass("AddressBook AddNew Button is displayed");
					HBC2075 = true;
				}
				else
				{
					Fail("AddressBook AddNew Button is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2117" +e.getMessage());
				Exception("HBC-2117 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2075 Verify that 'AddNew' button should be shown in the right corner below the Address Book title");
			if(HBC2075 == true)
			{
				Pass("AddressBook AddNew Button is displayed");
			}
			else
			{
				Fail("AddressBook AddNew Button is not displayed");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2119 Verify that "Remove" option should be shown near each displayed address*/
	public void MyAddressBookRemoveButton() 
	{
		ChildCreation("HUDSONBAY-2119 Verify that 'Remove' option should be shown near each displayed address");
		if(MyAddress == true)
		{
			boolean HBC2077 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageAddressRemoveButton))
				{
					Pass("AddressBook Remove Button is displayed");
					HBC2077 = true;
				}
				else
				{
					Fail("AddressBook Remove Button is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2119" +e.getMessage());
				Exception("HBC-2119 There is something wrong. Please Check." + e.getMessage());
			}
			ChildCreation("HUDSONBAY-2077 Verify that 'Remove' option should be shown near each displayed address");
			if(HBC2077 == true)
			{
				Pass("AddressBook Remove Button is displayed");
			}
			else
			{
				Fail("AddressBook Remove Button is not displayed");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2130 Verify that on tapping the "New Address +" link "Add New Address" page/overlay should be shown*/
	public void MyAddressBookAddNewClick() 
	{
		ChildCreation("HUDSONBAY-2130 Verify that on tapping the 'New Address +' link 'Add New Address' page/overlay should be shown");
		if(MyAddress == true)
		{
			boolean HBC2088 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(myAddressBookPageAddressAddNewButton))
				{
					Pass("AddressBook AddNew Button is displayed");
					HBCBasicfeature.jsclick(myAddressBookPageAddressAddNewButton, driver);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(myAddressBookNewFname));
					if(HBCBasicfeature.isElementPresent(myAddressBookNewFname))
					{
						boolean fname = myAddressBookNewFname.getAttribute("value").isEmpty();
						if(fname==true)
						{
							Pass("on tapping the 'New Address +' link 'Add New Address' page/overlay shown");
							HBC2088 = true;
						}
						else
						{
							Fail("on tapping the 'New Address +' link 'Add New Address' page/overlay not shown");
						}
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
				}
				else
				{
					Fail("AddressBook AddNew Button is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2130" +e.getMessage());
				Exception("HBC-2130 There is something wrong. Please Check." + e.getMessage());
			}

			ChildCreation("HUDSONBAY-2130 Verify that on tapping the 'New Address +' link 'Add New Address' page/overlay should be shown");
			if(HBC2088 == true)
			{
				Pass("on tapping the 'New Address +' link 'Add New Address' page/overlay shown");
			}
			else
			{
				Fail("on tapping the 'New Address +' link 'Add New Address' page/overlay not shown");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2131 Verify that "Add New Address" page should be displayed as per the creative*/
	public void MyAddressBookAddNewAddressCreative(String tc) 
	{
		if(tc=="HBC2131")
		{
			ChildCreation("HUDSONBAY-2131 Verify that 'Add New Address' page should be displayed as per the creative");
		}
		else
		{
			ChildCreation("HUDSONBAY-2089 Verify that 'Add New Address' page should be displayed as per the creative");
		}
		if(MyAddress == true)
		{		
			try
			{				
				if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
				{
					Pass("My AddressBook Page displayed");
					if(HBCBasicfeature.isElementPresent(myAccountPageAddressDropDown))
					{
						Pass("Address DropDown is displayed");
					}
					else
					{
						Fail("Address DropDown is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAccountPageAddressRemoveButton))
					{
						Pass("Address Remove Button is displayed");
					}
					else
					{
						Fail("Address Remove Button is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPageAddressAddNewButton))
					{
						Pass("Address AddNew Button is displayed");
					}
					else
					{
						Fail("Address AddNew Button is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookNewFname))
					{
						Pass("FirstName field is displayed");
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookNewLname))
					{
						Pass("LastName field is displayed");
					}
					else
					{
						Fail("LastName field is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myAddressBookNewStAddres))
					{
						Pass("Street Address field 1 is displayed");
					}
					else
					{
						Fail("Street Address field 1 is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookNew2StAddres))
					{
						Pass("Street Address field 2 is displayed");
					}
					else
					{
						Fail("Street Address field 2 is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookNewCity))
					{
						HBCBasicfeature.scrolldown(myAddressBookNewCity, driver);
						Pass("City field is displayed");
					}
					else
					{
						Fail("City field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewCountry))
					{
						HBCBasicfeature.scrolldown(myAddressBookPageCountry, driver);
						Pass("Country dropdown field is displayed");						
					}
					else
					{
						Fail("Country dropdown field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookNewState))
					{
						HBCBasicfeature.scrolldown(myAddressBookNewState, driver);
						Pass("State/province dropdown field is displayed");
					}
					else
					{
						Fail("State/province dropdown field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookNewZipcode))
					{
						HBCBasicfeature.scrolldown(myAddressBookNewZipcode, driver);
						Pass("PostalCode field is displayed");
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewPhoneField)&&HBCBasicfeature.isElementPresent(myAddressBookPageNewPhoneExt))
					{
						HBCBasicfeature.scrolldown(myAddressBookPageNewPhoneField, driver);
						Pass("Phone field & extension field is displayed");
					}
					else
					{
						Fail("Phone field & extension field is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewCancel))
					{
						Pass("Cancel button is displayed");
					}
					else
					{
						Fail("Cancel Button is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewSubmit))
					{
						Pass("Submit button is displayed");
					}
					else
					{
						Fail("Submit Button is not displayed");
					}					
					/*if(HBCBasicfeature.isElementPresent(signupEmails))
					{
						Pass("SignUp for emails button is displayed");
					}
					else
					{
						Fail("SignUp for emails Button is not displayed");
					}*/
					if(HBCBasicfeature.isElementPresent(footerContainer))
					{
						Pass("Footer container is displayed");
					}
					else
					{
						Fail("Footer container is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(myAccountpageBack))
					{
						HBCBasicfeature.scrollup(myAccountpageBack, driver);
						Pass("'< Account'link is displayed");
					}
					else
					{
						Fail("'< Account'link is not displayed");
					}						
					HBCBasicfeature.jsclick(myAddressBookPageNewCancel, driver);
					Thread.sleep(1000);
				}
				else
				{
					Fail("My AddressBook page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2131" +e.getMessage());
				Exception("HBC-2131 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2134 Verify that alert should be shown when the user leaves any field without filling the details*/
	/*HUDSONBAY-2136 Verify that corresponding alert message needs to be shown on navigating through the tabs or on tappping the "Submit" button*/	
	public void MyAddressBookEmptyValidation() 
	{
		ChildCreation("HUDSONBAY-2134 Verify that alert should be shown when the user leaves any field without filling the details");
		if(MyAddress == true)
		{
			boolean HBC2136 = false;
			try
			{
				String[] expectedAlert = HBCBasicfeature.getExcelVal("HBC1976", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
				{
					Pass("MyProfile Page displayed");
					if(HBCBasicfeature.isElementPresent(myPageFname))
					{
						Pass("FirstName field is displayed");
						myPageFname.clear();
						myPageLname.click();
						wait.until(ExpectedConditions.visibilityOf(myPageFnameAlert));
						if(HBCBasicfeature.isElementPresent(myPageFnameAlert))
						{
							String actualAlert = myPageFnameAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[0]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[0]))
							{
								Pass("Alert shown when the user leaves Firstname field without filling the details",log);
								HBC2136 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Firstname field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when FirstName field is empty ");
						}						
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageLname))
					{
						Pass("LastName field is displayed");
						myPageLname.clear();
						myPageFname.click();
						wait.until(ExpectedConditions.visibilityOf(myPageLnameAlert));
						if(HBCBasicfeature.isElementPresent(myPageLnameAlert))
						{
							String actualAlert = myPageLnameAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[1]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[1]))
							{
								Pass("Alert shown when the user leaves Lastname field without filling the details",log);
								HBC2136 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Lastname field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Lastname field is empty ");
						}						
					}
					else
					{
						Fail("LastName field is not displayed");
					}			
					
					if(HBCBasicfeature.isElementPresent(myPageStAddres))
					{
						Pass("Street Address field 1 is displayed");
						myPageStAddres.clear();
						myPage2StAddres.click();
						wait.until(ExpectedConditions.visibilityOf(myPageStAddressAlert));
						if(HBCBasicfeature.isElementPresent(myPageStAddressAlert))
						{
							String actualAlert = myPageStAddressAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[4]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[4]))
							{
								Pass("Alert shown when the user leaves Street Address field without filling the details",log);
								HBC2136 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Street Address field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Street Address field is empty ");
						}						
					}
					else
					{
						Fail("Street Address field 1 is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myPageCity))
					{
						HBCBasicfeature.scrolldown(myPageCity, driver);
						Pass("City field is displayed");
						myPageCity.clear();
						myPageStAddres.click();
						wait.until(ExpectedConditions.visibilityOf(myPageStCityAlert));
						if(HBCBasicfeature.isElementPresent(myPageStCityAlert))
						{
							String actualAlert = myPageStCityAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[5]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[5]))
							{
								Pass("Alert shown when the user leaves City field without filling the details",log);
								HBC2136 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves City field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when City field is empty ");
						}						
					}
					else
					{
						Fail("City field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						HBCBasicfeature.scrolldown(myPageZipcode, driver);
						Pass("PostalCode field is displayed");
						myPageZipcode.clear();
						myPageCity.click();
						wait.until(ExpectedConditions.visibilityOf(myPageZipAlert));
						if(HBCBasicfeature.isElementPresent(myPageZipAlert))
						{
							String actualAlert = myPageZipAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[6]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[6]))
							{
								Pass("Alert shown when the user leaves PostalCode field without filling the details",log);
								HBC2136 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves PostalCode field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when PostalCode field is empty ");
						}						
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPagePhoneField))
					{
						HBCBasicfeature.scrolldown(myAddressBookPagePhoneField, driver);
						Pass("Phone field is displayed");
						myAddressBookPagePhoneField.clear();
						myPageZipcode.click();
						wait.until(ExpectedConditions.visibilityOf(myPagePhoneAlert));
						if(HBCBasicfeature.isElementPresent(myPagePhoneAlert))
						{
							String actualAlert = myPagePhoneAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[7]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[7]))
							{
								Pass("Alert shown when the user leaves Phone field without filling the details",log);
								HBC2136 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Phone field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Phone field is empty ");
						}						
					}
					else
					{
						Fail("Phone field is not displayed");
					}				
				}
				else
				{
					Fail("My AddressBook page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2134" +e.getMessage());
				Exception("HBC-2134 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2136 Verify that corresponding alert message needs to be shown on navigating through the tabs or on tappping the 'Submit' button");
			if(HBC2136 == true)
			{
				Pass("corresponding alert message shown on navigating through the tabs or on tappping the 'Submit' button");
			}
			else
			{
				Fail("corresponding alert message not shown on navigating through the tabs or on tappping the 'Submit' button");
			}
			
			ChildCreation("HUDSONBAY-2092 Verify that corresponding alert message needs to be shown on navigating through the tabs or on tappping the 'update' button");
			if(HBC2136 == true)
			{
				Pass("corresponding alert message shown on navigating through the tabs or on tappping the 'update' button");
			}
			else
			{
				Fail("corresponding alert message not shown on navigating through the tabs or on tappping the 'update' button");
			}
			
			ChildCreation("HUDSONBAY-2094 Verify that corresponding alert message needs to be shown on navigating through the tabs or on tappping the 'Submit' button");
			if(HBC2136 == true)
			{
				Pass("corresponding alert message shown on navigating through the tabs or on tappping the 'Submit' button");
			}
			else
			{
				Fail("corresponding alert message not shown on navigating through the tabs or on tappping the 'Submit' button");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2135 Verify that user should be able to enter details in all the input fields.*/
	public void MyAddressBookEnterDetails(String tc) 
	{
		if(tc == "HBC2135")
		{
			ChildCreation("HUDSONBAY-2135 Verify that user should be able to enter details in all the input fields.");			
		}
		else
		{
			ChildCreation("HUDSONBAY-2093 Verify that user should be able to enter details in all the input fields.");			
		}
		if(MyAddress == true)
		{		
			try
			{	
				String fname = HBCBasicfeature.getExcelVal("HBC2135", sheet, 1);
				String lname = HBCBasicfeature.getExcelVal("HBC2135", sheet, 2);
				String stAddr = HBCBasicfeature.getExcelVal("HBC2135", sheet, 3);
				String city = HBCBasicfeature.getExcelVal("HBC2135", sheet, 4);
				String zip = HBCBasicfeature.getExcelVal("HBC2135", sheet, 6);
				String phone = HBCBasicfeature.getExcelNumericVal("HBC2135", sheet, 7);
				if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
				{
					Pass("My AddressBook Page displayed");					
					if(HBCBasicfeature.isElementPresent(myPageFname))
					{
						Pass("FirstName field is displayed");
						myPageFname.clear();
						myPageFname.sendKeys(fname);
						Thread.sleep(200);
						if(myPageFname.getAttribute("value").isEmpty())
						{
							Fail("First name field is empty");
						}
						else if(myPageFname.getAttribute("value").equalsIgnoreCase(fname))
						{
							Pass("User able to enter details in First name field ");
						}
						else
						{
							Fail("User able to enter details but not matched");
						}						
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageLname))
					{
						Pass("Last Name field is displayed");
						myPageLname.clear();
						myPageLname.sendKeys(lname);
						Thread.sleep(200);
						if(myPageLname.getAttribute("value").isEmpty())
						{
							Fail("Last name field is empty");
						}
						else if(myPageLname.getAttribute("value").equalsIgnoreCase(lname))
						{
							Pass("User able to enter details in Last name field ");
						}
						else
						{
							Fail("User able to enter details but not matched");
						}						
					}
					else
					{
						Fail("LastName field is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myPageStAddres))
					{
						Pass("Street Address field 1 is displayed");
						myPageStAddres.clear();
						myPageStAddres.sendKeys(stAddr);
						Thread.sleep(200);
						if(myPageStAddres.getAttribute("value").isEmpty())
						{
							Fail("Street Address field is empty");
						}
						else if(myPageStAddres.getAttribute("value").equalsIgnoreCase(stAddr))
						{
							Pass("User able to enter details in Street Address field");
						}
						else
						{
							Fail("User able to enter details but not matched");
						}						
					}
					else
					{
						Fail("Street Address field 1 is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPage2StAddres))
					{
						Pass("Street Address field 2 is displayed");
						myPage2StAddres.clear();
						myPage2StAddres.sendKeys(stAddr);
						Thread.sleep(200);
						if(myPage2StAddres.getAttribute("value").isEmpty())
						{
							Fail("Street Address 2 field is empty");
						}
						else if(myPage2StAddres.getAttribute("value").equalsIgnoreCase(stAddr))
						{
							Pass("User able to enter details in Street Address 2 field");
						}
						else
						{
							Fail("User able to enter details but not matched");
						}						
					}
					else
					{
						Fail("Street Address field 2 is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageCity))
					{
						HBCBasicfeature.scrolldown(myPageCity, driver);
						Pass("City field is displayed");
						myPageCity.clear();
						myPageCity.sendKeys(city);
						Thread.sleep(200);
						if(myPageCity.getAttribute("value").isEmpty())
						{
							Fail("City field is empty");
						}
						else if(myPageCity.getAttribute("value").equalsIgnoreCase(city))
						{
							Pass("User able to enter details in City field ");
						}
						else
						{
							Fail("User able to enter details but not matched");
						}					
					}
					else
					{
						Fail("City field is not displayed");
					}
					
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						HBCBasicfeature.scrolldown(myPageZipcode, driver);
						Pass("PostalCode field is displayed");
						myPageZipcode.clear();
						myPageZipcode.sendKeys(zip);
						Thread.sleep(200);
						if(myPageZipcode.getAttribute("value").isEmpty())
						{
							Fail("PostalCode field is empty");
						}
						else if(myPageZipcode.getAttribute("value").equalsIgnoreCase(zip))
						{
							Pass("User able to enter details in PostalCode field ");
						}
						else
						{
							Fail("User able to enter details but not matched");
						}					
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPagePhoneField))
					{
						HBCBasicfeature.scrolldown(myAddressBookPagePhoneField, driver);
						Pass("Phone field is displayed");
						myAddressBookPagePhoneField.clear();
						myAddressBookPagePhoneField.sendKeys(phone);
						Thread.sleep(200);
						if(myAddressBookPagePhoneField.getAttribute("value").isEmpty())
						{
							Fail("Phone field is empty");
						}
						else if(myAddressBookPagePhoneField.getAttribute("value").replaceAll("-", "").equalsIgnoreCase(phone))
						{
							Pass("User able to enter details in Phone field ");
						}
						else
						{
							Fail("User able to enter details but not matched");
						}					
					}
					else
					{
						Fail("Phone field is not displayed");
					}					
				}
				else
				{
					Fail("My AddressBook page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2135" +e.getMessage());
				Exception("HBC-2135 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
		
	/*HUDSONBAY-2137 Verify that only alphabets should be accepted in "First Name" and "Last Name" field*/
	public void MyAddressBookFirstLastNameCharValidation() 
	{
		ChildCreation("HUDSONBAY-2137 Verify that only alphabets should be accepted in 'First Name' and 'Last Name' field");
		if(MyAddress == true)
		{
			boolean HBC2095 = false;
			try
			{
				String fname = HBCBasicfeature.getExcelVal("HBC1978", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageFname))
				{
					Pass("FirstName field is displayed");
					myPageFname.clear();
					log.add( "The Entered value from the excel file is : " + fname);
					myPageFname.sendKeys(fname);
					if(myPageFname.getAttribute("value").isEmpty())
					{
						Fail("The First Name is empty");
					}
					else if(myPageFname.getAttribute("value").equalsIgnoreCase(fname))
					{
						Pass("Only alphabets accepted in 'First Name' field",log);
						HBC2095 = true;
					}
					else
					{
						Fail("Alphabets not accepted in 'First Name' field",log);
					}
					myPageFname.clear();
				}
				else
				{
					Fail("FirstName field is not displayed");
				}
				HBC2095 = false;
				String lname = HBCBasicfeature.getExcelVal("HBC1978", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myPageLname))
				{
					Pass("Last Name field is displayed");
					myPageLname.clear();
					log.add( "The Entered value from the excel file is : " + lname);
					myPageLname.sendKeys(lname);
					if(myPageLname.getAttribute("value").isEmpty())
					{
						Fail("The Last Name field is empty");
					}
					else if(myPageLname.getAttribute("value").equalsIgnoreCase(lname))
					{
						Pass("Only alphabets accepted in 'Last Name' field",log);
						HBC2095 = true;
					}
					else
					{
						Fail("Alphabets not accepted in 'Last Name' field",log);
					}
					myPageLname.clear();
				}
				else
				{
					Fail("Last Name field is not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2137" +e.getMessage());
				Exception("HBC-2137 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2095 Verify that only alphabets should be accepted in 'First Name' and 'Last Name' field");
			if(HBC2095 == true)
			{
				Pass("only alphabets accepted in 'First Name' and 'Last Name' field");
			}
			else
			{
				Fail("only alphabets not accepted in 'First Name' and 'Last Name' field");
			}
			
		}
		else
		{
			Skip("My Address Book Page not displayed");
		}
	}
		
	/*HUDSONBAY-2138 Verify that "First Name" and "Last Name" field should accept numbers and special characters and the corresponding alert message should be shown*/
	public void MyAddressBookFirstLastNameValidationAlert() 
	{
		ChildCreation("HUDSONBAY-2138 Verify that 'First Name' and 'Last Name' field should accept numbers and special characters and the corresponding alert message should be shown");
		if(MyAddress == true)
		{
			boolean HBC2096 = false;
			try
			{
				String[] expectedAlert = HBCBasicfeature.getExcelVal("HBC1979", sheet, 2).split("\n");
				
				String fname = HBCBasicfeature.getExcelVal("HBC1979", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageFname))
				{
					Pass("FirstName field is displayed");
					myPageFname.clear();
					log.add( "The Entered value from the excel file is : " + fname);
					myPageFname.sendKeys(fname);
					if(myPageFname.getAttribute("value").isEmpty())
					{
						Fail("The First Name field is empty and not accept numbers and special characters",log);
					}
					else 
					{
						Pass("Accepts numbers and special characters in 'First Name' field",log);
						myPageLname.click();
						wait.until(ExpectedConditions.visibilityOf(myPageFnameAlert));
						if(HBCBasicfeature.isElementPresent(myPageFnameAlert))
						{
							String actualAlert = myPageFnameAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[0]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[0]))
							{
								Pass("Alert shown when numbers and special characters are entered in Firstname",log);
								HBC2096 = true;
							}
							else
							{
								Fail("Alert not shown when numbers and special characters are entered in Firstname",log);
							}
						}
						else
						{
							Fail("Alert not displayed");
						}					
					}
				}
				else
				{
					Fail("First Name field is not displayed");
				}
				HBC2096 = false;
				String lname = HBCBasicfeature.getExcelVal("HBC1979", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageLname))
				{
					Pass("Last Name field is displayed");
					myPageLname.clear();
					log.add( "The Entered value from the excel file is : " + lname);
					myPageLname.sendKeys(lname);
					if(myPageLname.getAttribute("value").isEmpty())
					{
						Fail("The Last Name field is empty and not accept numbers and special characters",log);
					}
					else 
					{
						Pass("Accepts numbers and special characters in 'Last Name' field",log);
						myPageFname.click();
						wait.until(ExpectedConditions.visibilityOf(myPageLnameAlert));
						if(HBCBasicfeature.isElementPresent(myPageLnameAlert))
						{
							String actualAlert = myPageLnameAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[1]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[1]))
							{
								Pass("Alert shown when numbers and special characters are entered in Last name",log);
								HBC2096 = true;
							}
							else
							{
								Fail("Alert not shown when numbers and special characters are entered in Last name",log);
							}
						}
						else
						{
							Fail("Alert not displayed");
						}					
					}
				}
				else
				{
					Fail("Last Name field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2138" +e.getMessage());
				Exception("HBC-2138 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2096 Verify that 'First Name' and 'Last Name' field should accept numbers and special characters and the corresponding alert message should be shown");
			if(HBC2096 == true)
			{
				Pass("Alert shown when numbers and special characters are entered in Firstname & Last name");
			}
			else
			{
				Fail("Alert not shown when numbers and special characters are entered in Firstname & Last name");
			}
			
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
			
	/*HUDSONBAY-2139 Verify that user should be able to enter street address in the "Street Address" field and it should display two text boxes.*/
	public void MyAddressBookStreetAddressFields() 
	{
		ChildCreation("HUDSONBAY-2139 Verify that user should be able to enter street address in the 'Street Address' field and it should display two text boxes.");
		if(MyAddress == true)
		{
			boolean HBC2097 = false;
			try
			{
				String addr = HBCBasicfeature.getExcelVal("HBC1988", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageStAddres)&&HBCBasicfeature.isElementPresent(myPage2StAddres))
				{
					Pass(" 'Street Address' field two text boxes are displayed");
					myPageStAddres.clear();
					log.add( "The Entered value from the excel file is : " + addr.toString());
					myPageStAddres.sendKeys(addr);
					if(myPageStAddres.getAttribute("value").isEmpty())
					{
						Fail( "The 'Street Address' field is empty.");
					}
					else if(myPageStAddres.getAttribute("value").equalsIgnoreCase(addr))
					{
						Pass( "The 'Street Address'  field was enterable",log);
						HBC2097 = true;
					}
					else
					{
						Fail( "The 'Street Address field was enterable but enetered characters are not displayed.",log);
					}
					myPageStAddres.clear();
					HBC2097 = false;
					myPage2StAddres.clear();
					log.add( "The Entered value from the excel file is : " + addr.toString());
					myPage2StAddres.sendKeys(addr);
					if(myPage2StAddres.getAttribute("value").isEmpty())
					{
						Fail( "The 'Street Address' field 2 is empty.");
					}
					else if(myPage2StAddres.getAttribute("value").equalsIgnoreCase(addr))
					{
						Pass( "The 'Street Address'  field 2 was enterable",log);
						HBC2097 = true;
					}
					else
					{
						Fail( "The 'Street Address field 2 was enterable but enetered characters are not displayed.",log);
					}
					myPage2StAddres.clear();					
				}
				else
				{
					Fail("'Street Address' field two text boxes are not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2139" +e.getMessage());
				Exception("HBC-2139 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2097 Verify that user should be able to enter street address in the 'Street Address' field and it should display two text boxes.");
			if(HBC2097 == true)
			{
				Pass( "Able to enter street address in the 'Street Address' field and it display two text boxes");
			}
			else
			{
				Fail( "Not able to enter street address in the 'Street Address' field ");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2140 Verify that user should be able to enter characters in the "City" field*/
	public void MyAddressBookCityField() 
	{
		ChildCreation("HUDSONBAY-2140 Verify that user should be able to enter characters in the 'City' field");
		if(MyAddress == true)
		{
			boolean HBC2098 = false;
			try
			{
				String city = HBCBasicfeature.getExcelVal("HBC1989", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageCity))
				{
					HBCBasicfeature.scrolldown(myPageCity, driver);
					Pass("City field is displayed");
					myPageCity.clear();
					log.add( "The Entered value from the excel file is : " + city.toString());
					myPageCity.sendKeys(city);
					if(myPageCity.getAttribute("value").isEmpty())
					{
						Fail( "The 'City' field is empty.");
					}
					else if(myPageCity.getAttribute("value").equalsIgnoreCase(city))
					{
						Pass( "The 'City'  field was enterable",log);
						HBC2098 = true;
					}
					else
					{
						Fail( "The 'City' field was enterable but enetered characters are not displayed.",log);
					}
					myPageCity.clear();					
				}
				else
				{
					Fail("City field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2140" +e.getMessage());
				Exception("HBC-2140 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2098 Verify that user should be able to enter characters in the 'City' field");
			if(HBC2098 == true)
			{
				Pass( "The 'City' field was able to enter characters");
			}
			else
			{
				Fail( "The 'City' field was not able to enter characters ");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2141 Verify that "City" field should accept numbers and special characters and the corresponding alert message should be shown*/
	public void MyAddressBookCityFieldAlert() 
	{
		ChildCreation("HUDSONBAY-2141 Verify that 'City' field should accept numbers and special characters and the corresponding alert message should be shown");
		if(MyAddress == true)
		{
			boolean HBC2099 = false;
			try
			{
				String excelVal = HBCBasicfeature.getExcelVal("HBC1990", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1990", sheet, 2);

				if(HBCBasicfeature.isElementPresent(myPageCity))
				{
					Pass("City field is displayed");
					myPageCity.clear();
					log.add( "The Entered value from the excel file is : " + excelVal.toString());
					myPageCity.sendKeys(excelVal);
					myPage2StAddres.click();
					wait.until(ExpectedConditions.attributeContains(myPageStCityAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(myPageStCityAlert))
					{
						String actualAlert = myPageStCityAlert.getText();
						log.add("Actual text is: "+actualAlert);
						log.add("Expected text is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("'City' field accept numbers and special characters and the corresponding alert message shown",log);
							 HBC2099 = true;
						}
						else
						{
							Fail("'City' field accept numbers and special characters and the corresponding alert message not shown",log);
						}
						myPageCity.clear();
					}
					else
					{
						Fail("City field alert is not displayed");
					}					
				}
				else
				{
					Fail("City field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2141" +e.getMessage());
				Exception("HBC-2141 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2099 Verify that 'City' field should accept numbers and special characters and the corresponding alert message should be shown");
			if( HBC2099 == true)
			{
				Pass("'City' field accept numbers and special characters and the corresponding alert message shown");
			}
			else
			{
				Fail("'City' field accept numbers and special characters and the corresponding alert message not shown");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2142 Verify that alert should be shown when the user enters more than 35 characters in the "City" field*/
	public void MyAddressBookCityFieldExceedLimitAlert() 
	{
		ChildCreation("HUDSONBAY-2142 Verify that alert should be shown when the user enters more than 35 characters in the 'City' field");
		if(MyAddress == true)
		{
			boolean HBC2100 = false;
			try
			{
				String excelVal = HBCBasicfeature.getExcelVal("HBC1990", sheet, 3);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1990", sheet, 4);

				if(HBCBasicfeature.isElementPresent(myPageCity))
				{
					Pass("City field is displayed");
					myPageCity.clear();
					log.add( "The Entered value from the excel file is : " + excelVal.toString());
					myPageCity.sendKeys(excelVal);
					myPage2StAddres.click();
					wait.until(ExpectedConditions.attributeContains(myPageStCityAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(myPageStCityAlert))
					{
						String actualAlert = myPageStCityAlert.getText();
						log.add("Actual text is: "+actualAlert);
						log.add("Expected text is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("Alert shown when the user enters more than 35 characters in the 'City' field",log);
							HBC2100 = true;
						}
						else
						{
							Fail("Alert not shown when the user enters more than 35 characters in the 'City' field",log);
						}
						myPageCity.clear();
					}
					else
					{
						Fail("City field alert is not displayed");
					}					
				}
				else
				{
					Fail("City field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2142" +e.getMessage());
				Exception("HBC-2142 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2100 Verify that alert should be shown when the user enters more than 35 characters in the 'City' field");
			if(HBC2100 == true)
			{
				Pass("Alert shown when the user enters more than 35 characters in the 'City' field");
			}
			else
			{
				Fail("Alert not shown when the user enters more than 35 characters in the 'City' field");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2143 Verify that "Country" field should have a maximum of 3 dropdown options.*/
	public void MyAddressBookCountryFields() 
	{
		ChildCreation("HUDSONBAY-2143 Verify that 'Country' field should have a maximum of 3 dropdown options.");
		if(MyAddress == true)
		{
			boolean HBC2101 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(myAddressBookPageCountry);
					int cntysize = sel.getOptions().size();
					for( int i = 0; i<cntysize;i++)
					{
						String cnty = sel.getOptions().get(i).getText();
						log.add( "The Displayed country option is  : " + cnty);
					}
					if(cntysize==3)
					{
						Pass( "The Country Default Options is of 3 Sizes.",log);
						HBC2101 = true;
					}
					else
					{
						Fail( "The Country Default Options is of not the expected 3.The displayed total country sizes are : " + cntysize, log);
					}
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2143" +e.getMessage());
				Exception("HBC-2143 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2101 Verify that 'Country' field should have a maximum of 3 dropdown options.");
			if(HBC2101 == true)
			{
				Pass( "The Country Default Options is of 3 Sizes.");				
			}
			else
			{
				Fail( "The Country Default Options is of not the expected 3.The displayed total country sizes are : ");
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-2144 Verify that "Province" dropdown field should be shown, when the country is selected in Canada.*/
	public void MyAddressBookProvinceField() 
	{
		ChildCreation("HUDSONBAY-2144 Verify that 'Province' dropdown field should be shown, when the country is selected in Canada.");
		if(MyAddress == true)
		{
			boolean HBC2102 = false;
			try
			{
				String country = "";
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						Pass("'Province' dropdown field shown, when the country is selected in Canada");
						HBC2102 = true;
					}
					else
					{
						Fail("'Province' dropdown field not shown, when the country is selected in Canada");
					}
				}
				else
				{
					Fail("country not selected is Canada");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2144" +e.getMessage());
				Exception("HBC-2144 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2102 Verify that 'Province' dropdown field should be shown, when the country is selected in Canada.");
			if(HBC2102 == true)
			{
				Pass("'Province' dropdown field shown, when the country is selected in Canada");
			}
			else
			{
				Fail("'Province' dropdown field not shown, when the country is selected in Canada");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2146 Verify that "Select" option should be selected by default in the "Providence" dropdown field.*/
	public void MyAddressBookProvinceFieldSelectDefault() 
	{
		ChildCreation("HUDSONBAY-2146 Verify that 'Select' option should be selected by default in the 'Province' dropdown field");
		if(MyAddress == true)
		{
			boolean HBC2103 = false;
			try
			{
				String country = "";
				String expVal = HBCBasicfeature.getExcelVal("HBC1993", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						Pass("'Province' dropdown field displayed");
						Select sel = new Select(myPageState);
						sel.getOptions().get(0).click();
						String actualVal= sel.getFirstSelectedOption().getText();
						log.add( "The Expected Value was : " + expVal);
						log.add( "The Actual Value is : " + actualVal);
						if(expVal.equalsIgnoreCase(actualVal))
						{
							Pass( "The Expected Default Value is selected.",log);
							HBC2103 = true;
						}
						else
						{
							Fail( "The Expected Default Value is not selected.",log);
						}						
					}
					else
					{
						Fail("'Province' dropdown field not shown, when the country is selected in Canada");
					}
				}
				else
				{
					Fail("country not selected is Canada");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2146" +e.getMessage());
				Exception("HBC-2146 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2103 Verify that 'Select' option should be selected by default in the 'Province' dropdown field");
			if(HBC2103 == true)
			{
				Pass(" 'Select' option selected by default in the 'Province' dropdown field");
			}
			else
			{
				Fail(" 'Select' option not selected by default in the 'Province' dropdown field");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2147 Verify that "Providence" dropdown field should be changed to textbox field when the country is selected in "United Kingdom"*/
	public void  MyAddressBookProvincetextBoxUK() 
	{
		ChildCreation("HUDSONBAY-2147 Verify that 'Providence' dropdown field should be changed to textbox field when the country is selected in 'United Kingdom'");
		if(MyAddress == true)
		{
			boolean HBC2104 = false;
			try
			{
				String country = "";
				String expVal = HBCBasicfeature.getExcelVal("HBC1994", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("United Kingdom");
					if(cnty==false)
					{
						sel.selectByValue("GB");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("United Kingdom"))
				{
					Pass("country selected is United Kingdom");
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						Pass("'Province' textbox field displayed");
						myPageState.clear();
						log.add( "The Entered value from the excel file is : " + expVal.toString());
						myPageState.sendKeys(expVal);
						Thread.sleep(500);
						if(myPageState.getAttribute("value").isEmpty())
						{
							Fail(" 'Providence' textbox field is empty",log);
						}						
						else if(myPageState.getAttribute("value").equalsIgnoreCase(expVal))
						{
							Pass( "'Providence' dropdown field changed to textbox field when the country is selected in 'United Kingdom",log);
							HBC2104 = true;
						}
						else
						{
							Fail( "'Providence' dropdown field not changed to textbox field",log);
						}						
					}
					else
					{
						Fail("'Province' dropdown field not shown, when the country is selected in Canada");
					}
				}
				else
				{
					Fail("country not selected is Canada");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2147" +e.getMessage());
				Exception("HBC-2147 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2104 Verify that 'Providence' dropdown field should be changed to textbox field when the country is selected in 'United Kingdom'");
			if(HBC2104 == true)
			{
				Pass( "'Providence' dropdown field changed to textbox field when the country is selected in 'United Kingdom");
			}
			else
			{
				Fail( "'Providence' dropdown field not changed to textbox field");
			}	
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2148 Verify that user should be able to select state from the "Providence" drop down, when the country is selected in "United States".*/
	public void MyAddressBookProvinceDropDownUS() 
	{
		ChildCreation("HUDSONBAY-2148 Verify that user should be able to select state from the 'Providence' drop down, when the country is selected in 'United States'");
		if(MyAddress == true)
		{
			boolean HBC2105 = false;
			try
			{
				String country = "";
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("United States");
					if(cnty==false)
					{
						sel.selectByValue("US");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("United States"))
				{
					Pass("country selected is United States");
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						Pass("'Province' dropdown field displayed");
						Random r  = new Random();
						Select sel =  new Select(myPageState);
						int size = sel.getOptions().size();
						int i = r.nextInt(size);
						String state =  sel.getOptions().get(i).getText();
						sel.getOptions().get(i).click();
						log.add("selected state is: "+state);
						log.add("Displayed state name is: "+sel.getFirstSelectedOption().getText());
						Thread.sleep(200);
						if(state.equalsIgnoreCase(sel.getFirstSelectedOption().getText()))
						{
							Pass(" User able to select state from the 'Providence' drop down, when the country is selected in 'United States'",log);
							 HBC2105 = true;
						}
						else
						{
							Fail("User not able to select state from the 'Providence' drop down, when the country is selected in 'United States'",log);
						}
					}
					else
					{
						Fail("'Province' dropdown field not displayed");
					}
				}
				else
				{
					Fail("country selected is not United States");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-2148" +e.getMessage());
				Exception("HBC-2148 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2105 Verify that user should be able to select state from the 'Providence' drop down, when the country is selected in 'United States'");
			if(HBC2105 == true)
			{
				Pass(" User able to select state from the 'Providence' drop down, when the country is selected in 'United States'");
			}
			else
			{
				Fail("User not able to select state from the 'Providence' drop down, when the country is selected in 'United States'");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2149 Verify that user should be able to enter zip code details in the "Zip Code" field, when the country is selected in "United States".*/
	public void MyAddressBookZipCodeUS() 
	{
		ChildCreation("HUDSONBAY-2149 Verify that user should be able to enter zip code details in the 'Zip Code' field, when the country is selected in 'United States'");
		if(MyAddress == true)
		{
			boolean HBC2106 = false;
			try
			{
				String country = "";
				String zipCode  = HBCBasicfeature.getExcelNumericVal("HBC1996", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("United States");
					if(cnty==false)
					{
						sel.selectByValue("US");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("United States"))
				{
					Pass("country selected is United States");
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						Pass("'Zipcode' field displayed");
						myPageZipcode.clear();
						log.add( "The Entered value from the excel file is : " + zipCode.toString());
						myPageZipcode.sendKeys(zipCode);
						Thread.sleep(500);
						if(myPageZipcode.getAttribute("value").isEmpty())
						{
							Fail(" 'Zip Code' field is empty",log);
						}						
						else if(myPageZipcode.getAttribute("value").equalsIgnoreCase(zipCode))
						{
							Pass( "User able to enter zip code details in the 'Zip Code' field, when the country is selected in 'United States'",log);
							HBC2106 = true;
						}
						else
						{
							Fail( "User able to enter zip code details but not matched",log);
						}						
					}
					else
					{
						Fail("'Zipcode' field not displayed");
					}
				}
				else
				{
					Fail("country selected is not United States");
				}				
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2149" +e.getMessage());
				Exception("HBC-2149 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2106 Verify that user should be able to enter zip code details in the 'Zip Code' field, when the country is selected in 'United States'");
			if(HBC2106 == true)
			{
				Pass( "User able to enter zip code details in the 'Zip Code' field, when the country is selected in 'United States'");
			}
			else
			{
				Fail( "User able to enter zip code details but not matched");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2150 Verify that user should be able to enter maximum of 5 numbers in the "Zip Code" field*/
	/*HUDSONBAY-2151 Verify that on changing the counrty from canada to united states, the prefilled 6 digits postal code number should not be maintained*/
	public void MyAddressBookZipCodeValidation() 
	{
		ChildCreation("HUDSONBAY-2150 Verify that user should be able to enter maximum of 5 numbers in the 'Zip Code' field");
		if(MyAddress == true)
		{
			boolean HBC2151 = false;
			try
			{
				String country = "";
				String[] cellVal  = HBCBasicfeature.getExcelVal("HBC1997", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("United States");
					if(cnty==false)
					{
						sel.selectByValue("US");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(country.contains("United States"))
				{
					Pass( "country selected is United States");
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						Pass("'Zipcode' field displayed");
						for(int i = 0; i<cellVal.length;i++)
						{
							myPageZipcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							myPageZipcode.sendKeys(cellVal[i]);
							if(myPageZipcode.getAttribute("value").isEmpty())
							{
								Fail( "The Zip Code field is empty.");
							}
							else if(myPageZipcode.getAttribute("value").length()<=5)
							{
								Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
								HBC2151 = true;
							}
							else
							{
								Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
							}
							myPageZipcode.clear();
						}
					}
					else
					{
						Fail( "The Zip Code Area is not displayed.");
					}
				}
				else
				{
					Fail( "country selected is not United States.");
				}			
			}
			catch (Exception e)
			{
				System.out.println("HBC-2150" +e.getMessage());
				Exception("HBC-2150 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HBC- 2151 Verify that on changing the counrty from canada to united states, the prefilled 6 digits postal code number should not be maintained");
			if(HBC2151 ==true)
			{
				Pass("on changing the counrty from canada to united states, the prefilled 6 digits postal code number not maintained");
			}
			else
			{
				Fail("on changing the counrty from canada to united states, the prefilled 6 digits postal code number maintained");
			}	
			
			ChildCreation("HBC- 2107 Verify that user should be able to enter maximum of 5 numbers in the 'Zip Code' field");
			if(HBC2151 ==true)
			{
				Pass("User able to enter maximum of 5 numbers in the 'Zip Code' field");
			}
			else
			{
				Fail("User not able to enter maximum of 5 numbers in the 'Zip Code' field");
			}		
			
			ChildCreation(" HBC-2108 Verify that on changing the counrty from canada to united states, the prefilled 6 digits postal code number should not be maintained");
			if(HBC2151 ==true)
			{
				Pass("user able to enter maximum of 5 numbers in the 'Zip Code' field");
			}
			else
			{
				Fail("user not able to enter maximum of 5 numbers in the 'Zip Code' field");
			}			
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2152 Verify that "Zip Code" field should accept numbers and special characters and the corresponding alert message should be shown*/
	public void MyAddressZipCodeFieldAlert() 
	{
		ChildCreation("HUDSONBAY-2152 Verify that 'Zip Code' field should accept numbers and special characters and the corresponding alert message should be shown");
		if(MyAddress == true)
		{
			boolean HBC2109 = false;
			try
			{
				String excelVal = HBCBasicfeature.getExcelVal("HBC1998", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1998", sheet, 2);

				if(HBCBasicfeature.isElementPresent(myPageZipcode))
				{
					Pass("Zip Code field is displayed");
					myPageZipcode.clear();
					log.add( "The Entered value from the excel file is : " + excelVal.toString());
					myPageZipcode.sendKeys(excelVal);
					myAddressBookPagePhoneField.click();
					wait.until(ExpectedConditions.attributeContains(myPageZipAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(myPageZipAlert))
					{
						String actualAlert = myPageZipAlert.getText();
						log.add("Actual text is: "+actualAlert);
						log.add("Expected text is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("'Zip Code' field accept numbers and special characters and the corresponding alert message shown",log);
							HBC2109 = true;
						}
						else
						{
							Fail("'Zip Code' field accept numbers and special characters and the corresponding alert message not shown",log);
						}
						myPageZipcode.clear();
					}
					else
					{
						Fail("Zip Code field alert is not displayed");
					}					
				}
				else
				{
					Fail("Zip Code field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2152" +e.getMessage());
				Exception("HBC-2152 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2108 Verify that 'Zip Code' field should accept numbers and special characters and the corresponding alert message should be shown");
			if( HBC2109 == true)
			{
				Pass("'Zip Code' field accept numbers and special characters and the corresponding alert message shown");
			}
			else
			{
				Fail("'Zip Code' field not accept numbers and special characters and the corresponding alert message shown");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2153 Verify that when the user entered less than 5 digits in the "Zip Code" field, then the corresponding alert message should be shown*/
	public void MyAddressZipCodeFieldMinAlert() 
	{
		ChildCreation("HUDSONBAY-2153 Verify that when the user entered less than 5 digits in the 'Zip Code' field, then the corresponding alert message should be shown");
		if(MyAddress == true)
		{
			boolean HBC2110 = false;
			try
			{
				String excelVal = HBCBasicfeature.getExcelNumericVal("HBC1999", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1999", sheet, 2);

				if(HBCBasicfeature.isElementPresent(myPageZipcode))
				{
					Pass("Zip Code field is displayed");
					myPageZipcode.clear();
					Thread.sleep(500);
					log.add( "The Entered value from the excel file is : " + excelVal.toString());
					myPageZipcode.sendKeys(excelVal);
					myAddressBookPagePhoneField.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(myPageZipAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(myPageZipAlert))
					{
						String actualAlert = myPageZipAlert.getText();
						log.add("Actual text is: "+actualAlert);
						log.add("Expected text is: "+expectedAlert);
						Thread.sleep(500);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("'Zip Code' field accept numbers and special characters and the corresponding alert message shown",log);
							HBC2110 = true;
						}
						else
						{
							Fail("'Zip Code' field accept numbers and special characters and the corresponding alert message not shown",log);
						}
						myPageZipcode.clear();
					}
					else
					{
						Fail("Zip Code field alert is not displayed");
					}					
				}
				else
				{
					Fail("Zip Code field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2153" +e.getMessage());
				Exception("HBC-2153 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2109 Verify that when the user entered less than 5 digits in the 'Zip Code' field, then the corresponding alert message should be shown");
			if(HBC2110 == true)
			{
				Pass("'Zip Code' field accept numbers and special characters and the corresponding alert message shown");
			}
			else
			{
				Fail("'Zip Code' field not accept numbers and special characters and the corresponding alert message not shown");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2154 Verify that "Postal Code" field should accept alphanumeric characters.*/
	public void MyAddressPostalAlphaNumericValidation() throws Exception 
	{
		ChildCreation("HUDSONBAY-2154 Verify that 'Postal Code' field should accept alphanumeric characters.");
		if(MyAddress == true)
		{
			boolean HBC2111 = false;
			String cellVal = HBCBasicfeature.getExcelNumericVal("HBC2000", sheet, 1);
			try
			{
				String country = "";
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				
				if(country.contains("Canada"))
				{
					Pass( "The Desired Country was Selected.");
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						Pass( "The Postal Code Field box is displayed.");
						myPageZipcode.click();
						myPageZipcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						myPageZipcode.sendKeys(cellVal);
						String val = myPageZipcode.getAttribute("value");
						log.add( "The Current value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail( "The User was not able to enter any value in the Postal Code field ");
						}
						else if(val.equals(cellVal))
						{
							Pass( "The User able to enter value in the Postal Code field and it matches.",log);
							HBC2111 = true;
						}
						else
						{
							Fail( "The User was able to enter value in the Postal Code field and it does not matches.",log);
						}						
						myPageZipcode.clear();
					}
					else
					{
						Fail( "The Postal Code Field is not displayed.");
						
					}
				}
				else
				{
					Fail( "The Desired Country was not selected.");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2154" +e.getMessage());
				Exception("HBC-2154 There is something wrong. Please Check." + e.getMessage());
			}
			ChildCreation("HUDSONBAY-2110 Verify that 'Postal Code' field should accept alphanumeric characters.");
			if(HBC2111 == true)
			{
				Pass( "'Postal Code' field accept alphanumeric characters.",log);
			}
			else
			{
				Fail( "'Postal Code' field not accept alphanumeric characters.",log);
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2155 Verify that "Postal Code" field should accept maximum of 7 characters.*/
	public void MyAddressPostalMaxCharacters() throws Exception 
	{
		ChildCreation("HUDSONBAY-2155 Verify that 'Postal Code' field should accept maximum of 7 characters.");
		if(MyAddress == true)
		{
			boolean HBC2112 = false;
			String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC2000", sheet, 6).split("\n");
			try
			{
				String country = "";
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				
				if(country.contains("Canada"))
				{
					Pass( "The Desired Country was Selected.");
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						Pass( "The Postal Code Field box is displayed.");
						for(int i=0;i<cellVal.length;i++)
						{
							myPageZipcode.click();
							myPageZipcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							myPageZipcode.sendKeys(cellVal[i]);
							String val = myPageZipcode.getAttribute("value");
							log.add( "The Current value in the field is : " + val);
							if(val.isEmpty())
							{
								Fail( "The User was not able to enter any value in the Postal Code field ");
							}
							else if(val.length()<=7)
							{
								Pass( "The User able to enter value in the Postal Code field and it matches.",log);
								HBC2112 = true;
							}
							else
							{
								Fail( "The User was able to enter value in the Postal Code field and it does not matches.",log);
							}						
							myPageZipcode.clear();
						}
					}
					else
					{
						Fail( "The Postal Code Field is not displayed.");
						
					}
				}
				else
				{
					Fail( "The Desired Country was not selected.");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2155" +e.getMessage());
				Exception("HBC-2155 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2111 Verify that 'Postal Code' field should accept maximum of 7 characters.");
			if( HBC2112 == true)
			{
				Pass("'Postal Code' field accept maximum of 7 characters");
			}
			else
			{
				Fail("'Postal Code' field not accept maximum of 7 characters");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
		
	/*HUDSONBAY-2156 Verify that when the user entered less than 6 digits in the "Postal Code" field, then the corresponding alert message should be shown*/
	/*HUDSONBAY-2157 Verify that alert message should be shown when the user entered alphabets or numbers in the "Postal Code" field*/
	public void MyAddressBookPostalCodeFieldAlert() 
	{
		ChildCreation("HUDSONBAY-2156 Verify that when the user entered less than 6 digits in the 'Postal Code' field, then the corresponding alert message should be shown");
		if(MyAddress == true)
		{
			boolean HBC2157 = false;
			try
			{
				String excelVal = HBCBasicfeature.getExcelVal("HBC2000", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC2000", sheet, 2);
				String country = "";
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");							
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						Pass("Zip Code field is displayed");
						myPageZipcode.clear();
						log.add( "The Entered value from the excel file is : " + excelVal.toString());
						myPageZipcode.sendKeys(excelVal);
						myAddressBookPagePhoneField.click();
						wait.until(ExpectedConditions.attributeContains(myPageZipAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(myPageZipAlert))
						{
							String actualAlert = myPageZipAlert.getText();
							log.add("Actual text is: "+actualAlert);
							log.add("Expected text is: "+expectedAlert);
							if(actualAlert.equalsIgnoreCase(expectedAlert))
							{
								Pass("user entered less than 6 digits in the 'Postal Code' field, then the corresponding alert message shown",log);
								HBC2157 = true;
							}
							else
							{
								Fail("user entered less than 6 digits in the 'Postal Code' field, then the corresponding alert message not shown",log);
							}
							myPageZipcode.clear();
						}
						else
						{
							Fail("Zip Code field alert is not displayed");
						}					
					}
					else
					{
						Fail("Zip Code field is not displayed");
					}
				}
				else
				{
					Fail("Country not selected Canada");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2156" +e.getMessage());
				Exception("HBC-2156 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HBC - 2157 Verify that alert message should be shown when the user entered alphabets or numbers in the 'Postal Code' field");
			if(HBC2157 == true)
			{
				Pass("Alert message shown when the user entered alphabets or numbers in the 'Postal Code' field");
			}
			else
			{
				Fail("Alert message not shown when the user entered alphabets or numbers in the 'Postal Code' field");
			}
			
			ChildCreation("HBC - 2113 Verify that alert message should be shown when the user entered less than 6 digits in the 'Postal Code' field");
			if(HBC2157 == true)
			{
				Pass("Alert message shown when the user entered alphabets or numbers in the 'Postal Code' field");
			}
			else
			{
				Fail("Alert message not shown when the user entered alphabets or numbers in the 'Postal Code' field");
			}
			
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2159 Verify that user should be able to enter phone number in the "Phone Number" field and it should accept maximum of 10 numbers.*/
	public void MyAddressPhoneNumberField() 
	{
		ChildCreation("HUDSONBAY-2159 Verify that user should be able to enter phone number in the 'Phone Number' field and it should accept maximum of 10 numbers.");
		if(MyAddress == true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC2001", sheet, 1).split("\n");
				String country = "";
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");	
					if(HBCBasicfeature.isElementPresent(myAddressBookPagePhoneField))
					{
						HBCBasicfeature.scrolldown(myAddressBookPagePhoneField, driver);
						Pass("Phone field is displayed");
						for(int i = 0; i<cellVal.length;i++)
						{
							myAddressBookPagePhoneField.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							myAddressBookPagePhoneField.sendKeys(cellVal[i]);
							if(myAddressBookPagePhoneField.getAttribute("value").isEmpty())
							{
								Fail( "The Phone field is empty.");
							}
							else if(myAddressBookPagePhoneField.getAttribute("value").length()-2<=10)//since '-' increases size +2 hence reducing
							{
								Pass( "The Phone field was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Phone field was enterable but it is not within the specified character limit.",log);
							}
							myAddressBookPagePhoneField.clear();
						}					
					}
					else
					{
						Fail("Phone field is not displayed");
					}
				}
				else
				{
					Fail("Country selected is not Canada");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2159" +e.getMessage());
				Exception("HBC-2159 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Address Book Page not displayed");
		}
	}
	
	/*HUDSONBAY-2160 Verify that "ext." field should accept maximum of 5 digits*/
	public void MyAddressPhoneNumberExtField() 
	{
		ChildCreation("HUDSONBAY-2160 Verify that 'ext.' field should accept maximum of 5 digits.");
		if(MyAddress == true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1997", sheet, 1).split("\n");
				String country = "";
				if(HBCBasicfeature.isElementPresent(myAddressBookPageCountry))
				{
					Select sel = new Select(myAddressBookPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");	
					if(HBCBasicfeature.isElementPresent(myPagePhoneExt))
					{
						HBCBasicfeature.scrolldown(myPagePhoneExt, driver);
						Pass("Phone Extension field is displayed");
						for(int i = 0; i<cellVal.length;i++)
						{
							myPagePhoneExt.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							myPagePhoneExt.sendKeys(cellVal[i]);
							if(myPagePhoneExt.getAttribute("value").isEmpty())
							{
								Fail( "The Phone Ext field is empty.");
							}
							else if(myPagePhoneExt.getAttribute("value").length()<=5)
							{
								Pass( "The Phone Ext field was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Phone Ext field was enterable but it is not within the specified character limit.",log);
							}
							myPagePhoneExt.clear();
						}					
					}
					else
					{
						Fail("Phone Ext field is not displayed");
					}
				}
				else
				{
					Fail("Country selected is not Canada");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2160" +e.getMessage());
				Exception("HBC-2160 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Address Book Page not displayed");
		}
	}
	
	/*HUDSONBAY-2165 Verify that "Submit" button should be shown in black color with white inline text as per the creative*/
	public void MyAddressBookSubmitButtonCreative() 
	{
		ChildCreation("HUDSONBAY-2165 Verify that 'Submit' button should be shown in black color with white inline text as per the creative");
		if(MyAddress == true)
		{
			try
			{
				String actualBgColor = HBCBasicfeature.getExcelVal("HBC2165", sheet, 1);
				String actualTextColor = HBCBasicfeature.getExcelVal("HBC2165", sheet, 2);
				
				if(HBCBasicfeature.isElementPresent(myAddressBookPageAddressAddNewButton))
				{
					Pass("AddressBook AddNew Button is displayed");
					HBCBasicfeature.scrollup(myAddressBookPageAddressAddNewButton, driver);
					HBCBasicfeature.jsclick(myAddressBookPageAddressAddNewButton, driver);
					Thread.sleep(1500);
					wait.until(ExpectedConditions.visibilityOf(myAddressBookPageNewSubmit));
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewSubmit))
					{
						HBCBasicfeature.scrolldown(myAddressBookPageNewSubmit, driver);
						Pass("Submit button is displayed");
						String bgcolor = myAddressBookPageNewSubmit.getCssValue("background").substring(0, 15);
						String expectedBgcolor = HBCBasicfeature.colorfinder(bgcolor);
						Thread.sleep(500);
						String textcolor = myAddressBookPageNewSubmit.getCssValue("color");
						String expectedTextcolor = HBCBasicfeature.colorfinder(textcolor);
						Thread.sleep(500);						
						log.add("Actual Button color is: "+actualBgColor);
						log.add("Expected Button color is: "+expectedBgcolor);
						log.add("Actual inline text color is: "+actualTextColor);
						log.add("Expected inline text color is: "+expectedTextcolor);
						Thread.sleep(500);						
						if(actualBgColor.equalsIgnoreCase(expectedBgcolor)&&actualTextColor.equalsIgnoreCase(expectedTextcolor))
						{
							Pass("'Submit' button shown in black color with white inline text as per the creative",log);
						}
						else
						{
							Fail("'Submit' button not as per the creative",log);
						}						
					}
					else
					{
						Fail("Submit button is not displayed");
					}
				}
				else
				{
					Fail("AddressBook AddNew Button is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2165" +e.getMessage());
				Exception("HBC-2165 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2166 Verify that "Cancel" button should be shown in black color with white inline text as per the creative*/
	public void MyAddressBookCancelButtonCreative() 
	{
		ChildCreation("HUDSONBAY-2166 Verify that 'Cancel' button should be shown in black color with white inline text as per the creative");
		if(MyAddress == true)
		{
			try
			{
				String actualBgColor = HBCBasicfeature.getExcelVal("HBC2165", sheet, 1);
				String actualTextColor = HBCBasicfeature.getExcelVal("HBC2165", sheet, 2);							
				if(HBCBasicfeature.isElementPresent(myAddressBookPageNewCancel))
				{
					HBCBasicfeature.scrolldown(myAddressBookPageNewCancel, driver);
					Pass("Cancel button is displayed");
					String bgcolor = myAddressBookPageNewCancel.getCssValue("background").substring(0, 15);
					String expectedBgcolor = HBCBasicfeature.colorfinder(bgcolor);
					Thread.sleep(500);
					String textcolor = myAddressBookPageNewCancel.getCssValue("color");
					String expectedTextcolor = HBCBasicfeature.colorfinder(textcolor);
					Thread.sleep(500);						
					log.add("Actual Button color is: "+actualBgColor);
					log.add("Expected Button color is: "+expectedBgcolor);
					log.add("Actual inline text color is: "+actualTextColor);
					log.add("Expected inline text color is: "+expectedTextcolor);
					Thread.sleep(500);						
					if(actualBgColor.equalsIgnoreCase(expectedBgcolor)&&actualTextColor.equalsIgnoreCase(expectedTextcolor))
					{
						Pass("'Cancel' button shown in black color with white inline text as per the creative",log);
					}
					else
					{
						Fail("'Cancel' button not as per the creative",log);
					}						
				}
				else
				{
					Fail("Cancel button is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2166" +e.getMessage());
				Exception("HBC-2166 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2161 Verify that on entering all the details and on tapping "Cancel" button, the page/overlay should be closed and updated details should not get updated*/
	public void MyAddressBookAddNewAddressCancel() 
	{
		ChildCreation("HUDSONBAY-2161 Verify that on entering all the details and on tapping 'Cancel' button, the page/overlay should be closed and updated details should not get updated");			
		if(MyAddress == true)
		{		
			try
			{
				String fname = HBCBasicfeature.getExcelVal("HBC2135", sheet, 1);
				String lname = HBCBasicfeature.getExcelVal("HBC2135", sheet, 2);
				String stAddr = HBCBasicfeature.getExcelVal("HBC2135", sheet, 3);
				String city = HBCBasicfeature.getExcelVal("HBC2135", sheet, 4);
				String zip = HBCBasicfeature.getExcelVal("HBC2135", sheet, 6);
				String phone = HBCBasicfeature.getExcelNumericVal("HBC2135", sheet, 7);
				if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
				{
					Pass("My AddressBook Page displayed");
					if(HBCBasicfeature.isElementPresent(myAddressBookNewFname))
					{
						Pass("FirstName field is displayed");
						myAddressBookNewFname.clear();
						myAddressBookNewFname.sendKeys(fname);
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookNewLname))
					{
						Pass("LastName field is displayed");
						myAddressBookNewLname.clear();
						myAddressBookNewLname.sendKeys(lname);
					}
					else
					{
						Fail("LastName field is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myAddressBookNewStAddres))
					{
						Pass("Street Address field 1 is displayed");
						myAddressBookNewStAddres.clear();
						myAddressBookNewStAddres.sendKeys(stAddr);
					}
					else
					{
						Fail("Street Address field 1 is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myAddressBookNewCity))
					{
						HBCBasicfeature.scrolldown(myAddressBookNewCity, driver);
						Pass("City field is displayed");
						myAddressBookNewCity.clear();
						myAddressBookNewCity.sendKeys(city);
					}
					else
					{
						Fail("City field is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myAddressBookNewZipcode))
					{
						HBCBasicfeature.scrolldown(myAddressBookNewZipcode, driver);
						Pass("PostalCode field is displayed");
						myAddressBookNewZipcode.clear();
						myAddressBookNewZipcode.sendKeys(zip);
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewPhoneField))
					{
						HBCBasicfeature.scrolldown(myAddressBookPageNewPhoneField, driver);
						Pass("Phone field & extension field is displayed");
						myAddressBookPageNewPhoneField.clear();
						myAddressBookPageNewPhoneField.sendKeys(phone);
					}
					else
					{
						Fail("Phone field & extension field is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewCancel))
					{
						Pass("Cancel button is displayed");
						HBCBasicfeature.jsclick(myAddressBookPageNewCancel, driver);
						Thread.sleep(1000);
						String actual = myPageFname.getAttribute("value");
						if(actual.equalsIgnoreCase(fname))
						{
							Fail("on entering all the details and on tapping 'Cancel' button, the page/overlay closed and updated details get updated");
						}
						else
						{
							Pass("on entering all the details and on tapping 'Cancel' button, the page/overlay closed and updated details not get updated");
						}						
					}
					else
					{
						Fail("Cancel Button is not displayed");
					}				
				}
				else
				{
					Fail("My AddressBook page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2161" +e.getMessage());
				Exception("HBC-2161 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
		
	/*HUDSONBAY-2128 Verify that on updating the details and tapping on "< Account" link, the deatils should not get updated*/
	public void MyAddressBackAccountClickNoUpdate() throws Exception 
	{
		ChildCreation("HUDSONBAY-2128 Verify that on updating the details and tapping on '< Account' link, the deatils should not get updated");
		if(MyAddress == true)
		{
			boolean HBC2086 = false;
			String bfreAddr = "";
			String addr = HBCBasicfeature.getExcelVal("HBC2007", sheet, 1); 
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					HBCBasicfeature.scrollup(myAccountpageBack, driver);
					Pass("'< Account'link is displayed");
					HBCBasicfeature.jsclick(myAccountpageBack, driver);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
					{
						Pass("MyAccount Page profile details are displayed");
						bfreAddr = myAccountPageMyProfileDetails.getText();	
						MyAccount = true;
						MyAddress = false;
					}
					else
					{
						Fail("MyAccount Page profile details are not displayed");
					}
					
					if(HBCBasicfeature.isElementPresent(myAccountPageAddressbook))
					{							
						Pass("My AddressBook section displayed");
						HBCBasicfeature.jsclick(myAccountPageAddressbook, driver);									
						wait.until(ExpectedConditions.visibilityOf(myAccountAddressBookpage));
						if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
						{
							Pass("My AddressBook page displayed");
							MyAccount = false;
							MyAddress = true;
							if(HBCBasicfeature.isElementPresent(myPageStAddres))
							{
								myPageStAddres.clear();
								myPageStAddres.sendKeys(addr);
								log.add("Entered value is: "+addr);
								Thread.sleep(200);
								HBCBasicfeature.jsclick(myAccountpageBack, driver);
								wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
								if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
								{
									MyAccount = true;
									MyAddress = false;
									String afterAddr = myAccountPageMyProfileDetails.getText();
									log.add("Before updating the details: "+bfreAddr);
									log.add("After updating the details: "+afterAddr);
									Thread.sleep(500);
									if(bfreAddr.contains(afterAddr))
									{
										Pass("on updating the details and tapping on '< Account' link, the deatils not get updated",log);
										HBC2086 = true;
									}
									else
									{
										Fail("on updating the details and tapping on '< Account' link, the deatils get updated",log);
									}
								}
								else
								{
									Fail("MyAccount Page profile details are not displayed");
								}
							}
							else
							{
								Fail("street Address field not displayed");
							}
						}
						else
						{
							Fail("My AddressBook page not displayed");
						}							
					}
					else
					{
						Fail("My AddressBook section not displayed");
					}					
				}
				else
				{
					Fail("'< Account'link is not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println("HBC-2128" +e.getMessage());
				Exception("HBC-2128 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2086 Verify that on updating the details and tapping on '< Account' link, the deatils should not get updated");
			if(HBC2086 == true)
			{
				Pass("on updating the details and tapping on '< Account' link, the deatils not get updated");
			}
			else
			{
				Fail("on updating the details and tapping on '< Account' link, the deatils get updated");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2129 Verify that on updating the details and tapping on "Update" button, the details should be updated in the "Address Book" page*/
	public void MyAddressBookUpdateClick() throws Exception 
	{
		ChildCreation("HUDSONBAY-2129 Verify that on updating the details and tapping on 'Update' button, the details should be updated in the 'Address Book' page");
		if(MyAccount == true)
		{
			boolean HBC2087 = false;
			String bfreAddr = "";
			String addr = HBCBasicfeature.getExcelVal("HBC2007", sheet, 1); 
			String addr1 = HBCBasicfeature.getExcelVal("HBC2007", sheet, 2); 

			try
			{		
				wait.until(ExpectedConditions.visibilityOf(myAccountPageAddressbook));
				if(HBCBasicfeature.isElementPresent(myAccountPageAddressbook))
				{							
					Pass("My AddressBook section displayed");
					HBCBasicfeature.jsclick(myAccountPageAddressbook, driver);									
					wait.until(ExpectedConditions.visibilityOf(myAccountAddressBookpage));
					if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
					{
						MyAddress = true;
						Pass("MyProfile page displayed");
						if(HBCBasicfeature.isElementPresent(myPageStAddres))
						{
							bfreAddr = myPageStAddres.getAttribute("value");
							myPageStAddres.clear();
							myPageStAddres.sendKeys(addr);
							log.add("Entered value is: "+addr);
							Thread.sleep(200);
							if(HBCBasicfeature.isElementPresent(myAddressBookPageUpdate))
							{
								HBCBasicfeature.scrolldown(myAddressBookPageUpdate, driver);
								Thread.sleep(200);
								HBCBasicfeature.jsclick(myAddressBookPageUpdate, driver);
								try
								{
									wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
									if(HBCBasicfeature.isElementPresent(addressuseasEntered))
									{
										addressuseasEntered.click();
									}
								}
								catch(Exception e)
								{
									Fail("Address verification overlay not displayed");
								}	
								wait.until(ExpectedConditions.attributeContains(myAddressBookSuccessAddAlert, "style", "block"));								
								if(HBCBasicfeature.isElementPresent(myPageStAddres))
								{
									String afterAddr = myPageStAddres.getAttribute("value");
									log.add("Before updating the details: "+bfreAddr);
									log.add("After updating the details: "+afterAddr);
									Thread.sleep(500);
									if(bfreAddr.contains(afterAddr))
									{
										Fail("on updating the details and tapping on 'Update' link, the deatils not get updated",log);
									}
									else
									{
										Pass("on updating the details and tapping on 'Update' link, the deatils get updated",log);
										HBC2087 = true;
									}
									
									//Revert the changes back for further execution	
									
									if(HBCBasicfeature.isElementPresent(myPageStAddres))
									{
										bfreAddr = myPageStAddres.getAttribute("value");
										myPageStAddres.clear();
										myPageStAddres.sendKeys(addr1);
										log.add("Entered value is: "+addr1);
										Thread.sleep(200);
										if(HBCBasicfeature.isElementPresent(myAddressBookPageUpdate))
										{
											HBCBasicfeature.scrolldown(myAddressBookPageUpdate, driver);
											Thread.sleep(200);
											HBCBasicfeature.jsclick(myAddressBookPageUpdate, driver);
											try
											{
												wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
												if(HBCBasicfeature.isElementPresent(addressuseasEntered))
												{
													addressuseasEntered.click();
												}
											}
											catch(Exception e)
											{
												Fail("Address verification overlay not displayed");
											}	
											wait.until(ExpectedConditions.attributeContains(myAddressBookSuccessAddAlert, "style", "block"));								
											if(HBCBasicfeature.isElementPresent(myPageStAddres))
											{
												String currentAddr = myPageStAddres.getAttribute("value");
												log.add("Before updating the details: "+bfreAddr);
												log.add("After updating the details: "+afterAddr);
												Thread.sleep(500);
												if(bfreAddr.contains(currentAddr))
												{
													Fail("on updating the details and tapping on 'Update' link, the deatils not get updated",log);
												}
												else
												{
													Pass("on updating the details and tapping on 'Update' link, the deatils get updated",log);													
												}
											}
											else
											{
												Fail("street Address field not displayed");
											}
										}
										else
										{
											Fail("Update Button not displayed");
										}						
									}
									else
									{
										Fail("street Address field not displayed");
									}
								}
								else
								{
									Fail("MyAccount Page profile details are not displayed");
								}								
							}
							else
							{
								Fail("Update Button not displayed");
							}
						}
						else
						{
							Fail("street Address field not displayed");
						}
					}
					else
					{
						Fail("My AddressBook page not displayed");
					}							
				}
				else
				{
					Fail("My AddressBook section not displayed");
				}						
			}
			catch (Exception e)
			{
				System.out.println("HBC-2129" +e.getMessage());
				Exception("HBC-2129 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2087 Verify that on updating the details and tapping on 'Update' button, the details should be updated in the 'Address Book' page");
			if(HBC2087==true)
			{
				Pass("on updating the details and tapping on 'Update' link, the deatils get updated");
			}
			else
			{
				Fail("on updating the details and tapping on 'Update' link, the deatils not get updated");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2125 Verify that user not able to delete the address, when only one address is shown*/
	public void MyAddressNoDelete() 
	{
		ChildCreation("HUDSONBAY-2125 Verify that user not able to delete the address, when only one address is shown");
		if(MyAddress == true)
		{
			boolean HBC2083 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageAddressDropDown))
				{
					Pass("AddressBook DropDown displayed");
					Select sel = new Select(myAccountPageAddressDropDown);
					int size = sel.getOptions().size();
					if(size==1)
					{
						String bfreAddress = sel.getOptions().get(0).getAttribute("value");
						myAccountPageAddressRemoveButton.click();
						wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
						String aftrAddress = sel.getOptions().get(0).getAttribute("value");
						Thread.sleep(200);
						log.add("Before delete address name is: "+bfreAddress);
						log.add("After delete address name is: "+aftrAddress);						
						if(bfreAddress.equalsIgnoreCase(aftrAddress))
						{
							Pass("User not able to delete the address, when only one address is shown",log);
							HBC2083 = true;
						}
						else
						{
							Fail("User able to delete the address, when only one address is shown",log);
						}						
					}
					else
					{
						Fail("More than one address is displayed"+ size);
					}
				}
				else
				{
					Fail("AddressBook DropDown not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2125" +e.getMessage());
				Exception("HBC-2125 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-2083 Verify that user not able to delete the address, when only one address is shown");
			if(HBC2083==true)
			{
				Pass("User not able to delete the address, when only one address is shown");
			}
			else
			{
				Fail("User able to delete the address, when only one address is shown");
			}	
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	/*HUDSONBAY-2124 Verify that on deleting the address, the success alert should be shown*/
	public void MyAddressBookDeleteAddress() 
	{
		ChildCreation("HUDSONBAY-2124 Verify that on deleting the address, the success alert should be shown");			
		if(MyAddress == true)
		{
			boolean HBC2082 = false;
			try
			{
				String fname = HBCBasicfeature.getExcelVal("HBC2135", sheet, 1);
				String lname = HBCBasicfeature.getExcelVal("HBC2135", sheet, 2);
				String stAddr = HBCBasicfeature.getExcelVal("HBC2135", sheet, 3);
				String city = HBCBasicfeature.getExcelVal("HBC2135", sheet, 4);
				String prov = HBCBasicfeature.getExcelVal("HBC2135", sheet, 5);
				String zip = HBCBasicfeature.getExcelVal("HBC2135", sheet, 6);
				String phone = HBCBasicfeature.getExcelNumericVal("HBC2135", sheet, 7);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC2124", sheet, 1);
				String defaultaddr ="";
				Select sel = new Select(myAccountPageAddressDropDown);
				int size = sel.getOptions().size();
				if(size==1)
				{
					defaultaddr = sel.getOptions().get(0).getAttribute("value");
				}
				
				if(HBCBasicfeature.isElementPresent(myAddressBookPageAddressAddNewButton))
				{
					Pass("Addnew Button displayed");
					HBCBasicfeature.jsclick(myAddressBookPageAddressAddNewButton, driver);
					Thread.sleep(1500);					
					if(HBCBasicfeature.isElementPresent(myAddressBookNewFname))
					{
						Pass("FirstName field is displayed");
						myAddressBookNewFname.clear();
						myAddressBookNewFname.sendKeys(fname);
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookNewLname))
					{
						Pass("LastName field is displayed");
						myAddressBookNewLname.clear();
						myAddressBookNewLname.sendKeys(lname);
					}
					else
					{
						Fail("LastName field is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myAddressBookNewStAddres))
					{
						Pass("Street Address field 1 is displayed");
						myAddressBookNewStAddres.clear();
						myAddressBookNewStAddres.sendKeys(stAddr);
					}
					else
					{
						Fail("Street Address field 1 is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myAddressBookNewCity))
					{
						HBCBasicfeature.scrolldown(myAddressBookNewCity, driver);
						Pass("City field is displayed");
						myAddressBookNewCity.clear();
						myAddressBookNewCity.sendKeys(city);
					}
					else
					{
						Fail("City field is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewCountry))
					{
						Select sel1 = new Select(myAddressBookPageNewCountry);
						String val = sel1.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel1.selectByValue("CA");						
						}
						wait.until(ExpectedConditions.visibilityOf(myAddressBookPageNewCountry));
						String country = sel1.getFirstSelectedOption().getText();
						if(country.contains("Canada"))
						{
							Pass("country selected is Canada");
						}
						else
						{
							Fail("country selected is not Canada");
						}
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}				
					
					if(HBCBasicfeature.isElementPresent(myAddressBookNewState))
					{
						HBCBasicfeature.scrolldown(myAddressBookNewState, driver);
						Pass("Province field is displayed");
						Select sell = new Select(myAddressBookNewState);
						sell.selectByValue(prov);						
						wait.until(ExpectedConditions.visibilityOf(myAddressBookNewState));
						String province = sell.getFirstSelectedOption().getText();
						if(province.contains("Nova Scotia"))
						{
							Pass("Province selected is Nova Scotia");
						}
						else
						{
							Fail("Province selected is not Nova Scotia");
						}						
					}
					else
					{
						Fail("Province field is not displayed");
					}				
					if(HBCBasicfeature.isElementPresent(myAddressBookNewZipcode))
					{
						HBCBasicfeature.scrolldown(myAddressBookNewZipcode, driver);
						Pass("PostalCode field is displayed");
						myAddressBookNewZipcode.clear();
						myAddressBookNewZipcode.sendKeys(zip);
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewPhoneField))
					{
						HBCBasicfeature.scrolldown(myAddressBookPageNewPhoneField, driver);
						Pass("Phone field  is displayed");
						myAddressBookPageNewPhoneField.clear();
						myAddressBookPageNewPhoneField.sendKeys(phone);
					}
					else
					{
						Fail("Phone field is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(myAddressBookPageNewSubmit))
					{
						Pass("Submit button is displayed");
						HBCBasicfeature.jsclick(myAddressBookPageNewSubmit, driver);
						try
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							if(HBCBasicfeature.isElementPresent(addressuseasEntered))
							{
								addressuseasEntered.click();
							}
						}
						catch(Exception e)
						{
							Fail("Address verification overlay not displayed");
						}	
						wait.until(ExpectedConditions.attributeContains(myAddressBookSuccessAddAlert, "style", "block"));	
						if(HBCBasicfeature.isElementPresent(myAccountPageAddressDropDown))
						{
							Pass("AddressBook DropDown displayed");
							size = sel.getOptions().size();
							if(size>1)
							{
								do
								{
									int i=1;
									String address = sel.getOptions().get(i).getAttribute("value");
									Thread.sleep(200);
									if(address.equals(defaultaddr))
									{
										Pass("Default Address: "+address);
									}
									else
									{
										sel.getOptions().get(i).click();
										Thread.sleep(500);
										myAccountPageAddressRemoveButton.click();
										Thread.sleep(2000);										
										wait.until(ExpectedConditions.visibilityOf(myAddressBookSuccessRemoveAlert));	
										if(HBCBasicfeature.isElementPresent(myAddressBookSuccessRemoveAlert))
										{
											Pass("on deleting the address, the success alert shown");
											String actualAlert = myAddressBookSuccessRemoveAlert.getText();
											log.add("Actual alert is: "+actualAlert);
											log.add("Expected alert is: "+expectedAlert);
											Thread.sleep(500);
											if(expectedAlert.equalsIgnoreCase(actualAlert))
											{
												Pass("Expected alert displayed on deleting the address ",log);
												HBC2082 = true;
											}
											else
											{
												Fail("Expected alert not displayed on deleting the address",log);
											}
										}
										else
										{
											Fail("on deleting the address, the success alert not shown");
										}																			
									}										
									size = sel.getOptions().size();
								}
								while(size!=1);									
							}
							else
							{
								Fail("Not more than one Address displayed");
							}
						}
						else
						{
							Fail("AddressBook DropDown not displayed");
						}												
					}
					else
					{
						Fail("Cancel Button is not displayed");
					}				
				}
				else
				{
					Fail("My AddressBook page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2124" +e.getMessage());
				Exception("HBC-2124 There is something wrong. Please Check." + e.getMessage());
			}

			ChildCreation("HUDSONBAY-2082 Verify that on deleting the address, the success alert should be shown");			
			if(HBC2082 == true)
			{
				Pass("Expected alert displayed on deleting the address ");
			}
			else
			{
				Fail("Expected alert not displayed on deleting the address");
			}
		}
		else
		{
			Skip("My AddressBook Page not displayed");
		}
	}
	
	
}

