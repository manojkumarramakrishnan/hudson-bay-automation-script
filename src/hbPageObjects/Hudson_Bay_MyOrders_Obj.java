package hbPageObjects;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_MyOrders;

public class Hudson_Bay_MyOrders_Obj extends Hudson_Bay_MyOrders implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_MyOrders_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+homepagee+"")
	WebElement homepage;	
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;	
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+pancakeStaticWelcomee+"")
	WebElement pancakeStaticWelcome;	
	
	@FindBy(xpath = ""+signInPagee+"")
	WebElement signInPage;
	
	@FindBy(xpath = ""+emailFieldd+"")
	WebElement emailField;
	
	@FindBy(xpath = ""+pwdFieldd+"")
	WebElement pwdField;
	
	@FindBy(xpath = ""+signInButtonn+"")
	WebElement signInButton;
	
	@FindBy(xpath = ""+myAccountPagee+"")
	WebElement myAccountPage;
	
	@FindBy(xpath = ""+myAccountPageMyProfileTxtt+"")
	WebElement myAccountPageMyProfileTxt;
	
	@FindBy(xpath = ""+myAccountPageMyProfileDetailss+"")
	WebElement myAccountPageMyProfileDetails;
	
	@FindBy(xpath = ""+myAccountPageMyOrderss+"")
	WebElement myAccountPageMyOrders;
	
	@FindBy(xpath = ""+myAccountMyOrdersPagee+"")
	WebElement myAccountMyOrdersPage;
	
	@FindBy(xpath = ""+myAccountpageBackk+"")
	WebElement myAccountpageBack;	
	
	@FindBy(xpath = ""+myOrdersOrdernumberr+"")
	WebElement myOrdersOrdernumber;
	
	@FindBy(xpath = ""+myOrdersPostalnumberr+"")
	WebElement myOrdersPostalnumber;
	
	@FindBy(xpath = ""+myOrdersGetStatusButtonn+"")
	WebElement myOrdersGetStatusButton;
	
	@FindBy(xpath = ""+myOrdersGetStatusCSSButtonn+"")
	WebElement myOrdersGetStatusCSSButton;	
	
	@FindBy(xpath = ""+myOrdersCustServiceNumberr+"")
	WebElement myOrdersCustServiceNumber;	
	
	@FindBy(xpath = ""+myOrdersOrdernumberAlertt+"")
	WebElement myOrdersOrdernumberAlert;
	
	@FindBy(xpath = ""+myOrdersPostalnumberAlertt+"")
	WebElement myOrdersPostalnumberAlert;		
	
	@FindBy(xpath = ""+addressVerificationoverlayy+"")
	WebElement addressVerificationoverlay;
	
	@FindBy(xpath = ""+addressuseasEnteredd+"")
	WebElement addressuseasEntered;
	
	
	boolean MyOrders, MyAccount = false;
	
	/*SignIn page to MyAccount-MyOrders Page*/
	public void signin() throws java.lang.Exception
	{
		ChildCreation("SignIn page to MyAccount-MyOrders Page");
		String email = HBCBasicfeature.getExcelVal("login", sheet, 5);
		String pwd = HBCBasicfeature.getExcelVal("login", sheet, 1);
		try
		{
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{					
					try
					{						
						do
						{
							try
							{
								HBCBasicfeature.jsclick(hamburger, driver);
								Thread.sleep(2000);
								if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
								{
									flag = true;
									break;
								}
							}
							catch(Exception e)
							{
								continue;
							}
						}
						while(flag!=true);		
						if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
						{
							flag = false;
							HBCBasicfeature.scrolldown(pancakeStaticWelcome, driver);
							pancakeStaticWelcome.click();
							try
							{
								wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
								flag = true;
								break;
							}
							catch(Exception e)
							{
								flag = false;
								continue;
							}
						}
						else
						{
							continue;
						}
					}
					catch(Exception e)
					{
						driver.navigate().refresh();
						continue;
					}
				}
				while(flag!=true);	
				wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
				if(HBCBasicfeature.isElementPresent(emailField))
				{	
					emailField.sendKeys(email);								
					log.add("Entered Email ID is: "+ email);
					if(HBCBasicfeature.isElementPresent(pwdField))
					{	
						pwdField.sendKeys(pwd);									
						log.add("Entered Password is: "+ pwd);
						if(HBCBasicfeature.isElementPresent(signInButton))
						{
							signInButton.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.visibilityOf(myAccountPage));			
							if(HBCBasicfeature.isElementPresent(myAccountPage))
							{	
								Pass("MyAccount Page displayed",log);
								if(HBCBasicfeature.isElementPresent(myAccountPageMyOrders))
								{											
									Pass("My Orders section is displayed");
									HBCBasicfeature.jsclick(myAccountPageMyOrders, driver);									
									wait.until(ExpectedConditions.visibilityOf(myAccountMyOrdersPage));
									if(HBCBasicfeature.isElementPresent(myAccountMyOrdersPage))
									{
										Pass("My Orders page displayed");
										MyOrders = true;
									}
									else
									{
										Fail("My Orders page not dispalyed");
									}
								}
								else
								{
									Fail("My Orders section not displayed");
								}								
							}
							else
							{
								Fail("MyAccount page not displayed",log);
							}
						}
						else
						{
							Fail("SignIn button not displayed");
						}
					}
					else
					{
						Fail("Password Field not displayed");
					}
				}
				else
				{
					Fail("Email Field not displayed");
				}		
			}
			else
			{
				Fail("Hamburger menuu not displayed");
			}				
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-2032 Verify that "My Order" page should be displayed as per the creative*/
	public void MyOrderspageCreative() 
	{
		if(MyOrders == true)
		{		
			ChildCreation("HUDSONBAY-2032 Verify that 'My Order' page should be displayed as per the creative");			
			try
			{				
				if(HBCBasicfeature.isElementPresent(myAccountMyOrdersPage))
				{
					Pass("My Orders Page displayed");
					if(HBCBasicfeature.isElementPresent(myOrdersOrdernumber))
					{
						Pass("Order Number field is displayed");
					}
					else
					{
						Fail("Order Number field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myOrdersPostalnumber))
					{
						Pass("Postal Number field is displayed");
					}
					else
					{
						Fail("Postal Number field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myOrdersGetStatusButton))
					{
						Pass("Get order status Button is displayed");
					}
					else
					{
						Fail("Get order status Button is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myOrdersCustServiceNumber))
					{
						Pass("Customer Sercive number is displayed");
					}
					else
					{
						Fail("Customer Sercive number is not displayed");
					}					
					/*if(HBCBasicfeature.isElementPresent(signupEmails))
					{
						Pass("SignUp for emails button is displayed");
					}
					else
					{
						Fail("SignUp for emails Button is not displayed");
					}*/
					if(HBCBasicfeature.isElementPresent(footerContainer))
					{
						Pass("Footer container is displayed");
					}
					else
					{
						Fail("Footer container is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(myAccountpageBack))
					{
						HBCBasicfeature.scrollup(myAccountpageBack, driver);
						Pass("'< Account'link is displayed");
					}
					else
					{
						Fail("'< Account'link is not displayed");
					}			
				}
				else
				{
					Fail("My Order page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2032" +e.getMessage());
				Exception("HBC-2032 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Order Page not displayed");
		}
	}
	
	/*HUDSONBAY-2033 Verify that "< Account" link should be shown in the left side of the page and it should be highlighted in blue color as per the creative*/
	public void MyOrdersPageBackArrow() 
	{
		ChildCreation("HUDSONBAY-2033 Verify that '< Account' link should be shown in the left side of the page and it should be highlighted in blue color as per the creative");
		if(MyOrders == true)
		{
			try
			{
				String expectedTxt = HBCBasicfeature.getExcelVal("HBC2005", sheet, 1);
				String expectedColor = HBCBasicfeature.getExcelVal("HBC2005", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					Pass("'< Account'link is displayed");	
					String actualTxt = myAccountpageBack.getText();
					String Color = myAccountpageBack.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(Color);
					log.add("The Actual text is: "+actualTxt);
					log.add("The Expected text is: "+expectedTxt);
					log.add("The Actual Color is: "+actualColor);
					log.add("The Expected Color is: "+expectedColor);					
					if(actualTxt.equalsIgnoreCase(expectedTxt)&&actualColor.equalsIgnoreCase(expectedColor))
					{
						Pass("'< Account' link shown in the left side of the page and it highlighted in blue color as per the creative",log);
					}
					else
					{
						Fail("'< Account' link not shown in the left side of the page and not as per the creative",log);
					}				
				}
				else
				{
					Fail("'< Account' link not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2033" +e.getMessage());
				Exception("HBC-2033 There is something wrong. Please Check." + e.getMessage());
			}	
		}
		else
		{
			Skip("My Orders Page not displayed");
		}
	}
	
	/*HUDSONBAY-2034 Verify that on tapping "< Account" link, it should navigates to "My Account" page*/
	public void MyOrdersBackArrowClick() 
	{
		ChildCreation("HUDSONBAY-2034 Verify that on tapping '< Account' link, it should navigates to 'My Account' page");
		if(MyOrders == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					HBCBasicfeature.jsclick(myAccountpageBack, driver);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileTxt))
					{
						Pass("on tapping '< Account' link, navigates to 'My Account' page");
						MyOrders=false;				
					}
					else
					{
						Fail("on tapping '< Account' link, not navigates to 'My Account' page");
					}					
					if(HBCBasicfeature.isElementPresent(myAccountPageMyOrders))
					{						
						HBCBasicfeature.jsclick(myAccountPageMyOrders, driver);									
						wait.until(ExpectedConditions.visibilityOf(myAccountMyOrdersPage));
						if(HBCBasicfeature.isElementPresent(myAccountMyOrdersPage))
						{
							Pass("Navigated back to My Orders page ");
							MyOrders = true;
						}
						else
						{
							Fail("My Orders page not dispalyed");
						}
					}
					else
					{
						Fail("MyOrders section not displayed");
					}
				}
				else
				{
					Fail("'< Account' link not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2034" +e.getMessage());
				Exception("HBC-2034 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Orders Page not displayed");
		}
	}
	
	/*HUDSONBAY-2035 Verify that "Order Number", "Billing Postal Code" fields should be shown in the "My Orders" page*/
	public void MyOrderOrderNumberPostalNumberFields() 
	{
		ChildCreation("HUDSONBAY-2035 Verify that 'Order Number', 'Billing Postal Code' fields should be shown in the 'My Orders' page");
		if(MyOrders == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myOrdersOrdernumber))
				{
					Pass("Order Number field is displayed");
				}
				else
				{
					Fail("Order Number field is not displayed");
				}
				if(HBCBasicfeature.isElementPresent(myOrdersPostalnumber))
				{
					Pass("Postal Code Number field is displayed");
				}
				else
				{
					Fail("Postal Code Number field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2035" +e.getMessage());
				Exception("HBC-2035 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Order Page not displayed");
		}
	}
		
	/*HUDSONBAY-2036 Verify that on tapping inside the fields and navigating to other fields alert should be shown as like the classic site*/
	public void MyOrdersFieldsEmptyValidation() 
	{
		ChildCreation("HUDSONBAY-2036 Verify that on tapping inside the fields and navigating to other fields alert should be shown as like the classic site");
		if(MyOrders == true)
		{
			try
			{
				String[] expectedAlert = HBCBasicfeature.getExcelVal("HBC2036", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(myAccountMyOrdersPage))
				{
					Pass("MyOrders Page displayed");
					if(HBCBasicfeature.isElementPresent(myOrdersOrdernumber))
					{
						Pass("Order Number field is displayed");
						myOrdersOrdernumber.clear();
						myOrdersPostalnumber.click();
						wait.until(ExpectedConditions.visibilityOf(myOrdersOrdernumberAlert));
						if(HBCBasicfeature.isElementPresent(myOrdersOrdernumberAlert))
						{
							String actualAlert = myOrdersOrdernumberAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[0]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[0]))
							{
								Pass("Alert shown when the user leaves Order Number field without filling the details",log);
							}
							else
							{
								Fail("Alert not matches when the user leaves Order Number field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Order Number field is empty ");
						}						
					}
					else
					{
						Fail("Order Number field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myOrdersPostalnumber))
					{
						Pass("PostalCode field is displayed");
						myOrdersPostalnumber.clear();
						myOrdersOrdernumber.click();
						wait.until(ExpectedConditions.visibilityOf(myOrdersPostalnumberAlert));
						if(HBCBasicfeature.isElementPresent(myOrdersPostalnumberAlert))
						{
							String actualAlert = myOrdersPostalnumberAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[1]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[1]))
							{
								Pass("Alert shown when the user leaves PostalCode field without filling the details",log);
							}
							else
							{
								Fail("Alert not matches when the user leaves PostalCode field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when PostalCode field is empty ");
						}						
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}						
				}
				else
				{
					Fail("My Orders page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2036" +e.getMessage());
				Exception("HBC-2036 There is something wrong. Please Check." + e.getMessage());
			}		
		}
		else
		{
			Skip("My Orders Page not displayed");
		}
	}
	
	/*HUDSONBAY-2037 Verify that Customer care phone number should be highlighted in blue color as per the creative*/
	public void MyOrderCustomerCareNumber() throws Exception 
	{
		ChildCreation("HUDSONBAY-2037 Verify that Customer care phone number should be highlighted in blue color as per the creative");
		if(MyOrders == true)
		{
			String expColor = HBCBasicfeature.getExcelVal("HBC2037", sheet, 2);
			try
			{
				if(HBCBasicfeature.isElementPresent(myOrdersCustServiceNumber))
				{
					Pass("Customer care phone number displayed");
					String color = myOrdersCustServiceNumber.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(color);
					log.add("The Actual highlighted color is: "+actualColor);
					log.add("The Expected color is: "+expColor);
					Thread.sleep(500);
					if(actualColor.equalsIgnoreCase(expColor))
					{
						Pass("Customer care phone number highlighted in blue color as per the creative",log);
					}
					else
					{
						Fail("Customer care phone number not highlighted in blue color as per the creative",log);
					}
				}
				else
				{
					Fail("Customer care phone number not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2037" +e.getMessage());
				Exception("HBC-2037 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Order Page not displayed");
		}
	}
	
	/*HUDSONBAY-2042 Verify that "Get Order Status" button should be shown in black color with white inline text as per the creative*/
	public void MyOrdersGetOrderStatusButtonCreative() 
	{
		ChildCreation("HUDSONBAY-2042 Verify that 'Get Order Status' button should be shown in black color with white inline text as per the creative");
		if(MyOrders == true)
		{
			try
			{
				String actualBgColor = HBCBasicfeature.getExcelVal("HBC2165", sheet, 1);
				String actualTextColor = HBCBasicfeature.getExcelVal("HBC2165", sheet, 2);								
				if(HBCBasicfeature.isElementPresent(myOrdersGetStatusButton))
				{
					HBCBasicfeature.scrolldown(myOrdersGetStatusButton, driver);
					Pass("Get Order Status button is displayed");
					String bgcolor = myOrdersGetStatusCSSButton.getCssValue("background").substring(0, 15);
					String expectedBgcolor = HBCBasicfeature.colorfinder(bgcolor);
					Thread.sleep(500);
					String textcolor = myOrdersGetStatusCSSButton.getCssValue("color");
					String expectedTextcolor = HBCBasicfeature.colorfinder(textcolor);
					Thread.sleep(500);						
					log.add("Actual Button color is: "+actualBgColor);
					log.add("Expected Button color is: "+expectedBgcolor);
					log.add("Actual inline text color is: "+actualTextColor);
					log.add("Expected inline text color is: "+expectedTextcolor);
					Thread.sleep(500);						
					if(actualBgColor.equalsIgnoreCase(expectedBgcolor)&&actualTextColor.equalsIgnoreCase(expectedTextcolor))
					{
						Pass("'Get Order Status' button shown in black color with white inline text as per the creative",log);
					}
					else
					{
						Fail("'Get Order Status' button not as per the creative",log);
					}						
				}
				else
				{
					Fail("Get Order Status button is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2042" +e.getMessage());
				Exception("HBC-2042 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("My Orders Page not displayed");
		}
	}
	
}

