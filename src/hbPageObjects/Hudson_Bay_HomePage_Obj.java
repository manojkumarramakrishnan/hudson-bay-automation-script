package hbPageObjects;

import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_HomePage;

public class Hudson_Bay_HomePage_Obj extends Hudson_Bay_HomePage implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_HomePage_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+promoBannerr+"")
	WebElement promoBanner;
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;
	
	@FindBy(xpath = ""+promocarousell+"")
	WebElement promocarousel;	

	@FindBy(xpath = ""+headerr+"")
	WebElement header;
		
	@FindBy(xpath = ""+ProductCarousell+"")
	WebElement ProductCarousel;
		
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;		
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement pdpPage;	
		
	@FindBy(xpath = ""+footerBottomCopyrightt+"")
	WebElement footerBottomCopyright;	

	@FindBy(xpath = ""+footerBottomm+"")
	WebElement footerBottom;
	
	@FindBy(xpath = ""+footerSocialIconn+"")
	WebElement footerSocialIcon;
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+footerTopp+"")
	WebElement footerTop;	
	
	@FindBy(xpath = ""+footerTopTitlee+"")
	WebElement footerTopTitle;
	
	@FindBy(xpath = ""+footerSubTopp+"")
	WebElement footerSubTop;
	
	@FindBy(xpath = ""+footerSubTopCalll+"")
	WebElement footerSubTopCall;
	
	@FindBy(xpath = ""+footerSubTopEmaill+"")
	WebElement footerSubTopEmail;
	
	@FindBy(xpath = ""+footerMenuListt+"")
	WebElement footerMenuList;	
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;	
		
	@FindBy(xpath = ""+herobannerActivee+"")
	WebElement herobannerActive;	
	
	@FindBy(how = How.XPATH,using = ""+heroBannerr+"")
	List<WebElement> heroBanner;	
			
	@FindBy(how = How.XPATH,using = ""+advertiseBannerr+"")
	List<WebElement> advertiseBanner;	
	
	@FindBy(how = How.XPATH,using = ""+productCarouselListt+"")
	List<WebElement> productCarouselList;	
	
	@FindBy(how = How.XPATH,using = ""+advertisementBannerr+"")
	List<WebElement> advertisementBannerList;	
	
	
	
	/*HUDSONBAY-74 Verify that home page should be displayed as per the creative and fit to the screen for both landscape and portrait*/
	public void homepageCreative() throws Exception
		{
			ChildCreation("HUDSONBAY-74 Verify that home page should be displayed as per the creative and fit to the screen for both landscape and portrait");
			try
			{
				if(HBCBasicfeature.isElementPresent(promoBanner))
				{
					log.add("Promo banner displayed in the home page");						
				}
				else
				{
					log.add("Promo banner not displayed in the home page");
				}
				if(HBCBasicfeature.isElementPresent(header))
				{
					log.add("Header section displayed in the home page");						
				}
				else
				{
					log.add("Header section not displayed in the home page");
				}
				if(HBCBasicfeature.isListElementPresent(heroBanner))
				{
					log.add("Hero banner displayed in the home page");						
				}
				else
				{
					log.add("Hero banner not displayed in the home page");
				}
				
				/*String expected = HBCBasicfeature.getExcelVal("HB84", sheet, 1);
				wait.until(ExpectedConditions.visibilityOf(signupEmails));
				if(HBCBasicfeature.isElementPresent(signupEmails))
				{
					String actual = signupEmails.getText();		
					if(actual.equalsIgnoreCase(expected))
					{
						log.add("Signup for daily Emails button displayed as per the creative");	
					}					
					else
					{
						log.add("Signup for daily Emails button not displayed as per the creative");						
					}
				}
				else
				{
					Fail("Signup for daily Emails button not displayed");
				}*/
				wait.until(ExpectedConditions.visibilityOf(footerTop));
				if(HBCBasicfeature.isElementPresent(footerTop))
				{
					log.add("Footer top panel is displayed");	
					wait.until(ExpectedConditions.visibilityOf(footerTopTitle));
					if(HBCBasicfeature.isElementPresent(footerTopTitle))
					{
						log.add("Footer top panel title is displayed");	
					}
					else
					{
						log.add("Footer top panel title is not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerSubTop));
					if(HBCBasicfeature.isElementPresent(footerSubTop))
					{
						log.add("Footer sub top panel is displayed");	
						wait.until(ExpectedConditions.visibilityOf(footerSubTopCall));
						if(HBCBasicfeature.isElementPresent(footerSubTopCall))
						{
							log.add("Footer call option is displayed");		
						}
						else
						{
							log.add("Footer Call option is not displayed");						
						}
						wait.until(ExpectedConditions.visibilityOf(footerSubTopEmail));
						if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
						{
							log.add("Footer Email option is displayed");	
						}
						else
						{
							log.add("Footer email option is not displayed");						
						}
					}
					else
					{
						Fail("Footer sub top panel is not displayed");						
					}
				}
				else
				{
					Fail("Footer top panel is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					log.add("Footer Menu List option is displayed");
				}
				else
				{
					log.add("Footer menu list is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerBottom));
				if(HBCBasicfeature.isElementPresent(footerBottom))
				{
					log.add("Footer bottom panel is displayed");
					wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
					if(HBCBasicfeature.isElementPresent(footerSocialIcon))
					{
						log.add("Footer social icon options are displayed");
					}
					else
					{
						log.add("Footer social icon options are not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{						
						log.add("Footer copyright section is displayed");	
						Pass("Home page displayed as per the creative",log);
							
					}
					else
					{
						log.add("Footer copyright section is not displayed");							
						Fail("Home page not displayed as per the creative",log);
					}
				}
				else
				{
					Fail("Footer bottom panel is not displayed");						
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/* HUDSONBAY-75 Verify that promo banner should be displayed in the home page*/
	public void PromoBanner()
		{
			ChildCreation("HUDSONBAY-75 Verify that promo banner should be displayed in the home page");
			try
			{				
				//wait.until(ExpectedConditions.visibilityOf(promoBanner));
				if(HBCBasicfeature.isElementPresent(promocarousel))
				{
					log.add("Promo carousel displayed in the banner");						
				}
				else
				{
					log.add("Promo carousel not displayed in the banner");
				}
				if(HBCBasicfeature.isElementPresent(promoBanner))
				{
					Pass("Promo banner displayed in the home page",log);						
				}
				else
				{
					Fail("Promo banner not displayed in the home page",log);
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}	
	
	/*HUDSONBAY-76 Verify that header should be displayed with icons in the home page*/
	public void headerIcons()
		{
			ChildCreation("HUDSONBAY-76 Verify that header should be displayed with icons in the home page");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));			
				if(HBCBasicfeature.isElementPresent(header))
				{
					wait.until(ExpectedConditions.visibilityOf(hamburger));			
					if(HBCBasicfeature.isElementPresent(hamburger))
					{
						Pass("Hamburger menu displayed in the header");
					}
					else
					{
						Fail("Hamburger menu not displayed in the header");
					}
					wait.until(ExpectedConditions.visibilityOf(logo));			
					if(HBCBasicfeature.isElementPresent(logo))
					{
						Pass("Logo displayed in the header");
					}
					else
					{
						Fail("Logo not displayed in the header");
					}
					wait.until(ExpectedConditions.visibilityOf(searchIcon));			
					if(HBCBasicfeature.isElementPresent(searchIcon))
					{
						Pass("Search Icon displayed in the header");
					}
					else
					{
						Fail("Search Icon not displayed in the header");
					}
					wait.until(ExpectedConditions.visibilityOf(bagIcon));			
					if(HBCBasicfeature.isElementPresent(bagIcon))
					{
						log.add("Header contains hamburger, logo,search and shopping bag icons");
						Pass("Shopping bag Icon displayed in the header",log);
					}
					else
					{
						Fail("Shopping bag Icon not displayed in the header",log);
					}
				}
				else
				{
					Fail("Header not displayed in the page");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-77 verify that page is longer than the device height, the user should be able to scroll the page.*/
	public void pageScrolling()
		{
			ChildCreation("HUDSONBAY-77 verify that page is longer than the device height, the user should be able to scroll the page.");
			try
			{				
				if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
				{
					HBCBasicfeature.scrolldown(footerBottomCopyright, driver);
					log.add("User can able to scroll down the page");
					HBCBasicfeature.scrollup(logo, driver);
					log.add("User can able to scroll Up the page");
					Pass("Page is longer than the device height, the user able to scroll the page",log);
				}
				else
				{
					Fail("The user cant able to scroll the page");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-84 Verify that 'SIGN UP OF DAILY EMAILS' button should be displayed*/
	public void signupEmails() throws java.lang.Exception
		{
			String expected = HBCBasicfeature.getExcelVal("HB84", sheet, 1);	
			ChildCreation("HUDSONBAY-84 Verify that 'SIGN UP OF DAILY EMAILS' button should be displayed");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(signupEmails));
				if(HBCBasicfeature.isElementPresent(signupEmails))
				{
					String actual = signupEmails.getText();		
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("Signup for daily Emails button displayed as per the creative");	
					}					
					else
					{
						Fail("Signup for daily Emails button not displayed as per the creative");						
					}
				}
				else
				{
					Fail("Signup for daily Emails button not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
		}
	
	/*HUDSONBAY-91 Verify that footer should be displayed with all the links and icons in the home page*/
	public void footerCreative()
		{
			ChildCreation("HUDSONBAY-91 Verify that footer should be displayed with all the links and icons in the home page");
			try
			{
				HBCBasicfeature.scrolldown(footerContainer, driver);
				/*wait.until(ExpectedConditions.visibilityOf(signupEmails));
				if(HBCBasicfeature.isElementPresent(signupEmails))
				{
					log.add("Signup for daily Emails option is displayed");	
				}
				else
				{
					log.add("Signup for daily Emails option is not displayed");						
				}			*/
				wait.until(ExpectedConditions.visibilityOf(footerTop));
				if(HBCBasicfeature.isElementPresent(footerTop))
				{
					Pass("Footer top panel is displayed");	
					wait.until(ExpectedConditions.visibilityOf(footerTopTitle));
					if(HBCBasicfeature.isElementPresent(footerTopTitle))
					{
						log.add("Footer top panel title is displayed");	
					}
					else
					{
						log.add("Footer top panel title is not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerSubTop));
					if(HBCBasicfeature.isElementPresent(footerSubTop))
					{
						Pass("Footer sub top panel is displayed");	
						wait.until(ExpectedConditions.visibilityOf(footerSubTopCall));
						if(HBCBasicfeature.isElementPresent(footerSubTopCall))
						{
							log.add("Footer call option is displayed");		
						}
						else
						{
							log.add("Footer Call option is not displayed");						
						}
						wait.until(ExpectedConditions.visibilityOf(footerSubTopEmail));
						if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
						{
							log.add("Footer Email option is displayed");	
						}
						else
						{
							log.add("Footer email option is not displayed");						
						}
					}
					else
					{
						Fail("Footer sub top panel is not displayed");						
					}
				}
				else
				{
					Fail("Footer top panel is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					log.add("Footer Menu List option is displayed");
				}
				else
				{
					log.add("Footer menu list is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerBottom));
				if(HBCBasicfeature.isElementPresent(footerBottom))
				{
					Pass("Footer bottom panel is displayed");
					wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
					if(HBCBasicfeature.isElementPresent(footerSocialIcon))
					{
						log.add("Footer social icon options are displayed");
					}
					else
					{
						log.add("Footer social icon options are not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{						
						log.add("Footer copyright section is displayed");	
						Pass("Footer displayed as per the creative and fit to the screen",log);
							
					}
					else
					{
						log.add("Footer copyright section is not displayed");							
						Fail("Footer is not displayed as per the creative and fit to the screen",log);
					}
				}
				else
				{
					Fail("Footer bottom panel is not displayed");						
				}							
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}			
	
	/*HUDSONBAY-79 Verify that hero banner and advertisement are displayed as per the studio authored content*/
	public void herobanner()
		{
			ChildCreation("HUDSONBAY-79 Verify that hero banner and advertisement are displayed as per the studio authored content");
			try
			{
				if(HBCBasicfeature.isListElementPresent(heroBanner))
				{				
					int size = heroBanner.size();
					log.add("Total Displayed Hero Banner is: "+size);
					Pass("Hero banner displayed in the home page",log);						
				}
				else
				{
					Fail("Hero banner not displayed in the home page");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}

	/*HUDSONBAY-80 Verify that list of product image carousel are displayed as per studio authored*/
	public void productCarousel()
		{
			ChildCreation("HUDSONBAY-80 Verify that list of product image carousel are displayed as per studio authored");
			try
			{
				if(HBCBasicfeature.isElementPresent(ProductCarousel))
				{
					HBCBasicfeature.scrollup(ProductCarousel, driver);
					int size = productCarouselList.size();
					log.add("Total Product image carousel displayed in the homepage is: "+size);
					Pass("Product Carousel displayed in the home page",log);						
				}
				else
				{
					Fail("Product Carousel not displayed in the home page",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}

	/*HUDSONBAY-304 Verify that on tapping the hero banners, user should navigate to the respective page*/
	public void heroBannerClick()
		{
			ChildCreation("HUDSONBAY-304 Verify that on tapping the hero banners, user should navigate to the respective page");
			try
			{	
				Random r = new Random();
				if(HBCBasicfeature.isListElementPresent(heroBanner))
				{				
					int size = heroBanner.size();
					log.add("Total Displayed Hero Banner is: "+size);
					Pass("Hero banner displayed in the home page",log);						
					int i = r.nextInt(size);					
					WebElement hero = driver.findElement(By.xpath("//*[@widgettype='hbcHeroWidget']["+(i+1)+"]"));
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(hero));
					if(HBCBasicfeature.isElementPresent(hero))
					{
						HBCBasicfeature.scrollup(hero, driver);
						Thread.sleep(1000);
						hero.click();
						Thread.sleep(2000);
						wait.until(ExpectedConditions.visibilityOf(logo));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							Pass("On tapping the hero banners, user navigates to the respective PLP page");
						}
						else if(HBCBasicfeature.isElementPresent(pdpPage))
						{
							Pass("On tapping the hero banners, user navigates to the respective PDP page");
						}
						else
						{
							Skip("On tapping the hero banners, user not navigates to the respective PLP/PDP page");
						}							
					}
					else
					{
						Fail("Selected hero banner not displayed");
					}								
				}
				else
				{
					Fail("Hero banner not displayed in the home page");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
}

