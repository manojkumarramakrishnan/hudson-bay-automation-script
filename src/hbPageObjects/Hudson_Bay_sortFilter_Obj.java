package hbPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_SortFilter;

public class Hudson_Bay_sortFilter_Obj extends Hudson_Bay_SortFilter implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_sortFilter_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}	
	
		// Pancake To PLP
		@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id_1_0']")
		WebElement pancakemenu;
		
		@FindBy(xpath = "//*[@id='sk_mobCategoryItem_id_1_1'][2]")
		WebElement pancakemenuCategory;
			
	@FindBy(xpath = ""+plpItemContt+"")
	WebElement plpItemCont;			
		
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;	
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+loadingbarr+"")
	WebElement loadingbar;
	
	@FindBy(xpath = ""+FilterCountt+"")
	WebElement FilterCount;	
	
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+sortt+"")
	WebElement sort;	
	
	@FindBy(xpath = ""+RefineHeaderr+"")
	WebElement RefineHeader;	
			
	
	@FindBy(xpath = ""+Refinewindoww+"")
	WebElement Refinewindow;	
	
	@FindBy(xpath = ""+RefinewindowClearAlll+"")
	WebElement RefinewindowClearAll;		
	
	@FindBy(xpath = ""+RefinewindowApplyy+"")
	WebElement RefinewindowApply;		
	
	@FindBy(xpath = ""+RefinewindowClosee+"")
	WebElement RefinewindowClose;	
	
	@FindBy(xpath = ""+Filterr+"")
	WebElement Filter;
	
	@FindBy(xpath = ""+filterOptionSelectetickk+"")
	WebElement filterOptionSelectetick;	
	
	@FindBy(xpath = ""+filterOptionSelectedd+"")
	WebElement filterOptionSelected;		
	
	@FindBy(xpath = ""+sortByOpenn+"")
	WebElement sortByOpen;
	
	@FindBy(xpath = ""+sortOptionSelectedd+"")
	WebElement sortOptionSelected;
	
	@FindBy(xpath = ""+sortOptionSelectetickk+"")
	WebElement sortOptionSelectetick;	
	
	@FindBy(xpath = ""+sortOptionBoldd+"")
	WebElement sortOptionBold;	
	
	@FindBy(xpath = ""+sortByArroww+"")
	WebElement sortByArrow;	
		
	@FindBy(xpath = ""+sortByOpenDomm+"")
	WebElement sortByOpenDom;	
			
	@FindBy(xpath = ""+sortByy+"")
	WebElement sortBy;
	
	@FindBy(xpath = ""+sortByOptionn+"")
	WebElement sortByOption;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+otherlanguagee+"")
	WebElement otherlanguage;
	
	
	@FindBy(how = How.XPATH,using = ""+FilterAttributess+"")
	List<WebElement> FilterAttributes;
	
	@FindBy(how = How.XPATH,using = ""+sortOptionss+"")
	List<WebElement> sortOptionsList;
	
	@FindBy(how = How.XPATH,using = ""+sortOptionSelectedd+"")
	List<WebElement> sortOptionSelectedList;
	
	@FindBy(how = How.XPATH,using = ""+sortOptionNOTSelectedd+"")
	List<WebElement> sortOptionNOTSelected;
	
	@FindBy(how = How.XPATH,using = ""+sortOptionNOTSelecteddd+"")
	List<WebElement> sortOptionNOTSelectedddd;	
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	@FindBy(how = How.XPATH,using = ""+filterOptionSelectetickk+"")
	List<WebElement> filterOptionSelectetickList;
	
		
	String plpCount = "";
	
	String refCount = "";
	
		
	public void plp() throws Exception
	{
		ChildCreation("PLP Page");
		String key = HBCBasicfeature.getExcelVal("searchkey", sheet, 1);
		try
		{
/*			try
			{
				Thread.sleep(1000);
				wait.until(ExpectedConditions.elementToBeClickable(hamburger));	
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					hamburger.click();
					wait.until(ExpectedConditions.visibilityOf(pancakemenu));
					if(HBCBasicfeature.isElementPresent(pancakemenu))
					{
						pancakemenu.click();
						wait.until(ExpectedConditions.visibilityOf(pancakemenuCategory));
						if(HBCBasicfeature.isElementPresent(pancakemenuCategory))
						{
							pancakemenuCategory.click();
							boolean pgLoad1 = false;
							do
							{				
								try
								{
									Thread.sleep(1000);
									if(HBCBasicfeature.isElementPresent(plpPage))		
									{
										pgLoad1=true;	
										break;		
									}
								}
								catch (Exception e1)
								{
									pgLoad1=false;
									driver.navigate().refresh();	
									continue;
								}
							}
							while(!pgLoad1==true);
							if(HBCBasicfeature.isElementPresent(plpPage))
							{
								Pass("PLP Page Displayed");
							}
							else
							{
								Fail("Selecting the sub-categories in the pancake menu,the selected category PLP page not displayed");
							}							
						}
						else
						{
							Fail("Pancake Menu category not displayed");
						}
					}
					else
					{
						Fail("Pancake menu not displayed");
					}				
				}
				else
				{
					Fail("Hamburger not displayed");
				}			
			}*/
		/*	catch (Exception e)
			{
				System.out.println(e.getMessage());
				*/Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));			
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					action.moveToElement(searchIcon).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(searchBox));			
					if(HBCBasicfeature.isElementPresent(searchBox))
					{											
						searchBox.clear();
						searchBox.sendKeys(key);
						searchBox.sendKeys(Keys.ENTER);
						wait.until(ExpectedConditions.visibilityOf(plpPage));			
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							Pass("Selecting the enter button in virtual keyboard after entering valid keyword,the search results page displayed");
						}
						else
						{
							Fail("Selecting the enter button in virtual keyboard after entering valid keyword,the search results page displayed");
						}
					}
					else
					{
						Fail("Search Box not displayed");
					}						
				}
				else
				{
					Fail("Search Icon not displayed");
				}				
			/*}*/
			
			//Selecting PLP Items count
			
			if(HBCBasicfeature.isElementPresent(plpItemCont))
			{
				String val = plpItemCont.getText();
				String[] split = val.split(" ");
				plpCount = split[0]; 
			}
			else
			{
				Skip("Product count not displayed");
			}		
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}
	}
	
	/*HUDSONBAY-355 Verify that while selecting the sort/filter options then "Refine" page should be displayed*/
	public void plpSort_Refine()
		{
			ChildCreation("HUDSONBAY-355 Verify that while selecting the sort/filter options then 'Refine' page should be displayed");
			try
			{
				Actions action = new Actions(driver);
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					HBCBasicfeature.jsclick(sort, driver);					
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						Pass("On selecting Sort option then 'Refine' page displayed ");
						RefinewindowClose.click();						
					}
					else
					{
						Fail("On selecting Sort option then 'Refine' page not displayed ");
					}					
				}
				else
				{
					Fail("Sort option not displayed");
				}
				
				wait.until(ExpectedConditions.visibilityOf(Filter));
				if(HBCBasicfeature.isElementPresent(Filter))
				{
					action.moveToElement(Filter).click().build().perform();
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						Pass("On selecting Filter option then 'Refine' page displayed ");
						//RefinewindowClose.click();
					}
					else
					{
						Fail("On selecting Filter option then 'Refine' page not displayed ");
					}					
				}
				else
				{
					Fail("Filter option not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-357 Verify that header should be hidden when the sort/filter drawer is in enabled state*/
	public void plpRefine_HeaderDisabled()
		{
			ChildCreation("HUDSONBAY-357 Verify that header should be hidden when the sort/filter drawer is in enabled state");
			try
			{				
				wait.until(ExpectedConditions.visibilityOf(Refinewindow));
				if(HBCBasicfeature.isElementPresent(Refinewindow))
				{
					try
					{	
						logo.click();
						Fail("Header not hidden when the sort/filter drawer is in enabled state");
					}
					catch (Exception e)
					{
						Pass("Header not displayed when the sort/filter drawer is in enabled state");
					}
				}
				else
				{
					Fail("Refine window not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-358/687/688 Verify that "Apply" text link should be shown in the center of the drawer*/
	public void plpRefine_Apply() throws Exception
		{
			ChildCreation("HUDSONBAY-358 Verify that 'Apply' text link  should be shown in the center of the drawer");
			boolean flag = false;
			String refApply= HBCBasicfeature.getExcelVal("HB358", sheet, 1);
			try
			{
				wait.until(ExpectedConditions.visibilityOf(Refinewindow));
				if(HBCBasicfeature.isElementPresent(RefinewindowApply))
				{
					String title = RefinewindowApply.getText();
					if(title.equalsIgnoreCase(refApply))
					{
						log.add("Text displayed is: "+title);
						Pass("'Apply' text link shown in the center of the drawer",log);
						flag = true;
					}
					else
					{
						Fail("'Apply' text link not shown in the center of the drawer",log);
					}
				}
				else
				{
					Fail("Refine window not displayed",log);
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-687 Verify that 'Apply' button should be shown in the top of the sort/filter page");
			if(flag==true)
			{
				Pass("'Apply' button shown in the top of the sort/filter page");
			}
			else
			{
				Fail("'Apply' button not shown in the top of the sort/filter page");
			}
			
			ChildCreation("HUDSONBAY-688 Verify that 'Apply' button should be in disabled state before selecting the sort/filter options");
			if(flag==true)
			{
				log.add("Filter 'Apply' button displayed");
				try
				{						
					RefinewindowApply.click();		
					logo.click();
					Fail("'Apply' button not in disabled state before selecting the sort/filter options",log);
				}
				catch(Exception e)
				{
					Pass("'Apply' button in disabled state before selecting the sort/filter options",log);
				}
			}
			else
			{
				Fail("'Apply' button not shown in the top of the sort/filter page");
			}			
		}
	
	/*HUDSONBAY-370 Verify that "clear All" button should be shown in the top right corner of the drawer*/
	public void plpRefine_ClearAll() throws Exception
		{
			ChildCreation("HUDSONBAY-370 Verify that 'clear All' button should be shown in the top right corner of the drawer");
			String clear= HBCBasicfeature.getExcelVal("HB370", sheet, 1);
			try
			{
				wait.until(ExpectedConditions.visibilityOf(Refinewindow));
				if(HBCBasicfeature.isElementPresent(RefinewindowClearAll))
				{
					String text = RefinewindowClearAll.getText();
					if(text.equalsIgnoreCase(clear))
					{
						log.add("Text displayed is: "+text);
						Pass("'clear All' button shown in the top right corner of the drawer",log);					
					}
					else
					{
						Fail("'clear All' button not shown in the top right corner of the drawer",log);
					}
				}
				else
				{
					Fail("clear All button not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-359 Verify that Close "X" icon should be shown in the top left corner of the drawer*/
	public void plpRefine_close()
		{
			ChildCreation("HUDSONBAY-359 Verify that Close X icon should be shown in the top left corner of the drawer");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(RefinewindowClose));
				if(HBCBasicfeature.isElementPresent(RefinewindowClose))
				{
					Pass("Close X icon shown in the top left corner of the drawer");
				}
				else
				{
					Fail("Close X icon not shown in the top left corner of the drawer");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-360 Verify while selecting the Close "X" icon then the sort/filter drawer should be closed(i.e. moves from top to bottom)*/
	public void plpRefine_CloseClick()
		{
			ChildCreation("HUDSONBAY-360 Verify while selecting the Close X icon then the sort/filter drawer should be closed(i.e. moves from top to bottom)");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(RefinewindowClose));
				if(HBCBasicfeature.isElementPresent(RefinewindowClose))
				{
					RefinewindowClose.click();
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						Fail("Selecting the Close X icon then the sort/filter drawer not closed");
					}
					else
					{
						Pass("Selecting the Close X icon then the sort/filter drawer closed");
					}					
				}
				else
				{
					Fail("Refine close icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}	
	
	/*HUDSONBAY-361 Verify that on selecting the filter button the different types of filter attributes and the values should be displayed*/
	public void filterOptions() throws Exception
		{
			ChildCreation("HUDSONBAY-361 Verify that on selecting the filter button the different types of filter attributes and the values should be displayed");
			String key = HBCBasicfeature.getExcelVal("HB361", sheet, 2);
			String[] filter = key.split("\n");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(Refinewindow));
				if(HBCBasicfeature.isElementPresent(Refinewindow))
				{
					wait.until(ExpectedConditions.visibilityOf(sortBy));
					if(HBCBasicfeature.isElementPresent(sortBy))
					{
						String actual_txt = sortBy.getText();
						if(filter[0].equalsIgnoreCase(actual_txt))
						{
							Pass("Sort By option displayed",log);
						}						
					}
					else
					{
						Fail("Sort By option not displayed",log);
					}
					
					if(HBCBasicfeature.isListElementPresent(FilterAttributes))
					{
						int size = FilterAttributes.size();
						for(int i=0;i<size;i++)
						{
							String attribute = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]")).getText();
							//System.out.println(attribute);
							//System.out.println(filter[(i+1)]);
							if(filter[(i+1)].equalsIgnoreCase(attribute))
							{
								Pass("Filter attribute "+ attribute +" displayed");
							}
							else
							{
								Fail("Filter attribute "+ attribute +" not displayed");
							}							
						}
					}
					else
					{
						Fail("Filter attributes not displayed");
					}					
				}
				else
				{
					Fail("Sort & Filter window not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-362/681/683 Verify that user can't able to select multiple "Sort By" options of his choice in the refine panel*/
	public void sortOptionSelected()
		{
			boolean flag= false;
			ChildCreation("HUDSONBAY-362 Verify that user can't able to select multiple 'Sort By' options of his choice in the refine panel");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(sortByOpen));
				if(HBCBasicfeature.isListElementPresent(sortOptionsList))
				{
					int size = sortOptionsList.size();
					for(int i=1;i<=size;i++)
					{
						WebElement sortOptions = driver.findElement(By.xpath("//*[@id='sk_sortOptionContainer_id']["+i+"]"));
						wait.until(ExpectedConditions.visibilityOf(sortOptions));
						if(HBCBasicfeature.isElementPresent(sortOptions))
						{
							sortOptions.click();
							wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
						}
						else
						{
							Fail("Selected sort option not displayed");
						}
					}
					int act_size = sortOptionSelectedList.size();
					if(act_size==1)
					{
						String selectedSort = sortOptionSelected.getText();
						log.add("The selected Sort option is: "+selectedSort);
						Pass("User can't able to select multiple 'Sort By' options of his choice in the refine panel",log);
						flag= true;
					}
					else
					{
						log.add("Totall selected sort options are: "+act_size);
						Fail("User can able to select multiple 'Sort By' options of his choice in the refine panel",log);
					}					
				}
				else
				{
					Fail("Sort options not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-681 Verify that user should able to select only one sorting option in the sort/filter page");
			if(flag==true)
			{
				Pass("User able to select only one sorting option in the sort/filter page");
			}
			else
			{
				Fail("User not able to select only one sorting option in the sort/filter page");
			}
			
			ChildCreation("HUDSONBAY-683 Verify that multiple sorting option should not get selected");
			if(flag==true)
			{
				Pass("Multiple sorting option not get selected");
			}
			else
			{
				Fail("Multiple sorting option get selected");
			}
		}
	
	/*HUDSONBAY-682 Verify that selected sorting option should be changed when the user selects other option*/
	public void sortOptionChange()
		{
			ChildCreation("HUDSONBAY-682 Verify that selected sorting option should be changed when the user selects other option");
			try
			{				
				Random r = new Random();
				wait.until(ExpectedConditions.visibilityOf(sortByOpen));
				if(HBCBasicfeature.isListElementPresent(sortOptionsList))
				{
					String selectedSort = sortOptionSelected.getText();
					log.add("Already selected option: "+selectedSort);
					int size = sortOptionsList.size();
					int i = r.nextInt(size);	
					if(i==0)
					{
						i=i+1;
					}
					WebElement sortOptions = driver.findElement(By.xpath("//*[@id='sk_sortOptionContainer_id']["+i+"]"));
					Thread.sleep(500);
					String txt = sortOptions.getText();
					if(txt.equalsIgnoreCase(selectedSort))
					{
						i--;
					}
					WebElement sortOption = driver.findElement(By.xpath("//*[@id='sk_sortOptionContainer_id']["+i+"]"));
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(sortOption));
					if(HBCBasicfeature.isElementPresent(sortOption))
					{
						sortOption.click();
						wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));					
						String selectedCurrentSort = sortOptionSelected.getText();
						log.add("Current selected option is: "+selectedCurrentSort);
						if(selectedSort.equalsIgnoreCase(selectedCurrentSort))
						{
							Fail("Selected sorting option not changed when the user selects other option",log);
						}
						else
						{
							Pass("Selected sorting option changed when the user selects other option",log);
						}
					}
				else
				{
					Fail("Selected sort option not displayed");
				}
			}				
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-363 Verify that blue tick mark should be shown for selected filters at the right corner in the "Refine" panel*/
	public void sortOptionTick()
		{
			ChildCreation("HUDSONBAY-363 Verify that blue tick mark should be shown for selected filters at the right corner in the 'Refine' panel");
					try
					{
						wait.until(ExpectedConditions.visibilityOf(sortByOpen));
						if(HBCBasicfeature.isListElementPresent(sortOptionsList))
						{
							String selectedSort = sortOptionSelected.getText().replaceAll("\\s+", "");
							log.add("Selected sort option is: "+selectedSort);
							if(HBCBasicfeature.isElementPresent(sortOptionSelectetick))
							{
								String tickOption = sortOptionSelectetick.getAttribute("class").substring(19).replace("sk_selectedSort", "").trim();
								log.add("Selected Tick mark sort option is: "+tickOption);
								if(selectedSort.equalsIgnoreCase(tickOption))
								{
									Pass("Blue tick mark shown for selected filters at the right corner in the 'Refine' panel",log);
								}
								else
								{
									Fail("Blue tick mark not shown for selected filters at the right corner in the 'Refine' panel",log);
								}
							}
							else
							{
								Fail("Tick Mark not displayed");
							}						
						}
						else
						{
							Fail("Sort options are not displayed");
						}
						
					}
					catch (Exception e)
					{
						System.out.println(e.getMessage());
						Exception("There is something wrong. Please Check." + e.getMessage());
					}			
				}
		
	/*HUDSONBAY-364/695 Verify that selected "Sort By" filter options should be highlighted in bold*/
	public void sortOptionBold() throws Exception
				{
					ChildCreation("HUDSONBAY-364 Verify that selected 'Sort By' filter options should be highlighted in bold");
					String expctd = HBCBasicfeature.getExcelVal("HB364", sheet, 3);
					boolean flag = false;
					try
					{
						log.add("The Expected color is: "+expctd);
						wait.until(ExpectedConditions.visibilityOf(sortByOpen));							
						if(HBCBasicfeature.isElementPresent(sortOptionBold))
						{
							String clr = sortOptionBold.getCssValue("color");
							String act_Color = HBCBasicfeature.colorfinder(clr);
							log.add("The Actual color is: "+act_Color);
							Thread.sleep(200);
							if(act_Color.equalsIgnoreCase(expctd))
							{
								Pass("Selected 'Sort By' filter options highlighted in bold",log);
								flag= true;
							}
							else
							{
								Fail("Selected 'Sort By' filter options not highlighted in bold",log);
							}							
						}
						else
						{
							Fail("Sort option is not bold");
						}												
					}
					catch (Exception e)
					{
						System.out.println(e.getMessage());
						Exception("There is something wrong. Please Check." + e.getMessage());
					}
					
					ChildCreation("HUDSONBAY-695 Verify that selected 'Sort By' filter options should be highlighted in bold");
					if(flag==true)
					{
						Pass("Selected 'Sort By' filter options highlighted in bold");
					}
					else
					{
						Fail("Selected 'Sort By' filter option not highlighted in bold");
					}				
				}
	
	/*HUDSONBAY-369 Verify that unselected sort by options should be shown in greyed out color*/
	public void sortOptionUnselected() throws Exception
	{
		ChildCreation("HUDSONBAY-369 Verify that unselected sort by options should be shown in greyed out color");
		String expctd = HBCBasicfeature.getExcelVal("HB359", sheet, 3);
		WebElement sortOption = null;
		try
		{
			log.add("The Expected color is: "+expctd);
			wait.until(ExpectedConditions.visibilityOf(sortByOpen));
			if(HBCBasicfeature.isListElementPresent(sortOptionNOTSelected)||HBCBasicfeature.isListElementPresent(sortOptionNOTSelectedddd))
			{	
				int size = 0; 			
				if(HBCBasicfeature.isListElementPresent(sortOptionNOTSelected))
				{
					size = sortOptionNOTSelected.size();
				}
				else if(HBCBasicfeature.isListElementPresent(sortOptionNOTSelectedddd))
				{
					size = sortOptionNOTSelectedddd.size();
				}
				else
				{
					Fail("Changes in unselected sort by options DOM ");
				}
				for(int i=1;i<=size;i++)
				{
					try
					{
						sortOption = driver.findElement(By.xpath("(//*[@class='sk_sortOptionContainer ']//*[@class='skRes_sortOption'])["+i+"]"));
					}
					catch(Exception e)
					{
						sortOption = driver.findElement(By.xpath("(//*[@class='sk_sortOptionContainer']//*[@class='skRes_sortOption'])["+i+"]"));
					}
					Thread.sleep(500);
					String sortOptionClr = sortOption.getCssValue("color");
					String act_Color = HBCBasicfeature.colorfinder(sortOptionClr);
					log.add("The Actual color is: "+act_Color);
					Thread.sleep(200);
					if(act_Color.equalsIgnoreCase(expctd))
					{
						Pass("unselected 'Sort By' filter option "+sortOption.getText()+" shown in greyed out color",log);
					}
					else
					{
						Fail("unselected 'Sort By' filter option "+sortOption.getText()+" not shown in greyed out color",log);
					}							
				}
			}
			else
			{
				Fail("Not selected sort by options not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-690 Verify that on selecting the sorting options and tapping the "X" close button, the PLP page should not get updated*/
	public void sort_not_selected()
		{
			ChildCreation("HUDSONBAY-690 Verify that on selecting the sorting options and tapping the 'X' close button, the PLP page should not get updated");
			try
			{
				String selectedSort = sortOptionSelected.getText();
				if(!selectedSort.isEmpty())
				{
					log.add("Selected Sort option is: "+selectedSort);					
					if(HBCBasicfeature.isElementPresent(RefinewindowClose))
					{
						RefinewindowClose.click();
						wait.until(ExpectedConditions.visibilityOf(plpItemCont));
						if(HBCBasicfeature.isElementPresent(plpItemCont))
						{
							String val = plpItemCont.getText();
							String[] split = val.split(" ");
							String aftrPlpCount = split[0]; 
							log.add("Before count: "+plpCount);
							log.add("After count: "+aftrPlpCount);
							Thread.sleep(500);
							if(aftrPlpCount.contentEquals(plpCount))
							{
								Pass("on selecting the sorting options and tapping the 'X' close button, the PLP page not get updated",log);
							}
							else
							{
								Fail("on selecting the sorting options and tapping the 'X' close button, the PLP page get updated",log);
							}								
						}
						else
						{
							Fail("PLP Item count not displayed");
						}							
					}
					else
					{
						Fail("Close option not displayed");
					}
				}
				else
				{
					Fail("Sort option is not selected");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
			
	/*HUDSONBAY-691/689 Verify that on selecting the sorting/filter options and tapping the "Apply" button, the PLP page should get updated*/
	public void sortApply()
		{
			ChildCreation("HUDSONBAY-691 Verify that on selecting the sorting/filter options and tapping the 'Apply' button, the PLP page should get updated");
			boolean apply = false;
			try
			{
				Random r = new Random();
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					HBCBasicfeature.jsclick(sort, driver);					
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));
					
					//Selecting Sort option
					int size = sortOptionsList.size();
					int i = r.nextInt(size);
					if(i==0)
					{
						i=i+1;
					}
					WebElement sortOptions = driver.findElement(By.xpath("//*[@id='sk_sortOptionContainer_id']["+i+"]"));
					sortOptions.click();
					if(HBCBasicfeature.isElementPresent(RefinewindowApply))
					{
						HBCBasicfeature.scrollup(RefinewindowApply, driver);
						RefinewindowApply.click();
						wait.until(ExpectedConditions.visibilityOf(plpItemCont));
						if(HBCBasicfeature.isElementPresent(plpItemCont))
						{
							apply = true;
							String val = plpItemCont.getText();
							String[] split = val.split(" ");
							String aftrPlpCount = split[0]; 
							log.add("Before count: "+plpCount);
							log.add("After count: "+aftrPlpCount);
							Thread.sleep(500);
							if(aftrPlpCount.contentEquals(plpCount))
							{
								Pass("On selecting the sorting options and tapping the 'Apply' button, the PLP page get updated",log);
							}
							else
							{
								Fail("On selecting the sorting options and tapping the 'Apply' button, the PLP page not get updated",log);								
							}
						}
						else
						{
							Fail("Item count not displayed");
						}
					}
					else
					{
						Fail("Apply button not displayed");
					}
				}
				else
				{
					Fail("Sort option not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-689 Verify that 'Apply' button should be enabled on selecting the some sort/filter options");
			if(apply==true)
			{
				Pass("'Apply' button enabled on selecting the some sort/filter options");
			}
			else
			{
				Fail("'Apply' button not enabled on selecting the some sort/filter options");
			}
		}
	
	/*HUDSONBAY-694 Verify that selected sort by option should be shown as "SORT BY:XXXXXX" in the sort/filter page*/
	public void selectedSortName() throws Exception
		{
			ChildCreation("HUDSONBAY-694 Verify that selected sort by option should be shown as 'SORT BY:XXXXXX' in the sort/filter page");
			String key = HBCBasicfeature.getExcelVal("HB361", sheet, 2);
			String[] sortByTxt = key.split("\n");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					HBCBasicfeature.jsclick(sort, driver);					
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));
					String selectedSort = sortOptionSelected.getText();
					if(HBCBasicfeature.isElementPresent(sortBy))
					{
						String expectedSortBy = sortBy.getText();
						if(HBCBasicfeature.isElementPresent(sortByOption))
						{
							String expectedSortOption = sortByOption.getText();							
							if(sortByTxt[0].equalsIgnoreCase(expectedSortBy) && selectedSort.equalsIgnoreCase(expectedSortOption))
							{
								log.add("Actual SortBy text is: "+sortByTxt[0]);
								log.add("Displayed SortBy text is: "+expectedSortBy);
								log.add("Selected Sort option is: "+selectedSort);
								log.add("Displayed Sort option is: "+expectedSortOption);
								Pass("Selected sort by option shown as 'SORT BY:XXXXXX' in the sort/filter page",log);
							}
							else
							{
								log.add("Actual SortBy text is: "+sortByTxt[0]);
								log.add("Displayed SortBy text is: "+expectedSortBy);
								log.add("Selected Sort option is: "+selectedSort);
								log.add("Displayed Sort option is: "+expectedSortOption);
								Fail("Selected sort by option not shown as 'SORT BY:XXXXXX' in the sort/filter page",log);
							}							
						}
						else
						{
							Fail("Sort option not selected");
						}
					}
					else
					{
						Fail("Sort by text not displayed");
					}					
				}
				else
				{
					Fail("Sort option not displayed");
				}
			}				
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-366 Verify that Down arrow/Up arrow should be shown for Open/Close Refine By options */
	public void upDownArrow()
		{
			ChildCreation("HUDSONBAY-366 Verify that Down arrow/Up arrow should be shown for Open/Close Refine By options ");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(sortBy));
				if(HBCBasicfeature.isElementPresent(sortBy))
				{
					log.add("Sort option displayed");
					if(HBCBasicfeature.isElementPresent(sortByArrow))
					{
						Pass("Down arrow/Up arrow displayed near SortBy option", log);
					}	
					else
					{
						Fail("Down arrow/Up arrow not displayed near SortBy option", log);
					}
				}
				else
				{
					Fail("Sort option not displayed");
				}
				
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					int size = FilterAttributes.size();
					for(int i=0;i<size;i++)
					{
						WebElement arrow = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]")).getText().replace(":","");
						if(HBCBasicfeature.isElementPresent(arrow))
						{
							Pass("Down arrow/Up arrow shown for Refine By option: "+filter_Name);
						}
						else
						{
							Fail("Down arrow/Up arrow not shown for Refine By option: "+filter_Name);
						}
					}						
				}
				else
				{
					Fail("Filter options not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-368/382 Verify that Down arrow should be shown for the closed refine by option*/
	public void DownArrowClosedState()
		{
			WebDriverWait wait = new WebDriverWait(driver, 5);
			ChildCreation("HUDSONBAY-368 Verify that Down arrow should be shown for the closed refine by option");
			boolean flag = false;
			try
			{
				//Down Arrow for SortBy option
				try
				{
					wait.until(ExpectedConditions.attributeContains(sortByOpenDom, "style", "none"));	
				}
				catch(Exception e)
				{
					sortByArrow.click();
					wait.until(ExpectedConditions.attributeContains(sortByOpenDom, "style", "none"));	
				}
				if(HBCBasicfeature.isElementPresent(sortByArrow))
				{
					Pass("Down arrow displayed near SortBy option");
				}
				else
				{
					Fail("Down arrow not displayed near SortBy option");
				}			
				
				//Down Arrow for Filter options
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					int size = FilterAttributes.size();
					for(int i=0;i<size;i++)
					{
						flag = false;
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(filter, "style", "display: none"));	
						}
						catch(Exception e)
						{
							WebElement arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							arrow_clk.click();
							wait.until(ExpectedConditions.attributeContains(filter, "style", "display: none"));	
						}
						WebElement arrow = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						Thread.sleep(500);
						String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");
						if(HBCBasicfeature.isElementPresent(arrow))
						{
							flag = true;
							Pass("Down arrow shown for Refine By option: "+filter_Name);
							HBCBasicfeature.scrolldown(arrow, driver);
							arrow.click();
						}
						else
						{
							Fail("Down arrow not shown for Refine By option: "+filter_Name);
						}
					}
				}
				else
				{
					Fail("Filter options not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-382 Verify that initially all the filter/sort options should be in collapsed state");
			if(flag==true)
			{
				Pass("Initially all the filter/sort options in collapsed state");
			}
			else
			{
				Fail("Initially all the filter/sort options not in collapsed state");
			}
		}
	
	/*HUDSONBAY-853 Verify that color difference needs to be shown as per the creative in sort/filter page*/
	public void colorDifference() throws java.lang.Exception
		{
			boolean flag= false; 
			ChildCreation("HUDSONBAY-853 Verify that color difference needs to be shown as per the creative in sort/filter page");
			String Color = HBCBasicfeature.getExcelVal("HB853", sheet, 3);
			String split[] = Color.split("\n");		
			log.add("Actual sort/filter header background color is: "+split[0]);
			log.add("Actual sort/filter option background color is: "+split[1]);
			int i = 0;
			try
			{
				Random r = new Random();
				int size = FilterAttributes.size();
				i = r.nextInt(size);					
				String filter = driver.findElement(By.xpath("(//*[@class='skMob_filterContentWrapper  skMob_filterOpen'])["+(i+1)+"]")).getCssValue("background-color");
				String expctdFilterColor  = HBCBasicfeature.colorfinder(filter);
				log.add("Filter background Color is: "+expctdFilterColor);				
				if(split[0].equalsIgnoreCase(expctdFilterColor))
				{
					String filterOptionColor = driver.findElement(By.xpath("(//*[@class='skMob_filterItemContentWrapper'])["+(i+1)+"]")).getCssValue("background-color");
					String expctdFilterOptionColor = HBCBasicfeature.colorfinder(filterOptionColor);
					log.add("Filter options background Color is: "+expctdFilterOptionColor);
					if(split[1].equalsIgnoreCase(expctdFilterOptionColor))
					{
						Pass("color difference shown as per the creative in filter page",log);
					}
					else
					{
						Fail("color difference not shown as per the creative in filter page",log);
					}
				}
				else
				{
					Fail("color difference not shown as per the creative in filter page",log);
				}				
				
				String sortColor = driver.findElement(By.xpath("//*[@class='skRes_sortSelected']")).getCssValue("background-color");
				String expctdSortColor = HBCBasicfeature.colorfinder(sortColor);
				log.add("Sort background Color displayed is: "+expctdSortColor);
				if(split[0].equalsIgnoreCase(expctdSortColor))
				{					
					String sortOptionBG = sortByOpenDom.getCssValue("background-color");
					String expctdSortoptionColor  = HBCBasicfeature.colorfinder(sortOptionBG);
					log.add("Sort option background color is: "+expctdSortoptionColor);
					if(split[1].equalsIgnoreCase(expctdSortoptionColor))
					{
						Pass("color difference shown as per the creative in Sort page: ",log);
						flag= true;
					}
					else
					{
						Fail("color difference not shown as per the creative in Sort page",log);
					}
					
				}
				else
				{
					Fail("color difference not shown as per the creative in Sort page: ",log);
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-365  Verify that difference needs to be shown for the Sort By and Refine By options");
			if(flag==true)
			{
				Pass("Difference shown for the Sort By and Refine By options");
			}
			else
			{
				Skip("Difference not shown for the Sort By and Refine By options");
			}
		}	
		
	
	/*HUDSONBAY-718 Verify that user should be able to scroll the sort/filter page*/
	public void sort_Filter_Scroll()
		{
			ChildCreation("HUDSONBAY-718 Verify that user should be able to scroll the sort/filter page");
			try
			{
				HBCBasicfeature.scrollup(RefineHeader, driver);
				Pass("User able to scroll the sort/filter page");
			}
			catch (Exception e)
			{
				Fail("User not able to scroll the sort/filter page");
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-367 Verify that Up arrow should be shown for the opened refine by option*/
	public void UpArrowOpenedState()
		{
			WebDriverWait wait = new WebDriverWait(driver, 5);
			ChildCreation("HUDSONBAY-367 Verify that Up arrow should be shown for the opened refine by option");
			try
			{
				//Up Arrow for SortBy option
				try
				{
					wait.until(ExpectedConditions.attributeContains(sortByOpenDom, "style", "block"));	
				}
				catch(Exception e)
				{
					sortByArrow.click();
					wait.until(ExpectedConditions.attributeContains(sortByOpenDom, "style", "block"));	
				}
				if(HBCBasicfeature.isElementPresent(sortByArrow))
				{
					Pass("Up arrow displayed near SortBy option");
					sortByArrow.click();
				}
				else
				{
					Fail("Up arrow not displayed near SortBy option");
				}			
				
				//Up Arrow for Filter options
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					int size = FilterAttributes.size();
					for(int i=0;i<size;i++)
					{
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}
						catch(Exception e)
						{
							WebElement arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							HBCBasicfeature.scrolldown(arrow_clk, driver);
							arrow_clk.click();
						}												
						wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						WebElement arrow = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						Thread.sleep(500);
						String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
						if(HBCBasicfeature.isElementPresent(arrow))
						{
							Pass("Up arrow shown for Refine By option: "+filter_Name);
							HBCBasicfeature.scrolldown(arrow, driver);
							arrow.click();
						}
						else
						{
							Fail("Up arrow not shown for Refine By option: "+filter_Name);
						}						
					}
				}
				else
				{
					Fail("Filter options not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-372 Verify that sort/filter page should not be closed on tapping the "Clear All" button*/
	public void ClearAll_no_close()
		{
			ChildCreation("HUDSONBAY-372 Verify that sort/filter page should not be closed on tapping the ClearAll button");
			try
			{
				wait.until(ExpectedConditions.elementToBeClickable(RefinewindowClearAll));
				if(HBCBasicfeature.isElementPresent(RefinewindowClearAll))
				{
					RefinewindowClearAll.click();
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						Pass("Sort/filter page not closed on tapping the ClearAll button");
					}
					else
					{
						Fail("Sort/filter page closed on tapping the ClearAll button");
					}					
				}
				else
				{
					Fail("Clear All buttoon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-377/374/685 Verify that "View All" option should be selected in default in the sort/filter page*/
	public void FilterViewAll() throws Exception
		{
		boolean child = false;
			WebDriverWait wait = new WebDriverWait(driver, 5);
			String viewall = HBCBasicfeature.getExcelVal("HB377", sheet, 1);
			ChildCreation("HUDSONBAY-377 Verify that 'View All' option should be selected in default in the sort/filter page");
			try
			{
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					int size = FilterAttributes.size();
					for(int i=0;i<size;i++)
					{
						child = false;
						WebElement arrow_clk = null ;
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}
						catch(Exception e)
						{
							arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							HBCBasicfeature.scrolldown(arrow_clk, driver);
							arrow_clk.click();
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}
						Thread.sleep(500);
						String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
						String expctd = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[@class='skMob_filterItems  skMob_filterItemSelected  ']")).getText();
						Thread.sleep(500);
						if(expctd.equalsIgnoreCase(viewall))
						{
							Pass(""+expctd+" option selected in default for refine option: "+filter_Name);
							HBCBasicfeature.scrolldown(arrow_clk, driver);							
							arrow_clk.click();
							child = true;
						}
						else
						{
							log.add("Selected default option is: "+expctd);
							Fail("'View All' option not selected in default for refine option: "+filter_Name,log);
						}						
					}
				}
				else
				{
					Fail("Filter options not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-374 Verify that 'View All' Filter option should be shown under Refine by options, when the product has more number of filter options");
			if(child==true)
			{
				Pass("'View All' Filter option shown under Refine by options, when the product has more number of filter options");
			}
			else
			{
				Fail("'View All' Filter option not shown under Refine by options, when the product has more number of filter options");
			}	
			
			ChildCreation("HUDSONBAY-685 Verify that by default 'View All' option should be selected in the filter sections");
			if(child==true)
			{
				Pass("By default 'View All' option selected in the filter sections");
			}
			else
			{
				Fail("By default 'View All' option not selected in the filter sections");
			}				
		}
	
	/*HUDSONBAY-693 Verify that selected filter count should not be shown for the default sort/filter options like "View All"*/
	public void filterCountEmpty()
		{
			ChildCreation("HUDSONBAY-693 Verify that selected filter count should not be shown for the default sort/filter options like 'View All'");
			try		
			{
				int size = FilterAttributes.size();
				for(int i=0;i<size;i++)
				{
					String filter_Count = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentCount']")).getText();
					boolean res  = filter_Count.isEmpty();
					if(res==true)
					{
						Pass("Filter count not shown for the default sort/filter options like 'View All for the filter: '"+filter_Count);
					}
					else
					{
						Fail("Filter count shown for the default sort/filter options like 'View All' for the filter: "+filter_Count);
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
			
	/*HUDSONBAY-378/380/686/696  Verify that on selecting some filter options in the sort/filter then View All option should be disabled*/
	public void FilterViewAllDisabled() throws java.lang.Exception
		{
		boolean filterslct = false;
		int i =0,j =0;
		WebElement filterSelect = null;
		WebElement arrow_clk = null ;
		String currentFilter ="";
		String filter_Name ="";
		WebDriverWait wait = new WebDriverWait(driver, 10);
		String viewall = HBCBasicfeature.getExcelVal("HB377", sheet, 1);
		ChildCreation("HUDSONBAY-378 Verify that on selecting some filter options in the sort/filter then View All option should be disabled");
			try
			{
				if(HBCBasicfeature.isListElementPresent(FilterAttributes))
				{
					Random r = new Random();
					int size = FilterAttributes.size();
					i = r.nextInt(size);					
						
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}
						catch(Exception e)
						{
							arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							HBCBasicfeature.scrolldown(arrow_clk, driver);
							arrow_clk.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						}	
						Thread.sleep(500);
						filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
						Thread.sleep(500);
						log.add("The Selected Filter is: "+filter_Name);
						Thread.sleep(1000);
						String expctd = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
						Thread.sleep(500);
						log.add("The default selected option is: "+expctd);
						Thread.sleep(1000);
						if(expctd.equalsIgnoreCase(viewall))
						{
							log.add("View All option selected as default");	
							Thread.sleep(500);
							List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
							int ssize = filterList.size();
							j = r.nextInt(ssize);
							if(j==0)
							{
								j=j+1;
							}
							filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
							HBCBasicfeature.scrolldown(filterSelect, driver);
							filterSelect.click();
							wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
							Thread.sleep(1000);
							arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							HBCBasicfeature.scrolldown(arrow_clk, driver);
							arrow_clk.click();
							Thread.sleep(500);
							currentFilter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
							log.add("The current selected filter option is: "+currentFilter);                                                                                              
							if(currentFilter.equalsIgnoreCase(expctd))
							{
								Fail("on selecting some filter options in the sort/filter then View All option not disabled",log);
							}
							else
							{
								Pass("on selecting some filter options in the sort/filter then View All option  disabled",log);
								filterslct = true;
							}							
						}
						else
						{
							Fail("View All option not selected as default",log);
						}						
				}
				else
				{
					Fail("Filter Attributes not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-696 Verify that blue tick mark should be shown for selected filters in the right corner of it");
			if(filterslct==true)
			{
				filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
				log.add("The Selected Filter is: "+filter_Name);
				Thread.sleep(500);
				currentFilter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
				log.add("The current selected filter option is: "+currentFilter);   
				Thread.sleep(500);
				WebElement filterTick = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]//*[@class='cls_skCustomChkboxSpan']"));
				if(HBCBasicfeature.isElementPresent(filterTick))
				{					
					Pass("Blue tick mark shown for selected filters in the right corner of it",log);
				}
				else
				{
					Fail("Blue tick mark not shown for selected filters in the right corner of it",log);
				}					
			}		
			else
			{
				Skip("Filter option not selected");
			}	
			
			ChildCreation("HUDSONBAY-380 Verify that selected filter option should be deselected on tapping the selected filter option");
			if(filterslct==true)
			{
				Thread.sleep(1000);
				filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
				Thread.sleep(500);
				HBCBasicfeature.scrolldown(filterSelect, driver);
				if(HBCBasicfeature.isElementPresent(filterSelect))
				{
					filterslct = false;
					String selected = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
					log.add("The selected filter option is: "+selected);      
					filterSelect.click();
					wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
					Thread.sleep(500);
					arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
					HBCBasicfeature.scrolldown(arrow_clk, driver);
					arrow_clk.click();
					Thread.sleep(500);
					String current = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
					log.add("The current selected filter option is: "+current); 
					Thread.sleep(500);
					if(current.equalsIgnoreCase(selected))
					{
						Fail("Selected filter option not deselected on tapping the selected filter option",log);						
					}
					else
					{
						Pass("Selected filter option deselected on tapping the selected filter option",log);
						filterslct = true;						
					}					
				}
				else
				{
					Fail("Selected filter option is not displayed");
				}
			}
			else
			{
				Skip("Filter option not selected");
			}	
			
			ChildCreation("HUDSONBAY-686 Verify that when the user selects some filter option, then 'View All' option should be deselected by default");
			if(filterslct==true)
			{
				Pass("Selects some filter option, then 'View All' option deselected by default");
			}
			else
			{
				Fail("Selects some filter option, then 'View All' option not deselected by default");
			}
			
		}					
	
	/*HUDSONBAY-379 Verify that on tapping the "X" close icon after selecting the filter options, PLP page should not be updated based on the selected filter options*/
	public void Filter_not_selected()
		{
			ChildCreation("HUDSONBAY-379 Verify that on tapping the 'X' close icon after selecting the filter options, PLP page should not be updated based on the selected filter options");
			int i =0;
			WebElement filterSelect = null;
			WebDriverWait wait = new WebDriverWait(driver, 5);			
				try
				{					
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));
					if(HBCBasicfeature.isListElementPresent(FilterAttributes))
					{
						Random r = new Random();
							int size = FilterAttributes.size();
							i = r.nextInt(size);					
								WebElement arrow_clk = null ;
								WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
								try
								{
									wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
								}
								catch(Exception e)
								{
									arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
									HBCBasicfeature.scrolldown(arrow_clk, driver);
									arrow_clk.click();
									wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
								}	
								String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
								log.add("The Selected Filter is: "+filter_Name);
								Thread.sleep(1000);
								List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
								int ssize = filterList.size();
								int j = r.nextInt(ssize);
								if(j==0)
								{
									j=j+1;
								}
								filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
								HBCBasicfeature.scrolldown(filterSelect, driver);
								filterSelect.click();
								wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
								Thread.sleep(500);
								arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
								HBCBasicfeature.scrolldown(arrow_clk, driver);
								arrow_clk.click();
								Thread.sleep(1000);
								String current = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
								log.add("The current selected filter option is: "+current);      
								wait.until(ExpectedConditions.visibilityOf(RefinewindowClose));
								if(HBCBasicfeature.isElementPresent(RefinewindowClose))
								{
									log.add("X close icon displayed");
									HBCBasicfeature.scrollup(RefinewindowClose, driver);
									RefinewindowClose.click();
									wait.until(ExpectedConditions.visibilityOf(plpItemCont));
									if(HBCBasicfeature.isElementPresent(plpItemCont))
									{
										String val = plpItemCont.getText();
										String[] split = val.split(" ");
										String aftrPlpCount = split[0]; 
										log.add("Before count: "+plpCount);
										log.add("After count: "+aftrPlpCount);
										Thread.sleep(500);
										if(aftrPlpCount.contentEquals(plpCount))
										{
											Pass("'X' close icon after selecting the sort/filter options, PLP page not updated based on the selected filter options",log);
										}
										else
										{
											Fail("'X' close icon after selecting the sort/filter options, PLP page updated",log);
										}								
									}
									else
									{
										Fail("PLP Item count not displayed");
									}							
								}
								else
								{
									Fail("'X' close icon not displayed");
								}				
						}
						else
						{
							Fail("Filter Attributes not displayed");
						}			
				}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-373/376/381/684/697/699/868/698 Verify that product count should be updated based on applied filter as per the classic site*/
	public void Filter_ProductCount() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-373 Verify that product count should be updated based on applied filter as per the classic site");
			Random r = new Random();
			ArrayList<String> filters  = new ArrayList<String>();
			ArrayList<String> filtersUpdate  = new ArrayList<String>();
			ArrayList<Integer> result = new ArrayList<Integer>();

			String slct = HBCBasicfeature.getExcelVal("HB376", sheet, 1);
			int i = 0, j =0, k=0, l = 0;
			boolean opt = false;
			boolean multipleFilter = false;
			boolean filterCount = false;
			String filter_Name ="";
			String current ="";
			try
			{
				driver.navigate().refresh();
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					HBCBasicfeature.jsclick(sort, driver);					
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));	
					if(HBCBasicfeature.isListElementPresent(FilterAttributes))
					{
						int size = FilterAttributes.size();
						i = r.nextInt(size);	
						Thread.sleep(500);
						WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
						filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");
						log.add("The Selected Filter is: "+filter_Name);
						Thread.sleep(500);
						WebElement arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						HBCBasicfeature.scrolldown(arrow_clk, driver);
						arrow_clk.click();
						wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
						Thread.sleep(500);
						List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
						int ssize = filterList.size();
						j = r.nextInt(ssize);
						if(j==0)
						{
							j=j+1;
						}
						l = j;
						WebElement filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
						Thread.sleep(500);						
						HBCBasicfeature.scrolldown(filterSelect, driver);
						filterSelect.click();
						wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
						 Thread.sleep(500);
						 arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
						 HBCBasicfeature.scrolldown(arrow_clk, driver);
						 arrow_clk.click();
						 Thread.sleep(500);
						current = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]")).getText();
						log.add("The current selected filter option is: "+current);  
						filters.add(current);
						wait.until(ExpectedConditions.visibilityOf(RefinewindowApply));
						if(HBCBasicfeature.isElementPresent(RefinewindowApply))
						{
							HBCBasicfeature.scrollup(RefinewindowApply, driver);
							RefinewindowApply.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
							wait.until(ExpectedConditions.visibilityOf(plpItemCont));
							if(HBCBasicfeature.isElementPresent(plpItemCont))
							{
								String val = plpItemCont.getText();
								refCount = FilterCount.getText();	//For comparing HB376
								String[] split = val.split(" ");
								String aftrPlpCount = split[0]; 
								log.add("Before count: "+plpCount);
								log.add("After count: "+aftrPlpCount);
								Thread.sleep(500);
								if(!aftrPlpCount.contentEquals(plpCount))
								{
									Pass("Product count updated based on applied filter as per the classic site",log);
									opt = true;
								}
								else
								{
									Fail("Product count not updated based on applied filter as per the classic site",log);
								}								
							}
							else
							{
								Fail("PLP Item count not displayed");
							}							
						}
						else
						{
							Fail("Apply button not displayed");
						}						
					}
					else
					{
						Fail("Filter attributes not displayed");
					}
				}
				else
				{
					Fail("Sort option not sdisplayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
			ChildCreation("HUDSONBAY-376 Verify that selected refine by options count (filtered count) should be shown near the refine by option as X selected");
			if(opt==true)
			{
				wait.until(ExpectedConditions.visibilityOf(sort));
				if(HBCBasicfeature.isElementPresent(sort))
				{
					HBCBasicfeature.jsclick(sort, driver);					
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));					
					if(HBCBasicfeature.isElementPresent(Refinewindow))
					{
						filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");
						log.add("The Selected Filter is: "+filter_Name);
						String filter_Count = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentCount']")).getText();
						Thread.sleep(500);
						String[] split = filter_Count.split(" ");
						String count = split[0]; 
						String selected = split[1];
						log.add("The displayed count is: "+count);
						log.add("The displayed text is: "+selected);
						if(count.contains(refCount) && selected.equalsIgnoreCase(slct))
						{
							Pass("Selected refine by options count (filtered count) shown near the refine by option as X selected",log);	
							filterCount = true;
						}
						else
						{
							Fail("Selected refine by options count (filtered count) shown near the refine by option as X selected",log);
						}
					}
					else
					{
						Fail("Refine window not displayed");
					}
				}
				else
				{
					Fail("Sort option not displayed");
				}
			}
			else
			{
				Skip("Filter option not selected");
			}
			
			ChildCreation("HUDSONBAY-697 Verify that Selected filter count should be shown near the filter options in the sort/filter page");
			if(filterCount==true)
			{
				Pass("Selected filter count shown near the filter options in the sort/filter page");
			}
			else
			{
				Skip("Filter count not displayed");
			}
						
			ChildCreation("HUDSONBAY-699 Verify that selected filter count should be shown in the PLP page near the sort/filter option");
			if(filterCount==true)
			{
				if(refCount!="0")
				{
					log.add("Count after applied Filter is: "+refCount);					
					Pass("Selected filter count shown near the filter options in the PLP page near sort/filter page",log);
				}
				else
				{
					Fail("Selected filter count not shown near the filter options in the PLP page near sort/filter page",log);
				}				
			}
			else
			{
				Skip("Filter count not displayed");
			}
			
			ChildCreation("HUDSONBAY-381 Verify that selected filter options should be maintained with previously selected filters");
			if(opt==true)
			{	
				Actions act = new Actions(driver);
				WebElement arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
				HBCBasicfeature.scrolldown(arrow_clk, driver);
				act.moveToElement(arrow_clk).click().build().perform();
				Thread.sleep(1000);
				List<WebElement> filterList = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]"));
				int ssize = filterList.size();
				j = r.nextInt(ssize);
				if(j==l||j==0)
				{
					j=j+1;
				}
				WebElement filterSelect = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]"));
				Thread.sleep(500);						
				HBCBasicfeature.scrolldown(filterSelect, driver);
				filterSelect.click();
				wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
				Thread.sleep(1000);
				arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
				HBCBasicfeature.scrolldown(arrow_clk, driver);
				act.moveToElement(arrow_clk).click().build().perform();
				Thread.sleep(500);
				String filt= driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItems')]["+(j+1)+"]")).getText();
				Thread.sleep(500);
				filters.add(filt);
				wait.until(ExpectedConditions.visibilityOf(RefinewindowApply));
				if(HBCBasicfeature.isElementPresent(RefinewindowApply))
				{
					HBCBasicfeature.scrollup(RefinewindowApply, driver);
					RefinewindowApply.click();
					wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
					Thread.sleep(1000);
					HBCBasicfeature.jsclick(sort, driver);					
					arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
					HBCBasicfeature.scrolldown(arrow_clk, driver);
					arrow_clk.click();
					Thread.sleep(1000);
					List<WebElement> appliedFilters = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]"));
					for(k=1;k<=appliedFilters.size();k++)
					{
						String FilterNames = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]["+k+"]")).getText();
						Thread.sleep(500);
						filtersUpdate.add(FilterNames);
					}
					for (String temp : filtersUpdate)
					{
						result.add(filters.contains(temp) ? 1 : 0);
						//System.out.println(result);
						log.add("Compared result is: "+result);
					}
					
					if(!result.contains(0))
				  	{
						multipleFilter = true;
						log.add("The previously selected filters are: "+filters);
				  		log.add("The current displayed filters are: "+filtersUpdate);
				  		Pass("Selected filter options maintained with previously selected filters",log);
				  		
				  	}
				  	else
				  	{	
				  		log.add("The previously selected filters are: "+filters);
				  		log.add("The current displayed filters are: "+filtersUpdate);
				  		Fail("Selected filter options not maintained with previously selected filters",log);				  		
				  	}						
				}
				else
				{
					Fail("Refine apply not displayed");
				}				
			}
			else
			{
				Skip("Filter option not selected");
			}
			
			ChildCreation("HUDSONBAY-684 Verify that user can able to select multiple filter options of his choice");
			if(multipleFilter==true)
			{
				int size = filtersUpdate.size();
				log.add("Applied filters size is: "+size);
				if(size>1)
				{
					Pass("User can able to select multiple filter options of his choice",log);
				}
				else
				{
					Fail("User not able to select multiple filter options of his choice",log);
				}				
			}
			else
			{
				Skip("Multiple Filter not selected");
			}
			
			ChildCreation("HUDSONBAY-868 Verify that No other filter should be deselected when we select the some other filter in PLP");
			if(multipleFilter==true)
			{
				Pass("No other filter deselected when we select the some other filter in PLP");
			}
			else
			{
				Skip("Multiple Filter not selected");
			}
			
			ChildCreation("	HUDSONBAY-698 Verify that on navigating to other pages after applying some sort/filter options and coming back to the same page, the selected filters should be maintained");
			if(multipleFilter==true)
			{
				
				//Navigating to PDP
				
				if(HBCBasicfeature.isElementPresent(RefinewindowClose))
				{
					RefinewindowClose.click();
					wait.until(ExpectedConditions.visibilityOf(plpPage));
					int size = plpItemContList.size();
					int m = r.nextInt(size);
					WebElement product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(m+1)+"]"));
					wait.until(ExpectedConditions.visibilityOf(product));
					HBCBasicfeature.scrolldown(product, driver);
					if(HBCBasicfeature.isElementPresent(product))
					{
						WebElement product_Clk = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])["+(m+1)+"]"));		
						product_Clk.click();
						wait.until(ExpectedConditions.visibilityOf(PDPPage));
						if(HBCBasicfeature.isElementPresent(PDPPage))
						{
							log.add("Pdp page displayed");
							driver.navigate().back();
							HBCBasicfeature.jsclick(sort, driver);					
							WebElement arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
							HBCBasicfeature.scrolldown(arrow_clk, driver);
							arrow_clk.click();
							Thread.sleep(1000);
							filtersUpdate.clear();
							List<WebElement> appliedFilters = driver.findElements(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]"));
							for(k=1;k<=appliedFilters.size();k++)
							{
								String FilterNames = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[contains(@class,'skMob_filterItemSelected')]["+k+"]")).getText();
								Thread.sleep(500);
								filtersUpdate.add(FilterNames);
							}
							for (String temp : filtersUpdate)
							{
								result.add(filters.contains(temp) ? 1 : 0);
								//System.out.println(result);
								log.add("Compared result is: "+result);
							}
							
							if(!result.contains(0))
						  	{
								log.add("The previously selected filters are: "+filters);
						  		log.add("The current displayed filters are: "+filtersUpdate);
						  		Pass("On Navigating to other pages after applying some sort/filter options and coming back to the same page, the selected filters are maintained",log);						  		
						  	}
						  	else
						  	{	
						  		log.add("The previously selected filters are: "+filters);
						  		log.add("The current displayed filters are: "+filtersUpdate);
						  		Fail("On Navigating to other pages after applying some sort/filter options and coming back to the same page, the selected filters are not maintained",log);				  		
						  	}						
						}
						else
						{
							Fail("PDP page not displayed");
						}
					}
					else
					{
						Fail("Product not displayed in PLP");
					}
				}
				else
				{
					Fail("Refine close icom not dispalyed");
				}				
			}
			else
			{
				Skip("Multiple Filters not selected");
			}	
		}
		
	/*HUDSONBAY-371/692 Verify that sort/filter options should be cleared and default options should be selected on tapping the "Clear All" link*/
	public void filterClearAll() throws Exception
		{
			ChildCreation("HUDSONBAY-371 Verify that sort/filter options should be cleared and default options should be selected on tapping the 'Clear All' link");
			WebDriverWait wait = new WebDriverWait(driver, 5);
			String viewall = HBCBasicfeature.getExcelVal("HB377", sheet, 1);
			boolean flag = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(RefinewindowClearAll))
				{
					HBCBasicfeature.scrollup(RefinewindowClearAll, driver);
					RefinewindowClearAll.click();
					wait.until(ExpectedConditions.visibilityOf(plpPage));
					HBCBasicfeature.jsclick(sort, driver);					
					wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));	
					if(HBCBasicfeature.isListElementPresent(FilterAttributes))
					{
						int size = FilterAttributes.size();
						for(int i=0;i<size;i++)
						{
							flag = false;
							WebElement arrow_clk = null ;
							WebElement filter = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]"));
							try
							{
								wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
							}
							catch(Exception e)
							{
								arrow_clk = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentsBtnIcon']"));
								HBCBasicfeature.scrolldown(arrow_clk, driver);
								arrow_clk.click();
								wait.until(ExpectedConditions.attributeContains(filter, "style", "block"));	
							}
							Thread.sleep(500);
							String filter_Name = driver.findElement(By.xpath("//*[@class='skTab_filterContentScrollerContainer']["+(i+1)+"]//*[@class='skMob_filterContentItems']")).getText().replace(":","");;
							String expctd = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(i+1)+"]//*[@class='skMob_filterItems  skMob_filterItemSelected  ']")).getText();
							Thread.sleep(500);
							if(expctd.equalsIgnoreCase(viewall))
							{
								Pass(""+expctd+" option selected in default after clearall all the filters for option: "+filter_Name);
								HBCBasicfeature.scrolldown(arrow_clk, driver);							
								arrow_clk.click();
								flag = true;
							}
							else
							{
								log.add("Selected default option is: "+expctd);
								Fail("'View All' option not selected in default after clearall all the filters for refine option: "+filter_Name,log);
							}						
						}
					}
					else
					{
						Fail("Filter options not displayed");
					}
				}
				else
				{
					Fail("Clear All option not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-692 Verify that on selecting the 'Clear All' link, the selected sort/filter options should be deselected and reset to the default options");
			if(flag==true)
			{
				Pass("On selecting the 'Clear All' link, the selected sort/filter options deselected and reset to the default options");
			}
			else
			{
				Fail("On selecting the 'Clear All' link, the selected sort/filter options not deselected and reset to the default options");
			}			
		}
	
	/*HUDSONBAY-858 Verify that respective french strings should be shown as per the classic site in Sort/FIlter page*/
	public void frenchStrings() throws Exception
		{
 			ChildCreation("HUDSONBAY-858 Verify that respective french strings should be shown as per the classic site in Sort/FIlter page");
			String frenchString  = HBCBasicfeature.getExcelVal("HB858", sheet, 1);
			String[] split = frenchString.split("\n");
			try
			{
				if(HBCBasicfeature.isElementPresent(RefinewindowClose))
				{
					RefinewindowClose.click();
					Thread.sleep(1000);
					
							boolean flag =false;
							do
							{
								wait.until(ExpectedConditions.elementToBeClickable(hamburger));
								if(HBCBasicfeature.isElementPresent(hamburger))
								{
									hamburger.click();
									if(HBCBasicfeature.isElementPresent(otherlanguage))
									{
										HBCBasicfeature.scrolldown(otherlanguage, driver);
										Thread.sleep(1000);
										((JavascriptExecutor) driver).executeScript("arguments[0].click();", otherlanguage);											
										String url = driver.getCurrentUrl();
										if(url.contains("labaie.com"))
										{
											flag=true;
											break;											
										}
										else
										{
											driver.navigate().refresh();
											continue;
										}
									}										
								}
							}
							while(flag!=true);
							wait.until(ExpectedConditions.elementToBeClickable(hamburger));	
							if(HBCBasicfeature.isElementPresent(hamburger))
							{
								Thread.sleep(500);
								hamburger.click();
								wait.until(ExpectedConditions.visibilityOf(pancakemenu));
								if(HBCBasicfeature.isElementPresent(pancakemenu))
								{
									pancakemenu.click();
									wait.until(ExpectedConditions.visibilityOf(pancakemenuCategory));
									if(HBCBasicfeature.isElementPresent(pancakemenuCategory))
									{
										pancakemenuCategory.click();
										boolean pgLoad1 = false;
										do
										{				
											try
											{
												Thread.sleep(1000);
												if(HBCBasicfeature.isElementPresent(plpPage))		
												{
													pgLoad1=true;
													break;		
												}
											}
											catch (Exception e1)
											{
												pgLoad1=false;
												driver.navigate().refresh();	
												continue;
											}
										}
										while(!pgLoad1==true);
										if(HBCBasicfeature.isElementPresent(plpPage))
										{
											log.add("PLP Page Displayed");
											((JavascriptExecutor) driver).executeScript("arguments[0].click();", sort);		
											wait.until(ExpectedConditions.attributeContains(Refinewindow, "style", "block"));	
											if(HBCBasicfeature.isElementPresent(RefinewindowApply))
											{
												String title = RefinewindowApply.getText();
												if(title.equalsIgnoreCase(split[0]))
												{
													log.add("Text displayed is: "+title);
													Pass("'Apply' text link shown in the center of the drawer",log);
												}
												else
												{
													Fail("'Apply' text link not shown in the center of the drawer",log);
												}
											}
											else
											{
												Fail("Refine window not displayed",log);
											}	
											if(HBCBasicfeature.isElementPresent(RefinewindowClearAll))
											{
												String text = RefinewindowClearAll.getText();
												if(text.equalsIgnoreCase(split[1]))
												{
													log.add("Text displayed is: "+text);
													Pass("'clear All' button shown in the top right corner of the drawer",log);					
												}
												else
												{
													Fail("'clear All' button not shown in the top right corner of the drawer",log);
												}
											}
											else
											{
												Fail("clear All button not displayed");
											}											
										}
										else
										{
											Fail("Selecting the sub-categories in the pancake menu,the selected category PLP page not displayed");
										}							
									}
									else
									{
										Fail("Pancake Menu category not displayed");
									}
								}
								else
								{
									Fail("Pancake menu not displayed");
								}				
							/*}
							else
							{
								Fail("Hamburger not displayed");
							}
						}
						else
						{
							Fail("Other language link not dsiplayed");
						}						*/
					}
					else
					{
						Fail("Hamburger not displayed");
					}
				}
				else
				{
					Fail("Clear all link not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	
	
	
	
	}
