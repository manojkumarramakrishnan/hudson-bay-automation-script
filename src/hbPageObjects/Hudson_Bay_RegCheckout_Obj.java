package hbPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_RegCheckout;

public class Hudson_Bay_RegCheckout_Obj extends Hudson_Bay_RegCheckout implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_RegCheckout_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	@FindBy(xpath = ""+homepagee+"")
	WebElement homepage;	
	
	@FindBy(xpath = ""+bagIconCountt+"")
	WebElement bagIconCount;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath = ""+pancakeStaticCategoryy+"")
	WebElement pancakeStaticCategory;
	
	@FindBy(xpath = ""+pancakeStaticWelcomee+"")
	WebElement pancakeStaticWelcome;	
	
	@FindBy(xpath = ""+pancakeStaticSignOutt+"")
	WebElement pancakeStaticSignOut;	
	
	@FindBy(xpath = ""+maskk+"")
	WebElement mask;		
	
	@FindBy(xpath = ""+signInPagee+"")
	WebElement signInPage;
	
	@FindBy(xpath = ""+emailFieldd+"")
	WebElement emailField;
	
	@FindBy(xpath = ""+pwdFieldd+"")
	WebElement pwdField;
	
	@FindBy(xpath = ""+signInButtonn+"")
	WebElement signInButton;
	
	@FindBy(xpath = ""+myAccountPagee+"")
	WebElement myAccountPage;
	
	@FindBy(xpath = ""+myAccountPageAddressbookk+"")
	WebElement myAccountPageAddressbook;	
	
	@FindBy(xpath = ""+myAccountPageAddressDropDownn+"")
	WebElement myAccountPageAddressDropDown;	
	
	@FindBy(xpath = ""+myAccountAddressBookpagee+"")
	WebElement myAccountAddressBookpage;		
	
	@FindBy(xpath = ""+myAccountPageAddressRemoveButtonn+"")
	WebElement myAccountPageAddressRemoveButton;	
		
	@FindBy(how = How.XPATH,using = ""+myAccountPageAddressDropDownn+"")
	List<WebElement> myAccountPageAddressDropDownList;
		
	@FindBy(xpath = ""+shoppingCartPagee+"")
	WebElement shoppingBagPage;
	
	@FindBy(xpath = ""+prodQuantyy+"")
	WebElement prodQuanty;
	
	@FindBy(xpath = ""+prodQuantyIncc+"")
	WebElement prodQuantyInc;
	
	@FindBy(xpath = ""+prodQuantyDecc+"")
	WebElement prodQuantyDec;	
	
	@FindBy(xpath = ""+checkoutButtonn+"")
	WebElement checkoutButton;
	
	@FindBy(xpath = ""+shippingPaymentPagee+"")
	WebElement shippingPaymentPage;
	
	@FindBy(xpath = ""+checkoutNavTitlee+"")
	WebElement checkoutNavTitle;
	
	@FindBy(xpath = ""+checkoutNavTabss+"")
	WebElement checkoutNavTabs;
	
	@FindBy(xpath = ""+checkoutNavTabb1+"")
	WebElement checkoutNavTab1;
	
	@FindBy(xpath = ""+checkoutNavTabb2+"")
	WebElement checkoutNavTab2;
	
	@FindBy(xpath = ""+checkoutNavTabb3+"")
	WebElement checkoutNavTab3;	
	
	@FindBy(xpath = ""+checkoutCurrNavTitlee+"")
	WebElement checkoutCurrNavTitle;	
	
	@FindBy(xpath = ""+shipandPaypageShipSecc+"")
	WebElement shipandPaypageShipSec;	
	
	@FindBy(xpath = ""+shipandPaypagePaySecc+"")
	WebElement shipandPaypagePaySec;	
	
	@FindBy(xpath = ""+shippingAlertt+"")
	WebElement shippingAlert;
	
	@FindBy(xpath = ""+paymentAlertt+"")
	WebElement paymentAlert;	
	
	@FindBy(xpath = ""+shipandPaypageShipEditLinkk+"")
	WebElement shipandPaypageShipEditLink;
	
	@FindBy(xpath = ""+shipandPaypagePayEditLinkk+"")
	WebElement shipandPaypagePayEditLink;
	
	@FindBy(xpath = ""+shipandPaypageShipCreateAddLinkk+"")
	WebElement shipandPaypageShipCreateAddLink;
	
	@FindBy(xpath = ""+shipandPaypagePayCreateAddLinkk+"")
	WebElement shipandPaypagePayCreateAddLink;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresss+"")
	WebElement shipandPaypageShipShipCreateEditAddress;
	
	@FindBy(xpath = ""+shipandPaypageShipCreateEditAddressOverlayy+"")
	WebElement shipandPaypageShipCreateEditAddressOverlay;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddressCloseLinkk+"")
	WebElement shipandPaypageShipShipCreateEditAddressCloseLink;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddressCancelBtnn+"")
	WebElement shipandPaypageShipShipCreateEditAddressCancelBtn;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddressOkBtnn+"")
	WebElement shipandPaypageShipShipCreateEditAddressOkBtn;
	
	@FindBy(xpath = ""+shipandPaypageShipAddressDispp+"")
	WebElement shipandPaypageShipAddressDisp;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssFNamee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssFName;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssLNamee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssLName;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssStAddresss+"")
	WebElement shipandPaypageShipShipCreateEditAddresssStAddress;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssStAddresss2+"")
	WebElement shipandPaypageShipShipCreateEditAddresssStAddress2;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssCtyy+"")
	WebElement shipandPaypageShipShipCreateEditAddresssCty;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssCountryy+"")
	WebElement shipandPaypageShipShipCreateEditAddresssCountry;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssStatee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssState;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPayStatee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPayState;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPayUKStatee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPayUKState;
		
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPOcodee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPOcode;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssNonUsUkPOcodee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode;
		
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHH1+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPH1;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHH2+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPH2;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHH3+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPH3;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHH4+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPH4;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditEmaill+"")
	WebElement shipandPaypageShipShipCreateEditEmail;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditTermsandCondd+"")
	WebElement shipandPaypageShipShipCreateEditTermsandCond;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditTermsandCondChkboxx+"")
	WebElement shipandPaypageShipShipCreateEditTermsandCondChkbox;		
	
	@FindBy(xpath = ""+shipandPaypageShipAddressColorDispp+"")
	WebElement shipandPaypageShipAddressColorDisp;
		
	@FindBy(xpath = ""+addressVerificationoverlayy+"")
	WebElement addressVerificationoverlay;
		
	@FindBy(xpath = ""+addressVerificationEditt+"")
	WebElement addressVerificationEdit;
	
	@FindBy(xpath = ""+bpostalCodeee+"")
	WebElement bpostalCode;
	
	@FindBy(xpath = ""+checkoutBPOAlertt+"")
	WebElement checkoutBPOAlert;
	
	@FindBy(xpath = ""+checkoutSPHAlertt1+"")
	WebElement checkoutSPHAlert1;
	
	@FindBy(xpath = ""+checkoutSPHAlertt3+"")
	WebElement checkoutSPHAlert3;
	
	@FindBy(xpath = ""+shipandPaypageShipShipToNamee+"")
	WebElement shipandPaypageShipShipToName;
	
	@FindBy(xpath = ""+shipandPaypageShipPayToNamee+"")
	WebElement shipandPaypageShipPayToName;	
	
	@FindBy(xpath = ""+addressuseasEnteredd+"")
	WebElement addressuseasEntered;
	
	@FindBy(xpath = ""+progressBarWidgett+"")
	WebElement progressBarWidget;
	
	@FindBy(xpath = ""+shipandPaypagee+"")
	WebElement shipandPaypage;
		
	@FindBy(xpath = ""+shipandPaypagePayAddressDispp+"")
	WebElement shipandPaypagePayAddressDisp;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssUKPhonee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssUKPhone;	
	
	@FindBy(xpath = ""+paymentOptionsPayByCardd+"")
	WebElement paymentOptionsPayByCard;
	
	@FindBy(xpath = ""+paymentOptionspaybycardChkkBoxx+"")
	WebElement paymentOptionspaybycardChkkBox;
		
	@FindBy(xpath = ""+paymentOptionsCreditCardd+"")
	WebElement paymentOptionsCreditCard;	
	
	@FindBy(xpath = ""+paymentOptionsCreditCardChkkBoxx+"")
	WebElement paymentOptionsCreditCardChkkBox;
	
	@FindBy(xpath = ""+paymentOptionspaybycardChkkBoxSelectedd+"")
	WebElement paymentOptionspaybycardChkkBoxSelected;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardSecEnablee+"")
	WebElement paymentOptionsCreditCardSecEnable;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardSelectedd+"")
	WebElement paymentOptionsCreditCardSelected;
	
	@FindBy(xpath = ""+paymentOptionsPaypall+"")
	WebElement paymentOptionsPaypal;
	
	@FindBy(xpath = ""+paymentOptionsWhatIspaypall+"")
	WebElement paymentOptionsWhatIspaypal;	
	
	@FindBy(xpath = ""+paymentOptionsPaypalSelectedd+"")
	WebElement paymentOptionsPaypalSelected;
		
	@FindBy(xpath = ""+paymentOptionsPaypalChkkBoxx+"")
	WebElement paymentOptionsPaypalChkkBox;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardCardSelDropDownn+"")
	WebElement paymentOptionsCreditCardCardSelDropDown;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardCardSelDropDownLabell+"")
	WebElement paymentOptionsCreditCardCardSelDropDownLabel;	
	
	@FindBy(xpath = ""+paymentOptionsGiftCardd+"")
	WebElement paymentOptionsGiftCard;
	
	@FindBy(xpath = ""+paymentOptionsGiftCardChkkBoxx+"")
	WebElement paymentOptionsGiftCardChkkBox;
		
	@FindBy(xpath = ""+paymentOptionsRewardCardFieldLabell+"")
	WebElement paymentOptionsRewardCardFieldLabel;	
	
	@FindBy(xpath = ""+paymentOptionsRewardCardFieldd1+"")
	WebElement paymentOptionsRewardCardField1;
	
	@FindBy(xpath = ""+paymentOptionsRewardCardFieldd2+"")
	WebElement paymentOptionsRewardCardField2;
	
	@FindBy(xpath = ""+paymentOptionsRewardCardFieldAlertt+"")
	WebElement paymentOptionsRewardCardFieldAlert;	
	
	@FindBy(xpath = ""+paymentOptionsPromoCodee+"")
	WebElement paymentOptionsPromoCode;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodeApplyy+"")
	WebElement paymentOptionsPromoCodeApply;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodeApplyAlertt+"")
	WebElement paymentOptionsPromoCodeApplyAlert;	
	
	@FindBy(xpath = ""+paymentOptionsPromoCodeApplyInvalidAlertt+"")
	WebElement paymentOptionsPromoCodeApplyInvalidAlert;
	
	@FindBy(xpath = ""+paymentOptionsShippingMethoddropdownn+"")
	WebElement paymentOptionsShippingMethoddropdown;
	
	@FindBy(xpath = ""+paymentOptionsEstimatedSubtotall+"")
	WebElement paymentOptionsEstimatedSubtotal;
	
	@FindBy(xpath = ""+paymentOptionsEstimatedSubtotalPricee+"")
	WebElement paymentOptionsEstimatedSubtotalPrice;
	
	@FindBy(xpath = ""+paymentOptionsShippingAmountlabell+"")
	WebElement paymentOptionsShippingAmountlabel;
	
	@FindBy(xpath = ""+paymentOptionsShippingAmountPricee+"")
	WebElement paymentOptionsShippingAmountPrice;
	
	@FindBy(xpath = ""+paymentOptionsPSTLabell+"")
	WebElement paymentOptionsPSTLabel;
	
	@FindBy(xpath = ""+paymentOptionsPSTPricee+"")
	WebElement paymentOptionsPSTPrice;
	
	@FindBy(xpath = ""+paymentOptionsGSTHSTLabell+"")
	WebElement paymentOptionsGSTHSTLabel;
	
	@FindBy(xpath = ""+paymentOptionsGSTHSTPricee+"")
	WebElement paymentOptionsGSTHSTPrice;
		
	@FindBy(xpath = ""+paymentOptionsOrderTotall+"")
	WebElement paymentOptionsOrderTotal;
	
	@FindBy(xpath = ""+paymentOptionsOrderTotalLabell+"")
	WebElement paymentOptionsOrderTotalLabel;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardAmountToPaylabell+"")
	WebElement paymentOptionsCreditCardAmountToPaylabel;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardAmountToPayPricee+"")
	WebElement paymentOptionsCreditCardAmountToPayPrice;
	
	@FindBy(xpath = ""+loadingGaugeDomm+"")
	WebElement loadingGaugeDom;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardNumberFieldd+"")
	WebElement paymentOptionsCreditCardNumberField;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardNumberFieldAlertt+"")
	WebElement paymentOptionsCreditCardNumberFieldAlert;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardSecurityCodeFieldd+"")
	WebElement paymentOptionsCreditCardSecurityCodeField;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardSecurityCodeFieldAlertt+"")
	WebElement paymentOptionsCreditCardSecurityCodeFieldAlert;
		
	@FindBy(xpath = ""+paymentOptionsCreditCardExpMonthFieldd+"")
	WebElement paymentOptionsCreditCardExpMonthField;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardExpYearFieldd+"")
	WebElement paymentOptionsCreditCardExpYearField;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardExpMonthFieldAlertt+"")
	WebElement paymentOptionsCreditCardExpMonthFieldAlert;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardExpYearFieldAlertt+"")
	WebElement paymentOptionsCreditCardExpYearFieldAlert;

	@FindBy(xpath = ""+paymentOptionsApplyGiftCardNumberFieldd+"")
	WebElement paymentOptionsApplyGiftCardNumberField;
	
	@FindBy(xpath = ""+paymentOptionsApplyGiftCardPinFieldd+"")
	WebElement paymentOptionsApplyGiftCardPinField;
	
	@FindBy(xpath = ""+paymentOptionsApplyGiftCardApplyy+"")
	WebElement paymentOptionsApplyGiftCardApply;
	
	@FindBy(xpath = ""+paymentOptionsAddAnotherGiftCardd+"")
	WebElement paymentOptionsAddAnotherGiftCard;	
	
	@FindBy(xpath = ""+addressVerificationInfoTextt+"")
	WebElement addressVerificationInfoText;
	
	@FindBy(xpath = ""+addressVerificationAddressEditLinkk+"")
	WebElement addressVerificationAddressEditLink;
	
	@FindBy(xpath = ""+addressSuggestionListt+"")
	WebElement addressSuggestionList;
	
	@FindBy(xpath = ""+addressSuggestionDeliveryWarningg+"")
	WebElement addressSuggestionDeliveryWarning;
	
	@FindBy(xpath = ""+addressSuggestionClosee+"")
	WebElement addressSuggestionClose;
	
	@FindBy(xpath = ""+addressSuggestedBtnn+"")
	WebElement addressSuggestedBtn;
	
	@FindBy(xpath = ""+addressVerificationbuildingTextboxx+"")
	WebElement addressVerificationbuildingTextbox;
	
	@FindBy(xpath = ""+addressVerificationPotentialMatchesTextt+"")
	WebElement addressVerificationPotentialMatchesText;
	
	@FindBy(xpath = ""+addressVerificationPotentialMatchesAddresss+"")
	WebElement addressVerificationPotentialMatchesAddress;
	
	@FindBy(how = How.XPATH, using = ""+addressVerificationSuggestedAddresss+"")
	List<WebElement> addressVerificationSuggestedAddress;
	
	@FindBy(how = How.XPATH, using = ""+addressVerificationPotentialMatchesAddressListt+"")
	List<WebElement> addressVerificationPotentialMatchesAddressList;
	
	@FindBy(how = How.XPATH,using = ""+addressSuggestionAddressListt+"")
	List<WebElement> addressSuggestionAddressList;
		
	@FindBy(how = How.XPATH,using = ""+userEnteredAddresss+"")
	List<WebElement> userEnteredAddress;
		
	@FindBy(xpath = ""+reviewOrderGeneralErrorr+"")
	WebElement reviewOrderGeneralError;
	
	
	@FindBy(xpath = ""+secondGiftCardd+"")
	WebElement secondGiftCard;		
	
	@FindBy(xpath = ""+paymentOptionReviewOrderr+"")
	WebElement paymentOptionReviewOrder;
	
	@FindBy(xpath = ""+ReviewandSubmitPagee+"")
	WebElement ReviewandSubmitPage;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingAddresss+"")
	WebElement ReviewandSubmitShippingAddress;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingMethodd+"")
	WebElement ReviewandSubmitShippingMethod;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingAddresss+"")
	WebElement ReviewandSubmitBillingAddress;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingMethodd+"")
	WebElement ReviewandSubmitBillingMethod;
	
	@FindBy(xpath = ""+ReviewandSubmitCheckoutSummaryy+"")
	WebElement ReviewandSubmitCheckoutSummary;
	
	@FindBy(xpath = ""+ReviewandSubmitPlaceorderbuttonn+"")
	WebElement ReviewandSubmitPlaceorderbutton;
	
	@FindBy(xpath = ""+shippingandPaymentt+"")
	WebElement shippingandPayment;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingAddressEditt+"")
	WebElement ReviewandSubmitShippingAddressEdit;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingMethodEditt+"")
	WebElement ReviewandSubmitShippingMethodEdit;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingAddressEditt+"")
	WebElement ReviewandSubmitBillingAddressEdit;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingMethodEditt+"")
	WebElement ReviewandSubmitBillingMethodEdit;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingAddressDetailss+"")
	WebElement ReviewandSubmitShippingAddressDetails;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingMethodSelectedd+"")
	WebElement ReviewandSubmitShippingMethodSelected;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingAddressDetailss+"")
	WebElement ReviewandSubmitBillingAddressDetails;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingMethodDetailss+"")
	WebElement ReviewandSubmitBillingMethodDetails;

	@FindBy(xpath = ""+ReviewandSubmitBillingAddressNameDetailss+"")
	WebElement ReviewandSubmitBillingAddressNameDetails;
	
	@FindBy(xpath = ""+ReviewandSubmitOrderTotall+"")
	WebElement ReviewandSubmitOrderTotal;
		
	@FindBy(xpath = ""+placeOrderr+"")
	WebElement placeOrder;
	
	@FindBy(xpath = ""+confirmOrderPagee+"")
	WebElement confirmOrderPage;
	
	@FindBy(xpath = ""+confirmThanksSectionn+"")
	WebElement confirmThanksSection;
	
	@FindBy(xpath = ""+confirmThanksSectionOrdernumberr+"")
	WebElement confirmThanksSectionOrdernumber;	
	
	@FindBy(xpath = ""+confirmShippingInfoSectionn+"")
	WebElement confirmShippingInfoSection;
	
	@FindBy(xpath = ""+confirmPaymentInfoSectionn+"")
	WebElement confirmPaymentInfoSection;
	
	@FindBy(xpath = ""+confirmPaymentDetailSectionn+"")
	WebElement confirmPaymentDetailSection;
	
	@FindBy(xpath = ""+confirmRewardDetailSectionn+"")
	WebElement confirmRewardDetailSection;
	
	@FindBy(xpath = ""+confirmsubTotalSectionn+"")
	WebElement confirmsubTotalSection;
	
	@FindBy(xpath = ""+confirmPromotionSectionn+"")
	WebElement confirmPromotionSection;
	
	@FindBy(xpath = ""+confirmShippingTaxesSectionn+"")
	WebElement confirmShippingTaxesSection;
	
	@FindBy(xpath = ""+confirmOrderTotalSectionn+"")
	WebElement confirmOrderTotalSection;
		
	@FindBy(xpath = ""+chkoutAddressPagee+"")
	WebElement chkoutAddressPage;
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;
	
	@FindBy(xpath = ""+firstNamee+"")
	WebElement firstName;
	
	@FindBy(xpath = ""+AddressSubmitt+"")
	WebElement AddressSubmit;
	
	@FindBy(xpath = ""+shoppingBagPageProdTitlee+"")
	WebElement shoppingBagProdTitle;
	
	//Label names Edit Shipping Address field
	
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssFnameLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssFnameLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssLNameLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssLNameLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssStAddressLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssStAddressLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssCtyLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssCtyLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssCountryLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssCountryLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssStateLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssStateLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddressStateLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddressStateLabel;
						
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPOcodeLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssPOcodeLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssPHLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHUKLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssPHUKLabel;		
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssEmailLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssEmailLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssUKContryLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssUKContryLabel;
		
		@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssNonUsPOcodeLabell+"")
		WebElement shipandPaypageShipShipCreateEditAddresssNonUsPOcodeLabel;
		
	@FindBy(how = How.XPATH,using = ""+recentSearchListt+"")
	List<WebElement> recentSearchList;
	
	@FindBy(how = How.XPATH,using = ""+paymentOptionsCreditCardImagess+"")
	List<WebElement> paymentOptionsCreditCardImages;
			
	ArrayList<String> searchKeywords =new ArrayList<String>();
	
	String keyword = "", paybefAdd = "", payDefSelCountr = ""; 
	
	String shipAddress = "", billAddress = "", shipMethod = "";
	
	String prodTitle = "";
	
	boolean shippingPage = false, addVerEnable = false, reviewSubmit = false , orderConfirm = false;
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}		

	// Load Address Verification Page
	public boolean ldAddressVerification()
	{
		boolean addOverlay = false; 
		int count = 0;
		WebDriverWait w1 = new WebDriverWait(driver, 5);
		do
		{
			try
			{
				count++;
				jsclick(shipandPaypageShipShipCreateEditAddressOkBtn);
				Thread.sleep(1000);
				w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				addOverlay = true;
				if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
				{
					jsclick(addressVerificationEdit);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
					Thread.sleep(1000);
				}
			}
			catch(Exception e)
			{
				addOverlay = false;
			}
		}while((addOverlay==true)&&(count<1));
		return addOverlay;
	}
		
	// check Address Verification Page
		public boolean ldAddressVerification2()
		{
			boolean addOverlay = false; 
			int count = 0;
			WebDriverWait w1 = new WebDriverWait(driver, 5);
			do
			{
				try
				{
					count++;
					jsclick(shipandPaypageShipShipCreateEditAddressOkBtn);
					Thread.sleep(1000);
					w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
					addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
					addOverlay = true;					
				}
				catch(Exception e)
				{
					addOverlay = false;
				}
			}while((addOverlay==true)&&(count<1));
			return addOverlay;
		}
		
	// Load Address Verification Page
	public boolean ldNoAddressVerification()
		{
			boolean addOverlay = false; 
			int count = 0;
			WebDriverWait w1 = new WebDriverWait(driver, 5);
			do
			{
				try
				{
					count++;
					shipandPaypageShipShipCreateEditAddressOkBtn.click();
					w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
					addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
					addOverlay = false;
					if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
					{
						jsclick(addressVerificationEdit);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
						Thread.sleep(1000);
					}
				}
				catch(Exception e)
				{
					addOverlay = true;
				}
			}while((addOverlay==false)&&(count<1));
			return addOverlay;
		}
			
	/* SignIn -1 page to Checkout Page */
	public void signinToChkoutPage() throws java.lang.Exception
		{
			ChildCreation("SignIn page to Checkout Page");
			String email = HBCBasicfeature.getExcelVal("login", sheet, 4);
			String pwd = HBCBasicfeature.getExcelVal("login", sheet, 5);
			try
			{
				Actions action = new Actions(driver);
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(hamburger));	
				if(HBCBasicfeature.isElementPresent(hamburger))
				{
					boolean flag = false;
					do
					{
						try
						{
							action.moveToElement(hamburger).click().build().perform();
							Thread.sleep(2000);
							if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
							{
								flag = true;
								break;
							}
							else
							{
								continue;
							}
						}
						catch(Exception e)
						{
							continue;
						}
					}
					while(flag!=true);					
					if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
					{
						HBCBasicfeature.scrolldown(pancakeStaticWelcome, driver);
						Thread.sleep(500);
						pancakeStaticWelcome.click();
						wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
						if(HBCBasicfeature.isElementPresent(emailField))
						{	
							emailField.sendKeys(email);								
							log.add("Enetered Email ID is: "+ email);
							if(HBCBasicfeature.isElementPresent(pwdField))
							{	
								pwdField.sendKeys(pwd);									
								log.add("Enetered Password is: "+ pwd);
								if(HBCBasicfeature.isElementPresent(signInButton))
								{
									signInButton.click();
									Thread.sleep(1000);
									wait.until(ExpectedConditions.visibilityOf(myAccountPage));			
									if(HBCBasicfeature.isElementPresent(myAccountPage))
									{	
										log.add("MyAccount Page displayed");
										wait.until(ExpectedConditions.visibilityOf(bagIcon));			
										if(HBCBasicfeature.isElementPresent(bagIcon))
										{													
											wait.until(ExpectedConditions.visibilityOf(bagIconCount));			
											if(HBCBasicfeature.isElementPresent(bagIconCount))
											{
												int count = Integer.parseInt(bagIconCount.getText());
												log.add("The Product count shown in bag is: "+count);
												if(count>0)
												{
													log.add("Products displayed in shopping bag");
													bagIcon.click();
													Thread.sleep(1000);
													wait.until(ExpectedConditions.visibilityOf(shoppingBagPage));			
													if(HBCBasicfeature.isElementPresent(shoppingBagPage))
													{
														log.add("Shopping Bag page displayed");
														prodTitle = shoppingBagProdTitle.getText();
														Thread.sleep(1000);
														wait.until(ExpectedConditions.visibilityOf(checkoutButton));			
														if(HBCBasicfeature.isElementPresent(checkoutButton))
														{
															checkoutButton.click();
															Thread.sleep(1000);
															wait.until(ExpectedConditions.attributeContains(shippingPaymentPage, "style", "visible"));
															if(HBCBasicfeature.isElementPresent(shippingPaymentPage))
															{
																Pass("Checkout shipping & payment page displayed",log);
																shippingPage = true;
															}
															else
															{
																Fail("Checkout shipping & payment page not displayed",log);
															}																
														}
														else
														{
															Fail("Checkout button not displayed");
														}															
													}
													else
													{
														Fail("Shopping Bag page not displayed");
													}
												}
												else
												{
													Fail("Bag count is '0' no products displayed");
												}
											}
											else
											{
												Fail("Bag icon count not displayed");
											}
										}
										else
										{
											Fail("Bagicon not displayed");
										}
									}
									else
									{
										Fail("MyAccount page not displayed");
									}
								}
								else
								{
									Fail("SignIn button not displayed");
								}
							}
							else
							{
								Fail("Password Field not displayed");
							}
						}
						else
						{
							Fail("Email Field not displayed");
						}
					}
					else
					{
						Fail("Pancake WelcomesignIn option not displayed");
					}
				}
				else
				{
					Fail("Hamburger menuu not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/* SignIn -2 page to Checkout Page */
	public void signin2() throws java.lang.Exception
	{
		ChildCreation("SignIn-2 page to Checkout Page");
		String email = HBCBasicfeature.getExcelVal("login", sheet, 6);
		String pwd = HBCBasicfeature.getExcelVal("login", sheet, 7);
		shippingPage = false;
		try
		{
			Actions action = new Actions(driver);
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{
					try
					{
						action.moveToElement(hamburger).click().build().perform();
						if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
						{
							flag = true;
							break;
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
				while(flag!=true);					
				if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
				{
					HBCBasicfeature.scrolldown(pancakeStaticWelcome, driver);
					pancakeStaticWelcome.click();
					wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
					if(HBCBasicfeature.isElementPresent(emailField))
					{	
						emailField.sendKeys(email);								
						log.add("Enetered Email ID is: "+ email);
						if(HBCBasicfeature.isElementPresent(pwdField))
						{	
							pwdField.sendKeys(pwd);									
							log.add("Enetered Password is: "+ pwd);
							if(HBCBasicfeature.isElementPresent(signInButton))
							{
								signInButton.click();
								wait.until(ExpectedConditions.visibilityOf(myAccountPage));			
								if(HBCBasicfeature.isElementPresent(myAccountPage))
								{	
									log.add("MyAccount Page displayed");
									wait.until(ExpectedConditions.visibilityOf(bagIcon));			
									if(HBCBasicfeature.isElementPresent(bagIcon))
									{													
										wait.until(ExpectedConditions.visibilityOf(bagIconCount));			
										if(HBCBasicfeature.isElementPresent(bagIconCount))
										{
											int count = Integer.parseInt(bagIconCount.getText());
											log.add("The Product count shown in bag is: "+count);
											if(count>0)
											{
												log.add("Products displayed in shopping bag");
												bagIcon.click();
												wait.until(ExpectedConditions.visibilityOf(shoppingBagPage));			
												if(HBCBasicfeature.isElementPresent(shoppingBagPage))
												{
													log.add("Shopping Bag page displayed");
													prodTitle = shoppingBagProdTitle.getText();
													wait.until(ExpectedConditions.visibilityOf(checkoutButton));			
													if(HBCBasicfeature.isElementPresent(checkoutButton))
													{
														checkoutButton.click();
														wait.until(ExpectedConditions.attributeContains(shippingPaymentPage, "style", "visible"));
														if(HBCBasicfeature.isElementPresent(shippingPaymentPage))
														{
															Pass("Checkout shipping & payment page displayed",log);
															shippingPage = true;
														}
														else
														{
															Fail("Checkout shipping & payment page not displayed",log);
														}																
													}
													else
													{
														Fail("Checkout button not displayed");
													}															
												}
												else
												{
													Fail("Shopping Bag page not displayed");
												}
											}
											else
											{
												Fail("Bag count is '0' no products displayed");
											}
										}
										else
										{
											Fail("Bag icon count not displayed");
										}
									}
									else
									{
										Fail("Bagicon not displayed");
									}
								}
								else
								{
									Fail("MyAccount page not displayed");
								}
							}
							else
							{
								Fail("SignIn button not displayed");
							}
						}
						else
						{
							Fail("Password Field not displayed");
						}
					}
					else
					{
						Fail("Email Field not displayed");
					}
				}
				else
				{
					Fail("Pancake WelcomesignIn option not displayed");
				}
			}
			else
			{
				Fail("Hamburger menuu not displayed");
			}				
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/* SignOut*/
	public void signOut() throws java.lang.Exception
	{
		ChildCreation("SignOut from current account");
		try
		{
			Actions action = new Actions(driver);
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{
					try
					{
						action.moveToElement(hamburger).click().build().perform();
						if(HBCBasicfeature.isElementPresent(pancakeStaticSignOut))
						{
							flag = true;
							break;
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
				while(flag!=true);					
				if(HBCBasicfeature.isElementPresent(pancakeStaticSignOut))
				{
					HBCBasicfeature.scrolldown(pancakeStaticSignOut, driver);
					jsclick(pancakeStaticSignOut);	
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")));	
					Pass("signed out successfully");
				}
				else
				{
					Fail("Signout option not available");
				}
			}
			else
			{
				Fail("Hamburger menu not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}
	}
	
	public void AddPaymentReviewOrderClick()
	{
		if(shippingPage==true)
		{
			try
			{
				reviewSubmit = false;
				String actual = HBCBasicfeature.getExcelVal("HBCCard", sheet, 1);
				String cnumber = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 2);
				String cvv = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 3);
				String expMonth = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 4);
				String expyear = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 5);
				if(HBCBasicfeature.isElementPresent(shipandPaypage))
				{
					Pass("Shipping & payment page displayed");				
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardChkkBox))
					{
						jsclick(paymentOptionsCreditCardChkkBox);
						wait.until(ExpectedConditions.visibilityOf(paymentOptionsCreditCardSelected));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
						{
							Pass( "The Credit Card option was selected.");
							wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecEnable, "style", "block"));
							Thread.sleep(1000);
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
							{
								Pass("Credit type drop down is displayed");
								Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
								sel.selectByValue("MAST");
								wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
								String current = sel.getFirstSelectedOption().getText();
								log.add("The selected by default option is: "+actual);
								log.add("The Current selected option  is: "+current);
								Thread.sleep(500);
								if(actual.equalsIgnoreCase(current))
								{
									Pass("Mastercard option selected");
									if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
									{
										HBCBasicfeature.scrolldown(paymentOptionsCreditCardNumberField, driver);
										paymentOptionsCreditCardNumberField.clear();
										paymentOptionsCreditCardNumberField.sendKeys(cnumber);
										if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
										{
											paymentOptionsCreditCardSecurityCodeField.clear();
											paymentOptionsCreditCardSecurityCodeField.sendKeys(cvv);
											if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
											{
												paymentOptionsCreditCardExpMonthField.clear();
												paymentOptionsCreditCardExpMonthField.sendKeys(expMonth);
												if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
												{
													paymentOptionsCreditCardExpYearField.clear();
													paymentOptionsCreditCardExpYearField.sendKeys(expyear);
													if(HBCBasicfeature.isElementPresent(paymentOptionReviewOrder))
													{
														jsclick(paymentOptionReviewOrder);
														Thread.sleep(2000);
														//wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
														wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
														if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
														{
															Pass("Payment added, hence Review and Submit page displayed");
															reviewSubmit = true;
														}
														else
														{
															Fail("Review and Submit page not displayed");
														}
													}
													else
													{
														Fail("Review order button not displayed");
													}
												}
												else
												{
													Fail("Credit card expiry  year field not displayed");
												}
											}
											else
											{
												Fail("Credit card expiry  month field not displayed");
											}
										}
										else
										{
											Fail("Credit card security code field not displayed");
										}											
									}
									else
									{
										Fail("Credit card number field not displayed");
									}
								}
								else
								{
									Fail("user not able to choose the option in the 'Card type' dropdown field in the 'Payment' column",log);
								}							
							}
							else
							{
								Fail("Credit card dropdown not displayed");
							}
						}
						else
						{
							Fail("Pay by credit card not selected");
						}						
					}
					else
					{
						Fail("Pay by credit card checkbox not displayed");
					}	
				}
				else
				{
					Fail("Shipping & Payment page not displayed");
				}
			}
			catch(Exception e)
			{
				System.out.println( e.getMessage());
				Exception(e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}			
	}
	
	/* Remove added address from AddressBook */
	public void removeAddress(String tc) throws java.lang.Exception
	{
		String defaultaddr = "";
		if(tc=="Super")
		{
			defaultaddr = HBCBasicfeature.getExcelNumericVal("addressVal", sheet, 1);
		}
		else
		{
			defaultaddr = HBCBasicfeature.getExcelNumericVal("addressVal", sheet, 2);
		}
		ChildCreation("Remove added address from AddressBook");	
		try
		{
			Actions action = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(logo));	
			logo.click();
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{
					try
					{
						HBCBasicfeature.scrollup(hamburger, driver);
						action.moveToElement(hamburger).click().build().perform();
						Thread.sleep(2000);
						if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
						{
							flag = true;
							break;
						}
						else
						{
							continue;
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
				while(flag!=true);					
				if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
				{
					HBCBasicfeature.scrolldown(pancakeStaticWelcome, driver);
					action.moveToElement(pancakeStaticWelcome).click().build().perform();
					Thread.sleep(2000);
					wait.until(ExpectedConditions.visibilityOf(myAccountPage));	
					if(HBCBasicfeature.isElementPresent(myAccountPage))
					{
						Pass("MyAccount Page dispalyed");
						if(HBCBasicfeature.isElementPresent(myAccountPageAddressbook))
						{
							action.moveToElement(myAccountPageAddressbook).click().build().perform();
							Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOf(myAccountAddressBookpage));
							wait.until(ExpectedConditions.visibilityOf(myAccountPageAddressDropDown));	
							if(HBCBasicfeature.isElementPresent(myAccountPageAddressDropDown))
							{
								Pass("AddressBook DropDown displayed");
								Select sel = new Select(myAccountPageAddressDropDown);
								int size = sel.getOptions().size();
								if(size>1)
								{
									do
									{
										int i=1;
										String address = sel.getOptions().get(i).getAttribute("value");
										Thread.sleep(500);
										if(address.equals(defaultaddr))
										{
											Pass("Default Address: "+address);
										}
										else
										{
											sel.getOptions().get(i).click();
											Thread.sleep(500);
											myAccountPageAddressRemoveButton.click();
											wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
											Thread.sleep(1000);											
											Pass("Address removed successfully: "+address);											
										}										
										size = sel.getOptions().size();
									}
									while(size!=1);									
								}
								else
								{
									Pass("Not more than one Address displayed");
								}
							}
							else
							{
								Fail("Address DropDown not displayed");
							}							
						}
						else
						{
							Fail("AddressBook option not displayed ");
						}
					}
					else
					{
						Fail("MyAccount Page displayed");
					}				
				}
				else
				{
					Fail("Pancake Welcome option not displayed");
				}
			}
			else
			{
				Fail("Hamburger menu not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	
	/* HUDSONBAY-1613 Reg Checkout - Verify that "Shipping & Payment" section, should be as per the creative*/
	public void ShippingPaymentCreative() throws Exception
		{
			ChildCreation("HUDSONBAY-1613 Reg Checkout - Verify that 'Shipping & Payment' section, should be as per the creative");
			if(shippingPage==true)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(checkoutNavTitle));
					if(HBCBasicfeature.isElementPresent(checkoutNavTitle))
					{
						Pass("Checkout Navigation Tab's are displayed");
					}
					else
					{
						Fail("	Checkout Navigation Tab's are  not displayed");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipSec))
					{
						Pass("Shiping Information section displayed");
					}
					else
					{
						Fail("Shiping Information section  not displayed");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypagePaySec))
					{
						Pass("Payment Information section displayed");
					}
					else
					{
						Fail("Payment Information section  not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCard))
					{
						HBCBasicfeature.scrolldown(paymentOptionsCreditCard, driver);
						Pass( "The Credit Card payment Checkbox is displayed .");
					}
					else
					{
						Fail( "The Credit Card payment Checkbox is not displayed .");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsPaypal))
					{
						Pass( "The Paypal payment Checkbox is displayed .");
					}
					else
					{
						Fail( "The Paypal payment Checkbox is not displayed .");
					}	
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardFieldLabel))
					{	
						HBCBasicfeature.scrolldown(paymentOptionsRewardCardFieldLabel, driver);
						Pass( "The 'HBC rewards card number' is displayed .");						
					}
					else
					{
						Fail( "The 'HBC rewards card number' label is not displayed .");
					}		
					Thread.sleep(500);
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField1))
					{
						Pass( "The Reward card number field-1 is displayed .");
					}
					else
					{
						Fail( "The Reward card number field-1 is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
					{
						Pass( "The Reward card number field-2 is displayed .");
					}
					else
					{
						Fail( "The Reward card number field-2 is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
					{
						Pass( "The Add promo code field is displayed .");
					}
					else
					{
						Fail( "The Add promo code field is not displayed .");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
					{
						Pass( "The Shipping Method dropdown is displayed .");						
					}
					else
					{
						Fail( "The Shipping Method dropdown is not displayed .");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsEstimatedSubtotal))
					{
						Pass("Order Sub Total option displayed");
					}
					else
					{
						Fail("Order Sub Total option not displayed");
					}
					Thread.sleep(500);
					if(HBCBasicfeature.isElementPresent(paymentOptionsShippingAmountlabel))
					{
						HBCBasicfeature.scrolldown(paymentOptionsShippingAmountlabel, driver);
						Pass("Order Shipping Amount option displayed");
					}
					else
					{
						Fail("Order Shipping Amount option not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsPSTLabel))
					{
						Pass("PST label option displayed");
					}
					else
					{
						Fail("PST label option not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsGSTHSTLabel))
					{
						Pass("GST/HST label option displayed");
					}
					else
					{
						Fail("GST/HST label option not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotalLabel))
					{
						Pass("Order Total option displayed");
					}
					else
					{
						Fail("Order Total option not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(paymentOptionsGiftCard))
					{
						Pass( "The Pay by Gift card option is displayed .");						
					}
					else
					{
						Fail( "The Pay by Gift card option is not displayed .");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionReviewOrder))
					{
						Pass( "The Review Order option is displayed .");						
					}
					else
					{
						Fail( "The Review Order option is not displayed .");
					}
					
				}		
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("Shipping & Payment page not displayed");
			}
		}
	
	/* HUDSONBAY-1454 Reg Checkout - Verify that "Addresses", "Shipping & Payment" and "Review & Submit" tabs should be shown*/
	public void checkoutPage_Tabs() throws Exception
		{
			ChildCreation("HUDSONBAY-1454 Reg Checkout - Verify that 'Addresses', 'Shipping & Payment' and 'Review & Submit' tabs should be shown");
			if(shippingPage==true)
			{
				String title = HBCBasicfeature.getExcelVal("HB1454", sheet, 1);
				String[] titles = title.split("\n");
				try
				{
					wait.until(ExpectedConditions.visibilityOf(checkoutNavTitle));
					if(HBCBasicfeature.isElementPresent(checkoutNavTitle))
					{
						if(HBCBasicfeature.isElementPresent(checkoutNavTab1))
						{
							String actualTxt = checkoutNavTab1.getText();
							log.add("The current tab title is: "+actualTxt);
							log.add("The Expected tab title is: "+titles[0]);
							if(actualTxt.equalsIgnoreCase(titles[0]))
							{
								Pass("'Addresses' tab displayed on top of the page",log);
							}
							else
							{
								Fail("'Addresses' tab not displayed on top of the page",log);
							}
						}
						else
						{
							Fail("Address Tab-1 not displayed");
						}
						if(HBCBasicfeature.isElementPresent(checkoutNavTab2))
						{
							String actualTxt = checkoutNavTab2.getText();
							log.add("The current tab title is: "+actualTxt);
							log.add("The Expected tab title is: "+titles[1]);
							if(actualTxt.equalsIgnoreCase(titles[1]))
							{
								Pass("'Shipping & Payment' tab displayed on top of the page",log);
							}
							else
							{
								Fail("'Shipping & Payment' tab not displayed on top of the page",log);
							}
						}
						else
						{
							Fail("Shipping & Payment Tab-2 not displayed");
						}
						if(HBCBasicfeature.isElementPresent(checkoutNavTab3))
						{
							String actualTxt = checkoutNavTab3.getText();
							log.add("The current tab title is: "+actualTxt);
							log.add("The Expected tab title is: "+titles[2]);
							if(actualTxt.equalsIgnoreCase(titles[2]))
							{
								Pass("'Review & Submit' tab displayed on top of the page",log);
							}
							else
							{
								Fail("'Review & Submit' tab not displayed on top of the page",log);
							}
						}
						else
						{
							Fail("Review & Submit Tab-3 not displayed");
						}				
					}
					else
					{
						Fail("	Checkout Navigation Tab's  'Addresses', 'Shipping & Payment' and 'Review & Submit' are  not displayed");
					}				
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("Shipping & Payment page not displayed");
			}
		}
	
	/*HUDSONBAY-1455 Reg Checkout - Verify that the current tabs(breadcrumb) should be highlighted in black, and the other tabs should be grayed out.*/
	public void checkoutCurrentSelectedTab() throws Exception
		{
			ChildCreation("HUDSONBAY-1455 Reg Checkout - Verify that the current tabs(breadcrumb) should be highlighted in black, and the other tabs should be grayed out.");
			if(shippingPage==true)
			{
				String tabColors = HBCBasicfeature.getExcelVal("HB1455", sheet, 2);
				String[] actualColor = tabColors.split("\n");
				try
				{
					if((HBCBasicfeature.isElementPresent(checkoutNavTab1))&&(HBCBasicfeature.isElementPresent(checkoutNavTab3)))
					{
						String color1 = checkoutNavTab1.getCssValue("color");
						String Tab1colour = HBCBasicfeature.colorfinder(color1);
						String color2 = checkoutNavTab3.getCssValue("color");
						String Tab3colour = HBCBasicfeature.colorfinder(color2);
						
						log.add("Expected Tab 1 & 3 color  is: "+actualColor[0]);
						log.add("Current Tab-1 color is: "+Tab1colour);
						log.add("Current Tab-3 color is: "+Tab3colour);
						
						if((Tab1colour.equalsIgnoreCase(actualColor[0]))&&(Tab3colour.equalsIgnoreCase(actualColor[0])))
						{
							Pass("Other tabs colors are matched and in grayed out color",log);
						}
						else
						{
							Fail("Other tabs are not matched and not in grayed out color",log);
						}
					}
					else
					{
						Fail("Checkout Navigation Tabs 1 & 3 are not displayed");
					}
					
					if(HBCBasicfeature.isElementPresent(checkoutCurrNavTitle))
					{
						String color = checkoutCurrNavTitle.getCssValue("color");
						String currentTabColor = HBCBasicfeature.colorfinder(color);
						log.add("Expected current selected tab color  is: "+actualColor[1]);
						log.add("Current selected tab color is: "+currentTabColor);
						if(currentTabColor.equalsIgnoreCase(actualColor[1]))
						{
							Pass("Current tab highlighted in black color as per creative",log);
						}
						else
						{
							Fail("Current tab not highlighted in black color",log);
						}
					}
					else
					{
						Fail("Checkout Navigation Current selected tab not displayed");
					}				
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("Shipping & Payment page not displayed");
			}
		}
	
	/* HBC - 1456 Reg Checkout - Verify that "Shipping & Payment" tab should have "Shipping Information" & "Billing Information" section.*/
	public void ShipBillingSections()
	{
		ChildCreation(" HBC - 1456 Reg Checkout - Verify that Shipping & Payment tab should have Shipping Information & Billing Information section.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipSec))
				{
					Pass( "The Shipping Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Shipping Section in the Shipping and Billing Tab is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypagePaySec))
				{
					Pass( "The Billing Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Billing Section in the Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1456 Issue ." + e.getMessage());
				Exception(" HBC - 1456 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ("Shipping & Payment page not displayed");
		}
	}
	
	/*HUDSONBAY-1457 Reg Checkout - Verify that "Please edit your shipping address..." alert should be shown, when no shipping details is present.*/
	public void editShippingAlert() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-1457 Reg Checkout - Verify that 'Please edit your shipping address...' alert should be shown, when no shipping details is present.");
			String expctdAlert = HBCBasicfeature.getExcelVal("HB1457", sheet, 1);
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(shippingAlert))
					{
						String currentAlert = shippingAlert.getText();
						log.add("The Expected alert to be displayed is: "+expctdAlert);
						log.add("The Actual alert displayed is: "+currentAlert);
						if(currentAlert.equalsIgnoreCase(expctdAlert))
						{
							Pass("'Please edit your shipping address...' alert shown, when no shipping details is present",log);
						}
						else
						{
							Fail("'Please edit your shipping address...' alert not shown, when no shipping details is present",log);
						}
					}
					else
					{
						Fail("Edit shipping Alert not dispayed");
					}					
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("Shipping & Payment page not displayed");
			}
		}
	
	/*HUDSONBAY-1458 Reg Checkout - Verify that "Please edit your billing address..." alert should be shown, when no billing details is present.*/
	public void editBillingAlert() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-1458 Reg Checkout - Verify that 'Please edit your billing address...' alert should be shown, when no billing details is present.");
			String expctdAlert = HBCBasicfeature.getExcelVal("HB1458", sheet, 1);
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentAlert))
					{
						String currentAlert = paymentAlert.getText();
						log.add("The Expected alert to be displayed is: "+expctdAlert);
						log.add("The Actual alert displayed is: "+currentAlert);
						if(currentAlert.equalsIgnoreCase(expctdAlert))
						{
							Pass("'Please edit your billing address...' alert shown, when no billing details is present",log);
						}
						else
						{
							Fail("'Please edit your billing address...' alert not shown, when no billing details is present",log);
						}
					}
					else
					{
						Fail("Edit billing Alert not dispayed");
					}					
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("Shipping & Payment page not displayed");
			}
		}
	
	/* HBC - 1459 Reg Checkout - Shipping Information Section > Verify that Selected shipping address should be shown with "Edit" text link & "Create Address" text links below the shipping information section.*/
	public void ShipSectionsEditCreateLinks()
	{
		ChildCreation(" HBC - 1459 Reg Checkout - Shipping Information Section > Verify that Selected shipping address should be shown with Edit text link & Create Address text links below the shipping information section.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					Pass( "The Create Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Create Link in the Shipping Section in the Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1250 Issue ." + e.getMessage());
				Exception(" HBC - 1250 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1460 Reg Checkout - Shipping Information Section > Verify that Selected shipping address should be highlighted as per the creative in the shipping information section.*/
	public void ShipSectionsAddressHighlight() throws Exception
	{
		ChildCreation(" HBC - 1460 Reg Checkout - Shipping Information Section > Verify that Selected shipping address should be highlighted as per the creative in the shipping information section");
		String expected = HBCBasicfeature.getExcelVal("HBC1460", sheet, 2);
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressColorDisp))
				{
					String act = shipandPaypageShipAddressColorDisp.getCssValue("color");
					String actual = HBCBasicfeature.colorfinder(act);
					log.add("Actual higlighted color is: "+actual);
					log.add("Expected highlighted color is: "+expected);
					Thread.sleep(500);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("Selected shipping address highlighted as per the creative in the shipping information section",log);
					}
					else
					{
						Fail("Selected shipping address not highlighted as per the creative in the shipping information section",log);
					}					
				}
				else
				{
					Fail("Shipping address not displayed");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1460 Issue ." + e.getMessage());
				Exception(" HBC - 1460 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
		
	/* HBC - 1464 Register Checkout - Shipping Information Section > Verify that "X" close icon should be shown in the "Shipping Address" overlay/page.*/
	/* HBC - 1465 Register Checkout - Shipping Information Section > Verify that "Ok" button & "Cancel" button should be shown in the "Shipping Address" overlay/page.*/
	/* HBC - 1467 Register Checkout - Shipping Information Section > Shipping Information Section > Verify that on tapping the "X" icon, the "Shipping Address" overlay should be closed*/
	/* HBC - 1462 Register Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be closed on tapping the "Cancel" button.*/
	/* HBC - 1466 Register Checkout - Shipping Information Section > Verify that clicking on "Cancel" button after making changes in the "Shipping Address" overlay/page should be displayed without any changes.*/
	public void ShipSectionsShipEditLinkOverlay()
	{
		boolean addOverlayState = false, addOverlayCloseState = false;
		String befAdd = "";	
				
		if(shippingPage==true)
		{
			if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
			{
				befAdd = shipandPaypageShipAddressDisp.getText();
			}
			
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					jsclick(shipandPaypageShipEditLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						addOverlayState = true;
						Pass("The Edit Shipping Address Overlay is dispalyed.");
					}
					else
					{
						Fail("The Edit Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			/* HBC - 1464 Register Checkout - Shipping Information Section > Verify that "X" close icon should be shown in the "Shipping Address" overlay/page.*/
			ChildCreation(" HBC - 1464 Reg Checkout - Shipping Information Section > Verify that X close icon should be shown in the Shipping Address overlay/page.");
			try
			{
				if(addOverlayState==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
					{
						Pass( "The X Close Link in Edit overlay is displayed.");
					}
					else
					{
						Fail( "The X Close Link in Edit overlay is not displayed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1464 Issue ." + e.getMessage());
				Exception(" HBC - 1464 Issue ." + e.getMessage());
			}
			
			/* HBC - 1465 Register Checkout - Shipping Information Section > Verify that "Ok" button & "Cancel" button should be shown in the "Shipping Address" overlay/page..*/
			ChildCreation(" HBC - 1465 Register Checkout - Shipping Information Section > Verify that Ok button & Cancel button should be shown in the Shipping Address overlay/page.");
			try
			{
				if(addOverlayState==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
					{
						Pass( "The Cancel Link in Edit overlay is displayed.");
					}
					else
					{
						Fail( "The Cancel Link in Edit overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
					{
						Pass( "The Ok button in Edit overlay is displayed.");
					}
					else
					{
						Fail( "The Ok button in Edit overlay is not displayed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1465 Issue ." + e.getMessage());
				Exception(" HBC - 1465 Issue ." + e.getMessage());
			}
			
			/* HBC - 1467 Register Checkout -  Shipping Information Section > Verify that on tapping the "X" icon, the "Shipping Address" overlay should be closed*/
			ChildCreation(" HBC - 1467 Reg Checkout - Shipping Information Section > Verify that on tapping the 'X' icon, the 'Shipping Address' overlay should be closed");
			try
			{
				if(addOverlayState==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
					{
						jsclick(shipandPaypageShipShipCreateEditAddressCloseLink);	
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
						Thread.sleep(1000);
						if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
						{
							Pass( "On tapping the X Close Link 'Shipping Address' overlay closed.");
							addOverlayCloseState = false;
						}
						else
						{
							Fail("On tapping the X Close Link 'Shipping Address' overlay  not closed.");
						}						
					}
					else
					{
						Fail( "The X Close Link in Edit overlay is not displayed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1467 Issue ." + e.getMessage());
				Exception(" HBC - 1467 Issue ." + e.getMessage());
			}
			
			/* HBC - 1462 Register Checkout - Shipping Information Section > Verify that clicking on "Cancel" button after making changes in the "Shipping Address" overlay/page should be displayed without any changes.*/
			ChildCreation(" HBC - 1462 Register Checkout - Shipping Information Section > Verify that clicking on Cancel button after making changes in the Shipping Address overlay/page should be displayed without any changes.");
			try
			{
				if(addOverlayCloseState == false)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
					{
						Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
						jsclick(shipandPaypageShipEditLink);
						Thread.sleep(3000);
						wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
						{
							addOverlayState = true;
						}
					}
					else
					{
						Fail("Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed");
					}
				}
				else
				{
					Skip("Edit overlay not in closed state");
				}
				
				if(addOverlayState==true)
				{
					String aftAdd = "";							
					String fName = HBCBasicfeature.getExcelVal("HB1462", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name Field in the Edit form is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(fName);
						log.add("Entered value in first name field is: "+shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value"));
					}
					else
					{
						Fail( "The First Name in the Edit field is not displayed.");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
					{
						Pass( "The Cancel button in the Edit Overlay is displayed.");
						jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
						Thread.sleep(1000);
						if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
						{
							addOverlayCloseState = true;
							aftAdd = shipandPaypageShipAddressDisp.getText();
							log.add( "The address before modification is  : " + befAdd);
							log.add( "The address after modification is  : " + aftAdd);
							if((befAdd.isEmpty())||(aftAdd.isEmpty()))
							{
								Fail( "The user failed to get the before address and after address they are empty.",log);
							}
							else if(befAdd.equals(aftAdd))
							{
								Pass( " There is no modification done when cancel is clicked in the Address Verification Page.",log);
							}
							else
							{
								Fail( "There is mismatch in the before address and after address.",log);
							}
						}
						else
						{
							Fail("The Saved Address list is not displayed.");
						}
					}
					else
					{
						Fail( "The Cancel button in the Edit Overlay is displayed is not displayed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1462 Issue ." + e.getMessage());
				Exception(" HBC - 1462 Issue ." + e.getMessage());
			}
			
			/* HBC - 1466 Register Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be closed on tapping the "Cancel" button.*/
			ChildCreation(" HBC - 1466 Register Checkout - Shipping Information Section > Verify that Shipping Address overlay/page should be closed on tapping the Cancel button.");
			try
			{
				if(addOverlayCloseState==true)
				{
					Pass( "The Address Edit Overlay was closed when cancel button was clicked.");
				}
				else
				{
					Skip ( "The address edit overlay was not enabled in the first place to close.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1466 Issue ." + e.getMessage());
				Exception(" HBC - 1466 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " Shipping & payment page not displayed");
		}
	}
	
	/* HBC - 1468 Register Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be as per the creative.*/
	public void ShipPageCreateShipOverlay()
	{
		ChildCreation("HBC - 1468 Register Checkout - Shipping Information Section > Verify that Shipping Address overlay/page should be as per the creative.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					jsclick(shipandPaypageShipCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The First Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Last Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Street Address is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The City Field is not displayed in the Shipping Address Page.");
						}

						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Country is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
						{
							Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Phone Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Close Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
						{
							Pass( "The Cancel Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Cancel Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
						{
							Pass( "The Ok button in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Ok button in Create overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1468 Issue ." + e.getMessage());
				Exception(" HBC - 1468 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " Shipping & payment page not displayed");
		}
	}
	
	/* HBC - 1470 Register Checkout - Verify that "Shipping Address" overlay/page should have the following text boxes : "First Name", "Last Name", "Street Address", and so on.*/
	/* HBC - 1473 Register Checkout - Verify that required input fields needs to be displayed to add a new shipping address.*/
	public void ShipPageCreateShipOverlayFields(String tc)
	{
		String tcId = "";
		if(tc=="HBC1470")
		{
			tcId = " HBC - 1470 Register Checkout - Verify that Shipping Address overlay/page should have the following text boxes : First Name, Last Name, Street Address, and so on. ";
		}
		else
		{
			tcId = " HBC - 1473 Register Checkout - Verify that required input fields needs to be displayed to add a new shipping address.";
		}
		ChildCreation(tcId);
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The City Field is not displayed in the Shipping Address Page.");
					}

					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Pass( "The State Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The State Field is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The PO Code Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The PO Code is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
					{
						Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Phone Code is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
					{
						Pass( "The Close Link in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Close Link in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
					{
						Pass( "The Cancel Link in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Cancel Link in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
					{
						Pass( "The Ok button in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Ok button in Create overlay is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - HBC1470 Issue ." + e.getMessage());
				Exception(" HBC - HBC1470 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " Shipping & payment page not displayed");
		}
	}
	
	/* HBC - 1471 Register Checkout - Verify that mandatory field names should be mentioned with star(*) in the left side of textbox in the "Shipping Address" overlay*/
	public void ShipPageCreateShipOverlayMandatoryFields() throws Exception
	{
		ChildCreation("HBC - 1471 Register Checkout - Verify that mandatory field names should be mentioned with star(*) in the left side of textbox in the 'Shipping Address' overlay");
		String star = HBCBasicfeature.getExcelVal("HBC1471", sheet, 1);
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssFnameLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The First Name field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The First Name field displayed in the Shipping Address Page not mentioned with star(*).");
						}						
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssLNameLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The Last Name field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The Last Name field displayed in the Shipping Address Page not mentioned with star(*).");
						}										
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssStAddressLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The  Street Address field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The  Street Address displayed in the Shipping Address Page not mentioned with star(*).");
						}										
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssCtyLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The  City field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The City field displayed in the Shipping Address Page not mentioned with star(*).");
						}			
					}
					else
					{
						Fail( "The City Field is not displayed in the Shipping Address Page.");
					}

					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssCountryLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The  Country field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The Country field displayed in the Shipping Address Page not mentioned with star(*).");
						}			
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddressStateLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The  State field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The State field displayed in the Shipping Address Page not mentioned with star(*).");
						}		
					}
					else
					{
						Fail( "The State Field is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssPOcodeLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The  PO Code field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The PO Code field displayed in the Shipping Address Page not mentioned with star(*).");
						}		
					}
					else
					{
						Fail( "The PO Code is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssPHLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The Phone Code field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The Phone Code field displayed in the Shipping Address Page not mentioned with star(*).");
						}		
					}
					else
					{
						Fail( "The Phone Code is not displayed in the Shipping Address Page.");
					}										
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}			
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1471 Issue ." + e.getMessage());
				Exception(" HBC - 1471 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "Shipping & payment page not displayed");
		}
	}
	
	/* HBC - 1475 Reg  Checkout - Verify that user should be able to enter first name in the "First Name" field and it should accept maximum of 64 characters.*/
	public void ShipPageCreateShipOverlayFNLenVal()
	{
		ChildCreation("HBC - 1475 Reg  Checkout - Verify that user should be able to enter first name in the First Name field and it should accept maximum of 64 characters.");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1475", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").isEmpty())
							{
								Fail( "The First Name is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").length()<=64)
							{
								Pass( "The first name was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The first name was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssFName.clear();
						}
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1475 Issue ." + e.getMessage());
				Exception(" HBC - 1475 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
		
	/* HBC - 1476 Reg Checkout - Verify that user should be able to enter last name in the "Last Name" field and it should accept maximum of 64 characters.*/
	public void ShipPageCreateShipOverlayLNLenVal()
	{
		ChildCreation(" HBC - 1476 Reg Checkout - Verify that user should be able to enter last name in the Last Name field and it should accept maximum of 64 characters.");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1475", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").isEmpty())
							{
								Fail( "The Last Name is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").length()<=64)
							{
								Pass( "The Last Name  was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Last Name  was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssLName.clear();
						}
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1476 Issue ." + e.getMessage());
				Exception(" HBC - 1476 Issue ." + e.getMessage());
			}
		}
		else
		{
			 Skip( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1477 Reg Checkout - Verify that only characters should be accepted in "First Name" and "Last Name" field.*/
	public void FNLNameNumValidation()
	{
		ChildCreation(" HBC - 1477 Reg Checkout - Verify that only characters should be accepted in First Name and Last Name field..");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HB1477", sheet, 1).split("\n");
				String cty = HBCBasicfeature.getExcelVal("HBC1477", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1477", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1477", sheet, 6);
				String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 7);
				String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 8);
				String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 9);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[0].toString());
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[0]);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[1].toString());
						log.add( "The Value from the excel file was : " + cellVal[1].toString());
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[1]);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						sel.selectByValue("CA");
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Pass( "The State field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
						sel.selectByValue(prv);
						Thread.sleep(1000);
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					boolean addOverlay = ldNoAddressVerification();
					
					if(addOverlay==true)
					{
						Pass( "The User is not navigated to the Address Verification page.");
					}
					else
					{
						Fail( "The User is navigated to the Address Verification page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1477 Issue ." + e.getMessage());
				Exception(" HBC - 1477 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1479 Reg Checkout - Shipping Address Section > Verify that "Street Address" field should have two text boxes.*/
	public void StreetAddFields()
	{
		ChildCreation(" HBC - 1479 Reg Checkout - Shipping Address Section > Verify that Street Address field should have two text boxes.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
				{
					Pass( "The Street Address field is displayed.");
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress2))
				{
					Pass( "The Street Address 2 field is displayed.");
				}
				else
				{
					Fail( "The Street Address 2 field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1479 Issue ." + e.getMessage());
				Exception(" HBC - 1479 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/*HBC - 1480 Reg Checkout - Shipping Address Section > Verify that user should be able to enter street address in the "Street Address" field and it should accept character as per the classic site*/
	public void StAddFieldsLenVal()
	{
		ChildCreation(" HBC - 1480 Reg Checkout - Shipping Address Section > Verify that user should be able to enter street address in the 'Street Address' field and it should accept character as per the classic site");
		if(shippingPage==true)
		{
			try
			{
				String[] st1 = HBCBasicfeature.getExcelVal("HBC1480", sheet, 1).split("\n");
				String[] st2 = HBCBasicfeature.getExcelVal("HBC1480", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
				{
					Pass( "The Street Address field is displayed.");
					for(int i = 0; i<st1.length;i++)
					{
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						log.add( "The Entered value from the excel file is : " + st1[i].toString());
						log.add( "The Value from the excel file was : " + st1[i].toString() + " and is length is : " + st1[i].toString().length());
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(st1[i]);
						if(shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value").isEmpty())
						{
							Fail( "The Street Address is empty.");
						}
						else if(shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value").length()>=70)
						{
							Pass( "The Street Address was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Street Address was enterable but it is not within the specified character limit.",log);
						}
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
					}
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress2))
				{
					Pass( "The Street Address 2 field is displayed.");
					for(int i = 0; i<st2.length;i++)
					{
						shipandPaypageShipShipCreateEditAddresssStAddress2.clear();
						log.add( "The Entered value from the excel file is : " + st2[i].toString());
						log.add( "The Value from the excel file was : " + st2[i].toString() + " and is length is : " + st2[i].toString().length());
						shipandPaypageShipShipCreateEditAddresssStAddress2.sendKeys(st2[i]);
						if(shipandPaypageShipShipCreateEditAddresssStAddress2.getAttribute("value").isEmpty())
						{
							Fail( "The Street Address 2 is empty.");
						}
						else if(shipandPaypageShipShipCreateEditAddresssStAddress2.getAttribute("value").length()>=50)
						{
							Pass( "The Street Address 2 was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Street Address 2 was enterable but it is not within the specified character limit.",log);
						}
						shipandPaypageShipShipCreateEditAddresssStAddress2.clear();
					}
				}
				else
				{
					Fail( "The Street Address 2 field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1480 Issue ." + e.getMessage());
				Exception(" HBC - 1480 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1481 Reg Checkout - Shipping Address Section > Verify that user should be able to enter characters in the "City" field.*/
	public void CityEnterable()
	{
		ChildCreation(" HBC - 1481 Reg Checkout - Shipping Address Section > Verify that user should be able to enter characters in the City field.");
		if(shippingPage==true)
		{
			try
			{
				String cityy = HBCBasicfeature.getExcelVal("HBC1481", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
				{
					Pass( "The City field is displayed.");
					shipandPaypageShipShipCreateEditAddresssCty.click();
					shipandPaypageShipShipCreateEditAddresssCty.clear();
					log.add( "The Entered value from the excel file is : " + cityy);
					shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cityy);
					String ctyVal = shipandPaypageShipShipCreateEditAddresssCty.getAttribute("value");
					
					if(ctyVal.isEmpty())
					{
						Fail( "The city field is empty.",log);
					}
					else
					{
						Pass( "The city name was enterable.",log);
					}
					shipandPaypageShipShipCreateEditAddresssCty.clear();
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1481 Issue ." + e.getMessage());
				Exception(" HBC - 1481 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1482 Reg Checkout - Shipping Address Section > Verify that "City" field should accept numbers and special characters and the corresponding alert message should be shown.
	public void CityyValidation()
	{
		ChildCreation(" HBC - 1482 Reg Checkout - Shipping Address Section > Verify that City field should accept numbers and special characters and the corresponding alert message should be shown.");
		WebDriverWait wait = new WebDriverWait(driver,5);
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1482", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1482", sheet, 2);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
				{
					Pass( "The First Name field is displayed.");
					shipandPaypageShipShipCreateEditAddresssCty.click();
					shipandPaypageShipShipCreateEditAddresssCty.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cellVal);
					shipandPaypageShipShipCreateEditAddresssCty.click();
					wait.until(ExpectedConditions.attributeContains(checkoutSCityAlert,"style","block"));
					String cityAlert = checkoutSCityAlert.getText();
					
					if(cityAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The City alert was as expected.",log);
					}
					else
					{
						Fail( "The City alert was not as expected.",log);
					}
					shipandPaypageShipShipCreateEditAddresssCty.clear();
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1482 Issue ." + e.getMessage());
				Exception(" HBC - 1482 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	*/
		
	/* HBC - 1483 Reg Checkout - Verify that first name & last name fields accepts empty spaces in "Shipping Address" overlay/page.*/
	public void FNLNameNumSpaceValidation()
	{
		ChildCreation(" HBC - 1483 Reg Checkout - Verify that only characters should be accepted in First Name and Last Name field..");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1483", sheet, 1).split("\n");
				String cty = HBCBasicfeature.getExcelVal("HBC1477", sheet, 4);
				String adr = HBCBasicfeature.getExcelVal("HBC1483", sheet, 2);

				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[0].toString());
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[0]);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[1].toString());
						log.add( "The Value from the excel file was : " + cellVal[1].toString());
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[1]);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(adr);
					}
					else
					{
						Fail( "The City field is not displayed.");
					}

					boolean addOverlay = ldAddressVerification();
					if(addOverlay==true)
					{
						Pass( "The User is able to enter the First and Last Name with the Spaces and they are navigated to the Address Verification page.",log);
					}
					else
					{
						Fail( "The User is navigated to the Address Verification page when they enter the First name and Last name with the Spaces.",log);
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1483 Issue ." + e.getMessage());
				Exception(" HBC - 1483 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1484 Reg Checkout - Verify that "Country" field should have only one dropdown option.*/
	public void ShipPageCreateShipStreetCountryDDSizes()
	{
		ChildCreation(" HBC - 1484 Reg Checkout - Verify that Country field should have only one dropdown option.");		
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						int size = sel.getOptions().size();
						if(size==1)
						{
							Pass( "The Country Drop down has no more than one values.");
						}
						else
						{
							for(int i = 0; i<size; i++)
							{
								log.add( "The Displayed Country name is : " + sel.getOptions().get(i).getText());
							}
							Fail( "The Country Drop down has more than one values. The total value is : " + size +". The displayed countries in the list are : ", log);
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1484 Issue ." + e.getMessage());
				Exception(" HBC - 1484 Issue ." + e.getMessage());
			}			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1485 Reg Checkout - Verify that "Country" field should should be selected in "Canada" option by default.*/
	public void ShipPageCreateShipStreetDefaultCountry() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1485 Reg Checkout - Verify that Country field should should be selected in Canada option by default.");
		if(shippingPage==true)
		{
			boolean dropdownCanada = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1485", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String txt = sel.getFirstSelectedOption().getText().replaceAll("\\s", "");
						log.add( "The Expected Value was : " + expVal);
						log.add( "The Actual Value is : " + txt);
						if(txt.isEmpty())
						{
							Fail( "The Country Selection seems to be empty.");
						}
						else if(txt.equalsIgnoreCase(expVal))
						{
							Pass( " The Default Selected country is as expected.",log);
							dropdownCanada = true;
						}
						else
						{
							Fail( "There is mismatch in the expected and the actual default country selection.",log);
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1485 Issue ." + e.getMessage());
				Exception(" HBC - 1485 Issue ." + e.getMessage());
			}
			
			ChildCreation(" HBC - 1492 Reg Checkout -  Verify that country drop should be displayed only 'canada'");
			if(dropdownCanada==true)
			{
				Pass("Country drop down displayed 'canada' only");
			}
			else
			{
				Fail("Country drop down not displayed 'canada' only");
			}
			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
				
	/* HBC - 1486 Reg Checkout - Verify that "Province" dropdown field should be selected in "Select" option by default.*/
	public void ShippingCADefaultStateSel()
	{
		ChildCreation(" HBC - 1486 Reg Checkout - Verify that Province dropdown field should be selected in Select option by default.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1486", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
						sel.selectByIndex(0);
						String actVal = sel.getFirstSelectedOption().getText();
						log.add( "The Expected Value was : " + expVal);
						log.add( "The Actual Value is : " + actVal);
						if(expVal.equalsIgnoreCase(actVal))
						{
							Pass( "The Expected Default Value is selected.",log);
						}
						else
						{
							Fail( "The Expected Default Value is not selected.",log);
						}
					}
					else
					{
						Fail( "The State drop down is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1486 Issue ." + e.getMessage());
				Exception(" HBC - 1486 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1487 Reg Checkout - Billing Address Section > Verify that "Zip Code" field should accept alphanumeric characters.*/
	public void ShippingZipAlphaNumericValidation()
	{
		ChildCreation(" HBC - 1487 Reg Checkout - Billing Address Section > Verify that Zip Code field should accept alphanumeric characters.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
					
					String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1487", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						String val = bpostalCode.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + cellVal);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(val.equalsIgnoreCase(cellVal))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1487 Issue ." + e.getMessage());
				Exception(" HBC - 1487 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1488 Reg Checkout - Verify that "Postal Code" field should accept maximum of 7 characters.*/
	public void ShippingCAPOLength()
	{
		ChildCreation(" HBC - 1488 Reg Checkout - Verify that Postal Code field should accept maximum of 7 characters.");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1488", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code Area is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").isEmpty())
							{
								Fail( "The Zip Code field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").length()<=7)
							{
								Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						}
					}
					else
					{
						Fail( "The Postal Code Area is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1488 Issue ." + e.getMessage());
				Exception(" HBC - 1488 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1489 Reg Checkout - Verify that when the user entered less than 6 digits in the "Postal Code" field, then the corresponding alert message should be shown.*/
	public void ShippingCAPOLengthValidationAlert()
	{
		ChildCreation(" HBC - 1489 Reg Checkout - Verify that when the user entered less than 6 digits in the Postal Code field, then the corresponding alert message should be shown.");
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1489", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					String alert = HBCBasicfeature.getExcelVal("HBC1489", sheet, 2);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The expected alert was : " + alert.toString());
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						String poAlert = checkoutBPOAlert.getText();
						
						if(poAlert.equalsIgnoreCase(alert.toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1489 Issue ." + e.getMessage());
				Exception(" HBC - 1489 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1490 Reg Checkout - Verify that when the user entered alphabets or numbers in the "Postal Code" field, then the corresponding alert message should be shown.*/
	public void ShippingCAPOAlphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1490 Reg Checkout - Verify that when the user entered alphabets or numbers in the Postal Code field, then the corresponding alert message should be shown.");
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1490", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					String alert = HBCBasicfeature.getExcelVal("HBC1490", sheet, 2);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The expected alert was : " + alert.toString());
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						String poAlert = checkoutBPOAlert.getText();
						
						if(poAlert.equalsIgnoreCase(alert.toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1490 Issue ." + e.getMessage());
				Exception(" HBC - 1490 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1493 Reg Checkout - Verify that user should be able to enter phone number in the "Phone Number" field and it should accept maximum of 10 numbers.*/
	public void ShippingCAPHNumLenValidation()
	{
		ChildCreation(" HBC - 1493 Reg Checkout - Verify that when the user entered alphabets or numbers in the Postal Code field, then the corresponding alert message should be shown.");
		if(shippingPage==true)
		{
			try 
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Postal Code Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1493", sheet, 1).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").length()<=3)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Postal Code Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1493", sheet, 2).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").length()<=3)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1493", sheet, 3).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").length()<=4)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1493 Issue ." + e.getMessage());
				Exception(" HBC - 1493 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1494 Reg Checkout - Verify that corresponding alert message should be shown when the user entered alphabets or special characters in the "Phone Number" field.*/
	public void ShippingCAPHAphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1494 Reg Checkout - Verify that corresponding alert message should be shown when the user entered alphabets or special characters in the Phone Number field.");
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1494", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					String[] alert = HBCBasicfeature.getExcelVal("HBC1494", sheet, 2).split("\n");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert1,"style","block"));
						String ctAlert = checkoutSPHAlert1.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[0].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert3,"style","block"));
						String ctAlert = checkoutSPHAlert3.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[1].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1494 Issue ." + e.getMessage());
				Exception(" HBC - 1494 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1495 Reg Checkout - Verify that when the "Phone Number" field is left blank and tapping on other text boxes, then the corresponding alert message should be shown.*/
	public void ShippingCAPHEmptyValidationAlert()
	{
		ChildCreation(" HBC - 1495 Reg Checkout - Verify that when the Phone Number field is left blank and tapping on other text boxes, then the corresponding alert message should be shown.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
				{
					jsclick(shipandPaypageShipShipCreateEditAddressCloseLink);	
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
					{
						Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
						jsclick(shipandPaypageShipEditLink);
						Thread.sleep(3000);
						wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
						{
							Pass("The Create Shipping Address Overlay is dispalyed.");
							Select sel;
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
							{
								sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
								String val = sel.getFirstSelectedOption().getText();
								boolean cnty = val.contains("Canada");
								if(cnty==false)
								{
									sel.selectByValue("CA");
									wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
								}
							}						
							String[] alert = HBCBasicfeature.getExcelVal("HBC1495", sheet, 1).split("\n");
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))))
							{
								Pass( "The Postal Code Area is displayed.");
								shipandPaypageShipShipCreateEditAddresssPH1.clear();
								shipandPaypageShipShipCreateEditAddresssPH2.clear();
								shipandPaypageShipShipCreateEditAddresssPH3.clear();
								shipandPaypageShipShipCreateEditAddresssPOcode.click();
								wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert3,"style","block"));
								String ctAlert = checkoutSPHAlert3.getText();
								
								if(ctAlert.equalsIgnoreCase(alert[0].toString()))
								{
									Pass( "The Phone Number alert was as expected.",log);
								}
								else
								{
									Fail( "The Phone Number alert was not as expected.",log);
								}
								shipandPaypageShipShipCreateEditAddresssPH1.clear();
							}
							else
							{
								Fail( "The Contact Number is not displayed.");
							}
						}
						else
						{
							Fail("The Create Shipping Address Overlay is not dispalyed.");
						}
					}
					else
					{
						Fail("Edit Address link not displayed");
					}
				}
				else
				{
					Fail("X close option not displayed");
				}				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1495 Issue ." + e.getMessage());
				Exception(" HBC - 1495 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1496 Reg Checkout - Verify that alert must be shown on entering only space in any of the input fields and selecting "Ok" button.*/
	public void ShipPageEmptyAddValidation()
	{
		ChildCreation(" HBC - 1496 Reg Checkout - Verify that alert must be shown on entering only space in any of the input fields and selecting Ok button.");
		if(shippingPage==true)
		{
			try
			{
				Actions act = new Actions(driver);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssFName).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssStAddress).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssCty).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssPOcode).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssPH1).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssPH2).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssPH3).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					WebDriverWait w1 = new WebDriverWait(driver, 5);
					boolean addOverlay = true;
					do
					{
						try
						{
							shipandPaypageShipShipCreateEditAddressOkBtn.click();
							w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							addOverlay = true;
							if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
							{
								jsclick(addressVerificationEdit);
								wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
								Thread.sleep(1000);
							}
						}
						catch(Exception e)
						{
							addOverlay = false;
						}
					}while(addOverlay==true);
					
					if(addOverlay==false)
					{
						Pass( "The User is not navigated to the Address Verification page.");
					}
					else
					{
						Fail( "The User is navigated to the Address Verification page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1496 Issue ." + e.getMessage());
				Exception(" HBC - 1496 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1474 Verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address.*/
	public void ShippingAddressFieldsTabFocus()
	{
		ChildCreation(" HBC - 1474 Verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{			
					Actions act = new Actions(driver);
					shipandPaypageShipShipCreateEditAddresssFName.click();
					for(int i = 1; i < 12 ; i++)
					{
						act.sendKeys(Keys.TAB).build().perform();
						Pass("The tab focus for " +  i  + " field is made successfully.");
					}
				}
				else
				{
					Fail( "The Edit Shipping Address overlay is not displayed.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1474 Issue ." + e.getMessage());
				Exception(" HBC - 1474 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1497 Reg Checkout - Verify that on entering html tags in the fields should not be treated as strings.*/
	/* HBC - 1498 Reg Checkout - Verify that on entering the values to the fields where alert is displayed, the alert should be dismissed automatically.*/
	public void ShipPageAddShippAddressHTMLValidation()
	{
		ChildCreation(" HBC - 1497 Reg Checkout - Verify that on entering html tags in the fields should not be treated as strings.");
		boolean HBC1498 = true;
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1497", sheet, 1);
				String[] cellVall = HBCBasicfeature.getExcelVal("HB1477", sheet, 1).split("\n");
				String pocode = HBCBasicfeature.getExcelVal("HBC1477", sheet, 6);
				String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 7);
				String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 8);
				String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 9);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVall[0]);
						String prevVal = shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal);
						boolean addOverlay = ldNoAddressVerification();
						if(addOverlay==true)
						{
							Pass( "The User is able to Html but it was not treated as strings.");
						}
						else
						{
							Fail( "The User is navigated to the Address Verification page when they enter the html tags.");
						}
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(prevVal);
						shipandPaypageShipShipCreateEditAddresssLName.click();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVall[1]);
						String prevVal = shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal);
						boolean addOverlay = ldNoAddressVerification();
						if(addOverlay==true)
						{
							Pass( "The User is able to Html but it was not treated as strings.");
						}
						else
						{
							Fail( "The User is navigated to the Address Verification page when they enter the html tags.");
						}
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(prevVal);
						shipandPaypageShipShipCreateEditAddresssStAddress.click();
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						String prevVal = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						boolean addOverlay = ldNoAddressVerification();
						if(addOverlay==true)
						{
							Pass( "The User is able to Html but it was not treated as strings.");
						}
						else
						{
							Fail( "The User is navigated to the Address Verification page when they enter the html tags.");
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(prevVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						if(HBCBasicfeature.isElementPresent(checkoutBPOAlert))
						{
							Fail( "The PO Alert is not dismissed.");
							if(HBC1498 == true)
							{
								HBC1498 = false;
							}
						}
						else
						{
							Pass( "The PO Alert is dismissed.");
							if(HBC1498 == true)
							{
								HBC1498 = true;
							}
						}
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						Thread.sleep(200);
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						Thread.sleep(200);
						shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						shipandPaypageShipShipCreateEditAddresssPH3.click();
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						Thread.sleep(200);
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);

						String prevVal = shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value");
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						Thread.sleep(200);
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal);
						boolean addOverlay = ldNoAddressVerification();
						if(addOverlay==true)
						{
							Pass( "The User is able to Html but it was not treated as strings.");
						}
						else
						{
							Fail( "The User is navigated to the Address Verification page when they enter the html tags.");
						}
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(prevVal);
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						Thread.sleep(500);
						if(HBCBasicfeature.isElementPresent(checkoutSPHAlert1))
						{
							Fail( "The Invalid Contact Number Alert is not dismissed.");
							if(HBC1498 == true)
							{
								HBC1498 = false;
							}
						}
						else
						{
							Pass( "The Invalid Contact Number Alert is dismissed.");
							if(HBC1498 == true)
							{
								HBC1498 = true;
							}
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1497 Issue ." + e.getMessage());
				Exception(" HBC - 1497 Issue ." + e.getMessage());
			}
			
			/* HBC - 1498 Reg Checkout - Verify that on entering the values to the fields where alert is displayed, the alert should be dismissed automatically.*/
			ChildCreation("HBC - 1498 Reg Checkout - Verify that on entering the values to the fields where alert is displayed, the alert should be dismissed automatically.");
			try
			{
				if(HBC1498==true)
				{
					Pass( "The Alert got dismissed when values are entered.");
				}
				else
				{
					Fail( "The Alert was not dismissed when values are entered.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1498 Issue ." + e.getMessage());
				Exception(" HBC - 1498 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1500 Reg Checkout - Shipping Information Section > Verify that "Ship to" dropdown field should be shown below the "Shipping Information" section.*/
	public void ShippingPageShipToDD()
	{
		ChildCreation("HBC - 1500 Reg Checkout - Shipping Information Section > Verify that Ship to dropdown field should be shown below the Shipping Information section.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
				{
					Pass( " The Ship To Address Customer Drop Down is displayed.");
				}
				else
				{
					Fail( " The Ship To Address Customer Drop Down is not displayed..");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1500 Issue ." + e.getMessage());
				Exception(" HBC - 1500 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1472 Reg Checkout - Verify that Reg user should be able to add the new shipping address details in the "Shipping Address" overlay/page.*/
	/* HBC - 1499 Reg Checkout - Shipping Information Section > Verify that user can be able to add multiple shipping address in the "Shipping & Payment" section.*/
	/* HBC - 1501 Reg Checkout - Shipping Information Section > Verify that recently added address First Name field text should be shown by default in "Ship to" dropdown field.*/
	public void ShipPageCreateAddressAddShippingAdd() throws Exception
	{
		ChildCreation(" HBC - 1472 Reg Checkout - Verify that Reg user should be able to add the new shipping address details in the Shipping Address overlay/page." );
		boolean HBC1499 = false;
		String aftAdd = "", sfn = "", lsn = "", befAdd="";
		
		if(shippingPage==true)
		{
			if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
			{
				befAdd = shipandPaypageShipAddressDisp.getText();			
			}			
			try
			{
				sfn = HBCBasicfeature.getExcelVal("HBC1477", sheet, 1);
				lsn = HBCBasicfeature.getExcelVal("HBC1477", sheet, 2);
				String stAdd = HBCBasicfeature.getExcelVal("HBC1477", sheet, 3);
				String cty = HBCBasicfeature.getExcelVal("HBC1477", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1477", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1477", sheet, 6);
				String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 7);
				String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 8);
				String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 9);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						sel.selectByValue("CA");
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Pass( "The State field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
						sel.selectByValue(prv);
						Thread.sleep(1000);
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					boolean addOverlay = ldAddressVerification();
					if(addOverlay==true)
					{
						jsclick(shipandPaypageShipShipCreateEditAddressOkBtn);
						wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
						addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
						if(addVerEnable==true)
						{
							Pass( "The Address Verification Overlay is enabled.");
							if(HBCBasicfeature.isElementPresent(addressuseasEntered))
							{
								Pass( "The Use As Entered button is displayed.");
								jsclick(addressuseasEntered);
								wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
								wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
								wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
								Thread.sleep(1000);
								aftAdd = shipandPaypageShipAddressDisp.getText();
								log.add( "The address before modification is  : " + befAdd);
								log.add( "The address after modification is  : " + aftAdd);
								if((befAdd.isEmpty())||(aftAdd.isEmpty()))
								{
									Fail( "The user failed to get the before address and after address they are empty.",log);
								}
								else if(befAdd.equals(aftAdd))
								{
									Fail( " The newly modified address is not dispalyed.",log);
								}
								else
								{
									HBC1499 = true;
									log.add( " There is update made to the address list.");
									log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
									log.add( " There is update made to the address list. The entered first name was : " + sfn);
									
									if(aftAdd.contains(sfn))
									{
										Pass( "The Added First Name is dispalyed.",log);
									}
									else
									{
										Fail( "The Added First Name is not dispalyed.",log);
									}
									
									log.add( " There is update made to the address list.");
									log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
									log.add( " There is update made to the address list. The entered first name was : " + sfn);
									
									if(aftAdd.contains(lsn))
									{
										Pass( "The Added Last Name is dispalyed.",log);
									}
									else
									{
										Fail( "The Added Last Name is not dispalyed.",log);
									}
								}
								
								if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
								{
									Pass( "The Selected Address Drop Down is displayed.");
									Select sel = new Select(shipandPaypageShipShipToName);
									int size = sel.getOptions().size();
									if(size>1)
									{
										HBC1499 = true;
										for( int i = 0; i<size; i++)
										{
											String addedAdress = sel.getOptions().get(i).getText();
											log.add(" The displayed address list was : " + addedAdress);
										}
										Pass( "The newly added address is added to the drop down.",log);
									}
									else
									{
										HBC1499 = false;
										Fail( "The newly added address is not added to the drop down.",log);
									}
								}
								else
								{
									Fail( "The Selected Address Drop Down is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Use As entere is not displayed.");
								if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
								{
									jsclick(addressVerificationEdit);
									wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
									Thread.sleep(1000);
									if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
									{
										Pass( "The Cancel Link in Create overlay is displayed.");
										jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
										Thread.sleep(1000);
										wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
										Thread.sleep(1000);
										if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
										{
											Pass( "The Address Edit Overlay is closed.");
										}
										else
										{
											Fail( "The Address Edit Overlay is not closed.");
										}
									}
									else
									{
										Fail( "The Cancel Link in Create overlay is not displayed.");
									}
								}
								else
								{
									Fail( "The Edit link in the Address Verification overlay is not displayed.");
								}
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail( "The Address Verification Overlay is not enabled.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1472 Issue ." + e.getMessage());
				Exception(" HBC - 1472 Issue ." + e.getMessage());
			}
			
			/* HBC - 1499 Reg Checkout - Shipping Information Section > Verify that user can be able to add multiple shipping address in the "Shipping & Payment" section.*/
			ChildCreation(" HBC - 1499 Reg Checkout - Shipping Information Section > Verify that user can be able to add multiple shipping address in the Shipping & Payment section." );
			try
			{
				if(HBC1499==true)
				{
					Pass( "The User was able to create additional address.");
				}
				else
				{
					Fail( "The User was not able to create additional address.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1499 Issue ." + e.getMessage());
				Exception(" HBC - 1499 Issue ." + e.getMessage());
			}
			
			/* HBC - 1501 Reg Checkout - Shipping Information Section > Verify that recently added address First Name field text should be shown by default in "Ship to" dropdown field.*/
			ChildCreation("HBC - 1501 Reg Checkout - Shipping Information Section > Verify that recently added address First Name field text should be shown by default in Ship to dropdown field.");
			try
			{
				if(addVerEnable==true)
				{
					if((befAdd.isEmpty())||(aftAdd.isEmpty()))
					{
						Fail( "The user failed to get the before address and after address they are empty.",log);
					}
					else if(befAdd.equals(aftAdd))
					{
						Fail( " The newly modified address is not dispalyed.",log);
					}
					else
					{
						HBC1499 = true;
						log.add( " There is update made to the address list.");
						log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
						log.add( " There is update made to the address list. The entered first name was : " + sfn);
						
						if(aftAdd.contains(sfn))
						{
							Pass( "The Added First Name is dispalyed.",log);
						}
						else
						{
							Fail( "The Added First Name is not dispalyed.",log);
						}
						
						log.add( " There is update made to the address list.");
						log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
						log.add( " There is update made to the address list. The entered first name was : " + sfn);
						
						if(aftAdd.contains(lsn))
						{
							Pass( "The Added Last Name is dispalyed.",log);
						}
						else
						{
							Fail( "The Added Last Name is not dispalyed.",log);
						}
					}
				}
				else
				{
					Fail( " No Address Verification overlay was enabled to add the address.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1501 Issue ." + e.getMessage());
				Exception(" HBC - 1501 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1461 Reg Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be shown with the prefilled shipping details on clicking the "Edit" link.*/
	/*HBC - 1463 Reg Checkout - Shipping Information Section > Verify that clicking on Ok button after making changes in the Shipping Address overlay/page should be displayed with the modified changes.*/
	public void ShipSectionsShipToNMEditLinkDet()
	{
		ChildCreation(" HBC - 1461 Reg Checkout - Shipping Information Section > Shipping Information Section > Verify that Shipping Address overlay/page should be shown with the prefilled shipping details on clicking the Edit link");
		boolean addOverlayState = false;
		String befAdd = "", aftAdd = "";
		try
		{
			if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
			{
				befAdd = shipandPaypageShipAddressDisp.getText();
			}
				
			if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
			{
				Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
				jsclick(shipandPaypageShipEditLink);
				Thread.sleep(3000);
				wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
				Thread.sleep(1000);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{				
					addOverlayState = true;	
					Pass("The Edit Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						boolean res = shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").isEmpty();
						if(res==false)
						{
							Pass("Shipping Address overlay FirstName field shown with the prefilled details on clicking the Edit link");
						}
						else
						{
							Fail("Shipping Address overlay FirstName field not shown with the prefilled details on clicking the Edit link");
						}
					}
					else
					{
						Fail("FirstName field not displayed");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						boolean res = shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").isEmpty();
						if(res==false)
						{
							Pass("Shipping Address overlay LastName field shown with the prefilled details on clicking the Edit link");
						}
						else
						{
							Fail("Shipping Address overlay LastName field not shown with the prefilled details on clicking the Edit link");
						}
					}
					else
					{
						Fail("LastName field not displayed");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						boolean res = shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value").isEmpty();
						if(res==false)
						{
							Pass("Shipping Address overlay StreetAddress field shown with the prefilled details on clicking the Edit link");
						}
						else
						{
							Fail("Shipping Address overlay StreetAddress field not shown with the prefilled details on clicking the Edit link");
						}
					}
					else
					{
						Fail("StreetAddress field not displayed");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						boolean res = shipandPaypageShipShipCreateEditAddresssCty.getAttribute("value").isEmpty();
						if(res==false)
						{
							Pass("Shipping Address overlay City field shown with the prefilled details on clicking the Edit link");
						}
						else
						{
							Fail("Shipping Address overlay City field not shown with the prefilled details on clicking the Edit link");
						}
					}
					else
					{
						Fail("City field not displayed");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						boolean res = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").isEmpty();
						if(res==false)
						{
							Pass("Shipping Address overlay PO code field shown with the prefilled details on clicking the Edit link");
						}
						else
						{
							Fail("Shipping Address overlay PO code field not shown with the prefilled details on clicking the Edit link");
						}
					}
					else
					{
						Fail("PO code field not displayed");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))))
					{
						boolean res1 = shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").isEmpty();
						if(res1==false)
						{
							Pass("Shipping Address overlay PhoneNumber1 field shown with the prefilled details on clicking the Edit link");
						}
						else
						{
							Fail("Shipping Address overlay PhoneNumber1 field not shown with the prefilled details on clicking the Edit link");
						}
						boolean res2 = shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").isEmpty();
						if(res2==false)
						{
							Pass("Shipping Address overlay PhoneNumber2 field shown with the prefilled details on clicking the Edit link");
						}
						else
						{
							Fail("Shipping Address overlay PhoneNumber2 field not shown with the prefilled details on clicking the Edit link");
						}
						boolean res3 = shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").isEmpty();
						if(res3==false)
						{
							Pass("Shipping Address overlay PhoneNumber3 field shown with the prefilled details on clicking the Edit link");
						}
						else
						{
							Fail("Shipping Address overlay PhoneNumber3 field not shown with the prefilled details on clicking the Edit link");
						}
					}
					else
					{
						Fail("PhoneNumber fields not displayed");
					}
				}
				else
				{
					Fail("The Edit Shipping Address Overlay is not dispalyed.");
				}
			}
			else
			{
				Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" HBC - 1461 Issue ." + e.getMessage());
			Exception(" HBC - 1461 Issue ." + e.getMessage());
		}		
		if(addOverlayState == true)
		{
			ChildCreation(" HBC - 1463 Reg Checkout - Shipping Information Section > Verify that clicking on Ok button after making changes in the Shipping Address overlay/page should be displayed with the modified changes.");
			try
				{								
					String fName = HBCBasicfeature.getExcelVal("HBC1463", sheet, 1);
					String LName = HBCBasicfeature.getExcelVal("HBC1463", sheet, 2);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name Field in the Edit form is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(fName);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name Field in the Edit form is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(LName);
							Thread.sleep(1000);
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
							{
								Pass( "The Ok button in the Edit Overlay is displayed.");
								Thread.sleep(1000);
								boolean addOverlay  = false;
								do
								{
									WebDriverWait w1 = new WebDriverWait(driver, 5);
									try
									{
										shipandPaypageShipShipCreateEditAddressOkBtn.click();
										w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
										addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
										addOverlay = true;
									}
									catch(Exception e)
									{
										addOverlay = false;
									}
								}while((addOverlay==false));
								if(addOverlay==true)
								{
									wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
									addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
									if(addVerEnable==true)
									{
										Pass( "The Address Verification Overlay is enabled.");
										if(HBCBasicfeature.isElementPresent(addressuseasEntered))
										{
											Pass( "The Use As Entered button is displayed.");
											jsclick(addressuseasEntered);
											Thread.sleep(500);
											wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
											wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
											wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
											Thread.sleep(1000);
											aftAdd = shipandPaypageShipAddressDisp.getText();
											log.add( "The address before modification is  : " + befAdd);
											log.add( "The address after modification is  : " + aftAdd);
											if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddress))
											{
												Pass("The Edit Shipping Address Overlay is dispalyed.");
												if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
												{
													aftAdd = shipandPaypageShipAddressDisp.getText();
													log.add( "The address before modification is  : " + befAdd);
													log.add( "The address after modification is  : " + aftAdd);
													if((befAdd.isEmpty())||(aftAdd.isEmpty()))
													{
														Fail( "The user failed to get the before address and after address they are empty.",log);
													}
													else if(befAdd.equals(aftAdd))
													{
														Fail( " There is no modification done when Ok is clicked in the Address Verification Page.",log);
													}
													else
													{
														log.add( "The Expected Modified First Name was : " + fName);
														log.add( "The Expected Modified First Name was : " + aftAdd);
														if(aftAdd.contains(fName))
														{
															Pass( "The Updated First Name is displayed. ");
														}
														else
														{
															Fail( "There is mismatch in the before address and after address.",log);
														}
														
														log.add( "The Expected Modified Last Name was : " + fName);
														log.add( "The Expected Modified Last Name was : " + aftAdd);
														if(aftAdd.contains(LName))
														{
															Pass( "The Updated Last Name is displayed. ");
														}
														else
														{
															Fail( "There is mismatch in the before address and after address.",log);
														}
													}													
												}
												else
												{
													Fail("The Saved Address list is not displayed.");
												}
											}
											else
											{
												Fail( "The User failed to load the Shippment and Payment Page.");
											}
										}
										else
										{
											Fail( " The Use As Entered Button is not displayed.");
										}
									}
									else
									{
										Fail( " The User failed to load the address Verification page.");
									}
								}
								else
								{
									Fail("The Edit Shipping Address Overlay is not dispalyed.");
								}
							}
							else
							{
								Fail( "The Cancel button in the Edit Overlay is displayed is not displayed.");
							}
						}
						else
						{
							Fail( "The First Name in the Edit field is not displayed.");
						}
					}
					else
					{
						Fail( "The First Name in the Edit field is not displayed.");
					}				
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1463 Issue ." + e.getMessage());
					Exception(" HBC - 1463 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip("Edit Address overlay is not displayed");
			}
		}
	
	/* HBC - 1502 Reg Checkout - Shipping Information Section > Verify that user can be able to change the shipping address by changing the dropdown option in the "Ship to" dropdown field. */
	public void ShipPageSelectAddFromShipList()
	{
		ChildCreation("HBC - 1502 Reg Checkout - Shipping Information Section > Verify that user can be able to change the shipping address by changing the dropdown option in the Ship to dropdown field.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
				{
					Pass( "The Selected Address Drop Down is displayed.");
					Select sel = new Select(shipandPaypageShipShipToName);
					String currAdd = sel.getFirstSelectedOption().getText();
					int size = sel.getOptions().size();
					if(size>1)
					{
						Pass( "The newly added address is added to the drop down.");
						//Random r = new Random();
						//int sAddsel = r.nextInt(size);
						sel.getOptions().get(0).click();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "none"));
						Thread.sleep(1000);
						String newAdd = sel.getFirstSelectedOption().getText();
						log.add( "The Old address was : " + currAdd);
						log.add( "The Old address was : " + newAdd);
						if(newAdd.equals(currAdd))
						{
							Fail( "The User was not able to select the new Address from the drop down.",log);
						}
						else
						{
							Pass( "The User was able to select the new Address from the drop down.",log);
						}
					}
					else
					{
						Fail( "The address list less than or equal to one in the drop down.");
					}
				}
				else
				{
					Fail( "The Selected Address Drop Down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1293 Issue ." + e.getMessage());
				Exception(" HBC - 1293 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1642 Verify that "Address Verification" overlay should be as per the creative.*/
	/* HBC - 1643 Verify that Address Verification page should be shown after adding new shipping address or updating the existing address.*/
	/* HBC - 1644 Verify that suggested address should be shown in the address verification page for the entered address.*/
	/* HBC - 1647 Verify that "We found more than...." text should be shown in bold, when more than 1 address suggestion is displayed.*/
	/* HBC - 1654 Verify that user entered address should be displayed with Edit option with the button caption "Use Address as entered" in the address verification page as per the creative.*/
	/* HBC - 1660 Verify that partial entered address should be shown below the "Use Address as entered" button.*/
	/* HBC - 1661 Verify that "*Your address may be undeliverable" text should be shown below the "Use Address as entered" button.*/
	/* HBC - 1662 Verify that edit address page / overlay with prefilled address should be open on tapping the Edit option in address verification page.*/
	/* HBC - 1657 Verify that "Use Suggested Address" button should not be shown, when more than 1 address suggestion is displayed*/

	public void HBCShipPageAddShippingAddressVerification1()
	{
		ChildCreation(" HBC - 1642 Verify that Address Verification overlay should be as per the creative." );
		String sfn = "", lsn = "", actText = "", suggInfoTxt = "", stAdd = "", cty = "", prv = "", pocode = "";
		boolean HBC1643 = false, HBC1647 = false;
		if(shippingPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipCreateAddLink));
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					jsclick(shipandPaypageShipCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						sfn = HBCBasicfeature.getExcelVal("HBC1642AddVer", sheet, 1);
						lsn = HBCBasicfeature.getExcelVal("HBC1642AddVer", sheet, 2);
						stAdd = HBCBasicfeature.getExcelVal("HBC1642AddVer", sheet, 3);
						cty = HBCBasicfeature.getExcelVal("HBC1642AddVer", sheet, 4);
						prv = HBCBasicfeature.getExcelVal("HBC1642AddVer", sheet, 5);
						pocode = HBCBasicfeature.getExcelVal("HBC1642AddVer", sheet, 6);
						String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1642AddVer", sheet, 7);
						String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1642AddVer", sheet, 8);
						String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1642AddVer", sheet, 9);
						String editTxt = HBCBasicfeature.getExcelNumericVal("HBC1642", sheet, 1);
						suggInfoTxt = HBCBasicfeature.getExcelNumericVal("HBC1642", sheet, 2);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address field is displayed.");
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
						}
						else
						{
							Fail( "The Street Address field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City field is displayed.");
							shipandPaypageShipShipCreateEditAddresssCty.clear();
							shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
						}
						else
						{
							Fail( "The City field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							sel.selectByValue("CA");
						}
						else
						{
							Fail( "The Country field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						boolean addOverlay = ldAddressVerification2();
						if(addOverlay==true)
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								HBC1643 = true;
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
								{
									Pass( "The Address Verification Overlay is dispalyed.");
									if(HBCBasicfeature.isElementPresent(addressVerificationInfoText))
									{
										actText = addressVerificationInfoText.getText();
										if(actText.isEmpty())
										{
											Fail( "The address verification details text is not displayed and it seems to be empty.");
										}
										else
										{
											Pass( "The address verification details text is displayed.The displayed text is " + actText);
											if(actText.contains(suggInfoTxt))
											{
												HBC1647 = true;
											}
										}
									}
									else
									{
										Fail( "The Address Verification text is not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
									{
										Pass( "The Address Verification Edit link is displayed.");
										String actTxt = addressVerificationAddressEditLink.getText();
										log.add(" The expected text was : " + editTxt);
										log.add(" The actual text is : " + actTxt);
										if(actTxt.equalsIgnoreCase(editTxt))
										{
											Pass( "There is no mismatch in the expected and the actual text.",log);
										}
										else
										{
											Fail( "There is some mismatch in the expected and the actual text.",log);
										}
									}
									else
									{
										Fail( "The Address Verification Edit link is not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressuseasEntered))
									{
										Pass( "The Use as Entered Button is displayed.");
									}
									else
									{
										Fail( "The Use as Entered Button is not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressSuggestionList))
									{
										boolean addrSugg = addressSuggestionList.getAttribute("style").contains("block");
										if(addrSugg==true)
										{
											Pass( "The Address Suggestion List is enabled and the address is displayed.");
										}
										else
										{
											Skip(" The Address Suggestion List is not displayed.");
										}
									}
									else
									{
										Fail( "The Address Suggestion List is not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressSuggestionDeliveryWarning))
									{
										Pass( "The Address Suggestion delivery warning is displayed.");
									}
									else
									{
										Fail( "The Address Suggestion delivery warning is  not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressSuggestionClose))
									{
										Pass( "The Address Suggestion Close icon is displayed.");
									}
									else
									{
										Fail( "The Address Suggestion Close icon is not displayed.");
									}
								}
								else
								{
									Fail( "The Address Verification overlay is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Create Shipping Address Link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1642 Issue ." + e.getMessage());
				Exception(" HBC - 1642 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1647 Verify that "We found more than...." text should be shown in bold, when more than 1 address suggestion is displayed.*/
		ChildCreation(" HBC - 1647 Verify that We found more than.... text should be shown in bold, when more than 1 address suggestion is displayed.");
		if(shippingPage==true)
		{
			try
			{
				log.add( "The displayed address suggestion information is : " + actText);
				log.add( "The expected address suggestion information is : " + suggInfoTxt);
				if(HBC1647==true)
				{
					Pass( "The Address Verification page is displayed on entering the new Address and more than one address found info is displayed.",log);
				}
				else
				{
					Fail( " No Address Verification page is displayed on entering the new Address.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1647 Issue ." + e.getMessage());
				Exception(" HBC - 1647 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1643 Verify that Address Verification page should be shown after adding new shipping address or updating the existing address.*/
		ChildCreation("HBC - 1643 Verify that Address Verification page should be shown after adding new shipping address or updating the existing address.");
		if(shippingPage==true)
		{
			try
			{
				if(HBC1643==true)
				{
					Pass( "The Address Verification page is displayed on entering the new Address.");
				}
				else
				{
					Fail( " No Address Verification page is displayed on entering the new Address.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1643 Issue ." + e.getMessage());
				Exception(" HBC - 1643 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1661 Verify that "*Your address may be undeliverable" text should be shown below the "Use Address as entered" button.*/
		ChildCreation("HBC - 1661 Verify that *Your address may be undeliverable text should be shown below the Use Address as entered button.");
		if(shippingPage==true)
		{
			try
			{
				String warnText = HBCBasicfeature.getExcelVal("HBC1661", sheet, 2);
				if(HBCBasicfeature.isElementPresent(addressSuggestionDeliveryWarning))
				{
					Pass( "The Address Suggestion delivery warning is displayed.");
					String actTxt = addressSuggestionDeliveryWarning.getText();
					log.add( "The displayed warning text was : " + actTxt);
					log.add( "The Expected text is  : " + warnText);
					if(warnText.equalsIgnoreCase(actTxt))
					{
						Pass( "The expected and the actual text matches.",log);
					}
					else
					{
						Fail( "The expected and the actual text does not matches.",log);
					}
				}
				else
				{
					Fail( "The Address Suggestion delivery warning is  not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1661 Issue ." + e.getMessage());
				Exception(" HBC - 1661 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1657 Verify that "Use Suggested Address" button should not be shown, when more than 1 address suggestion is displayed.*/
		ChildCreation("HBC - 1657 Verify that Use Suggested Address button should not be shown, when more than 1 address suggestion is displayed.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressSuggestionList))
				{
					boolean addrSugg = addressSuggestionList.getAttribute("style").contains("block");
					if(addrSugg==true)
					{
						Pass( "The Address Suggestion List is enabled and the address is displayed.");
						if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
						{
							Fail( "The Use Suggested Address button is displayed when more than one address suggestion is displayed.");
						}
						else
						{
							Pass( "The Use Suggested Address button is not displayed when more than one address suggestion is displayed.");
						}
					}
					else
					{
						Skip(" The Address Suggestion List is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Suggestion List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1657 Issue ." + e.getMessage());
				Exception(" HBC - 1657 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1660 Verify that partial entered address should be shown below the "Use Address as entered" button.*/
		ChildCreation(" HBC - 1660 Verify that partial entered address should be shown below the Use Address as entered button.");
		boolean HBC1654 = false;
		if(shippingPage==true)
		{
			boolean addFound = false;
			try
			{
				if(HBCBasicfeature.isListElementPresent(userEnteredAddress))
				{
					for(int k = 0; k<userEnteredAddress.size(); k++)
					{
						addFound = false;
						String addDet = userEnteredAddress.get(k).getText();
						if(addDet.contains(stAdd))
						{
							addFound = true;
							HBC1654 = true;
							break;
						}
					}
					
					if(addFound==true)
					{
						Pass( "The User Entered Address is displayed.");
					}
					else
					{
						Fail( "The User Entered Address is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Suggestion List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1660 Issue ." + e.getMessage());
				Exception(" HBC - 1660 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1654 Verify that user entered address should be displayed with Edit option with the button caption "Use Address as entered" in the address verification page as per the creative.*/
		ChildCreation(" HBC - 1654 Verify that user entered address should be displayed with Edit option with the button caption Use Address as entered in the address verification page as per the creative.");
		if(shippingPage==true)
		{
			try
			{
				if(HBC1654==true)
				{
					Pass( "The User Entered Address is displayed in the Address Verification Page. ");
				}
				else
				{
					Fail( "The User Entered Address is not displayed in the Address Verification Page. ");
				}
				
				if(HBCBasicfeature.isElementPresent(addressuseasEntered))
				{
					Pass( "The Use as Entered Button is displayed.");
				}
				else
				{
					Fail( "The Use as Entered Button is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
				{
					Pass( "The Address Verification Edit link is displayed.");
				}
				else
				{
					Fail( "The Address Verification Edit link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1654 Issue ." + e.getMessage());
				Exception(" HBC - 1654 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
			
		/* HBC - 1644 Verify that suggested address should be shown in the address verification page for the entered address.*/
		ChildCreation("HBC - 1644 Verify that suggested address should be shown in the address verification page for the entered address.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressSuggestionList))
				{
					boolean addrSugg = addressSuggestionList.getAttribute("style").contains("block");
					if(addrSugg==true)
					{
						Pass( "The Address Suggestion List is enabled and the address is displayed.");
						if(HBCBasicfeature.isListElementPresent(addressSuggestionAddressList))
						{
							for(int i = 0; i<addressSuggestionAddressList.size(); i++)
							{
								String addrSuggested = addressSuggestionAddressList.get(i).getText();
								if(addrSuggested.isEmpty())
								{
									Fail( "The Suggested address is empty.");
								}
								else
								{
									Pass( " The Address Suggested is displayed. The displayed address is  : " + addrSuggested);
								}
							}
						}
						else
						{
							Fail( "The Address Suggestion is not displayed.");
						}
					}
					else
					{
						Skip(" The Address Suggestion List is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Suggestion List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1644 Issue ." + e.getMessage());
				Exception(" HBC - 1644 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1662 Verify that edit address page / overlay with prefilled address should be open on tapping the Edit option in address verification page.*/
		ChildCreation("HBC - 1662 Verify that edit address page / overlay with prefilled address should be open on tapping the Edit option in address verification page.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
				{
					Pass( "The Address Verification Edit link is displayed.");
					jsclick(addressVerificationAddressEditLink);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The First Name field is empty.");
						}
						else
						{
							Pass( "The First Name field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Last Name field is empty.");
						}
						else
						{
							Pass( "The Last Name field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Street Address field is empty.");
						}
						else
						{
							Pass( "The Street Address field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssCty.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The City field is empty.");
						}
						else
						{
							Pass( "The City field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						if(val.isEmpty())
						{
							Fail( "The Country field is empty.");
						}
						else
						{
							Pass( "The Country field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Pass( "The State field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
						String val = sel.getFirstSelectedOption().getText();
						if(val.isEmpty())
						{
							Fail( "The State field is empty.");
						}
						else
						{
							Pass( "The State field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The PO CODE field is empty.");
						}
						else
						{
							Pass( "The PO CODE field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Phone Number field 1 is empty.");
						}
						else
						{
							Pass( "The Phone Number field 1 field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Phone Number field 2 is empty.");
						}
						else
						{
							Pass( "The Phone Number field 2 field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Phone Number field 3 is empty.");
						}
						else
						{
							Pass( "The Phone Number field 3 field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Edit link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1662 Issue ." + e.getMessage());
				Exception(" HBC - 1662 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1664 Verify that updated address should be displayed in address verification page while selecting the "Ok" / "Continue" button in the edit shipping address page/overlay.*/
	/* HBC - 1645 Verify that "Use Address as entered" button alone should be displayed with the Edit option, when no address suggestion is shown.*/
	/* HBC - 1646 Verify that "Sorry we could not find...." text should be shown in bold, when no address suggestion is displayed.*/
	public void HBCShipPageAddShippingAddressNoMatchAddressVerification2()
	{
		ChildCreation(" HBC - 1664 Verify that updated address should be displayed in address verification page while selecting the Ok / Continue button in the edit shipping address page/overlay.");
		String stAdd = "";
		if(shippingPage==true)
		{
			try
			{
				stAdd = HBCBasicfeature.getExcelVal("HBC1664", sheet, 3);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
				{
					Pass( "The Street Address field is displayed.");
					shipandPaypageShipShipCreateEditAddresssStAddress.clear();
					shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				boolean addOverlay = ldAddressVerification2();
				if(addOverlay==true)
				{
					wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
					addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
					if(addVerEnable==true)
					{
						Pass( "The Address Verification Overlay is enabled.");
						if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
						{
							Pass( "The Address Verification Overlay is dispalyed.");
							if(HBCBasicfeature.isListElementPresent(userEnteredAddress))
							{
								boolean addFound = false;
								for(int k = 0; k<userEnteredAddress.size(); k++)
								{
									addFound = false;
									String addDet = userEnteredAddress.get(k).getText();
									if(addDet.contains(stAdd))
									{
										addFound = true;
										break;
									}
								}
								
								if(addFound==true)
								{
									Pass( "The User Entered Address is displayed.");
								}
								else
								{
									Fail( "The User Entered Address is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Suggestion List is not displayed.");
							}
						}
						else
						{
							Fail( "The Address Verification overlay is not displayed.");
						}
					}
					else
					{
						Fail( "The Address Verification Overlay is not enabled.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1664 Issue ." + e.getMessage());
				Exception(" HBC - 1664 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1645 Verify that "Use Address as entered" button alone should be displayed with the Edit option, when no address suggestion is shown.*/
		ChildCreation(" HBC - 1645 Verify that Use Address as entered button alone should be displayed with the Edit option, when no address suggestion is shown.");
		if(shippingPage==true)
		{
			try
			{
				String editTxt = HBCBasicfeature.getExcelNumericVal("HBC1642", sheet, 1);
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
						{
							Pass( "The Address Verification Edit link is displayed.");
							String actTxt = addressVerificationAddressEditLink.getText();
							log.add(" The expected text was : " + editTxt);
							log.add(" The actual text is : " + actTxt);
							if(actTxt.equalsIgnoreCase(editTxt))
							{
								Pass( "There is no mismatch in the expected and the actual text.",log);
							}
							else
							{
								Fail( "There is some mismatch in the expected and the actual text.",log);
							}
						}
						else
						{
							Fail( "The Address Verification Edit link is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(addressuseasEntered))
						{
							Pass( "The Use as Entered Button is displayed.");
						}
						else
						{
							Fail( "The Use as Entered Button is not displayed.");
						}
						
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1645 Issue ." + e.getMessage());
				Exception(" HBC - 1645 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1646 Verify that "Sorry we could not find...." text should be shown in bold, when no address suggestion is displayed.*/
		ChildCreation(" HBC - 1646 Verify that Sorry we could not find.... text should be shown in bold, when no address suggestion is displayed.");
		if(shippingPage==true)
		{
			try
			{
				String actText = "";
				String suggInfoTxt = HBCBasicfeature.getExcelNumericVal("HBC1646", sheet, 1);
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(addressVerificationInfoText))
						{
							actText = addressVerificationInfoText.getText();
							if(actText.isEmpty())
							{
								Fail( "The address verification details text is not displayed and it seems to be empty.");
							}
							else if(actText.contains(suggInfoTxt))
							{
								Pass(" The expected and the actual text matches.",log);
							}
							else
							{
								Fail(" The expected and the actual text does not matches.",log);
							}
						}
						else
						{
							Fail( "The Address Verification text is not displayed.");
						}
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1646 Issue ." + e.getMessage());
				Exception(" HBC - 1646 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1648 Verify that "Sorry, we don't recognize...." text should be shown in bold, when the house or building number is invalid or not able to recognize.*/
	/* HBC - 1649 Verify that "Confirm your House/Building number" textbox should be shown, when the house or building number is invalid or not able to recognize.*/
	/* HBC - 1650 Verify that "Confirm Number" button should be shown near the "Confirm your House/Building number" textbox.*/
	/* HBC - 1651 Verify that user should navigates to checkout pages on tapping the "Confirm Number" button after entering the number in Confirm Number field.*/
	public void HBCShipPageAddShippingAddressBuildingNumberVerification3()
	{
		ChildCreation(" HBC - 1648 Verify that Sorry, we don't recognize.... text should be shown in bold, when the house or building number is invalid or not able to recognize." );
		String sfn = "", lsn = "", actText = "", suggInfoTxt = "", stAdd = "", cty = "", prv = "", pocode = "", expColor = "";
		if(shippingPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipCreateAddLink));
				if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
				{
					Pass( "The Address Suggestion Close icon is displayed.");
					jsclick(addressVerificationAddressEditLink);
					Thread.sleep(2000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					//Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						sfn = HBCBasicfeature.getExcelVal("HBC1648AddrVer", sheet, 2);
						lsn = HBCBasicfeature.getExcelVal("HBC1648AddrVer", sheet, 1);
						stAdd = HBCBasicfeature.getExcelVal("HBC1648AddrVer", sheet, 3);
						cty = HBCBasicfeature.getExcelVal("HBC1648AddrVer", sheet, 4);
						prv = HBCBasicfeature.getExcelVal("HBC1648AddrVer", sheet, 5);
						pocode = HBCBasicfeature.getExcelVal("HBC1648AddrVer", sheet, 6);
						suggInfoTxt = HBCBasicfeature.getExcelVal("HBC1648", sheet, 1);
						expColor = HBCBasicfeature.getExcelVal("HBC1648", sheet, 2);
						String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1648AddrVer", sheet, 7);
						String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1648AddrVer", sheet, 8);
						String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1648AddrVer", sheet, 9);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address field is displayed.");
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
						}
						else
						{
							Fail( "The Street Address field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City field is displayed.");
							shipandPaypageShipShipCreateEditAddresssCty.clear();
							shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
						}
						else
						{
							Fail( "The City field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							sel.selectByValue("CA");
						}
						else
						{
							Fail( "The Country field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						boolean addOverlay = ldAddressVerification2();
						if(addOverlay==true)
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
								{
									Pass( "The Address Verification Overlay is dispalyed.");
									if(HBCBasicfeature.isElementPresent(addressVerificationInfoText))
									{
										actText = addressVerificationInfoText.getText();
										if(actText.isEmpty())
										{
											Fail( "The address verification details text is not displayed and it seems to be empty.");
										}
										else
										{
											Pass( "The address verification details text is displayed.The displayed text is " + actText);
											log.add( "The Expected value was : " + suggInfoTxt);
											log.add( "The actual value is : " + actText);
											if(actText.contains(suggInfoTxt))
											{
												Pass( "The Expected and the actual value matches.",log);
											}
											else
											{
												Fail( "The Expected and the actual value does not matches.",log);
											}
										}
									}
									else
									{
										Fail( "The Address Verification text is not displayed.");
									}
									
									String txtCol = addressVerificationInfoText.getCssValue("color");
									Color col = Color.fromString(txtCol);
									String actCol = col.asHex();
									log.add( "The expected color was : " + expColor);
									log.add( "The actual color is : " + actCol);
									if(expColor.equalsIgnoreCase(actCol))
									{
										Pass( "The Expected and the Actual color matches.",log);
									}
									else
									{
										Fail( "The Expected and the Actual color does not matches.",log);
									}
								}
								else
								{
									Fail( "The Address Verification overlay is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Address Suggestion Edit Link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1648 Issue ." + e.getMessage());
				Exception(" HBC - 1648 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1649 Verify that "Confirm your House/Building number" textbox should be shown, when the house or building number is invalid or not able to recognize.*/
		ChildCreation("HBC - 1649 Verify that Confirm your House/Building number textbox should be shown, when the house or building number is invalid or not able to recognize.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressVerificationbuildingTextbox))
				{
					Pass( "The Building Number Text Box is diplayed.");
				}
				else
				{
					Fail( "The Building Number Text Box is not diplayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1649 Issue ." + e.getMessage());
				Exception(" HBC - 1649 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1650 Verify that "Confirm Number" button should be shown near the "Confirm your House/Building number" textbox.*/
		ChildCreation(" HBC - 1650 Verify that Confirm Number button should be shown near the Confirm your House/Building number textbox.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
				{
					Pass( "The Confirm Building Number button is diplayed.");
				}
				else
				{
					Fail( "The Confirm Building Number button is not diplayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1650 Issue ." + e.getMessage());
				Exception(" HBC - 1650 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1651 Verify that user should navigates to checkout pages on tapping the "Confirm Number" button after entering the number in Confirm Number field.*/
		ChildCreation(" HBC - 1651 Verify that user should navigates to checkout pages on tapping the Confirm Number button after entering the number in Confirm Number field.");
		if(shippingPage==true)
		{
			try
			{
				String buildNo = HBCBasicfeature.getExcelNumericVal("HBC1651", sheet, 1);
				if(HBCBasicfeature.isElementPresent(addressVerificationbuildingTextbox))
				{
					Pass( "The Building Number Text Box is diplayed.");
					addressVerificationbuildingTextbox.clear();
					addressVerificationbuildingTextbox.sendKeys(buildNo);
				}
				else
				{
					Fail( "The Building Number Text Box is not diplayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
				{
					Pass( "The Confirm Building Number button is diplayed.");
					jsclick(addressSuggestedBtn);
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
					wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
					wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
					Thread.sleep(1000);
					boolean shipPge = shipandPaypage.getAttribute("style").contains("block");
					if(shipPge==true)
					{
						Pass( "The User is navigated to the Ship Page.");
					}
					else
					{
						Fail( "The User is not navigated to the Ship Page.");
					}
				}
				else
				{
					Fail( "The Confirm Building Number button is not diplayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1651 Issue ." + e.getMessage());
				Exception(" HBC - 1651 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1444 Verify that "Use Suggested Address" button should be displayed above the suggested address.*/
	/* HBC - 1656 Verify that suggested address should be updated in the shipping address on tapping the "Use Suggested Address" button.*/
	public void HBCShipPageAddShippingSuggestedAddressVerification4()
	{
		ChildCreation(" HBC - 1444 Verify that Use Suggested Address button should be displayed above the suggested address." );
		String sfn = "", lsn = "",stAdd = "", cty = "", prv = "", pocode = "";
		if(shippingPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipCreateAddLink));
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					jsclick(shipandPaypageShipCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						sfn = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 2);
						lsn = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 1);
						stAdd = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 3);
						cty = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 4);
						prv = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 5);
						pocode = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 6);
						String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1444AddrVer", sheet, 7);
						String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1444AddrVer", sheet, 8);
						String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1444AddrVer", sheet, 9);
						String expVal = HBCBasicfeature.getExcelVal("HBC1444", sheet, 1);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.click();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address field is displayed.");
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
						}
						else
						{
							Fail( "The Street Address field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City field is displayed.");
							shipandPaypageShipShipCreateEditAddresssCty.clear();
							shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
						}
						else
						{
							Fail( "The City field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							sel.selectByValue("CA");
						}
						else
						{
							Fail( "The Country field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						boolean addOverlay = ldAddressVerification2();
						if(addOverlay==true)
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
								{
									Pass( "The Address Verification Overlay is dispalyed.");
									if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
									{
										Pass( "The Use Suggested Address button is diplayed.");
										String actVal = addressSuggestedBtn.getAttribute("value");
										log.add( "The Expected value was : " + expVal);
										log.add( "The actual value is : " + actVal);
										if(expVal.equalsIgnoreCase(actVal))
										{
											Pass( "The Expected and the actual value matches.",log);
										}
										else
										{
											Fail( "The Expected and the actual value does not matches.",log);
										}
									}
									else
									{
										Fail( "The Confirm Building Number button is not diplayed.");
									}
								}
								else
								{
									Fail( "The Address Verification overlay is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Create Shipping Address Link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1444 Issue ." + e.getMessage());
				Exception(" HBC - 1444 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1656 Verify that suggested address should be updated in the shipping address on tapping the "Use Suggested Address" button.*/
		ChildCreation(" HBC - 1656 Verify that suggested address should be updated in the shipping address on tapping the Use Suggested Address button." );
		if(shippingPage==true)
		{
			String currAdd = "";
			try
			{
				if(HBCBasicfeature.isListElementPresent(addressVerificationSuggestedAddress))
				{
					ArrayList<String> suggadd = new ArrayList<>();
					for(int i = 0; i<addressVerificationSuggestedAddress.size(); i++)
					{
						suggadd.add(addressVerificationSuggestedAddress.get(i).getText());
					}
					
					log.add( "The Suggested address is  : " + suggadd);
					if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
					{
						Pass( "The Use Suggested Address button is diplayed.");
						jsclick(addressSuggestedBtn);
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
						wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
						Thread.sleep(1000);
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
						{
							currAdd = shipandPaypageShipAddressDisp.getText();
							log.add( "The Current Updated address is  : " + currAdd);
						}
						boolean shipPge = shipandPaypage.getAttribute("style").contains("block");
						if(shipPge==true)
						{
							Pass( "The User is navigated to the Ship Page.");
							boolean addFound = false;
							for(int k = 0; k<suggadd.size(); k++)
							{
								String suggA = suggadd.get(k).toString();
								if(currAdd.contains(suggA))
								{
									addFound = true;
									break;
								}
								else
								{
									continue;
								}
							}
							
							if(addFound==true)
							{
								Pass( "The Suggested Address is updated and it is displayed in the Checkout page.",log);
							}
							else
							{
								Fail( "The Suggested Address is not updated and it is not displayed in the Checkout page.",log);
							}
						}
						else
						{
							Fail( "The User is not navigated to the Ship Page.");
						}
					}
					else
					{
						Fail( "The Confirm Building Number button is not diplayed.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1656 Issue ." + e.getMessage());
				Exception(" HBC - 1656 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1652 Verify that "Show all Potential Matches" text link should be shown below the "Confirm your House/Building number" textbox.*/
	/* HBC - 1653 Verify that all the matched suggested address should be shown on tapping "Show all Potential Matches" text link.*/
	/* HBC - 1663 Verify that user should be able to choose from suggested address in the address verification page.*/
	/* HBC - 1658 Verify that shipping address details should be updated and it should redirects to checkout page on tapping on any suggested address.*/
	public void HBCShipPageAddShippingPotentialAddressVerification5()
	{
		ChildCreation(" HBC - 1652 Verify that Show all Potential Matches text link should be shown below the Confirm your House/Building number textbox." );
		String sfn = "", lsn = "",stAdd = "", cty = "", prv = "", pocode = "";
		int sele = 0;
		if(shippingPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipCreateAddLink));
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					jsclick(shipandPaypageShipCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						sfn = HBCBasicfeature.getExcelVal("HBC1652AddrVer", sheet, 2);
						lsn = HBCBasicfeature.getExcelVal("HBC1652AddrVer", sheet, 1);
						stAdd = HBCBasicfeature.getExcelVal("HBC1652AddrVer", sheet, 3);
						cty = HBCBasicfeature.getExcelVal("HBC1652AddrVer", sheet, 4);
						prv = HBCBasicfeature.getExcelVal("HBC1652AddrVer", sheet, 5);
						pocode = HBCBasicfeature.getExcelVal("HBC1652AddrVer", sheet, 6);
						String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1652AddrVer", sheet, 7);
						String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1652AddrVer", sheet, 8);
						String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1652AddrVer", sheet, 9);
						String expVal = HBCBasicfeature.getExcelVal("HBC1652", sheet, 1);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address field is displayed.");
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
						}
						else
						{
							Fail( "The Street Address field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City field is displayed.");
							shipandPaypageShipShipCreateEditAddresssCty.clear();
							shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
						}
						else
						{
							Fail( "The City field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							sel.selectByValue("CA");
						}
						else
						{
							Fail( "The Country field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						boolean addOverlay = ldAddressVerification2();
						if(addOverlay==true)
						{
							Thread.sleep(1000);
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
								{
									Pass( "The Address Verification Overlay is dispalyed.");
									if(HBCBasicfeature.isElementPresent(addressVerificationPotentialMatchesText))
									{
										Pass( "The Potential Matches Link is displayed.");
										String actVal = addressVerificationPotentialMatchesText.getText();
										log.add( "The Expected value was : " + expVal);
										log.add( "The actual value is : " + actVal);
										if(expVal.equalsIgnoreCase(actVal))
										{
											Pass( "The Expected and the actual value matches.",log);
										}
										else
										{
											Fail( "The Expected and the actual value does not matches.",log);
										}
									}
									else
									{
										Fail( "The Confirm Building Number button is not diplayed.");
									}
								}
								else
								{
									Fail( "The Address Verification overlay is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Create Shipping Address Link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1652 Issue ." + e.getMessage());
				Exception(" HBC - 1652 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1653 Verify that all the matched suggested address should be shown on tapping "Show all Potential Matches" text link.*/
		ChildCreation(" HBC - 1653 Verify that all the matched suggested address should be shown on tapping Show all Potential Matches text link." );
		if(shippingPage==true)
		{
			try
			{
				Thread.sleep(1000);
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(addressVerificationPotentialMatchesText))
						{
							Pass( "The Potential Matches Link is displayed.");
							jsclick(addressVerificationPotentialMatchesText);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.attributeContains(addressVerificationPotentialMatchesAddress, "style", "block"));
							if(HBCBasicfeature.isListElementPresent(addressVerificationPotentialMatchesAddressList))
							{
								Pass( "The Address List is displayed.");
							}
							else
							{
								Fail( "The Address List is not displayed.");
							}
						}
						else
						{
							Fail( "The Confirm Building Number button is not diplayed.");
						}
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1653 Issue ." + e.getMessage());
				Exception(" HBC - 1653 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1663 Verify that user should be able to choose from suggested address in the address verification page.*/
		ChildCreation(" HBC - 1663 Verify that user should be able to choose from suggested address in the address verification page." );
		if(shippingPage==true)
		{
			try
			{
				Thread.sleep(1000);
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isListElementPresent(addressVerificationPotentialMatchesAddressList))
						{
							Pass( "The Address List is displayed.");
							Random r = new Random();
							sele = r.nextInt(addressVerificationPotentialMatchesAddressList.size());
							HBCBasicfeature.scrolldown(addressVerificationPotentialMatchesAddressList.get(sele), driver);
							Thread.sleep(1000);
							String addTxt = driver.findElement(By.xpath("(//*[@class='QAS_Pick']//tr)["+sele+"]")).getText();
							Pass( "The user was able to select the address from the list. The Selected address was : " + addTxt);
						}
						else
						{
							Fail( "The Address List is not displayed.");
						}
						
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1663 Issue ." + e.getMessage());
				Exception(" HBC - 1663 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1658 Verify that shipping address details should be updated and it should redirects to checkout page on tapping on any suggested address.*/
		ChildCreation(" HBC - 1658 Verify that shipping address details should be updated and it should redirects to checkout page on tapping on any suggested address." );
		if(shippingPage==true)
		{
			try
			{
				Thread.sleep(1000);
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isListElementPresent(addressVerificationPotentialMatchesAddressList))
						{
							Pass( "The Address List is displayed.");
							Random r = new Random();
							sele = r.nextInt(addressVerificationPotentialMatchesAddressList.size());
							HBCBasicfeature.scrolldown(addressVerificationPotentialMatchesAddressList.get(sele), driver);
							Thread.sleep(1000);
							WebElement addTxt = driver.findElement(By.xpath("(//*[@class='QAS_Pick']//tr//td)["+sele+"]"));
							//String selAdd = addTxt.getText();
							Actions act = new Actions(driver);
							act.moveToElement(addTxt).click().build().perform();
							Thread.sleep(2000);
							wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
							wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
							wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
							Thread.sleep(1000);
							boolean shipPge = shipandPaypage.getAttribute("style").contains("block");
							if(shipPge==true)
							{
								Pass( "The User is navigated to the Ship Page.");
							}
							else
							{
								Fail( "The User is not navigated to the Ship Page.");
							}
						}
						else
						{
							Fail( "The Address List is not displayed.");
						}
						
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1658 Issue ." + e.getMessage());
				Exception(" HBC - 1658 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
		
	/* HBC - 1512 Reg Checkout - Payment Information Section > Verify that Selected Billing address should be shown with "Edit" & "Create Address" text links below the Payment information section.*/
	public void PaySectionsEditCreateLinks()
	{
		ChildCreation(" HBC - 1512 Reg Checkout - Payment Information Section > Verify that Selected Billing address should be shown with Edit & Create Address text links below the Payment information section.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayEditLink))
				{
					Pass( "The Edit Link in the Payment Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Edit Link in the Payment Section in Shipping and Billing Tab is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayCreateAddLink))
				{
					Pass( "The Create Link in the Payment Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Create Link in the Payment Section in the Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1512 Issue ." + e.getMessage());
				Exception(" HBC - 1512 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "Shipping Page overlay is not displayed");
		}
	}
	
	/* HBC - 1513 Reg Checkout - Payment Information Section > Payment Information Section > Verify that Selected billing address should be highlighted in black below the Payment information section.*/
	public void PaymentSectionsAddressHighlight() throws Exception
	{
		ChildCreation(" HBC - 1513 Reg Checkout - Payment Information Section > Verify that Selected billing address should be highlighted in black below the Payment information section");
		String expected = HBCBasicfeature.getExcelVal("HBC1460", sheet, 2);
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayAddressDisp))
				{
					String act = shipandPaypageShipAddressColorDisp.getCssValue("color");
					String actual = HBCBasicfeature.colorfinder(act);
					log.add("Actual higlighted color is: "+actual);
					log.add("Expected highlighted color is: "+expected);
					Thread.sleep(500);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("Selected shipping address highlighted as per the creative in the shipping information section",log);
					}
					else
					{
						Fail("Selected shipping address not highlighted as per the creative in the shipping information section",log);
					}					
				}
				else
				{
					Fail("Shipping address not displayed");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1460 Issue ." + e.getMessage());
				Exception(" HBC - 1460 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1514 Reg Checkout - Payment Information Section > Verify that "Billing Address" overlay/page should be shown with the prefilled billing details on clicking the Edit link.*/
	/* HBC - 1515 Reg Checkout - Payment Information Section > Verify that "X" close icon should be shown in the "Billing Address" overlay/page.*/
	/* HBC - 1516 Reg Checkout - Payment Information Section > Verify that "Ok" button & "Cancel" button should be shown in the "Billing Address" overlay/page.*/
	/* HBC - 1517 Reg Checkout - Payment Information Section > Verify that "Billing Address" overlay/page should be closed on tapping the "Cancel" button*/
	public void PayAddressEditView()
	{
		ChildCreation(" HBC - 1514 Reg Checkout - Payment Information Section > Verify that Billing Address overlay/page should be shown with the prefilled billing details on clicking the Edit link.");
		String cnty = "";
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
				{
					paybefAdd = shipandPaypagePayAddressDisp.getText();
				}
					
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					jsclick(shipandPaypagePayEditLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Payment Address Overlay is dispalyed.");
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The First Name Value is not displayed.");
							}
							else
							{
								Pass( " The First Name value is displayed. The displayed value is  : " + txt);
							}
						}
						else
						{
							Fail( "The First Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The Last Name Value is not displayed.");
							}
							else
							{
								Pass( " The Last Name value is displayed. The displayed value is  : " + txt);
							}
						}
						else
						{
							Fail( "The Last Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The Street Address Name Value is not displayed.");
							}
							else
							{
								Pass( " The Street Address value is displayed. The displayed value is  : " + txt);
							}
						}
						else
						{
							Fail( "The Street Address is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City Field is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssCty.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The City Name Value is not displayed.");
							}
							else
							{
								Pass( " The City Name value is displayed. The displayed value is  : " + txt);
							}
						}
						else
						{
							Fail( "The City Field is not displayed in the Shipping Address Page.");
						}

						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country is displayed in the Shipping Address Page.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							cnty = sel.getFirstSelectedOption().getText();
							if(cnty.isEmpty())
							{
								Fail( "The Country Name Value is not displayed.");
							}
							else
							{
								Pass( " The Country Name value is displayed. The displayed value is  : " + cnty);
							}
						}
						else
						{
							Fail( "The Country is not displayed in the Shipping Address Page.");
						}
						
						if(cnty.contains("Canada")||(cnty.contains("States")))
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
							{
								Pass( "The State Field is displayed in the Shipping Address Page.");
								Select sel = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
								String txt = sel.getFirstSelectedOption().getText();
								if(txt.isEmpty())
								{
									Fail( "The State Name Value is not displayed.");
								}
								else
								{
									Pass( " The State Name value is displayed. The displayed value is  : " + txt);
								}
							}
							else
							{
								Fail( "The State Field is not displayed in the Shipping Address Page.");
							}
						}
						else
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
							{
								Pass( "The State Field is displayed in the Shipping Address Page.");
								Select sel = new Select(shipandPaypageShipShipCreateEditAddresssPayUKState);
								String txt = sel.getFirstSelectedOption().getText();
								if(txt.isEmpty())
								{
									Fail( "The State Name Value is not displayed.");
								}
								else
								{
									Pass( " The State Name value is displayed. The displayed value is  : " + txt);
								}
							}
							else
							{
								Fail( "The State Field is not displayed in the Shipping Address Page.");
							}
						}
						
						if(cnty.contains("States"))
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
							{
								Pass( "The PO Code Field is displayed in the Shipping Address Page.");
								String txt = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value");
								if(txt.isEmpty())
								{
									Fail( "The First Name Value is not displayed.");
								}
								else
								{
									Pass( " The First Name value is displayed. The displayed value is  : " + txt);
								}
							}
							else
							{
								Fail( "The PO Code is not displayed in the Shipping Address Page.");
							}
						}
						else
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
							{
								Pass( "The PO Code Field is displayed in the Shipping Address Page.");
								String txt = shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value");
								if(txt.isEmpty())
								{
									Fail( "The First Name Value is not displayed.");
								}
								else
								{
									Pass( " The First Name value is displayed. The displayed value is  : " + txt);
								}
							}
							else
							{
								Fail( "The PO Code is not displayed in the Shipping Address Page.");
							}
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
						{
							Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The Contact Number Value is not displayed.");
							}
							else
							{
								Pass( " The Contact Number value is displayed. The displayed value is  : " + txt);
							}
							
							String txt1 = shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value");
							if(txt1.isEmpty())
							{
								Fail( "The Contact Number Value is not displayed.");
							}
							else
							{
								Pass( " The Contact Number value is displayed. The displayed value is  : " + txt1);
							}
							
							String txt2 = shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value");
							if(txt2.isEmpty())
							{
								Fail( "The Contact Number Value is not displayed.");
							}
							else
							{
								Pass( " The Contact Number value is displayed. The displayed value is  : " + txt2);
							}
						}
						else
						{
							Fail( "The Phone Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Close Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
						{
							Pass( "The Cancel Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Cancel Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
						{
							Pass( "The Ok button in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Ok button in Create overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Payment Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1514 Issue ." + e.getMessage());
				Exception(" HBC - 1514 Issue ." + e.getMessage());
			}
			
			/* HBC - 1515 Reg Checkout - Payment Information Section > Verify that "X" close icon should be shown in the "Billing Address" overlay/page.*/
			ChildCreation(" HBC - 1515 Reg Checkout - Payment Information Section > Verify that X close icon should be shown in the Billing Address overlay/page.");
			try
			{
				if(shippingPage==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Edit overlay is displayed.");
						}
						else
						{
							Fail( "The Close Link in Edit overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Payment Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Skip ( " The User was unable to select the address from the verification page in the checkout page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1515 Issue ." + e.getMessage());
				Exception(" HBC - 1515 Issue ." + e.getMessage());
			}
			
			/* HBC - 1516 Reg Checkout - Payment Information Section > Verify that "Ok" button & "Cancel" button should be shown in the "Billing Address" overlay/page.*/
			ChildCreation(" HBC - 1516 Reg Checkout - Payment Information Section > Verify that Ok button & Cancel button should be shown in the Billing Address overlay/page.");
			try
			{
				if(shippingPage==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
						{
							Pass( "The Cancel Link in Edit overlay is displayed.");
						}
						else
						{
							Fail( "The Cancel Link in Edit overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
						{
							Pass( "The Ok button in Edit overlay is displayed.");
						}
						else
						{
							Fail( "The Ok button in Edit overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Payment Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1516 Issue ." + e.getMessage());
				Exception(" HBC - 1516 Issue ." + e.getMessage());
			}
			
			/* HBC - 1517 Reg Checkout - Payment Information Section > Verify that "Billing Address" overlay/page should be closed on tapping the "Cancel" button.*/
			ChildCreation(" HBC - 1517 Reg Checkout - Payment Information Section > Verify that Billing Address overlay/page should be closed on tapping the Cancel button.");
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
				{
					Pass( "The Cancel button in the Edit Overlay is displayed.");
					jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
					Thread.sleep(1000);
					if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Edit Address Overlay is not displayed.");
					}
					else
					{
						Fail("The Saved Address list is not displayed.");
					}
				}
				else
				{
					Fail( "The Cancel button in the Edit Overlay is displayed is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1517 Issue ." + e.getMessage());
				Exception(" HBC - 1517 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1518 Reg Checkout - Payment Information Section > Verify that "Billing Address" overlay/page should be closed on tapping the "X" icon. */
	public void PayEditAddressOverlayClose()
	{
		ChildCreation(" HBC - 1518 Reg Checkout - Payment Information Section > Verify that Billing Address overlay/page should be closed on tapping the X icon.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					jsclick(shipandPaypagePayEditLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Edit Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Edit overlay is displayed.");
							jsclick(shipandPaypageShipShipCreateEditAddressCloseLink);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
							Thread.sleep(1000);
							if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
							{
								Pass( "The Address Edit Overlay is closed.");
							}
							else
							{
								Fail( "The Address Edit Overlay is not closed.");
							}
						}
						else
						{
							Fail( "The Close Link in Edit overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Payment Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Payment Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1518 Issue ." + e.getMessage());
				Exception(" HBC - 1518 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1520 Reg Checkout - Billing Address Overlay/Page > Verify that "Billing Address" overlay/page should be as per the creative. */
	public void PayCreateShippingAddress()
	{
		ChildCreation(" HBC - 1520 Reg Checkout - Billing Address Overlay/Page > Verify that Billing Address overlay/page should be as per the creative.");
		String cnty = "";
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayCreateAddLink))
				{
					jsclick(shipandPaypagePayCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The First Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Last Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Street Address is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The City Field is not displayed in the Shipping Address Page.");
						}

						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country is displayed in the Shipping Address Page.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							cnty = sel.getFirstSelectedOption().getText();
						}
						else
						{
							Fail( "The Country is not displayed in the Shipping Address Page.");
						}
						
						if(cnty.contains("Canada")||(cnty.contains("States")))
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
							{
								Pass( "The State Field is displayed in the Shipping Address Page.");
							}
							else
							{
								Fail( "The State Field is not displayed in the Shipping Address Page.");
							}
						}
						else
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
							{
								Pass( "The State Field is displayed in the Shipping Address Page.");
							}
							else
							{
								Fail( "The State Field is not displayed in the Shipping Address Page.");
							}
						}
						
						if(cnty.contains("States"))
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
							{
								Pass( "The PO Code Field is displayed in the Shipping Address Page.");
							}
							else
							{
								Fail( "The PO Code is not displayed in the Shipping Address Page.");
							}
						}
						else
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
							{
								Pass( "The PO Code Field is displayed in the Shipping Address Page.");
							}
							else
							{
								Fail( "The PO Code is not displayed in the Shipping Address Page.");
							}
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
						{
							Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Phone Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
						{
							Pass( "The Email Field in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Email Field in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditTermsandCond))
						{
							Pass( "The Terms and Condition in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Terms and Condition in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Close Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
						{
							Pass( "The Cancel Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Cancel Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
						{
							Pass( "The Ok button in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Ok button in Create overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Create Billing Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1520 Issue ." + e.getMessage());
				Exception(" HBC - 1520 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1522 Reg Checkout - Verify that "Billing Address" overlay/page should have the following text boxes : "First Name", "Last Name", "Street Address", and so on. */
	public void PayCreateShippingAddressFields(String tc)
	{
		String tcId = tc;
		if(tcId=="HBC1522")
		{
			tcId = "HBC - 1522 Reg Checkout - Verify that Billing Address overlay/page should have the following text boxes : First Name, Last Name, Street Address, and so on..";
		}
		else
		{
			tcId = "HBC - 1525 Reg Checkout - Verify that required input fields needs to be displayed to add a new Billing address";
		}
		ChildCreation(tcId);
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The City Field is not displayed in the Shipping Address Page.");
					}

					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						payDefSelCountr = sel.getFirstSelectedOption().getText();
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					else if(payDefSelCountr.contains("Kingdom"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					
					if(payDefSelCountr.contains("States"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
					{
						Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Phone Code is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
					{
						Pass( "The Email Field in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Email Field in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditTermsandCond))
					{
						Pass( "The Terms and Condition in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Terms and Condition in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
					{
						Pass( "The Close Link in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Close Link in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
					{
						Pass( "The Cancel Link in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Cancel Link in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
					{
						Pass( "The Ok button in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Ok button in Create overlay is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1522 Issue ." + e.getMessage());
				Exception(" HBC - 1522 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1523 Register Checkout - Verify that mandatory field names should be mentioned with star(*) in the left side of textbox in the "Billing  Address" overlay*/
	public void PaymentPageCreateShipOverlayMandatoryFields() throws Exception
	{
		ChildCreation("HBC - 1523 Register Checkout - Verify that mandatory field names should be mentioned with star(*) in the left side of textbox in the 'Billing  Address' overlay");
		String star = HBCBasicfeature.getExcelVal("HBC1471", sheet, 1);
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Billing Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssFnameLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The First Name field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The First Name field displayed in the Shipping Address Page not mentioned with star(*).");
						}						
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssLNameLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The Last Name field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The Last Name field displayed in the Shipping Address Page not mentioned with star(*).");
						}										
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssStAddressLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The  Street Address field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The  Street Address displayed in the Shipping Address Page not mentioned with star(*).");
						}										
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssCtyLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The  City field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The City field displayed in the Shipping Address Page not mentioned with star(*).");
						}			
					}
					else
					{
						Fail( "The City Field is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssCountryLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The  Country field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The Country field displayed in the Shipping Address Page not mentioned with star(*).");
						}	
						
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						payDefSelCountr = sel.getFirstSelectedOption().getText();
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
						{
							String expctdStar = shipandPaypageShipShipCreateEditAddresssStateLabel.getText().substring(0,1);
							if(expctdStar.contains(star))
							{
								Pass( "The  Canda/US Province/State field displayed in the Shipping Address Page mentioned with star(*).");
							}
							else
							{
								Fail("The Canda/US Province/State field displayed in the Shipping Address Page not mentioned with star(*).");
							}		
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					else if(payDefSelCountr.contains("Kingdom"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							String expctdStar = shipandPaypageShipShipCreateEditAddresssUKContryLabel.getText().substring(0,1);
							if(expctdStar.contains(star))
							{
								Pass( "The UK Province field displayed in the Shipping Address Page mentioned with star(*).");
							}
							else
							{
								Fail("The UK Province field displayed in the Shipping Address Page not mentioned with star(*).");
							}		
						}
						else
						{
							Fail( "The UK State Field is not displayed in the Shipping Address Page.");
						}
					}
					
					if(payDefSelCountr.contains("States"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							String expctdStar = shipandPaypageShipShipCreateEditAddresssPOcodeLabel.getText().substring(0,1);
							if(expctdStar.contains(star))
							{
								Pass( "The US Zip Code field displayed in the Shipping Address Page mentioned with star(*).");
							}
							else
							{
								Fail("The US Zip Code field displayed in the Shipping Address Page not mentioned with star(*).");
							}		
						}
						else
						{
							Fail( "The  US Zip Code Code is not displayed in the Shipping Address Page.");
						}
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							String expctdStar = shipandPaypageShipShipCreateEditAddresssNonUsPOcodeLabel.getText().substring(0,1);
							if(expctdStar.contains(star))
							{
								Pass( "The  UK/Canda PO Code field displayed in the Shipping Address Page mentioned with star(*).");
							}
							else
							{
								Fail("The UK/Canda PO Code field displayed in the Shipping Address Page not mentioned with star(*).");
							}		
						}
						else
						{
							Fail( "The UK/Canda PO Code is not displayed in the Shipping Address Page.");
						}
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssPHLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The UK Phone Code field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The UK Phone Code field displayed in the Shipping Address Page not mentioned with star(*).");
						}		
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssUKPhone))
						{
							String expctdStar = shipandPaypageShipShipCreateEditAddresssPHUKLabel.getText();
							expctdStar.trim().substring(0, 1);
							if(expctdStar.contains(star))
							{
								Pass( "The Canada/US Phone Code field displayed in the Shipping Address Page mentioned with star(*).");
							}
							else
							{
								Fail("The Canada/US Phone Code field displayed in the Shipping Address Page not mentioned with star(*).");
							}		
						}
						else
						{
							Fail( "The Canada/US Phone Code is not displayed in the Shipping Address Page.");
						}
					}				
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
					{
						String expctdStar = shipandPaypageShipShipCreateEditAddresssEmailLabel.getText().substring(0,1);
						if(expctdStar.contains(star))
						{
							Pass( "The Email field displayed in the Shipping Address Page mentioned with star(*).");
						}
						else
						{
							Fail("The Email field displayed in the Shipping Address Page not mentioned with star(*).");
						}		
					}
					else
					{
						Fail( "The Email Field is not displayed in the Shipping Address Page.");
					}					
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}			
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1523 Issue ." + e.getMessage());
				Exception(" HBC - 1523 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "Shipping & payment page not displayed");
		}
	}
	
	/* HBC - 1526 Verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address.*/
	public void PaymentAddressFieldsTabFocus()
	{
		ChildCreation(" HBC - 1526 Verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address.");
		if(shippingPage==true)
		{
			try
			{
				Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
				sel.selectByValue("CA");
				Thread.sleep(1000);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{			
					Actions act = new Actions(driver);
					shipandPaypageShipShipCreateEditAddresssFName.click();
					for(int i = 1; i < 13 ; i++)
					{
						act.sendKeys(Keys.TAB).build().perform();
						Pass("The tab focus for " +  i  + " field is made successfully.");
					}
				}
				else
				{
					Fail( "The Edit Shipping Address overlay is not displayed.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1526 Issue ." + e.getMessage());
				Exception(" HBC - 1526 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1527 Reg Checkout - Billing Address Section > Verify that user should be able to enter first name in the "First Name" field and it should accept maximum of 64 characters.*/
	public void PayFNameEnterMax()
	{
		ChildCreation(" HBC - 1527 Reg Checkout - Billing Address Section > Verify that user should be able to enter first name in the First Name field and it should accept maximum of 64 characters.");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1475", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
				{
					Pass( "The First Name field is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[i]);
						if(shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").isEmpty())
						{
							Fail( "The First Name is empty.");
						}
						else if(shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").length()<=64)
						{
							Pass( "The first name was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The first name was enterable but it is not within the specified character limit.",log);
						}
						shipandPaypageShipShipCreateEditAddresssFName.clear();
					}
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1527 Issue ." + e.getMessage());
				Exception(" HBC - 1527 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1528 Reg Checkout - Billing Address Section > Verify that user should be able to enter last name in the "Last Name" field and it should accept maximum of 64 characters.*/
	public void PayLNameEnterMax()
	{
		ChildCreation(" HBC - 1528 Reg Checkout - Billing Address Section > Verify that user should be able to enter last name in the Last Name field and it should accept maximum of 64 characters.");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1475", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
				{
					Pass( "The Last Name field is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[i]);
						if(shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").isEmpty())
						{
							Fail( "The Last Name is empty.");
						}
						else if(shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").length()<=64)
						{
							Pass( "The last name was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The last name was enterable but it is not within the specified character limit.",log);
						}
						shipandPaypageShipShipCreateEditAddresssLName.clear();
					}
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1528 Issue ." + e.getMessage());
				Exception(" HBC - 1528 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1529 Reg Checkout - Billing Address Section > Verify that only characters should be accepted in "First Name" and "Last Name" field.*/
	public void PayFNLNameNumValidation()
	{
		ChildCreation(" HBC - 1529 Reg Checkout - Billing Address Section > Verify that only characters should be accepted in First Name and Last Name field.");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HB1477", sheet, 1).split("\n");
				String stAdd = HBCBasicfeature.getExcelVal("HBC1477", sheet, 3);
				String cty = HBCBasicfeature.getExcelVal("HBC1477", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1477", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1477", sheet, 6);
				String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 7);
				String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 8);
				String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 9);
				String ema = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 10);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[0]);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Value from the excel file was : " + cellVal[1].toString());
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[1]);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.click();
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						log.add( "The Value from the excel file was : " + stAdd);
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.click();
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						log.add( "The Value from the excel file was : " + cty);
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						sel.selectByValue("CA");
						Thread.sleep(500);
						payDefSelCountr = sel.getFirstSelectedOption().getText();
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
							shipandPaypageShipShipCreateEditAddresssPayUKState.sendKeys(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					
					if(payDefSelCountr.contains("States"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.click();
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							log.add( "The Value from the excel file was : " + pocode);
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.click();
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
							log.add( "The Value from the excel file was : " + pocode);
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(pocode);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						log.add( "The Value from the excel file was : " + cNum1);
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						log.add( "The Value from the excel file was : " + cNum2);
						shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.click();
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						log.add( "The Value from the excel file was : " + cNum3);
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditEmail.click();
						shipandPaypageShipShipCreateEditEmail.clear();
						log.add( "The Value from the excel file was : " + ema);
						shipandPaypageShipShipCreateEditEmail.sendKeys(ema);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					boolean addOverlay = ldNoAddressVerification();
					if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
					{
						jsclick(addressVerificationEdit);
						wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
						Thread.sleep(1000);
					}
					
					if(addOverlay==true)
					{
						Pass( "The User is not navigated to the Address Verification page.");
					}
					else
					{
						Fail( "The User is navigated to the Address Verification page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1529 Issue ." + e.getMessage());
				Exception(" HBC - 1529 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1530 Reg Checkout - Verify that "Country" field should should be selected in "Canada" option by default.*/
	public void ShipPageCreatePayDefaultCountry()
	{
		ChildCreation(" HBC - 1530 Reg Checkout - Verify that Country field should should be selected in Canada option by default.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1485", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String txt = sel.getFirstSelectedOption().getText().replaceAll("\\s", "");
						log.add( "The Expected Value was : " + expVal);
						log.add( "The Actual Value is : " + txt);
						if(txt.isEmpty())
						{
							Fail( "The Country Selection seems to be empty.");
						}
						else if(txt.equalsIgnoreCase(expVal))
						{
							Pass( " The Default Selected country is as expected.",log);
						}
						else
						{
							Fail( "There is mismatch in the expected and the actual default country selection.",log);
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1530 Issue ." + e.getMessage());
				Exception(" HBC - 1530 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1532 Reg Checkout - Billing Address Overlay/Page > Verify that "Street Address" field should have two text boxes.*/
	public void ShipPageCreatePayStreetAddressFields()
	{
		ChildCreation(" HBC - 1532 Reg Checkout - Billing Address Overlay/Page > Verify that Street Address field should have two text boxes.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress2))
					{
						Pass( "The  Street Address 2 is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The  Street Address 2 is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1532 Issue ." + e.getMessage());
				Exception(" HBC - 1532 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
		
	/* HBC - 1533 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter street address in the "Street Address" field.*/
	public void PayStAddFieldsLenVal()
	{
		ChildCreation(" HBC - 1533 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter street address in the Street Address field.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Billing Address Overlay is dispalyed.");
					String st1 = HBCBasicfeature.getExcelVal("HBC1533", sheet, 1);
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						log.add( "The Entered value from the excel file is : " + st1);
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(st1);
						String curVal = shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value");
						log.add( "The Current value in the field is : " + curVal);
						if(curVal.isEmpty())
						{
							Fail( "The Street Address is empty.");
						}
						else if(curVal.equals(st1))
						{
							Pass( "The Street Address was enterable and it is matches the exact content.",log);
						}
						else
						{
							Fail( "The Street Address was enterable but it is there is some mismatch.",log);
						}
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress2))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress2.clear();
						log.add( "The Entered value from the excel file is : " + st1);
						shipandPaypageShipShipCreateEditAddresssStAddress2.sendKeys(st1);
						String curVal = shipandPaypageShipShipCreateEditAddresssStAddress2.getAttribute("value");
						log.add( "The Current value in the field is : " + curVal);
						if(curVal.isEmpty())
						{
							Fail( "The Street Address is empty.");
						}
						else if(curVal.equals(st1))
						{
							Pass( "The Street Address was enterable and it is matches the exact content.",log);
						}
						else
						{
							Fail( "The Street Address was enterable but it is there is some mismatch.",log);
						}
						shipandPaypageShipShipCreateEditAddresssStAddress2.clear();
					}
					else
					{
						Fail( "The Street Address 2 field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1533 Issue ." + e.getMessage());
				Exception(" HBC - 1533 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1534 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter characters in the "City" field.*/
	public void PayBillCityEnterable()
	{
		ChildCreation(" HBC - 1534 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter characters in the City field.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Billing Address Overlay is dispalyed.");
					String cityy = HBCBasicfeature.getExcelVal("HBC1481", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.click();
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						log.add( "The Entered value from the excel file is : " + cityy);
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cityy);
						String ctyVal = shipandPaypageShipShipCreateEditAddresssCty.getAttribute("value");
						
						if(ctyVal.isEmpty())
						{
							Fail( "The city field is empty.",log);
						}
						else
						{
							Pass( "The city name was enterable.",log);
						}
						shipandPaypageShipShipCreateEditAddresssCty.clear();
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1534 Issue ." + e.getMessage());
				Exception(" HBC - 1534 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1535 Reg Checkout - Billing Address Section > Verify that "City" field should accept numbers and special characters and the corresponding alert message should be shown.
	public void PayCityyValidation()
	{
		ChildCreation(" HBC - 1535 Reg Checkout - Billing Address Section > Verify that City field should accept numbers and special characters and the corresponding alert message should be shown.");
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1186", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1186", sheet, 2);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
				{
					Pass( "The First Name field is displayed.");
					shipandPaypageShipShipCreateEditAddresssCty.click();
					shipandPaypageShipShipCreateEditAddresssCty.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cellVal);
					shipandPaypageShipShipCreateEditAddresssStAddress2.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBCityAlert,"style","block"));
					String cityAlert = checkoutBCityAlert.getText();
					
					if(cityAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The City alert was as expected.",log);
					}
					else
					{
						Fail( "The City alert was not as expected.",log);
					}
					shipandPaypageShipShipCreateEditAddresssCty.clear();
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1535 Issue ." + e.getMessage());
				Exception(" HBC - 1535 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}*/
	
	
	/* HBC - 1536 Reg Checkout - Billing Address Overlay/Page > Verify that "Country" field should have a maximum of 3 dropdown options.*/
	public void PayBillingCountryOptions()
	{
		ChildCreation(" HBC - 1536 Reg Checkout - Billing Address Overlay/Page > Verify that Country field should have a maximum of 3 dropdown options.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Billing Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						int cntysize = sel.getOptions().size();
						for( int i = 0; i<cntysize;i++)
						{
							String cnty = sel.getOptions().get(i).getText();
							log.add( "The Displayed country option is  : " + cnty);
						}
						if(cntysize==3)
						{
							Pass( "The Country Default Options is of 3 Sizes.",log);
						}
						else
						{
							Fail( "The Country Default Options is of not the expected 3.The displayed total country sizes are : " + cntysize, log);
						}
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1536 Issue ." + e.getMessage());
				Exception(" HBC - 1536 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1537 Reg Checkout - Billing Address Overlay/Page > Verify that "Province" dropdown field should be shown, when the country is selected in Canada.*/
	public void PayBillingCADefaultStateSel()
	{
		ChildCreation(" HBC - 1537 Reg Checkout - Billing Address Overlay/Page > Verify that Province dropdown field should be shown, when the country is selected in Canada.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Billing Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1537", sheet, 1);
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStateLabel))
						{
							Pass( "The Province Label is displayed.");
							boolean sel = shipandPaypageShipShipCreateEditAddresssStateLabel.getAttribute("style").contains("block");
							if(sel==true)
							{
								Pass( "The Province Drop Down is selected.");
								log.add( "The expected Value was : " + expVal);
								String txt = shipandPaypageShipShipCreateEditAddresssStateLabel.getText().replaceAll("\\s", "");
								log.add( "The Actual Value was : " + txt);
								if(txt.contains(expVal))
								{
									Pass( " The Expected and actual label matches.",log);
								}
								else
								{
									Fail( " The Expected and actual label does not matches.",log);
								}
							}
							else
							{
								Fail( " The Province Drop Dwon is not selected.");
							}
						}
						else
						{
							Fail( " The Province label is not displayed.");
						}
					}
					else
					{
						Fail( "The User failed to select the CANADA Country.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1537 Issue ." + e.getMessage());
				Exception(" HBC - 1537 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1538 Reg Checkout - Billing Address Overlay/Page > Verify that "Select" option should be selected by default in the "Province" dropdown field.*/
	public void PayShippingCADefaultStateSel()
	{
		ChildCreation(" HBC - 1538 Reg Checkout - Billing Address Overlay/Page >  Verify that 'Select' option should be selected by default in the 'Province' dropdown field.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1486", sheet, 1);
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStateLabel)))
						{
							Pass( "The Province Drop Down is displayed.");
							boolean sel = shipandPaypageShipShipCreateEditAddresssStateLabel.getAttribute("style").contains("block");
							if(sel==true)
							{
								Pass( "The Province Drop Down is selected.");
								Select sell = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
								sell.selectByVisibleText("Select");
								Thread.sleep(1000);
								String actVal = sell.getFirstSelectedOption().getText();
								log.add( "The Expected Value was : " + expVal);
								log.add( "The Actual Value is : " + actVal);
								if(expVal.equalsIgnoreCase(actVal))
								{
									Pass( "The Expected Default Value is selected.",log);
								}
								else
								{
									Fail( "The Expected Default Value is not selected.",log);
								}
							}
							else
							{
								Fail( " The Province Drop Dwon is not selected.");
							}
						}
						else
						{
							Fail( " The Province label is not displayed.");
						}
					}
					else
					{
						Fail( "The User failed to select the CANADA Country.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1538 Issue ." + e.getMessage());
				Exception(" HBC - 1538 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1539 Reg Checkout - Billing Address Overlay/Page > Verify that "Province" dropdown field should be changed to textbox field, when the country is selected in "United Kingdom".*/
	public void PayBillingUKStateText()
	{
		ChildCreation(" HBC - 1539 Reg Checkout - Billing Address Overlay/Page > Verify that Province dropdown field should be changed to textbox field, when the country is selected in United Kingdom");
		if(shippingPage==true)
		{
			try
			{
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Kingdom");
						if(cnty==false)
						{
							sel.selectByValue("GB");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayUKState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(payDefSelCountr.contains("Kingdom"))
					{
						Pass( "The Desired Country was Selected.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							Pass( "Province dropdown field changed to textbox field, when the country is selected in United Kingdom");
							jsclick(shipandPaypageShipShipCreateEditAddresssPayUKState);
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
							{
								Pass( "The Country is displayed in the Shipping Address Page.");
								Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
								sel.selectByValue("CA");
								Thread.sleep(500);
								payDefSelCountr = sel.getFirstSelectedOption().getText();
							}
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					else
					{
						Fail( "The Desired Country was not selected.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1539 Issue ." + e.getMessage());
				Exception(" HBC - 1539 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1531 Reg Checkout - Billing Address Overlay/Page > Verify that "Province", "Postal Code" texboxes gets hidden and "State", "Zip Code" text boxes is shown on clicking "United States" option in the country dropdown.*/
	/* HBC - 1540 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to select state from the "State" drop down, when the country is selected in "United States".*/
	/* HBC - 1541 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter zip code details in the "Zip Code" field, when the country is selected in "United States".*/
	/* HBC - 1542 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter maximum of 5 numbers in the "Zip Code" field as per the classic site.*/
	/* HBC - 1543 Reg Checkout - Billing Address Overlay/Page > Verify that "Zip Code" field should accept numbers and special characters and the corresponding alert message should be shown as per the classic site.*/
	/* HBC - 1544 Reg Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 5 digits in the "Zip Code" field as per the classic site.*/
	public void PayShipPageCreateChangeCountry()
	{
		ChildCreation(" HBC - 1531 Reg Checkout - Billing Address Overlay/Page > Verify that Province, Postal Code texboxes gets hidden and State, Zip Code text boxes is shown on clicking United States option in the country dropdown.");
		boolean HBC1540 = false, HBC1541 = false, HBC1544 = false; 
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("States");
						if(cnty==false)
						{
							sel.selectByValue("US");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("States"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							Fail( "The Province Drop Down box is displayed.");
						}
						else
						{
							Pass( "The Province Drop Down box is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Fail( "The Postal Code Field is displayed.");
						}
						else
						{
							Pass( "The Postal Code Field box is not displayed.");
							
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
						{
							Pass( "The State Drop Down box is displayed.");
							Select stSel = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
							String orignialTxt = stSel.getFirstSelectedOption().getText();
							log.add( "The Original State before selection was : " + orignialTxt);
							stSel.selectByValue("NY");
							String seleSt = stSel.getFirstSelectedOption().getText();
							log.add( "The State after selection was : " + seleSt);
							if(orignialTxt.equals(seleSt))
							{
								Fail( "The Original State before and after selection seems to be same.",log);
							}
							else
							{
								Pass( "The Original State before and after selection seems to be different.",log);
								HBC1540 = true;
							}
						}
						else
						{
							Fail( "The State Drop Down box is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code Field box is displayed.");
							String zpval = HBCBasicfeature.getExcelNumericVal("HBC1531", sheet, 1);
							
							shipandPaypageShipShipCreateEditAddresssPOcode.click();
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							log.add( "The Entered value from the excel file is : " + zpval);
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(zpval);
							String zpVal = bpostalCode.getAttribute("value");
							
							if(zpVal.isEmpty())
							{
								Fail( "The Zip Code  field is empty.",log);
							}
							else if(zpval.equalsIgnoreCase(zpVal))
							{
								Pass( "The Zip Code was enterable and it matches.",log);
								HBC1541 = true;
							}
							else
							{
								Fail( "The Zip Code was enterable and it does not matches.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						}
						else
						{
							Fail( "The Province Drop Down box is not displayed.");
						}
					}
					else
					{
						Fail( "The User failed to load the Country United States.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1531 Issue ." + e.getMessage());
				Exception(" HBC - 1531 Issue ." + e.getMessage());
			}
			
			/* HBC - 1540 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to select state from the "State" drop down, when the country is selected in "United States".*/
			ChildCreation(" HBC - 1540 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to select state from the State drop down, when the country is selected in United States");
			try
			{
				log.add( "Please refer HBC - 1531 log for details.");
				if(HBC1540==true)
				{
					Pass( "The Original State before and after selection seems to be different.",log);
				}
				else
				{
					Fail( "The Original State before and after selection seems to be same.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1540 Issue ." + e.getMessage());
				Exception(" HBC - 1540 Issue ." + e.getMessage());
			}
			
			/* HBC - 1541 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter zip code details in the "Zip Code" field, when the country is selected in "United States".*/
			ChildCreation(" HBC - 1541 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter zip code details in the Zip Code field, when the country is selected in United States");
			try
			{
				log.add( "Please refer HBC - 1531 log for details.");
				if(HBC1541==true)
				{
					Pass( "The Zip Code was enterable and it matches.",log);
				}
				else
				{
					Fail( "The Zip Code was enterable and it does not matches.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1541 Issue ." + e.getMessage());
				Exception(" HBC - 1541 Issue ." + e.getMessage());
			}
			
			/* HBC - 1542 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter maximum of 5 numbers in the "Zip Code" field as per the classic site.*/
			ChildCreation("HBC - 1542 Reg Checkout - Billing Address Overlay/Page > Verify that user should be able to enter maximum of 5 numbers in the Zip Code field as per the classic site.");
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1542", sheet, 1).split("\n");
				Select sel;
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
				{
					sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						Thread.sleep(500);
					}
				}
				
				if(payDefSelCountr.contains("States"))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code Area is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal[i]);
							int currVal = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").length();
							log.add( "The Current length in the PO Field is  : " + currVal);
							if(shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").isEmpty())
							{
								Fail( "The Zip Code field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").length()<=5)
							{
								Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						}
					}
					else
					{
						Fail( "The Province Drop Down box is not displayed.");
					}
				}
				else
				{
					Fail( "The User failed to load the Country United States.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1542 Issue ." + e.getMessage());
				Exception(" HBC - 1542 Issue ." + e.getMessage());
			}
			
			/* HBC - 1543 Reg Checkout - Billing Address Overlay/Page > Verify that "Zip Code" field should accept numbers and special characters and the corresponding alert message should be shown as per the classic site.*/
			ChildCreation(" HBC - 1543 Reg Checkout - Billing Address Overlay/Page > Verify that Zip Code field should accept numbers and special characters and the corresponding alert message should be shown as per the classic site.");
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1543", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1543", sheet, 2);

				if(payDefSelCountr.contains("States"))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The expected alert was : " + alert.toString());
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						String poAlert = checkoutBPOAlert.getText();
						log.add( "The actual alert was : " + poAlert);
						if(poAlert.equalsIgnoreCase(alert.toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
							HBC1544 = true;
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Province Drop Down box is not displayed.");
					}
				}
				else
				{
					Fail( "The User failed to select the drop down field.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1543 Issue ." + e.getMessage());
				Exception(" HBC - 1543 Issue ." + e.getMessage());
			}
			
			/* HBC - 1544 Reg Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 5 digits in the "Zip Code" field as per the classic site.*/
			ChildCreation(" HBC - 1544 Reg Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 5 digits in the Zip Code field as per the classic site.");
			try
			{
				log.add( "Please refer HBC - 1543 log for details.");
				if(HBC1544==true)
				{
					Pass( "The Zip Code alert was as expected.",log);
				}
				else
				{
					Fail( "The Zip Code alert was not as expected.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1544 Issue ." + e.getMessage());
				Exception(" HBC - 1544 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1545 Register Checkout - Billing Address Overlay/Page > Verify that "Postal Code" field should accept alphanumeric characters.*/
	public void ShippingCAPOAlphaNumericValidation()
	{
		ChildCreation("HBC - 1545 Register Checkout - Billing Address Overlay/Page > Verify that Postal Code field should accept alphanumeric characters.");
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1545", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(payDefSelCountr.contains("Canada"))
					{
						Pass( "The Desired Country was Selected.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Pass( "The Postal Code Field box is not displayed.");
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.click();
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal.toString());
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(cellVal);
							String val = shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value");
							log.add( "The Current value in the field is : " + val);
							if(val.isEmpty())
							{
								Fail( "The User was not able to enter any value in the Postal Code field of the shipping address.");
							}
							else if(val.equals(cellVal))
							{
								Pass( "The User able to enter value in the Postal Code field of the shipping address and it matches.",log);
							}
							else
							{
								Fail( "The User was able to enter value in the Postal Code field of the shipping address and it does not matches.",log);
							}
							
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
						}
						else
						{
							Fail( "The Postal Code Field is displayed.");
							
						}
					}
					else
					{
						Fail( "The Desired Country was not selected.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1545 Issue ." + e.getMessage());
				Exception(" HBC - 1545 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1546 Register Checkout - Billing Address Overlay/Page > Verify that "Postal Code" field should accept maximum of 15 characters.*/
	public void PayBillingCAPOLength()
	{
		ChildCreation(" HBC - 1546 Register Checkout - Billing Address Overlay/Page > Verify that Postal Code field should accept maximum of 15 characters.");
		if(shippingPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1546", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("states");
						if(cnty==false)
						{
							sel.selectByValue("GB");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
					{
						Pass( "The Postal Code Area is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(cellVal[i]);
							String val = shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value");
							log.add( "The Current value in the field is : " + val);
							if(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value").isEmpty())
							{
								Fail( "The Zip Code field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value").length()<=15)
							{
								Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
						}
					}
					else
					{
						Fail( "The Postal Code Area is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1546 Issue ." + e.getMessage());
				Exception(" HBC - 1546 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1547 Register Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 6 digits in the "Postal Code" field.*/
	/* HBC - 1548 Register Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered alphabets or numbers in the "Postal Code" field as per the classic site.*/
	public void BillingCAPOLengthValidationAlert()
	{
		ChildCreation(" HBC - 1547 Register Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 6 digits in the Postal Code field.");
		boolean HBC1548 = false;
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1489", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					String alert = HBCBasicfeature.getExcelVal("HBC1489", sheet, 2);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.click();
						shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The expected alert was : " + alert.toString());
						shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						Thread.sleep(1000);
						String poAlert = checkoutBPOAlert.getText();
						log.add( "The actual alert was : " + poAlert);
						if(poAlert.equalsIgnoreCase(alert.toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
							HBC1548 = true;
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1547 Issue ." + e.getMessage());
				Exception(" HBC - 1547 Issue ." + e.getMessage());
			}
			
			/* HBC - 1548 Register Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered alphabets or numbers in the "Postal Code" field as per the classic site.*/
			ChildCreation(" HBC - 1548 Register Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered alphabets or numbers in the Postal Code field as per the classic site.");
			try
			{
				log.add( "Refer log 1547 for reports.");
				if(HBC1548==true)
				{
					Pass( "The Zip Code alert was as expected.",log);
				}
				else
				{
					Fail( "The Zip Code alert was not as expected.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1548 Issue ." + e.getMessage());
				Exception(" HBC - 1548 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1551 Register Checkout - Billing Address Overlay/Page > Verify that user should be able to enter phone number in the "Phone Number" field and it should accept maximum of 10 numbers.*/
	/* HBC - 1554 Register Checkout - Billing Address Overlay/Page > Verify that in the "Phone Number" field, the first two text boxes accepts 3 digits and the 3rd text box accepts 4 digits.*/
	public void PayCAPHNumLenValidation(String tc)
	{
		String tcId = tc;
		if(tc=="HBC-1551")
		{
			tcId = "HBC - 1551 Register Checkout - Billing Address Overlay/Page > Verify that user should be able to enter phone number in the Phone Number field and it should accept maximum of 10 numbers.";
		}
		else
		{
			tcId = "HBC - 1554 Register Checkout - Billing Address Overlay/Page > Verify that in the Phone Number field, the first two text boxes accepts 3 digits and the 3rd text box accepts 4 digits.";
		}
		
		ChildCreation(tcId);
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
				
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Postal Code Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1493", sheet, 1).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").length()<=3)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Postal Code Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1493", sheet, 2).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").length()<=3)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1493", sheet, 3).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").length()<=4)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1551 Issue ." + e.getMessage());
				Exception(" HBC - 1551 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1553 Register Checkout - Billing Address Overlay/Page > Verify that "Phone Number" field should have three text boxes as per the classic site.*/
	public void ShipPageCreatePayPHTextBoxes()
	{
		ChildCreation(" HBC - 1553 Register Checkout - Billing Address Overlay/Page > Verify that Phone Number field should have three text boxes as per the classic site.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
					{
						Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Phone Code is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1553 Issue ." + e.getMessage());
				Exception(" HBC - 1553 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1552 Register Checkout - Billing Address Overlay/Page > Verify that corresponding alert message should be shown, when the user entered alphabets or special characters in the "Phone Number" field.*/
	public void PayCAPHAphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1552 Register Checkout - Billing Address Overlay/Page > Verify that corresponding alert message should be shown, when the user entered alphabets or special characters in the Phone Number field.");
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1494", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
				
					String[] alert = HBCBasicfeature.getExcelVal("HBC1494", sheet, 2).split("\n");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert1,"style","block"));
						String ctAlert = checkoutSPHAlert1.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[0].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert3,"style","block"));
						String ctAlert = checkoutSPHAlert3.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[1].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1552 Issue ." + e.getMessage());
				Exception(" HBC - 1552 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1555 Register Checkout - Billing Address Overlay/Page > Verify that "ext." field should accept maximum of 5 digits.*/
	public void BillingCAExtNumLenValidation()
	{
		ChildCreation(" HBC - 1555 Register Checkout - Billing Address Section > Verify that ext. field should accept maximum of 5 digits.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1542", sheet, 1).split("\n");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4))
					{
						Pass( "The Extension Code Area is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH4.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH4.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH4.getAttribute("value").isEmpty())
							{
								Fail( "The Extension Code field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH4.getAttribute("value").length()<=5)
							{
								Pass( "The Extension Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Extension Code was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH4.clear();
						}
					}
					else
					{
						Fail( "The Extension Code is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1555 Issue ." + e.getMessage());
				Exception(" HBC - 1555 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1556 Register Checkout - Billing Address Section > Verify that user should be able to enter email in the "Email:" field and it should accept both alphanumerics and characters.*/
	public void ShipPageBillingValidEmail()
	{
		ChildCreation(" HBC - 1556 Register Checkout - Billing Address Section > Verify that user should be able to enter email in the Email: field and it should accept both alphanumerics and characters.");
		if(shippingPage==true)
		{
			try
			{
				String zpval = HBCBasicfeature.getExcelNumericVal("HBC1556", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
					{
						Pass( "The Email Field is displayed.");
						shipandPaypageShipShipCreateEditEmail.click();
						shipandPaypageShipShipCreateEditEmail.clear();
						log.add( "The Entered value from the excel file is : " + zpval);
						shipandPaypageShipShipCreateEditEmail.sendKeys(zpval);
						Thread.sleep(500);
						String emVal = shipandPaypageShipShipCreateEditEmail.getAttribute("value");
						if(emVal.isEmpty())
						{
							Fail( "The Email Field field is empty.",log);
						}
						else if(zpval.equalsIgnoreCase(emVal))
						{
							Pass( "The Email Field was enterable and it matches.",log);
						}
						else
						{
							Fail( "The Email Field  was enterable and it does not matches.",log);
						}
						shipandPaypageShipShipCreateEditEmail.clear();
					}
					else
					{
						Fail( "The Email Field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1556 Issue ." + e.getMessage());
				Exception(" HBC - 1556 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1557 Register Checkout - Billing Address Section > Verify that email alert should be shown, when the user entered a invalid format in the "Email" field.
	public void BillingCAEmailValidationAlert()
	{
		ChildCreation(" HBC - 1557 Register Checkout - Billing Address Section > Verify that email alert should be shown, when the user entered a invalid format in the Email field.");
		if(shippingPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1557", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1557", sheet, 2);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
				{
					Pass( "The Reward Card Number field is displayed.");
					shipandPaypageShipShipCreateEditEmail.click();
					shipandPaypageShipShipCreateEditEmail.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					shipandPaypageShipShipCreateEditEmail.sendKeys(cellVal);
					shipandPaypageShipShipCreateEditAddresssFName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBcaEmailAlert,"style","block"));
					String emdAlert = checkoutBcaEmailAlert.getText();
					
					if(emdAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Invalid Email alert was as expected.",log);
					}
					else
					{
						Fail( "The Invalid Email alert was not as expected.",log);
					}
					shipandPaypageShipShipCreateEditEmail.clear();
				}
				else
				{
					Fail( "The Email field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1557 Issue ." + e.getMessage());
				Exception(" HBC - 1557 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	*/
		
	/* HBC - 1559 Register Checkout - Billing Address Overlay/Page > Verify that "I agree to receive electronic messages from Hudson's Bay..." check box should be unchecked by default.*/
	public void BillingTermandConditionChkBoxState()
	{
		ChildCreation(" HBC - 1559 Register Checkout - Billing Address Overlay/Page > Verify that I agree to receive electronic messages from Hudson's Bay... check box should be unchecked by default.");
		if(shippingPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditTermsandCond))
					{
						Pass( "The Terms and Condition link is displayed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditTermsandCondChkbox))
						{
							Pass( "The Checkbox is displayed.");
							boolean chkd = shipandPaypageShipShipCreateEditTermsandCondChkbox.getAttribute("value").contains("0");
							if(chkd==false)
							{
								Pass( "The Checkbox is in Unchecked Mode.");
							}
							else
							{
								Fail( "The Checkbox is not in Unchecked Mode.");
							}
							shipandPaypageShipShipCreateEditAddressCloseLink.click();
						}
						else
						{
							Fail( "The Checkbox is not displayed.");
						}
					}
					else
					{
						Fail( "The Terms and Condition link is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1559 Issue ." + e.getMessage());
				Exception(" HBC - 1559 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1524 Reg Checkout -Verify that user should be able to add the new Billing address details in the "Billing Address" overlay.*/	
	/* HBC - 1519 Reg Checkout -Payment Information Section > Verify that edited field details in the "Billing Address" overlay should get updated in "Billing Information" section on tapping the "Ok" button*/	
	/* HBC - 1659  Verify that entered address should be updated in the shipping address and it should redirect to the checkout page on tapping the "Use Address as entered" button*/
	public void PayPageCreateAddressAddShippingAdd() throws Exception
		{
			ChildCreation(" HBC - 1524 Reg Checkout - Verify that user should be able to add the new Billing address details in the 'Billing Address' overlay." );
			String aftAdd = "", sfn = "", lsn = "", befAdd="";
			boolean HBC1519 = false, HBC1659A = false, HBC1659B = false;
			if(shippingPage==true)
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayEditLink))
				{
					befAdd = shipandPaypagePayAddressDisp.getText();			
				}	
				
				try
				{
					sfn = HBCBasicfeature.getExcelVal("HBC1477", sheet, 1);
					lsn = HBCBasicfeature.getExcelVal("HBC1477", sheet, 2);
					String stAdd = HBCBasicfeature.getExcelVal("HBC1477", sheet, 3);
					String cty = HBCBasicfeature.getExcelVal("HBC1477", sheet, 4);
					String prv = HBCBasicfeature.getExcelVal("HBC1477", sheet, 5);
					String pocode = HBCBasicfeature.getExcelVal("HBC1477", sheet, 6);
					String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 7);
					String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 8);
					String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1477", sheet, 9);
					String ema = HBCBasicfeature.getExcelVal("HBC1477", sheet, 10);
					
					if(HBCBasicfeature.isElementPresent(shipandPaypagePayCreateAddLink))
					{
						jsclick(shipandPaypagePayCreateAddLink);
						Thread.sleep(4000);
						wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
						Thread.sleep(1000);				
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
						{
							Pass("Create Billing address overlay displayed");						
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
							{
								Pass( "The First Name field is displayed.");
								shipandPaypageShipShipCreateEditAddresssFName.clear();
								shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
							}
							else
							{
								Fail( "The First Name field is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
							{
								Pass( "The Last Name field is displayed.");
								shipandPaypageShipShipCreateEditAddresssLName.clear();
								shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
							}
							else
							{
								Fail( "The Last Name field is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
							{
								Pass( "The Street Address field is displayed.");
								shipandPaypageShipShipCreateEditAddresssStAddress.clear();
								shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
							}
							else
							{
								Fail( "The Street Address field is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
							{
								Pass( "The City field is displayed.");
								shipandPaypageShipShipCreateEditAddresssCty.clear();
								shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
							}
							else
							{
								Fail( "The City field is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
							{
								Pass( "The Country field is displayed.");
								Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
								sel.selectByValue("CA");
							}
							else
							{
								Fail( "The Country field is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
							{
								Pass( "The State field is displayed.");
								Select sel = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
								sel.selectByValue(prv);
								Thread.sleep(1000);
							}
							else
							{
								Fail( "The State field is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
							{
								Pass( "The Postal Code field is displayed.");
								shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
								shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(pocode);
							}
							else
							{
								Fail( "The Postal Code is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
							{
								Pass( "The Contact Number field is displayed.");
								shipandPaypageShipShipCreateEditAddresssPH1.clear();
								shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
							}
							else
							{
								Fail( "The Contact Number is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
							{
								Pass( "The Contact Number field is displayed.");
								shipandPaypageShipShipCreateEditAddresssPH2.clear();
								shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
							}
							else
							{
								Fail( "The Contact Number is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
							{
								Pass( "The Contact Number field is displayed.");
								shipandPaypageShipShipCreateEditAddresssPH3.clear();
								shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
							}
							else
							{
								Fail( "The Contact Number is not displayed.");
							}
							
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
							{
								Pass( "The Email Field is displayed.");
								shipandPaypageShipShipCreateEditEmail.clear();							
								shipandPaypageShipShipCreateEditEmail.sendKeys(ema);
							}
							else
							{
								Fail( "The Email Field is not displayed.");
							}
								
							boolean addOverlay = ldAddressVerification();
							if(addOverlay==true)
							{
								jsclick(shipandPaypageShipShipCreateEditAddressOkBtn);
								wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
								addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
								if(addVerEnable==true)
								{
									Pass( "The Address Verification Overlay is enabled.");
									if(HBCBasicfeature.isElementPresent(addressuseasEntered))
									{
										Pass( "The Use As Entered button is displayed.");
										jsclick(addressuseasEntered);
										wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
										wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
										wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
										Thread.sleep(1000);
										aftAdd = shipandPaypagePayAddressDisp.getText();
										log.add( "The address before modification is  : " + befAdd);
										log.add( "The address after modification is  : " + aftAdd);
										if((befAdd.isEmpty())||(aftAdd.isEmpty()))
										{
											Fail( "The user failed to get the before address and after address they are empty.",log);
										}
										else if(befAdd.equals(aftAdd))
										{
											Fail( " The newly modified address is not dispalyed.",log);
										}
										else
										{
											log.add( " There is update made to the address list.");
											log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
											log.add( " There is update made to the address list. The entered first name was : " + sfn);
											HBC1519 = true;
											if(aftAdd.contains(sfn))
											{
												HBC1659A = true;
												Pass( "The Added First Name is dispalyed.",log);
											}
											else
											{
												Fail( "The Added First Name is not dispalyed.",log);
											}
											
											log.add( " There is update made to the address list.");
											log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
											log.add( " There is update made to the address list. The entered first name was : " + lsn);
											
											if(aftAdd.contains(lsn))
											{
												HBC1659B = true;
												Pass( "The Added Last Name is dispalyed.",log);
											}
											else
											{
												Fail( "The Added Last Name is not dispalyed.",log);
											}
										}
										
										if(HBCBasicfeature.isElementPresent(shipandPaypageShipPayToName))
										{
											Pass( "The Selected Address Drop Down is displayed.");
											Select sel = new Select(shipandPaypageShipPayToName);
											int size = sel.getOptions().size();
											if(size>1)
											{
												for( int i = 0; i<size; i++)
												{
													String addedAdress = sel.getOptions().get(i).getText();
													log.add(" The displayed address list was : " + addedAdress);
												}
												Pass( "The newly added address is added to the drop down.",log);
												HBC1519 = true;
											}
											else
											{
												Fail( "The newly added address is not added to the drop down.",log);
											}
										}
										else
										{
											Fail( "The Selected Address Drop Down is not displayed.");
										}
									}
									else
									{
										Fail( "The Address Use As entere is not displayed.");
										if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
										{
											jsclick(addressVerificationEdit);
											wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
											Thread.sleep(1000);
											if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
											{
												Pass( "The Cancel Link in Create overlay is displayed.");
												jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
												Thread.sleep(1000);
												wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
												Thread.sleep(1000);
												if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
												{
													Pass( "The Address Edit Overlay is closed.");
												}
												else
												{
													Fail( "The Address Edit Overlay is not closed.");
												}
											}
											else
											{
												Fail( "The Cancel Link in Create overlay is not displayed.");
											}
										}
										else
										{
											Fail( "The Edit link in the Address Verification overlay is not displayed.");
										}
									}
								}
								else
								{
									Fail( "The Address Verification Overlay is not enabled.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail("The Edit Address link is not dispalyed.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1524 Issue ." + e.getMessage());
					Exception(" HBC - 1524 Issue ." + e.getMessage());
				}
				
				ChildCreation(" HBC - 1519 Reg Checkout - Payment Information Section > Verify that edited field details in the Billing Address overlay should get updated in Billing Information section on tapping the 'Ok' button" );
				if(HBC1519==true)
				{
					try
					{
						String sfn1 = HBCBasicfeature.getExcelVal("HBC1477", sheet, 11);
						if(HBCBasicfeature.isElementPresent(shipandPaypagePayEditLink))
						{
							befAdd = shipandPaypagePayAddressDisp.getText();			
						}	
						if(HBCBasicfeature.isElementPresent(shipandPaypagePayEditLink))
						{
							Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
							jsclick(shipandPaypagePayEditLink);
							Thread.sleep(2000);
							wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
							Thread.sleep(1000);
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
							{
								Pass("The Create Payment Address Overlay is dispalyed.");
								if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
								{
									Pass( "The Email Field is displayed.");
									shipandPaypageShipShipCreateEditAddresssFName.clear();							
									shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn1);
								}
								else
								{
									Fail( "The Email Field is not displayed.");
								}
									
								boolean addOverlay = ldAddressVerification();
								if(addOverlay==true)
								{
									jsclick(shipandPaypageShipShipCreateEditAddressOkBtn);
									wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
									addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
									if(addVerEnable==true)
									{
										Pass( "The Address Verification Overlay is enabled.");
										if(HBCBasicfeature.isElementPresent(addressuseasEntered))
										{
											Pass( "The Use As Entered button is displayed.");
											jsclick(addressuseasEntered);
											wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
											wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
											wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
											Thread.sleep(1000);
											aftAdd = shipandPaypagePayAddressDisp.getText();
											log.add( "The address before modification is  : " + befAdd);
											log.add( "The address after modification is  : " + aftAdd);
											if((befAdd.isEmpty())||(aftAdd.isEmpty()))
											{
												Fail( "The user failed to get the before address and after address they are empty.",log);
											}
											else if(befAdd.equals(aftAdd))
											{
												Fail( " The newly modified address is not dispalyed.",log);
											}
											else
											{
												log.add( " There is update made to the address list.");
												log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
												log.add( " There is update made to the address list. The entered email id was : " + sfn1);
												if(aftAdd.contains(sfn1))
												{
													Pass( "The Added First Name is dispalyed.",log);
												}
												else
												{
													Fail( "The Added First Name is not dispalyed.",log);
												}									
											}							
										}
										else
										{
											Fail( "The Address Use As entere is not displayed.");
											if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
											{
												jsclick(addressVerificationEdit);
												wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
												Thread.sleep(1000);
												if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
												{
													Pass( "The Cancel Link in Create overlay is displayed.");
													jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
													Thread.sleep(1000);
													wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
													Thread.sleep(1000);
													if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
													{
														Pass( "The Address Edit Overlay is closed.");
													}
													else
													{
														Fail( "The Address Edit Overlay is not closed.");
													}
												}
												else
												{
													Fail( "The Cancel Link in Create overlay is not displayed.");
												}
											}
											else
											{
												Fail( "The Edit link in the Address Verification overlay is not displayed.");
											}
										}
									}
									else
									{
										Fail( "The Address Verification Overlay is not enabled.");
									}
								}
								else
								{
									Fail( "The Address Verification Overlay is not enabled.");
								}
							}
						}
					}
					catch(Exception e)
					{
						System.out.println(" HBC - 1519 Issue ." + e.getMessage());
						Exception(" HBC - 1519 Issue ." + e.getMessage());
					}					
				}
				else
				{
					Skip("HBC - 1524 New Billing Address not added successfully");
				}	
				
				ChildCreation("HBC - 1659 Verify that entered address should be updated in the shipping address and it should redirect to the checkout page on tapping the 'Use Address as entered' button");
				if(HBC1659A ==true && HBC1659B ==true)
				{
					Pass("Entered address updated in the shipping address and it should redirect to the checkout page on tapping the 'Use Address as entered' button");
				}
				else
				{
					Fail("Entered address not updated in the shipping address and it should redirect to the checkout page on tapping the 'Use Address as entered' button");
				}			
			}
			else
			{
				Skip("Billing & Shipping page is not displayed");
			}
		}	
		
		/* HBC - 1578 Register Checkout - Payment Information Section > Verify that "Pay by Credit Card", "Pay by PayPal" radio tab should be shown below the payment information section.*/
		public void PaymentSectionsOptions()
		{
			ChildCreation("HBC - 1578 Register Checkout - Payment Information Section > Verify that Pay by Credit Card, Pay by PayPal radio tab should be shown below the payment information section.");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCard))
					{
						HBCBasicfeature.scrolldown(paymentOptionsCreditCard, driver);
						Pass( "The Credit Card payment Checkbox is displayed .");
					}
					else
					{
						Fail( "The Credit Card payment Checkbox is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsPaypal))
					{
						Pass( "The Paypal payment Checkbox is displayed .");
					}
					else
					{
						Fail( "The Paypal payment Checkbox is not displayed .");
					}					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1578 Issue ." + e.getMessage());
					Exception(" HBC - 1578 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1561 Reg Checkout - Payment Information Section > Verify that "Payment" column, "Shipping & taxes" column should be shown under Payment Information Section*/
		public void PaymentShippingTaxesOptions()
		{
			ChildCreation("HBC - 1561 Reg Checkout - Payment Information Section > Verify that 'Payment' column, 'Shipping & taxes' column should be shown under Payment Information Section");
			if(shippingPage==true)
			{
				try
				{				
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCard))
					{
						HBCBasicfeature.scrolldown(paymentOptionsCreditCard, driver);
						Pass( "The Credit Card payment Checkbox is displayed .");
					}
					else
					{
						Fail( "The Credit Card payment Checkbox is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsPaypal))
					{
						Pass( "The Paypal payment Checkbox is displayed .");
					}
					else
					{
						Fail( "The Paypal payment Checkbox is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsGiftCard))
					{
						Pass( "The Apply Gift card link is displayed .");
					}
					else
					{
						Fail( "The Apply Gift card link is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField1))
					{
						Pass( "The Reward card field-1 is displayed .");
					}
					else
					{
						Fail( "The Reward card field-1 is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
					{
						Pass( "The Reward card field-2 is displayed .");
					}
					else
					{
						Fail( "The Reward card field-2 is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
					{
						Pass( "The Add promo code field is displayed .");
					}
					else
					{
						Fail( "The Add promo code field is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
					{
						Pass( "The Shipping Method dropdown is displayed .");
					}
					else
					{
						Fail( "The Shipping Method dropdown is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsEstimatedSubtotal))
					{
						Pass( "The Estimated sub total is displayed .");
					}
					else
					{
						Fail( "The Estimated sub total is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotal))
					{
						Pass( "The Order total is displayed .");
					}
					else
					{
						Fail( "The Order total is not displayed .");
					}					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1561 Issue ." + e.getMessage());
					Exception(" HBC - 1561 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1562 Reg Checkout - Payment Information Section > Verify that "HBC rewards card number:" field, "Promo Codes" field should be shown under Payment Information Section*/
		public void PaymentHBCrewards()
		{
			ChildCreation("HBC - 1562 Reg Checkout - Payment Information Section > Verify that 'HBC rewards card number:' field, 'Promo Codes' field should be shown under Payment Information Section");
			if(shippingPage==true)
			{
				try
				{
					String expected = HBCBasicfeature.getExcelVal("HBC1562", sheet, 1);					
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardFieldLabel))
					{
						String actual = paymentOptionsRewardCardFieldLabel.getText();
						log.add("The actual HBC rewards label is: "+actual);
						log.add("The expected HBC rewards label is: "+expected);
						if(actual.equalsIgnoreCase(expected))
						{
							Pass( "The 'HBC rewards card number' is displayed .",log);
						}
						else
						{
							Fail( "The 'HBC rewards card number' is not displayed .");
						}
					}
					else
					{
						Fail( "The 'HBC rewards card number' label is not displayed .");
					}						
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField1))
					{
						Pass( "The Reward card number field-1 is displayed .");
					}
					else
					{
						Fail( "The Reward card number field-1 is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
					{
						Pass( "The Reward card number field-2 is displayed .");
					}
					else
					{
						Fail( "The Reward card number field-2 is not displayed .");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
					{
						Pass( "The Add promo code field is displayed .");
					}
					else
					{
						Fail( "The Add promo code field is not displayed .");
					}
					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1562 Issue ." + e.getMessage());
					Exception(" HBC - 1562 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1563 Reg Checkout - Payment Information Section > Verify that user should be able to enter card number in the "HBC rewards card number:" field and it should accept maximum of 9 digits*/
		public void PaymentHBCrewardValidation() throws Exception
		{
			ChildCreation("HBC - 1563 Reg Checkout - Payment Information Section > Verify that user should be able to enter card number in the 'HBC rewards card number:' field and it should accept maximum of 9 digits");
			if(shippingPage==true)
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1563", sheet, 1);
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
					{
						Pass( "The Reward card number field-2 is displayed .");
						paymentOptionsRewardCardField2.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						paymentOptionsRewardCardField2.sendKeys(cellVal);
						if(paymentOptionsRewardCardField2.getAttribute("value").isEmpty())
						{
							Fail( "The 'HBC rewards card number:' field is empty.");
						}
						else if(paymentOptionsRewardCardField2.getAttribute("value").length()==9)
						{
							Pass( "The  'HBC rewards card number:' field was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
						}
						paymentOptionsRewardCardField2.clear();	
					}
					else
					{
						Fail("HBC Reward card field is not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1563 Issue ." + e.getMessage());
					Exception(" HBC - 1563 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
	
		/* HBC - 1564 Reg Checkout - Payment Information Section >  Verify that corresponding alert message should be shown, when the user entered less than 9 digits in the "HBC rewards card number" field*/
		/* HBC - 1565 Reg Checkout - Payment Information Section >  Verify that corresponding alert message should be shown, when the user entered alphabets or special characters  in the "HBC rewards card number" field*/

		public void PaymentHBCrewardlenValidation() throws Exception
		{
			ChildCreation("HBC - 1564 Reg Checkout - Payment Information Section >  Verify that corresponding alert message should be shown, when the user entered less than 9 digits in the 'HBC rewards card number' field");
			if(shippingPage==true)
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1564", sheet, 1).split("\n");
				String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1564", sheet, 2);
				boolean HBC1565 = false;
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
					{
						Pass( "The Reward card number field-2 is displayed .");	
						for(int i=0;i<cellVal.length;i++)
						{
							paymentOptionsRewardCardField2.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							paymentOptionsRewardCardField2.sendKeys(cellVal[i]);
							if(paymentOptionsRewardCardField2.getAttribute("value").length()<9)
							{
								paymentOptionsPromoCode.click();
								wait.until(ExpectedConditions.attributeContains(paymentOptionsRewardCardFieldAlert, "style", "block"));
								Thread.sleep(100);
								if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardFieldAlert))
								{
									String alert = paymentOptionsRewardCardFieldAlert.getText();
									log.add("The displayed alert is: "+alert);
									log.add("The Actual alert is: "+ ExpctdAlert);
									if(alert.equalsIgnoreCase(ExpctdAlert))
									{
										Pass("corresponding alert message shown, when the user entered less than 9 digits in the 'HBC rewards card number' field",log);
										HBC1565 = true;
									}
									else
									{
										Fail("corresponding alert message not shown, when the user entered less than 9 digits in the 'HBC rewards card number' field",log);
									}
									paymentOptionsRewardCardField2.clear();	
								}
								else
								{
									Fail("Alert is not displayed",log);
								}						
							}
							else
							{
								Fail( "The Reward card Number was enterable but it is not within the specified character limit.",log);
							}							
						}
					}
					else
					{
						Fail("HBC Reward card field is not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1564 Issue ." + e.getMessage());
					Exception(" HBC - 1564 Issue ." + e.getMessage());
				}
				
				ChildCreation("HBC - 1565 Reg Checkout - Payment Information Section >  Verify that corresponding alert message should be shown, when the user entered  alphabets or special characters in the 'HBC rewards card number' field");
				if(HBC1565 == true)
				{
					Pass("corresponding alert message shown, when the user entered  alphabets or special characters in the 'HBC rewards card number' field",log);
				}
				else
				{
					Fail("corresponding alert message not shown, when the user entered alphabets or special characters in the 'HBC rewards card number' field",log);
				}				
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		
		/* HBC - 1566 Reg Checkout - Payment Information Section >   Verify that "Apply" text link should be shown near the "Promo Codes" text box*/
		public void PaymentHBCrewardsApply() throws Exception
		{
			ChildCreation("HBC - 1566 Reg Checkout - Payment Information Section >   Verify that 'Apply' text link should be shown near the 'Promo Codes' text box");
			if(shippingPage==true)
			{
				try
				{
					String expctd = HBCBasicfeature.getExcelVal("HBC1566", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
					{
						Pass( "The Add promo code field is displayed .");
						if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApply))
						{
							String actual = paymentOptionsPromoCodeApply.getText();
							log.add("Actual text link displayed is: "+actual);
							log.add("Expected text link displayed is: "+expctd);
							if(actual.equalsIgnoreCase(expctd))
							{
								Pass(" 'Apply' text link shown near the 'Promo Codes' text box",log);
							}
							else
							{
								Fail("'Apply' text link not shown near the 'Promo Codes' text box",log);
							}
						}
						else
						{
							Fail("Promo code Apply link mnot displayed");
						}
					}
					else
					{
						Fail( "The Add promo code field is not displayed .");
					}
				}		
				catch(Exception e)
				{
					System.out.println(" HBC - 1566 Issue ." + e.getMessage());
					Exception(" HBC - 1566 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1567 Reg Checkout - Payment Information Section >  Verify that alert should be shown on tapping the "Apply" text link near the "Promo Codes" text box without entering any characters*/
		public void PaymentHBCrewardsEmptyApply() throws Exception
		{
			ChildCreation("HBC - 1567 Reg Checkout - Payment Information Section >   Verify that alert should be shown on tapping the Apply text link near the 'Promo Codes' text box without entering any characters");
			if(shippingPage==true)
			{
				try
				{					
					String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1567", sheet, 2);
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
					{
						Pass( "The Add promo code field is displayed .");
						paymentOptionsPromoCode.clear();
						if(paymentOptionsPromoCode.getAttribute("value").isEmpty())
						{
							log.add("PromoCode box is empty");
							if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApply))
							{
								jsclick(paymentOptionsPromoCodeApply);
								wait.until(ExpectedConditions.attributeContains(paymentOptionsPromoCodeApplyAlert, "style", "block"));
								if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApplyAlert))
								{	
									String alert = paymentOptionsPromoCodeApplyAlert.getText();
									log.add("The displayed alert is: "+alert);
									log.add("The Actual alert is: "+ ExpctdAlert);
									if(alert.equalsIgnoreCase(ExpctdAlert))
									{
										Pass("corresponding alert message shown, when tapping the Apply text link near the 'Promo Codes' text box without entering any characters",log);
									}
									else
									{
										Fail("corresponding alert message not shown, when tapping the Apply text link near the 'Promo Codes' text box without entering any characters",log);
									}
								}
								else
								{
									Fail("Alert not displayed",log);
								}								
							}
							else
							{
								Fail("Promo code Apply link mnot displayed");
							}
						}
						else
						{
							Fail("The Add promo code field is not Empty");
						}
					}
					else
					{
						Fail( "The Add promo code field is not displayed .");
					}					
				}		
				catch(Exception e)
				{
					System.out.println(" HBC - 1567 Issue ." + e.getMessage());
					Exception(" HBC - 1567 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1570 Reg Checkout - Payment Information Section >  Verify that clicking on "Apply" link after entering the invalid numbers alert should be shown as like classic site*/
		public void PaymentHBCrewardInvalidPromocode() throws Exception
		{
			ChildCreation("HBC - 1570 Reg Checkout - Payment Information Section > Verify that clicking on 'Apply' link after entering the invalid numbers alert should be shown as like classic site");
			if(shippingPage==true)
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1570", sheet, 1);
				String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1570", sheet, 2);
				boolean HBC1571 = false;
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
					{
						Pass( "The Add Promo code field is displayed .");							
						paymentOptionsPromoCode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());						
						paymentOptionsPromoCode.sendKeys(cellVal);
						jsclick(paymentOptionsPromoCodeApply);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsPromoCodeApplyInvalidAlert, "style", "block"));
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApplyInvalidAlert))
						{
							String alert = paymentOptionsPromoCodeApplyInvalidAlert.getText();
							log.add("The displayed alert is: "+alert);
							log.add("The Actual alert is: "+ ExpctdAlert);
							if(alert.equalsIgnoreCase(ExpctdAlert))
							{
								Pass("corresponding alert message shown, when clicking on 'Apply' link after entering the invalid numbers",log);
								HBC1571 = true;
							}
							else
							{
								Fail("corresponding alert message not shown, when clicking on 'Apply' link after entering the invalid numbers' field",log);
							}
							paymentOptionsPromoCode.clear();	
						}
						else
						{
							Fail("Alert is not displayed",log);
						}							
					}
					else
					{
						Fail("HBC Reward Promo code field is not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1570 Issue ." + e.getMessage());
					Exception(" HBC - 1570 Issue ." + e.getMessage());
				}
				
				ChildCreation("HBC - 1571 Reg Checkout - Payment Information Section > Verify that alert should be shown on entering less than the acceptable characters in the 'Promo Codes' field");
				if(HBC1571 == true)
				{
					Pass("corresponding alert message shown, when the user entered less than the acceptable characters in the 'Promo Codes' field");
				}
				else
				{
					Fail("corresponding alert message not shown, when the user entered less than the acceptable characters in the 'Promo Codes' field");
				}				
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		
		/* HBC - 1569 Register Checkout - Billing Address Overlay/Page >  Verify that user should be able to enter promo code in "Promo Codes" field and it should accept maximum of 20 digits*/
		public void PaymentHBCrewardLenPromocode()
		{	
			ChildCreation("HBC - 1569 Reg Checkout - Payment Information Section >  Verify that user should be able to enter promo code in 'Promo Codes' field and it should accept maximum of 20 digits");
			if(shippingPage==true)
			{
				try
				{
					String[] cellVal = HBCBasicfeature.getExcelVal("HBC1569", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
					{
						Pass( "The Add Promo code field is displayed.");					
						for(int i = 0; i<cellVal.length;i++)
						{
							paymentOptionsPromoCode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							paymentOptionsPromoCode.sendKeys(cellVal[i]);
							if(paymentOptionsPromoCode.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(paymentOptionsPromoCode.getAttribute("value").length()<=20)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							paymentOptionsPromoCode.clear();
						}
					}
					else
					{
						Fail("HBC Reward Promo code field is not displayed");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1551 Issue ." + e.getMessage());
					Exception(" HBC - 1551 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
			}
		}
		
		/* HBC - 1572 Register Checkout - Payment Information Section >  Verify that "Shipping & Taxes" column should display "Order sub total", "Shipping amount", "PST", "GST/HST", "HBC Reward Points Earned", "Order total"*/
		public void PaymentShippingandTaxes() throws Exception
		{
			ChildCreation("HBC - 1572 Reg Checkout - Payment Information Section >  Verify that 'Shipping & Taxes' column should display 'Order sub total', 'Shipping amount', 'PST', 'GST/HST', 'HBC Reward Points Earned', 'Order total'");
			if(shippingPage==true)
			{
				try
				{			
					if(HBCBasicfeature.isElementPresent(paymentOptionsEstimatedSubtotal))
					{
						Pass("Order Sub Total option displayed");
					}
					else
					{
						Fail("Order Sub Total option not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsShippingAmountlabel))
					{
						Pass("Order Shipping Amount option displayed");
					}
					else
					{
						Fail("Order Shipping Amount option not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsPSTLabel))
					{
						Pass("PST label option displayed");
					}
					else
					{
						Fail("PST label option not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsGSTHSTLabel))
					{
						Pass("GST/HST label option displayed");
					}
					else
					{
						Fail("GST/HST label option not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotalLabel))
					{
						Pass("Order Total option displayed");
					}
					else
					{
						Fail("Order Total option not displayed");
					}					
				}						
				catch(Exception e)
				{
					System.out.println(" HBC - 1572 Issue ." + e.getMessage());
					Exception(" HBC - 1572 Issue ." + e.getMessage());
				}			
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1573 Register Checkout - Payment Information Section >  Verify that "Order total" and its amount should be highlighted in black"*/
		public void PaymentOrderTotalHighlight() throws Exception
		{
			ChildCreation("HBC - 1573 Reg Checkout - Payment Information Section >  Verify that 'Order total' and its amount should be highlighted in black");
			if(shippingPage==true)
			{
				try
				{	
					String expctdColor = HBCBasicfeature.getExcelVal("HBC1573", sheet, 2);
					if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotalLabel))
					{
						String actual = paymentOptionsOrderTotalLabel.getCssValue("color");
						String actualColor = HBCBasicfeature.colorfinder(actual);
						log.add("The actual color is: "+actualColor);
						log.add("The expected color is: "+expctdColor);
						if(actualColor.equalsIgnoreCase(expctdColor))
						{
							Pass("'Order total'  highlighted in black",log);
						}
						else
						{
							Fail("'Order total' not highlighted in black",log);
						}
					}
					else
					{
						Fail("Order Total label is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotal))
					{
						String actual = paymentOptionsOrderTotal.getCssValue("color");
						String actualColor = HBCBasicfeature.colorfinder(actual);
						log.add("The actual color is: "+actualColor);
						log.add("The expected color is: "+expctdColor);
						if(actualColor.equalsIgnoreCase(expctdColor))
						{
							Pass("'Order total' amount highlighted in black",log);
						}
						else
						{
							Fail("'Order total'amount not highlighted in black",log);
						}
					}
					else
					{
						Fail("Order Total amount is not displayed");
					}
					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1573 Issue ." + e.getMessage());
					Exception(" HBC - 1573 Issue ." + e.getMessage());
				}			
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1574 Register Checkout - Payment Information Section >  Verify that "Shipping method" dropdown field should be shown under "Shipping & Taxes" column"*/
		public void PaymentShippingMethodDropDown() throws Exception
		{
			ChildCreation("HBC - 1574 Reg Checkout - Payment Information Section > Verify that 'Shipping method' dropdown field should be shown under 'Shipping & Taxes' column");
			if(shippingPage==true)
			{
				try
				{	
					if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
					{
						Pass( "The Shipping Method dropdown is displayed .");						
					}
					else
					{
						Fail( "The Shipping Method dropdown is not displayed .");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1574 Issue ." + e.getMessage());
					Exception(" HBC - 1574 Issue ." + e.getMessage());
				}			
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1575 Register Checkout - Payment Information Section >  Verify that "Standard Delivery" option should be selected in default in "Shipping method" dropdown field"*/
		public void PaymentShippingMethodDropDownDefault() throws Exception
		{
			ChildCreation("HBC - 1575 Reg Checkout - Payment Information Section > Verify that 'Standard Delivery' option should be selected in default in 'Shipping method' dropdown field");
			if(shippingPage==true)
			{
				try
				{	
					String expctd = HBCBasicfeature.getExcelVal("HBC1575", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
					{
						Pass( "The Shipping Method dropdown is displayed .");							
						Select sel = new Select(paymentOptionsShippingMethoddropdown);
						String actual = sel.getFirstSelectedOption().getText().trim();
						log.add("The selected by default option is: "+actual);
						log.add("The expected option  is: "+expctd);
						Thread.sleep(500);
						if(actual.equalsIgnoreCase(expctd))
						{
							Pass("'Standard Delivery' option selected in default in 'Shipping method' dropdown field",log);
						}
						else
						{
							Fail("'Standard Delivery' option not selected in default in 'Shipping method' dropdown field",log);
						}						
					}
					else
					{
						Fail( "The Shipping Method dropdown is not displayed .");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1575 Issue ." + e.getMessage());
					Exception(" HBC - 1575 Issue ." + e.getMessage());
				}			
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		
		/* HBC - 1576 Register Checkout - Payment Information Section >   Verify that "Shipping method" dropdown field should display maximum of 3 options"*/
		public void PaymentShippingMethodDropDownOptions() throws Exception
		{
			ChildCreation("HBC - 1576 Reg Checkout - Payment Information Section >  Verify that 'Shipping method' dropdown field should display maximum of 3 options");
			if(shippingPage==true)
			{
				try
				{	
					if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
					{
						Select sel = new Select(paymentOptionsShippingMethoddropdown);
						int size = sel.getOptions().size();
						log.add("Displayed option size is: "+size);
						if(size==3)
						{
							Pass("Dropdown field has maximum 3 options", log);
							for(int i=0;i<size;i++)
							{
								sel.getOptions().get(i).click();
								wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
								String option = sel.getFirstSelectedOption().getText();
								Pass("The 'Shipping method' dropdown field option is: "+option);
							}
							sel.getOptions().get(0).click();
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						}
						else
						{
							Fail("'Shipping method' dropdown field doesn't has maximum of 3 options",log);
						}
					}
					else
					{
						Fail("DropDown field not displayed");
					}					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1576 Issue ." + e.getMessage());
					Exception(" HBC - 1576 Issue ." + e.getMessage());
				}			
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1577 Register Checkout - Payment Information Section >  Verify that prices in the "Shipping & taxes" column is also changed on changing the "Shipping method" dropdown option"*/
		public void PaymentShippingMethodDropDownOptionChange() throws Exception
		{
			ChildCreation("HBC - 1577 Reg Checkout - Payment Information Section > Verify that prices in the 'Shipping & taxes' column is also changed on changing the 'Shipping method' dropdown option");
			if(shippingPage==true)
			{
				try
				{	
					if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
					{
						Select sel = new Select(paymentOptionsShippingMethoddropdown);
						int size = sel.getOptions().size();
						log.add("Displayed option size is: "+size);
						String currentOption = sel.getFirstSelectedOption().getText().trim();
						String currentVal = paymentOptionsShippingAmountPrice.getText();
						Thread.sleep(200);
						if(size>1)
						{
							sel.getOptions().get(1).click();
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
							Thread.sleep(1000);
							String option = sel.getFirstSelectedOption().getText().trim();
							String Val = paymentOptionsShippingAmountPrice.getText();	
							Thread.sleep(200);
							if(currentOption.equalsIgnoreCase(option)&&currentVal.equalsIgnoreCase(Val))
							{
								Fail("'Shipping & taxes' column is not changed on changing the 'Shipping method' dropdown option",log);
							}
							else
							{
								Pass("'Shipping & taxes' column is also changed on changing the 'Shipping method' dropdown option",log);
							}							
							sel.getOptions().get(0).click();
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						}
						else
						{
							Fail("'Shipping method' dropdown field doesn't has more than 1 options",log);
						}
					}
					else
					{
						Fail("DropDown field not displayed");
					}					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1577 Issue ." + e.getMessage());
					Exception(" HBC - 1577 Issue ." + e.getMessage());
				}			
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1607  Payment Information Section > Verify that "Pay by gift card" checkbox field should be unselected by default.*/
		public void PaymentSectionsPayByGiftCardUnselected()
		{
			ChildCreation(" HBC - 1607  Payment Information Section > Verify that 'Pay by gift card' checkbox field should be unselected by default.");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsGiftCard))
					{
						Pass( "The Pay By Gift card checkbox field is displayed .");
						if(HBCBasicfeature.isElementPresent(paymentOptionsGiftCardChkkBox))
						{
							HBCBasicfeature.scrollup(paymentOptionsGiftCardChkkBox, driver);
							boolean flag = paymentOptionsGiftCardChkkBox.isSelected();
							if(flag==false)
							{
								Pass("'Pay by gift card' checkbox field unselected by default");
							}
							else
							{
								Fail("'Pay by gift card' checkbox field selected by default");
							}
						}
						else
						{
							Fail("'Pay by gift card' checkbox field not displayed");
						}						
					}
					else
					{
						Fail( "The  Pay By Gift card field is not displayed .");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1607 Issue ." + e.getMessage());
					Exception(" HBC - 1607 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1581  Payment Information Section >Verify that "What is PayPal" text link should be shown below the paypal logo near "Pay by PayPal" radio tab in "Payment" column*/
		public void PaymentSectionsWhatIsPayPal()
		{ 
			ChildCreation(" HBC - 1581  Payment Information Section > Verify that 'What is PayPal' text link should be shown below the paypal logo near 'Pay by PayPal' radio tab in 'Payment' column");
			if(shippingPage==true)
			{
				try
				{
					String expected = HBCBasicfeature.getExcelVal("HBC1581", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsPaypalSelected))
					{
						Pass( "The Pay By Paypal field is displayed .");
						if(HBCBasicfeature.isElementPresent(paymentOptionsWhatIspaypal))
						{
							HBCBasicfeature.scrollup(paymentOptionsWhatIspaypal, driver);
							String actual = paymentOptionsWhatIspaypal.getText();
							log.add("The Actual text link displayed is: "+actual);
							log.add("The Expected text link displayed is: "+expected);
							if(expected.equalsIgnoreCase(actual))
							{
								Pass("'What is paypal' text link  shown in 'Payment' column",log);
							}
							else
							{
								Fail("'What is paypal' text link not shown in 'Payment' column",log);
							}
						}
						else
						{
							Fail("'What is paypal' text link not displayed");
						}						
					}
					else
					{
						Fail( "The Pay By Paypal field is not displayed .");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1581 Issue ." + e.getMessage());
					Exception(" HBC - 1581 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
				
		/* HBC - 1582  Payment Information Section >  Verify that in "Payment" column, on tapping the "What is PayPal" text link should redirect or paypal overlay should be shown*/
		public void PaymentSectionsWhatIsPayPalClick()
		{ 
			ChildCreation(" HBC - 1582  Payment Information Section >  Verify that in 'Payment' column, on tapping the 'What is PayPal' text link should redirect or paypal overlay should be shown");
			if(shippingPage==true)
			{
				try
				{
					String payPalUrl = HBCBasicfeature.getExcelVal("HBC1582", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsPaypalSelected))
					{
						Pass( "The Pay By Paypal field is displayed .");
						if(HBCBasicfeature.isElementPresent(paymentOptionsWhatIspaypal))
						{
							HBCBasicfeature.scrollup(paymentOptionsWhatIspaypal, driver);
							String handle = driver.getWindowHandle();
							jsclick(paymentOptionsWhatIspaypal);
							Thread.sleep(2000);
							for(String newWin: driver.getWindowHandles())
							{
								if(!newWin.equals(handle))
								{
									driver.switchTo().window(newWin);
									Thread.sleep(200);
									String url = driver.getCurrentUrl();
									if(url.contains(payPalUrl))
									{
										Pass("On selecting 'What is PayPal' text link paypal overlay shown");
									}
									else
									{
										Fail("On selecting 'What is PayPal' text link paypal overlay shown");
									}
									driver.close();		
									driver.switchTo().window(handle);
								}						
							}									
						}
						else
						{
							Fail("'What is paypal' text link not displayed");
						}						
					}
					else
					{
						Fail( "The Pay By Paypal field is not displayed .");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1582 Issue ." + e.getMessage());
					Exception(" HBC - 1582 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1580  Payment Information Section >  Verify that "Pay by credit card", "Pay by gift card" checkboxes should be unselected & it should be collapsed in "Payment" column on tapping the "Pay by PayPal" radio tab*/
		public void PaymentSectionPaypalSelectedCreditCardUnselected()
		{ 
			ChildCreation(" HBC - 1580  Payment Information Section >  Verify that 'Pay by credit card', 'Pay by gift card' checkboxes should be unselected & it should be collapsed in 'Payment' column on tapping the 'Pay by PayPal' radio tab");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardChkkBox))
					{
						boolean creditChkbox = paymentOptionsCreditCardChkkBox.isSelected();						
						if(creditChkbox==false)
						{
							Pass("Pay by credit card' checkbox unselected on tapping the 'Pay by PayPal' radio tab");
						}
						else
						{
							Fail("Pay by credit card' checkbox selected on tapping the 'Pay by PayPal' radio tab");
						}						
					}
					else
					{
						Fail("Pay by Creditcard checkbox is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(paymentOptionsGiftCardChkkBox))
					{
						boolean giftcardChkbox = paymentOptionsGiftCardChkkBox.isSelected();						
						if(giftcardChkbox==false)
						{
							Pass("Pay by gift card' checkbox unselected on tapping the 'Pay by PayPal' radio tab");
						}
						else
						{
							Fail("Pay by gift card' checkbox selected on tapping the 'Pay by PayPal' radio tab");
						}						
					}
					else
					{
						Fail("Pay by gift card checkbox is not displayed");
					}				
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1580 Issue ." + e.getMessage());
					Exception(" HBC - 1580 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1579 Register Checkout - Payment Information Section > Verify that "Pay by Card" radio tab, "Pay by credit card" checkbox should be selected in default in "Payment" column*/
		public void PaymentSectionsPaybyCardPaybycreditcardSelected()
		{
			ChildCreation(" HBC - 1579  Register Checkout - Payment Information Section >  Verify that 'Pay by Card' radio tab, 'Pay by credit card' checkbox should be selected in default in 'Payment' column");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionspaybycardChkkBox)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardChkkBox))
					{
						Pass( "The'Pay by Card' radio tab & & 'Pay by credit card' checkbox is displayed .");
						HBCBasicfeature.scrollup(paymentOptionspaybycardChkkBox, driver);
						jsclick(paymentOptionspaybycardChkkBox);
						wait.until(ExpectedConditions.visibilityOf(paymentOptionspaybycardChkkBoxSelected));
						wait.until(ExpectedConditions.visibilityOf(paymentOptionsCreditCardSelected));
						if(HBCBasicfeature.isElementPresent(paymentOptionspaybycardChkkBoxSelected)&&HBCBasicfeature.isElementPresent(paymentOptionspaybycardChkkBoxSelected))
						{
							Pass("'Pay by Card' radio tab & 'Pay by credit card' checkbox  selected in 'Payment' column");
						}
						else
						{
							Fail("'Pay by Card' radio tab & 'Pay by credit card' checkbox not selected in 'Payment' column");							
						}						
					}
					else
					{
						Fail("'Pay by card radio' tab & & 'Pay by credit card' checkbox is not displayed");
					}					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1579 Issue ." + e.getMessage());
					Exception(" HBC - 1579 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1583 Register Checkout - Payment Information Section > Verify that "Pay by card" section should display "Card type" dropdown field in the "Payment" column.*/
		public void PaymentSectionsCreditCardFieldDropDown()
		{
			ChildCreation(" HBC - 1583  Register Checkout - Payment Information Section > Verify that Pay by card section should display Card type dropdown field in the Payment column.");
			if(shippingPage==true)
			{
				try
				{					
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
					{
						Pass( "The Credit Card option was selected.");
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecEnable, "style", "block"));
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
						{
							Pass( "The Credit Card option was selected.");
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
							{
								Pass( "The Credit Card Drop Down is displayed.");
							}
							else
							{
								Fail( "The Credit Card Drop Down is not displayed.");
							}
						}
						else
						{
							Fail( "The Credit Card option was not selected.");
						}
					}
					else
					{
						Fail("Pay by card option not selected");
					}						
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1583 Issue ." + e.getMessage());
					Exception(" HBC - 1583 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1584 Register Checkout - Payment Information Section > Verify that "Card type" dropdown field should not be marked with star(*) in the "Payment" column*/
		public void PaymentSectionsCreditCardTypeSymbol()
		{
			ChildCreation(" HBC - 1584  Register Checkout - Payment Information Section > Verify that 'Card type' dropdown field should not be marked with star(*) in the 'Payment' column");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDownLabel))
					{
						String card = paymentOptionsCreditCardCardSelDropDownLabel.getText();
						if(card.contains("*"))
						{
							Fail("'Card type' dropdown field marked with star(*) in the 'Payment' column");
						}
						else
						{
							Pass("'Card type' dropdown field not marked with star(*) in the 'Payment' column");
						}						
					}
					else
					{
						Fail("'Card type' dropdown field label not displayed");
					}				
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1584 Issue ." + e.getMessage());
					Exception(" HBC - 1584 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1588 Register Checkout - Payment Information Section > Verify that "Card type" dropdown field should be selected in "Select" option by default in "Payment" column*/
		public void PaymentSectionsCreditCardDropdownDefault()
		{
			ChildCreation(" HBC - 1588  Register Checkout - Payment Information Section > Verify that 'Card type' dropdown field should be selected in 'Select' option by default in 'Payment' column");
			if(shippingPage==true)
			{
				try
				{
					String expctd = HBCBasicfeature.getExcelVal("HBC1486", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
					{
						Pass("Credit type drop down is displayed");
						Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
						String actual = sel.getFirstSelectedOption().getText();
						log.add("The selected by default option is: "+actual);
						log.add("The expected option  is: "+expctd);
						Thread.sleep(200);
						if(actual.equalsIgnoreCase(expctd))
						{
							Pass(" 'Card type' dropdown field selected in 'Select' option by default in 'Payment' column",log);
						}
						else
						{
							Fail("'Card type' dropdown field not selected in 'Select' option by default in 'Payment' column",log);
						}
					}
					else
					{
						Fail("'Card type' dropdown field not displayed");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1588 Issue ." + e.getMessage());
					Exception(" HBC - 1588 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1585 Register Checkout - Payment Information Section > Verify that user should be able to choose the option in the "Card type" dropdown field in the "Payment" column*/
		public void PaymentSectionsCreditCardDropdownSelectOptions()
		{
			ChildCreation(" HBC - 1585  Register Checkout - Payment Information Section > Verify that user should be able to choose the option in the 'Card type' dropdown field in the 'Payment' column");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
					{
						Pass("Credit type drop down is displayed");
						Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
						String actual = sel.getFirstSelectedOption().getText();
						log.add("The selected by default option is: "+actual);
						int size = sel.getOptions().size();
						if(size>1)
						{
							sel.getOptions().get(1).click();
							Thread.sleep(1000);
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
							Thread.sleep(1000);
							String current = sel.getFirstSelectedOption().getText();
							log.add("The selected by default option is: "+actual);
							log.add("The Current selected option  is: "+current);
							if(actual.equalsIgnoreCase(current))
							{
								Fail("user not able to choose the option in the 'Card type' dropdown field in the 'Payment' column",log);
							}
							else
							{
								Pass("user able to choose the option in the 'Card type' dropdown field in the 'Payment' column",log);
							}							
						}
						else
						{
							Fail("Drop down size is not more than 1");
						}
					}
					else
					{
						Fail("Card type drop down is not displayed");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1585 Issue ." + e.getMessage());
					Exception(" HBC - 1585 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1586 Register Checkout - Payment Information Section > Verify that credit card images should be shown in the "Pay by credit card" section under "Payment" column*/
		public void PaymentSectionsCreditCardImages()
		{
			ChildCreation(" HBC - 1586  Register Checkout - Payment Information Section > Verify that credit card images should be shown in the 'Pay by credit card' section under 'Payment' column");
			if(shippingPage==true)
			{
				try
				{
					int ctr = 0;
					if(HBCBasicfeature.isListElementPresent(paymentOptionsCreditCardImages))
					{
						Pass("Credit Card Images are displayed");
						int size = paymentOptionsCreditCardImages.size();
						for(int i=1;i<=size;i++)
						{
							Thread.sleep(200);
							boolean image = driver.findElement(By.xpath("//*[@id='CreditCardList']//li["+i+"]")).isDisplayed();
							if(image==true)
							{
								ctr++;
							}
						}
						if(size==ctr)
						{
							Pass("Credit card images shown in the 'Pay by credit card' section",log);
						}
						else
						{
							Fail("Credit card images not shown in the 'Pay by credit card' section",log);
						}
					}
					else
					{
						Fail("Credit Card Images are not displayed");
					}					
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1586 Issue ." + e.getMessage());
					Exception(" HBC - 1586 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1587 Register Checkout - Payment Information Section > Verify that "Amount to pay by credit card: $XXX.XX" text should be shown in the "Pay by credit card" section under "Payment" column*/
		public void PaymentSectionsCreditCardAmountToPay()
		{
			ChildCreation(" HBC - 1587  Register Checkout - Payment Information Section > Verify that 'Amount to pay by credit card: $XXX.XX' text should be shown in the 'Pay by credit card' section under 'Payment' column");
			if(shippingPage==true)
			{
				try
				{
					String expctd = HBCBasicfeature.getExcelVal("HBC1587", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardAmountToPaylabel)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardAmountToPayPrice))
					{
						String actual = paymentOptionsCreditCardAmountToPaylabel.getText();
						Thread.sleep(200);
						String actualval = paymentOptionsCreditCardAmountToPayPrice.getText();
						Thread.sleep(200);
						log.add("The selected by default option is: "+actual);
						log.add("The Current selected option  is: "+expctd);
						log.add("The Price amount displayed is: "+actualval);
						Thread.sleep(200);
						if(actual.equalsIgnoreCase(expctd)&&actualval.contains("$")&&actualval.matches(".*\\d+.*"))
						{
							Pass("'Amount to pay by credit card:  $XXX.XX' text shown in the 'Pay by credit card' section",log);
						}
						else
						{
							Fail("'Amount to pay by credit card: $XXX.XX 'text not shown in the 'Pay by credit card' section",log);
						}					
					}
					else
					{
						Fail("'Amount to pay by credit card: ' label and price not displayed");
					}				
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1587 Issue ." + e.getMessage());
					Exception(" HBC - 1587 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1589 Register Checkout - Payment Information Section > Verify that "Card number" textbox field should be shown on selecting "HBC Credit Card" option in the "Card type" dropdown field*/
		public void PaymentSectionsCreditCardNumberField()
		{
			ChildCreation(" HBC - 1589  Register Checkout - Payment Information Section > Verify that 'Card number' textbox field should be shown on selecting 'HBC Credit Card' option in the 'Card type' dropdown field");
			if(shippingPage==true)
			{
				try
				{
					String expctd = HBCBasicfeature.getExcelVal("HBC1589", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
					{
						Pass("Credit type drop down is displayed");
						Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
						String actual = sel.getFirstSelectedOption().getText();
						Thread.sleep(200);
						log.add("The selected by default option is: "+actual);
						sel.selectByValue("HBC");
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						String current = sel.getFirstSelectedOption().getText();
						Thread.sleep(200);
						log.add("The Current selected option  is: "+current);
						if(current.equalsIgnoreCase(expctd))
						{
							Pass(" 'HBC Credit Card' option selected");
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
							{
								Pass("'Card number' textbox field shown on selecting 'HBC Credit Card' option ",log);
							}
							else
							{
								Fail("'Card number' textbox field not shown on selecting 'HBC Credit Card' option ",log);
							}						
						}
						else
						{
							Fail("user not selected 'HBC Credit Card' option in the 'Card type' dropdown field",log);
						}
					}
					else
					{
						Fail("Card type drop down is not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1589 Issue ." + e.getMessage());
					Exception(" HBC - 1589 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1590 Register Checkout - Payment Information Section > Verify that "Card number" textbox fields should accept maximum of 16 digits*/
		public void PaymentSectionsCreditCardNumberFieldMaximum()
		{
			ChildCreation(" HBC - 1590  Register Checkout - Payment Information Section > Verify that 'Card number' textbox fields should accept maximum of 16 digits");
			if(shippingPage==true)
			{
				try
				{
					String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1590", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
					{
						Pass("'Card number' textbox field shown ");
						for(int i = 0; i<cellVal.length;i++)
						{
							paymentOptionsCreditCardNumberField.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							paymentOptionsCreditCardNumberField.sendKeys(cellVal[i]);
							Thread.sleep(500);
							if(paymentOptionsCreditCardNumberField.getAttribute("value").isEmpty())
							{
								Fail( "The 'Card number' textbox field is empty.");
							}
							else if(paymentOptionsCreditCardNumberField.getAttribute("value").length()<=16)
							{
								Pass( "The 'Card number' textbox field was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The 'Card number' textbox field was enterable but it is not within the specified character limit.",log);
							}
							paymentOptionsCreditCardNumberField.clear();
						}
					}
					else
					{
						Fail("'Card number' textbox field not shown ");
					}						
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1590 Issue ." + e.getMessage());
					Exception(" HBC - 1590 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1591 Register Checkout - Payment Information Section > Verify that alert should be shown on navigating to other fields without entering characters in the "Card number" fields*/
		public void PaymentSectionsCreditCardNumberEmptyFieldAlert()
		{
			ChildCreation(" HBC - 1591  Register Checkout - Payment Information Section > Verify that alert should be shown on navigating to other fields without entering characters in the 'Card number' fields");
			if(shippingPage==true)
			{
				try
				{			
					String expctd = HBCBasicfeature.getExcelVal("HB1591", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
					{
						Pass("'Card number' textbox field shown ");					
						if(paymentOptionsCreditCardNumberField.getAttribute("value").isEmpty())
						{
							log.add("'Card number' textbox field is empty");
							paymentOptionsRewardCardField2.click();
							Thread.sleep(200);
							wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardNumberFieldAlert, "style", "block"));
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberFieldAlert))
							{
								String actual = paymentOptionsCreditCardNumberFieldAlert.getText().trim();
								log.add("The actual alert displayed is: "+actual);
								log.add("The expected alert to be displayed is: "+expctd);
								Thread.sleep(200);
								if(actual.equalsIgnoreCase(expctd))
								{
									Pass("Alert shown on navigating to other fields without entering characters in the 'Card number' field",log);
								}
								else
								{
									Fail("Alert not shown on navigating to other fields without entering characters in the 'Card number' field",log);
								}			
							}
							else
							{
								Fail("Alert not displayed");
							}
						}
						else
						{
							Fail("Credit card number field not empty");
						}							
					}
					else
					{
						Fail("Credit card number field not displayed");
					}	
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1591 Issue ." + e.getMessage());
					Exception(" HBC - 1591 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1593 Register Checkout - Payment Information Section > Verify that alert should be shown on entering the special characters or alphabets or invalid numbers in the "Card number" field and on tapping other fields*/
		public void PaymentSectionsCreditCardNumberInvalidFieldAlert() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1593  Register Checkout - Payment Information Section > Verify that alert should be shown on entering the special characters or alphabets or invalid numbers in the 'Card number' field and on tapping other fields");
			if(shippingPage==true)
			{
				String val = HBCBasicfeature.getExcelVal("HB1593", sheet, 1);
				String valAlert = HBCBasicfeature.getExcelVal("HB1593", sheet, 2);
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
					{						
						Pass("'Card number' textbox field shown ");	
						HBCBasicfeature.scrolldown(paymentOptionsCreditCardNumberField, driver);
						paymentOptionsCreditCardNumberField.clear();
						paymentOptionsCreditCardNumberField.sendKeys(val);
						paymentOptionsRewardCardField2.click();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardNumberFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberFieldAlert))
						{
							String actual = paymentOptionsCreditCardNumberFieldAlert.getText().trim();
							Thread.sleep(200);
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+valAlert);
							Thread.sleep(300);
							if(actual.equalsIgnoreCase(valAlert))
							{
								Pass("Alert shown on entering the special characters or alphabets or invalid numbers in the 'Card number' field",log);
							}
							else
							{
								Fail("Alert not shown on entering the special characters or alphabets or invalid numbers in the 'Card number' field",log);
							}			
						}
						else
						{
							Fail("Alert not displayed");
						}
					}
					else
					{
						Fail("Credit card number field not displayed");
					}							
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1593 Issue ." + e.getMessage());
					Exception(" HBC - 1593 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1592 Register Checkout - Payment Information Section >Verify that on selecting other than "HBC Credit Card" option in the "Card type" dropdown field, "Card number", "Security code", "Expiration" text boxes should be shown*/
		public void PaymentSectionsCreditCardFields() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1592  Register Checkout - Payment Information Section > Verify that on selecting other than 'HBC Credit Card' option in the 'Card type' dropdown field, 'Card number', 'Security code', 'Expiration' text boxes should be shown");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
					{
						Pass("Credit type drop down is displayed");
						HBCBasicfeature.scrolldown(paymentOptionsCreditCardCardSelDropDown, driver);
						Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
						String actual = sel.getFirstSelectedOption().getText();
						log.add("The selected by default option is: "+actual);
						Thread.sleep(200);
						int size = sel.getOptions().size();
						Random r  = new Random();
						boolean flag = false;
						String current = "";						
						do
						{
							int i = r.nextInt(size);	
							if(i<=1)
							{
								flag = false;
							}
							else
							{
								sel.getOptions().get(i).click();
								wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
								current = sel.getFirstSelectedOption().getText();		
								Thread.sleep(200);
								if(!actual.equalsIgnoreCase(current))
								{
									flag = true;
									continue;
								}
							}
						}while(!flag == true);
						log.add("The Current selected option is: "+current);
						Thread.sleep(200);
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
						{
							Pass("'Card number' textbox field shown on selecting other than'HBC Credit Card' option ",log);
						}
						else
						{
							Fail("'Card number' textbox field not shown on selecting other than 'HBC Credit Card' option ",log);
						}						

						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
						{
							Pass("'Card number' Security code field shown on selecting other than'HBC Credit Card' option ");
						}
						else
						{
							Fail("'Card number' Security code field not shown on selecting other than 'HBC Credit Card' option ");
						}						
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
						{
							Pass("'Card number'Expiration Month field shown on selecting other than'HBC Credit Card' option ");
						}
						else
						{
							Fail("'Card number' number'Expiration Year field not shown on selecting other than 'HBC Credit Card' option ");
						}			
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
						{
							Pass("'Card number' Security code field shown on selecting other than'HBC Credit Card' option ");
						}
						else
						{
							Fail("'Card number' Security code field not shown on selecting other than 'HBC Credit Card' option ");
						}			
					}
					else
					{
						Fail("Card type drop down is not displayed");
					}					
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1592 Issue ." + e.getMessage());
					Exception(" HBC - 1592 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1596 Register Checkout - Payment Information Section > Verify that user should be able to enter security code in the "Security Code" field	*/
		public void PaymentSectionsCreditCardSecurityCode() 
		{
			ChildCreation(" HBC - 1596  Register Checkout - Payment Information Section > Verify that user should be able to enter security code in the 'Security Code' field");
			if(shippingPage==true)
			{
				try
				{
					String expctd = HBCBasicfeature.getExcelNumericVal("HB1596", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
					{
						Pass("'Card number' Security code field displayed");
						paymentOptionsCreditCardSecurityCodeField.clear();
						paymentOptionsCreditCardSecurityCodeField.sendKeys(expctd);
						Thread.sleep(200);
						if(!paymentOptionsCreditCardSecurityCodeField.getAttribute("value").isEmpty())
						{
							String actual = paymentOptionsCreditCardSecurityCodeField.getAttribute("value");
							log.add("The actual displayed value is: "+actual);
							log.add("The expected value is: "+expctd);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(expctd))
							{
								Pass(" user able to enter security code in the 'Security Code' field",log);
							}
							else
							{
								Fail("user not able to enter security code in the 'Security Code' field",log);
							}			
						}
						else
						{
							Fail("'Security Code' textbox field is empty");
						}
					}
					else
					{
						Fail("'Security Code' Security code field not shown");
					}			
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1596 Issue ." + e.getMessage());
					Exception(" HBC - 1596 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1599 Register Checkout - Payment Information Section > Verify that "Security Code" fields, should accept maximum of 4 digits*/
		public void PaymentSectionsCreditCardSecurityCodeFieldMaximum()
		{
			ChildCreation(" HBC - 1599  Register Checkout - Payment Information Section > Verify that 'Security Code' fields, should accept maximum of 4 digits");
			if(shippingPage==true)
			{
				try
				{
					String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1599", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
					{
						Pass("'Security code' textbox field shown ");
						for(int i = 0; i<cellVal.length;i++)
						{
							paymentOptionsCreditCardSecurityCodeField.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							paymentOptionsCreditCardSecurityCodeField.sendKeys(cellVal[i]);
							Thread.sleep(500);
							if(paymentOptionsCreditCardSecurityCodeField.getAttribute("value").isEmpty())
							{
								Fail( "The 'Security code' textbox field is empty.");
							}
							else if(paymentOptionsCreditCardSecurityCodeField.getAttribute("value").length()<=4)
							{
								Pass( "The 'Security code' textbox field was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The 'Security code' textbox field was enterable but it is not within the specified character limit.",log);
							}
							paymentOptionsCreditCardSecurityCodeField.clear();
						}
					}
					else
					{
						Fail("'Security code' textbox field not shown ");
					}						
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1599 Issue ." + e.getMessage());
					Exception(" HBC - 1599 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1598 Register Checkout - Payment Information Section >  Verify that alert should be shown on navigating to other fields without entering characters in the "Security Code" fields*/
		public void PaymentSectionsCreditCardSecurityEmptyFieldAlert()
		{
			ChildCreation(" HBC - 1598  Register Checkout - Payment Information Section > Verify that alert should be shown on navigating to other fields without entering characters in the 'Security Code' fields");
			if(shippingPage==true)
			{
				try
				{			
					String expctd = HBCBasicfeature.getExcelVal("HB1598", sheet, 1);
					Actions act = new Actions(driver);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
					{
						Pass("'Security Code' textbox field shown ");	
						Thread.sleep(500);
						paymentOptionsCreditCardSecurityCodeField.click();
						paymentOptionsCreditCardSecurityCodeField.clear();
						if(paymentOptionsCreditCardSecurityCodeField.getAttribute("value").isEmpty())
						{
							log.add("'Security Code' textbox field is empty");
							Thread.sleep(500);
							act.moveToElement(paymentOptionsCreditCardExpMonthField).click().build().perform();
							wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecurityCodeFieldAlert, "style", "block"));
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeFieldAlert))
							{
								String actual = paymentOptionsCreditCardSecurityCodeFieldAlert.getText().trim();
								Thread.sleep(200);
								log.add("The actual alert displayed is: "+actual);
								log.add("The expected alert to be displayed is: "+expctd);
								Thread.sleep(200);
								if(actual.equalsIgnoreCase(expctd))
								{
									Pass("Alert shown on navigating to other fields without entering characters in the 'Security Code' field",log);
								}
								else
								{
									Fail("Alert not shown on navigating to other fields without entering characters in the 'Security Code' field",log);
								}			
							}
							else
							{
								Fail("Alert not displayed");
							}
						}
						else
						{
							Fail("Security Code field not empty");
						}							
					}
					else
					{
						Fail("Security Code field not displayed");
					}	
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1598 Issue ." + e.getMessage());
					Exception(" HBC - 1598 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1597 Register Checkout - Payment Information Section > Verify that alert should be shown on entering the special characters or alphabets or invalid numbers in the "Security Code" field and on tapping other fields*/
		public void PaymentSectionsCreditCardSecurityInvalidFieldAlert() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1597  Register Checkout - Payment Information Section > Verify that alert should be shown on entering the special characters or alphabets or invalid numbers in the 'Security Code' field and on tapping other fields");
			if(shippingPage==true)
			{
				String val = HBCBasicfeature.getExcelVal("HB1597", sheet, 1);
				String valAlert = HBCBasicfeature.getExcelVal("HB1597", sheet, 2);
				Actions act = new Actions(driver);
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
					{
						Pass("'Security Code' textbox field shown ");					
						paymentOptionsCreditCardSecurityCodeField.clear();
						paymentOptionsCreditCardSecurityCodeField.sendKeys(val);
						Thread.sleep(1500);
						act.moveToElement(paymentOptionsCreditCardExpMonthField).click().build().perform();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecurityCodeFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeFieldAlert))
						{
							String actual = paymentOptionsCreditCardSecurityCodeFieldAlert.getText().trim();
							Thread.sleep(200);
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+valAlert);
							Thread.sleep(500);
							if(actual.equalsIgnoreCase(valAlert))
							{
								Pass("Alert shown on entering the special characters or alphabets or invalid numbers in the 'Security Code' field",log);
							}
							else
							{
								Fail("Alert not shown on entering the special characters or alphabets or invalid numbers in the 'Security Code' field",log);
							}			
						}
						else
						{
							Fail("Alert not displayed");
						}
					}
					else
					{
						Fail("Security Code field not displayed");
					}							
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1597 Issue ." + e.getMessage());
					Exception(" HBC - 1597 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1600 Register Checkout - Verify that user should be able to enter expiration date in the "expiration" field*/
		public void PaymentSectionsCreditCardExpirationMonthYear() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1600  Register Checkout - Payment Information Section > Verify that user should be able to enter expiration date in the 'expiration' field");
			if(shippingPage==true)
			{
				try
				{
					String expctdMonth = HBCBasicfeature.getExcelNumericVal("HB1600", sheet, 1);
					String expctdYear = HBCBasicfeature.getExcelNumericVal("HB1600", sheet, 2);

					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
					{
						Pass("Expiration Month field displayed");
						paymentOptionsCreditCardExpMonthField.clear();
						paymentOptionsCreditCardExpMonthField.sendKeys(expctdMonth);
						Thread.sleep(200);
						if(!paymentOptionsCreditCardExpMonthField.getAttribute("value").isEmpty())
						{
							String actual = paymentOptionsCreditCardExpMonthField.getAttribute("value");
							log.add("The actual displayed value is: "+actual);
							log.add("The expected value is: "+expctdMonth);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(expctdMonth))
							{
								Pass(" user able to enter Expiration date in the 'expiration' field",log);
							}
							else
							{
								Fail("user not able to enter Expiration date in the 'expiration' field",log);
							}
							paymentOptionsCreditCardExpMonthField.clear();
						}
						else
						{
							Fail("'Expiration month' textbox field is empty");
						}
					}
					else
					{
						Fail("'Expiration month' not displayed ");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
					{
						Pass("Expiration Year field displayed");
						paymentOptionsCreditCardExpYearField.clear();
						paymentOptionsCreditCardExpYearField.sendKeys(expctdYear);
						Thread.sleep(200);
						if(!paymentOptionsCreditCardExpYearField.getAttribute("value").isEmpty())
						{
							String actual = paymentOptionsCreditCardExpYearField.getAttribute("value");
							log.add("The actual displayed value is: "+actual);
							log.add("The expected value is: "+expctdYear);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(expctdYear))
							{
								Pass(" user able to enter Expiration Year in the 'expiration' field",log);
							}
							else
							{
								Fail("user not able to enter Expiration Year in the 'expiration' field",log);
							}
							paymentOptionsCreditCardExpYearField.clear();
						}
						else
						{
							Fail("'Expiration Year' textbox field is empty");
						}
					}
					else
					{
						Fail("'Expiration Year' not displayed ");
					}			
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1600 Issue ." + e.getMessage());
					Exception(" HBC - 1600 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1601 Register Checkout - Verify that Expiration date format (MM / YYYY) should be shown below the "Expiration Date" field*/
		public void PaymentSectionsCreditCardExpirationMonthYearFormat() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1601  Register Checkout - Payment Information Section > Verify that Expiration date format (MM / YYYY) should be shown below the 'Expiration Date' field");
			if(shippingPage==true)
			{
				try
				{
					String[] expected = HBCBasicfeature.getExcelVal("HB1601", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
					{
						Pass("Expiration Month & Year field displayed");
						String actualMonthTxt = paymentOptionsCreditCardExpMonthField.getAttribute("placeholder");
						Thread.sleep(200);
						String actualYearTxt = paymentOptionsCreditCardExpYearField.getAttribute("placeholder");
						log.add("Expected Month field format is: "+expected[0]);
						log.add("Actual Month field format is: "+actualMonthTxt);
						log.add("Expected Year field format is: "+expected[1]);
						log.add("Actual Year field format is: "+actualYearTxt);
						Thread.sleep(200);
						if(actualMonthTxt.equalsIgnoreCase(expected[0])&&actualYearTxt.equalsIgnoreCase(expected[1]))
						{
							Pass("Expiration date format (MM / YYYY) shown below the 'Expiration Date' field",log);
						}
						else
						{
							Fail("Expiration date format (MM / YYYY) not shown below the 'Expiration Date' field",log);
						}
					}
					else
					{
						Fail("Expiration Month & Year field is not displayed");
					}						
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1601 Issue ." + e.getMessage());
					Exception(" HBC - 1601 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1602 Register Checkout - Payment Information Section > Verify that on entering the special characters or alphabets or invalid numbers in the "Expiration" field and tapping on other fields, alert should be shown*/
		public void PaymentSectionsCreditCardExpirationInvalidFieldAlert() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1602  Register Checkout - Payment Information Section > Verify that on entering the special characters or alphabets or invalid numbers in the 'Expiration' field and tapping on other fields, alert should be shown");
			if(shippingPage==true)
			{
				String monthVal = HBCBasicfeature.getExcelVal("HB1602", sheet, 1);
				String monthAlert = HBCBasicfeature.getExcelVal("HB1602", sheet, 2);
				String yearVal = HBCBasicfeature.getExcelVal("HBC1602", sheet, 1);
				String yearAlert = HBCBasicfeature.getExcelVal("HBC1602", sheet, 2);
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
					{
						Pass("Expiration Month & Year field displayed");
						paymentOptionsCreditCardExpMonthField.clear();
						paymentOptionsCreditCardExpMonthField.sendKeys(monthVal);
						paymentOptionsCreditCardExpYearField.click();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpMonthFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthFieldAlert))
						{
							String actual = paymentOptionsCreditCardExpMonthFieldAlert.getText().trim();
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+monthAlert);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(monthAlert))
							{
								Pass("Alert shown on entering the special characters or alphabets or invalid numbers in the 'Expiration Month' field",log);
							}
							else
							{
								Fail("Alert not shown on entering the special characters or alphabets or invalid numbers in the 'Expiration Month' field",log);
							}
							paymentOptionsCreditCardExpMonthField.clear();
						}
						else
						{
							Fail("Month alert not displayed");
						}
						
						paymentOptionsCreditCardExpYearField.clear();
						paymentOptionsCreditCardExpYearField.sendKeys(yearVal);
						paymentOptionsCreditCardExpMonthField.click();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpYearFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearFieldAlert))
						{
							String actual = paymentOptionsCreditCardExpYearFieldAlert.getText().trim();
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+yearAlert);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(yearAlert))
							{
								Pass("Alert shown on entering the special characters or alphabets or invalid numbers in the 'Expiration Year' field",log);
							}
							else
							{
								Fail("Alert not shown on entering the special characters or alphabets or invalid numbers in the 'Expiration Year' field",log);
							}	
							paymentOptionsCreditCardExpYearField.clear();
						}
						else
						{
							Fail("Year alert not displayed");
						}					
					}
					else
					{
						Fail("Expiration month and year field not displayed");
					}							
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1602 Issue ." + e.getMessage());
					Exception(" HBC - 1602 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1603 Register Checkout - Payment Information Section >Verify that alert should be shown on navigating to other fields without entering characters in the "expiration" fields*/
		public void PaymentSectionsCreditCardExpirationEmptyFieldAlert() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1603  Register Checkout - Payment Information Section >Verify that alert should be shown on navigating to other fields without entering characters in the 'expiration' fields");
			if(shippingPage==true)
			{
				String emptyMAlert = HBCBasicfeature.getExcelVal("HB1603", sheet, 1);
				String emptyYAlert = HBCBasicfeature.getExcelVal("HB1603", sheet, 2);
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
					{
						Pass("Expiration Month field displayed");						
						paymentOptionsCreditCardExpMonthField.clear();						
						if(paymentOptionsCreditCardExpMonthField.getAttribute("value").isEmpty())
						{
							Pass("Month field is empty");
							paymentOptionsCreditCardExpYearField.click();
							wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpMonthFieldAlert, "style", "block"));
							Thread.sleep(200);
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthFieldAlert))
							{
								String actualMAlert = paymentOptionsCreditCardExpMonthFieldAlert.getText().trim();
								Thread.sleep(200);								
								log.add("The actual Month alert displayed is: "+actualMAlert);
								log.add("The expected Month alert to be displayed is: "+emptyMAlert);								
								Thread.sleep(200);
								if(actualMAlert.equalsIgnoreCase(emptyMAlert))
								{
									Pass("Alert shown on navigating to other fields without entering characters in the 'Expiration Month' field",log);
								}
								else
								{
									Fail("Alert not shown on navigating to other fields without entering characters in the 'Expiration Month' field",log);
								}
							}
							else
							{
								Fail("Respective alerts not displayed");
							}
						}
						else
						{
							Fail("Month field is not empty");
						}						
					}
					else
					{
						Fail("Expiration month field not displayed");
					}
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
					{
						Pass("Expiration Year field displayed");						
						paymentOptionsCreditCardExpYearField.clear();						
						if(paymentOptionsCreditCardExpYearField.getAttribute("value").isEmpty())
						{
							Pass("Year field is empty");
							paymentOptionsCreditCardExpMonthField.click();
							wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpYearFieldAlert, "style", "block"));
							Thread.sleep(200);
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearFieldAlert))
							{
								String actualYAlert = paymentOptionsCreditCardExpYearFieldAlert.getText().trim();
								Thread.sleep(200);
								log.add("The actual Year alert displayed is: "+actualYAlert);
								log.add("The expected Year alert to be displayed is: "+emptyYAlert);
								Thread.sleep(200);
								if(actualYAlert.equalsIgnoreCase(emptyYAlert))
								{
									Pass("Alert shown on navigating to other fields without entering characters in the 'Expiration Year' field",log);
								}
								else
								{
									Fail("Alert not shown on navigating to other fields without entering characters in the 'Expiration Year' field",log);
								}
							}
							else
							{
								Fail("Respective alerts not displayed");
							}
						}
						else
						{
							Fail("Year field is not empty");
						}						
					}
					else
					{
						Fail("Expiration Year field not displayed");
					}					
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1603 Issue ." + e.getMessage());
					Exception(" HBC - 1603 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1604 Register Checkout - Payment Information Section >Verify that alert should be shown on entering less than the required digits in the "expiration" fields*/
		public void PaymentSectionsCreditCardExpirationLessDigitsFieldAlert() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1604  Register Checkout - Payment Information Section >Verify that alert should be shown on entering less than the required digits in the 'expiration' fields");
			if(shippingPage==true)
			{
				String monthVal = HBCBasicfeature.getExcelNumericVal("HB1604", sheet, 1);
				String monthAlert = HBCBasicfeature.getExcelVal("HB1604", sheet, 2);
				String yearVal = HBCBasicfeature.getExcelNumericVal("HBC1604", sheet, 1);
				String yearAlert = HBCBasicfeature.getExcelVal("HBC1604", sheet, 2);

				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
					{
						Pass("Expiration Month & Year field displayed");
						HBCBasicfeature.scrollup(paymentOptionsCreditCardExpMonthField, driver);
						paymentOptionsCreditCardExpMonthField.clear();
						paymentOptionsCreditCardExpMonthField.sendKeys(monthVal);
						Thread.sleep(200);
						if(paymentOptionsCreditCardExpMonthField.getAttribute("value").length()<2)
						{
							log.add("Entered value is Less than 2");
							paymentOptionsCreditCardExpYearField.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpMonthFieldAlert, "style", "block"));
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthFieldAlert))
							{
								String actual = paymentOptionsCreditCardExpMonthFieldAlert.getText().trim();
								log.add("The actual alert displayed is: "+actual);
								log.add("The expected alert to be displayed is: "+monthAlert);
								Thread.sleep(500);
								if(actual.equalsIgnoreCase(monthAlert))
								{
									Pass("Alert shown on entering less than the required digits in the 'Expiration Month' field",log);
								}
								else
								{
									Fail("Alert not shown on entering the less than the required digits in the 'Expiration Month' field",log);
								}
								paymentOptionsCreditCardExpMonthField.clear();
							}
							else
							{
								Fail("Month alert not displayed");
							}
						}
						else
						{
							Fail("Entered value length not less than 2");
						}
							
						paymentOptionsCreditCardExpYearField.clear();
						paymentOptionsCreditCardExpYearField.sendKeys(yearVal);
						Thread.sleep(200);
						if(paymentOptionsCreditCardExpYearField.getAttribute("value").length()<4)
						{
							log.add("Entered value is Less than 4");
							paymentOptionsCreditCardExpMonthField.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpYearFieldAlert, "style", "block"));
							if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearFieldAlert))
							{
								String actual = paymentOptionsCreditCardExpYearFieldAlert.getText().trim();
								log.add("The actual alert displayed is: "+actual);
								log.add("The expected alert to be displayed is: "+yearAlert);
								Thread.sleep(200);
								if(actual.equalsIgnoreCase(yearAlert))
								{
									Pass("Alert shown  on entering less than the required digits in the 'Expiration Year' field",log);
								}
								else
								{
									Fail("Alert not shown on entering less than the required digits in the 'Expiration Year' field",log);
								}	
								paymentOptionsCreditCardExpYearField.clear();
							}
							else
							{
								Fail("Year alert not displayed");
							}					
						}
						else
						{
							Fail("Entered value length not less than 4");
						}						
					}
					else
					{
						Fail("Expiration month and year field not displayed");
					}							
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1604 Issue ." + e.getMessage());
					Exception(" HBC - 1604 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1605 Register Checkout - Payment Information Section > Verify that "Expiration" fields, should accept maximum of 2 digits in month field and maximum of 4 digits in year field*/
		public void PaymentSectionsCreditCardExpirationFieldMaximum()
		{
			ChildCreation(" HBC - 1605  Register Checkout - Payment Information Section > Verify that 'Expiration' fields, should accept maximum of 2 digits in month field and maximum of 4 digits in year field");
			if(shippingPage==true)
			{
				try
				{
					String[] cellMVal = HBCBasicfeature.getExcelNumericVal("HBC1605", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
					{
						Pass("'Expiration month' textbox field shown ");
						for(int i = 0; i<cellMVal.length;i++)
						{
							paymentOptionsCreditCardExpMonthField.clear();
							log.add( "The Entered value from the excel file is : " + cellMVal[i].toString());
							log.add( "The Value from the excel file was : " + cellMVal[i].toString() + " and is length is : " + cellMVal[i].toString().length());
							paymentOptionsCreditCardExpMonthField.sendKeys(cellMVal[i]);
							if(paymentOptionsCreditCardExpMonthField.getAttribute("value").isEmpty())
							{
								Fail( "The 'Expiration month' textbox field is empty.");
							}
							else if(paymentOptionsCreditCardExpMonthField.getAttribute("value").length()<=2)
							{
								Pass( "The 'Expiration month' textbox field was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The 'Expiration month' textbox field was enterable but it is not within the specified character limit.",log);
							}
							paymentOptionsCreditCardExpMonthField.clear();
						}
					}
					else
					{
						Fail("'Expiration month' textbox field not shown ");
					}	
					
					String[] cellYVal = HBCBasicfeature.getExcelNumericVal("HBC1605", sheet, 2).split("\n");
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
					{
						Pass("'Expiration Year' textbox field shown ");
						for(int i = 0; i<cellYVal.length;i++)
						{
							paymentOptionsCreditCardExpYearField.clear();
							log.add( "The Entered value from the excel file is : " + cellYVal[i].toString());
							log.add( "The Value from the excel file was : " + cellYVal[i].toString() + " and is length is : " + cellYVal[i].toString().length());
							paymentOptionsCreditCardExpYearField.sendKeys(cellYVal[i]);
							if(paymentOptionsCreditCardExpYearField.getAttribute("value").isEmpty())
							{
								Fail( "The 'Expiration Year' textbox field is empty.");
							}
							else if(paymentOptionsCreditCardExpYearField.getAttribute("value").length()<=4)
							{
								Pass( "The 'Expiration Year' textbox field was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The 'Expiration Year' textbox field was enterable but it is not within the specified character limit.",log);
							}
							paymentOptionsCreditCardExpYearField.clear();
						}
					}
					else
					{
						Fail("'Expiration Year' textbox field not shown ");
					}						
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1605 Issue ." + e.getMessage());
					Exception(" HBC - 1605 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1606 Register Checkout - Verify that alert should be shown on entering the expired year in the "expiration" field*/
		public void PaymentSectionsCreditCardExpiredYear() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1606  Register Checkout - Payment Information Section > Verify that alert should be shown on entering the expired year in the 'expiration' field");
			if(shippingPage==true)
			{
				try
				{
					String expYear = HBCBasicfeature.getExcelNumericVal("HB1606", sheet, 1);
					String yearAlert = HBCBasicfeature.getExcelVal("HBC1604", sheet, 2);

			
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
					{
						Pass("Expiration Year field displayed");
						paymentOptionsCreditCardExpYearField.clear();
						paymentOptionsCreditCardExpYearField.sendKeys(expYear);
						paymentOptionsCreditCardExpMonthField.click();	
						Thread.sleep(200);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpYearFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearFieldAlert))
						{
							String actual = paymentOptionsCreditCardExpYearFieldAlert.getText().trim();
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+yearAlert);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(yearAlert))
							{
								Pass("Alert shown  on entering expired year in the 'Expiration Year' field",log);
							}
							else
							{
								Fail("Alert not shown on entering expired year in the 'Expiration Year' field",log);
							}	
							paymentOptionsCreditCardExpYearField.clear();
						}
						else
						{
							Fail("Year alert not displayed");
						}
					}
					else
					{
						Fail("'Expiration Year'field not displayed ");
					}			
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1606 Issue ." + e.getMessage());
					Exception(" HBC - 1606 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1608 Register Checkout - Verify that "Pay by gift card" section should display "Card number", "Pin" text boxes with "Apply" button.*/
		public void PaymentSectionsPaybygiftcard() throws java.lang.Exception
		{
			ChildCreation(" HBC - 1608  Register Checkout - Payment Information Section > Verify that 'Pay by gift card' section should display 'Card number', 'Pin' text boxes with 'Apply' button.");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsGiftCard))
					{
						Pass("Apply Gift Card option is displayed");
						HBCBasicfeature.scrollup(paymentOptionsGiftCard, driver);
						jsclick(paymentOptionsGiftCardChkkBox);
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						
						if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardNumberField))
						{
							Pass("Pay by gift card' section display 'Card number' field");
						}
						else
						{
							Fail("Pay by gift card' section not display 'Card number' field");
						}
						
						if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardPinField))
						{
							Pass("Pay by gift card' section display 'Pin' text boxes field");
						}
						else
						{
							Fail("Pay by gift card' section not display 'Pin' text boxes field");
						}
						
						if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardApply))
						{
							Pass("Pay by gift card' section display 'Apply' button");
						}
						else
						{
							Fail("Pay by gift card' section not display 'Apply' button");
						}						
					}
					else
					{
						Fail("Apply Gift card option is not displayed");
					}					
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1608 Issue ." + e.getMessage());
					Exception(" HBC - 1608 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}	
		
		/* HBC - 1609 Register Checkout - Payment Information Section > Verify that "Pin" fields should accept maximum of 4  digits*/
		public void PaymentSectionsApplyGiftPinFieldMaximum()
		{
			ChildCreation(" HBC - 1609  Register Checkout - Payment Information Section > Verify that 'Pin' fields should accept maximum of 4 digits");
			if(shippingPage==true)
			{
				try
				{
					String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1609", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardPinField))
					{
						Pass("'Pin' number field shown ");
						for(int i = 0; i<cellVal.length;i++)
						{
							paymentOptionsApplyGiftCardPinField.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							paymentOptionsApplyGiftCardPinField.sendKeys(cellVal[i]);
							Thread.sleep(500);
							if(paymentOptionsApplyGiftCardPinField.getAttribute("value").isEmpty())
							{
								Fail( "The 'Pin number' field is empty.");
							}
							else if(paymentOptionsApplyGiftCardPinField.getAttribute("value").length()<=4)
							{
								Pass( "The 'Pin number' field was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The 'Pin number' field was enterable but it is not within the specified character limit.",log);
							}
							paymentOptionsApplyGiftCardPinField.clear();
						}
					}
					else
					{
						Fail("'Pin number' field not shown ");
					}						
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1609 Issue ." + e.getMessage());
					Exception(" HBC - 1609 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1610 Register Checkout - Payment Information Section > Verify that "Add Another Gift Card" text link should be shown in "Pay by gift card" section*/
		public void PaymentSectionsAddAnotherGiftCard()
		{
			ChildCreation(" HBC - 1610  Register Checkout - Payment Information Section > Verify that 'Add Another Gift Card' text link should be shown in 'Pay by gift card' section");
			if(shippingPage==true)
			{
				try
				{
					String expctd = HBCBasicfeature.getExcelVal("HBC1610", sheet, 1);
					if(HBCBasicfeature.isElementPresent(paymentOptionsAddAnotherGiftCard))
					{
						String actual = paymentOptionsAddAnotherGiftCard.getText();
						log.add("The actual text link is: "+ actual);
						log.add("The expected text link is: "+ expctd);
						
						if(actual.equalsIgnoreCase(expctd))
						{
							Pass("'Add Another Gift Card' text link shown in 'Pay by gift card' section",log);
						}
						else
						{
							Fail("'Add Another Gift Card' text link not shown in 'Pay by gift card' section",log);
						}					
					}
					else
					{
						Fail("'Add Another Gift Card' text link not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1610 Issue ." + e.getMessage());
					Exception(" HBC - 1610 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
	
		/* HBC - 1611 Register Checkout - Payment Information Section > Verify that on tapping "Add Another Gift Card" text link in "Pay by gift card" section the respective action should be done*/
		public void PaymentSectionsAddAnotherGiftCardClick()
		{
			ChildCreation(" HBC - 1611  Register Checkout - Payment Information Section > Verify that on tapping 'Add Another Gift Card' text link in 'Pay by gift card' section the respective action should be done");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsAddAnotherGiftCard))
					{
						Pass("'Add Another Gift Card' text link is displayed");
						paymentOptionsAddAnotherGiftCard.click();				
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(secondGiftCard));
						if(HBCBasicfeature.isElementPresent(secondGiftCard))
						{
							Pass( "The Second Gift Card is displayed.");
						}
						else
						{
							Fail( "The Second Gift Card is not displayed.");
						}
					}
					else
					{
						Fail("'Add Another Gift Card' text link is not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1611 Issue ." + e.getMessage());
					Exception(" HBC - 1611 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1612 Register Checkout - Payment Information Section > Verify that "Pay by credit card" checkbox should be deselected automatically on tapping "Pay by PayPal" radio tab*/
		public void PaymentSectionsPaybycreditcardDeselect()
		{
			ChildCreation(" HBC - 1612  Register Checkout - Payment Information Section > Verify that 'Pay by credit card' checkbox should be deselected automatically on tapping 'Pay by PayPal' radio tab");
			if(shippingPage==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
					{
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecEnable, "style", "block"));
						Pass( "The Credit Card option was selected.");
						if(HBCBasicfeature.isElementPresent(paymentOptionsPaypalChkkBox))
						{
							HBCBasicfeature.scrollup(paymentOptionsPaypalChkkBox, driver);
							jsclick(paymentOptionsPaypalChkkBox);
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
							wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecEnable, "style", "none"));
							Thread.sleep(500);
							if(!HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected)&&HBCBasicfeature.isElementPresent(paymentOptionsPaypalSelected))
								
							{
								Pass("'Pay by credit card' checkbox deselected automatically on tapping 'Pay by PayPal' radio tab");
							}
							else
							{
								Fail("'Pay by credit card' checkbox not deselected automatically on tapping 'Pay by PayPal' radio tab");
							}
						}
						else
						{
							Fail("'Pay by paypal' option not displayed");
						}					
					}
					else
					{
						Fail("'Add Another Gift Card' text link is not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1612 Issue ." + e.getMessage());
					Exception(" HBC - 1612	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1614 Register Checkout - Verify that "Review & Submit" section should be as per the creative*/
		public void ReviewAndSubmitCreative()
		{
			ChildCreation(" HBC - 1614  Register Checkout - Verify that 'Review & Submit' section should be as per the creative");
			if(shippingPage==true)
			{
				try
				{
					AddPaymentReviewOrderClick();
					if(reviewSubmit==true)
					{
						wait.until(ExpectedConditions.visibilityOf(checkoutNavTabs));
						if(HBCBasicfeature.isElementPresent(checkoutNavTabs))
						{
							Pass("Checkout Navigation Tab's are displayed");
						}
						else
						{
							Fail("Checkout Navigation Tab's are  not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddress))
						{
							Pass("Review & Submit page Shipping Address section is displayed");
						}
						else
						{
							Fail("Review & Submit page Shipping Address section is not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethod))
						{
							Pass("Review & Submit page Shipping Method section is displayed");
						}
						else
						{
							Fail("Review & Submit page Shipping Method section is not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddress))
						{
							Pass("Review & Submit page Billing Address section is displayed");
						}
						else
						{
							Fail("Review & Submit page Billing Address section is not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethod))
						{
							Pass("Review & Submit page Billing Method section is displayed");
						}
						else
						{
							Fail("Review & Submit page Billing Method section is not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitCheckoutSummary))
						{
							HBCBasicfeature.scrolldown(ReviewandSubmitCheckoutSummary, driver);
							Pass("Review & Submit page checkout summary section is displayed");
						}
						else
						{
							Fail("Review & Submit page checkout summary section is not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitPlaceorderbutton))
						{
							Pass("Review & Submit page Placeorder button is displayed");
						}
						else
						{
							Fail("Review & Submit page Placeorder button is not displayed");
						}						
					}
					else
					{
						Fail("Review & Submit page not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1614 Issue ." + e.getMessage());
					Exception(" HBC - 1614	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1615 Register Checkout - Verify that user can be able to navigate to the previous section*/
		/* HBC - 1616 Register Checkout -  Verify that user should be navigate to the 'Shipping & Payment' section on clicking the 'Shipping & Payment' link*/
		public void ReviewAndSubmitToPreviousTabs()
		{
			ChildCreation(" HBC - 1615  Register Checkout - Verify that user can be able to navigate to the previous section");
			boolean HBC1616 = false;
			if(reviewSubmit==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
					{
						Pass("Currently in Review and Submit page");
						if(HBCBasicfeature.isElementPresent(shippingandPayment))
						{
							shippingandPayment.click();
							wait.until(ExpectedConditions.attributeContains(shippingPaymentPage, "style", "visible"));
							if(HBCBasicfeature.isElementPresent(shippingPaymentPage))
							{
								Pass("Checkout shipping & payment page displayed");
								Pass("user can able to navigate to the previous sections");
								HBC1616 = true;	
								
								//To compare HBC1626
								 shipAddress = shipandPaypageShipAddressDisp.getText();
								 billAddress = shipandPaypagePayAddressDisp.getText();	
								 HBCBasicfeature.scrolldown(paymentOptionsShippingMethoddropdown, driver);
								 Select sel = new Select(paymentOptionsShippingMethoddropdown);
								 shipMethod = sel.getFirstSelectedOption().getText();	
							}
							else
							{
								Fail("Checkout shipping & payment page not displayed");
							}																
						}
						else
						{
							Fail("Shipping & Payment tab not displayed");
						}
					}
					else
					{
						Fail("Review and Submit page not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1615 Issue ." + e.getMessage());
					Exception(" HBC - 1615	 Issue ." + e.getMessage());
				}
				
				ChildCreation(" HBC - 1616  Register Checkout -  Verify that user should be navigate to the 'Shipping & Payment' section on clicking the 'Shipping & Payment' link");
				if(HBC1616==true)
				{
					Pass("user can navigate to the 'Shipping & Payment' section on clicking the 'Shipping & Payment' link");	
				}
				else
				{
					Fail("user not able to navigate to the 'Shipping & Payment' section on clicking the 'Shipping & Payment' link");
				}
				
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
				
		/* HBC - 1617 Register Checkout -  Verify that "Shipping Address", "Shipping Method", "Billing Address", "Payment Method" columns should be shown along with "Edit" text links in the "Review & Submit" section*/
		public void ReviewAndSubmitColumswithEdit()
		{
			ChildCreation(" HBC - 1617  Register Checkout -  Verify that 'Shipping Address', 'Shipping Method', 'Billing Address', 'Payment Method' columns should be shown along with Edit text links in the Review & Submit section");
			if(shippingPage==true)
			{
				try
				{
					AddPaymentReviewOrderClick();
					if(reviewSubmit==true)
					{						
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddress))
						{
							Pass("Review & Submit page Shipping Address section is displayed");
							if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressEdit))
							{
								Pass("Edit link displayed for 'Shipping Address' section ");
							}
							else
							{
								Fail("Edit link not displayed for 'Shipping Address' section");
							}
						}
						else
						{
							Fail("Review & Submit page Shipping Address section is not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethod))
						{
							Pass("Review & Submit page Shipping Method section is displayed");
							if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodEdit))
							{
								Pass("Edit link displayed for 'Shipping Method' section ");
							}
							else
							{
								Fail("Edit link not displayed for 'Shipping Method' section");
							}
						}
						else
						{
							Fail("Review & Submit page Shipping Method section is not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddress))
						{
							Pass("Review & Submit page Billing Address section is displayed");
							if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressEdit))
							{
								Pass("Edit link displayed for 'Billing Address' section ");
							}
							else
							{
								Fail("Edit link not displayed for 'Billing Address' section");
							}
						}
						else
						{
							Fail("Review & Submit page Billing Address section is not displayed");
						}
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethod))
						{
							Pass("Review & Submit page Billing Method section is displayed");
							if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodEdit))
							{
								Pass("Edit link displayed for 'Payment Method' section ");
							}
							else
							{
								Fail("Edit link not displayed for 'Payment Method' section");
							}
						}
						else
						{
							Fail("Review & Submit page Billing Method section is not displayed");
						}						
					}
					else
					{
						Fail("Review & Submit page not displayed");
					}
				}			
				catch(Exception e)
				{
					System.out.println(" HBC - 1617 Issue ." + e.getMessage());
					Exception(" HBC - 1617	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1618 Register Checkout -  Verify that saved "Shipping Address", "Shipping Method", "Billing Address", "Payment Method" should be shown along with "Edit" text links in "Review & Submit" section*/
		public void ReviewAndSubmitSavedDetails()
		{
			ChildCreation(" HBC - 1618  Register Checkout - Verify that saved 'Shipping Address', 'Shipping Method', 'Billing Address', 'Payment Method' should be shown along with 'Edit' text links in 'Review & Submit' section");
			if(reviewSubmit==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressDetails))
					{
						Pass("Review & Submit page Shipping Address Details section is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressEdit))
						{
							Pass("Edit link displayed for 'Shipping Address' section ");
						}
						else
						{
							Fail("Edit link not displayed for 'Shipping Address' section");
						}
					}
					else
					{
						Fail("Review & Submit page Shipping Address Details section is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodSelected))
					{
						Pass("Review & Submit page Shipping Method section details is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodEdit))
						{
							Pass("Edit link displayed for 'Shipping Method' section ");
						}
						else
						{
							Fail("Edit link not displayed for 'Shipping Method' section");
						}
					}
					else
					{
						Fail("Review & Submit page Shipping Method section details is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressDetails))
					{
						Pass("Review & Submit page Billing Address section details is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressEdit))
						{
							Pass("Edit link displayed for 'Billing Address' section ");
						}
						else
						{
							Fail("Edit link not displayed for 'Billing Address' section");
						}
					}
					else
					{
						Fail("Review & Submit page Billing Address section details is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodDetails))
					{
						Pass("Review & Submit page Billing Method section Details is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodEdit))
						{
							Pass("Edit link displayed for 'Payment Method' section ");
						}
						else
						{
							Fail("Edit link not displayed for 'Payment Method' section");
						}
					}
					else
					{
						Fail("Review & Submit page Billing Method section Details is not displayed");
					}						
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1618 Issue ." + e.getMessage());
					Exception(" HBC - 1618	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1626 Register Checkout -  Verify that saved "Shipping Address", "Shipping Method", "Billing Address", "Payment Method"  must be exactly same as when entered in the forms*/
		public void ReviewAndSubmitSavedDetailsCompare()
		{
			ChildCreation(" HBC - 1626  Register Checkout - Verify that saved 'Shipping Address', 'Shipping Method', 'Billing Address', 'Payment Method'  must be exactly same as when entered in the forms");
			if(reviewSubmit==true)
			{
				try
				{
					String actual = HBCBasicfeature.getExcelVal("HBCCard", sheet, 1);
					String cnumber = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 2);
					String[] rsBillAddress , BillAddres, rspaymethod  ;
					String[] rsShipAddress , shipAddres;
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressDetails))
					{
						Pass("Shipping Address Details are displayed");
						rsShipAddress = ReviewandSubmitShippingAddressDetails.getText().trim().split("\n");
						Thread.sleep(200);
						log.add("Entered details in form is: "+shipAddress);
						shipAddres = shipAddress.replaceAll(",", "").trim().split("\n");
						for(int i=0;i<2;i++)
						{							
							log.add("The current dispalyed Shipping Address Detail: "+rsShipAddress[i]);
							Thread.sleep(200);
							if(shipAddres[i].isEmpty()&&rsShipAddress[i].isEmpty())
							{
								Fail("Address details are empty",log);
							}
							else if(shipAddres[i].contains(rsShipAddress[i]))
							{
								Pass("'Shipping Address'detail exactly same as when entered in the forms",log);
							}
							else
							{
								Fail("Shipping Address' not same as when entered in the forms",log);
							}						
						}					
					}
					else
					{
						Fail("Review & Submit page Shipping Address section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodSelected))
					{
						Pass("Review & Submit page Shipping Method section details is displayed");
						String rsShippingMethod = ReviewandSubmitShippingMethodSelected.getText().trim();
						Thread.sleep(200);
						log.add("The current dispalyed Shipping Method Detail: "+rsShippingMethod);
						log.add("Entered Shipping Method details in form is: "+shipMethod);
						Thread.sleep(200);
						if(shipMethod.isEmpty()&&rsShippingMethod.isEmpty())
						{
							Fail("Shipping Mentod detail is empty",log);
						}
						else if(shipMethod.contains(rsShippingMethod))
						{
							Pass("'Shipping Method' exactly same as when entered in the forms",log);
						}
						else
						{
							Fail("Shipping Method' not same as when entered in the forms",log);
						}
					}
					else
					{
						Fail("Review & Submit page Shipping Method section details is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressDetails))
					{
						Pass("Review & Submit page Shipping Method section details is displayed");
						rsBillAddress = ReviewandSubmitBillingAddressDetails.getText().trim().split("\n");
						Thread.sleep(200);
						log.add("Entered Billing Address details in form is: "+billAddress);
						BillAddres = shipAddress.replaceAll(",", "").split("\n");
						Thread.sleep(200);
						for(int i=0;i<2;i++)
						{	
							log.add("The current dispalyed Billing Address Detail: "+rsBillAddress[i]);
							if(BillAddres[i].isEmpty()&&rsBillAddress[i].isEmpty())
							{
								Fail("Billing address detail is empty",log);
							}
							else if(BillAddres[i].contains(rsBillAddress[i]))
							{
								Pass("'Billing address' exactly same as when entered in the forms",log);
							}
							else
							{
								Fail("Billing address' not same as when entered in the forms",log);
							}
						}
					}
					else
					{
						Fail("Review & Submit page Billing Address section details is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodDetails))
					{
						Pass("Review & Submit page Billing Method section details is displayed");
						rspaymethod = ReviewandSubmitBillingMethodDetails.getText().trim().split("\n");
						Thread.sleep(200);
						log.add("Entered  Billing Method  details in form is: "+actual+" and "+cnumber);	
						String snum = rspaymethod[1].substring(12);
						String eNum = cnumber.substring(12);
						Thread.sleep(200);
						if(rspaymethod[0].isEmpty()&&rspaymethod[1].isEmpty())
						{
							Fail(" Billing Method detail is empty",log);
						}
						else if(rspaymethod[0].equalsIgnoreCase(actual)&&snum.contains(eNum))
						{
							Pass("' Billing Method ' exactly same as when entered in the forms",log);
						}
						else
						{
							Fail("' Billing Method ' not same as when entered in the forms",log);
						}
					}
					else
					{
						Fail("Review & Submit page Billing Method  section details is not displayed");
					}					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1626 Issue ." + e.getMessage());
					Exception(" HBC - 1626	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1628 Register Checkout -   Verify that details in the 'Review & Submit' section must be displayed with the modified changes on making changes and selecting 'Ok' button*/
		public void ReviewAndSubmitAfterModification()
		{
			ChildCreation(" HBC - 1628  Register Checkout - Verify that details in the 'Review & Submit' section must be displayed with the modified changes on making changes and selecting 'Ok' button");
			if(reviewSubmit==true)
			{
				boolean HBC1621 = false;
				try
				{
					String name = HBCBasicfeature.getExcelNumericVal("HBC1628", sheet, 1);
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressDetails))
					{
						Pass("Shipping Address Details are displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressNameDetails))
						{
							String currentName = ReviewandSubmitBillingAddressNameDetails.getText();
							log.add("Phone Number displayed currently is: "+currentName);
							if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressEdit))
							{
								Pass("Edit link displayed for 'Billing Address' section ");
								jsclick(ReviewandSubmitBillingAddressEdit);
								Thread.sleep(1000);
								wait.until(ExpectedConditions.visibilityOf(chkoutAddressPage));
								if(HBCBasicfeature.isElementPresent(chkoutAddressPage))
								{
									reviewSubmit=false;
									HBC1621 = true;
									Pass("Address Page displayed");
									if(HBCBasicfeature.isElementPresent(firstName))
									{
										HBCBasicfeature.scrolldown(firstName, driver);
										firstName.clear();
										firstName.sendKeys(name);
										Thread.sleep(500);
										HBCBasicfeature.scrolldown(AddressSubmit, driver);
										AddressSubmit.click();
										try
										{
											wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
											if(HBCBasicfeature.isElementPresent(addressuseasEntered))
											{
												addressuseasEntered.click();
												wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
												if(HBCBasicfeature.isElementPresent(shipandPaypage))
												{
													Pass("Shipping & Payment page displayed");
												}
											}
										}
										catch(Exception e)
										{
											Pass("No Address verification overlay displayed");
										}
										if(HBCBasicfeature.isElementPresent(shipandPaypage))
										{
											Pass("Shipping & payment page displayed");
											if(HBCBasicfeature.isElementPresent(shipandPaypageShipPayToName))
											{
												Pass("Billing Address dropdown displayed");
												Select sel = new Select(shipandPaypageShipPayToName);
												Thread.sleep(200);
												sel.selectByVisibleText("Bat Man");
												wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
												Thread.sleep(1000);											
												AddPaymentReviewOrderClick();
												if(reviewSubmit==true)
												{
													wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
													if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
													{
														Pass("Review & Submit page displayed");
														if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressNameDetails))
														{
															String UpdatedName = ReviewandSubmitBillingAddressNameDetails.getText();
															log.add("Phone Number displayed after update is: "+UpdatedName);
															Thread.sleep(500);
															if(currentName.equalsIgnoreCase(UpdatedName))
															{
																Fail("'Review & Submit' section not displayed with the modified changes on making changes and selecting 'Ok' button",log);
															}
															else
															{
																Pass("'Review & Submit' section displayed with the modified changes on making changes and selecting 'Ok' button",log);
															}
														}
														else
														{
															Fail("BillingAddress Name not displayed");
														}
													}
													else
													{
														Fail("Chceckout review & Submit tab not displayed");
													}
												}
												else
												{
													Fail("Chceckout review & Submit page not displayed");
												}
											}
											else
											{
												Fail("Payment  section Select address dropdown not displayed");
											}
										}
										else
										{
											Fail("Shipping & payment page not displayed");
										}
									}
									else
									{
										Fail("Addresss section Firstname field not displayed");
									}									
								}
								else
								{
									Fail("Addresss section page not displayed");
								}		
							}	
							else
							{
								Fail("Edit link not displayed for 'Billing Address' section");
							}	
						}
						else
						{
							Fail("BillingAddress Name not displayed");
						}
					}	
					else
					{
						Fail("BillingAddress Name not displayed");
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1628 Issue ." + e.getMessage());
					Exception(" HBC - 1628	 Issue ." + e.getMessage());
				}
				
				ChildCreation(" HBC - 1621  Register Checkout - Verify that page should navigates to 'Addresses' section on tapping the 'Edit' link near the Billing Address text");
				if(HBC1621==true)
				{
					Pass("Page navigates to 'Addresses' section on tapping the 'Edit' link near the Billing Address text");
				}
				else
				{
					Fail("Page not navigates to 'Addresses' section on tapping the 'Edit' link near the Billing Address text");
				}			
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1619 Register Checkout -   Verify that page should navigates to "Addresses" section on tapping the "Edit" link near the Shipping Address text*/
		public void ReviewAndSubmitShippingEditToAddressSection()
		{
			ChildCreation(" HBC - 1619  Register Checkout - Verify that page should navigates to 'Addresses' section on tapping the 'Edit' link near the Shipping Address text");
			if(reviewSubmit==true)
			{
				try
				{					
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressDetails))
					{
						Pass("Review & Submit page Shipping Address Details section is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressEdit))
						{
							Pass("Edit link displayed for 'Shipping Address' section ");
							ReviewandSubmitShippingAddressEdit.click();
							wait.until(ExpectedConditions.visibilityOf(chkoutAddressPage));
							if(HBCBasicfeature.isElementPresent(chkoutAddressPage))
							{
								reviewSubmit=false;
								Pass("page navigates to 'Addresses' section on tapping the 'Edit' link near the Shipping Address text");
								if(HBCBasicfeature.isElementPresent(AddressSubmit))
								{
									HBCBasicfeature.scrolldown(AddressSubmit, driver);
									AddressSubmit.click();
									try
									{
										wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
										if(HBCBasicfeature.isElementPresent(addressuseasEntered))
										{
											addressuseasEntered.click();
											wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
											if(HBCBasicfeature.isElementPresent(shipandPaypage))
											{
												Pass("Shipping & Payment page displayed");
											}
										}
									}
									catch(Exception e)
									{
										Pass("No Address verification overlay displayed");
									}
									if(HBCBasicfeature.isElementPresent(shipandPaypage))
									{
										Pass("Shipping & payment page displayed");
										if(HBCBasicfeature.isElementPresent(shipandPaypageShipPayToName))
										{
											Pass("Billing Address dropdown displayed");
											/*Select sel = new Select(shipandPaypageShipPayToName);
											Thread.sleep(500);
											sel.selectByVisibleText("Bat Man(2)");
											wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
											Thread.sleep(1000);											*/
											AddPaymentReviewOrderClick();
											if(reviewSubmit==true)
											{
												wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
												if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
												{
													Pass("Review & Submit page displayed");
												}
											}
											else
											{
												Fail("Review & Submit page not displayed");
											}
										}
										else
										{
											Fail("Billing Address dropdown not displayed");
										}
									}
									else
									{
										Fail("Shipping & payment page not displayed");
									}
								}
								else
								{
									Fail("Address submit option not displayed");
								}
							}
							else
							{
								Fail("page not navigates to 'Addresses' section on tapping the 'Edit' link near the Shipping Address text");
							}
						}
						else
						{
							Fail("Edit link not displayed for 'Shipping Address' section");
						}
					}
					else
					{
						Fail("Review & Submit page Shipping Address Details section is not displayed");
					}	
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1619 Issue ." + e.getMessage());
					Exception(" HBC - 1619	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1620 Register Checkout -   Verify that page should navigates to "Shipping & Payment" section on tapping the "Edit" link near the Shipping Method  text*/
		public void ReviewAndSubmitShippingMethodEditToShipPayment()
		{
			ChildCreation(" HBC - 1620  Register Checkout - Verify that page should navigates to 'Shipping & Payment' section on tapping the 'Edit' link near the Shipping Method  text");
			if(reviewSubmit==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodSelected))
					{
						Pass("Review & Submit page Shipping Method Details section is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodEdit))
						{
							Pass("Edit link displayed for 'Shipping Method' section ");
							jsclick(ReviewandSubmitShippingMethodEdit);
							wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
							if(HBCBasicfeature.isElementPresent(shipandPaypage))
							{
								reviewSubmit=false;
								Pass("page navigates to 'Shipping & Payment' section on tapping the 'Edit' link near the Shipping Method text");
								AddPaymentReviewOrderClick();
								if(reviewSubmit==true)
								{
									wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
									if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
									{
										Pass("Review & Submit page displayed");
									}
								}
								else
								{
									Fail("Review & Submit page not displayed");
								}
							}										
							else
							{
								Fail("Shipping & payment page not displayed on tapping the 'Edit' link near the Shipping Method text");
							}							
						}
						else
						{
							Fail("Edit link not displayed for 'Shipping Method' section");
						}
					}
					else
					{
						Fail("Review & Submit page Shipping Method Details section is not displayed");
					}	
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1620 Issue ." + e.getMessage());
					Exception(" HBC - 1620	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1622 Register Checkout -   Verify that page should navigates to "Shipping & Payment" section on tapping the "Edit" link near the Payment  Method  text*/
		public void ReviewAndSubmitPaymentMethodEditToShipPayment()
		{
			ChildCreation(" HBC - 1622  Register Checkout - Verify that page should navigates to 'Shipping & Payment' section on tapping the 'Edit' link near the Payment Method text");
			if(reviewSubmit==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodDetails))
					{
						Pass("Review & Submit page Payment Method Details section is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodEdit))
						{
							Pass("Edit link displayed for 'Payment Method' section ");
							jsclick(ReviewandSubmitBillingMethodEdit);
							wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
							if(HBCBasicfeature.isElementPresent(shipandPaypage))
							{
								reviewSubmit=false;
								Pass("page navigates to 'Shipping & Payment' section on tapping the 'Edit' link near the Payment Method text");
								AddPaymentReviewOrderClick();
								if(reviewSubmit==true)
								{
									wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
									if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
									{
										Pass("Review & Submit page displayed");
									}
								}
								else
								{
									Fail("Review & Submit page not displayed");
								}
							}										
							else
							{
								Fail("Shipping & payment page not displayed on tapping the 'Edit' link near the Payment Method text");
							}							
						}
						else
						{
							Fail("Edit link not displayed for 'Payment Method' section");
						}
					}
					else
					{
						Fail("Review & Submit page Payment Method Details section is not displayed");
					}	
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1622 Issue ." + e.getMessage());
					Exception(" HBC - 1622	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1625 Register Checkout -   Verify that price must also be updated in the 'Review & Submit' section on updating the product quantity in the shopping bag bag*/
		public void ReviewAndSubmitPriceChangewhenQuantityUpdate()
		{
			ChildCreation(" HBC - 1625  Register Checkout -  Verify that price must also be updated in the 'Review & Submit' section on updating the product quantity in the shopping bag bag");
			if(reviewSubmit==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
					{
						Pass("Review & Submit page displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitOrderTotal))
						{
							HBCBasicfeature.scrolldown(ReviewandSubmitOrderTotal, driver);
							String currentPrice = ReviewandSubmitOrderTotal.getText().replaceAll("$", "");
							log.add("Current order total price is: "+ currentPrice);
							HBCBasicfeature.scrollup(bagIcon, driver);
							Thread.sleep(500);
							jsclick(bagIcon);
							wait.until(ExpectedConditions.visibilityOf(shoppingBagPage));			
							if(HBCBasicfeature.isElementPresent(shoppingBagPage))
							{
								Pass("Shopping Bag page displayed",log);
								int qnty = Integer.parseInt(prodQuanty.getAttribute("value"));
								log.add("Product count is: "+qnty);
								if(qnty<5)
								{
									Thread.sleep(500);
									prodQuantyInc.click();
									Thread.sleep(1000);
									wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
									if(HBCBasicfeature.isElementPresent(checkoutButton))
									{
										jsclick(checkoutButton);
										wait.until(ExpectedConditions.attributeContains(shippingPaymentPage, "style", "visible"));
										if(HBCBasicfeature.isElementPresent(shippingPaymentPage))
										{
											Pass("shipping & payment page displayed",log);
											AddPaymentReviewOrderClick();
											if(reviewSubmit==true)
											{
												if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
												{
													Pass("Review & Submit page displayed");
													if(HBCBasicfeature.isElementPresent(ReviewandSubmitOrderTotal))
													{
														HBCBasicfeature.scrolldown(ReviewandSubmitOrderTotal, driver);
														String UpdatedPrice = ReviewandSubmitOrderTotal.getText().replaceAll("$", "");
														Thread.sleep(500);
														log.add("Updated order total price is: "+ UpdatedPrice);
														if(currentPrice==UpdatedPrice)
														{
															Fail("Price not updated in the 'Review & Submit' section on updating the product quantity in the shopping bag",log);
														}
														else
														{
															Pass("Price updated in the 'Review & Submit' section on updating the product quantity in the shopping bag",log);
														}
														
														//Decrementing the product count
														HBCBasicfeature.scrollup(bagIcon, driver);
														jsclick(bagIcon);
														wait.until(ExpectedConditions.visibilityOf(shoppingBagPage));			
														if(HBCBasicfeature.isElementPresent(shoppingBagPage))
														{
															log.add("Shopping Bag page displayed");
															reviewSubmit =false;
															if(HBCBasicfeature.isElementPresent(prodQuantyDec))
															{
																prodQuantyDec.click();	
																wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
																Thread.sleep(1500);
														/*		if(HBCBasicfeature.isElementPresent(checkoutButton))
																{
																	checkoutButton.click();
																	wait.until(ExpectedConditions.attributeContains(shippingPaymentPage, "style", "visible"));
																	if(HBCBasicfeature.isElementPresent(shippingPaymentPage))
																	{
																		Pass("shipping & payment page displayed");
																		AddPaymentReviewOrderClick();
																		if(reviewSubmit==true)
																		{
																			if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
																			{
																				Pass("Review & Submit page displayed");
																			}
																			else
																			{
																				Fail("Review & submit page not displayed");
																			}
																		}																
																		else
																		{
																			Fail("Review & submit page not displayed");
																		}
																	}
																	else
																	{
																		Fail("shipping & payment page not displayed");
																	}
																}
																else
																{
																	Fail("Checkout button not displayed");
																}	*/
															}
															else
															{
																Fail("Prod Quantity decrement not displayed");
															}														
														}
														else
														{
															Fail("Shopping bag page not displayed");
														}
													}
													else
													{
														Fail("OrderTotal not displayed");
													}
												}
												else
												{
													Fail("Review & submit page not displayed");
												}
											}
											else
											{
												Fail("Review & submit page not displayed");
											}
										}
										else
										{
											Fail("shipping & payment page not displayed");
										}
									}
									else
									{
										Fail("Checkout button not displayed");
									}											
								}
								else
								{
									Fail("Cant able to add more products already qnty is 5 products ",log);
								}
							}
							else
							{
								Fail("Shopping bag page not displayed");
							}
						}
						else
						{
							Fail("OrderTotal not displayed");
						}
					}
					else
					{
						Fail("Review & submit page not displayed");
					}						
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1625 Issue ." + e.getMessage());
					Exception(" HBC - 1625	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1629 Register Checkout -    Verify that Order details page must be displayed on tapping the "Place Order" button*/
		public void ReviewAndSubmitPlaceOrderToOrderConfrim()
		{
			ChildCreation(" HBC - 1629  Register Checkout -  Verify that Order details page must be displayed on tapping the 'Place Order' button");
			if(reviewSubmit==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(placeOrder))
					{
						HBCBasicfeature.scrolldown(placeOrder, driver);
						jsclick(placeOrder);
						wait.until(ExpectedConditions.attributeContains(confirmOrderPage, "style", "visible"));
						if(HBCBasicfeature.isElementPresent(confirmOrderPage))
						{
							Pass("Order details page displayed on tapping the 'Place Order' button");
							orderConfirm = true;
						}
						else
						{
							Fail("Order details page not displayed on tapping the 'Place Order' button");
						}						
					}
					else
					{
						Fail("Place order button not displayed");
					}					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1629 Issue ." + e.getMessage());
					Exception(" HBC - 1629	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1630 Register Checkout - Verify that "Order Confirmation" page should be as per the creative*/
		public void OrderConfirmationCreative()
		{
			ChildCreation(" HBC - 1630  Register Checkout -  Verify that 'Order Confirmation' page should be as per the creative");
			orderConfirm = true;
			if(orderConfirm==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(confirmOrderPage))
					{
						Pass("Order details page displayed");
						if(HBCBasicfeature.isElementPresent(confirmThanksSection))
						{
							Pass("Order details Thanks for order section displayed");
						}
						else
						{
							Fail("Order details Thanks for order section not displayed");
						}
						if(HBCBasicfeature.isElementPresent(confirmShippingInfoSection))
						{
							Pass("Order details Shipping Info section displayed");
						}
						else
						{
							Fail("Order details Shipping Info section not displayed");
						}
						if(HBCBasicfeature.isElementPresent(confirmPaymentInfoSection))
						{
							Pass("Order details Payment Info section displayed");
						}
						else
						{
							Fail("Order details Payment Info section not displayed");
						}	
						if(HBCBasicfeature.isElementPresent(confirmPaymentDetailSection))
						{
							Pass("Order details Payment Detail section displayed");
						}
						else
						{
							Fail("Order details Payment Detail section not displayed");
						}	
						if(HBCBasicfeature.isElementPresent(confirmRewardDetailSection))
						{
							Pass("Order details Reward Detail section displayed");
						}
						else
						{
							Fail("Order details Reward Detail section not displayed");
						}		
						if(HBCBasicfeature.isElementPresent(confirmsubTotalSection))
						{
							Pass("Order details subTotal section displayed");
						}
						else
						{
							Fail("Order details subTotal section not displayed");
						}		
						if(HBCBasicfeature.isElementPresent(confirmPromotionSection))
						{
							Pass("Order details Promotion section displayed");
						}
						else
						{
							Fail("Order details Promotion section not displayed");
						}		
						if(HBCBasicfeature.isElementPresent(confirmShippingTaxesSection))
						{
							HBCBasicfeature.scrolldown(confirmShippingTaxesSection, driver);
							Pass("Order details ShippingTaxes section displayed");
						}
						else
						{
							Fail("Order details ShippingTaxes section not displayed");
						}	
						if(HBCBasicfeature.isElementPresent(confirmOrderTotalSection))
						{
							Pass("Order Total section displayed");
						}
						else
						{
							Fail("Order Total not displayed");
						}
						/*if(HBCBasicfeature.isElementPresent(signupEmails))
						{
							HBCBasicfeature.scrolldown(signupEmails, driver);
							Pass("SignUp for Emails section displayed");
						}
						else
						{
							Fail("SignUp for Emails section not displayed");
						}	*/
					}
					else
					{
						Fail("Order details page not displayed");
					}					
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1630 Issue ." + e.getMessage());
					Exception(" HBC - 1630	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1631 Register Checkout -  Verify that order number should be shown in the order confirmation page*/
		public void OrderConfirmationOrderNumber()
		{
			ChildCreation(" HBC - 1631  Register Checkout -  Verify that order number should be shown in the order confirmation page");
			if(orderConfirm==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(confirmThanksSectionOrdernumber))
					{
						HBCBasicfeature.scrollup(confirmThanksSectionOrdernumber, driver);
						String number =  confirmThanksSectionOrdernumber.getText();
						log.add("OrderNumber displayed is: "+number);
						Pass("order number shown in the order confirmation page",log);
					}
					else
					{
						Fail("order number not shown in the order confirmation page",log);
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1631 Issue ." + e.getMessage());
					Exception(" HBC - 1631	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1633 Register Checkout -  Verify that shipping address are displayed in the order confirmation page*/
		public void OrderConfirmationShippingAddress()
		{
			ChildCreation(" HBC - 1633  Register Checkout -  Verify that shipping address are displayed in the order confirmation page");
			if(orderConfirm==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(confirmShippingInfoSection))
					{
						HBCBasicfeature.scrollup(confirmShippingInfoSection, driver);
						String shipping =  confirmShippingInfoSection.getText();
						log.add("shipping address displayed is: "+shipping);
						Pass("Shipping address shown in the order confirmation page",log);
					}
					else
					{
						Fail("Shipping address not shown in the order confirmation page",log);
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1633 Issue ." + e.getMessage());
					Exception(" HBC - 1633	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		/* HBC - 1635 Register Checkout -  Verify that payment details  displayed in the order confirmation page*/
		public void OrderConfirmationPaymentdetails()
		{
			ChildCreation(" HBC - 1635  Register Checkout -  Verify that payment details displayed in the order confirmation page");
			if(orderConfirm==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(confirmPaymentDetailSection))
					{
						HBCBasicfeature.scrollup(confirmPaymentDetailSection, driver);
						String shipping =  confirmPaymentDetailSection.getText();
						log.add("Payment details displayed is: "+shipping);
						Pass("Payment details shown in the order confirmation page",log);
					}
					else
					{
						Fail("Payment details not shown in the order confirmation page",log);
					}
				}
				catch(Exception e)
				{
					System.out.println(" HBC - 1635 Issue ." + e.getMessage());
					Exception(" HBC - 1635	 Issue ." + e.getMessage());
				}
			}
			else
			{
				Skip ( " The User was unable to select the address from the verification page in the checkout page.");
			}
		}
		
		
		
	}

