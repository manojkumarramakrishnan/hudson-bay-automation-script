package hbPageObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_menu;

public class Hudson_Bay_menu_obj extends Hudson_Bay_menu implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_menu_obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver; 
		this.sheet = sheet;
		wait = new WebDriverWait(driver, 60);
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(xpath = "//*[@class='sk_shop sk_shop']")
	WebElement hamburger;
	
	@FindBy(xpath = "//*[@class='sk_mobContainerMask showPanCakeMask']")
	WebElement pancakeMask;
	
	@FindBy(xpath = "//*[@class='sk_mobPanCakeBrowseMenu']")
	WebElement pancakeMenu;
	

	@FindBy(xpath = "//*[@class='productlistTab sk_sideFilter sk_sideFilterEnabled']")
	WebElement plpPage;	

	@FindBy(xpath = "//*[contains(@class,'sk_menuOpen ')]//*[@class='sk_mobCategoryItemLink']")
	WebElement catBack;
	
	
	@FindBy(how = How.XPATH,using = "(//*[@class='sk_mobCategoryItem '])")
	List<WebElement> pancakeMenulist;
	
	@FindBy(xpath = "//*[@class='sk_backBtn open']")
	WebElement pancakemenuBackButton;
	
	public void pancakeTap()
	{
		ChildCreation("HUDSONBAY-41 Verify that while selecting the pancake icon,the pancake menu should render smoothly");
		try
		{
			Actions action = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(hamburger));			
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				action.moveToElement(hamburger).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(pancakeMask));			
				if(HBCBasicfeature.isElementPresent(pancakeMenu))
				{
					Pass("Tapping Pancake Icon pancake menu displayed");
				}
				else
				{
					Fail("Tapping Pancake Icon pancake menu not displayed");
				}						
			}
			else
			{
				Fail("Pancake icon not displayed in the header");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}		
	}	

		
	/* Random category / Sub category selection*/
	public void menurandom()
		{
			ChildCreation("menurandom");
			ArrayList<Integer> categoryId = new ArrayList
					<>();	
			categoryId.addAll(Arrays.asList(1, 2, 3, 4, 5));
			boolean subcatpresent = false;
			do
			{
				try
				{									
					String menuName ="";
					String CategoryName ="";
					String SubCategoryName ="";
					int size = pancakeMenulist.size();
					System.out.println("The Pancake menu size is "+size);
					for(int i =1;i<size;i++)
					{						
						try
						{			 	
							WebElement menu = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
							Thread.sleep(1000);
							menuName = menu.getText();
							Thread.sleep(1000);
							System.out.println("The selected Menu is: "+menuName);				
							WebElement category =  driver.findElement(By.xpath("//*[@id='sk_mobSublevel_"+i+"_0']//*[@level='level_1']"));
			 				if(HBCBasicfeature.isElementPresent(category))
							{
								menu.click();									
								Thread.sleep(2000);
								
								/*********************Category selection*********************/

								List<WebElement> categorySize = driver.findElements(By.xpath("//*[@id='sk_mobSublevel_"+i+"_0'][@style='display: block;']//*[@id='sk_mobCategoryItem_id__1']"));
								Thread.sleep(1000);
								int CatSize = categorySize.size();		
								System.out.println("The Pancake menu " +menuName+" category size is: "+CatSize);	
								
									for(int j=1	;j<=CatSize;j++)
									{							
										WebElement Category = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"]"));
										Thread.sleep(1000);
										CategoryName = Category.getText();
										try
										{	
											WebElement subCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']"));
											if(HBCBasicfeature.isElementPresent(subCategory))
											{												
												Category.click();	
												System.out.println("The selected category " +CategoryName+" has sub category");
												
												/*********************Sub Category selection*********************/

												List<WebElement> subcategorySize = driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']"));
												Thread.sleep(1000);
												int subCatSize = subcategorySize.size();				
												System.out.println("The Pancake menu " +menuName+"'s category "+CategoryName+" has subcategory size is: "+subCatSize);	
												Thread.sleep(1000);	
												
												for(int k=1	;k<=subCatSize;k++)
												{	
													WebElement SubCategory = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']["+k+"]"));
													Thread.sleep(1000);
													SubCategoryName = SubCategory.getText();
													try
													{	
														WebElement subsubCategory =  driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"])//*[@id='sk_mobCategoryItem_id__2']["+k+"]//*[@id='sk_mobSublevel__2']"));
														if(HBCBasicfeature.isElementPresent(subsubCategory))
														{
															SubCategory.click();
															System.out.println("The selected sub category " +SubCategoryName+" has sub sub category");
														}
													}
													catch(Exception e)
													{
														System.out.println("The selected sub category "+SubCategoryName+" has no sub sub category");	
														SubCategory.click();
														wait.until(ExpectedConditions.visibilityOf(plpPage));
														if(HBCBasicfeature.isElementPresent(plpPage))
														{
															System.out.println("The selected sub category "+SubCategoryName+" plp page displayed");
															hamburger.click();
															menu = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
															menu.click();
															Thread.sleep(1000);
															Category = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobCategoryItem_id__1']["+j+"]"));
															Category.click();
															continue;
														}
														else
														{
															String url = driver.getCurrentUrl();
															System.out.println(url);
															continue;
														}			
													}													
												}
												catBack.click();										
											}
										}
										catch(Exception e)
										{
											System.out.println("The selected category "+CategoryName+" has no sub category");	
											Category.click();	
											wait.until(ExpectedConditions.visibilityOf(plpPage));
											if(HBCBasicfeature.isElementPresent(plpPage))
											{
												System.out.println("The selected category "+CategoryName+" plp page displayed");
												hamburger.click();
												menu = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']"));
												menu.click();
												continue;
											}
											else
											{
												String url = driver.getCurrentUrl();
												System.out.println(url);
												continue;
											}			
										}											
									} 						
								}
							}
						catch(Exception e)
						{
							System.out.println("The selected menu "+menuName+" has no category");
							continue;						
						}	
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			while(!subcatpresent==true);	
		}
	}

