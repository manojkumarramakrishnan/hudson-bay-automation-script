package hbPageObjects;

import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_MyProfile;

public class Hudson_Bay_MyProfile_Obj extends Hudson_Bay_MyProfile implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_MyProfile_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+homepagee+"")
	WebElement homepage;	
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;	
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+pancakeStaticWelcomee+"")
	WebElement pancakeStaticWelcome;	
	
	@FindBy(xpath = ""+signInPagee+"")
	WebElement signInPage;
	
	@FindBy(xpath = ""+emailFieldd+"")
	WebElement emailField;
	
	@FindBy(xpath = ""+pwdFieldd+"")
	WebElement pwdField;
	
	@FindBy(xpath = ""+signInButtonn+"")
	WebElement signInButton;
	
	@FindBy(xpath = ""+myAccountPagee+"")
	WebElement myAccountPage;
	
	@FindBy(xpath = ""+myAccountPageMyProfileTxtt+"")
	WebElement myAccountPageMyProfileTxt;
	
	@FindBy(xpath = ""+myAccountPageMyProfileDetailss+"")
	WebElement myAccountPageMyProfileDetails;
	
	@FindBy(xpath = ""+myAccountPageMyProfilee+"")
	WebElement myAccountPageMyProfile;
	
	@FindBy(xpath = ""+myAccountMyProfilepagee+"")
	WebElement myAccountMyProfilepage;
	
	@FindBy(xpath = ""+myAccountpageBackk+"")
	WebElement myAccountpageBack;	
	
	@FindBy(xpath = ""+myPageFnamee+"")
	WebElement myPageFname ;
	
	@FindBy(xpath = ""+myPageLnamee+"")
	WebElement myPageLname;
	
	@FindBy(xpath = ""+myPageEmaill+"")
	WebElement myPageEmail;
	
	@FindBy(xpath = ""+myPageEmailAlertt+"")
	WebElement myPageEmailAlert;	
	
	@FindBy(xpath = ""+myPagePasswordd+"")
	WebElement myPagePassword;
	
	@FindBy(xpath = ""+myPageVerifyPasswordd+"")
	WebElement myPageVerifyPassword;
	
	@FindBy(xpath = ""+myPageRewardPointIdd+"")
	WebElement myPageRewardPointId;
	
	@FindBy(xpath = ""+myPageRewardCardNumm+"")
	WebElement myPageRewardCardNum;
	
	@FindBy(xpath = ""+myPageStAddress+"")
	WebElement myPageStAddres;
	
	@FindBy(xpath = ""+myPage2StAddress+"")
	WebElement myPage2StAddres;
	
	@FindBy(xpath = ""+myPageCityy+"")
	WebElement myPageCity;
	
	@FindBy(xpath = ""+myPageCountryy+"")
	WebElement myPageCountry;
	
	@FindBy(xpath = ""+myPageStatee+"")
	WebElement myPageState;	
	
	@FindBy(xpath = ""+myPageZipcodee+"")
	WebElement myPageZipcode;	
	
	@FindBy(xpath = ""+myPagePhoneFieldd+"")
	WebElement myPagePhoneField;
	
	@FindBy(xpath = ""+myPagePhoneExtt+"")
	WebElement myPagePhoneExt ;
	
	@FindBy(xpath = ""+myPagePrefLangg+"")
	WebElement myPagePrefLang;
	
	@FindBy(xpath = ""+myPageBirthmonthh+"")
	WebElement myPageBirthmonth;
	
	@FindBy(xpath = ""+myPageBirthDatee+"")
	WebElement myPageBirthDate;
	
	@FindBy(xpath = ""+myPageGenderFemalee+"")
	WebElement myPageGenderFemale;
	
	@FindBy(xpath = ""+myPageGenderMalee+"")
	WebElement myPageGenderMale;
	
	@FindBy(xpath = ""+myPagePrivacyy+"")
	WebElement myPagePrivacy;
		
	@FindBy(xpath = ""+myPageUpdatee+"")
	WebElement myPageUpdate;
	
	@FindBy(xpath = ""+myPageFnameAlertt+"")
	WebElement myPageFnameAlert;
	
	@FindBy(xpath = ""+myPageLnameAlertt+"")
	WebElement myPageLnameAlert;
	
	@FindBy(xpath = ""+myPagePasswordAlertt+"")
	WebElement myPagePasswordAlert;
	
	@FindBy(xpath = ""+myPageVerifyPasswordAlertt+"")
	WebElement myPageVerifyPasswordAlert;
	
	@FindBy(xpath = ""+myPageRewardAlertt+"")
	WebElement myPageRewardAlert;	
	
	@FindBy(xpath = ""+myPageStAddressAlertt+"")
	WebElement myPageStAddressAlert;
	
	@FindBy(xpath = ""+myPageStCityAlertt+"")
	WebElement myPageStCityAlert;
	
	@FindBy(xpath = ""+myPageZipAlertt+"")
	WebElement myPageZipAlert;
	
	@FindBy(xpath = ""+myPagePhoneAlertt+"")
	WebElement myPagePhoneAlert;

	@FindBy(xpath = ""+addressVerificationoverlayy+"")
	WebElement addressVerificationoverlay;
	
	@FindBy(xpath = ""+addressuseasEnteredd+"")
	WebElement addressuseasEntered;
	
	
	boolean MyProfile = false;
	
	/*SignIn page to MyAccount-MyProfile Page*/
	public void signin() throws java.lang.Exception
	{
		ChildCreation("SignIn page to MyAccount-MyProfile Page");
		String email = HBCBasicfeature.getExcelVal("login", sheet, 6);
		String pwd = HBCBasicfeature.getExcelVal("login", sheet, 1);
		try
		{
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{					
					try
					{						
						do
						{
							try
							{
								HBCBasicfeature.jsclick(hamburger, driver);
								Thread.sleep(2000);
								if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
								{
									flag = true;
									break;
								}
							}
							catch(Exception e)
							{
								continue;
							}
						}
						while(flag!=true);		
						if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
						{
							flag = false;
							HBCBasicfeature.scrolldown(pancakeStaticWelcome, driver);
							pancakeStaticWelcome.click();
							try
							{
								wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
								flag = true;
								break;
							}
							catch(Exception e)
							{
								flag = false;
								continue;
							}
						}
						else
						{
							continue;
						}
					}
					catch(Exception e)
					{
						driver.navigate().refresh();
						continue;
					}
				}
				while(flag!=true);	
				wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
				if(HBCBasicfeature.isElementPresent(emailField))
				{	
					emailField.sendKeys(email);								
					log.add("Entered Email ID is: "+ email);
					if(HBCBasicfeature.isElementPresent(pwdField))
					{	
						pwdField.sendKeys(pwd);									
						log.add("Entered Password is: "+ pwd);
						if(HBCBasicfeature.isElementPresent(signInButton))
						{
							signInButton.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.visibilityOf(myAccountPage));			
							if(HBCBasicfeature.isElementPresent(myAccountPage))
							{	
								Pass("MyAccount Page displayed",log);
								if(HBCBasicfeature.isElementPresent(myAccountPageMyProfile))
								{	
									
									Pass("MyProfile section displayed");
									HBCBasicfeature.jsclick(myAccountPageMyProfile, driver);
									Thread.sleep(500);
									wait.until(ExpectedConditions.visibilityOf(myAccountMyProfilepage));
									if(HBCBasicfeature.isElementPresent(myAccountMyProfilepage))
									{
										Pass("MyProfile page displayed");
										MyProfile = true;
									}
									else
									{
										Fail("Myprofile page not dispalyed");
									}
								}
								else
								{
									Fail("MyProfile section displayed");
								}								
							}
							else
							{
								Fail("MyAccount page not displayed",log);
							}
						}
						else
						{
							Fail("SignIn button not displayed");
						}
					}
					else
					{
						Fail("Password Field not displayed");
					}
				}
				else
				{
					Fail("Email Field not displayed");
				}
			}
			else
			{
				Fail("Hamburger menuu not displayed");
			}				
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-1975 Verify that "My Profile" page should be displayed as per the creative*/
	/*HUDSONBAY-1972 Verify that 'My Profile' page should display 'First Name', 'Last Name' and so on*/
	public void MyProfilepageCreative(String tcID) 
	{
		if(MyProfile == true)
		{
			if(tcID=="HBC1975")
			{
				ChildCreation("HUDSONBAY-1975 Verify that 'My Profile' page should be displayed as per the creative");
			}
			else
			{
				ChildCreation("HUDSONBAY-1972 Verify that 'My Profile' page should display 'First Name', 'Last Name' and so on");
			}
			try
			{				
				if(HBCBasicfeature.isElementPresent(myAccountMyProfilepage))
				{
					Pass("MyProfile Page displayed");
					if(HBCBasicfeature.isElementPresent(myPageFname))
					{
						Pass("FirstName field is displayed");
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageLname))
					{
						Pass("LastName field is displayed");
					}
					else
					{
						Fail("LastName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageEmail))
					{
						Pass("Email field is displayed");
					}
					else
					{
						Fail("Email field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPagePassword))
					{
						Pass("Password field is displayed");
					}
					else
					{
						Fail("Password field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageVerifyPassword))
					{
						Pass("Verify Password field is displayed");
					}
					else
					{
						Fail("Verify Password field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageRewardPointId))
					{
						Pass("RewardID field is displayed");
					}
					else
					{
						Fail("RewardID field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageRewardCardNum))
					{
						Pass("Reward Number field is displayed");
					}
					else
					{
						Fail("Reward Number field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageStAddres))
					{
						Pass("Street Address field 1 is displayed");
					}
					else
					{
						Fail("Street Address field 1 is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPage2StAddres))
					{
						Pass("Street Address field 2 is displayed");
					}
					else
					{
						Fail("Street Address field 2 is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageCity))
					{
						HBCBasicfeature.scrolldown(myPageCity, driver);
						Pass("City field is displayed");
					}
					else
					{
						Fail("City field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageCountry))
					{
						HBCBasicfeature.scrolldown(myPageCountry, driver);
						Pass("Country dropdown field is displayed");						
					}
					else
					{
						Fail("Country dropdown field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						HBCBasicfeature.scrolldown(myPageState, driver);
						Pass("State/province dropdown field is displayed");
					}
					else
					{
						Fail("State/province dropdown field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						HBCBasicfeature.scrolldown(myPageZipcode, driver);
						Pass("PostalCode field is displayed");
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPagePhoneField)&&HBCBasicfeature.isElementPresent(myPagePhoneExt))
					{
						HBCBasicfeature.scrolldown(myPagePhoneField, driver);
						Pass("Phone field & extension field is displayed");
					}
					else
					{
						Fail("Phone field & extension field is not displayed");
					}					
					
					if(HBCBasicfeature.isElementPresent(myPagePrefLang))
					{
						Pass("Preferrred language field is displayed");
					}
					else
					{
						Fail("Preferrred language field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageBirthmonth)&&HBCBasicfeature.isElementPresent(myPageBirthDate))
					{
						Pass("Birth Month & Date field is displayed");
					}
					else
					{
						Fail("Birth Month & Date field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageGenderFemale)&&HBCBasicfeature.isElementPresent(myPageGenderMale))
					{
						HBCBasicfeature.scrolldown(footerContainer, driver);
						Pass("Gender Male & female radio button is displayed");
					}
					else
					{
						Fail("Gender Male & female radio button is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPagePrivacy))
					{
						Pass("Privacy Policy field is displayed");
					}
					else
					{
						Fail("Privacy Policy field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageUpdate))
					{
						Pass("Update Button is displayed");
					}
					else
					{
						Fail("Update Button is not displayed");
					}
					/*if(HBCBasicfeature.isElementPresent(signupEmails))
					{
						Pass("SignUp for emails button is displayed");
					}
					else
					{
						Fail("SignUp for emails Button is not displayed");
					}*/
					if(HBCBasicfeature.isElementPresent(footerContainer))
					{
						Pass("Footer container is displayed");
					}
					else
					{
						Fail("Footer container is not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(myAccountpageBack))
					{
						HBCBasicfeature.scrollup(myAccountpageBack, driver);
						Pass("'< Account'link is displayed");
					}
					else
					{
						Fail("'< Account'link is not displayed");
					}			
				}
				else
				{
					Fail("MyProfile page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1975" +e.getMessage());
				Exception("HBC-1975 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-2005 Verify that "< Account" link should be shown in the left side of the page and it should be highlighted in blue color as per the creative*/
	public void MyProfileBackArrow() 
	{
		ChildCreation("HUDSONBAY-2005 Verify that '< Account' link should be shown in the left side of the page and it should be highlighted in blue color as per the creative");
		if(MyProfile == true)
		{
			try
			{
				String expectedTxt = HBCBasicfeature.getExcelVal("HBC2005", sheet, 1);
				String expectedColor = HBCBasicfeature.getExcelVal("HBC2005", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					Pass("'< Account'link is displayed");	
					String actualTxt = myAccountpageBack.getText();
					String Color = myAccountpageBack.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(Color);
					log.add("The Actual text is: "+actualTxt);
					log.add("The Expected text is: "+expectedTxt);
					log.add("The Actual Color is: "+actualColor);
					log.add("The Expected Color is: "+expectedColor);					
					if(actualTxt.equalsIgnoreCase(expectedTxt)&&actualColor.equalsIgnoreCase(expectedColor))
					{
						Pass("'< Account' link shown in the left side of the page and it highlighted in blue color as per the creative",log);
					}
					else
					{
						Fail("'< Account' link not shown in the left side of the page and not as per the creative",log);
					}				
				}
				else
				{
					Fail("'< Account' link not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2005" +e.getMessage());
				Exception("HBC-2005 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-2006 Verify that on tapping "< Account" link, it should navigates to "My Account" page*/
	public void MyProfileBackArrowClick() 
	{
		ChildCreation("HUDSONBAY-2006 Verify that on tapping '< Account' link, it should navigates to 'My Account' page");
		if(MyProfile == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					HBCBasicfeature.jsclick(myAccountpageBack, driver);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileTxt))
					{
						Pass("on tapping '< Account' link, navigates to 'My Account' page");
						MyProfile=false;					}
					else
					{
						Fail("on tapping '< Account' link, not navigates to 'My Account' page");
					}					
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfile))
					{						
						HBCBasicfeature.jsclick(myAccountPageMyProfile, driver);									
						wait.until(ExpectedConditions.visibilityOf(myAccountMyProfilepage));
						if(HBCBasicfeature.isElementPresent(myAccountMyProfilepage))
						{
							Pass("Navigated back to MyProfile page ");
							MyProfile = true;
						}
						else
						{
							Fail("Myprofile page not dispalyed");
						}
					}
					else
					{
						Fail("MyProfile section displayed");
					}
				}
				else
				{
					Fail("'< Account' link not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2006" +e.getMessage());
				Exception("HBC-2006 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1976 Verify that alert should be shown when the user leaves any field without filling the details*/
	/*HUDSONBAY-1977 Verify that corresponding alert message needs to be shown on navigating through the tabs or on tappping the "Submit" button*/	
	public void MyProfileEmptyValidation() 
	{
		ChildCreation("HUDSONBAY-1976 Verify that alert should be shown when the user leaves any field without filling the details");
		if(MyProfile == true)
		{
			boolean HBC1977 = false;
			try
			{
				String[] expectedAlert = HBCBasicfeature.getExcelVal("HBC1976", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(myAccountMyProfilepage))
				{
					Pass("MyProfile Page displayed");
					if(HBCBasicfeature.isElementPresent(myPageFname))
					{
						Pass("FirstName field is displayed");
						myPageFname.clear();
						myPageLname.click();
						wait.until(ExpectedConditions.visibilityOf(myPageFnameAlert));
						if(HBCBasicfeature.isElementPresent(myPageFnameAlert))
						{
							String actualAlert = myPageFnameAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[0]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[0]))
							{
								Pass("Alert shown when the user leaves Firstname field without filling the details",log);
								HBC1977 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Firstname field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when FirstName field is empty ");
						}						
					}
					else
					{
						Fail("FirstName field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageLname))
					{
						Pass("LastName field is displayed");
						myPageLname.clear();
						myPageFname.click();
						wait.until(ExpectedConditions.visibilityOf(myPageLnameAlert));
						if(HBCBasicfeature.isElementPresent(myPageLnameAlert))
						{
							String actualAlert = myPageLnameAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[1]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[1]))
							{
								Pass("Alert shown when the user leaves Lastname field without filling the details",log);
								HBC1977 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Lastname field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Lastname field is empty ");
						}						
					}
					else
					{
						Fail("LastName field is not displayed");
					}			
					if(HBCBasicfeature.isElementPresent(myPagePassword))
					{
						Pass("Password field is displayed");
						myPagePassword.clear();
						myPageLname.click();
						wait.until(ExpectedConditions.visibilityOf(myPagePasswordAlert));
						if(HBCBasicfeature.isElementPresent(myPagePasswordAlert))
						{
							String actualAlert = myPagePasswordAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[2]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[2]))
							{
								Pass("Alert shown when the user leaves Password field without filling the details",log);
								HBC1977 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Password field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Password field is empty ");
						}						
					}
					else
					{
						Fail("Password field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageVerifyPassword))
					{
						Pass("Verify Password field is displayed");
						myPageVerifyPassword.clear();
						myPagePassword.click();
						wait.until(ExpectedConditions.visibilityOf(myPageVerifyPasswordAlert));
						if(HBCBasicfeature.isElementPresent(myPageVerifyPasswordAlert))
						{
							String actualAlert = myPageVerifyPasswordAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[3]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[3]))
							{
								Pass("Alert shown when the user leaves Verify Password field without filling the details",log);
								HBC1977 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Verify Password field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Verify Password field is empty ");
						}						
					}
					else
					{
						Fail("Verify Password field is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myPageStAddres))
					{
						Pass("Street Address field 1 is displayed");
						myPageStAddres.clear();
						myPagePassword.click();
						wait.until(ExpectedConditions.visibilityOf(myPageStAddressAlert));
						if(HBCBasicfeature.isElementPresent(myPageStAddressAlert))
						{
							String actualAlert = myPageStAddressAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[4]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[4]))
							{
								Pass("Alert shown when the user leaves Street Address field without filling the details",log);
								HBC1977 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Street Address field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Street Address field is empty ");
						}						
					}
					else
					{
						Fail("Street Address field 1 is not displayed");
					}					
					if(HBCBasicfeature.isElementPresent(myPageCity))
					{
						HBCBasicfeature.scrolldown(myPageCity, driver);
						Pass("City field is displayed");
						myPageCity.clear();
						myPageStAddres.click();
						wait.until(ExpectedConditions.visibilityOf(myPageStCityAlert));
						if(HBCBasicfeature.isElementPresent(myPageStCityAlert))
						{
							String actualAlert = myPageStCityAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[5]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[5]))
							{
								Pass("Alert shown when the user leaves City field without filling the details",log);
								HBC1977 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves City field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when City field is empty ");
						}						
					}
					else
					{
						Fail("City field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						HBCBasicfeature.scrolldown(myPageZipcode, driver);
						Pass("PostalCode field is displayed");
						myPageZipcode.clear();
						myPageCity.click();
						wait.until(ExpectedConditions.visibilityOf(myPageZipAlert));
						if(HBCBasicfeature.isElementPresent(myPageZipAlert))
						{
							String actualAlert = myPageZipAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[6]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[6]))
							{
								Pass("Alert shown when the user leaves PostalCode field without filling the details",log);
								HBC1977 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves PostalCode field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when PostalCode field is empty ");
						}						
					}
					else
					{
						Fail("PostalCode field is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myPagePhoneField))
					{
						HBCBasicfeature.scrolldown(myPagePhoneField, driver);
						Pass("Phone field is displayed");
						myPagePhoneField.clear();
						myPageZipcode.click();
						wait.until(ExpectedConditions.visibilityOf(myPagePhoneAlert));
						if(HBCBasicfeature.isElementPresent(myPagePhoneAlert))
						{
							String actualAlert = myPagePhoneAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[7]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[7]))
							{
								Pass("Alert shown when the user leaves Phone field without filling the details",log);
								HBC1977 = true;
							}
							else
							{
								Fail("Alert not matches when the user leaves Phone field without filling the details",log);
							}
						}
						else
						{
							Fail("Alert not displayed when Phone field is empty ");
						}						
					}
					else
					{
						Fail("Phone field is not displayed");
					}				
				}
				else
				{
					Fail("MyProfile page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1976" +e.getMessage());
				Exception("HBC-1976 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-1977 Verify that corresponding alert message needs to be shown on navigating through the tabs or on tappping the 'update' button");
			if(HBC1977 == true)
			{
				Pass("corresponding alert message shown on navigating through the tabs or on tappping the 'update' button");
			}
			else
			{
				Fail("corresponding alert message not shown on navigating through the tabs or on tappping the 'update' button");
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1978 Verify that only alphabets should be accepted in "First Name" and "Last Name" field*/
	public void MyProfileFirstLastNameCharValidation() 
	{
		ChildCreation("HUDSONBAY-1978 Verify that only alphabets should be accepted in 'First Name' and 'Last Name' field");
		if(MyProfile == true)
		{
			try
			{
				String fname = HBCBasicfeature.getExcelVal("HBC1978", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageFname))
				{
					Pass("FirstName field is displayed");
					myPageFname.clear();
					log.add( "The Entered value from the excel file is : " + fname);
					myPageFname.sendKeys(fname);
					if(myPageFname.getAttribute("value").isEmpty())
					{
						Fail("The First Name is empty");
					}
					else if(myPageFname.getAttribute("value").equalsIgnoreCase(fname))
					{
						Pass("Only alphabets accepted in 'First Name' field",log);
					}
					else
					{
						Fail("Alphabets not accepted in 'First Name' field",log);
					}
					myPageFname.clear();
				}
				else
				{
					Fail("FirstName field is not displayed");
				}
				
				String lname = HBCBasicfeature.getExcelVal("HBC1978", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myPageLname))
				{
					Pass("Last Name field is displayed");
					myPageLname.clear();
					log.add( "The Entered value from the excel file is : " + lname);
					myPageLname.sendKeys(lname);
					if(myPageLname.getAttribute("value").isEmpty())
					{
						Fail("The Last Name field is empty");
					}
					else if(myPageLname.getAttribute("value").equalsIgnoreCase(lname))
					{
						Pass("Only alphabets accepted in 'Last Name' field",log);
					}
					else
					{
						Fail("Alphabets not accepted in 'Last Name' field",log);
					}
					myPageLname.clear();
				}
				else
				{
					Fail("Last Name field is not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1978" +e.getMessage());
				Exception("HBC-1978 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
		
	/*HUDSONBAY-1979 Verify that "First Name" and "Last Name" field should accept numbers and special characters and the corresponding alert message should be shown*/
	public void MypageFirstLastNameValidationAlert() 
	{
		ChildCreation("HUDSONBAY-1979 Verify that 'First Name' and 'Last Name' field should accept numbers and special characters and the corresponding alert message should be shown");
		if(MyProfile == true)
		{
			try
			{
				String[] expectedAlert = HBCBasicfeature.getExcelVal("HBC1979", sheet, 2).split("\n");
				
				String fname = HBCBasicfeature.getExcelVal("HBC1979", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageFname))
				{
					Pass("FirstName field is displayed");
					myPageFname.clear();
					log.add( "The Entered value from the excel file is : " + fname);
					myPageFname.sendKeys(fname);
					if(myPageFname.getAttribute("value").isEmpty())
					{
						Fail("The First Name field is empty and not accept numbers and special characters",log);
					}
					else 
					{
						Pass("Accepts numbers and special characters in 'First Name' field",log);
						myPageLname.click();
						wait.until(ExpectedConditions.visibilityOf(myPageFnameAlert));
						if(HBCBasicfeature.isElementPresent(myPageFnameAlert))
						{
							String actualAlert = myPageFnameAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[0]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[0]))
							{
								Pass("Alert shown when numbers and special characters are entered in Firstname",log);
							}
							else
							{
								Fail("Alert not shown when numbers and special characters are entered in Firstname",log);
							}
						}
						else
						{
							Fail("Alert not displayed");
						}					
					}
				}
				else
				{
					Fail("First Name field is not displayed");
				}
				
				String lname = HBCBasicfeature.getExcelVal("HBC1979", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageLname))
				{
					Pass("Last Name field is displayed");
					myPageLname.clear();
					log.add( "The Entered value from the excel file is : " + lname);
					myPageLname.sendKeys(lname);
					if(myPageLname.getAttribute("value").isEmpty())
					{
						Fail("The Last Name field is empty and not accept numbers and special characters",log);
					}
					else 
					{
						Pass("Accepts numbers and special characters in 'Last Name' field",log);
						myPageFname.click();
						wait.until(ExpectedConditions.visibilityOf(myPageLnameAlert));
						if(HBCBasicfeature.isElementPresent(myPageLnameAlert))
						{
							String actualAlert = myPageLnameAlert.getText();
							log.add("Actual alert is: "+actualAlert);
							log.add("Expected alert is: "+expectedAlert[1]);
							if(actualAlert.equalsIgnoreCase(expectedAlert[1]))
							{
								Pass("Alert shown when numbers and special characters are entered in Last name",log);
							}
							else
							{
								Fail("Alert not shown when numbers and special characters are entered in Last name",log);
							}
						}
						else
						{
							Fail("Alert not displayed");
						}					
					}
				}
				else
				{
					Fail("Last Name field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1979" +e.getMessage());
				Exception("HBC-1979 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1980 Verify that "Email" field should be not be editable and by default registered eail should be shown*/
	public void MypageEmaildefault() 
	{
		ChildCreation("HUDSONBAY-1980 Verify that 'Email' field should be not be editable and by default registered email should be shown");
		if(MyProfile == true)
		{
			try
			{
				String email = HBCBasicfeature.getExcelVal("login", sheet, 6);
				if(HBCBasicfeature.isElementPresent(myPageEmail))
				{
					Pass("Email field is displayed");
					try
					{
						myPageEmail.clear();
						Fail("'Email' field is editable");
					}
					catch (Exception e)
					{
						Pass("'Email' field is not editable");						
					}	
					if(myPageEmail.getAttribute("value").equalsIgnoreCase(email))
					{
						Pass("Default registered email displayed");
					}
					else
					{
						Fail("Default registered email not displayed");
					}
				}
				else
				{
					Fail("Email field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1980" +e.getMessage());
				Exception("HBC-1980 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1981 Verify that "Please note that if you change your password..." text should be shown below the Email field*/
	public void MypageEmaildefaultAlert() 
	{
		ChildCreation("HUDSONBAY-1981 Verify that 'Please note that if you change your password...' text should be shown below the Email field");
		if(MyProfile == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC1981", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageEmailAlert))
				{
					Pass("Email Alert text is displayed");
					String actual = myPageEmailAlert.getText();
					log.add("Actual text is: "+actual);
					log.add("Expected text is: "+expected);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("'Please note that if you change your password...' text shown below the Email field",log);
					}
					else
					{
						Fail("'Please note that if you change your password...' text not shown below the Email field",log);
					}
				}
				else
				{
					Fail("Email Alert is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1981" +e.getMessage());
				Exception("HBC-1981 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1982 Verify that "Password" field should accept alphanumerics & special characters.*/
	public void MypagePasswordField() 
	{
		ChildCreation("HUDSONBAY-1982 Verify that 'Password' field should accept alphanumerics & special characters.");
		if(MyProfile == true)
		{
			try
			{
				String password = HBCBasicfeature.getExcelNumericVal("login", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPagePassword))
				{
					Pass( "The Password Field is displayed.");
					myPagePassword.clear();
					log.add( "The Entered value from the excel file is : " + password);
					myPagePassword.sendKeys(password);
					String pwdVal = myPagePassword.getAttribute("value");					
					if(pwdVal.isEmpty())
					{
						Fail( "The Password field is empty.",log);
					}
					else if(password.equalsIgnoreCase(pwdVal))
					{
						Pass( "The Password Field was enterable and accepts alphanumerics & special characters.",log);
					}
					else
					{
						Fail( "The Password Field  was enterable and it does not matches.",log);
					}
					myPagePassword.clear();
				}
				else
				{
					Fail( "The Password Field is not displayed.");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1982" +e.getMessage());
				Exception("HBC-1982 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1983 Verify that password field should conatin one Special character, lower case, upper case, */
	public void MypagePasswordFormat() 
	{
		ChildCreation("HUDSONBAY-1983 Verify that password field should conatin one Special character, lower case, upper case, ");
		if(MyProfile == true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1983", sheet, 1).split("\n");
				String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1983", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myPagePassword))
				{
					Pass( "The Password Field is displayed.");
					for(int i=0;i<cellVal.length;i++)
					{
						myPagePassword.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						myPagePassword.sendKeys(cellVal[i]);						
						myPageFname.click();
						try
						{
							WebDriverWait w1 = new WebDriverWait(driver,5);
							w1.until(ExpectedConditions.attributeContains(myPagePasswordAlert, "style", "block"));
							Thread.sleep(100);
							if(HBCBasicfeature.isElementPresent(myPagePasswordAlert))
							{
								String Actualalert = myPagePasswordAlert.getText();
								log.add("The Actual alert is: "+Actualalert);
								log.add("The Expected alert is: "+ ExpctdAlert);
								if(Actualalert.equalsIgnoreCase(ExpctdAlert))
								{
									Pass("corresponding alert message shown, when the password not conatin one Special character, lower case, upper case",log);
								}
								else
								{
									Fail("corresponding alert message not shown, when the password not conatin one Special character, lower case, upper case",log);
								}
								myPagePassword.clear();	
							}
							else
							{
								Fail("Alert is not displayed",log);
							}
						}
						catch(Exception e)
						{
							if(!HBCBasicfeature.isElementPresent(myPagePasswordAlert))
							{
								Pass("Password field conatins one Special character, lower case, upper case",log);
								myPagePassword.clear();	
							}
							else
							{
								Fail("Alert displayed when Password field conatins one Special character, lower case, upper case",log);
							}
						}
					}									
				}
				else
				{
					Fail("Password field not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1983" +e.getMessage());
				Exception("HBC-1983 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1984 Verify that password should accept maximum of 146 characters*/
	public void MypagePasswordCharMax() 
	{
		ChildCreation("HUDSONBAY-1984 Verify that password should accept maximum of 146 characters");
		if(MyProfile == true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1984", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(myPagePassword))
				{
					Pass( "The Password field is displayed.");					
					for(int i = 0; i<cellVal.length;i++)
					{
						myPagePassword.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						myPagePassword.sendKeys(cellVal[i]);
						if(myPagePassword.getAttribute("value").isEmpty())
						{
							Fail( "The Password field is empty.");
						}
						else if(myPagePassword.getAttribute("value").length()<=147)
						{
							Pass( "The Password was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Password was enterable but it is not within the specified character limit.",log);
						}
						myPagePassword.clear();
					}
				}
				else
				{
					Fail("Password field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1984" +e.getMessage());
				Exception("HBC-1984 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1985 Verify that alert should be shown when the password in the "Password" field does not match the password in the "Verify Password" field*/
	public void MypagePasswordVerifyPasswordMismatchAlert() 
	{
		ChildCreation("HUDSONBAY-1985 Verify that alert should be shown when the password in the 'Password' field does not match the password in the 'Verify Password' field");
		if(MyProfile == true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1985", sheet, 1).split("\n");
				String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1985", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myPagePassword)&&HBCBasicfeature.isElementPresent(myPageVerifyPassword))
				{
					Pass("Password field & Verify Password field is displayed");
					myPagePassword.clear();
					log.add( "'Password' field : The Entered value from the excel file is : " + cellVal[0].toString());
					myPagePassword.sendKeys(cellVal[0]);
					myPageVerifyPassword.clear();
					log.add( " 'Verify Password' field : The Entered value from the excel file is : " + cellVal[1].toString());
					myPageVerifyPassword.sendKeys(cellVal[1]);
					myPageRewardCardNum.click();
					wait.until(ExpectedConditions.attributeContains(myPageVerifyPasswordAlert, "style", "block"));
					Thread.sleep(100);
					if(HBCBasicfeature.isElementPresent(myPageVerifyPasswordAlert))
					{
						String Actualalert = myPageVerifyPasswordAlert.getText();
						log.add("The Actual alert is: "+Actualalert);
						log.add("The Expected alert is: "+ ExpctdAlert);
						if(Actualalert.equalsIgnoreCase(ExpctdAlert))
						{
							Pass("corresponding alert message shown, when the password field does not match the password in the 'Verify Password' field",log);
						}
						else
						{
							Fail("corresponding alert message not shown, when the password field does not match the password in the 'Verify Password' field",log);
						}
						myPagePassword.clear();
						myPageVerifyPassword.clear();	
					}
					else
					{
						Fail("Alert is not displayed",log);
					}
				}
				else
				{
					Fail("Password field & Verify Password field is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1985" +e.getMessage());
				Exception("HBC-1985 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1986 Verify that user should be able to enter card number in the "HBC rewards card number:" field and it should accept maximum of 9 digits*/
	public void MypageHBCRewardFieldMax() 
	{
		ChildCreation("HUDSONBAY-1986 Verify that user should be able to enter card number in the 'HBC rewards card number:' field and it should accept maximum of 9 digits");
		if(MyProfile == true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1986", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(myPageRewardCardNum))
				{
					Pass( "The 'HBC rewards card number:' field is displayed.");					
					for(int i = 0; i<cellVal.length;i++)
					{
						myPageRewardCardNum.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						myPageRewardCardNum.sendKeys(cellVal[i]);
						if(myPageRewardCardNum.getAttribute("value").isEmpty())
						{
							Fail( "The 'HBC rewards card number:' field is empty.");
						}
						else if(myPageRewardCardNum.getAttribute("value").length()<=9)
						{
							Pass( "The 'HBC rewards card number:' field was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The 'HBC rewards card number:' field was enterable but it is not within the specified character limit.",log);
						}
						myPageRewardCardNum.clear();
					}
				}
				else
				{
					Fail("'HBC rewards card number:' field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1986" +e.getMessage());
				Exception("HBC-1986 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
		
	/*HUDSONBAY-1987 Verify that when the user entered less than 9 digits in the "HBC rewards card number" field, then the corresponding alert message should be shown*/
	public void MypageHBCCardlessNumberAlert() 
	{
		ChildCreation("HUDSONBAY-1987 Verify that when the user entered less than 9 digits in the 'HBC rewards card number' field, then the corresponding alert message should be shown");
		if(MyProfile == true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1986", sheet, 1).split("\n");
				String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1986", sheet, 2);
				if(HBCBasicfeature.isElementPresent(myPageRewardCardNum))
				{
					Pass("Password field & Verify Password field is displayed");
					myPageRewardCardNum.clear();
					log.add(" The Entered value from the excel file is : " + cellVal[0].toString());
					myPageRewardCardNum.sendKeys(cellVal[0]);
					myPageVerifyPassword.click();
					wait.until(ExpectedConditions.attributeContains(myPageRewardAlert, "style", "block"));
					Thread.sleep(100);
					if(HBCBasicfeature.isElementPresent(myPageRewardAlert))
					{
						String Actualalert = myPageRewardAlert.getText();
						log.add("The Actual alert is: "+Actualalert);
						log.add("The Expected alert is: "+ ExpctdAlert);
						if(Actualalert.equalsIgnoreCase(ExpctdAlert))
						{
							Pass("corresponding alert message shown, when the user entered less than 9 digits in the 'HBC rewards card number' field",log);
						}
						else
						{
							Fail("corresponding alert message not shown, when the user entered less than 9 digits in the 'HBC rewards card number' field",log);
						}
						myPageRewardCardNum.clear();
					}
					else
					{
						Fail("Alert is not displayed",log);
					}
				}
				else
				{
					Fail("'HBC rewards card number' field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1987" +e.getMessage());
				Exception("HBC-1987 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1988 Verify that user should be able to enter street address in the "Street Address" field and it should display two text boxes.*/
	public void MypageStreetAddressFields() 
	{
		ChildCreation("HUDSONBAY-1988 Verify that user should be able to enter street address in the 'Street Address' field and it should display two text boxes.");
		if(MyProfile == true)
		{
			try
			{
				String addr = HBCBasicfeature.getExcelVal("HBC1988", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageStAddres)&&HBCBasicfeature.isElementPresent(myPage2StAddres))
				{
					Pass(" 'Street Address' field two text boxes are displayed");
					myPageStAddres.clear();
					log.add( "The Entered value from the excel file is : " + addr.toString());
					myPageStAddres.sendKeys(addr);
					if(myPageStAddres.getAttribute("value").isEmpty())
					{
						Fail( "The 'Street Address' field is empty.");
					}
					else if(myPageStAddres.getAttribute("value").equalsIgnoreCase(addr))
					{
						Pass( "The 'Street Address'  field was enterable",log);
					}
					else
					{
						Fail( "The 'Street Address field was enterable but enetered characters are not displayed.",log);
					}
					myPageStAddres.clear();
					
					myPage2StAddres.clear();
					log.add( "The Entered value from the excel file is : " + addr.toString());
					myPage2StAddres.sendKeys(addr);
					if(myPage2StAddres.getAttribute("value").isEmpty())
					{
						Fail( "The 'Street Address' field 2 is empty.");
					}
					else if(myPage2StAddres.getAttribute("value").equalsIgnoreCase(addr))
					{
						Pass( "The 'Street Address'  field 2 was enterable",log);
					}
					else
					{
						Fail( "The 'Street Address field 2 was enterable but enetered characters are not displayed.",log);
					}
					myPage2StAddres.clear();					
				}
				else
				{
					Fail("'Street Address' field two text boxes are not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1988" +e.getMessage());
				Exception("HBC-1988 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1989 Verify that user should be able to enter characters in the "City" field*/
	public void MypageCityField() 
	{
		ChildCreation("HUDSONBAY-1989 Verify that user should be able to enter characters in the 'City' field");
		if(MyProfile == true)
		{
			try
			{
				String city = HBCBasicfeature.getExcelVal("HBC1989", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageCity))
				{
					HBCBasicfeature.scrolldown(myPageCity, driver);
					Pass("City field is displayed");
					myPageCity.clear();
					log.add( "The Entered value from the excel file is : " + city.toString());
					myPageCity.sendKeys(city);
					if(myPageCity.getAttribute("value").isEmpty())
					{
						Fail( "The 'City' field is empty.");
					}
					else if(myPageCity.getAttribute("value").equalsIgnoreCase(city))
					{
						Pass( "The 'City'  field was enterable",log);
					}
					else
					{
						Fail( "The 'City' field was enterable but enetered characters are not displayed.",log);
					}
					myPageCity.clear();					
				}
				else
				{
					Fail("City field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1989" +e.getMessage());
				Exception("HBC-1989 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1990 Verify that "City" field should accept numbers and special characters and the corresponding alert message should be shown*/
	public void MypageCityFieldAlert() 
	{
		ChildCreation("HUDSONBAY-1990 Verify that 'City' field should accept numbers and special characters and the corresponding alert message should be shown");
		if(MyProfile == true)
		{
			try
			{
				String excelVal = HBCBasicfeature.getExcelVal("HBC1990", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1990", sheet, 2);

				if(HBCBasicfeature.isElementPresent(myPageCity))
				{
					Pass("City field is displayed");
					myPageCity.clear();
					log.add( "The Entered value from the excel file is : " + excelVal.toString());
					myPageCity.sendKeys(excelVal);
					myPage2StAddres.click();
					wait.until(ExpectedConditions.attributeContains(myPageStCityAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(myPageStCityAlert))
					{
						String actualAlert = myPageStCityAlert.getText();
						log.add("Actual text is: "+actualAlert);
						log.add("Expected text is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("'City' field accept numbers and special characters and the corresponding alert message shown",log);
						}
						else
						{
							Fail("'City' field accept numbers and special characters and the corresponding alert message not shown",log);
						}
						myPageCity.clear();
					}
					else
					{
						Fail("City field alert is not displayed");
					}					
				}
				else
				{
					Fail("City field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1990" +e.getMessage());
				Exception("HBC-1990 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1991 Verify that "Country" field should have a maximum of 3 dropdown options.*/
	public void MypageCountryFields() 
	{
		ChildCreation("HUDSONBAY-1991 Verify that 'Country' field should have a maximum of 3 dropdown options.");
		if(MyProfile == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(myPageCountry);
					int cntysize = sel.getOptions().size();
					for( int i = 0; i<cntysize;i++)
					{
						String cnty = sel.getOptions().get(i).getText();
						log.add( "The Displayed country option is  : " + cnty);
					}
					if(cntysize==3)
					{
						Pass( "The Country Default Options is of 3 Sizes.",log);
					}
					else
					{
						Fail( "The Country Default Options is of not the expected 3.The displayed total country sizes are : " + cntysize, log);
					}
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1991" +e.getMessage());
				Exception("HBC-1991 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1992 Verify that "Province" dropdown field should be shown, when the country is selected in Canada.*/
	public void MyProfileProvinceField() 
	{
		ChildCreation("HUDSONBAY-1992 Verify that 'Province' dropdown field should be shown, when the country is selected in Canada.");
		if(MyProfile == true)
		{
			try
			{
				String country = "";
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Select sel = new Select(myPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						Pass("'Province' dropdown field shown, when the country is selected in Canada");
					}
					else
					{
						Fail("'Province' dropdown field not shown, when the country is selected in Canada");
					}
				}
				else
				{
					Fail("country not selected is Canada");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1992" +e.getMessage());
				Exception("HBC-1992 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1993 Verify that "Select" option should be selected by default in the "Providence" dropdown field.*/
	public void MyProfileProvinceFieldSelectDefault() 
	{
		ChildCreation("HUDSONBAY-1993 Verify that 'Select' option should be selected by default in the 'Province' dropdown field");
		if(MyProfile == true)
		{
			try
			{
				String country = "";
				String expVal = HBCBasicfeature.getExcelVal("HBC1993", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Select sel = new Select(myPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						Pass("'Province' dropdown field displayed");
						Select sel = new Select(myPageState);
						sel.getOptions().get(0).click();
						String actualVal= sel.getFirstSelectedOption().getText();
						log.add( "The Expected Value was : " + expVal);
						log.add( "The Actual Value is : " + actualVal);
						if(expVal.equalsIgnoreCase(actualVal))
						{
							Pass( "The Expected Default Value is selected.",log);
						}
						else
						{
							Fail( "The Expected Default Value is not selected.",log);
						}						
					}
					else
					{
						Fail("'Province' dropdown field not shown, when the country is selected in Canada");
					}
				}
				else
				{
					Fail("country not selected is Canada");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1993" +e.getMessage());
				Exception("HBC-1993 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1994 Verify that "Providence" dropdown field should be changed to textbox field when the country is selected in "United Kingdom"*/
	public void  MyProfileProvincetextBoxUK() 
	{
		ChildCreation("HUDSONBAY-1994 Verify that 'Providence' dropdown field should be changed to textbox field when the country is selected in 'United Kingdom'");
		if(MyProfile == true)
		{
			try
			{
				String country = "";
				String expVal = HBCBasicfeature.getExcelVal("HBC1994", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Select sel = new Select(myPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("United Kingdom");
					if(cnty==false)
					{
						sel.selectByValue("GB");						
					}
					wait.until(ExpectedConditions.visibilityOf(myPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("United Kingdom"))
				{
					Pass("country selected is United Kingdom");
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						Pass("'Province' textbox field displayed");
						myPageState.clear();
						log.add( "The Entered value from the excel file is : " + expVal.toString());
						myPageState.sendKeys(expVal);
						Thread.sleep(500);
						if(myPageState.getAttribute("value").isEmpty())
						{
							Fail(" 'Providence' textbox field is empty",log);
						}						
						else if(myPageState.getAttribute("value").equalsIgnoreCase(expVal))
						{
							Pass( "'Providence' dropdown field changed to textbox field when the country is selected in 'United Kingdom",log);
						}
						else
						{
							Fail( "'Providence' dropdown field not changed to textbox field",log);
						}						
					}
					else
					{
						Fail("'Province' dropdown field not shown, when the country is selected in Canada");
					}
				}
				else
				{
					Fail("country not selected is Canada");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1994" +e.getMessage());
				Exception("HBC-1994 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1995 Verify that user should be able to select state from the "Providence" drop down, when the country is selected in "United States".*/
	public void MyProfileProvinceDropDownUS() 
	{
		ChildCreation("HUDSONBAY-1995 Verify that user should be able to select state from the 'Providence' drop down, when the country is selected in 'United States'");
		if(MyProfile == true)
		{
			try
			{
				String country = "";
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Select sel = new Select(myPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("United States");
					if(cnty==false)
					{
						sel.selectByValue("US");						
					}
					wait.until(ExpectedConditions.visibilityOf(myPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("United States"))
				{
					Pass("country selected is United States");
					if(HBCBasicfeature.isElementPresent(myPageState))
					{
						Pass("'Province' dropdown field displayed");
						Random r  = new Random();
						Select sel =  new Select(myPageState);
						int size = sel.getOptions().size();
						int i = r.nextInt(size);
						String state =  sel.getOptions().get(i).getText();
						sel.getOptions().get(i).click();
						log.add("selected state is: "+state);
						log.add("Displayed state name is: "+sel.getFirstSelectedOption().getText());
						Thread.sleep(200);
						if(state.equalsIgnoreCase(sel.getFirstSelectedOption().getText()))
						{
							Pass(" User able to select state from the 'Providence' drop down, when the country is selected in 'United States'",log);
						}
						else
						{
							Fail("User not able to select state from the 'Providence' drop down, when the country is selected in 'United States'",log);
						}
					}
					else
					{
						Fail("'Province' dropdown field not displayed");
					}
				}
				else
				{
					Fail("country selected is not United States");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-1995" +e.getMessage());
				Exception("HBC-1995 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1996 Verify that user should be able to enter zip code details in the "Zip Code" field, when the country is selected in "United States".*/
	public void MyProfileZipCodeUS() 
	{
		ChildCreation("HUDSONBAY-1996 Verify that user should be able to enter zip code details in the 'Zip Code' field, when the country is selected in 'United States'");
		if(MyProfile == true)
		{
			try
			{
				String country = "";
				String zipCode  = HBCBasicfeature.getExcelNumericVal("HBC1996", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Select sel = new Select(myPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("United States");
					if(cnty==false)
					{
						sel.selectByValue("US");						
					}
					wait.until(ExpectedConditions.visibilityOf(myPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("United States"))
				{
					Pass("country selected is United States");
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						Pass("'Zipcode' field displayed");
						myPageZipcode.clear();
						log.add( "The Entered value from the excel file is : " + zipCode.toString());
						myPageZipcode.sendKeys(zipCode);
						Thread.sleep(500);
						if(myPageZipcode.getAttribute("value").isEmpty())
						{
							Fail(" 'Zip Code' field is empty",log);
						}						
						else if(myPageZipcode.getAttribute("value").equalsIgnoreCase(zipCode))
						{
							Pass( "User able to enter zip code details in the 'Zip Code' field, when the country is selected in 'United States'",log);
						}
						else
						{
							Fail( "User able to enter zip code details but not matched",log);
						}						
					}
					else
					{
						Fail("'Zipcode' field not displayed");
					}
				}
				else
				{
					Fail("country selected is not United States");
				}				
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1996" +e.getMessage());
				Exception("HBC-1996 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1997 Verify that user should be able to enter maximum of 5 numbers in the "Zip Code" field*/
	public void MyProfileZipCodeValidation() 
	{
		ChildCreation("HUDSONBAY-1997 Verify that user should be able to enter maximum of 5 numbers in the 'Zip Code' field");
		if(MyProfile == true)
		{
			try
			{
				String country = "";
				String[] cellVal  = HBCBasicfeature.getExcelVal("HBC1997", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Select sel = new Select(myPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("United States");
					if(cnty==false)
					{
						sel.selectByValue("US");						
					}
					wait.until(ExpectedConditions.visibilityOf(myPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(country.contains("United States"))
				{
					Pass( "country selected is United States");
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						Pass("'Zipcode' field displayed");
						for(int i = 0; i<cellVal.length;i++)
						{
							myPageZipcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							myPageZipcode.sendKeys(cellVal[i]);
							if(myPageZipcode.getAttribute("value").isEmpty())
							{
								Fail( "The Zip Code field is empty.");
							}
							else if(myPageZipcode.getAttribute("value").length()<=5)
							{
								Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
							}
							myPageZipcode.clear();
						}
					}
					else
					{
						Fail( "The Zip Code Area is not displayed.");
					}
				}
				else
				{
					Fail( "country selected is not United States.");
				}			
			}
			catch (Exception e)
			{
				System.out.println("HBC-1997" +e.getMessage());
				Exception("HBC-1997 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1998 Verify that "Zip Code" field should accept numbers and special characters and the corresponding alert message should be shown*/
	public void MypageZipCodeFieldAlert() 
	{
		ChildCreation("HUDSONBAY-1998 Verify that 'Zip Code' field should accept numbers and special characters and the corresponding alert message should be shown");
		if(MyProfile == true)
		{
			try
			{
				String excelVal = HBCBasicfeature.getExcelVal("HBC1998", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1998", sheet, 2);

				if(HBCBasicfeature.isElementPresent(myPageZipcode))
				{
					Pass("Zip Code field is displayed");
					myPageZipcode.clear();
					log.add( "The Entered value from the excel file is : " + excelVal.toString());
					myPageZipcode.sendKeys(excelVal);
					myPagePhoneField.click();
					wait.until(ExpectedConditions.attributeContains(myPageZipAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(myPageZipAlert))
					{
						String actualAlert = myPageZipAlert.getText();
						log.add("Actual text is: "+actualAlert);
						log.add("Expected text is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("'Zip Code' field accept numbers and special characters and the corresponding alert message shown",log);
						}
						else
						{
							Fail("'Zip Code' field accept numbers and special characters and the corresponding alert message not shown",log);
						}
						myPageZipcode.clear();
					}
					else
					{
						Fail("Zip Code field alert is not displayed");
					}					
				}
				else
				{
					Fail("Zip Code field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1998" +e.getMessage());
				Exception("HBC-1998 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-1999 Verify that when the user entered less than 5 digits in the "Zip Code" field, then the corresponding alert message should be shown*/
	public void MypageZipCodeFieldMinAlert() 
	{
		ChildCreation("HUDSONBAY-1999 Verify that when the user entered less than 5 digits in the 'Zip Code' field, then the corresponding alert message should be shown");
		if(MyProfile == true)
		{
			try
			{
				String excelVal = HBCBasicfeature.getExcelNumericVal("HBC1999", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1999", sheet, 2);

				if(HBCBasicfeature.isElementPresent(myPageZipcode))
				{
					Pass("Zip Code field is displayed");
					myPageZipcode.clear();
					Thread.sleep(500);
					log.add( "The Entered value from the excel file is : " + excelVal.toString());
					myPageZipcode.sendKeys(excelVal);
					myPagePhoneField.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(myPageZipAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(myPageZipAlert))
					{
						String actualAlert = myPageZipAlert.getText();
						log.add("Actual text is: "+actualAlert);
						log.add("Expected text is: "+expectedAlert);
						Thread.sleep(500);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("'Zip Code' field accept numbers and special characters and the corresponding alert message shown",log);
						}
						else
						{
							Fail("'Zip Code' field accept numbers and special characters and the corresponding alert message not shown",log);
						}
						myPageZipcode.clear();
					}
					else
					{
						Fail("Zip Code field alert is not displayed");
					}					
				}
				else
				{
					Fail("Zip Code field is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1999" +e.getMessage());
				Exception("HBC-1999 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-2000 Verify that when the user entered less than 6 digits in the "Postal Code" field, then the corresponding alert message should be shown*/
	public void MypagePostalCodeFieldAlert() 
	{
		ChildCreation("HUDSONBAY-2000 Verify that when the user entered less than 6 digits in the 'Postal Code' field, then the corresponding alert message should be shown");
		if(MyProfile == true)
		{
			try
			{
				String excelVal = HBCBasicfeature.getExcelVal("HBC2000", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC2000", sheet, 2);
				String country = "";
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Select sel = new Select(myPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");							
					if(HBCBasicfeature.isElementPresent(myPageZipcode))
					{
						Pass("Zip Code field is displayed");
						myPageZipcode.clear();
						log.add( "The Entered value from the excel file is : " + excelVal.toString());
						myPageZipcode.sendKeys(excelVal);
						myPagePhoneField.click();
						wait.until(ExpectedConditions.attributeContains(myPageZipAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(myPageZipAlert))
						{
							String actualAlert = myPageZipAlert.getText();
							log.add("Actual text is: "+actualAlert);
							log.add("Expected text is: "+expectedAlert);
							if(actualAlert.equalsIgnoreCase(expectedAlert))
							{
								Pass("user entered less than 6 digits in the 'Postal Code' field, then the corresponding alert message shown",log);
							}
							else
							{
								Fail("user entered less than 6 digits in the 'Postal Code' field, then the corresponding alert message not shown",log);
							}
							myPageZipcode.clear();
						}
						else
						{
							Fail("Zip Code field alert is not displayed");
						}					
					}
					else
					{
						Fail("Zip Code field is not displayed");
					}
				}
				else
				{
					Fail("Country not selected Canada");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2000" +e.getMessage());
				Exception("HBC-2000 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-2001 Verify that user should be able to enter phone number in the "Phone Number" field and it should accept maximum of 10 numbers.*/
	public void MyProfilePhoneNumberField() 
	{
		ChildCreation("HUDSONBAY-2001 Verify that user should be able to enter phone number in the 'Phone Number' field and it should accept maximum of 10 numbers.");
		if(MyProfile == true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC2001", sheet, 1).split("\n");
				String country = "";
				if(HBCBasicfeature.isElementPresent(myPageCountry))
				{
					Select sel = new Select(myPageCountry);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");						
					}
					wait.until(ExpectedConditions.visibilityOf(myPageCountry));
					country = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				if(country.contains("Canada"))
				{
					Pass("country selected is Canada");	
					if(HBCBasicfeature.isElementPresent(myPagePhoneField))
					{
						HBCBasicfeature.scrolldown(myPagePhoneField, driver);
						Pass("Phone field is displayed");
						for(int i = 0; i<cellVal.length;i++)
						{
							myPagePhoneField.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							myPagePhoneField.sendKeys(cellVal[i]);
							if(myPagePhoneField.getAttribute("value").isEmpty())
							{
								Fail( "The Phone field is empty.");
							}
							else if(myPagePhoneField.getAttribute("value").length()-2<=10)//since '-' increases size +2 hence reducing
							{
								Pass( "The Phone field was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Phone field was enterable but it is not within the specified character limit.",log);
							}
							myPagePhoneField.clear();
						}					
					}
					else
					{
						Fail("Phone field is not displayed");
					}
				}
				else
				{
					Fail("Country selected is not Canada");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-2001" +e.getMessage());
				Exception("HBC-2001 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
		
	/*HUDSONBAY-2002 Verify that "Preferred Language" dropdown field should be selected in "English" option by default.*/
	public void MyProfilePreferredLanguageDefault() 
	{
		ChildCreation("HUDSONBAY-2002 Verify that 'Preferred Language' dropdown field should be selected in 'English' option by default.");
		if(MyProfile == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC2002", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myPagePrefLang))
				{
					Pass("'Preferred Language' dropdown field displayed");
					HBCBasicfeature.scrolldown(myPagePrefLang, driver);
					Select sel = new Select(myPagePrefLang);
					String actual = sel.getFirstSelectedOption().getText();
					log.add("Actual selected language is: "+actual);
					log.add("Expected language is: "+expected);
					Thread.sleep(200);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("'Preferred Language' dropdown field selected in 'English' option by default",log);
					}
					else
					{
						Fail("'Preferred Language' dropdown field not selected in 'English' option by default",log);
					}
				}
				else
				{
					Fail("'Preferred Language' dropdown field not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2002" +e.getMessage());
				Exception("HBC-2002 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-2003 Verify that "Additional Information" section should be shown in the "My Profile" page*/
	/*HUDSONBAY-2004 Verify that "DOB" field, "Gender" radio tab should be shown*/

	public void MyProfileAdditionalInformation(String tc) 
	{
		if(tc=="HBC2003")
		{
			ChildCreation("HUDSONBAY-2003 Verify that 'Additional Information' section should be shown in the 'My Profile' page");
		}
		else
		{
			ChildCreation("HUDSONBAY-2004 Verify that 'DOB' field, 'Gender' radio tab should be shown");
		}
		if(MyProfile == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myPageBirthmonth))
				{
					HBCBasicfeature.scrolldown(myPageBirthmonth, driver);
					Pass(" Birth Month dropdown field displayed in 'Additional Information' section");
				}
				else
				{
					Fail(" Birth Month dropdown field not displayed in 'Additional Information' section");
				}
				if(HBCBasicfeature.isElementPresent(myPageBirthDate))
				{
					Pass(" Birth Date dropdown field displayed in 'Additional Information' section");
				}
				else
				{
					Fail(" Birth Date dropdown field not displayed in 'Additional Information' section");
				}
				if(HBCBasicfeature.isElementPresent(myPageGenderFemale))
				{
					Pass(" Gender Female radio button displayed in 'Additional Information' section");
				}
				else
				{
					Fail("  Gender Female radio button not displayed in 'Additional Information' section");
				}
				if(HBCBasicfeature.isElementPresent(myPageGenderMale))
				{
					Pass(" Gender Male radio button displayed in 'Additional Information' section");
				}
				else
				{
					Fail("  Gender Male radio button not displayed in 'Additional Information' section");
				}				
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-2003" +e.getMessage());
				Exception("HBC-2003 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-2007 Verify that on updating the details and tapping on "< Account" link, the deatils should not get updated*/
	public void MyProfileBackAccountClickNoUpdate() throws Exception 
	{
		ChildCreation("HUDSONBAY-2007 Verify that on updating the details and tapping on '< Account' link, the deatils should not get updated");
		if(MyProfile == true)
		{
			String bfreAddr = "";
			String addr = HBCBasicfeature.getExcelVal("HBC2007", sheet, 1); 
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountpageBack))
				{
					HBCBasicfeature.scrollup(myAccountpageBack, driver);
					Pass("'< Account'link is displayed");
					HBCBasicfeature.jsclick(myAccountpageBack, driver);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
					{
						Pass("MyAccount Page profile details are displayed");
						bfreAddr = myAccountPageMyProfileDetails.getText();						
					}
					else
					{
						Fail("MyAccount Page profile details are not displayed");
					}
					
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfile))
					{							
						Pass("MyProfile section displayed");
						HBCBasicfeature.jsclick(myAccountPageMyProfile, driver);									
						wait.until(ExpectedConditions.visibilityOf(myAccountMyProfilepage));
						if(HBCBasicfeature.isElementPresent(myAccountMyProfilepage))
						{
							Pass("MyProfile page displayed");
							if(HBCBasicfeature.isElementPresent(myPageStAddres))
							{
								myPageStAddres.clear();
								myPageStAddres.sendKeys(addr);
								log.add("Entered value is: "+addr);
								Thread.sleep(200);
								HBCBasicfeature.jsclick(myAccountpageBack, driver);
								wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
								if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
								{
									String afterAddr = myAccountPageMyProfileDetails.getText();
									log.add("Before updating the details: "+bfreAddr);
									log.add("After updating the details: "+afterAddr);
									Thread.sleep(500);
									if(bfreAddr.contains(afterAddr))
									{
										Pass("on updating the details and tapping on '< Account' link, the deatils not get updated",log);
									}
									else
									{
										Fail("on updating the details and tapping on '< Account' link, the deatils get updated",log);
									}
								}
								else
								{
									Fail("MyAccount Page profile details are not displayed");
								}
							}
							else
							{
								Fail("street Address field not displayed");
							}
						}
						else
						{
							Fail("MyProfile page not displayed");
						}							
					}
					else
					{
						Fail("MyProfile sectionnot displayed");
					}					
				}
				else
				{
					Fail("'< Account'link is not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println("HBC-2007" +e.getMessage());
				Exception("HBC-2007 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	/*HUDSONBAY-2008 Verify that on updating the details and tapping on "Update" button, the details should be updated*/
	public void MyProfileUpdateClick() throws Exception 
	{
		ChildCreation("HUDSONBAY-2008 Verify that on updating the details and tapping on 'Update' button, the details should be updated");
		if(MyProfile == true)
		{
			String bfreAddr = "";
			String pwd = HBCBasicfeature.getExcelVal("login", sheet, 1);
			String dupPwd = HBCBasicfeature.getExcelVal("HBC1947", sheet, 2);
			String addr = HBCBasicfeature.getExcelVal("HBC2007", sheet, 1); 
			String addr1 = HBCBasicfeature.getExcelVal("HBC2007", sheet, 2); 

			try
			{				
				wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
				if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
				{
					Pass("MyAccount Page profile details are displayed");
					bfreAddr = myAccountPageMyProfileDetails.getText();						
				}
				else
				{
					Fail("MyAccount Page profile details are not displayed");
				}
				
				if(HBCBasicfeature.isElementPresent(myAccountPageMyProfile))
				{							
					Pass("MyProfile section displayed");
					HBCBasicfeature.jsclick(myAccountPageMyProfile, driver);									
					wait.until(ExpectedConditions.visibilityOf(myAccountMyProfilepage));
					if(HBCBasicfeature.isElementPresent(myAccountMyProfilepage))
					{
						Pass("MyProfile page displayed");
						if(HBCBasicfeature.isElementPresent(myPageStAddres))
						{
							myPageStAddres.clear();
							myPageStAddres.sendKeys(addr);
							log.add("Entered value is: "+addr);
							myPagePassword.clear();
							myPagePassword.sendKeys(dupPwd);
							myPageVerifyPassword.clear();
							myPageVerifyPassword.sendKeys(dupPwd);
							Thread.sleep(200);
							if(HBCBasicfeature.isElementPresent(myPageUpdate))
							{
								HBCBasicfeature.scrolldown(myPageUpdate, driver);
								Thread.sleep(200);
								HBCBasicfeature.jsclick(myPageUpdate, driver);
								try
								{
									wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
									if(HBCBasicfeature.isElementPresent(addressuseasEntered))
									{
										addressuseasEntered.click();
									}
								}
								catch(Exception e)
								{
									Fail("Address verification overlay not displayed");
								}									
								wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
								if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
								{
									String afterAddr = myAccountPageMyProfileDetails.getText();
									log.add("Before updating the details: "+bfreAddr);
									log.add("After updating the details: "+afterAddr);
									Thread.sleep(500);
									if(bfreAddr.contains(afterAddr))
									{
										Fail("on updating the details and tapping on '< Account' link, the deatils not get updated",log);
									}
									else
									{
										Pass("on updating the details and tapping on '< Account' link, the deatils get updated",log);
									}
									
									//Revert the changes back for further execution
									
									if(HBCBasicfeature.isElementPresent(myAccountPageMyProfile))
									{							
										Pass("MyProfile section displayed");
										HBCBasicfeature.jsclick(myAccountPageMyProfile, driver);									
										wait.until(ExpectedConditions.visibilityOf(myAccountMyProfilepage));
										if(HBCBasicfeature.isElementPresent(myAccountMyProfilepage))
										{
											Pass("MyProfile page displayed");
											if(HBCBasicfeature.isElementPresent(myPageStAddres))
											{
												myPageStAddres.clear();
												myPageStAddres.sendKeys(addr1);
												myPagePassword.clear();
												myPagePassword.sendKeys(pwd);
												myPageVerifyPassword.clear();
												myPageVerifyPassword.sendKeys(pwd);
												Thread.sleep(200);
												if(HBCBasicfeature.isElementPresent(myPageUpdate))
												{
													HBCBasicfeature.scrolldown(myPageUpdate, driver);
													Thread.sleep(200);
													HBCBasicfeature.jsclick(myPageUpdate, driver);
													try
													{
														wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
														if(HBCBasicfeature.isElementPresent(addressuseasEntered))
														{
															addressuseasEntered.click();
														}
													}
													catch(Exception e)
													{
														Fail("Address verification overlay not displayed");
													}									
													wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
													if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
													{
														String currAddr = myAccountPageMyProfileDetails.getText();
														log.add("Before updating the details: "+bfreAddr);
														log.add("After updating the details: "+currAddr);
														Thread.sleep(500);
														if(bfreAddr.contains(currAddr))
														{
															Pass("Address reverted back",log);
														}
														else
														{
															Fail("Address not reverted back",log);
														}
													}
													else
													{
														Fail("MyAccount Page profile details are not displayed");
													}	
												}
												else
												{
													Fail("Update Button not displayed");
												}
											}
											else
											{
												Fail("street Address field not displayed");
											}
										}
										else
										{
											Fail("MyProfile page not displayed");
										}	
									}	
									else
									{
										Fail("MyProfile sectionnot displayed");
									}	
								}
								else
								{
									Fail("MyAccount Page profile details are not displayed");
								}								
							}
							else
							{
								Fail("Update Button not displayed");
							}
						}
						else
						{
							Fail("street Address field not displayed");
						}
					}
					else
					{
						Fail("MyProfile page not displayed");
					}							
				}
				else
				{
					Fail("MyProfile sectionnot displayed");
				}						
			}
			catch (Exception e)
			{
				System.out.println("HBC-2008" +e.getMessage());
				Exception("HBC-2008 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyProfile Page not displayed");
		}
	}
	
	
}

