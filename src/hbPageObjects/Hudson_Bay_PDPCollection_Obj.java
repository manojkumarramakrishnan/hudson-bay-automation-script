package hbPageObjects;

import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_PDPCollection;

public class Hudson_Bay_PDPCollection_Obj extends Hudson_Bay_PDPCollection implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_PDPCollection_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}	
	
	
	@FindBy(xpath=""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
		
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
		
	@FindBy(xpath=""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+PDPProdNamee+"")
	WebElement PDPProdName;		

	@FindBy(xpath = ""+pdpPageProductImagee+"")
	WebElement pdpPageProductImage;		
	
	@FindBy(xpath = ""+pdpcollectionImageSelectedd+"")
	WebElement pdpcollectionImageSelected;			
	
	@FindBy(xpath = ""+pdpPageSwatchess+"")
	WebElement pdpPageSwatchesContainer;
	
	@FindBy(xpath = ""+pdpPageActiveColorr+"")
	WebElement pdpPageActiveColor;	
		
	@FindBy(xpath = ""+pdppageSizee+"")
	WebElement pdppageSize;
		
	@FindBy(xpath = ""+	pdppageQuantityContainerr+"")
	WebElement 	pdppageQuantityContainer;
	
	@FindBy(xpath = ""+	pdppageQuantityPluss+"")
	WebElement 	pdppageQuantityPlus;
	
	@FindBy(xpath = ""+	pdppageQuantityMinuss+"")
	WebElement 	pdppageQuantityMinus;
	
	@FindBy(xpath = ""+	pdppageQuantityTextt+"")
	WebElement 	pdppageQuantityText;
	
	@FindBy(xpath = ""+	pdppageDetailss+"")
	WebElement 	pdppageDetails;
	
	@FindBy(xpath = ""+	pdpcollectionDetailsSectionn+"")
	WebElement 	pdpcollectionDetailsSection;
	
	@FindBy(xpath = ""+	pdppageATBb+"")
	WebElement 	pdppageATB;
		
	@FindBy(xpath = ""+	pdppageATBOverlayy+"")
	WebElement 	pdppageATBOverlay;
	
	@FindBy(xpath = ""+	loadingbarr+"")
	WebElement 	loadingbar;
	
	@FindBy(xpath = ""+pdppageATBOverlayClosee+"")
	WebElement 	pdppageATBOverlayClose;
	
	@FindBy(xpath = ""+pdppageShareIconss+"")
	WebElement 	pdppageShareIcons;
	
	@FindBy(xpath = ""+pdppageShareFBb+"")
	WebElement 	pdppageShareFB;
	
	@FindBy(xpath = ""+pdppageShareTwitterr+"")
	WebElement 	pdppageShareTwitter;
	
	@FindBy(xpath = ""+pdppageShareGpluss+"")
	WebElement 	pdppageShareGplus;
	
	@FindBy(xpath = ""+pdppageSizeSelectedDefaultt+"")
	WebElement 	pdppageSizeSelectedDefault;
	
	@FindBy(xpath = ""+pdppageSwatchColorSelectedd+"")
	WebElement 	pdppageSwatchColorSelected;
		
	@FindBy(xpath = ""+collectionProductSelectYourItemss+"")
	WebElement 	collectionProductSelectYourItems;
	
	@FindBy(xpath = ""+collectionProductContianerr+"")
	WebElement 	collectionProductContianer;
	
	@FindBy(how = How.XPATH,using  = ""+pdppageDetailsCollapsedd+"")
	List<WebElement> pdppageDetailsCollapsedList;		
	
	@FindBy(how = How.XPATH,using  = ""+pdppageDetailsExpandd+"")
	List<WebElement> pdppageDetailsExpandList;		
	
	@FindBy(how = How.XPATH,using  = ""+collectionProductListt+"")
	List<WebElement> collectionProductList;	
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
		//Footer:
		@FindBy(xpath = ""+signupEmailss+"")
		WebElement signupEmails;
	
		@FindBy(xpath = ""+footerContainerr+"")
		WebElement footerContainer;	
		
		@FindBy(xpath = ""+footerTopp+"")
		WebElement footerTop;	
		
		@FindBy(xpath = ""+footerTopTitlee+"")
		WebElement footerTopTitle;
		
		@FindBy(xpath = ""+footerSubTopp+"")
		WebElement footerSubTop;
		
		@FindBy(xpath = ""+footerSubTopCalll+"")
		WebElement footerSubTopCall;
		
		@FindBy(xpath = ""+footerSubTopEmaill+"")
		WebElement footerSubTopEmail;
		
		@FindBy(xpath = ""+footerMenuListt+"")
		WebElement footerMenuList;
		
		@FindBy(xpath = ""+footerBottomm+"")
		WebElement footerBottom;
		
		@FindBy(xpath = ""+footerSocialIconn+"")
		WebElement footerSocialIcon;
		
		@FindBy(xpath = ""+footerBottomCopyrightt+"")
		WebElement footerBottomCopyright;

		String PLPtitle = "";	

		boolean collections = false;
	

	//	Navigating to PDP Collections Page
	public void CollectionsPdp() throws Exception
		{
			ChildCreation("PDP Collections Page");	
			String Key = HBCBasicfeature.getExcelVal("collections", sheet, 1);
			WebElement product = null ;
			String product_BrandTitle = "";
			try
			{	
				wait.until(ExpectedConditions.visibilityOf(header));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{									
					boolean pgLoad = false;
					do
					{				
						try
						{
							searchIcon.click();
							wait.until(ExpectedConditions.visibilityOf(searchBox));
							searchBox.sendKeys(Key);
							searchBox.sendKeys(Keys.ENTER);
							Thread.sleep(1000);
							if(HBCBasicfeature.isElementPresent(plpPage))		
							{
								pgLoad=true;	
								break;		
							}
							else
							{
								continue;
							}
						}
						catch (Exception e)
						{
							pgLoad=false;
							driver.navigate().refresh();	
							continue;
						}
					}							
					while(!pgLoad==true); 							
					if(HBCBasicfeature.isElementPresent(plpPage))
					{
						Pass("PLP page displayed");
						Thread.sleep(1000);
						if(HBCBasicfeature.isListElementPresent(plpItemContList))
						{
							int i =0;
							boolean coll = false;								
							do
							{				
								try
								{
									Thread.sleep(500);
									int size = plpItemContList.size();
									Random r  = new Random();
									i = r.nextInt(size);
									Thread.sleep(500);
									product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
									wait.until(ExpectedConditions.visibilityOf(product));
									HBCBasicfeature.scrolldown(product, driver);	
									product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+(i+1)+"]")).getText();
									if(product_BrandTitle.contains("COLLECTION"))		
									{
										coll=true;	
										break;		
									}
								}
								catch (Exception e)
								{
									coll=false;
									continue;
								}
							}
							while(!coll==true);									
							if(HBCBasicfeature.isElementPresent(product))
							{
								product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+(i+1)+"]")).getText();
								String product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+(i+1)+"]")).getText();
								PLPtitle = product_BrandTitle +" "+ product_Title;
								log.add("The selected product title is: "+PLPtitle);
								Thread.sleep(1000);
								product.click();
								wait.until(ExpectedConditions.visibilityOf(PDPPage));
								if(HBCBasicfeature.isElementPresent(PDPPage))
								{
									Thread.sleep(1000);
									String PDPtitle = PDPProdName.getText();
									log.add("The PDP page title is: "+PDPtitle);
									if(PDPtitle.equalsIgnoreCase(PLPtitle))
									{
										Pass("PDP and PLP title matches",log);
										if(HBCBasicfeature.isElementPresent(collectionProductContianer))
										{
											Pass("Collections PDP page displayed",log);
											collections = true;
										}
										else
										{
											Fail("Selecting the product in the PLP page,not navigated to the corresponding collection PDP page",log);
										}	
									}
									else
									{
										Fail("PDP and PLP title not matches",log);
									}	
								}
								else
								{
									Fail("PDP Page not displayed");
								}
							}
							else
							{
								Fail("Selected product not displayed");
							}					
						}
						else
						{
							Fail("PLP products are not dispalyed");
						}
					}
					else
					{
						Fail("PLP page not dispalyed");
					}				
				}
				else
				{
					Fail("Search Icon not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
		
	/*HUDSONBAY-784 Verify that Collectibles PDP page should be shown as per the creative and fit to the Screen in both potrait and Landscape*/
	public void collectionsPDPCreative() 
	{
		ChildCreation("HUDSONBAY-1955 Verify that Collectibles PDP page should be shown as per the creative and fit to the Screen in both potrait and Landscape");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(pdpPageProductImage))
				{
					int res = HBCBasicfeature.imageBroken(pdpPageProductImage, log);
					if(res==200)
					{
						Pass("PDP page master image displayed");
					}
					else
					{
						Fail("PDP page master image not displayed");
					}
				}
				else
				{
					Fail("PDP page product image not displayed");
				}
				
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr= 0;
					for(int i=1;i<=size;i++)
					{
						log.add("The sub product "+i+" details are below");
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						log.add("The Product title is: "+prodTitle);
						Thread.sleep(500);
						String prodColor = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='pdpColorContainer']//*[@class='selectedColor']")).getText();
						log.add("The Product Color is: "+prodColor);
						Thread.sleep(500);
						String prodSize = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='pdpSizeContainer']//div")).getAttribute("value");
						log.add("The Product Size is: "+prodSize);
						Thread.sleep(500);
						List<WebElement> priceSize = driver.findElements(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdpPriceContent']//*[@class='skmob_priceValue ']"));
						int pSize = priceSize.size();
						for(int j=1;j<=pSize;j++)
						{
							String price = driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdpPriceContent']//*[@class='skmob_priceValue '])["+j+"]")).getText();
							log.add("Displayed price values are "+price);
						}
						ctr++;
					}	
					if(ctr==size)
					{
						Pass("On selecting 'Collective/ Master' product then the product with price range and its sub products are shown",log);
					}
					else
					{
						Fail("On selecting 'Collective/ Master' product then the product with price range and its sub products are not shown",log);
					}
				}
				else
				{
					Fail("Collection Product container not displayed");
				}
				if(HBCBasicfeature.isElementPresent(pdppageATB))
				{
					HBCBasicfeature.scrolldown(pdppageATB, driver);
					Pass("Add To Bag button displayed");
				}
				else
				{
					Fail("Add To Bag button not displayed");
				}
				if(HBCBasicfeature.isElementPresent(pdppageShareIcons))
				{
					HBCBasicfeature.scrolldown(pdppageShareIcons, driver);
					if(HBCBasicfeature.isElementPresent(pdppageShareTwitter))
					{
						Pass("Social Icon Twitter displayed");
					}
					else
					{
						Fail("Social Icon Twitter not displayed");
					}
					if(HBCBasicfeature.isElementPresent(pdppageShareGplus))
					{
						Pass("Social Icon Google+ displayed");
					}
					else
					{
						Fail("Social Icon Google+ not displayed");
					}
					if(HBCBasicfeature.isElementPresent(pdppageShareFB))
					{
						Pass("Social Icon FB+ displayed");
					}
					else
					{
						Fail("Social Icon FB not displayed");
					}
				}
				else
				{
					Fail("PDP share Icons container not displayed");
				}				
				/*if(HBCBasicfeature.isElementPresent(signupEmails))
				{
					Pass("Signup for daily Emails option is displayed");	
				}
				else
				{
					Fail("Signup for daily Emails option is not displayed");						
				}			*/
				if(HBCBasicfeature.isElementPresent(footerTop))
				{
					Pass("Footer top panel is displayed");	
					if(HBCBasicfeature.isElementPresent(footerTopTitle))
					{
						Pass("Footer top panel title is displayed");	
					}
					else
					{
						Fail("Footer top panel title is not displayed");						
					}
					if(HBCBasicfeature.isElementPresent(footerSubTop))
					{
						Pass("Footer sub top panel is displayed");	
						if(HBCBasicfeature.isElementPresent(footerSubTopCall))
						{
							Pass("Footer call option is displayed");		
						}
						else
						{
							Fail("Footer Call option is not displayed");						
						}
						if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
						{
							Pass("Footer Email option is displayed");	
						}
						else
						{
							Fail("Footer email option is not displayed");						
						}
					}
					else
					{
						Fail("Footer sub top panel is not displayed");						
					}
				}
				else
				{
					Fail("Footer top panel is not displayed");						
				}
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					Pass("Footer Menu List option is displayed");
				}
				else
				{
					Fail("Footer menu list is not displayed");						
				}
				if(HBCBasicfeature.isElementPresent(footerBottom))
				{
					Pass("Footer bottom panel is displayed");
					wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
					if(HBCBasicfeature.isElementPresent(footerSocialIcon))
					{
						Pass("Footer social icon options are displayed");
					}
					else
					{
						Fail("Footer social icon options are not displayed");						
					}
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{
						log.add("Footer displayed as per the creative and fit to the screen");
						Pass("Footer copyright section is displayed",log);										
					}
					else
					{
						log.add("Footer is not displayed as per the creative and fit to the screen");
						Fail("Footer copyright section is not displayed",log);	
					}
				}
				else
				{
					Fail("Footer bottom panel is not displayed");						
				}			
				HBCBasicfeature.scrollup(header, driver);
			}
			catch (Exception e)
			{
				System.out.println("HBC-784" +e.getMessage());
				Exception("HBC-784 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}	
	
	/*HUDSONBAY-786 Verify that user should able to scroll the PDP page vertically*/
	public void collectionsPdpVertical_Scroll()
	{
		ChildCreation("HUDSONBAY-786 Verify that user should able to scroll the PDP page vertically");
		if(collections == true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				if(HBCBasicfeature.isElementPresent(header))
				{
					HBCBasicfeature.scrolldown(footerContainer, driver);
					wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{
						Pass("User can able to scroll the PDP page vertically");
					}
					else
					{
						Fail("User not able to scroll the PDP page vertically ");
					}					
				}
				else
				{
					Fail("Header not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HUDSONBAY-786 "+e.getMessage());
				Exception("HUDSONBAY-786 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Collections PDP page not displayed");
		}
	}
	
	/*HUDSONBAY-787 Verify that header should be displayed in Collectibles  PDP as per the creative.*/
	public void collectionsPdpHeader()
		{
			ChildCreation("HUDSONBAY-787 Verify that header should be displayed in Collectibles  PDP as per the creative.");
			if(collections == true)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(header));
					if(HBCBasicfeature.isElementPresent(header))
					{
						HBCBasicfeature.scrollup(header, driver);
						if(HBCBasicfeature.isElementPresent(hamburger))
						{
							Pass("Hamburger menu displayed in the header");
						}
						else
						{
							Fail("Hamburger menu not displayed in the header");
						}
						if(HBCBasicfeature.isElementPresent(logo))
						{
							Pass("Logo displayed in the header");
						}
						else
						{
							Fail("Logo not displayed in the header");
						}
						if(HBCBasicfeature.isElementPresent(searchIcon))
						{
							Pass("Search icon displayed in the header");
						}
						else
						{
							Fail("Search icon not displayed in the header");
						}
						if(HBCBasicfeature.isElementPresent(bagIcon))
						{
							Pass("Shoppin Bag icon displayed in the header");
						}
						else
						{
							Fail("Shoppin Bag icon not displayed in the header");
						}
					}
					else
					{
						Fail("Header not displayed in the home page");
					}
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-787" +e.getMessage());
					Exception("HUDSONBAY-787 There is something wrong. Please Check." + e.getMessage());
				}
			}
		else
		{
			Skip("Collections PDP page not displayed");
		}
	}
		
	/*HUDSONBAY-788 Verify that product image should be displayed (without an image border) below the page header.*/
	public void collectionsPdpProduct_Image()
		{
			ChildCreation("HUDSONBAY-788 Verify that product image should be displayed (without an image border) below the page header.");
			if(collections == true)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(pdpPageProductImage));
					HBCBasicfeature.scrollup(pdpPageProductImage, driver);
					if(HBCBasicfeature.isElementPresent(pdpPageProductImage))
					{
						Pass("The image container is present for the selected product.");
						WebElement imageContainer = driver.findElement(By.xpath("//*[@id='id_pdpMasterImageCont']"));
						List<WebElement> img = imageContainer.findElements(By.tagName("img"));
						log.add("Total Images present for the Product is: "+img.size());
						for(WebElement image : img)
						{
							int status = HBCBasicfeature.imageBroken(image,log);
							if(status==200)
							{
								Pass("The displayed image for the product is Valid Image and it is not broken.", log);
							}
							else
							{
								Fail("The displayed image for the product is Invaalid Image and it is broken.", log);
							}
						}
					}
					else
					{
						Fail("The image container is not found for the selected product.");
					}				
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-788" +e.getMessage());
					Exception("HUDSONBAY-788 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("PDP collections Page not displayed");
			}
		}
	
	/*HUDSONBAY-792 Verify that by default the first image should be displayed in the Collectibles PDP.*/
	public void collectionspdpProduct_ImageDefault()
		{
			ChildCreation("HUDSONBAY-792 Verify that by default the first image should be displayed in the Collectibles PDP.");
			if(collections == true)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(pdpcollectionImageSelected));
					if(HBCBasicfeature.isElementPresent(pdpcollectionImageSelected))
					{
						String position = pdpcollectionImageSelected.getAttribute("index");
						int pos = Integer.parseInt(position);
						log.add("The selected image position is: "+position);
						if(pos==1)
						{
							Pass("By default the first image displayed",log);
						}
						else
						{
							Fail("By default the first image not displayed",log);
						}					
					}
					else
					{
						Fail("Image not selected by defalut");
					}
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-792 "+e.getMessage());
					Exception("HUDSONBAY-792 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("PDP collections Page not displayed");
			}
		}
	
	/*HUDSONBAY-796 Verify that product title should be displayed as per the classic site*/
	public void collectionsPdpProduct_Title()
		{
			ChildCreation("HUDSONBAY-796 Verify that product title should be displayed as per the classic site");
			if(collections == true)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(PDPProdName));
					HBCBasicfeature.scrollup(PDPProdName, driver);
					if(HBCBasicfeature.isElementPresent(PDPProdName))
					{
						String PDPtitle = PDPProdName.getText();
						log.add("The PDP page title is: "+PDPtitle);
						log.add("The Title from PLP page is: "+PLPtitle);
						if(PDPtitle.equalsIgnoreCase(PLPtitle))
						{
							Pass("Product title displayed as per the classic site",log);
						}
						else
						{
							Fail("Product title displayed as per the classic site",log);
						}					
					}
					else
					{
						Fail("Product title not displayed in the PDP page");
					}
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-796 "+e.getMessage());
					Exception("HUDSONBAY-796 There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("PDP collections Page not displayed");
			}
		}	
	
	/*HUDSONBAY-798 Verify that "Details" accordion should be shown below the product title*/
	/*HUDSONBAY-799 Verify that accordions arrangements should be matched with the classic site/creative*/
	public void collectionsPDPDetails() 
	{
		ChildCreation("HUDSONBAY-798 Verify that 'Details' accordion should be shown below the product title");
		if(collections == true)
		{
			boolean HBC799 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(pdpcollectionDetailsSection))
				{					
					String details = pdpcollectionDetailsSection.getText();
					log.add("The product description is: "+details);
					boolean result =details.isEmpty();
					if(result==false)
					{
						Pass("Product 'Details' accordion shown below the product title",log);
						HBC799 = true;
					}
					else	
					{
						Fail("Product 'Details' accordion not shown below the product title",log);
					}					
				}
				else
				{
					Fail("Details section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-798" +e.getMessage());
				Exception("HBC-798 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-799 Verify that accordions arrangements should be matched with the classic site/creative");
			if(HBC799 == true)
			{
				Pass("Accordions arrangements matched with the classic site/creative");
			}
			else
			{
				Fail("Accordions arrangements not matched with the classic site/creative");
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-800 Verify that all the accordions in the Collectibles PDP page should be in expanded state by default*/
	public void collectionsPDPAccordianDefaultState() 
	{
		ChildCreation("HUDSONBAY-800 Verify that all the accordions in the Collectibles PDP page should be in expanded state by default");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(pdpcollectionDetailsSection))
				{	
					String title = PDPProdName.getText();
					log.add("The Displayed produuct name is: "+title);
					Pass("Product details section in expanded state by default",log);
				}
				else
				{
					Fail("Product details section not in expanded state by default");
				}
				
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr = 0;
					for(int i=1;i<=size;i++)
					{
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						Thread.sleep(200);
						String accordianName = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						boolean subProductDetailsSection = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdp_Description expandDetails']")).isDisplayed();
						if(subProductDetailsSection==true)
						{
							Pass("Details section is in expanded state for default for accordian: "+accordianName);
							ctr++;
						}
						else
						{
							Fail("Details section is in expanded state for default for accordian: "+accordianName);
						}
					}
					if(ctr==size)
					{
						Pass("All the accordions in the Collectibles PDP page in expanded state by default");
					}
					else
					{
						Fail(" Accordions in the Collectibles PDP page not in expanded state by default");
					}
				}
				else
				{
					Fail("SubProducts container not displayed");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-800" +e.getMessage());
				Exception("HBC-800 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-803 Verify that "X" icon should be shown, when the accordion is in enabled state in Collectibles PDP*/
	/*HUDSONBAY-801 Verify that accordion should be closed on tapping the opened state accordion again or tapping the collapsed state accordion in Collectibles PDP*/
	public void collectionsAccordOpenStateIcon() 
	{
		ChildCreation("HUDSONBAY-803 Verify that 'X' icon should be shown, when the accordion is in enabled state in Collectibles PDP");
		if(collections == true)
		{
			boolean HBC801 = false;
			try
			{
				if(HBCBasicfeature.isListElementPresent(pdppageDetailsExpandList))
				{
					int size = pdppageDetailsExpandList.size();
					int i = 0;
					do
					{
						i = 1;
						boolean accordExpanded = driver.findElement(By.xpath("(//*[@class='pdp_Description expandDetails']//*[@class='pdp_DetailsLink'])["+i+"]")).isDisplayed();
						if(accordExpanded==true)
						{
							WebElement accordExpand = driver.findElement(By.xpath("(//*[@class='pdp_Description expandDetails']//*[@class='pdp_DetailsLink'])["+i+"]"));
							HBCBasicfeature.scrolldown(accordExpand, driver);
							accordExpand.click();
							Pass("'X' icon shown when the accordion is in enabled state in Collectibles PDP ");
							HBC801 = true;
							Thread.sleep(500);
							i--;
						}
						else
						{
							Fail("Accord not in expanded state");
						}
						 size = pdppageDetailsExpandList.size();
					}
					while(size!=0);
				}
				else
				{
					Fail("Accordians not in Expanded state");
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-803" +e.getMessage());
				Exception("HBC-803 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-801 Verify that accordion should be closed on tapping the opened state accordion again or tapping the collapsed state accordion in Collectibles PDP");
			if(HBC801==true)
			{
				Pass("Accordion closed on tapping the opened state accordion again");
			}
			else
			{
				Fail("No accordians in opened state");
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
		
	/*HUDSONBAY-802 Verify that "+" icon should be shown, when the accordion is in disabled state in Collectibles PDP*/
	public void collectionsAccordCloseStateIcon() 
	{
		ChildCreation("HUDSONBAY-802 Verify that "+" icon should be shown, when the accordion is in disabled state in Collectibles PDP");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isListElementPresent(pdppageDetailsCollapsedList))
				{
					int size = pdppageDetailsCollapsedList.size();
					int i = 0;
					do
					{
						i = 1;
						boolean accordClosed = driver.findElement(By.xpath("(//*[@class='pdp_Description']//*[@class='pdp_DetailsLink'])["+i+"]")).isDisplayed();
						if(accordClosed==true)
						{
							WebElement accordClose = driver.findElement(By.xpath("(//*[@class='pdp_Description']//*[@class='pdp_DetailsLink'])["+i+"]"));
							HBCBasicfeature.scrolldown(accordClose, driver);
							accordClose.click();
							Pass("'+' icon shown when the accordion is in disabled state in Collectibles PDP ");
							Thread.sleep(500);
							i--;
						}
						else
						{
							Fail("Accord not in disabled state");
						}
						 size = pdppageDetailsCollapsedList.size();
					}
					while(size!=0);
				}
				else
				{
					Fail("Accordians not in disabled state");
				}	
			}
			catch (Exception e)
			{
				System.out.println("HBC-802" +e.getMessage());
				Exception("HBC-802 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-805 Verify that below the "Customer Care" section of master product, "SELECT YOUR ITEMS" section should be shown*/
	/*HUDSONBAY-806 Verify that "SELECT YOUR ITEMS" section should contain its respective collectibles products*/
	public void collectionsSelectItems() 
	{
		ChildCreation("HUDSONBAY-805 Verify that below the 'Customer Care' section of master product, 'SELECT YOUR ITEMS' section should be shown");
		if(collections == true)
		{
			boolean HBC806 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductSelectYourItems))
				{
					Pass("'SELECT YOUR ITEMS' section displayed");
					int size = collectionProductList.size();
					int ctr= 0;
					for(int i=1;i<=size;i++)
					{
						log.add("The sub product "+i+" details are below");
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						log.add("The Product title is: "+prodTitle);
						Thread.sleep(500);						
						ctr++;
					}	
					if(ctr==size)
					{
						Pass("Below the 'Customer Care' section of master product, 'SELECT YOUR ITEMS' section shown",log);
						HBC806 = true;
					}
					else
					{
						Fail("Below the 'Customer Care' section of master product, 'SELECT YOUR ITEMS' section not shown",log);
					}
				}
				else
				{
					Fail("'SELECT YOUR ITEMS' section not shown");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-805" +e.getMessage());
				Exception("HBC-805 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-806 Verify that 'SELECT YOUR ITEMS' section should contain its respective collectibles products");
			if(HBC806 == true)
			{
				Pass("'SELECT YOUR ITEMS' section contain its respective collectibles products");
			}
			else
			{
				Fail("'SELECT YOUR ITEMS' section not contain its respective collectibles products");
			}			
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
			
	/*HUDSONBAY-807 Verify that all the sub products should display "product image", "Product Title", "Web ID", "Original Price", "Sale Price", "Qty" field with increment/decrement, "Color", "Size"*/
	public void collectionsSubProductsCreative() 
	{
		ChildCreation("HUDSONBAY-807 Verify that all the sub products should display 'product image', 'Product Title', 'Web ID', 'Original Price', 'Sale Price', 'Qty' field with increment/decrement, 'Color', 'Size'");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr= 0;
					for(int i=1;i<=size;i++)
					{
						Pass("The sub product "+i+" details are below");
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The Product title displayed is: "+prodTitle);
						Thread.sleep(500);			
						String prodWebID = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdp-collections-desc']//*[@class='desc_categoryid']")).getText();
						Pass("The Product WebID is: "+prodWebID);
						Thread.sleep(500);								
						WebElement image = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemImgLink']//img"));
						int res = HBCBasicfeature.imageBroken(image, log);
						if(res==200)
						{
							Pass("'product image' displayed for subProduct");
						}
						else
						{
							Fail("'product image' not displayed for subProduct");
						}
						Thread.sleep(200);
						String prodColor = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='pdpColorContainer']//*[@class='selectedColor']")).getText();
						Pass("The Product Color is: "+prodColor);
						Thread.sleep(500);					
						List<WebElement> prodSizee = driver.findElements(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='pdpSizeContainer']//div"));
						int pSize = prodSizee.size();
						for(int j=1;j<=pSize;j++)
						{
							String prodSize = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='pdpSizeContainer']//div")).getAttribute("value");
							Pass("Displayed Product size is "+prodSize);
						}
						Thread.sleep(500);
						List<WebElement> priceVal = driver.findElements(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdpPriceContent']//*[@class='skmob_priceValue ']"));
						int pVal = priceVal.size();
						for(int j=1;j<=pVal;j++)
						{
							String price = driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdpPriceContent']//*[@class='skmob_priceValue '])["+j+"]")).getText();
							Pass("Displayed price values are "+price);
						}
						Thread.sleep(500);
						boolean increment = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityPlus']")).isDisplayed();
						boolean decrement = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityMinus']")).isDisplayed();
						if(increment==true&&decrement==true)
						{
							String quantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
							log.add("Quantity dispalyed is: "+quantity);
							Pass("Quantity field with Increment and Decrement field is displayed",log);
						}						
						ctr++;
					}	
					if(ctr==size)
					{
						Pass("All the sub products display 'product image', 'Product Title', 'Web ID', 'Original Price', 'Sale Price', 'Qty' field with increment/decrement, 'Color', 'Size'");
					}
					else
					{
						Fail("All the sub products not display 'product image', 'Product Title', 'Web ID', 'Original Price', 'Sale Price', 'Qty' field with increment/decrement, 'Color', 'Size'");
					}
				}
				else
				{
					Fail("Collection Product container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-807" +e.getMessage());
				Exception("HBC-807 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
		
	/*HUDSONBAY-808 Verify that "Web ID" should be shown in grey color as per the creative*/
	public void collectionsWebIDColor() throws java.lang.Exception 
	{
		ChildCreation("HUDSONBAY-808 Verify that 'Web ID' should be shown in grey color as per the creative");
		if(collections == true)
		{
			String expected = HBCBasicfeature.getExcelVal("HBC808", sheet, 1);
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					for(int i=1;i<=size;i++)
					{						
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The sub product "+prodTitle+" WebId details are below");
						String prodWebID = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdp-collections-desc']//*[@class='desc_categoryid']")).getText();
						Pass("The Product WebID is: "+prodWebID);
						String color = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdp-collections-desc']//*[@class='desc_categoryid']")).getCssValue("color");
						String col = HBCBasicfeature.colorfinder(color);
						if(col.equalsIgnoreCase(expected))
						{
							Pass("'Web ID' shown in grey color as per the creative for the subProduct:  "+prodTitle);
						}
						else
						{
							Fail("'Web ID' not shown in grey color as per the creative for the subProduct:  "+prodTitle);
						}						
					}
				}
				else
				{
					Fail("subProduct container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-808" +e.getMessage());
				Exception("HBC-808 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-813 Verify that "Size Swatches" should be shown when the product has sizes*/
	/*HUDSONBAY-814 Verify size swatches should be shown when the product has more size*/
	public void collectionsSizeSwatches() 
	{
		ChildCreation("HUDSONBAY-813 Verify that 'Size Swatches' should be shown when the product has sizes");
		if(collections == true)
		{
			boolean HBC814 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr = 0;
					for(int i=1;i<=size;i++)
					{
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						Thread.sleep(200);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The sub product "+prodTitle+" details are below");
						try
						{
							List<WebElement> Swatches = driver.findElements(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[contains(@class,'pdpSizeSelect pdpSelectEle')]"));
							int swatchSize = Swatches.size();
							Pass("Size swatches displayed for the subProduct "+prodTitle+" and total swatches displayed is: "+swatchSize);		
							ctr++;
						}
						catch (Exception e)
						{
							Fail("Size swatches not displayed for the subProduct "+prodTitle+"");
						}
					}
					if(size==ctr)
					{
						Pass("'Size Swatches' shown when the product has sizes");
						HBC814 = true;
					}
					else
					{
						Fail("'Size Swatches' not shown when the product has sizes");
					}
				}
				else
				{
					Fail("subProduct container not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-813" +e.getMessage());
				Exception("HBC-813 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-814 Verify size swatches should be shown when the product has more size");
			if(HBC814==true)
			{
				Pass("'Size Swatches' shown when the product has sizes");
			}
			else
			{
				Fail("'Size Swatches' not shown when the product has sizes");
			}			
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-809 Verify that "Color Swatches" should be shown when the product has more colors*/
	/*HUDSONBAY-812 Verify color swatches should be shown when the product has more colors*/
	public void collectionsColorSwatches() 
	{
		ChildCreation("HUDSONBAY-809 Verify that 'Color Swatches' should be shown when the product has Color");
		if(collections == true)
		{
			boolean HBC812 = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr = 0;
					for(int i=1;i<=size;i++)
					{
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						Thread.sleep(200);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The sub product "+prodTitle+" details are below");
						try
						{
							List<WebElement> Swatches = driver.findElements(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[contains(@class,'pdpColorSwatch skMob_available')]"));
							int swatchSize = Swatches.size();
							Pass("Color swatches displayed for the subProduct "+prodTitle+" and total swatches displayed is: "+swatchSize);		
							ctr++;
						}
						catch (Exception e)
						{
							Fail("Color swatches not displayed for the subProduct "+prodTitle+"");
						}
					}
					if(size==ctr)
					{
						Pass("'Color Swatches' shown when the product has Color");
						HBC812  = true;
					}
					else
					{
						Fail("'Color Swatches' not shown when the product has Color");
					}
				}
				else
				{
					Fail("subProduct container not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-809" +e.getMessage());
				Exception("HBC-809 There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-812 Verify color swatches should be shown when the product has more colors");
			if(HBC812==true)
			{
				Pass("'Color Swatches' shown when the product has colors");
			}
			else
			{
				Fail("'Color Swatches' not shown when the product has colors");
			}			
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-815 Verify that Selected color name string(eg : SELECT COLOR: XXXXXXXXX) should be shown near the color swatches/dropdown*/
	public void collectionsSelectedColorString() 
	{
		ChildCreation("HUDSONBAY-815 Verify that Selected color name string(eg : SELECT COLOR: XXXXXXXXX) should be shown near the color swatches/dropdown");
		if(collections == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC815", sheet, 1);
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr = 0;
					for(int i=1;i<=size;i++)
					{
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						Thread.sleep(200);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The sub product "+prodTitle+" details are below");
						try
						{
							String[] selected = driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[@id='pdpActiveColor']")).getText().split(":");
							log.add("Actual Selected color name String is: "+selected[0]);
							log.add("Expected Selected color name String is: "+expected);
							log.add("Displayed color is: "+selected[1]);
							if(selected[0].contains(expected)&&!selected[1].isEmpty())
							{
								Pass("(SELECT COLOR: XXXXXXXXX) shown near the color swatches/dropdown for the product:"+prodTitle+" ",log);
								ctr++;
							}
							else
							{
								Fail("(SELECT COLOR: XXXXXXXXX) not shown near the color swatches/dropdown for the product:"+prodTitle+"",log);
							}
						}
						catch (Exception e)
						{
							Fail("Color swatches not displayed for the subProduct "+prodTitle+"");
						}
					}
					if(size==ctr)
					{
						Pass("Selected color name string(eg : SELECT COLOR: XXXXXXXXX) shown near the color swatches/dropdown");
					}
					else
					{
						Fail("Selected color name string(eg : SELECT COLOR: XXXXXXXXX) not shown near the color swatches/dropdown");
					}			
				}
				else
				{
					Fail("subProduct container not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-815" +e.getMessage());
				Exception("HBC-815 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-816 Verify that Selected size name string(eg : SELECT SIZE: XXXXXXXXX) should be shown near the size swatches/dropdown*/
	public void collectionsSelectedSizeString() 
	{
		ChildCreation("HUDSONBAY-816 Verify that Selected size name string(eg : SELECT SIZE: XXXXXXXXX) should be shown near the size swatches/dropdown");
		if(collections == true)
		{
			try
			{
				String[] expected = HBCBasicfeature.getExcelVal("HBC816", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr = 0;
					for(int i=1;i<=size;i++)
					{
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						Thread.sleep(200);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The sub product "+prodTitle+" details are below");
						try
						{
							String defTxt = driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[@class='pdpActiveSize isSizeAvailable']")).getText();
							String[] sizeName = driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[@class='pdpActiveSize isSizeAvailable']")).getText().split(":");
							log.add("Actual Selected SIZE name String is: "+defTxt);
							log.add("Expected Selected SIZE name String is: "+expected);
							log.add("Displayed SIZE is: "+sizeName);
							if((sizeName[0].trim().contains(expected[0].trim())||sizeName[0].trim().contains(expected[1].trim()))&&!sizeName[1].isEmpty())
							{
								Pass("(SELECT SIZE: XXXXXXXXX) shown near the SIZE swatches/dropdown for the product:"+prodTitle+" ",log);
								ctr++;
							}
							else
							{
								Fail("(SELECT SIZE: XXXXXXXXX) not shown near the SIZE swatches/dropdown for the product:"+prodTitle+"",log);
							}
						}
						catch (Exception e)
						{
							Fail("SIZE swatches not displayed for the subProduct "+prodTitle+"");
						}
					}
					if(size==ctr)
					{
						Pass("Selected SIZE name string(eg : SELECT SIZE: XXXXXXXXX) shown near the SIZE swatches/dropdown");
					}
					else
					{
						Fail("Selected SIZE name string(eg : SELECT COLOR: XXXXXXXXX) not shown near the SIZE swatches/dropdown");
					}			
				}
				else
				{
					Fail("subProduct container not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-816" +e.getMessage());
				Exception("HBC-816 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
		
	/*HUDSONBAY-817 Verify that difference needs to be shown for selected/unselected color/size swatches*/
	public void collectionsSelectUnselectDifference() 
	{
		ChildCreation("HUDSONBAY-817 Verify that difference needs to be shown for selected/unselected color/size swatches");
		if(collections == true)
		{
			try
			{
				String SelectedExpectedColor  = HBCBasicfeature.getExcelVal("HBC817", sheet, 1);
				String UnSelectedExpectedColor  = HBCBasicfeature.getExcelVal("HBC817", sheet, 2);
				boolean flag = false;
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					for(int i=1;i<=size;i++)
					{
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						Thread.sleep(200);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						try
						{
							List<WebElement> Swatches = driver.findElements(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[contains(@class,'pdpSizeSelect pdpSelectEle')]"));
							int swatchSize = Swatches.size();
							if(swatchSize>1)
							{
								Pass("The sub product "+prodTitle+" has more than one size swatches");
								flag = true;
								WebElement swatchFirst = driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[contains(@class,'pdpSizeSelect pdpSelectEle')][1]"));
								swatchFirst.click();
								Thread.sleep(500);
								String selected =  driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[@id='pdpSizeContainer']//*[contains(@class,'selected')]")).getCssValue("border");
								//System.out.println(selected);
								String selectedColor = HBCBasicfeature.colorfinder(selected.substring(12));
								log.add("Actual selected color is: "+selectedColor);
								log.add("Expected selected color is: "+SelectedExpectedColor);
								Thread.sleep(200);
								String unSelected =  driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[contains(@class,'pdpSizeSelect pdpSelectEle')][2]")).getCssValue("border");
								String unselectedColor = HBCBasicfeature.colorfinder(unSelected.substring(12));
								log.add("Actual Unselected color is: "+unselectedColor);
								log.add("Expected Unselected color is: "+UnSelectedExpectedColor);
								Thread.sleep(200);
								if(selectedColor.equalsIgnoreCase(SelectedExpectedColor)&&unselectedColor.equalsIgnoreCase(UnSelectedExpectedColor))
								{
									Pass("Difference shown for selected/unselected color/size swatches",log);
								}
								else
								{
									Fail("Difference not shown for selected/unselected color/size swatches",log);
								}
								break;
							}
							else
							{
								continue;
							}
						}
						catch (Exception e)
						{
							Fail("Size swatches not displayed for the subProduct "+prodTitle+"");
						}
					}
					if(flag == true)
					{
						Pass("Selected product's Sub Product has more than one swatches");
					}
					else
					{
						Skip("Selected product's Sub Product has not more than one swatches");
					}
				}
				else
				{
					Fail("subProduct container not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println("HBC-817" +e.getMessage());
				Exception("HBC-817 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-811 Verify that on changing the color the image should be updated as per the color is selected in Collectibles PDP*/
	public void collectionsImageChange() 
	{
		ChildCreation("HUDSONBAY-811 Verify that on changing the color the image should be updated as per the color is selected in Collectibles PDP");
		if(collections == true)
		{
			boolean flag = false;
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					for(int i=1;i<=size;i++)
					{
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						Thread.sleep(200);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						try
						{
							List<WebElement> Swatches = driver.findElements(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[contains(@class,'pdpColorSwatch skMob_available')]"));
							int swatchSize = Swatches.size();
							if(swatchSize>1)
							{
								flag=true;
								Pass("The sub product "+prodTitle+" has more than one color swatches");
								String currentSwatch = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemImgLink']//img")).getAttribute("src");
								log.add("Current selected swatch Image URL is: "+currentSwatch);
								WebElement swatch = driver.findElement(By.xpath("((//*[@class='skMob_pdpBundleItemCont']["+i+"])//*[contains(@class,'pdpColorSwatch skMob_available')])[2]"));
								swatch.click();
								Thread.sleep(500);
								String updatedSwatch = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemImgLink']//img")).getAttribute("src");										
								log.add("After changed swatch color,current swatch Image URL is: "+updatedSwatch);
								if(currentSwatch.equalsIgnoreCase(updatedSwatch))
								{
									Fail("on changing the color the image not updated as per the color is selected in Collectibles PDP",log);
								}
								else
								{
									Pass("on changing the color the image updated as per the color is selected in Collectibles PDP",log);
								}
								break;
							}
							else
							{
								continue;
							}
							
						}
						catch (Exception e)
						{
							Fail("Color swatches not displayed for the subProduct "+prodTitle+"");
						}
					}
					if(flag==true)
					{
						Pass("Selected product's Sub Product has more than one color swatches");
					}
					else
					{
						Skip("Selected product's Sub Product has not more than one color swatches");
					}
				}
				else
				{
					Fail("subProduct container not displayed");
				}		
			}
			catch (Exception e)
			{
				System.out.println("HBC-811" +e.getMessage());
				Exception("HBC-811 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-819 Verify that Quantity increment(+) or decrement(-) buttons should be displayed below the product price in Collectibles PDP.*/
	public void collectionsQuantityIncrementDecrement() 
	{
		ChildCreation("HUDSONBAY-819 Verify that Quantity increment(+) or decrement(-) buttons should be displayed below the product price in Collectibles PDP");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr= 0;
					for(int i=1;i<=size;i++)
					{
						Pass("The sub product "+i+" details are below");
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The Product title displayed is: "+prodTitle);
						Thread.sleep(500);		
						WebElement quantityContainer = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_quantityCont']"));
						if(HBCBasicfeature.isElementPresent(quantityContainer))
						{
							boolean increment = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityPlus']")).isDisplayed();
							boolean decrement = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityMinus']")).isDisplayed();
							if(increment==true&&decrement==true)
							{
								String quantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
								log.add("Quantity dispalyed is: "+quantity);
								Pass("Quantity field with Increment and Decrement field is displayed",log);
							}						
							ctr++;
						}
						else
						{
							Fail("Quantity container not displayed");
						}
					}
					if(ctr==size)
					{
						Pass("All the sub products display 'Qty' field with increment/decrement below the product price in Collectibles PDP");
					}
					else
					{
						Fail("All the sub products not display 'Qty' field with increment/decrement ");
					}
				}
				else
				{
					Fail("Collection Product container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-819" +e.getMessage());
				Exception("HBC-819 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-820 Verify that Quantity should be incremented on selecting the Quantity increment button("+") in Collectibles PDP.*/
	/*HUDSONBAY-821 Verify that Quantity should be Decremented on selecting the Quantity Decrement button("-") in Collectibles PDP.*/
	public void collectionsQuantityIncDecClick() 
	{
		ChildCreation("HUDSONBAY-820 Verify that Quantity should be incremented on selecting the Quantity increment button("+") in Collectibles PDP");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					for(int i=1;i<=size;i++)
					{
						Pass("The sub product "+i+" details are below");
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The Product title displayed is: "+prodTitle);
						Thread.sleep(500);		
						WebElement quantityContainer = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_quantityCont']"));
						if(HBCBasicfeature.isElementPresent(quantityContainer))
						{
							HBCBasicfeature.scrolldown(quantityContainer, driver);
							WebElement increment = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityPlus']"));
							WebElement decrement = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityMinus']"));
							Thread.sleep(200);		
							boolean inc  = increment.isDisplayed();
							boolean dec  = decrement.isDisplayed();
							if(inc==true&&dec==true)
							{
								String bfreQuantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
								log.add("Before Increment Quantity dispalyed is: "+bfreQuantity);
								increment.click();
								Thread.sleep(500);
								String aftreQuantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
								log.add("After Increment Quantity dispalyed is: "+aftreQuantity);
								if(bfreQuantity!=aftreQuantity)
								{
									Pass("Quantity incremented on selecting the Quantity increment button("+")",log);
								}
								else
								{
									Fail("Quantity not incremented on selecting the Quantity increment button("+")",log);
								}
								Thread.sleep(500);
								String bfreDecQuantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
								log.add("Before Decrement Quantity dispalyed is: "+bfreDecQuantity);
								decrement.click();
								Thread.sleep(500);
								String aftreDecQuantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
								log.add("After Decrement Quantity dispalyed is: "+aftreDecQuantity);
								if(bfreDecQuantity!=aftreDecQuantity)
								{
									Pass("Quantity Decremented on selecting the Quantity Decrement button('-')",log);
								}
								else
								{
									Fail("Quantity not Decremented on selecting the Quantity Decrement button('-')",log);
								}							
							}
							else
							{
								Fail("Quantity Increment/Decrement container not displayed");
							}
						}
						else
						{
							Fail("Quantity container not displayed");
						}
					}
				}
				else
				{
					Fail("Collection Product container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-820" +e.getMessage());
				Exception("HBC-820 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
		
	/*HUDSONBAY-822 Verify that User can add a maximum of 2 sub products as per the classic site in Collectibles PDP.*/
	public void collectionsQuantityIncMaximum() 
	{
		ChildCreation("HUDSONBAY-822 Verify that User can add a maximum of 2 sub products as per the classic site in Collectibles PDP");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					for(int i=1;i<=size;i++)
					{
						Pass("The sub product "+i+" details are below");
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The Product title displayed is: "+prodTitle);
						Thread.sleep(500);		
						WebElement quantityContainer = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_quantityCont']"));
						if(HBCBasicfeature.isElementPresent(quantityContainer))
						{
							HBCBasicfeature.scrolldown(quantityContainer, driver);
							WebElement increment = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityPlus']"));
							Thread.sleep(200);		
							boolean inc  = increment.isDisplayed();
							int count = 0;
							if(inc==true)
							{
								String bfreQuantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
								count = Integer.parseInt(bfreQuantity);
								log.add("Before Increment Quantity dispalyed is: "+count);	
								boolean flag = false;
								do
								{
									try
									{
										increment.click();
									}
									catch(Exception e)
									{
										bfreQuantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
										count = Integer.parseInt(bfreQuantity);
										if(count==2)
										{
											flag = true;
											break;
										}
									}
									bfreQuantity = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='id_skMob_quantityText']")).getAttribute("value");
									count = Integer.parseInt(bfreQuantity);
								}
								while(count!=3);
								Thread.sleep(500);
								log.add("Maximum count dispalyed is: "+count);	
								if(flag==true&&count==2)
								{
									Pass("User can add a maximum of 2 sub products as per the classic site in Collectibles PDP",log);
								}
								else
								{
									Fail("User add more than 2 sub products in Collectibles PDP",log);
								}
							}
							else
							{
								Fail("Quantity Increment/Decrement container not displayed");
							}
						}
						else
						{
							Fail("Quantity container not displayed");
						}
					}
				}
				else
				{
					Fail("Collection Product container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-822" +e.getMessage());
				Exception("HBC-822 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-823 Verify that "Details" section alone should be shown for the sub products in the Collectibles PDP*/
	public void collectionsSubProductsDetailsSection() 
	{
		ChildCreation("HUDSONBAY-823 Verify that 'Details' section alone should be shown for the sub products in the Collectibles PDP");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(collectionProductContianer))
				{
					HBCBasicfeature.scrolldown(collectionProductContianer, driver);
					Pass("Collections product container displayed");
					int size = collectionProductList.size();
					int ctr = 0;
					for(int i=1;i<=size;i++)
					{
						Pass("The sub product "+i+" details are below");
						WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
						HBCBasicfeature.scrolldown(prod, driver);
						String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
						Pass("The Sub Product title is: "+prodTitle);
						Thread.sleep(500);		
						WebElement detailsSection = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdp_Description expandDetails']"));
						if(HBCBasicfeature.isElementPresent(detailsSection))
						{
							HBCBasicfeature.scrolldown(detailsSection, driver);
							boolean result = detailsSection.getText().isEmpty();
							if(result==false)
							{
								Pass("'Details' section alone shown for the sub product");
								ctr++;
							}
							else
							{
								Fail("'Details' section not shown for the sub product");
							}							
						}
						else
						{
							Fail("Details section not displayed");
						}
					}
					if(size==ctr)
					{
						Pass("'Details' section alone shown for the sub products in the Collectibles PDP");
					}
					else
					{
						Fail("'Details' section not shown for the sub products in the Collectibles PDP");
					}
				}
				else
				{
					Fail("Collection Product container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-823" +e.getMessage());
				Exception("HBC-823 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-824 Verify that Add to bag button should be displayed as per the creative in Collectibles PDP*/
	public void collectionsATBButton() throws Exception 
	{
		ChildCreation("HUDSONBAY-824 Verify that Add to bag button should be displayed as per the creative in Collectibles PDP");
		if(collections == true)
		{
			String expectedColor = HBCBasicfeature.getExcelVal("HBC824", sheet, 1);
			try
			{
				if(HBCBasicfeature.isElementPresent(pdppageATB))
				{
					HBCBasicfeature.scrolldown(pdppageATB, driver);
					Pass(" Add to bag button displayed");
					String color = pdppageATB.getCssValue("background").substring(0, 12);
					//System.out.println(color);
					String actualColor = HBCBasicfeature.colorfinder(color);
					log.add("The Actual color displayed is: "+actualColor);
					log.add("The Expected color displayed is: "+expectedColor);
					if(actualColor.equalsIgnoreCase(expectedColor))
					{
						Pass(" Add to bag button displayed as per the creative in Collectibles PDP",log);
					}
					else
					{
						Fail("Add to bag button not displayed as per the creative in Collectibles PDP",log);
					}				
				}
				else
				{
					Fail("Add to bag button not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-824" +e.getMessage());
				Exception("HBC-824 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-825 Verify that "Add to Bag" success overlay should be shown on selecting the Add to bag button in Collectibles PDP*/
	public void collectionsATBButtonClick() throws Exception 
	{
		ChildCreation("HUDSONBAY-825 Verify that 'Add to Bag' success overlay should be shown on selecting the Add to bag button in Collectibles PDP");
		if(collections == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(pdppageATB))
				{
					HBCBasicfeature.scrolldown(pdppageATB, driver);
					Pass(" Add to bag button displayed");
					HBCBasicfeature.jsclick(pdppageATB, driver);
					wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
					wait.until(ExpectedConditions.attributeContains(pdppageATBOverlay, "style", "block"));
					if(HBCBasicfeature.isElementPresent(pdppageATBOverlay))
					{
						Pass("'Add to Bag' success overlay shown on selecting the Add to bag button in Collectibles PDP");
						HBCBasicfeature.jsclick(pdppageATBOverlayClose, driver);
					}
					else
					{
						Fail("'Add to Bag' success overlay not shown");
					}				
				}
				else
				{
					Fail("Add to bag button not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-825" +e.getMessage());
				Exception("HBC-825 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("PDP collections Page not displayed");
		}
	}
	
	/*HUDSONBAY-826 Verify that social icons such as "Facebook", "Google+", "Twitter" and so on should be shown for hitting the like for the product in Collectibles PDP*/
	public void collectionsSocialIcons()
		{
			ChildCreation("HUDSONBAY-826 Verify that social icons such as Facebook, Google+, Twitter and so on should be shown for hitting the like for the product in Collectibles PDP");
			if(collections == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageShareIcons))
					{
						HBCBasicfeature.scrolldown(pdppageShareIcons, driver);
						Pass("Share Icons container displayed");
						if(HBCBasicfeature.isElementPresent(pdppageShareTwitter))
						{
							Pass("Social Icon Twitter displayed");
						}
						else
						{
							Fail("Social Icon Twitter not displayed");
						}
						if(HBCBasicfeature.isElementPresent(pdppageShareGplus))
						{
							Pass("Social Icon Google+ displayed");
						}
						else
						{
							Fail("Social Icon Google+ not displayed");
						}
						if(HBCBasicfeature.isElementPresent(pdppageShareFB))
						{
							Pass("Social Icon FB+ displayed");
						}
						else
						{
							Fail("Social Icon FB not displayed");
						}
					}
					else
					{
						Fail("PDP share Icons container not displayed");
					}				
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-826" +e.getMessage());
					Exception("HUDSONBAY-826 There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("PDP collections Page not displayed");
			}
		}
	}
	
