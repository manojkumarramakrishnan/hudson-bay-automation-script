package hbPageObjects;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_MyAccountLanding;

public class Hudson_Bay_MyAccountLanding_Obj extends Hudson_Bay_MyAccountLanding implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_MyAccountLanding_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+homepagee+"")
	WebElement homepage;	
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;	
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+pancakeStaticWelcomee+"")
	WebElement pancakeStaticWelcome;	
	
	@FindBy(xpath = ""+signInPagee+"")
	WebElement signInPage;
	
	@FindBy(xpath = ""+emailFieldd+"")
	WebElement emailField;
	
	@FindBy(xpath = ""+pwdFieldd+"")
	WebElement pwdField;
	
	@FindBy(xpath = ""+signInButtonn+"")
	WebElement signInButton;
	
	@FindBy(xpath = ""+myAccountPagee+"")
	WebElement myAccountPage;
	
	@FindBy(xpath = ""+myAccountPageTitlee+"")
	WebElement myAccountPageTitle;
	
	@FindBy(xpath = ""+myAccountPageWelcomee+"")
	WebElement myAccountPageWelcome;
	
	@FindBy(xpath = ""+myAccountPageMyProfileTxtt+"")
	WebElement myAccountPageMyProfileTxt;
	
	@FindBy(xpath = ""+myAccountPageMyProfileEditt+"")
	WebElement myAccountPageMyProfileEdit;
	
	@FindBy(xpath = ""+myAccountPageMyProfileDetailss+"")
	WebElement myAccountPageMyProfileDetails;
	
	@FindBy(xpath = ""+myAccountPageMenuListt+"")
	WebElement myAccountPageMenuList;
	
	@FindBy(how = How.XPATH,using = ""+myAccountPageMenuListt+"")
	List<WebElement> myAccountPageMenuListtt;
	
	@FindBy(xpath = ""+myAccountPageSignOutt+"")
	WebElement myAccountPageSignOut;
	
	@FindBy(xpath = ""+myAccountPageAddressbookk+"")
	WebElement myAccountPageAddressbook;	
	
	@FindBy(xpath = ""+myAccountAddressBookpagee+"")
	WebElement myAccountAddressBookpage;	
	
	@FindBy(xpath = ""+myAccountPageMyProfilee+"")
	WebElement myAccountPageMyProfile;
	
	@FindBy(xpath = ""+myAccountPageNotificationPreff+"")
	WebElement myAccountPageNotificationPref;
	
	@FindBy(xpath = ""+myAccountNotifPreferncePagee+"")
	WebElement myAccountNotifPreferncePage;		
	
	@FindBy(xpath = ""+myAccountPageMyOrderss+"")
	WebElement myAccountPageMyOrders;
	
	@FindBy(xpath = ""+myAccountMyOrdersPagee+"")
	WebElement myAccountMyOrdersPage;		
	
	@FindBy(xpath = ""+myAccountPageMyWishlistt+"")
	WebElement myAccountPageMyWishlist;
	
	@FindBy(xpath = ""+WishListEmptyWishh+"")
	WebElement WishListEmptyWish;
	
	@FindBy(xpath = ""+myWishlistEmailWishh+"")
	WebElement myWishlistEmailWish;
		
	
	@FindBy(xpath = ""+myAccountMyProfilepagee+"")
	WebElement myAccountMyProfilepage;
	
	@FindBy(xpath = ""+myAccountpageBackk+"")
	WebElement myAccountpageBack;
	
	
	boolean MyAccount = false;
	
	/*SignIn page to MyAccount Page*/
	public void signin() throws java.lang.Exception
	{
		ChildCreation("SignIn page to MyAccount Page");
		String email = HBCBasicfeature.getExcelVal("login", sheet, 2);
		String pwd = HBCBasicfeature.getExcelVal("login", sheet, 1);
		try
		{
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{	
				boolean flag = false;
				do
				{					
					try
					{						
						do
						{
							try
							{
								HBCBasicfeature.jsclick(hamburger, driver);
								Thread.sleep(2000);
								if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
								{
									flag = true;
									break;
								}
							}
							catch(Exception e)
							{
								continue;
							}
						}
						while(flag!=true);		
						if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
						{
							flag = false;
							HBCBasicfeature.scrolldown(pancakeStaticWelcome, driver);
							pancakeStaticWelcome.click();
							try
							{
								wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
								flag = true;
								break;
							}
							catch(Exception e)
							{
								flag = false;
								continue;
							}
						}
						else
						{
							continue;
						}
					}
					catch(Exception e)
					{
						driver.navigate().refresh();
						continue;
					}
				}
				while(flag!=true);	
				wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
				if(HBCBasicfeature.isElementPresent(emailField))
				{	
					emailField.sendKeys(email);								
					log.add("Entered Email ID is: "+ email);
					if(HBCBasicfeature.isElementPresent(pwdField))
					{	
						pwdField.sendKeys(pwd);									
						log.add("Entered Password is: "+ pwd);
						if(HBCBasicfeature.isElementPresent(signInButton))
						{
							signInButton.click();
							Thread.sleep(500);
							wait.until(ExpectedConditions.visibilityOf(myAccountPage));			
							if(HBCBasicfeature.isElementPresent(myAccountPage))
							{	
								Pass("MyAccount Page displayed",log);
								MyAccount = true;
							}
							else
							{
								Fail("MyAccount page not displayed",log);
							}
						}
						else
						{
							Fail("SignIn button not displayed");
						}
					}
					else
					{
						Fail("Password Field not displayed");
					}
				}
				else
				{
					Fail("Email Field not displayed");
				}
			}		
			else
			{
				Fail("Hamburger menuu not displayed");
			}					
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-1955 Verify that "My Account" page should be displayed as per the creative*/
	public void MyAccountpageCreative() 
	{
		ChildCreation("HUDSONBAY-1955 Verify that 'My Account' page should be displayed as per the creative");
		if(MyAccount == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPage))
				{
					log.add("MyAccount Page displayed");
					if(HBCBasicfeature.isElementPresent(myAccountPageTitle))
					{
						Pass("MyAccount Page Title is displayed");
					}
					else
					{
						Fail("MyAccount Page Title is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAccountPageWelcome))
					{
						Pass("MyAccount Page Welcome text is displayed");
					}
					else
					{
						Fail("MyAccount Page Welcome text is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
					{
						Pass("MyAccount Page Profile details are displayed");
					}
					else
					{
						Fail("MyAccount Page Profile details are not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAccountPageSignOut))
					{
						Pass("MyAccount Page Signout link is displayed");
					}
					else
					{
						Fail("MyAccount Page Signout link is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(myAccountPageMenuList))
					{
						Pass("MyAccount Page options are displayed");
					}
					else
					{
						Fail("MyAccount Page options are not displayed");
					}
					/*if(HBCBasicfeature.isElementPresent(signupEmails))
					{
						Pass("MyAccount Page SignupEmails button is displayed");
					}
					else
					{
						Fail("MyAccount Page SignupEmails button is not displayed");
					}	*/
					if(HBCBasicfeature.isElementPresent(footerContainer))
					{
						Pass("MyAccount Page Footer Container is displayed");
					}
					else
					{
						Fail("MyAccount Page Footer Container is not displayed");
					}			
				}
				else
				{
					Fail("MyAccount page is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1955" +e.getMessage());
				Exception("HBC-1955 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1956 Verify that "My Account" page should have the following sections "My Profile", "My Address Book", "Notification Preferences", "My Orders", "My Wish List"*/
	public void MyAccountpageSections() 
	{
		ChildCreation("HUDSONBAY-1956 Verify that 'My Account' page should have the following sections 'My Profile', 'My Address Book', 'Notification Preferences', 'My Orders', 'My Wish List'");
		if(MyAccount == true)
		{
			try
			{
				String[] actual = HBCBasicfeature.getExcelVal("HBC1956", sheet, 3).split("\n");			
				if(HBCBasicfeature.isListElementPresent(myAccountPageMenuListtt))
				{
					int expctedsize = myAccountPageMenuListtt.size();	
					int actualSize = actual.length;
					log.add("Expected sections size:  "+ expctedsize);
					log.add("Actual sections size:  "+ actualSize);
					if(expctedsize==actualSize)
					{
						for(int i=1;i<=expctedsize;i++)
						{
							WebElement section = driver.findElement(By.xpath("//*[@class='subleftmenu']//li["+i+"]//a"));
							String expected = section.getText();						
							if(expected.equalsIgnoreCase(actual[i-1]))
							{
								Pass("'My Account' page has the following section: "+ expected,log);
							}
							else
							{
								Fail("'My Account' page don't have the following section: "+ expected,log);
							}
						}
					}
					else
					{
						Fail("Expected Section size and Actual size are not equal",log);
					}
				}
				else
				{
					Fail("'My Account' page sections not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1956" +e.getMessage());
				Exception("HBC-1956 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1957 Verify that by default "My Profile" section is displayed on tapping the "My Account" link*/
	/*HUDSONBAY-1965 Verify that 'My Profile' section should be shown in the 'My Account' page*/
	public void MyAccountpageDefaultView() 
	{
		ChildCreation("HUDSONBAY-1957 Verify that by default 'My Profile' section is displayed on tapping the 'My Account' link");
		boolean HBC1965 = false;
		if(MyAccount == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileTxt))
				{
					log.add("My Profile text is displayed");
					if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileDetails))
					{
						log.add("Displayed MyProfile details are: "+myAccountPageMyProfileDetails.getText());
						Pass("By default 'My Profile' section is displayed on tapping the 'My Account' link",log);
						HBC1965 = true;
					}
					else
					{
						Fail("By default 'My Profile' section not displayed on tapping the 'My Account' link",log);
					}
				}
				else
				{
					Fail("MyProfile title text is not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-1957" +e.getMessage());
				Exception("HBC-1957 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-1965 Verify that 'My Profile' section should be shown in the 'My Account' page");
			if(HBC1965==true)
			{
				Pass("'My Profile' section shown in the 'My Account' page");
			}
			else
			{
				Fail("'My Profile' section not shown in the 'My Account' page");
			}		
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1962 Verify that "Sign Out" link should be shown in the right top corner of the My Account page*/
	public void MyAccountpageSignOutLink() 
	{
		ChildCreation("HUDSONBAY-1962 Verify that 'Sign Out' link should be shown in the right top corner of the My Account page");
		if(MyAccount == true)
		{
			try
			{
				String actual = HBCBasicfeature.getExcelVal("HBC1962", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myAccountPageSignOut))
				{
					String expected = myAccountPageSignOut.getText(); 
					log.add("Actual text is: "+actual);
					log.add("Expected text is: "+expected);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("'Sign Out' link shown in the right top corner of the My Account page",log);
					}
					else
					{
						Fail("'Sign Out' link not shown in the right top corner of the My Account page",log);
					}
				}
				else
				{
					Fail("'Sign Out' link not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1962" +e.getMessage());
				Exception("HBC-1962 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1966 Verify that "Edit" link should be shown in the top right corner of the "My Profile" section*/
	public void MyAccountpageEditProfile() 
	{
		ChildCreation("HUDSONBAY-1966 Verify that 'Edit' link should be shown in the top right corner of the 'My Profile' section");
		if(MyAccount == true)
		{
			try
			{
				String actual = HBCBasicfeature.getExcelVal("HBC1966", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileEdit))
				{
					String expected = myAccountPageMyProfileEdit.getText(); 
					log.add("Actual text is: "+actual);
					log.add("Expected text is: "+expected);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("'Edit' link shown in the right top corner of the My Profile section",log);
					}
					else
					{
						Fail("'Edit' link not shown in the right top corner of the  My Profile section",log);
					}
				}
				else
				{
					Fail("'Edit' link not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1966" +e.getMessage());
				Exception("HBC-1966 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1964 Verify that "Welcome, XXXX" string should be shown below the "My Account" title string*/
	public void MyAccountpageWelcomeString() 
	{
		ChildCreation("HUDSONBAY-1964 Verify that 'Welcome, XXXX' string should be shown below the 'My Account' title string");
		if(MyAccount == true)
		{
			try
			{
				String actual = HBCBasicfeature.getExcelVal("HBC1964", sheet, 1);
				if(HBCBasicfeature.isElementPresent(myAccountPageWelcome))
				{
					String expected = myAccountPageWelcome.getText(); 
					log.add("Actual text is: "+actual);
					log.add("Expected text is: "+expected);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("'Edit' link shown in the right top corner of the My Profile section",log);
					}
					else
					{
						Fail("'Edit' link not shown in the right top corner of the  My Profile section",log);
					}
				}
				else
				{
					Fail("'Edit' link not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1964" +e.getMessage());
				Exception("HBC-1964 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1967 Verify that Separation line should be shown for each sections*/
	public void MyAccountpageSeparationSectionLine() 
	{
		ChildCreation("HUDSONBAY-1967 Verify that seperation line should be shown for each sections");
		if(MyAccount == true)
		{
			try
			{
				String actual = HBCBasicfeature.getExcelVal("HBC1967", sheet, 2);
				if(HBCBasicfeature.isListElementPresent(myAccountPageMenuListtt))
				{
					int size = myAccountPageMenuListtt.size();					
					for(int i=1;i<=size;i++)
					{
						WebElement section = driver.findElement(By.xpath("//*[@class='subleftmenu']//li["+i+"]"));
						String sectionName = driver.findElement(By.xpath("//*[@class='subleftmenu']//li["+i+"]//a")).getText();
						Thread.sleep(200);
						String sections = section.getCssValue("border-bottom").substring(12);
						String expected = HBCBasicfeature.colorfinder(sections);
						log.add("Actual seperation line color is: "+actual);
						log.add("Expected seperation line color is: "+expected);
						if(actual.equalsIgnoreCase(expected))
						{
							Pass("seperation line shown for section: "+sectionName,log);
						}
						else
						{
							Fail("seperation line  not shown for section: "+sectionName,log);
						}
					}					
				}
				else
				{
					Fail("'Edit' link not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1967" +e.getMessage());
				Exception("HBC-1967 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1968 Verify that on tapping the"Edit" link near My Profile section, "My Profile" page will be shown*/
	public void MyAccountpageEditProfilePage() 
	{
		ChildCreation("HUDSONBAY-1968 Verify that on tapping the 'Edit' link near My Profile section, 'My Profile' page will be shown");
		if(MyAccount == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageMyProfileEdit))
				{
					HBCBasicfeature.jsclick(myAccountPageMyProfileEdit, driver);
					wait.until(ExpectedConditions.visibilityOf(myAccountMyProfilepage));
					if(HBCBasicfeature.isElementPresent(myAccountMyProfilepage))
					{
						MyAccount = false;
						Pass("on tapping the 'Edit' link near My Profile section, 'My Profile' page shown");
						HBCBasicfeature.jsclick(myAccountpageBack, driver);
						wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
						MyAccount = true;
					}
					else
					{
						Fail("on tapping the 'Edit' link near My Profile section, 'My Profile' page not shown");
					}					
				}
				else
				{
					Fail("'Edit' link near My Profile section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1968" +e.getMessage());
				Exception("HBC-1968 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1958 Verify that "Address Book" page should be shown on tapping the "My Address Book" link*/
	public void MyAccountpageToMyAddressBook() 
	{
		ChildCreation("HUDSONBAY-1958 Verify that 'Address Book' page should be shown on tapping the 'My Address Book' link");
		if(MyAccount == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageAddressbook))
				{
					myAccountPageAddressbook.click();
					wait.until(ExpectedConditions.visibilityOf(myAccountAddressBookpage));
					if(HBCBasicfeature.isElementPresent(myAccountAddressBookpage))
					{
						MyAccount = false;
						Pass("on tapping the 'My Address Book' link , 'Address Book' page shown");
						HBCBasicfeature.jsclick(myAccountpageBack, driver);
						wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
						MyAccount = true;
					}
					else
					{
						Fail("on tapping the 'My Address Book' link , 'Address Book' page not shown");
					}					
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1958" +e.getMessage());
				Exception("HBC-1958 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1959 Verify that "Notification Preferences" page should be shown on tapping the "Notification Preferences" link*/
	public void MyAccountpageToNotificationPreferences() 
	{
		ChildCreation("HUDSONBAY-1959 Verify that 'Notification Preferences' page should be shown on tapping the 'Notification Preferences' link");
		if(MyAccount == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageNotificationPref))
				{
					myAccountPageNotificationPref.click();
					wait.until(ExpectedConditions.visibilityOf(myAccountNotifPreferncePage));
					if(HBCBasicfeature.isElementPresent(myAccountNotifPreferncePage))
					{
						MyAccount = false;
						Pass("on tapping the 'Notification Preferences' link , 'Notification Preferences' page shown");
						HBCBasicfeature.jsclick(myAccountpageBack, driver);
						wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
						MyAccount = true;
					}
					else
					{
						Fail("on tapping the Notification Preferences' link , 'Notification Preferences' page not shown");
					}					
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1959" +e.getMessage());
				Exception("HBC-1959 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1960 Verify that "My Orders" page should be shown on tapping the "My Orders" link*/
	public void MyAccountpageToMyOrders() 
	{
		ChildCreation("HUDSONBAY-1960 Verify that 'My Orders' page should be shown on tapping the 'My Orders' link");
		if(MyAccount == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageMyOrders))
				{
					myAccountPageMyOrders.click();
					wait.until(ExpectedConditions.visibilityOf(myAccountMyOrdersPage));
					if(HBCBasicfeature.isElementPresent(myAccountMyOrdersPage))
					{
						MyAccount = false;
						Pass("on tapping the 'My Orders' link , 'My Orders' page shown");
						HBCBasicfeature.jsclick(myAccountpageBack, driver);
						wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
						MyAccount = true;
					}
					else
					{
						Fail("on tapping the 'My Orders' link , 'My Orders' page not shown");
					}					
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1960" +e.getMessage());
				Exception("HBC-1960 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1961 Verify that "My Wish List" page should be shown on tapping the "My Wish List" link*/
	public void MyAccountpageToMyWishList() 
	{
		ChildCreation("HUDSONBAY-1961 Verify that 'My Wish List' page should be shown on tapping the 'My Wish List' link");
		if(MyAccount == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageMyWishlist))
				{
					myAccountPageMyWishlist.click();
					wait.until(ExpectedConditions.attributeContains(myAccountPage, "style", "visible"));
					if(HBCBasicfeature.isElementPresent(WishListEmptyWish)||HBCBasicfeature.isElementPresent(myWishlistEmailWish))
					{
						MyAccount = false;
						Pass("on tapping the 'My Wish List' link , 'My Wish List' page shown");
						HBCBasicfeature.jsclick(myAccountpageBack, driver);
						wait.until(ExpectedConditions.visibilityOf(myAccountPageMyProfileTxt));
						MyAccount = true;
					}
					else
					{
						Fail("on tapping the 'My Wish List' link , 'My Wish List' page not shown");
					}					
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1961" +e.getMessage());
				Exception("HBC-1961 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	/*HUDSONBAY-1963 Verify that on tapping "Sign Out" link the current reg user should be logged out and home page should be shown*/
	public void MyAccountpageSignOut() 
	{
		ChildCreation("HUDSONBAY-1963 Verify that on tapping 'Sign Out' link the current reg user should be logged out and home page should be shown");
		if(MyAccount == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(myAccountPageSignOut))
				{
					myAccountPageSignOut.click();
					wait.until(ExpectedConditions.visibilityOf(homepage));
					if(HBCBasicfeature.isElementPresent(myAccountPage))
					{
						Fail("on tapping 'Sign Out' link the current reg user not logged out");
					}
					else
					{
						Pass("on tapping 'Sign Out' link the current reg user logged out and home page shown");
					}
				}
				else
				{
					Fail("'Sign Out' link not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1963" +e.getMessage());
				Exception("HBC-1963 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("MyAccount Page not displayed");
		}
	}
	
	
}

