package hbPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.WordUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_GCkout;

public class Hudson_Bay_GC_Obj extends Hudson_Bay_GCkout implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
	static boolean prdtAdded = false, addVerEnable = false, shipPage = false, secAddressAdd = false, reviewSubmit = false, reviewSub = false, orderConfirm = false;
	static String befAdd = "", paybefAdd = "", sbefAdd = "", payDefSelCountr = "", addressCountry = "",  shipAddress = "", billAddress = "", paymethod = "", shipMethod = "";	
	
	public Hudson_Bay_GC_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	/*@FindBy(xpath = ""+bagIconn+"")
	WebElement header;*/
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+footerTopp+"")
	WebElement footerTop;	
	
	@FindBy(xpath = ""+footerTopTitlee+"")
	WebElement footerTopTitle;
	
	@FindBy(xpath = ""+footerSubTopp+"")
	WebElement footerSubTop;
	
	@FindBy(xpath = ""+footerSubTopCalll+"")
	WebElement footerSubTopCall;
	
	@FindBy(xpath = ""+footerSubTopEmaill+"")
	WebElement footerSubTopEmail;
	
	@FindBy(xpath = ""+footerMenuListt+"")
	WebElement footerMenuList;
	
	@FindBy(xpath = "//*[@class='footer-menu-list'][1]")
	WebElement footerMenuShopHBC;	
	
	@FindBy(xpath = "(//*[@class='sk_footerAccordIcon'])[1]")
	WebElement footerAccordShopHBC;	
	
	@FindBy(xpath = "//*[@class='footer-menu-list'][2]")
	WebElement footerMenuStoreCorp;	
	
	@FindBy(xpath = "(//*[@class='sk_footerAccordIcon'])[2]")
	WebElement footerAccordStoreCorp;	
	
	@FindBy(xpath = "//*[@class='footer-menu-list'][3]")
	WebElement footerMenuCustomerPolicy;	
	
	@FindBy(xpath = "(//*[@class='sk_footerAccordIcon'])[3]")
	WebElement footerAccordCustomerPolicy;	
	
	@FindBy(xpath = "//*[@class='footer-menu-list'][4]")
	WebElement footerHBCRewards;	
	
	@FindBy(xpath = "(//*[@class='sk_footerAccordIcon'])[4]")
	WebElement footerAccordHBCRewards;
	
	@FindBy(xpath = ""+footerBottomm+"")
	WebElement footerBottom;
	
	@FindBy(xpath = ""+footerSocialIconn+"")
	WebElement footerSocialIcon;
	
	@FindBy(xpath = ""+footerBottomCopyrightt+"")
	WebElement footerBottomCopyright;
	
	@FindBy(xpath = ""+footerMenuListActivee+"")
	WebElement footerMenuListActive;
	
	@FindBy(xpath = ""+footerMenuWrapperr+"")
	WebElement footerMenuWrapper;	
	
	@FindBy(xpath = ""+footerSocialFBb+"")
	WebElement footerSocialFB;
	
	@FindBy(xpath = ""+footerSocialPinn+"")
	WebElement footerSocialPin;
	
	@FindBy(xpath = ""+footerSocialInstaa+"")
	WebElement footerSocialInsta;
	
	@FindBy(xpath = ""+footerSocialTwitterr+"")
	WebElement footerSocialTwitter;
	
	@FindBy(xpath = ""+footerSocialYouu+"")
	WebElement footerSocialYou;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+noSearchRess+"")
	WebElement noSearchRes;
	
	@FindBy(xpath = ""+noPrdtFoundd+"")
	WebElement noPrdtFound;
	
	@FindBy(xpath = ""+pdpPageWrapperr+"")
	WebElement pdpPageWrapper;
	
	@FindBy(xpath = ""+PDPProdNamee+"")
	WebElement PDPProdName;
	
	@FindBy(xpath = ""+pdppageATBb+"")
	WebElement pdppageATB;
	
	@FindBy(xpath = ""+loadingbarr+"")
	WebElement loadingbar;
	
	@FindBy(xpath = ""+bagIconCountt+"")
	WebElement bagIconCount;
	
	@FindBy(xpath = ""+pdpSuccessIdd+"")
	WebElement pdpSuccessId;
	
	@FindBy(xpath = ""+pdppageATBOverlayy+"")
	WebElement pdppageATBOverlay;
	
	@FindBy(xpath = ""+pdpcheckOutBtnn+"")
	WebElement pdpcheckOutBtn;
	
	@FindBy(xpath = ""+shoppingCartPagee+"")
	WebElement shoppingCartPage;
	
	@FindBy(xpath = ""+shoppingCartCheckoutt+"")
	WebElement shoppingCartCheckout;
	
	@FindBy(xpath = ""+guestSignInPopupp+"")
	WebElement guestSignInPopup;
	
	@FindBy(xpath = ""+guestContinueBtnn+"")
	WebElement guestContinueBtn;

	@FindBy(xpath = ""+checkoutNavTitlee+"")
	WebElement checkoutNavTitle;

	@FindBy(xpath = ""+checkoutCurrNavTitlee+"")
	WebElement checkoutCurrNavTitle;
	
	@FindBy(xpath = ""+checkoutNavTabb1+"")
	WebElement checkoutNavTab1;
	
	@FindBy(xpath = ""+checkoutNavTabb2+"")
	WebElement checkoutNavTab2;
	
	@FindBy(xpath = ""+checkoutNavTabb3+"")
	WebElement checkoutNavTab3;
	
	@FindBy(xpath = ""+firstNamee+"")
	WebElement firstName;
	
	@FindBy(xpath = ""+lastNamee+"")
	WebElement lastName;
	
	@FindBy(xpath = ""+staddresss+"")
	WebElement staddress;
	
	@FindBy(xpath = ""+staddresss2+"")
	WebElement staddress2;
	
	@FindBy(xpath = ""+cityy+"")
	WebElement city;
	
	@FindBy(xpath = ""+bcountrySelectionn+"")
	WebElement bcountrySelection;
	
	@FindBy(xpath = ""+bStatee+"")
	WebElement bState;
	
	@FindBy(xpath = ""+bukStatee+"")
	WebElement bukState;
	
	@FindBy(xpath = ""+bukStateEnterr+"")
	WebElement bukStateEnter;
		
	@FindBy(xpath = ""+bpostalCodee+"")
	WebElement bpostalCode;
	
	@FindBy(xpath = ""+buspostalCodee+"")
	WebElement buspostalCode;
	
	/*@FindBy(xpath = ""+bcapostalCodee+"")
	WebElement bcapostalCode;*/
	
	@FindBy(xpath = ""+bContactNoo+"")
	WebElement bContactNo;
	
	@FindBy(xpath = ""+bextCodee+"")
	WebElement bextCode;
	
	@FindBy(xpath = ""+brewardssIdd+"")
	WebElement brewardssId;
	
	@FindBy(xpath = ""+brewardsscardNumm+"")
	WebElement brewardsscardNum;

	@FindBy(xpath = ""+checkoutAddressButtonss+"")
	WebElement checkoutAddressButtons;
	
	@FindBy(xpath = ""+emaill+"")
	WebElement email;
	
	@FindBy(xpath = ""+emailDisclaimerTxtt+"")
	WebElement emailDisclaimerTxt;
	
	@FindBy(xpath = ""+termsandCondd+"")
	WebElement termsandCond;
	
	@FindBy(xpath = ""+termsandConddChkBoxx+"")
	WebElement termsandConddChkBox;
		
	@FindBy(xpath = ""+checkoutSameAsBillingAddresss+"")
	WebElement checkoutSameAsBillingAddress;
	
	@FindBy(xpath = ""+checkoutBFirstNameAlertt+"")
	WebElement checkoutBFirstNameAlert;
	
	@FindBy(xpath = ""+checkoutSFirstNameAlertt+"")
	WebElement checkoutSFirstNameAlert;
	
	@FindBy(xpath = ""+checkoutBLastNameAlertt+"")
	WebElement checkoutBLastNameAlert;
	
	@FindBy(xpath = ""+checkoutSLastNameAlertt+"")
	WebElement checkoutSLastNameAlert;
	
	@FindBy(xpath = ""+checkoutBStaddressAlertt+"")
	WebElement checkoutBStaddressAlert;
	
	@FindBy(xpath = ""+checkoutSStaddressAlertt+"")
	WebElement checkoutSStaddressAlert;
	
	@FindBy(xpath = ""+checkoutBCityAlertt+"")
	WebElement checkoutBCityAlert;
	
	@FindBy(xpath = ""+checkoutSCityAlertt+"")
	WebElement checkoutSCityAlert;
	
	@FindBy(xpath = ""+checkoutBUKStateAlertt+"")
	WebElement checkoutBUKStateAlert;
	
	@FindBy(xpath = ""+checkoutBPOAlertt+"")
	WebElement checkoutBPOAlert;
	
	/*@FindBy(xpath = ""+checkoutBcaPOAlertt+"")
	WebElement checkoutBcaPOAlert;
	
	@FindBy(xpath = ""+checkoutScaPOAlertt+"")
	WebElement checkoutScaPOAlert;*/
	
	@FindBy(xpath = ""+checkoutBcaPHAlertt+"")
	WebElement checkoutBcaPHAlert;
	
	@FindBy(xpath = ""+checkoutScaPHAlertt+"")
	WebElement checkoutScaPHAlert;
	
	@FindBy(xpath = ""+checkoutSPHAlertt1+"")
	WebElement checkoutSPHAlert1;
	
	@FindBy(xpath = ""+checkoutSPHAlertt3+"")
	WebElement checkoutSPHAlert3;
	
	@FindBy(xpath = ""+checkoutBcaRWDAlertt+"")
	WebElement checkoutBcaRWDAlert;
	
	@FindBy(xpath = ""+checkoutBcaEmailAlertt+"")
	WebElement checkoutBcaEmailAlert;
	
	@FindBy(xpath = ""+checkoutshipAddressFormm+"")
	WebElement checkoutshipAddressForm;
	
	@FindBy(xpath = ""+sfirstNamee+"")
	WebElement sfirstName;
	
	@FindBy(xpath = ""+slastNamee+"")
	WebElement slastName;
	
	@FindBy(xpath = ""+sstaddresss+"")
	WebElement sstaddress;
	
	@FindBy(xpath = ""+sstaddresss2+"")
	WebElement sstaddress2;
	
	@FindBy(xpath = ""+scityy+"")
	WebElement scity;
	
	@FindBy(xpath = ""+scountrySelectionn+"")
	WebElement scountrySelection;
	
	@FindBy(xpath = ""+sStatee+"")
	WebElement sState;
	
	@FindBy(xpath = ""+spostalCodee+"")
	WebElement spostalCode;
	
	@FindBy(xpath = ""+sContactNoo+"")
	WebElement sContactNo;
	
	@FindBy(xpath = ""+sextCodee+"")
	WebElement sextCode;
	
	@FindBy(xpath = ""+bStateStatee+"")
	WebElement bStateState;
	
	@FindBy(xpath = ""+gccheckoutbillingAddressTitlee+"")
	WebElement gccheckoutbillingAddressTitle;
	
	@FindBy(xpath = ""+gccheckoutshippingAddressTitlee+"")
	WebElement gccheckoutshippingAddressTitle;
	
	@FindBy(xpath = ""+addressVerificationoverlayy+"")
	WebElement addressVerificationoverlay;
	
	@FindBy(xpath = ""+addressuseasEnteredd+"")
	WebElement addressuseasEntered;
	
	@FindBy(xpath = ""+addressVerificationEditt+"")
	WebElement addressVerificationEdit;
	
	@FindBy(xpath = ""+progressBarWidgett+"")
	WebElement progressBarWidget;
	
	@FindBy(xpath = ""+shipandPaypagee+"")
	WebElement shipandPaypage;
	
	@FindBy(xpath = ""+shipandPaypageShipSecc+"")
	WebElement shipandPaypageShipSec;
	
	@FindBy(xpath = ""+shipandPaypagePaySecc+"")
	WebElement shipandPaypagePaySec;
	
	@FindBy(xpath = ""+shipandPaypageShipEditLinkk+"")
	WebElement shipandPaypageShipEditLink;
	
	@FindBy(xpath = ""+shipandPaypagePayEditLinkk+"")
	WebElement shipandPaypagePayEditLink;
	
	@FindBy(xpath = ""+shipandPaypageShipCreateAddLinkk+"")
	WebElement shipandPaypageShipCreateAddLink;
	
	@FindBy(xpath = ""+shipandPaypagePayCreateAddLinkk+"")
	WebElement shipandPaypagePayCreateAddLink;
	
	@FindBy(xpath = ""+shipandPaypageShipShipToNamee+"")
	WebElement shipandPaypageShipShipToName;
	
	@FindBy(xpath = ""+shipandPaypageShipAddressDispp+"")
	WebElement shipandPaypageShipAddressDisp;
	
	@FindBy(xpath = ""+shipandPaypagePayAddressDispp+"")
	WebElement shipandPaypagePayAddressDisp;
	
	@FindBy(xpath = ""+shipandPaypageShipCreateEditAddressOverlayy+"")
	WebElement shipandPaypageShipCreateEditAddressOverlay;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresss+"")
	WebElement shipandPaypageShipShipCreateEditAddress;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddressCancelBtnn+"")
	WebElement shipandPaypageShipShipCreateEditAddressCancelBtn;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssFNamee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssFName;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssLNamee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssLName;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssStAddresss+"")
	WebElement shipandPaypageShipShipCreateEditAddresssStAddress;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssStAddresss2+"")
	WebElement shipandPaypageShipShipCreateEditAddresssStAddress2;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssCtyy+"")
	WebElement shipandPaypageShipShipCreateEditAddresssCty;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssCountryy+"")
	WebElement shipandPaypageShipShipCreateEditAddresssCountry;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssStatee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssState;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPayStatee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPayState;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPayNonUsStateLabell+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPayNonUsStateLabel;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPayUsStateLabell+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPayUsStateLabel;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPayUKStatee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPayUKState;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPOcodee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPOcode;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssNonUsUkPOcodee+"")
	WebElement shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHH1+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPH1;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHH2+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPH2;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHH3+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPH3;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddresssPHH4+"")
	WebElement shipandPaypageShipShipCreateEditAddresssPH4;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddressOkBtnn+"")
	WebElement shipandPaypageShipShipCreateEditAddressOkBtn;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditAddressCloseLinkk+"")
	WebElement shipandPaypageShipShipCreateEditAddressCloseLink;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditEmaill+"")
	WebElement shipandPaypageShipShipCreateEditEmail;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditTermsandCondd+"")
	WebElement shipandPaypageShipShipCreateEditTermsandCond;
	
	@FindBy(xpath = ""+shipandPaypageShipShipCreateEditTermsandCondChkBoxx+"")
	WebElement shipandPaypageShipShipCreateEditTermsandCondChkBox;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardd+"")
	WebElement paymentOptionsCreditCard;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardChkkBoxx+"")
	WebElement paymentOptionsCreditCardChkkBox;
	
	@FindBy(xpath = ""+paymentOptionsPaypall+"")
	WebElement paymentOptionsPaypal;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardSelectedd+"")
	WebElement paymentOptionsCreditCardSelected;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardSecEnablee+"")
	WebElement paymentOptionsCreditCardSecEnable;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardCardSelDropDownn+"")
	WebElement paymentOptionsCreditCardCardSelDropDown;
	
	@FindBy(xpath = ""+paymentOptionsPaypalChkkBoxx+"")
	WebElement paymentOptionsPaypalChkkBox;
	
	@FindBy(xpath = ""+paymentOptionsPaypalSelectedd+"")
	WebElement paymentOptionsPaypalSelected;
	
	@FindBy(xpath = ""+paymentOptionsWhatIspaypall+"")
	WebElement paymentOptionsWhatIspaypal;
		
	@FindBy(xpath = ""+loadingGaugeDomm+"")
	WebElement loadingGaugeDom;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardAmountToPaylabell+"")
	WebElement paymentOptionsCreditCardAmountToPaylabel;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardAmountToPayPricee+"")
	WebElement paymentOptionsCreditCardAmountToPayPrice;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardNumberFieldd+"")
	WebElement paymentOptionsCreditCardNumberField;
	
	@FindBy(xpath = ""+paymentOptionsRewardCardFieldd1+"")
	WebElement paymentOptionsRewardCardField1;
	
	@FindBy(xpath = ""+paymentOptionsRewardCardFieldd2+"")
	WebElement paymentOptionsRewardCardField2;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardNumberFieldAlertt+"")
	WebElement paymentOptionsCreditCardNumberFieldAlert;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardSecurityCodeFieldd+"")
	WebElement paymentOptionsCreditCardSecurityCodeField;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardExpMonthFieldd+"")
	WebElement paymentOptionsCreditCardExpMonthField;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardExpYearFieldd+"")
	WebElement paymentOptionsCreditCardExpYearField;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardSecurityCodeFieldAlertt+"")
	WebElement paymentOptionsCreditCardSecurityCodeFieldAlert;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardExpMonthFieldAlertt+"")
	WebElement paymentOptionsCreditCardExpMonthFieldAlert;
	
	@FindBy(xpath = ""+paymentOptionsCreditCardExpYearFieldAlertt+"")
	WebElement paymentOptionsCreditCardExpYearFieldAlert;
	
	@FindBy(xpath = ""+paymentOptionsApplyGiftCardd+"")
	WebElement paymentOptionsApplyGiftCard;
	
	@FindBy(xpath = ""+paymentOptionsApplyGiftCardLabell+"")
	WebElement paymentOptionsApplyGiftCardLabel;
	
	@FindBy(xpath = ""+paymentOptionsApplyGiftCardNumberFieldd+"")
	WebElement paymentOptionsApplyGiftCardNumberField;
	
	@FindBy(xpath = ""+paymentOptionsApplyGiftCardPinFieldd+"")
	WebElement paymentOptionsApplyGiftCardPinField;
	
	@FindBy(xpath = ""+paymentOptionsApplyGiftCardApplyy+"")
	WebElement paymentOptionsApplyGiftCardApply;
	
	@FindBy(xpath = ""+paymentOptionsAddAnotherGiftCardd+"")
	WebElement paymentOptionsAddAnotherGiftCard;
	
	@FindBy(xpath = ""+paymentOptionsRewardCardFieldLabell+"")
	WebElement paymentOptionsRewardCardFieldLabel;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodee+"")
	WebElement paymentOptionsPromoCode;
	
	@FindBy(xpath = ""+paymentOptionsRewardCardFieldAlertt+"")
	WebElement paymentOptionsRewardCardFieldAlert;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodeApplyy+"")
	WebElement paymentOptionsPromoCodeApply;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodeApplyAlertt+"")
	WebElement paymentOptionsPromoCodeApplyAlert;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodeApplyInvalidAlertt+"")
	WebElement paymentOptionsPromoCodeApplyInvalidAlert;
	
	@FindBy(xpath = ""+paymentOptionsEstimatedSubtotall+"")
	WebElement paymentOptionsEstimatedSubtotal;
	
	@FindBy(xpath = ""+paymentOptionsEstimatedSubtotalPricee+"")
	WebElement paymentOptionsEstimatedSubtotalPrice;
	
	@FindBy(xpath = ""+paymentOptionsShippingAmountlabell+"")
	WebElement paymentOptionsShippingAmountlabel;
	
	@FindBy(xpath = ""+paymentOptionsShippingAmountPricee+"")
	WebElement paymentOptionsShippingAmountPrice;
	
	@FindBy(xpath = ""+paymentOptionsPSTLabell+"")
	WebElement paymentOptionsPSTLabel;
	
	@FindBy(xpath = ""+paymentOptionsPSTPricee+"")
	WebElement paymentOptionsPSTPrice;
	
	@FindBy(xpath = ""+paymentOptionsGSTHSTLabell+"")
	WebElement paymentOptionsGSTHSTLabel;
	
	@FindBy(xpath = ""+paymentOptionsGSTHSTPricee+"")
	WebElement paymentOptionsGSTHSTPrice;
	
	@FindBy(xpath = ""+paymentOptionsOrderTotall+"")
	WebElement paymentOptionsOrderTotal;
	
	@FindBy(xpath = ""+paymentOptionsOrderTotalLabell+"")
	WebElement paymentOptionsOrderTotalLabel;
	
	@FindBy(xpath = ""+paymentOptionsShippingMethoddropdownn+"")
	WebElement paymentOptionsShippingMethoddropdown;
	
	@FindBy(xpath = ""+paymentOptionReviewOrderr+"")
	WebElement paymentOptionReviewOrder;
	
	@FindBy(xpath = ""+ReviewandSubmitPagee+"")
	WebElement ReviewandSubmitPage;
	
	@FindBy(xpath = ""+shippingandPaymentt+"")
	WebElement shippingandPayment;
	
	@FindBy(xpath = ""+shippingPaymentPagee+"")
	WebElement shippingPaymentPage;
	
	@FindBy(xpath = ""+checkoutNavTabss+"")
	WebElement checkoutNavTabs;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingAddresss+"")
	WebElement ReviewandSubmitShippingAddress;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingAddressEditt+"")
	WebElement ReviewandSubmitShippingAddressEdit;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingMethodd+"")
	WebElement ReviewandSubmitShippingMethod;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingMethodEditt+"")
	WebElement ReviewandSubmitShippingMethodEdit;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingAddresss+"")
	WebElement ReviewandSubmitBillingAddress;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingAddressEditt+"")
	WebElement ReviewandSubmitBillingAddressEdit;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingMethodd+"")
	WebElement ReviewandSubmitBillingMethod;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingMethodEditt+"")
	WebElement ReviewandSubmitBillingMethodEdit;
	
	@FindBy(xpath = ""+ReviewandSubmitCheckoutSummaryy+"")
	WebElement ReviewandSubmitCheckoutSummary;
	
	@FindBy(xpath = ""+ReviewandSubmitPlaceorderbuttonn+"")
	WebElement ReviewandSubmitPlaceorderbutton;
	
	@FindBy(xpath = ""+addressVerificationInfoTextt+"")
	WebElement addressVerificationInfoText;
	
	@FindBy(xpath = ""+addressVerificationAddressEditLinkk+"")
	WebElement addressVerificationAddressEditLink;
	
	@FindBy(xpath = ""+addressSuggestionListt+"")
	WebElement addressSuggestionList;
	
	@FindBy(xpath = ""+addressSuggestionDeliveryWarningg+"")
	WebElement addressSuggestionDeliveryWarning;
	
	@FindBy(xpath = ""+addressSuggestionClosee+"")
	WebElement addressSuggestionClose;
	
	@FindBy(xpath = ""+addressSuggestedBtnn+"")
	WebElement addressSuggestedBtn;
	
	@FindBy(xpath = ""+addressVerificationbuildingTextboxx+"")
	WebElement addressVerificationbuildingTextbox;
	
	@FindBy(xpath = ""+addressVerificationPotentialMatchesTextt+"")
	WebElement addressVerificationPotentialMatchesText;
	
	@FindBy(xpath = ""+addressVerificationPotentialMatchesAddresss+"")
	WebElement addressVerificationPotentialMatchesAddress;
	
	@FindBy(xpath = ""+secondGiftCardd+"")
	WebElement secondGiftCard;
	
	@FindBy(xpath = ""+reviewOrderGeneralErrorr+"")
	WebElement reviewOrderGeneralError;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingAddressDetailss+"")
	WebElement ReviewandSubmitShippingAddressDetails;
	
	@FindBy(xpath = ""+ReviewandSubmitShippingMethodSelectedd+"")
	WebElement ReviewandSubmitShippingMethodSelected;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingAddressDetailss+"")
	WebElement ReviewandSubmitBillingAddressDetails;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingMethodDetailss+"")
	WebElement ReviewandSubmitBillingMethodDetails;
	
	@FindBy(xpath = ""+chkoutAddressPagee+"")
	WebElement chkoutAddressPage;
	
	@FindBy(xpath = ""+AddressSubmitt+"")
	WebElement AddressSubmit;
	
	@FindBy(xpath = ""+shipandPaypageShipPayToNamee+"")
	WebElement shipandPaypageShipPayToName;
	
	@FindBy(xpath = ""+ReviewandSubmitBillingAddressNameDetailss+"")
	WebElement ReviewandSubmitBillingAddressNameDetails;
	
	@FindBy(xpath = ""+ReviewandSubmitOrderTotall+"")
	WebElement ReviewandSubmitOrderTotal;
	
	@FindBy(xpath = ""+shoppingCartPagee+"")
	WebElement shoppingBagPage;
	
	@FindBy(xpath = ""+prodQuantyy+"")
	WebElement prodQuanty;
	
	@FindBy(xpath = ""+prodQuantyIncc+"")
	WebElement prodQuantyInc;
	
	@FindBy(xpath = ""+prodQuantyDecc+"")
	WebElement prodQuantyDec;
	
	@FindBy(xpath = ""+checkoutButtonn+"")
	WebElement checkoutButton;
	
	@FindBy(xpath = ""+placeOrderr+"")
	WebElement placeOrder;
	
	@FindBy(xpath = ""+confirmOrderPagee+"")
	WebElement confirmOrderPage;
	
	@FindBy(xpath = ""+confirmThanksSectionn+"")
	WebElement confirmThanksSection;
	
	@FindBy(xpath = ""+reviewthankyouu+"")
	WebElement reviewthankyou;
	
	@FindBy(xpath = ""+confirmShippingInfoSectionn+"")
	WebElement confirmShippingInfoSection;
	
	@FindBy(xpath = ""+confirmPaymentInfoSectionn+"")
	WebElement confirmPaymentInfoSection;
	
	@FindBy(xpath = ""+confirmPaymentDetailSectionn+"")
	WebElement confirmPaymentDetailSection;
	
	@FindBy(xpath = ""+confirmRewardDetailSectionn+"")
	WebElement confirmRewardDetailSection;
	
	@FindBy(xpath = ""+confirmsubTotalSectionn+"")
	WebElement confirmsubTotalSection;
	
	@FindBy(xpath = ""+confirmPromotionSectionn+"")
	WebElement confirmPromotionSection;
	
	@FindBy(xpath = ""+confirmShippingTaxesSectionn+"")
	WebElement confirmShippingTaxesSection;
	
	@FindBy(xpath = ""+confirmOrderTotalSectionn+"")
	WebElement confirmOrderTotalSection;
	
	@FindBy(xpath = ""+confirmThanksSectionOrdernumberr+"")
	WebElement confirmThanksSectionOrdernumber;
	
	@FindBy(xpath = ""+reviewTabHBCTextt+"")
	WebElement reviewTabHBCText;
	
	@FindBy(xpath = ""+reviewshippingamounttxtt+"")
	WebElement reviewshippingamounttxt;
	
	@FindBy(xpath = ""+reviewshippingamountt+"")
	WebElement reviewshippingamount;
	
	@FindBy(xpath = ""+reviewpsttxtt+"")
	WebElement reviewpsttxt;
	
	@FindBy(xpath = ""+reviewpstamountt+"")
	WebElement reviewpstamount;
	
	@FindBy(xpath = ""+reviewgsttxtt+"")
	WebElement reviewgsttxt;
	
	@FindBy(xpath = ""+reviewgstamountt+"")
	WebElement reviewgstamount;
	
	@FindBy(xpath = ""+reviewBillinSummarySecc+"")
	WebElement reviewBillinSummarySec;
	
	@FindBy(xpath = ""+reviewOrderCreateAccountt+"")
	WebElement reviewOrderCreateAccount;
	
	@FindBy(xpath = ""+reviewOrderCreateAccountPagee+"")
	WebElement reviewOrderCreateAccountPage;
	
	//Lists:
		
	@FindBy(how = How.XPATH,using = ""+MenuListss+"")
	List<WebElement> MenuLists;
	
	@FindBy(how = How.XPATH,using = ""+socialIconsListss+"")
	List<WebElement> socialIconsLists;	
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	@FindBy(how = How.XPATH, using = ""+paymentOptionsCreditCardImagess+"")
	List<WebElement> paymentOptionsCreditCardImages;
	
	@FindBy(how = How.XPATH, using = ""+addressSuggestionAddressListt+"")
	List<WebElement> addressSuggestionAddressList;
	
	@FindBy(how = How.XPATH, using = ""+userEnteredAddresss+"")
	List<WebElement> userEnteredAddress;
	
	@FindBy(how = How.XPATH, using = ""+addressVerificationSuggestedAddresss+"")
	List<WebElement> addressVerificationSuggestedAddress;
	
	@FindBy(how = How.XPATH, using = ""+addressVerificationPotentialMatchesAddressListt+"")
	List<WebElement> addressVerificationPotentialMatchesAddressList;
	
	@FindBy(how = How.XPATH, using = ""+revieworderDetailsTextt+"")
	List<WebElement> revieworderDetailsText;
	
	
	
	// Billing Clear All
	public void bclearAll()
	{
		try
		{
			firstName.clear();
			lastName.clear();
			staddress.clear();
			staddress2.clear();
			city.clear();
			if(HBCBasicfeature.isElementPresent(bcountrySelection))
			{
				Select sel = new Select(bcountrySelection);
				String txt = sel.getFirstSelectedOption().getText();
				if(txt.contains("Canada"))
				{
					bpostalCode.clear();
				}
				else if(txt.contains("United"))
				{
					bpostalCode.clear();
				}
				else
				{
					bpostalCode.clear();
				}
			}
			
			bContactNo.clear();
			email.clear();
		}
		catch(Exception e)
		{
			System.out.println("Clear All Exception");
		}
	}
	
	/* Search EAN Numbers */
	public boolean searchEanNumbers(String ean) throws Exception
	{
		boolean pdpPageloaded = false;
		try
		{
			do
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(header));
					//wait.until(ExpectedConditions.visibilityOf(searchIcon));
					HBCBasicfeature.scrollup(header, driver);
					//BNBasicfeature.scrollup(searchIcon, driver);
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(searchIcon))
					{
						Actions act = new Actions(driver);
						act.moveToElement(searchIcon).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(searchBox));
						act.moveToElement(searchBox).click().build().perform();
						act.sendKeys(ean).sendKeys(Keys.ENTER).build().perform();
						//Thread.sleep(1000);
						//wait.until(ExpectedConditions.visibilityOf(searchresults));
						act.sendKeys(Keys.ENTER).build().perform();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(1000);
						try
						{
							if(HBCBasicfeature.isElementPresent(noPrdtFound))
							{
								pdpPageloaded = false;
								break;
							}
							else
							{
								boolean noSrch = noSearchRes.getAttribute("style").contains("block");
								if(noSrch==true)
								{
									pdpPageloaded = false;
									break;
								}
							}
						}
						catch(Exception e)
						{
							wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(pdpPageWrapper)));
							wait.until(ExpectedConditions.visibilityOf(PDPProdName));
							Thread.sleep(1000);
							if(HBCBasicfeature.isElementPresent(pdppageATB))
							{
								pdpPageloaded = true;
								break;
							}
							else
							{
								pdpPageloaded = false;
								continue;
							}
						}
					}
					else
					{
						System.out.println("The Search Icon is not found.Please Check.");
					}
				}
				catch(Exception e)
				{
					System.out.println("In Catch Block 1");
					System.out.println(e.getMessage());
					//driver.navigate().refresh();
				}
			}while(pdpPageloaded==true);
		}
		catch (Exception e) 
		{
			System.out.println("In Outer Catch Block 2");
			System.out.println(e.getMessage());
		}
		return pdpPageloaded;
	}
	
	// Check for Bag Count
	public boolean bagCount(boolean val) throws Exception
	{
		boolean prdtadded = false;
		HBCBasicfeature.scrollup(header, driver);
		int prdtval = Integer.parseInt(bagIconCount.getText());
		int count = 1;
		boolean prdtAdded = clickaddtoBag(prdtval);
		do
		{
			int currVal= Integer.parseInt(bagIconCount.getText());
			if(prdtAdded==true)
			{
				if(currVal>prdtval)
				{
					prdtadded=true;
					//bagIcon.click();
					break;
				}
				else
				{
					count++;
					clickaddtoBag(prdtval);
					continue;
				}
			}
			else
			{
				count++;
				clickaddtoBag(prdtval);
				continue;
			}
		}while(count<3||prdtadded==true);
		return prdtadded;
	}
	
	// Add to Bag
	public boolean clickaddtoBag(int prevVal) throws InterruptedException
	{
		boolean prdadded = false;
		try
		{
			jsclick(pdppageATB);
			wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
			prdadded = false;
			int count = 1;
			Thread.sleep(1000);
			do
			{
				int currVal = Integer.parseInt(bagIconCount.getText());
				if(HBCBasicfeature.isElementPresent(pdpSuccessId))
				{
					if(currVal>prevVal)
					{
						prdadded = true;
						break;
					}
					else
					{
						jsclick(pdppageATB);
						wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
						Thread.sleep(1000);
						count++;
						continue;
					}
				}
				else
				{
					count++;
					jsclick(pdppageATB);
					wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
					Thread.sleep(1000);
					continue;
				}
			}while(count<= 5 || prdadded == true);
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		return prdadded;
	}
	
	// Load Address Verification Page
	public boolean ldAddressVerification()
	{
		boolean addOverlay = false; 
		int count = 0;
		WebDriverWait w1 = new WebDriverWait(driver, 5);
		do
		{
			try
			{
				count++;
				shipandPaypageShipShipCreateEditAddressOkBtn.click();
				w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				addOverlay = true;
			}
			catch(Exception e)
			{
				addOverlay = false;
			}
		}while((addOverlay==false)&&(count<5));
		return addOverlay;
	}
	
	// Load Address Verification Page
	public boolean ldNoAddressVerification()
	{
		boolean addOverlay = false; 
		int count = 0;
		WebDriverWait w1 = new WebDriverWait(driver, 5);
		do
		{
			try
			{
				count++;
				shipandPaypageShipShipCreateEditAddressOkBtn.click();
				w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				addOverlay = false;
				if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
				{
					jsclick(addressVerificationEdit);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
					Thread.sleep(1000);
				}
			}
			catch(Exception e)
			{
				addOverlay = true;
			}
		}while((addOverlay==false)&&(count<5));
		return addOverlay;
	}
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
	
	public boolean AddPaymentReviewOrderClick()
	{
		if(shipPage==true)
		{
			try
			{
				reviewSubmit = false;
				String actual = HBCBasicfeature.getExcelVal("HBCCard", sheet, 1);
				String cnumber = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 2);
				String cvv = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 3);
				String expMonth = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 4);
				String expyear = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 5);
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardChkkBox))
				{
					jsclick(paymentOptionsCreditCardChkkBox);
					wait.until(ExpectedConditions.visibilityOf(paymentOptionsCreditCardSelected));
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
					{
						Pass( "The Credit Card option was selected.");
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecEnable, "style", "block"));
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
						{
							Pass("Credit type drop down is displayed");
							Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
							sel.selectByValue("MAST");
							Thread.sleep(1000);
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
							String current = sel.getFirstSelectedOption().getText();
							log.add("The selected by default option is: "+actual);
							log.add("The Current selected option  is: "+current);
							if(actual.equalsIgnoreCase(current))
							{
								Pass("Mastercard option selected");
								if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
								{
									HBCBasicfeature.scrolldown(paymentOptionsCreditCardNumberField, driver);
									paymentOptionsCreditCardNumberField.clear();
									paymentOptionsCreditCardNumberField.sendKeys(cnumber);
									if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
									{
										paymentOptionsCreditCardSecurityCodeField.clear();
										paymentOptionsCreditCardSecurityCodeField.sendKeys(cvv);
										if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
										{
											paymentOptionsCreditCardExpMonthField.clear();
											paymentOptionsCreditCardExpMonthField.sendKeys(expMonth);
											if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
											{
												paymentOptionsCreditCardExpYearField.clear();
												paymentOptionsCreditCardExpYearField.sendKeys(expyear);
												if(HBCBasicfeature.isElementPresent(paymentOptionReviewOrder))
												{
													jsclick(paymentOptionReviewOrder);
													Thread.sleep(4000);
													WebDriverWait w1 = new WebDriverWait(driver, 10);
													try
													{
														w1.until(ExpectedConditions.attributeContains(reviewOrderGeneralError, "style", "block"));
														boolean genwarn = reviewOrderGeneralError.getAttribute("style").contains("block");
														if(genwarn==false)
														{
															wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
															wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
															if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
															{
																Pass("Payment added, hence Review and Submit page displayed");
																reviewSubmit = true;
															}
															else
															{
																Fail("Review and Submit page not displayed");
															}
														}
														else
														{
															reviewSubmit = false;
														}
													}
													catch(Exception e)
													{
														//wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
														wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
														if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
														{
															Pass("Payment added, hence Review and Submit page displayed");
															reviewSubmit = true;
														}
														else
														{
															Fail("Review and Submit page not displayed");
														}
													}
												}
												else
												{
													Fail("Review order button not displayed");
												}
											}
											else
											{
												Fail("Credit card expiry  year field not displayed");
											}
										}
										else
										{
											Fail("Credit card expiry  month field not displayed");
										}
									}
									else
									{
										Fail("Credit card security code field not displayed");
									}											
								}
								else
								{
									Fail("Credit card number field not displayed");
								}
							}
							else
							{
								Fail("user not able to choose the option in the 'Card type' dropdown field in the 'Payment' column",log);
							}							
						}
						else
						{
							Fail("Credit card dropdown not displayed");
						}
					}
					else
					{
						Fail("Pay by credit card not selected");
					}						
				}
				else
				{
					Fail(" Pay by credit card checkbox not displayed");
				}					
			}
			catch(Exception e)
			{
				System.out.println( e.getMessage());
				Exception(e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		return reviewSubmit;			
	}
	
	/* HBC - 1172 Verify that user should want to view the screen slide up from the bottom of the screen and give the user access.*/
	public void HBC1172AddCartItems()
	{
		ChildCreation("HBC - 1172 Verify that user should want to view the screen slide up from the bottom of the screen and give the user access.");
		try
		{
			String[] eanNumbers = HBCBasicfeature.getExcelNumericVal("HBCEan", sheet, 1).split("\n");
			boolean prdtFound = false;
			
			for(int i = 0; i<eanNumbers.length;i++)
			{
				prdtFound = searchEanNumbers(eanNumbers[i]);
				if(prdtFound == true)
				{
				   break;
				}
				else
				{
					continue;
				}
			}
			
			if(prdtFound==true)
			{
				Pass( "The PDP page is loaded successfully.");
				prdtAdded = clickaddtoBag(0);
				if(prdtAdded==true)
				{
					Pass("The Product added successfully.");
					wait.until(ExpectedConditions.attributeContains(pdppageATBOverlay, "style", "block"));
					jsclick(pdpcheckOutBtn);
					wait.until(ExpectedConditions.visibilityOf(shoppingCartPage));
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(shoppingCartCheckout));
					jsclick(shoppingCartCheckout);
					Thread.sleep(1000);
					WebDriverWait w1 = new WebDriverWait(driver, 5);
					try
					{
						w1.until(ExpectedConditions.attributeContains(guestSignInPopup, "style", "display"));
					}
					catch(Exception e)
					{
						Pass( "The Guest Checkout pop up is displayed.");
						if(HBCBasicfeature.isElementPresent(guestContinueBtn))
						{
							Pass( "The Guest Sign In Page is displayed.");
							jsclick(guestContinueBtn);
							wait.until(ExpectedConditions.visibilityOf(checkoutNavTitle));
							wait.until(ExpectedConditions.visibilityOf(header));
							if(HBCBasicfeature.isElementPresent(checkoutNavTitle))
							{
								Pass( "The Guest Shipping Address Page is displayed.");
							}
							else
							{
								Fail( "The Guest Shipping Address Page is not displayed.");
							}
							prdtAdded = true;
						}
						else
						{
							Fail( "The Guest Sign In Page is not displayed.");
							prdtAdded = true;
						}
					}
					
				}
				else
				{
					Fail( "The User failed to add the product.");
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" HBC - 1172 Issue ." + e.getMessage());
			Exception(" HBC - 1172 Issue ." + e.getMessage());
		}
	}
	
	/* HBC - 1173 Verify that clicking on "Guest Checkout" button it should navigates to Addresses section (i.e.Both Shipping and Biliing Address).*/
	public void HBC1173GuestAddressPage()
	{
		ChildCreation("HBC - 1173 Verify that clicking on Guest Checkout button it should navigates to Addresses section (i.e.Both Shipping and Biliing Address).");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1173", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(checkoutNavTitle))
				{
					Pass( "The Guest Shipping Address Page is displayed.");
					if(HBCBasicfeature.isElementPresent(checkoutCurrNavTitle))
					{
						String txt = checkoutCurrNavTitle.getText();
						log.add( "The Expected title was : " + cellVal[0].toString());
						log.add( "The Actual title is : " + txt);
						if(txt.equalsIgnoreCase(cellVal[0]))
						{
							Pass( "The Address header title matches.",log);
						}
						else
						{
							Fail( " Mismatch in the Address header title .",log);
						}
					}
					else
					{
						Fail( "The Guest Checkout Address Header Title is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(gccheckoutbillingAddressTitle))
					{
						Pass( "The Address List header is displayed.");
						String txt = gccheckoutbillingAddressTitle.getText().toLowerCase();
						log.add( "The Actual title is : " + txt);
						if(txt.contains(cellVal[1].toLowerCase().toString()))
						{
							log.add( "The Expected title was : " + cellVal[1].toString());
							Pass( " The Address title is displayed and it matches the expected content.",log);
						}
						else
						{
							log.add( "The Expected title was : " + cellVal[1].toString());
							Fail( " The Address title is displayed and it does not matches the expected content.",log);
						}
					}
					else
					{
						Fail( "The Address List header is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(gccheckoutshippingAddressTitle))
					{
						Pass( "The Address List header is displayed.");
						String txt = gccheckoutshippingAddressTitle.getText().toLowerCase();
						log.add( "The Actual title is : " + txt);
						if(txt.contains(cellVal[2].toLowerCase().toString()))
						{
							log.add( "The Expected title was : " + cellVal[2].toString());
							Pass( " The Address title is displayed and it matches the expected content.",log);
						}
						else
						{
							log.add( "The Expected title was : " + cellVal[2].toString());
							Fail( " The Address title is displayed and it does not matches the expected content.",log);
						}
					}
					else
					{
						Fail( "The Address List header is not displayed.");
					}
				}
				else
				{
					Fail( "The Guest Shipping Address Page is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1173 Issue ." + e.getMessage());
				Exception(" HBC - 1173 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1174 Guest Checkout - Verify that Addresses section should be displayed as per the creative in the guest checkout page.*/
	public void HBC1174GuestCheckoutEle()
	{
		ChildCreation("HBC - 1174 Guest Checkout - Verify that Addresses section should be displayed as per the creative in the guest checkout page.");
		//prdtAdded = true;
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(checkoutCurrNavTitle))
				{
					Pass( "The Current Navigation title is displayed.");
				}
				else
				{
					Fail( "The Current Navigation title is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The Last Name field is displayed.");
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress))
				{
					Pass( "The Street Address field is displayed.");
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress2))
				{
					Pass( "The Street Address 2 field is displayed.");
				}
				else
				{
					Fail( "The Street Address 2 field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(city))
				{
					Pass( "The City field is displayed.");
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					addressCountry = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(addressCountry.contains("Canada")||(addressCountry.contains("States")))
				{
					if(HBCBasicfeature.isElementPresent(bState))
					{
						Pass( "The State field is displayed.");
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
				}
				else
				{
					if(HBCBasicfeature.isElementPresent(bukState))
					{
						Pass( "The State field is displayed.");
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
				}
				
				if(addressCountry.contains("Canada")||(addressCountry.contains("Kingdom")))
				{
					if(HBCBasicfeature.isElementPresent(bpostalCode))
					{
						Pass( "The Postal Code field is displayed.");
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
				}
				else
				{
					if(HBCBasicfeature.isElementPresent(buspostalCode))
					{
						Pass( "The Postal Code field is displayed.");
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
				}
				
				
				
				if(HBCBasicfeature.isElementPresent(bContactNo))
				{
					Pass( "The Contact Number field is displayed.");
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bextCode))
				{
					Pass( "The Extension Code field is displayed.");
				}
				else
				{
					Fail( "The Extension Code is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(brewardssId))
				{
					Pass( "The Rewards ID field is displayed.");
				}
				else
				{
					Fail( "The Rewards ID field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(brewardsscardNum))
				{
					Pass( "The Rewards Column Number field is displayed.");
				}
				else
				{
					Fail( "The Rewards Column Number field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(checkoutSameAsBillingAddress))
				{
					Pass( "The Same as Billing Address Checkbox is displayed.");
				}
				else
				{
					Fail( "The Same as Billing Address Checkbox is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(checkoutAddressButtons))
				{
					Pass( "The Checkout Address Button is displayed.");
				}
				else
				{
					Fail( "The Checkout Address Button field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(email))
				{
					Pass( "The Email Address in Shipping Section is displayed.");
				}
				else
				{
					Fail( "The Email Address field in Shipping Section is not displayed.");
				}
				
				/*if(HBCBasicfeature.isElementPresent(signupEmails))
				{
					Pass( "The Sign Up Email button is displayed.");
				}
				else
				{
					Fail( "The Sign Up Email button is not displayed.");
				}*/
				
				if(HBCBasicfeature.isElementPresent(footerContainer))
				{
					Pass( "The Footer Container is displayed.");
				}
				else
				{
					Fail( "The Footer Container section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1174 Issue ." + e.getMessage());
				Exception(" HBC - 1174 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1175 Guest Checkout - Verify that Addresses section should have the following text boxes : "First Name", "Last Name", "Street Address", and so on....*/
	public void HBC1175Addressfields()
	{
		ChildCreation(" HBC - 1175 Guest Checkout - Verify that Addresses section should have the following text boxes : First Name, Last Name, Street Address, and so on....");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The Last Name field is displayed.");
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress))
				{
					Pass( "The Street Address field is displayed.");
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress2))
				{
					Pass( "The Street Address 2 field is displayed.");
				}
				else
				{
					Fail( "The Street Address 2 field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(city))
				{
					Pass( "The City field is displayed.");
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					addressCountry = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(addressCountry.contains("Canada")||(addressCountry.contains("States")))
				{
					if(HBCBasicfeature.isElementPresent(bState))
					{
						Pass( "The State field is displayed.");
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
				}
				else
				{
					if(HBCBasicfeature.isElementPresent(bukState))
					{
						Pass( "The State field is displayed.");
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
				}
				
				if(addressCountry.contains("Canada")||(addressCountry.contains("Kingdom")))
				{
					if(HBCBasicfeature.isElementPresent(bpostalCode))
					{
						Pass( "The Postal Code field is displayed.");
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
				}
				else
				{
					if(HBCBasicfeature.isElementPresent(buspostalCode))
					{
						Pass( "The Postal Code field is displayed.");
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
				}
				
				if(HBCBasicfeature.isElementPresent(bContactNo))
				{
					Pass( "The Contact Number field is displayed.");
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bextCode))
				{
					Pass( "The Extension Code field is displayed.");
				}
				else
				{
					Fail( "The Extension Code is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(brewardssId))
				{
					Pass( "The Rewards ID field is displayed.");
				}
				else
				{
					Fail( "The Rewards ID field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(brewardsscardNum))
				{
					Pass( "The Rewards Column Number field is displayed.");
				}
				else
				{
					Fail( "The Rewards Column Number field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1175 Issue ." + e.getMessage());
				Exception(" HBC - 1175 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1177 Guest Checkout - Billing Address Section > Verify that user should be able to enter first name in the "First Name" field and it should accept maximum of 64 characters.*/
	public void HBC1177FNameEnterMax()
	{
		ChildCreation(" HBC - 1177 Guest Checkout - Billing Address Section > Verify that user should be able to enter first name in the First Name field and it should accept maximum of 64 characters.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1177", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						firstName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						firstName.sendKeys(cellVal[i]);
						if(firstName.getAttribute("value").isEmpty())
						{
							Fail( "The First Name is empty.");
						}
						else if(firstName.getAttribute("value").length()<=64)
						{
							Pass( "The first name was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The first name was enterable but it is not within the specified character limit.",log);
						}
						firstName.clear();
					}
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1177 Issue ." + e.getMessage());
				Exception(" HBC - 1177 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1178 Guest Checkout - Billing Address Section > Verify that user should be able to enter last name in the "Last Name" field and it should accept maximum of 64 characters.*/
	public void HBC1178LNameEnterMax()
	{
		ChildCreation(" HBC - 1178 Guest Checkout - Billing Address Section > Verify that user should be able to enter last name in the Last Name field and it should accept maximum of 64 characters.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1177", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The Last Name field is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						lastName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						lastName.sendKeys(cellVal[i]);
						if(lastName.getAttribute("value").isEmpty())
						{
							Fail( "The Last Name is empty.");
						}
						else if(lastName.getAttribute("value").length()<=64)
						{
							Pass( "The last name was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The last name was enterable but it is not within the specified character limit.",log);
						}
						lastName.clear();
					}
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1178 Issue ." + e.getMessage());
				Exception(" HBC - 1178 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1179 Guest Checkout - Billing Address Section > Verify that only characters should be accepted in "First Name" and "Last Name" field.*/
	public void HBC1179FNLNameNumValidation()
	{
		ChildCreation(" HBC - 1179 Guest Checkout - Billing Address Section > Verify that only characters should be accepted in First Name and Last Name field.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1179", sheet, 1).split("\n");
				String[] alert = HBCBasicfeature.getExcelVal("HBC1179", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
					firstName.click();
					firstName.clear();
					log.add( "The Entered value from the excel file is : " + cellVal[0].toString());
					log.add( "The Value from the excel file was : " + cellVal[0].toString());
					log.add( "The expected alert was : " + alert[0].toString());
					firstName.sendKeys(cellVal[0]);
					lastName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBFirstNameAlert,"style","block"));
					String fNameAlert = checkoutBFirstNameAlert.getText();
					
					if(fNameAlert.equalsIgnoreCase(alert[0].toString()))
					{
						Pass( "The first name alert was as expected.",log);
					}
					else
					{
						Fail( "The first name alert was not as expected.",log);
					}
					firstName.clear();
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The First Name field is displayed.");
					lastName.clear();
					lastName.click();
					lastName.clear();
					log.add( "The Entered value from the excel file is : " + cellVal[1].toString());
					log.add( "The Value from the excel file was : " + cellVal[1].toString());
					log.add( "The expected alert was : " + alert[1].toString());
					lastName.sendKeys(cellVal[1]);
					firstName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBLastNameAlert,"style","block"));
					String lNameAlert = checkoutBLastNameAlert.getText();
					
					if(lNameAlert.equalsIgnoreCase(alert[1].toString()))
					{
						Pass( "The last name alert was as expected.",log);
					}
					else
					{
						Fail( "The last name alert was not as expected.",log);
					}
					lastName.clear();	
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1179 Issue ." + e.getMessage());
				Exception(" HBC - 1179 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1180 Guest Checkout - Billing Address Section > Verify that "First Name" and "Last Name" field should accept numbers and special characters and the corresponding alert message should be shown.*/
	public void HBC1180FNLNameNumSplCharValidation()
	{
		ChildCreation(" HBC - 1180 Guest Checkout - Billing Address Section > Verify that First Name and Last Name field should accept numbers and special characters and the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1180", sheet, 1).split("\n");
				String[] alert = HBCBasicfeature.getExcelVal("HBC1180", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
					firstName.click();
					firstName.clear();
					log.add( "The Entered value from the excel file is : " + cellVal[0].toString());
					log.add( "The Value from the excel file was : " + cellVal[0].toString());
					log.add( "The expected alert was : " + alert[0].toString());
					firstName.sendKeys(cellVal[0]);
					lastName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBFirstNameAlert,"style","block"));
					String fNameAlert = checkoutBFirstNameAlert.getText();
					
					if(fNameAlert.equalsIgnoreCase(alert[0].toString()))
					{
						Pass( "The first name alert was as expected.",log);
					}
					else
					{
						Fail( "The first name alert was not as expected.",log);
					}
					firstName.clear();
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The First Name field is displayed.");
					lastName.clear();
					lastName.click();
					lastName.clear();
					log.add( "The Entered value from the excel file is : " + cellVal[1].toString());
					log.add( "The Value from the excel file was : " + cellVal[1].toString());
					log.add( "The expected alert was : " + alert[1].toString());
					lastName.sendKeys(cellVal[1]);
					firstName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBLastNameAlert,"style","block"));
					String lNameAlert = checkoutBLastNameAlert.getText();
					
					if(lNameAlert.equalsIgnoreCase(alert[1].toString()))
					{
						Pass( "The last name alert was as expected.",log);
					}
					else
					{
						Fail( "The last name alert was not as expected.",log);
					}
					lastName.clear();	
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1180 Issue ." + e.getMessage());
				Exception(" HBC - 1180 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}

	/* HBC - 1181 Verify that First Name field, Last Name field should contain inline text as "First Name", "Last Name".*/
	public void HBC1181FNLLNPlaceHolderText()
	{
		ChildCreation(" HBC - 1181 Verify that First Name field, Last Name field should contain inline text as First Name, Last Name.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1181", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					HBCBasicfeature.scrollup(firstName, driver);
					Pass( "The First Name field is displayed.");
					String txt = firstName.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[0].toString());
					if(cellVal[0].toString().equalsIgnoreCase(txt))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The Last Name field is displayed.");
					String txt = lastName.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[1].toString());
					if(cellVal[1].toString().equalsIgnoreCase(txt))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1181 Issue ." + e.getMessage());
				Exception(" HBC - 1181 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1182 Guest Checkout - Billing Address Section > Verify that "Street Address" field should have two text boxes.*/
	public void HBC1182StAddFields()
	{
		ChildCreation(" HBC - 1182 Guest Checkout - Billing Address Section > Verify that Street Address field should have two text boxes.");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(staddress))
				{
					Pass( "The Street Address field is displayed.");
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress2))
				{
					Pass( "The Street Address 2 field is displayed.");
				}
				else
				{
					Fail( "The Street Address 2 field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1182 Issue ." + e.getMessage());
				Exception(" HBC - 1182 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1183 Verify that all the fields should have the inline text based on the creative.*/
	public void HBC1183AllFieldsPlaceHolderText()
	{
		ChildCreation(" HBC - 1183 Verify that all the fields should have the inline text based on the creative.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1183", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
					String txt = firstName.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[0].toString());
					if(cellVal[0].toString().equalsIgnoreCase(txt))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The Last Name field is displayed.");
					String txt = lastName.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[1].toString());
					if(cellVal[1].toString().equalsIgnoreCase(txt))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress))
				{
					Pass( "The Street Address field is displayed.");
					String txt = staddress.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[2].toString());
					if(txt.contains(cellVal[2].toString()))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(city))
				{
					Pass( "The City field is displayed.");
					String txt = city.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[3].toString());
					if(cellVal[3].toString().equalsIgnoreCase(txt))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bpostalCode))
				{
					Pass( "The Postal Code field is displayed.");
					String txt = bpostalCode.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[4].toString());
					if(txt.contains(cellVal[4].toString()))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The Postal Code is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bContactNo))
				{
					Pass( "The Contact Number field is displayed.");
					String txt = bContactNo.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[5].toString());
					if(cellVal[5].toString().equalsIgnoreCase(txt))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bextCode))
				{
					Pass( "The Extension Code field is displayed.");
					String txt = bextCode.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[6].toString());
					if(cellVal[6].toString().equalsIgnoreCase(txt))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The Extension Code is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(email))
				{
					Pass( "The Email Address in Shipping Section is displayed.");
					String txt = email.getAttribute("placeholder");
					log.add( "The placeholder displayed text is  : " + txt);
					log.add( "The expected placeholder text was : " + cellVal[7].toString());
					if(cellVal[7].toString().equalsIgnoreCase(txt))
					{
						Pass( "The expected placeholder text is displayed.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected placeholder text and actual text.",log);
					}
				}
				else
				{
					Fail( "The Email Address field in Shipping Section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1183 Issue ." + e.getMessage());
				Exception(" HBC - 1183 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1184 Guest Checkout - Billing Address Section > Verify that user should be able to enter street address in the "Street Address" field and it should accept maximum of 70 characters in the 1st text box and 50 characters in the 2nd text box.*/
	public void HBC1184StAddFieldsLenVal()
	{
		ChildCreation(" HBC - 1184 Guest Checkout - Billing Address Section > Verify that user should be able to enter street address in the Street Address field and it should accept maximum of 70 characters in the 1st text box and 50 characters in the 2nd text box.");
		if(prdtAdded==true)
		{
			try
			{
				String[] st1 = HBCBasicfeature.getExcelVal("HBC1184", sheet, 1).split("\n");
				String[] st2 = HBCBasicfeature.getExcelVal("HBC1184", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(staddress))
				{
					Pass( "The Street Address field is displayed.");
					for(int i = 0; i<st1.length;i++)
					{
						staddress.clear();
						log.add( "The Entered value from the excel file is : " + st1[i].toString());
						log.add( "The Value from the excel file was : " + st1[i].toString() + " and is length is : " + st1[i].toString().length());
						staddress.sendKeys(st1[i]);
						if(staddress.getAttribute("value").isEmpty())
						{
							Fail( "The Street Address is empty.");
						}
						else if(staddress.getAttribute("value").length()<=70)
						{
							Pass( "The Street Address was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Street Address was enterable but it is not within the specified character limit.",log);
						}
						staddress.clear();
					}
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress2))
				{
					Pass( "The Street Address 2 field is displayed.");
					for(int i = 0; i<st2.length;i++)
					{
						staddress2.clear();
						log.add( "The Entered value from the excel file is : " + st2[i].toString());
						log.add( "The Value from the excel file was : " + st2[i].toString() + " and is length is : " + st2[i].toString().length());
						staddress2.sendKeys(st2[i]);
						if(staddress2.getAttribute("value").isEmpty())
						{
							Fail( "The Street Address 2 is empty.");
						}
						else if(staddress2.getAttribute("value").length()<=50)
						{
							Pass( "The Street Address 2 was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Street Address 2 was enterable but it is not within the specified character limit.",log);
						}
						staddress2.clear();
					}
				}
				else
				{
					Fail( "The Street Address 2 field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1184 Issue ." + e.getMessage());
				Exception(" HBC - 1184 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1185 Guest Checkout - Billing Address Section > Verify that user should be able to enter characters in the "City" field.*/
	public void HBC1185CityEnterable()
	{
		ChildCreation(" HBC - 1185 Guest Checkout - Billing Address Section > Verify that user should be able to enter characters in the City field.");
		if(prdtAdded==true)
		{
			try
			{
				String cityy = HBCBasicfeature.getExcelVal("HBC1185", sheet, 1);
				if(HBCBasicfeature.isElementPresent(city))
				{
					Pass( "The City field is displayed.");
					city.click();
					city.clear();
					log.add( "The Entered value from the excel file is : " + cityy);
					city.sendKeys(cityy);
					String ctyVal = city.getAttribute("value");
					
					if(ctyVal.isEmpty())
					{
						Fail( "The city field is empty.",log);
					}
					else
					{
						Pass( "The city name was enterable.",log);
					}
					city.clear();
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1185 Issue ." + e.getMessage());
				Exception(" HBC - 1185 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1186 Guest Checkout - Billing Address Section > Verify that "City" field should accept numbers and special characters and the corresponding alert message should be shown.*/
	public void HBC1186CityyValidation()
	{
		ChildCreation(" HBC - 1186 Guest Checkout - Billing Address Section > Verify that City field should accept numbers and special characters and the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1186", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1186", sheet, 2);
				if(HBCBasicfeature.isElementPresent(city))
				{
					Pass( "The First Name field is displayed.");
					city.click();
					city.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					city.sendKeys(cellVal);
					staddress2.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBCityAlert,"style","block"));
					String cityAlert = checkoutBCityAlert.getText();
					
					if(cityAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The City alert was as expected.",log);
					}
					else
					{
						Fail( "The City alert was not as expected.",log);
					}
					city.clear();
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1186 Issue ." + e.getMessage());
				Exception(" HBC - 1186 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1187 Guest Checkout - Billing Address Section > Verify that user should be able to select country from the drop down and by default "Country" option should be selected.*/
	public void HBC1187CountrySelection()
	{
		ChildCreation(" HBC - 1187 Guest Checkout - Billing Address Section > Verify that user should be able to select country from the drop down and by default Country option should be selected.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1187", sheet, 1);
				String cellVal1 = HBCBasicfeature.getExcelVal("HBC1187", sheet, 2);
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					String txt = sel.getFirstSelectedOption().getText();
					log.add( "The Expected country was : " + cellVal);
					log.add( "The Actual Selected country is : " + txt);
					if(txt.isEmpty())
					{
						Fail( "The Country Selection seems to be empty.");
					}
					else if(txt.equalsIgnoreCase(cellVal))
					{
						Pass( " The Default Selected country is as expected.",log);
					}
					else
					{
						Fail( "There is mismatch in the expected and the actual default country selection.",log);
					}
					
					sel.selectByValue("US");
					Thread.sleep(1000);
					txt = sel.getFirstSelectedOption().getText();
					log.add( "The Expected country was : " + cellVal1);
					log.add( "The Actual Selected country is : " + txt);
					if(txt.isEmpty())
					{
						Fail( "The Country Selection seems to be empty.");
					}
					else if(txt.equalsIgnoreCase(cellVal1))
					{
						Pass( " The Expected country was selected .",log);
					}
					else
					{
						Fail( " There is mismatch in the expected and the actual selected country.",log);
					}
						
					sel.selectByValue("CA");
					Thread.sleep(1000);
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1179 Issue ." + e.getMessage());
				Exception(" HBC - 1179 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1188 Guest Checkout - Billing Address Section > Verify that clicking on "United Kingdom" option in the country dropdown, the "Same as billing address" checkbox in the shipping address section should be expanded.*/
	public void HBC1188UKCountrySelection()
	{
		ChildCreation(" HBC - 1188 Guest Checkout - Billing Address Section > Verify that clicking on United Kingdom option in the country dropdown, the Same as billing address checkbox in the shipping address section should be expanded.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1188", sheet, 1);
				String cellVal1 = HBCBasicfeature.getExcelVal("HBC1188", sheet, 2);
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					String txt = sel.getFirstSelectedOption().getText();
					sel.selectByValue(cellVal);
					wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
					Thread.sleep(1000);
					txt = sel.getFirstSelectedOption().getText();
					log.add( "The Actual Selected country is : " + txt);
					if(txt.isEmpty())
					{
						Fail( "The Country Selection seems to be empty.");
					}
					else if(txt.equalsIgnoreCase(cellVal1))
					{
						Pass( " The Expected country was selected .",log);
						boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
						if(frmExpand==true)
						{
							Pass( "The Shipping Address Form is expanded as expected.");
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The Shipping Address Form is not expanded as expected.");	
						}
					}
					else
					{
						Fail( " There is mismatch in the expected and the actual selected country.",log);
					}
						
					sel.selectByValue("CA");
					Thread.sleep(1000);
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1179 Issue ." + e.getMessage());
				Exception(" HBC - 1179 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1189 Guest Checkout - Billing Address Section > Verify that clicking on "United Kingdom" or "United States" option in the country dropdown, the user cannot able to select "Same as billing address" checkbox in the shipping address section.*/
	public void HBC1189UKUSCountrySelectionChkdBoxState()
	{
		ChildCreation(" HBC - 1189 Guest Checkout - Billing Address Section > Verify that clicking on United Kingdom or United States option in the country dropdown, the user cannot able to select Same as billing address checkbox in the shipping address section.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1189", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					for(int i = 0; i<cellVal.length; i++)
					{
						HBCBasicfeature.scrolldown(bcountrySelection, driver);
						String txt = sel.getFirstSelectedOption().getText();
						sel.selectByValue(cellVal[i]);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
						txt = sel.getFirstSelectedOption().getText();
						log.add( "The Actual Selected country is : " + txt);
						if(txt.isEmpty())
						{
							Fail( "The Country Selection seems to be empty.");
						}
						else
						{
							Pass( " The Expected country was selected .",log);
							boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
							if(frmExpand==true)
							{
								Pass( "The Shipping Address Form is expanded as expected.");
								if(HBCBasicfeature.isElementPresent(checkoutSameAsBillingAddress))
								{
									Pass( "The Same as shipping address checkbox is dispalyed.");
									jsclick(checkoutSameAsBillingAddress);
									Thread.sleep(1000);
									boolean chkd = checkoutSameAsBillingAddress.getAttribute("class").contains("unchecked");
									if(chkd==true)
									{
										Pass( "The Checkbox is still in unchecked state.",log);
									}
									else
									{
										Fail( "The Checkbox is not in unchecked state.",log);
									}
								}
								else
								{
									Fail( "The Same as shipping address is not dispalyed.",log);
								}
							}
							else
							{
								Fail( "The Shipping Address Form is not expanded as expected.");	
							}
						}
					}
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1189 Issue ." + e.getMessage());
				Exception(" HBC - 1189 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1194 Guest Checkout - Verify that guest user should be able to add the new shipping address details in the shipping section.*/
	public void HBC1194ShippingAddAddress()
	{
		ChildCreation(" HBC - 1194 Guest Checkout - Verify that guest user should be able to add the new shipping address details in the shipping section.");
		if(prdtAdded==true)
		{
			try
			{
				String sfn = HBCBasicfeature.getExcelVal("HBC1194", sheet, 1);
				String lsn = HBCBasicfeature.getExcelVal("HBC1194", sheet, 2);
				String stAdd = HBCBasicfeature.getExcelVal("HBC1194", sheet, 3);
				String cty = HBCBasicfeature.getExcelVal("HBC1194", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1194", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1194", sheet, 6);
				String cNum = HBCBasicfeature.getExcelNumericVal("HBC1194", sheet, 7);
				
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					if(HBCBasicfeature.isElementPresent(sfirstName))
					{
						Pass( "The Shipping Address First Name field is displayed.");
						sfirstName.clear();
						sfirstName.sendKeys(sfn);
						String val = sfirstName.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + sfn);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(sfn.contains(val))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
					}
					else
					{
						Fail( "The Shipping Address First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(slastName))
					{
						Pass( "The Shipping Address Last Name field is displayed.");
						slastName.clear();
						slastName.sendKeys(lsn);
						String val = slastName.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + lsn);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(val.equals(lsn))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
					}
					else
					{
						Fail( "The Shipping Address Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sstaddress))
					{
						Pass( "The Shipping Address Street Address field is displayed.");
						sstaddress.clear();
						sstaddress.sendKeys(stAdd);
						String val = sstaddress.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + stAdd);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(val.equals(stAdd))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
					}
					else
					{
						Fail( "The Shipping Address Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sstaddress2))
					{
						Pass( "The Shipping Address Street Address 2 field is displayed.");
						sstaddress2.clear();
						sstaddress2.sendKeys(stAdd);
						String val = sstaddress2.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + sfn);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(val.equals(sfn))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
					}
					else
					{
						Fail( "The Shipping Address Street Address 2 field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(scity))
					{
						Pass( "The Shipping Address City field is displayed.");
						scity.clear();
						scity.sendKeys(cty);
						String val = scity.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + cty);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(val.equals(cty))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
					}
					else
					{
						Fail( "The Shipping Address City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(bcountrySelection))
					{
						Pass( "The Shipping Address Country field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sState))
					{
						Pass( "The Shipping Address State field is displayed.");
						Select sel = new Select(sState);
						sel.selectByValue(prv);
						String val = sel.getFirstSelectedOption().getText();
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else 
						{
							Pass( " The province is selected.",log);
						}
					}
					else
					{
						Fail( "The Shipping Address State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(spostalCode))
					{
						Pass( "The Shipping Address Postal Code field is displayed.");
						spostalCode.clear();
						spostalCode.sendKeys(pocode);
						String val = spostalCode.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + pocode);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(val.equals(pocode))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
					}
					else
					{
						Fail( "The Shipping Address Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sContactNo))
					{
						Pass( "The Shipping Address Contact Number field is displayed.");
						sContactNo.clear();
						sContactNo.sendKeys(cNum);
						String val = sContactNo.getAttribute("value").replaceAll("\\W", "").trim();
						log.add ( "The Value entered from the excel sheet was : " + cNum);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(val.equals(cNum))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
					}
					else
					{
						Fail( "The Shipping Address Contact Number is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1194 Issue ." + e.getMessage());
				Exception(" HBC - 1194 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1195 Guest Checkout - Verify that required input fields should be displayed to add a new shipping address.*/
	public void HBC1195ShippingAddressFields()
	{
		ChildCreation(" HBC - 1195 Guest Checkout - Verify that required input fields should be displayed to add a new shipping address.");
		if(prdtAdded==true)
		{
			try
			{
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					if(HBCBasicfeature.isElementPresent(sfirstName))
					{
						Pass( "The Shipping Address First Name field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(slastName))
					{
						Pass( "The Shipping Address Last Name field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sstaddress))
					{
						Pass( "The Shipping Address Street Address field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sstaddress2))
					{
						Pass( "The Shipping Address Street Address 2 field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Street Address 2 field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(scity))
					{
						Pass( "The Shipping Address City field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(scountrySelection))
					{
						Pass( "The Shipping Address Country field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sState))
					{
						Pass( "The Shipping Address State field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(spostalCode))
					{
						Pass( "The Shipping Address Postal Code field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(bContactNo))
					{
						Pass( "The Shipping Address Contact Number field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sextCode))
					{
						Pass( "The Shipping Address Extension Code field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Extension Code is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1195 Issue ." + e.getMessage());
				Exception(" HBC - 1195 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1196 Verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address.*/
	public void HBC1196ShippingAddressFieldsTabFocus()
	{
		ChildCreation(" HBC - 1196 Verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address.");
		if(prdtAdded==true)
		{
			try
			{
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					Actions act = new Actions(driver);
					firstName.click();
					for(int i = 1; i < 9 ; i++)
					{
						act.sendKeys(Keys.TAB).build().perform();
						Pass("The tab focus for " +  i  + " field is made successfully.");
					}
					HBCBasicfeature.scrollup(sfirstName,driver);
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1196 Issue ." + e.getMessage());
				Exception(" HBC - 1196 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1197 Guest Checkout - Billing Address Section > Verify that "Country" field should have a maximum of 3 dropdown options.*/
	public void HBC1197BillingCountryOptions()
	{
		ChildCreation(" HBC - 1197 Guest Checkout - Billing Address Section > Verify that Country field should have a maximum of 3 dropdown options.");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					int cntysize = sel.getOptions().size();
					for( int i = 0; i<cntysize;i++)
					{
						String cnty = sel.getOptions().get(i).getText();
						log.add( "The Displayed country option is  : " + cnty);
					}
					if(cntysize==3)
					{
						Pass( "The Country Default Options is of 3 Sizes.",log);
					}
					else
					{
						Fail( "The Country Default Options is of not the expected 3.The displayed total country sizes are : " + cntysize, log);
					}
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1197 Issue ." + e.getMessage());
				Exception(" HBC - 1197 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1198 Guest Checkout - Billing Address Section > Verify that "State" dropdown field should be shown, when the country is selected in Canada.*/
	public void HBC1198BillingCAStates()
	{
		ChildCreation(" HBC - 1198 Guest Checkout - Billing Address Section > Verify that State dropdown field should be shown, when the country is selected in Canada.");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					sel.selectByValue("CA");
					wait.until(ExpectedConditions.attributeContains(bStateState, "style", "block"));
					Thread.sleep(1000);
					boolean stSta = bStateState.getAttribute("style").contains("block");
					if(stSta==true)
					{
						Pass( "The State drop down is enabled.");
						if(HBCBasicfeature.isElementPresent(bState))
						{
							sel = new Select(bState);
							int stsize = sel.getOptions().size();
							for( int i = 0; i<stsize; i++)
							{
								String st = sel.getOptions().get(i).getText();
								log.add( "The displayed state is : " + st);
								if(st.isEmpty())
								{
									Fail( "The State list is empty.");
								}
								else
								{
									Pass( "The State is displayed for the selected country.",log);
								}
							}
						}
						else
						{
							Fail( "The State drop down is not displayed.");
						}
					}
					else
					{
						Fail( "The State drop down is not enabled.");
					}
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1198 Issue ." + e.getMessage());
				Exception(" HBC - 1198 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1199 Guest Checkout - Billing Address Section > Verify that "State" option should be selected by default in the "State" dropdown field.*/
	public void HBC1199BillingCADefaultStateSel()
	{
		ChildCreation(" HBC - 1199 Guest Checkout - Billing Address Section > Verify that State option should be selected by default in the State dropdown field.");
		if(prdtAdded==true)
		{
			try
			{
				String expVal = HBCBasicfeature.getExcelVal("HBC1199", sheet, 1);
				if(HBCBasicfeature.isElementPresent(bState))
				{
					Select sel = new Select(bState);
					String actVal = sel.getFirstSelectedOption().getText();
					log.add( "The Expected Value was : " + expVal);
					log.add( "The Actual Value is : " + actVal);
					if(expVal.equalsIgnoreCase(actVal))
					{
						Pass( "The Expected Default Value is selected.",log);
					}
					else
					{
						Fail( "The Expected Default Value is not selected.",log);
					}
				}
				else
				{
					Fail( "The State drop down is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1199 Issue ." + e.getMessage());
				Exception(" HBC - 1199 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1200 Guest Checkout - Billing Address Section > Verify that "State" dropdown field should be changed to textbox field, when the country is selected in "United Kingdom".*/
	public void HBC1200BillingUKStateText()
	{
		ChildCreation(" HBC - 1200 Guest Checkout - Billing Address Section > Verify that State dropdown field should be changed to textbox field, when the country is selected in United Kingdom.");
		if(prdtAdded==true)
		{
			try
			{
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Kingdom");
					if(cnty==false)
					{
						sel.selectByValue("GB");
						wait.until(ExpectedConditions.visibilityOf(bukState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(bukState))
				{
					Pass( "The State field is displayed.");
					jsclick(bukState);
				}
				else
				{
					Fail( "The State text field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1200 Issue ." + e.getMessage());
				Exception(" HBC - 1200 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1201 Guest Checkout - Billing Address Section > Verify that user should be able to select state from the "State" drop down, when the country is selected in "United States".*/
	public void HBC1201BillingUSCountrySel()
	{
		ChildCreation(" HBC - 1201 Guest Checkout - Billing Address Section > Verify that user should be able to select state from the State drop down, when the country is selected in United States.");
		if(prdtAdded==true)
		{
			try
			{
				Select sel;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(bState))
				{
					Pass( "The State drop down is displayed.");
				}
				else
				{
					Fail( "The State drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1201 Issue ." + e.getMessage());
				Exception(" HBC - 1201 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1190 Guest Checkout - Billing Address Section > Verify that clicking on "United States" option in the country dropdown, the "State", "Zip Code" texboxes gets hidden and "State", "Zip Code" text boxes is shown.*/
	public void HBC1190USCountryFields()
	{
		ChildCreation("HBC - 1190 Guest Checkout - Billing Address Section > Verify that clicking on United States option in the country dropdown, the State, Zip Code texboxes gets hidden and State, Zip Code text boxes is shown.");
		if(prdtAdded==true)
		{
			try
			{
				Select sel;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(bState))
				{
					Pass( "The State drop down is displayed.");
				}
				else
				{
					Fail( "The State drop down is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(buspostalCode))
				{
					Pass( "The Postal Code field is displayed.");
				}
				else
				{
					Fail( "The Postal Code field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bpostalCode))
				{
					Fail( "The Postal Code field is displayed.");
				}
				else
				{
					Pass( "The Postal Code field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1190 Issue ." + e.getMessage());
				Exception(" HBC - 1190 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1202 Guest Checkout - Billing Address Section > Verify that user should be able to enter zip code details in the "Zip Code" field, when the country is selected in "United States".*/
	public void HBC1202BillingUSZipCode()
	{
		ChildCreation(" HBC - 1202 Guest Checkout - Billing Address Section > Verify that user should be able to enter zip code details in the Zip Code field, when the country is selected in United States.");
		if(prdtAdded==true)
		{
			try
			{
				String zpval = HBCBasicfeature.getExcelNumericVal("HBC1202", sheet, 1);
				Select sel;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						wait.until(ExpectedConditions.visibilityOf(bState));
						Thread.sleep(1000);
					}
				}
				
				if(HBCBasicfeature.isElementPresent(buspostalCode))
				{
					Pass( "The Postal Code is displayed.");
					buspostalCode.click();
					buspostalCode.clear();
					log.add( "The Entered value from the excel file is : " + zpval);
					buspostalCode.sendKeys(zpval);
					String zpVal = buspostalCode.getAttribute("value");
					
					if(zpVal.isEmpty())
					{
						Fail( "The Zip Code  field is empty.",log);
					}
					else if(zpval.equalsIgnoreCase(zpVal))
					{
						Pass( "The Zip Code was enterable and it matches.",log);
					}
					else
					{
						Fail( "The Zip Code was enterable and it does not matches.",log);
					}
					buspostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1202 Issue ." + e.getMessage());
				Exception(" HBC - 1202 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1203 Guest Checkout - Billing Address Section > Verify that user should be able to enter maximum of 5 numbers in the "Zip Code" field.*/
	public void HBC1203BillingUSZipLength()
	{
		ChildCreation(" HBC - 1203 Guest Checkout - Billing Address Section > Verify that user should be able to enter maximum of 5 numbers in the Zip Code field.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1203", sheet, 1).split("\n");
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(buspostalCode))
				{
					Pass( "The Postal Code Area is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						buspostalCode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						buspostalCode.sendKeys(cellVal[i]);
						if(buspostalCode.getAttribute("value").isEmpty())
						{
							Fail( "The Zip Code field is empty.");
						}
						else if(buspostalCode.getAttribute("value").length()<=5)
						{
							Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
						}
						buspostalCode.clear();
					}
				}
				else
				{
					Fail( "The Postal Code Area is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1203 Issue ." + e.getMessage());
				Exception(" HBC - 1203 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1204 Guest Checkout - Billing Address Section > Verify that "Zip Code" field should accept numbers and special characters and the corresponding alert message should be shown.*/
	public void HBC1204BillinUSPOValidation()
	{
		ChildCreation(" HBC - 1204 Guest Checkout - Billing Address Section > Verify that Zip Code field should accept numbers and special characters and the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String cellVal = HBCBasicfeature.getExcelVal("HBC1204", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1204", sheet, 2);
				if(HBCBasicfeature.isElementPresent(buspostalCode))
				{
					Pass( "The First Name field is displayed.");
					buspostalCode.click();
					buspostalCode.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					buspostalCode.sendKeys(cellVal);
					bContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					String poAlert = checkoutBPOAlert.getText();
					log.add( "The actual alert was : " + poAlert);
					if(poAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					buspostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1204 Issue ." + e.getMessage());
				Exception(" HBC - 1204 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1205 Guest Checkout - Billing Address Section > Verify that when the user entered less than 5 digits in the "Zip Code" field, then the corresponding alert message should be shown.*/
	public void HBC1205BillUSPOLessLenValidation()
	{
		ChildCreation(" HBC - 1205 Guest Checkout - Billing Address Section > Verify that when the user entered less than 5 digits in the Zip Code field, then the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1205", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1205", sheet, 2);
				if(HBCBasicfeature.isElementPresent(buspostalCode))
				{
					Pass( "The First Name field is displayed.");
					buspostalCode.click();
					buspostalCode.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					buspostalCode.sendKeys(cellVal);
					bContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					String poAlert = checkoutBPOAlert.getText();
					log.add( "The actual alert was : " + poAlert);
					if(poAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					buspostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1205 Issue ." + e.getMessage());
				Exception(" HBC - 1205 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1206 Guest Checkout - Billing Address Section > Verify that "Zip Code" field should accept alphanumeric characters.*/
	public void HBC1206BillinUSAlphaNumericValidation()
	{
		ChildCreation(" HBC - 1206 Guest Checkout - Billing Address Section > Verify that Zip Code field should accept alphanumeric characters.");
		if(prdtAdded==true)
		{
			try
			{
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1206", sheet, 1);
				if(HBCBasicfeature.isElementPresent(buspostalCode))
				{
					Pass( "The First Name field is displayed.");
					buspostalCode.click();
					buspostalCode.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					buspostalCode.sendKeys(cellVal);
					String val = buspostalCode.getAttribute("value");
					log.add ( "The Value entered from the excel sheet was : " + cellVal);
					log.add ( "The Value in the field is : " + val);
					if(val.isEmpty())
					{
						Fail ( "The Field is empty.");
					}
					else if(val.equalsIgnoreCase(cellVal))
					{
						Pass( " There is no mismatch in the entered and the current value.",log);
					}
					else
					{
						Fail( " There is mismatch in the entered and the current value.",log);
					}
					buspostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1206 Issue ." + e.getMessage());
				Exception(" HBC - 1206 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1207 Guest Checkout - Billing Address Section > Verify that "Zip Code" field should accept maximum of 7 characters.*/
	public void HBC1207BillingCAPOLength()
	{
		ChildCreation(" HBC - 1207 Guest Checkout - Billing Address Section > Verify that Zip Code field should accept maximum of 7 characters.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1207", sheet, 1).split("\n");
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(bpostalCode))
				{
					Pass( "The Postal Code Area is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						bpostalCode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						bpostalCode.sendKeys(cellVal[i]);
						if(bpostalCode.getAttribute("value").isEmpty())
						{
							Fail( "The Zip Code field is empty.");
						}
						else if(bpostalCode.getAttribute("value").length()<=7)
						{
							Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
						}
						bpostalCode.clear();
					}
				}
				else
				{
					Fail( "The Postal Code Area is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1207 Issue ." + e.getMessage());
				Exception(" HBC - 1207 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1208 Guest Checkout - Billing Address Section > Verify that corresponding alert message should be shown, when the user entered less than 6 digits in the "Zip Code" field.*/
	public void HBC1208BillingCAPOLengthValidationAlert()
	{
		ChildCreation(" HBC - 1208 Guest Checkout - Billing Address Section > Verify that corresponding alert message should be shown, when the user entered less than 6 digits in the Zip Code field.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1208", sheet, 1);
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1208", sheet, 2);
				if(HBCBasicfeature.isElementPresent(bpostalCode))
				{
					Pass( "The First Name field is displayed.");
					bpostalCode.click();
					bpostalCode.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					bpostalCode.sendKeys(cellVal);
					bContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					Thread.sleep(1000);
					String poAlert = checkoutBPOAlert.getText();
					log.add( "The actual alert was : " + poAlert);
					if(poAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					bpostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1208 Issue ." + e.getMessage());
				Exception(" HBC - 1208 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1209 Guest Checkout - Billing Address Section > Verify that corresponding alert message should be shown, when the user entered alphabets or numbers in the "Postal Code" field.*/
	public void HBC1209BillingCAPOAlphaNumericValidation()
	{
		ChildCreation(" HBC - 1209 Guest Checkout - Billing Address Section > Verify that corresponding alert message should be shown, when the user entered alphabets or numbers in the Postal Code field.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1209", sheet, 1);
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1209", sheet, 2);
				if(HBCBasicfeature.isElementPresent(bpostalCode))
				{
					Pass( "The First Name field is displayed.");
					bpostalCode.click();
					bpostalCode.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					bpostalCode.sendKeys(cellVal);
					bContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					String poAlert = checkoutBPOAlert.getText();
					Thread.sleep(1000);
					log.add( "The actual alert was : " + poAlert);
					if(poAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					bpostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1209 Issue ." + e.getMessage());
				Exception(" HBC - 1209 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1210 Guest Checkout - Billing Address Section > Verify that user should be able to enter phone number in the "Phone Number" field and it should accept maximum of 10 numbers.*/
	public void HBC1210BillingCAPHNumLenValidation()
	{
		ChildCreation(" HBC - 1210 Guest Checkout - Billing Address Section > Verify that user should be able to enter phone number in the Phone Number field and it should accept maximum of 10 numbers.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1210", sheet, 1).split("\n");
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(bContactNo))
				{
					Pass( "The Postal Code Area is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						bContactNo.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						bContactNo.sendKeys(cellVal[i]);
						if(bContactNo.getAttribute("value").isEmpty())
						{
							Fail( "The Contact Number field is empty.");
						}
						else if(bContactNo.getAttribute("value").length()<=10)
						{
							Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
						}
						bContactNo.clear();
					}
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1210 Issue ." + e.getMessage());
				Exception(" HBC - 1210 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1211 Guest Checkout - Billing Address Section > Verify that corresponding alert message should be shown, when the user enter alphabets or special characters in the "Phone Number" field.*/
	public void HBC1211BillingCAPHAphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1211 Guest Checkout - Billing Address Section > Verify that corresponding alert message should be shown, when the user enter alphabets or special characters in the Phone Number field.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1211", sheet, 1);
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1211", sheet, 2);
				if(HBCBasicfeature.isElementPresent(bContactNo))
				{
					Pass( "The Contact Number field is displayed.");
					bContactNo.click();
					bContactNo.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					bContactNo.sendKeys(cellVal);
					bpostalCode.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(checkoutBcaPHAlert));
					//wait.until(ExpectedConditions.attributeContains(checkoutBcaPHAlert,"style","block"));
					String ctAlert = checkoutBcaPHAlert.getText();
					
					if(ctAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Phone Number alert was as expected.",log);
					}
					else
					{
						Fail( "The Phone Number alert was not as expected.",log);
					}
					bContactNo.clear();
				}
				else
				{
					Fail( "The Contact Number field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1211 Issue ." + e.getMessage());
				Exception(" HBC - 1211 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1212 Guest Checkout - Billing Address Section > Verify that "ext." field should accept maximum of 5 digits.*/
	public void HBC1212BillingCAExtNumLenValidation()
	{
		ChildCreation(" HBC - 1212 Guest Checkout - Billing Address Section > Verify that ext. field should accept maximum of 5 digits.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1212", sheet, 1).split("\n");
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(bextCode))
				{
					Pass( "The Extension Code Area is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						bextCode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						bextCode.sendKeys(cellVal[i]);
						if(bextCode.getAttribute("value").isEmpty())
						{
							Fail( "The Extension Code field is empty.");
						}
						else if(bextCode.getAttribute("value").length()<=5)
						{
							Pass( "The Extension Code was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Extension Code was enterable but it is not within the specified character limit.",log);
						}
						bextCode.clear();
					}
				}
				else
				{
					Fail( "The Extension Code is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1212 Issue ." + e.getMessage());
				Exception(" HBC - 1212 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1213 Guest Checkout - Billing Address Section > Verify that user should be able to enter card number in the "HBC rewards card number:" field and it should accept maximum of 9 digits.*/
	public void HBC1213BillingCARewardFldLenValidation()
	{
		ChildCreation(" HBC - 1213 Guest Checkout - Billing Address Section > Verify that user should be able to enter card number in the HBC rewards card number: field and it should accept maximum of 9 digits.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1213", sheet, 1).split("\n");
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(brewardsscardNum))
				{
					Pass( "The Reward Card Number Area is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						brewardsscardNum.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						brewardsscardNum.sendKeys(cellVal[i]);
						if(brewardsscardNum.getAttribute("value").isEmpty())
						{
							Fail( "The Reward Card Number field is empty.");
						}
						else if(brewardsscardNum.getAttribute("value").length()<=9)
						{
							Pass( "The Reward Card Number was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Reward Card Number was enterable but it is not within the specified character limit.",log);
						}
						brewardsscardNum.clear();
					}
				}
				else
				{
					Fail( "The Reward Card Number is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1213 Issue ." + e.getMessage());
				Exception(" HBC - 1213 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1214 Guest Checkout - Billing Address Section > Verify that when the user entered less than 9 digits in the "HBC rewards card number" field, then the corresponding alert message should be shown.*/
	public void HBC1214BillingCARewardLenValidationAlert()
	{
		ChildCreation(" HBC - 1214 Guest Checkout - Billing Address Section > Verify that when the user entered less than 9 digits in the HBC rewards card number field, then the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1214", sheet, 1);
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1214", sheet, 2);
				if(HBCBasicfeature.isElementPresent(brewardsscardNum))
				{
					Pass( "The Reward Card Number field is displayed.");
					brewardsscardNum.click();
					brewardsscardNum.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					brewardsscardNum.sendKeys(cellVal);
					email.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBcaRWDAlert,"style","block"));
					String rwdAlert = checkoutBcaRWDAlert.getText();
					
					if(rwdAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Reward Card Number alert was as expected.",log);
					}
					else
					{
						Fail( "The Reward Card Number alert was not as expected.",log);
					}
					brewardsscardNum.clear();
				}
				else
				{
					Fail( "The Reward Card Number field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1214 Issue ." + e.getMessage());
				Exception(" HBC - 1214 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1215 Guest Checkout - Billing Address Section > Verify that when the user entered alphabets or special characters in the "HBC rewards card number" field, then the corresponding alert message should be shown.*/
	public void HBC1215BillingCARewardAlphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1215 Guest Checkout - Billing Address Section > Verify that when the user entered alphabets or special characters in the HBC rewards card number field, then the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1215", sheet, 1);
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1215", sheet, 2);
				if(HBCBasicfeature.isElementPresent(brewardsscardNum))
				{
					Pass( "The Reward Card Number field is displayed.");
					brewardsscardNum.click();
					brewardsscardNum.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					brewardsscardNum.sendKeys(cellVal);
					email.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBcaRWDAlert,"style","block"));
					String rwdAlert = checkoutBcaRWDAlert.getText();
					
					if(rwdAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Reward Card Number alert was as expected.",log);
					}
					else
					{
						Fail( "The Reward Card Number alert was not as expected.",log);
					}
					brewardsscardNum.clear();
				}
				else
				{
					Fail( "The Reward Card Number field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1251 Issue ." + e.getMessage());
				Exception(" HBC - 1215 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1216 Guest Checkout - Billing Address Section > Verify that user should be able to enter email in the "Email:" field and it should accept both alphanumerics and characters.*/
	public void HBC1216BillingValidEmail()
	{
		ChildCreation(" HBC - 1216 Guest Checkout - Billing Address Section > Verify that user should be able to enter email in the Email: field and it should accept both alphanumerics and characters.");
		if(prdtAdded==true)
		{
			try
			{
				String zpval = HBCBasicfeature.getExcelNumericVal("HBC1216", sheet, 1);
				
				if(HBCBasicfeature.isElementPresent(email))
				{
					Pass( "The Email Field is displayed.");
					email.click();
					email.clear();
					log.add( "The Entered value from the excel file is : " + zpval);
					email.sendKeys(zpval);
					String emVal = email.getAttribute("value");
					
					if(emVal.isEmpty())
					{
						Fail( "The Email Field field is empty.",log);
					}
					else if(zpval.equalsIgnoreCase(emVal))
					{
						Pass( "The Email Field was enterable and it matches.",log);
					}
					else
					{
						Fail( "The Email Field  was enterable and it does not matches.",log);
					}
					email.clear();
				}
				else
				{
					Fail( "The Email Field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1216 Issue ." + e.getMessage());
				Exception(" HBC - 1216 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1217 Guest Checkout - Billing Address Section > Verify that email alert should be shown, when the user entered a invalid format in the "Email" field.*/
	public void HBC1217BillingCAEmailValidationAlert()
	{
		ChildCreation(" HBC - 1217 Guest Checkout - Billing Address Section > Verify that email alert should be shown, when the user entered a invalid format in the Email field.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1217", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1217", sheet, 2);
				if(HBCBasicfeature.isElementPresent(email))
				{
					Pass( "The Reward Card Number field is displayed.");
					email.click();
					email.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					email.sendKeys(cellVal);
					bpostalCode.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBcaEmailAlert,"style","block"));
					String emdAlert = checkoutBcaEmailAlert.getText();
					
					if(emdAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Invalid Email alert was as expected.",log);
					}
					else
					{
						Fail( "The Invalid Email alert was not as expected.",log);
					}
					email.clear();
				}
				else
				{
					Fail( "The Email field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1217 Issue ." + e.getMessage());
				Exception(" HBC - 1217 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1218 Guest Checkout - Billing Address Section > Verify that without entering any values in the input field clicking on each text boxes, should display a alert message.*/
	public void HBC1218AddressfieldsEmptyValidation()
	{
		ChildCreation(" HBC - 1218 Guest Checkout - Billing Address Section > Verify that without entering any values in the input field clicking on each text boxes, should display a alert message.");
		if(prdtAdded==true)
		{
			try
			{
				String[] alert = HBCBasicfeature.getExcelVal("HBC1218", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
					firstName.click();
					firstName.clear();
					lastName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBFirstNameAlert,"style","block"));
					String fNameAlert = checkoutBFirstNameAlert.getText();
					
					if(fNameAlert.equalsIgnoreCase(alert[0].toString()))
					{
						Pass( "The first name alert was as expected.",log);
					}
					else
					{
						Fail( "The first name alert was not as expected.",log);
					}
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The Last Name field is displayed.");
					lastName.clear();
					lastName.click();
					lastName.clear();
					log.add( "The expected alert was : " + alert[1].toString());
					firstName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBLastNameAlert,"style","block"));
					String lNameAlert = checkoutBLastNameAlert.getText();
					
					if(lNameAlert.equalsIgnoreCase(alert[1].toString()))
					{
						Pass( "The last name alert was as expected.",log);
					}
					else
					{
						Fail( "The last name alert was not as expected.",log);
					}
					lastName.clear();	
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress))
				{
					Pass( "The Street Address field is displayed.");
					staddress.click();
					staddress.clear();
					lastName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBStaddressAlert,"style","block"));
					String stAddAlert = checkoutBStaddressAlert.getText();
					
					if(stAddAlert.equalsIgnoreCase(alert[2].toString()))
					{
						Pass( "The Street Address alert was as expected.",log);
					}
					else
					{
						Fail( "The Street Address alert was not as expected.",log);
					}
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(city))
				{
					Pass( "The City field is displayed.");
					HBCBasicfeature.scrolldown(staddress, driver);
					city.click();
					city.clear();
					staddress2.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBCityAlert,"style","block"));
					String cityAlert = checkoutBCityAlert.getText();
					
					if(cityAlert.equalsIgnoreCase(alert[3].toString()))
					{
						Pass( "The City alert was as expected.",log);
					}
					else
					{
						Fail( "The City alert was not as expected.",log);
					}
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Kingdom");
					if(cnty==false)
					{
						sel.selectByValue("GB");
						wait.until(ExpectedConditions.visibilityOf(bukState));
					}
					if(HBCBasicfeature.isElementPresent(bukState))
					{
						Pass( "The State field is displayed.");
						bukState.click();
						city.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBUKStateAlert,"style","block"));
						String stAlert = checkoutBUKStateAlert.getText();
						
						if(stAlert.equalsIgnoreCase(alert[4].toString()))
						{
							Pass( "The State or Province alert was as expected.",log);
						}
						else
						{
							Fail( "The State or Province alert was not as expected.",log);
						}
						
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
					
					Thread.sleep(1000);
					val = sel.getFirstSelectedOption().getText();
					cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bpostalCode))
				{
					Pass( "The Postal Code field is displayed.");
					bpostalCode.click();
					bpostalCode.clear();
					bContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					String poAlert = checkoutBPOAlert.getText();
					log.add( "The actual alert was : " + poAlert);
					if(poAlert.equalsIgnoreCase(alert[5].toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					bpostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bContactNo))
				{
					Pass( "The Contact Number field is displayed.");
					bContactNo.click();
					bContactNo.clear();
					bpostalCode.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(checkoutBcaPHAlert));
					String ctAlert = checkoutBcaPHAlert.getText();
					if(ctAlert.equalsIgnoreCase(alert[6].toString()))
					{
						Pass( "The Phone Number alert was as expected.",log);
					}
					else
					{
						Fail( "The Phone Number alert was not as expected.",log);
					}
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(email))
				{
					Pass( "The Reward Card Number field is displayed.");
					email.click();
					email.clear();
					bpostalCode.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBcaEmailAlert,"style","block"));
					String emdAlert = checkoutBcaEmailAlert.getText();
					
					if(emdAlert.equalsIgnoreCase(alert[7].toString()))
					{
						Pass( "The Invalid Email alert was as expected.",log);
					}
					else
					{
						Fail( "The Invalid Email alert was not as expected.",log);
					}
					email.clear();
				}
				else
				{
					Fail( "The Email field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1218 Issue ." + e.getMessage());
				Exception(" HBC - 1218 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1219 Guest Checkout - Billing Address Section > Verify that on selecting the "Terms and Conditions" text link, the user can able to select the "I agree to receive electronic messages from Hudson's Bay..." check box.*/
	public void HBC1219BillingTermandCondition()
	{
		ChildCreation(" HBC - 1219 Guest Checkout - Billing Address Section > Verify that on selecting the Terms and Conditions text link, the user can able to select the I agree to receive electronic messages from Hudson's Bay... check box.");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(termsandCond))
				{
					HBCBasicfeature.scrolldown(termsandCond, driver);
					Pass( "The Terms and Condition link is displayed.");
					jsclick(termsandCond);
					wait.until(ExpectedConditions.attributeContains(termsandConddChkBox, "style", "block"));
					boolean termsOpen = termsandConddChkBox.getAttribute("style").contains("block");
					if(termsOpen==true)
					{
						Pass( "The Terms and Condition is displayed.");
					}
					else
					{
						Fail( "The Terms and Condition is not displayed.");
					}
					
					boolean chkd = termsandConddChkBox.getAttribute("class").contains("checked");
					if(chkd==false)
					{
						Pass( "The tems and condition check box is not in the checked mode.");
						jsclick(termsandConddChkBox);
						wait.until(ExpectedConditions.attributeContains(termsandConddChkBox, "class", "checked"));
						chkd = termsandConddChkBox.getAttribute("class").contains("checked");
						if(chkd==true)
						{
							Pass( "The User was able to select the cheeck box successfully.");
							jsclick(termsandConddChkBox);
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(termsandConddChkBox, "class", "checkbox"));
						}
						else
						{
							Fail( "The Checkbox is not selected.");
						}
					}
					else
					{
						chkd = termsandConddChkBox.getAttribute("class").contains("checked");
						if(chkd==true)
						{
							Pass( "The User was able to select the cheeck box successfully.");
							jsclick(termsandConddChkBox);
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(termsandConddChkBox, "class", "checkbox"));
						}
						else
						{
							Fail( "The Checkbox is not selected.");
						}
					}
					
					jsclick(termsandCond);
					wait.until(ExpectedConditions.attributeContains(termsandConddChkBox, "style", "none"));
					HBCBasicfeature.scrollup(checkoutNavTitle, driver);
				}
				else
				{
					Fail( "The Terms and Condition link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1219 Issue ." + e.getMessage());
				Exception(" HBC - 1219 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1220 Verify that below the "Email' field, "This is where we will...." text should be shown in greyed out color.*/
	public void HBC1220EmailDisclaimerDefText()
	{
		ChildCreation(" HBC - 1219 Verify that below the Email' field, This is where we will.... text should be shown in greyed out color.");
		if(prdtAdded==true)
		{
			try
			{
				String expVal = HBCBasicfeature.getExcelVal("HBC1220", sheet, 2);
				String expcol = HBCBasicfeature.getExcelVal("HBC1220", sheet, 1);
				if(HBCBasicfeature.isElementPresent(emailDisclaimerTxt))
				{
					Pass( "The Email Disclaimer Text is displayed.");
					String txt = emailDisclaimerTxt.getText();
					String txtCol = emailDisclaimerTxt.getCssValue("color");
					Color col = Color.fromString(txtCol);
					String hexCode = col.asHex();
					log.add( "The Expected Value was : " + expVal);
					log.add( "The Actual Value is : " + txt);
					if(expVal.equalsIgnoreCase(txt))
					{
						Pass( "There is no mismatch in the expected and the actual value.",log);
					}
					else
					{
						Fail( "There is some mismatch in the expected and the actual value.",log);
					}
					
					log.add( "The Expected Value was : " + expcol);
					log.add( "The Actual Value is : " + hexCode);
					if(expcol.equalsIgnoreCase(hexCode))
					{
						Pass( "There is no mismatch in the expected and the actual color.",log);
					}
					else
					{
						Fail( "There is some mismatch in the expected and the actual color.",log);
					}
				}
				else
				{
					Fail( "The Email Disclaimer Text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1220 Issue ." + e.getMessage());
				Exception(" HBC - 1220 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1221 Guest Checkout - Shipping Address Section > Verify that "Same as billing address" checkbox should be selected by default in the shipping address section, when the country is selected is in Canada in Billing address section.*/
	public void HBC1221SameEmailDefaultChecked()
	{
		ChildCreation(" HBC - 1221 Guest Checkout - Shipping Address Section > Verify that Same as billing address checkbox should be selected by default in the shipping address section, when the country is selected is in Canada in Billing address section.");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(checkoutSameAsBillingAddress))
				{
					Pass( "The Same as shipping address checkbox is dispalyed.");
					boolean chkd = checkoutSameAsBillingAddress.getAttribute("class").contains("checked");
					if(chkd==true)
					{
						Pass( "The Checkbox is in checked state by default.",log);
					}
					else
					{
						Fail( "The Checkbox is not in checked state by default.",log);
					}
				}
				else
				{
					Fail( "The Same as shipping address is not dispalyed.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1221 Issue ." + e.getMessage());
				Exception(" HBC - 1221 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1223 Guest Checkout - Shipping Address Section > Verify that shipping address details should not be updated on editing the billing address details, when the "Same as billing address" option is not selected.*/
	public void HBC1223BillingAddressNoUpdate()
	{
		ChildCreation(" HBC - 1223 Guest Checkout - Shipping Address Section > Verify that shipping address details should not be updated on editing the billing address details, when the Same as billing address option is not selected.");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(checkoutSameAsBillingAddress))
				{
					Pass( "The Same as shipping address checkbox is dispalyed.");
					boolean chkd = checkoutSameAsBillingAddress.getAttribute("class").contains("checked");
					if(chkd==true)
					{
						Pass( "The Checkbox is in checked state by default.",log);
						jsclick(checkoutSameAsBillingAddress);
						wait.until(ExpectedConditions.attributeContains(checkoutSameAsBillingAddress, "class", "checkbox"));
					}
				}
				else
				{
					Fail( "The Same as shipping address is not dispalyed.",log);
				}
				
				String sfn = HBCBasicfeature.getExcelVal("HBC1223", sheet, 1);
				String lsn = HBCBasicfeature.getExcelVal("HBC1223", sheet, 2);
				String stAdd = HBCBasicfeature.getExcelVal("HBC1223", sheet, 3);
				String cty = HBCBasicfeature.getExcelVal("HBC1223", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1223", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1223", sheet, 6);
				String cNum = HBCBasicfeature.getExcelNumericVal("HBC1223", sheet, 7);
				
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					if(HBCBasicfeature.isElementPresent(firstName))
					{
						Pass( "The Billing Address First Name field is displayed.");
						firstName.clear();
						firstName.sendKeys(sfn);
						String val = sfirstName.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + sfn);
						log.add ( "The Value in the Shipping First Name field is : " + val);
						if(val.isEmpty())
						{
							Pass ( "The Shipping Address Field is empty.");
						}
						else
						{
							Fail( " The Shipping Address First Name is not empty when Same as billing address is uncheck.",log);
						}
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(lastName))
					{
						Pass( "The Billing Address Last Name field is displayed.");
						lastName.clear();
						lastName.sendKeys(lsn);
						String val = slastName.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + lsn);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Pass ( "The Shipping Address Field is empty.");
						}
						else
						{
							Fail( " The Shipping Address Last Name is not empty when Same as billing address is uncheck.",log);
						}
					}
					else
					{
						Fail( "The Billing Address Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(staddress))
					{
						Pass( "The Shipping Address Street Address field is displayed.");
						staddress.clear();
						staddress.sendKeys(stAdd);
						String val = staddress2.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + stAdd);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Pass ( "The Shipping Address Field is empty.");
						}
						else
						{
							Fail( " The Shipping Address Street Address is not empty when Same as billing address is uncheck.",log);
						}
					}
					else
					{
						Fail( "The Billing Address Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(city))
					{
						Pass( "The Billing Address City field is displayed.");
						city.clear();
						city.sendKeys(cty);
						String val = scity.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + cty);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Pass ( "The  Address Field is empty.");
						}
						else
						{
							Fail( " The Shipping Address City is not empty when Same as billing address is uncheck.",log);
						}
					}
					else
					{
						Fail( "The Billing Address City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(bcountrySelection))
					{
						Pass( "The Shipping Address Country field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(bState))
					{
						Pass( "The Shipping Address State field is displayed.");
						Select sel = new Select(bState);
						sel.selectByValue(prv);
						log.add ( "The Value entered from the excel sheet was : " + prv);
						String val = sel.getFirstSelectedOption().getAttribute("value");
						log.add ( "The Value in the field is : " + val);
						if(val.equals(prv))
						{
							Pass ( "The Shipping Address State is not as expected .");
						}
						else
						{
							Fail( " The Shipping Address State is as expected and selected.",log);
						}
					}
					else
					{
						Fail( "The Billing State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(bpostalCode))
					{
						Pass( "The Shipping Address Postal Code field is displayed.");
						bpostalCode.clear();
						bpostalCode.sendKeys(pocode);
						String val = spostalCode.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + pocode);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Pass ( "The Shipping Address Postal Code Field is empty.");
						}
						else
						{
							Fail( " The Shipping Address Postal Code is not empty when Same as billing address is uncheck.",log);
						}
					}
					else
					{
						Fail( "The Billing Address Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(bContactNo))
					{
						Pass( "The Shipping Address Contact Number field is displayed.");
						bContactNo.clear();
						bContactNo.sendKeys(cNum);
						String val = sContactNo.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + cNum);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Pass( "The Shipping Address Contact Number Field is empty.");
						}
						else
						{
							Fail( " The Shipping Address Contact Number is not empty when Same as billing address is uncheck.",log);
						}
					}
					else
					{
						Fail( "The Billing Address Contact Number is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1223 Issue ." + e.getMessage());
				Exception(" HBC - 1223 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1224 Guest Checkout - Shipping Address Section > Verify that when the user deselect the "Same as billing address" checkbox, then it should be expanded.*/
	public void HBC1224ShippingAddressExpand()
	{
		ChildCreation(" HBC - 1224 Guest Checkout - Shipping Address Section > Verify that when the user deselect the Same as billing address checkbox, then it should be expanded.");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(checkoutSameAsBillingAddress))
				{
					Pass( "The Same as shipping address checkbox is dispalyed.");
					boolean chkd = checkoutSameAsBillingAddress.getAttribute("class").contains("checked");
					if(chkd==true)
					{
						Pass( "The Checkbox is in checked state by default.",log);
						jsclick(checkoutSameAsBillingAddress);
						wait.until(ExpectedConditions.attributeContains(checkoutSameAsBillingAddress, "class", "checkbox"));
					}
				}
				else
				{
					Fail( "The Same as shipping address is not dispalyed.",log);
				}
				
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					HBCBasicfeature.scrolldown(checkoutshipAddressForm, driver);
					Pass( "The Shipping Address Form is expanded as expected.");
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1224 Issue ." + e.getMessage());
				Exception(" HBC - 1224 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1225 Guest Checkout -Verify that expanded shipping address section should have the same text boxes and dropdowns as like billing address section.*/
	public void HBC1225ShippingAddressFields()
	{
		ChildCreation(" HBC - 1225 Guest Checkout - Verify that expanded shipping address section should have the same text boxes and dropdowns as like billing address section.");
		if(prdtAdded==true)
		{
			try
			{
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					if(HBCBasicfeature.isElementPresent(sfirstName))
					{
						Pass( "The Shipping Address First Name field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(slastName))
					{
						Pass( "The Shipping Address Last Name field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sstaddress))
					{
						Pass( "The Shipping Address Street Address field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sstaddress2))
					{
						Pass( "The Shipping Address Street Address 2 field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Street Address 2 field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(scity))
					{
						Pass( "The Shipping Address City field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sState))
					{
						Pass( "The Shipping Address State field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(spostalCode))
					{
						Pass( "The Shipping Address Postal Code field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sContactNo))
					{
						Pass( "The Shipping Address Contact Number field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sextCode))
					{
						Pass( "The Shipping Address Extension Code field is displayed.");
					}
					else
					{
						Fail( "The Shipping Address Extension Code is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1225 Issue ." + e.getMessage());
				Exception(" HBC - 1225 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1226 Guest Checkout - Shipping Address Section > Verify that user should be able to enter first name in the "First Name" field and it should accept maximum of 16 characters.*/
	public void HBC1226ShippingAddressFNLenVal()
	{
		ChildCreation(" HBC - 1226 Guest Checkout - Shipping Address Section > Verify that user should be able to enter first name in the First Name field and it should accept maximum of 16 characters.");
		if(prdtAdded==true)
		{
			try
			{
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					String[] cellVal = HBCBasicfeature.getExcelVal("HBC1226", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(sfirstName))
					{
						Pass( "The Shipping Address First Name field is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							sfirstName.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							sfirstName.sendKeys(cellVal[i]);
							if(sfirstName.getAttribute("value").isEmpty())
							{
								Fail( "The First Name is empty.");
							}
							else if(sfirstName.getAttribute("value").length()<=16)
							{
								Pass( "The first name was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The first name was enterable but it is not within the specified character limit.",log);
							}
							sfirstName.clear();
						}
					}
					else
					{
						Fail( "The Shipping Address First Name field is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1226 Issue ." + e.getMessage());
				Exception(" HBC - 1226 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1227 Guest Checkout - Shipping Address Section > Verify that user should be able to enter last name in the "Last Name" field and it should accept maximum of 16 characters.*/
	public void HBC1227ShippingAddressLNLenVal()
	{
		ChildCreation(" HBC - 1227 Guest Checkout - Shipping Address Section > Verify that user should be able to enter last name in the Last Name field and it should accept maximum of 16 characters.");
		if(prdtAdded==true)
		{
			try
			{
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					String[] cellVal = HBCBasicfeature.getExcelVal("HBC1226", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(slastName))
					{
						Pass( "The Shipping Address Last Name field is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							slastName.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							slastName.sendKeys(cellVal[i]);
							if(slastName.getAttribute("value").isEmpty())
							{
								Fail( "The Last Name is empty.");
							}
							else if(slastName.getAttribute("value").length()<=16)
							{
								Pass( "The Last Name  was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Last Name  was enterable but it is not within the specified character limit.",log);
							}
							slastName.clear();
						}
					}
					else
					{
						Fail( "The Shipping Address First Name field is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1227 Issue ." + e.getMessage());
				Exception(" HBC - 1227 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1228 Guest Checkout - Shipping Address Section > Verify that only characters should be accepted in "First Name" and "Last Name" field.*/
	public void HBC1228ShippingAddressFVLNCharVal()
	{
		ChildCreation(" HBC - 1228 Guest Checkout - Shipping Address Section > Verify that only characters should be accepted in First Name and Last Name field.");
		if(prdtAdded==true)
		{
			try
			{
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					String[] cellVal = HBCBasicfeature.getExcelVal("HBC1179", sheet, 1).split("\n");
					String[] alert = HBCBasicfeature.getExcelVal("HBC1179", sheet, 2).split("\n");
					if(HBCBasicfeature.isElementPresent(sfirstName))
					{
						Pass( "The First Name field is displayed.");
						sfirstName.click();
						sfirstName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[0].toString());
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						log.add( "The expected alert was : " + alert[0].toString());
						sfirstName.sendKeys(cellVal[0]);
						slastName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSFirstNameAlert,"style","block"));
						String fNameAlert = checkoutSFirstNameAlert.getText();
						
						if(fNameAlert.equalsIgnoreCase(alert[0].toString()))
						{
							Pass( "The first name alert was as expected.",log);
						}
						else
						{
							Fail( "The first name alert was not as expected.",log);
						}
						sfirstName.clear();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(slastName))
					{
						Pass( "The Last Name field is displayed.");
						slastName.clear();
						slastName.click();
						//slastName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[1].toString());
						log.add( "The Value from the excel file was : " + cellVal[1].toString());
						log.add( "The expected alert was : " + alert[1].toString());
						slastName.sendKeys(cellVal[1]);
						sfirstName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSLastNameAlert,"style","block"));
						String lNameAlert = checkoutSLastNameAlert.getText();
						
						if(lNameAlert.equalsIgnoreCase(alert[1].toString()))
						{
							Pass( "The last name alert was as expected.",log);
						}
						else
						{
							Fail( "The last name alert was not as expected.",log);
						}
						slastName.clear();	
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1228 Issue ." + e.getMessage());
				Exception(" HBC - 1228 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1229 Guest Checkout - Shipping Address Section > Verify that "First Name" and "Last Name" field should accept numbers and special characters and the corresponding alert message should be shown.*/
	public void HBC1229FNLNameNumSplCharValidation()
	{
		ChildCreation(" HBC - 1229 Guest Checkout - Shipping Address Section > Verify that First Name and Last Name field should accept numbers and special characters and the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					
					String[] cellVal = HBCBasicfeature.getExcelVal("HBC1180", sheet, 1).split("\n");
					String[] alert = HBCBasicfeature.getExcelVal("HBC1180", sheet, 2).split("\n");
					if(HBCBasicfeature.isElementPresent(sfirstName))
					{
						Pass( "The First Name field is displayed.");
						sfirstName.click();
						sfirstName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[0].toString());
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						log.add( "The expected alert was : " + alert[0].toString());
						sfirstName.sendKeys(cellVal[0]);
						slastName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSFirstNameAlert,"style","block"));
						String fNameAlert = checkoutSFirstNameAlert.getText();
						
						if(fNameAlert.equalsIgnoreCase(alert[0].toString()))
						{
							Pass( "The first name alert was as expected.",log);
						}
						else
						{
							Fail( "The first name alert was not as expected.",log);
						}
						sfirstName.clear();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(slastName))
					{
						Pass( "The First Name field is displayed.");
						slastName.clear();
						slastName.click();
						slastName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[1].toString());
						log.add( "The Value from the excel file was : " + cellVal[1].toString());
						log.add( "The expected alert was : " + alert[1].toString());
						slastName.sendKeys(cellVal[1]);
						sfirstName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSLastNameAlert,"style","block"));
						String lNameAlert = checkoutSLastNameAlert.getText();
						
						if(lNameAlert.equalsIgnoreCase(alert[1].toString()))
						{
							Pass( "The last name alert was as expected.",log);
						}
						else
						{
							Fail( "The last name alert was not as expected.",log);
						}
						slastName.clear();	
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1229 Issue ." + e.getMessage());
				Exception(" HBC - 1229 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1230 Guest Checkout - Shipping & Billing Address Section > Verify that on entering the alphabets(i.e.small letters) in the "First Name" and "Last Name" field and when tapping on other text boxes, the first letter should be automatically made to be capital.*/
	public void HBC1230FNLNameTitleCase()
	{
		ChildCreation(" HBC - 1230 Guest Checkout - Shipping & Billing Address Section > Verify that on entering the alphabets(i.e.small letters) in the First Name and Last Name field and when tapping on other text boxes, the first letter should be automatically made to be capital.");
		if(prdtAdded==true)
		{
			try
			{
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					String[] cellVal = HBCBasicfeature.getExcelVal("HBC1230", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(sfirstName))
					{
						Pass( "The First Name field is displayed.");
						sfirstName.click();
						sfirstName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[0].toString());
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						sfirstName.sendKeys(cellVal[0]);
						slastName.click();
						
						String fNameAlert = sfirstName.getAttribute("value");
						String capword  = WordUtils.capitalizeFully(cellVal[0].toString());
						log.add( "The Current Value in the First Name field is  : " + capword);
						if(fNameAlert.equals(capword))
						{
							Pass( "The first name was as expected.",log);
						}
						else
						{
							Fail( "The first name was not as expected.",log);
						}
						sfirstName.clear();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(slastName))
					{
						Pass( "The First Name field is displayed.");
						slastName.click();
						slastName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[0].toString());
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						slastName.sendKeys(cellVal[1]);
						sfirstName.click();
						
						String sNameAlert = slastName.getAttribute("value");
						String capword  = WordUtils.capitalizeFully(cellVal[1].toString());
						log.add( "The Current Value in the Last Name field is  : " + capword);
						if(sNameAlert.equals(capword))
						{
							Pass( "The last name was as expected.",log);
						}
						else
						{
							Fail( "The last name was not as expected.",log);
						}
						slastName.clear();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1230 Issue ." + e.getMessage());
				Exception(" HBC - 1230 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1231 Guest Checkout - Shipping Address Section > Verify that "Street Address" field should have two text boxes.*/
	public void HBC1231StAddFields()
	{
		ChildCreation(" HBC - 1231 Guest Checkout - Shipping Address Section > Verify that Street Address field should have two text boxes.");
		if(prdtAdded==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(sstaddress))
				{
					Pass( "The Street Address field is displayed.");
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(sstaddress2))
				{
					Pass( "The Street Address 2 field is displayed.");
				}
				else
				{
					Fail( "The Street Address 2 field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1231 Issue ." + e.getMessage());
				Exception(" HBC - 1231 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1232 Guest Checkout - Shipping Address Section > Verify that user should be able to enter street address in the "Street Address" field and it should accept maximum of 70 characters in the 1st text box and 50 characters in the 2nd text box.*/
	public void HBC1232StAddFieldsLenVal()
	{
		ChildCreation(" HBC - 1232 Guest Checkout - Shipping Address Section > Verify that user should be able to enter street address in the Street Address field and it should accept maximum of 70 characters in the 1st text box and 50 characters in the 2nd text box.");
		if(prdtAdded==true)
		{
			try
			{
				String[] st1 = HBCBasicfeature.getExcelVal("HBC1184", sheet, 1).split("\n");
				String[] st2 = HBCBasicfeature.getExcelVal("HBC1184", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(sstaddress))
				{
					Pass( "The Street Address field is displayed.");
					for(int i = 0; i<st1.length;i++)
					{
						sstaddress.clear();
						log.add( "The Entered value from the excel file is : " + st1[i].toString());
						log.add( "The Value from the excel file was : " + st1[i].toString() + " and is length is : " + st1[i].toString().length());
						sstaddress.sendKeys(st1[i]);
						if(sstaddress.getAttribute("value").isEmpty())
						{
							Fail( "The Street Address is empty.");
						}
						else if(sstaddress.getAttribute("value").length()<=70)
						{
							Pass( "The Street Address was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Street Address was enterable but it is not within the specified character limit.",log);
						}
						sstaddress.clear();
					}
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(sstaddress2))
				{
					Pass( "The Street Address 2 field is displayed.");
					for(int i = 0; i<st2.length;i++)
					{
						sstaddress2.clear();
						log.add( "The Entered value from the excel file is : " + st2[i].toString());
						log.add( "The Value from the excel file was : " + st2[i].toString() + " and is length is : " + st2[i].toString().length());
						sstaddress2.sendKeys(st2[i]);
						if(sstaddress2.getAttribute("value").isEmpty())
						{
							Fail( "The Street Address 2 is empty.");
						}
						else if(sstaddress2.getAttribute("value").length()<=50)
						{
							Pass( "The Street Address 2 was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Street Address 2 was enterable but it is not within the specified character limit.",log);
						}
						sstaddress2.clear();
					}
				}
				else
				{
					Fail( "The Street Address 2 field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1232 Issue ." + e.getMessage());
				Exception(" HBC - 1232 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1233 Guest Checkout - Shipping Address Section > Verify that user should be able to enter characters in the "City" field.*/
	public void HBC1233CityEnterable()
	{
		ChildCreation(" HBC - 1233 Guest Checkout - Shipping Address Section > Verify that user should be able to enter characters in the City field.");
		if(prdtAdded==true)
		{
			try
			{
				String cityy = HBCBasicfeature.getExcelVal("HBC1185", sheet, 1);
				if(HBCBasicfeature.isElementPresent(scity))
				{
					Pass( "The City field is displayed.");
					scity.click();
					scity.clear();
					log.add( "The Entered value from the excel file is : " + cityy);
					scity.sendKeys(cityy);
					String ctyVal = scity.getAttribute("value");
					
					if(ctyVal.isEmpty())
					{
						Fail( "The city field is empty.",log);
					}
					else
					{
						Pass( "The city name was enterable.",log);
					}
					scity.clear();
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1233 Issue ." + e.getMessage());
				Exception(" HBC - 1233 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1234 Guest Checkout - Shipping Address Section > Verify that "City" field should accept numbers and special characters and the corresponding alert message should be shown.*/
	public void HBC1234CityyValidation()
	{
		ChildCreation(" HBC - 1234 Guest Checkout - Shipping Address Section > Verify that City field should accept numbers and special characters and the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1186", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1186", sheet, 2);
				if(HBCBasicfeature.isElementPresent(scity))
				{
					Pass( "The First Name field is displayed.");
					scity.click();
					scity.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					scity.sendKeys(cellVal);
					sstaddress2.click();
					wait.until(ExpectedConditions.attributeContains(checkoutSCityAlert,"style","block"));
					String cityAlert = checkoutSCityAlert.getText();
					
					if(cityAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The City alert was as expected.",log);
					}
					else
					{
						Fail( "The City alert was not as expected.",log);
					}
					scity.clear();
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1234 Issue ." + e.getMessage());
				Exception(" HBC - 1234 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1235 Guest Checkout - Shipping Address Section > Verify that selected "Country" option in the billing address section should be same in the "Country" option in the shipping address section.*/
	public void HBC1235ShippingDisplayedCountry()
	{
		ChildCreation(" HBC - 1235 Guest Checkout - Shipping Address Section > Verify that selected Country option in the billing address section should be same in the Country option in the shipping address section.");
		if(prdtAdded==true)
		{
			try
			{
				String bCountry = "",sCountry = "";
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					bCountry = sel.getFirstSelectedOption().getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(scountrySelection))
				{
					Pass( "The Country field is displayed.");
					sCountry = scountrySelection.getText();
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				
				log.add( "The displayed billing country is  : " + bCountry);
				log.add( "The displayed shipping country is  : " + sCountry);
				if(bCountry.equalsIgnoreCase(sCountry))
				{
					Pass( "The Country displayed in the Billing and Shipping are same.",log);
				}
				else
				{
					Fail( "The Country displayed in the Billing and Shipping are not same.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1235 Issue ." + e.getMessage());
				Exception(" HBC - 1235 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1236 Guest Checkout - Shipping Address Section > Verify that "Province" dropdown field should be selected in "Select" option by default.*/
	public void HBC1236ShippingCADefaultStateSel()
	{
		ChildCreation(" HBC - 1236 Guest Checkout - Shipping Address Section > Verify that Province dropdown field should be selected in Select option by default.");
		if(prdtAdded==true)
		{
			try
			{
				String expVal = HBCBasicfeature.getExcelVal("HBC1199", sheet, 1);
				if(HBCBasicfeature.isElementPresent(sState))
				{
					Select sel = new Select(sState);
					String actVal = sel.getFirstSelectedOption().getText();
					log.add( "The Expected Value was : " + expVal);
					log.add( "The Actual Value is : " + actVal);
					if(expVal.equalsIgnoreCase(actVal))
					{
						Pass( "The Expected Default Value is selected.",log);
					}
					else
					{
						Fail( "The Expected Default Value is not selected.",log);
					}
				}
				else
				{
					Fail( "The State drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1236 Issue ." + e.getMessage());
				Exception(" HBC - 1236 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1237 Guest Checkout - Shipping Address Section > Verify that "Postal Code" field should accept alphanumeric characters.*/
	public void HBC1237ShippingCAPOAlphaNumericValidation()
	{
		ChildCreation(" HBC - 1237 Guest Checkout - Shipping Address Section > Verify that Postal Code field should accept alphanumeric characters.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1209", sheet, 1);
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
					
					if(HBCBasicfeature.isElementPresent(checkoutSameAsBillingAddress))
					{
						Pass( "The Same as shipping address checkbox is dispalyed.");
						boolean chkd = checkoutSameAsBillingAddress.getAttribute("class").contains("checked");
						if(chkd==true)
						{
							Pass( "The Checkbox is in checked state by default.",log);
							jsclick(checkoutSameAsBillingAddress);
							wait.until(ExpectedConditions.attributeContains(checkoutSameAsBillingAddress, "class", "checkbox"));
						}
					}
				}
				
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					Pass( "The Shipping Address Form is expanded as expected.");
					if(HBCBasicfeature.isElementPresent(spostalCode))
					{
						Pass( "The Postal Code field is displayed.");
						spostalCode.click();
						spostalCode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						spostalCode.sendKeys(cellVal);
						String val = spostalCode.getAttribute("value");
						log.add( "The Current value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail( "The User was not able to enter any value in the Postal Code field of the shipping address.");
						}
						else if(val.equals(cellVal))
						{
							Pass( "The User able to enter value in the Postal Code field of the shipping address and it matches.",log);
						}
						else
						{
							Fail( "The User was able to enter value in the Postal Code field of the shipping address and it does not matches.",log);
						}
						
						spostalCode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail( " The Shipping address is not expanded");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1209 Issue ." + e.getMessage());
				Exception(" HBC - 1209 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1238 Guest Checkout - Shipping Address Section > Verify that "Postal Code" field should accept maximum of 7 characters.*/
	public void HBC1238ShippingCAPOLength()
	{
		ChildCreation(" HBC - 1238 Guest Checkout - Shipping Address Section > Verify that Postal Code field should accept maximum of 7 characters.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1207", sheet, 1).split("\n");
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(spostalCode))
				{
					Pass( "The Postal Code Area is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						spostalCode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						spostalCode.sendKeys(cellVal[i]);
						if(spostalCode.getAttribute("value").isEmpty())
						{
							Fail( "The Zip Code field is empty.");
						}
						else if(spostalCode.getAttribute("value").length()<=7)
						{
							Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
						}
						spostalCode.clear();
					}
				}
				else
				{
					Fail( "The Postal Code Area is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1238 Issue ." + e.getMessage());
				Exception(" HBC - 1238 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1239 Guest Checkout - Shipping Address Section > Verify that corresponding alert message should be shown when the user entered less than 6 digits in the "Postal Code" field.*/
	public void HBC1239ShippingCAPOLengthValidationAlert()
	{
		ChildCreation(" HBC - 1239 Guest Checkout - Shipping Address Section > Verify that corresponding alert message should be shown when the user entered less than 6 digits in the Postal Code field.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1208", sheet, 1);
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1239", sheet, 2);
				if(HBCBasicfeature.isElementPresent(spostalCode))
				{
					Pass( "The Postal Code field is displayed.");
					spostalCode.click();
					spostalCode.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					spostalCode.sendKeys(cellVal);
					Thread.sleep(1000);
					sContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					String poAlert = checkoutBPOAlert.getText();
					log.add( "The Actual Alert was : " + poAlert);
					
					if(poAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					spostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1239 Issue ." + e.getMessage());
				Exception(" HBC - 1239 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1240 Guest Checkout - Shipping Address Section > Verify that corresponding alert message should be shown when the user entered alphabets or numbers in the "Postal Code" field.*/
	public void HBC1240ShippingCAPOAlphaNumericValidation()
	{
		ChildCreation(" HBC - 1240 Guest Checkout - Shipping Address Section > Verify that corresponding alert message should be shown when the user entered alphabets or numbers in the Postal Code field.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1209", sheet, 1);
				Select sel;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1239", sheet, 2);
				if(HBCBasicfeature.isElementPresent(spostalCode))
				{
					Pass( "The First Name field is displayed.");
					spostalCode.click();
					spostalCode.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					spostalCode.sendKeys(cellVal);
					Thread.sleep(1000);
					sContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					String poAlert = checkoutBPOAlert.getText();
					log.add( "The Actual Alert was : " + poAlert);
					if(poAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					spostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1240 Issue ." + e.getMessage());
				Exception(" HBC - 1240 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1241 Guest Checkout - Shipping Address Section > Verify that user should be able to enter phone number in the "Phone Number" field and it should accept maximum of 10 numbers.*/
	public void HBC1241ShippingCAPHNumLenValidation()
	{
		ChildCreation(" HBC - 1241 Guest Checkout - Shipping Address Section > Verify that user should be able to enter phone number in the Phone Number field and it should accept maximum of 10 numbers.");
		if(prdtAdded==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1210", sheet, 1).split("\n");
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(HBCBasicfeature.isElementPresent(sContactNo))
				{
					Pass( "The Contact Number is displayed.");
					for(int i = 0; i<cellVal.length;i++)
					{
						sContactNo.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						sContactNo.sendKeys(cellVal[i]);
						if(sContactNo.getAttribute("value").isEmpty())
						{
							Fail( "The Contact Number field is empty.");
						}
						else if(sContactNo.getAttribute("value").length()<=10)
						{
							Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
						}
						sContactNo.clear();
					}
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1241 Issue ." + e.getMessage());
				Exception(" HBC - 1241 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1242 Guest Checkout - Shipping Address Section > Verify that when the user entered alphabets or special characters in the "Phone Number" field, then the corresponding alert message should be shown.*/
	public void HBC1242ShippingCAPHAphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1242 Guest Checkout - Shipping Address Section > Verify that when the user entered alphabets or special characters in the Phone Number field, then the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1211", sheet, 1);
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1211", sheet, 2);
				if(HBCBasicfeature.isElementPresent(sContactNo))
				{
					Pass( "The Contact Number field is displayed.");
					sContactNo.click();
					sContactNo.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The expected alert was : " + alert.toString());
					sContactNo.sendKeys(cellVal);
					spostalCode.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(checkoutScaPHAlert));
					//wait.until(ExpectedConditions.attributeContains(checkoutScaPHAlert,"style","block"));
					String ctAlert = checkoutScaPHAlert.getText();
					
					if(ctAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Phone Number alert was as expected.",log);
					}
					else
					{
						Fail( "The Phone Number alert was not as expected.",log);
					}
					sContactNo.clear();
				}
				else
				{
					Fail( "The Contact Number field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1242 Issue ." + e.getMessage());
				Exception(" HBC - 1242 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1243 Guest Checkout - Shipping Address Section > Verify that when the "Phone Number" field is left blank and tapping on other text boxes, then the corresponding alert message should be shown.*/
	public void HBC1243ShippingCAEmptyPHValidationAlert()
	{
		ChildCreation(" HBC - 1243 Guest Checkout - Shipping Address Section > Verify that when the Phone Number field is left blank and tapping on other text boxes, then the corresponding alert message should be shown.");
		if(prdtAdded==true)
		{
			try
			{
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				String alert = HBCBasicfeature.getExcelVal("HBC1243", sheet, 1);
				if(HBCBasicfeature.isElementPresent(sContactNo))
				{
					Pass( "The Contact Number field is displayed.");
					sContactNo.click();
					sContactNo.clear();
					spostalCode.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(checkoutScaPHAlert));
					//wait.until(ExpectedConditions.attributeContains(checkoutScaPHAlert,"style","block"));
					String ctAlert = checkoutScaPHAlert.getText();
					
					if(ctAlert.equalsIgnoreCase(alert.toString()))
					{
						Pass( "The Phone Number alert was as expected.",log);
					}
					else
					{
						Fail( "The Phone Number alert was not as expected.",log);
					}
					sContactNo.clear();
				}
				else
				{
					Fail( "The Contact Number field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1243 Issue ." + e.getMessage());
				Exception(" HBC - 1243 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1244 Guest Checkout - On entering only space in any of the input fields and selecting "Ok" button then the alert must be shown.*/
	public void HBC1244ShippingAddressfieldsEmptyValidation()
	{
		ChildCreation(" HBC - 1244 Guest Checkout - On entering only space in any of the input fields and selecting Ok button then the alert must be shown.");
		if(prdtAdded==true)
		{
			try
			{
				String[] alert = HBCBasicfeature.getExcelVal("HBC1218", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(sfirstName))
				{
					Pass( "The First Name field is displayed.");
					sfirstName.click();
					sfirstName.clear();
					slastName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutSFirstNameAlert,"style","block"));
					String fNameAlert = checkoutSFirstNameAlert.getText();
					
					if(fNameAlert.equalsIgnoreCase(alert[0].toString()))
					{
						Pass( "The first name alert was as expected.",log);
					}
					else
					{
						Fail( "The first name alert was not as expected.",log);
					}
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(slastName))
				{
					Pass( "The Last Name field is displayed.");
					slastName.clear();
					slastName.click();
					slastName.clear();
					log.add( "The expected alert was : " + alert[1].toString());
					sfirstName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutSLastNameAlert,"style","block"));
					String lNameAlert = checkoutSLastNameAlert.getText();
					
					if(lNameAlert.equalsIgnoreCase(alert[1].toString()))
					{
						Pass( "The last name alert was as expected.",log);
					}
					else
					{
						Fail( "The last name alert was not as expected.",log);
					}
					slastName.clear();	
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(sstaddress))
				{
					Pass( "The Street Address field is displayed.");
					sstaddress.click();
					sstaddress.clear();
					slastName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutSStaddressAlert,"style","block"));
					String stAddAlert = checkoutSStaddressAlert.getText();
					
					if(stAddAlert.equalsIgnoreCase(alert[2].toString()))
					{
						Pass( "The Street Address alert was as expected.",log);
					}
					else
					{
						Fail( "The Street Address alert was not as expected.",log);
					}
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(scity))
				{
					Pass( "The City field is displayed.");
					scity.click();
					scity.clear();
					sstaddress2.click();
					wait.until(ExpectedConditions.attributeContains(checkoutSCityAlert,"style","block"));
					String cityAlert = checkoutSCityAlert.getText();
					
					if(cityAlert.equalsIgnoreCase(alert[3].toString()))
					{
						Pass( "The City alert was as expected.",log);
					}
					else
					{
						Fail( "The City alert was not as expected.",log);
					}
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(spostalCode))
				{
					Pass( "The Postal Code field is displayed.");
					spostalCode.click();
					spostalCode.clear();
					sContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					String poAlert = checkoutBPOAlert.getText();
					log.add( "The actual alert was : " + poAlert);
					if(poAlert.equalsIgnoreCase(alert[5].toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					spostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(sContactNo))
				{
					Pass( "The Contact Number field is displayed.");
					sContactNo.click();
					sContactNo.clear();
					spostalCode.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(checkoutScaPHAlert));
					//wait.until(ExpectedConditions.attributeContains(checkoutScaPHAlert,"style","block"));
					String ctAlert = checkoutScaPHAlert.getText();
					if(ctAlert.equalsIgnoreCase(alert[8].toString()))
					{
						Pass( "The Phone Number alert was as expected.",log);
					}
					else
					{
						Fail( "The Phone Number alert was not as expected.",log);
					}
					sContactNo.clear();
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1244 Issue ." + e.getMessage());
				Exception(" HBC - 1244 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1245 Guest Checkout - On entering html tags in the fields should not be treated as strings.*/
	public void HBC1245BillShipHtmlValidation()
	{
		ChildCreation(" HBC - 1245 Guest Checkout - On entering html tags in the fields should not be treated as strings.");
		if(prdtAdded==true)
		{
			try
			{
				String cellval = HBCBasicfeature.getExcelVal("HBC1245", sheet, 1);
				String[] alert = HBCBasicfeature.getExcelVal("HBC1245", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
					firstName.click();
					firstName.clear();
					firstName.sendKeys(cellval);
					lastName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBFirstNameAlert,"style","block"));
					String fNameAlert = checkoutBFirstNameAlert.getText();
					log.add( "The Actual Alert was : " + fNameAlert);
					if(fNameAlert.equalsIgnoreCase(alert[0].toString()))
					{
						Pass( "The first name alert was as expected.",log);
					}
					else
					{
						Fail( "The first name alert was not as expected.",log);
					}
					firstName.clear();
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The Last Name field is displayed.");
					lastName.clear();
					lastName.click();
					lastName.sendKeys(cellval);
					log.add( "The expected alert was : " + alert[1].toString());
					firstName.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBLastNameAlert,"style","block"));
					String lNameAlert = checkoutBLastNameAlert.getText();
					log.add( "The Actual Alert was : " + lNameAlert);
					if(lNameAlert.equalsIgnoreCase(alert[1].toString()))
					{
						Pass( "The last name alert was as expected.",log);
					}
					else
					{
						Fail( "The last name alert was not as expected.",log);
					}
					lastName.clear();	
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(city))
				{
					Pass( "The City field is displayed.");
					city.click();
					city.clear();
					city.sendKeys(cellval);
					staddress2.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBCityAlert,"style","block"));
					String cityAlert = checkoutBCityAlert.getText();
					log.add( "The Actual Alert was : " + cityAlert);
					if(cityAlert.equalsIgnoreCase(alert[2].toString()))
					{
						Pass( "The City alert was as expected.",log);
					}
					else
					{
						Fail( "The City alert was not as expected.",log);
					}
					city.clear();
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
				
				/*if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Kingdom");
					if(cnty==false)
					{
						sel.selectByValue("GB");
						wait.until(ExpectedConditions.visibilityOf(bukState));
					}
					if(HBCBasicfeature.isElementPresent(bukState))
					{
						Pass( "The State field is displayed.");
						bukState.click();
						bukStateEnter.sendKeys(cellval);
						city.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBUKStateAlert,"style","block"));
						String stAlert = checkoutBUKStateAlert.getText();
						
						if(stAlert.equalsIgnoreCase(alert[3].toString()))
						{
							Pass( "The State or Province alert was as expected.",log);
						}
						else
						{
							Fail( "The State or Province alert was not as expected.",log);
						}
						
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
					
					cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}*/
				
				if(HBCBasicfeature.isElementPresent(bpostalCode))
				{
					Select sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("Canada");
					if(cnty==false)
					{
						sel.selectByValue("CA");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
					Pass( "The Postal Code field is displayed.");
					bpostalCode.click();
					bpostalCode.clear();
					bpostalCode.sendKeys(cellval);
					bContactNo.click();
					wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
					String poAlert = checkoutBPOAlert.getText();
					log.add( "The Actual Alert was : " + poAlert);
					if(poAlert.equalsIgnoreCase(alert[3].toString()))
					{
						Pass( "The Zip Code alert was as expected.",log);
					}
					else
					{
						Fail( "The Zip Code alert was not as expected.",log);
					}
					bpostalCode.clear();
				}
				else
				{
					Fail( "The Postal Code is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bContactNo))
				{
					Pass( "The Contact Number field is displayed.");
					bContactNo.click();
					bContactNo.clear();
					bContactNo.sendKeys(cellval);
					bpostalCode.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(checkoutBcaPHAlert));
					//wait.until(ExpectedConditions.attributeContains(checkoutBcaPHAlert,"style","block"));
					String ctAlert = checkoutBcaPHAlert.getText();
					log.add( "The Actual Alert was : " + ctAlert);
					if(ctAlert.equalsIgnoreCase(alert[4].toString()))
					{
						Pass( "The Phone Number alert was as expected.",log);
					}
					else
					{
						Fail( "The Phone Number alert was not as expected.",log);
					}
					bContactNo.clear();
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(checkoutSameAsBillingAddress))
				{
					Pass( "The Same as shipping address checkbox is dispalyed.");
					boolean chkd = checkoutSameAsBillingAddress.getAttribute("class").contains("checked");
					if(chkd==true)
					{
						Pass( "The Checkbox is in checked state by default.",log);
						jsclick(checkoutSameAsBillingAddress);
						wait.until(ExpectedConditions.attributeContains(checkoutSameAsBillingAddress, "class", "checkbox"));
					}
				}
				else
				{
					Fail( "The Same as shipping address is not dispalyed.",log);
				}
				
				wait.until(ExpectedConditions.attributeContains(checkoutshipAddressForm, "style", "block"));
				boolean frmExpand = checkoutshipAddressForm.getAttribute("style").contains("block");
				if(frmExpand==true)
				{
					Pass( "The Shipping Address Form is expanded as expected.");
					HBCBasicfeature.scrolldown(checkoutshipAddressForm, driver);
					if(HBCBasicfeature.isElementPresent(sfirstName))
					{
						Pass( "The First Name field is displayed.");
						sfirstName.click();
						sfirstName.clear();
						sfirstName.sendKeys(cellval);
						slastName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSFirstNameAlert,"style","block"));
						String fNameAlert = checkoutSFirstNameAlert.getText();
						log.add( "The Actual Alert was : " + fNameAlert);
						if(fNameAlert.equalsIgnoreCase(alert[0].toString()))
						{
							Pass( "The first name alert was as expected.",log);
						}
						else
						{
							Fail( "The first name alert was not as expected.",log);
						}
						sfirstName.clear();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(slastName))
					{
						Pass( "The Last Name field is displayed.");
						slastName.clear();
						slastName.click();
						slastName.sendKeys(cellval);
						log.add( "The expected alert was : " + alert[1].toString());
						sfirstName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSLastNameAlert,"style","block"));
						String lNameAlert = checkoutSLastNameAlert.getText();
						log.add( "The Actual Alert was : " + lNameAlert);
						if(lNameAlert.equalsIgnoreCase(alert[1].toString()))
						{
							Pass( "The last name alert was as expected.",log);
						}
						else
						{
							Fail( "The last name alert was not as expected.",log);
						}
						slastName.clear();	
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					/*if(HBCBasicfeature.isElementPresent(sstaddress))
					{
						Pass( "The Street Address field is displayed.");
						sstaddress.click();
						sstaddress.clear();
						sstaddress.sendKeys(cellval);
						slastName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSStaddressAlert,"style","block"));
						String stAddAlert = checkoutSStaddressAlert.getText();
						
						if(stAddAlert.equalsIgnoreCase(alert[2].toString()))
						{
							Pass( "The Street Address alert was as expected.",log);
						}
						else
						{
							Fail( "The Street Address alert was not as expected.",log);
						}
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}*/
					
					if(HBCBasicfeature.isElementPresent(scity))
					{
						Pass( "The City field is displayed.");
						scity.click();
						scity.clear();
						scity.sendKeys(cellval);
						sstaddress2.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSCityAlert,"style","block"));
						String cityAlert = checkoutSCityAlert.getText();
						log.add( "The Actual Alert was : " + cityAlert);
						if(cityAlert.equalsIgnoreCase(alert[2].toString()))
						{
							Pass( "The City alert was as expected.",log);
						}
						else
						{
							Fail( "The City alert was not as expected.",log);
						}
						scity.clear();
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(spostalCode))
					{
						Pass( "The Postal Code field is displayed.");
						spostalCode.click();
						spostalCode.clear();
						spostalCode.sendKeys(cellval);
						sContactNo.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						String poAlert = checkoutBPOAlert.getText();
						log.add( "The Actual Alert was : " + poAlert);
						if(poAlert.equalsIgnoreCase(alert[5].toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						spostalCode.clear();
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(sContactNo))
					{
						Pass( "The Contact Number field is displayed.");
						sContactNo.click();
						sContactNo.clear();
						sContactNo.sendKeys(cellval);
						spostalCode.click();
						Thread.sleep(500);
						wait.until(ExpectedConditions.visibilityOf(checkoutScaPHAlert));
						//wait.until(ExpectedConditions.attributeContains(checkoutScaPHAlert,"style","block"));
						String ctAlert = checkoutScaPHAlert.getText().trim();
						log.add( "The Actual Alert was : " + ctAlert);
						if(ctAlert.equalsIgnoreCase(alert[4].toString().trim()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						sContactNo.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail( "The Shipping Address Form is not expanded as expected.");	
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1245 Issue ." + e.getMessage());
				Exception(" HBC - 1245 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1246 Guest Checkout - Billing Address Section > Verify that clicking on "Continue" button after entering valid details in the addresses section, "Address Verification" overlay/page is shown.*/
	public void HBC1246BillingAddAddAddressVerification()
	{
		ChildCreation(" HBC - 1246 Guest Checkout - Billing Address Section > Verify that clicking on Continue button after entering valid details in the addresses section, Address Verification overlay/page is shown.");
		if(prdtAdded==true)
		{
			try
			{
				bclearAll();
				String sfn = HBCBasicfeature.getExcelVal("HBC1246", sheet, 1);
				String lsn = HBCBasicfeature.getExcelVal("HBC1246", sheet, 2);
				String stAdd = HBCBasicfeature.getExcelVal("HBC1246", sheet, 3);
				String cty = HBCBasicfeature.getExcelVal("HBC1246", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1246", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1246", sheet, 6);
				String cNum = HBCBasicfeature.getExcelNumericVal("HBC1246", sheet, 7);
				String eml = HBCBasicfeature.getExcelVal("HBC1246", sheet, 8);
				
				if(HBCBasicfeature.isElementPresent(firstName))
				{
					Pass( "The First Name field is displayed.");
					firstName.sendKeys(sfn);
				}
				else
				{
					Fail( "The First Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(lastName))
				{
					Pass( "The Last Name field is displayed.");
					lastName.sendKeys(lsn);
				}
				else
				{
					Fail( "The Last Name field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(staddress))
				{
					Pass( "The Street Address field is displayed.");
					staddress.sendKeys(stAdd);
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(city))
				{
					Pass( "The City field is displayed.");
					city.sendKeys(cty);
				}
				else
				{
					Fail( "The City field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					Pass( "The Country field is displayed.");
					Select sel = new Select(bcountrySelection);
					sel.selectByValue("CA");
					wait.until(ExpectedConditions.attributeContains(bStateState, "style", "block"));
				}
				else
				{
					Fail( "The Country field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bState))
				{
					Pass( "The State field is displayed.");
					Select sel = new Select(bState);
					sel.selectByValue(prv);
					Thread.sleep(1000);
				}
				else
				{
					Fail( "The State field is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bpostalCode))
				{
					Pass( "The Postal Code field is displayed.");
					bpostalCode.sendKeys(pocode);
				}
				else
				{
					Fail( "The Postal Code is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(bContactNo))
				{
					Pass( "The Contact Number field is displayed.");
					bContactNo.sendKeys(cNum);
				}
				else
				{
					Fail( "The Contact Number is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(email))
				{
					Pass( "The email field is displayed.");
					email.sendKeys(eml);
				}
				else
				{
					Fail( " The email field is displayed is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(checkoutSameAsBillingAddress))
				{
					Pass( "The Same as shipping address checkbox is dispalyed.");
					boolean chkd = checkoutSameAsBillingAddress.getAttribute("class").contains("checked");
					if(chkd==false)
					{
						Pass( "The Checkbox is in checked state by default.",log);
						jsclick(checkoutSameAsBillingAddress);
						wait.until(ExpectedConditions.attributeContains(checkoutSameAsBillingAddress, "class", "checked"));
					}
				}
				else
				{
					Fail( "The Same as shipping address is not dispalyed.",log);
				}
				
				WebDriverWait w1 = new WebDriverWait(driver, 10);
				boolean addOverlay = false;
				do
				{
					try
					{
						checkoutAddressButtons.click();
						w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
						addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
						addOverlay = true;
					}
					catch(Exception e)
					{
						addOverlay = false;
					}
				}while(addOverlay==false);
				
				wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1246 Issue ." + e.getMessage());
				Exception(" HBC - 1246 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1247 Guest Checkout - Verify that "Addresses", "Shipping & Payment" and "Review & Submit" tabs should be shown as per the creative.*/
	public void HBC1247PayAndShipPgTabCreative()
	{
		ChildCreation("HBC - 1247 Guest Checkout - Verify that Addresses, Shipping & Payment and Review & Submit tabs should be shown as per the creative.");
		if(addVerEnable==true)
		{
			try
			{
				String expTitle = HBCBasicfeature.getExcelVal("HBC1247", sheet, 2);
				String expcurrCol = HBCBasicfeature.getExcelVal("HBC1247", sheet, 1);
				String othtabCol = HBCBasicfeature.getExcelVal("HBC1247", sheet, 3);
				if(HBCBasicfeature.isElementPresent(addressuseasEntered))
				{
					Pass( "The Use As Entered button is displayed.");
					jsclick(addressuseasEntered);
					wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(checkoutCurrNavTitle))
					{
						String currTitl = checkoutCurrNavTitle.getText();
						log.add( "The Expected title was : " + expTitle);
						log.add( "The Actual title is : " + currTitl);
						if(currTitl.contains(expTitle))
						{
							Pass( "The User is navigated to the expected page and the title matches.",log);
						}
						else
						{
							Fail( "The User is navigated to the expected page but the title mismatches.",log);
						}
						shipPage = true;
						
						if(HBCBasicfeature.isElementPresent(checkoutNavTab1))
						{
							Pass( "The Checkout Navigation Tab - 1 is displayed.");
							String col = checkoutNavTab1.getCssValue("color");
							Color colhex = Color.fromString(col);
							String expCol = colhex.asHex();
							log.add( "The Expected Color was : " + othtabCol);
							log.add( "The Actual Color is : " + col);
							if(expCol.contains(othtabCol))
							{
								Pass( "The Color matches the expected content.",log);
							}
							else
							{
								Fail( "The Color does not matches the expected content.",log);
							}
						}
						else
						{
							Fail( "The Checkout Navigation Tab - 1 is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(checkoutNavTab2))
						{
							Pass( "The Checkout Navigation Tab - 2 is displayed.");
							String col = checkoutNavTab2.getCssValue("color");
							Color expC = Color.fromString(col);
							String actCol = expC.asHex();
							log.add( "The Expected Color was : " + expcurrCol);
							log.add( "The Actual Color is : " + actCol);
							if(expcurrCol.contains(actCol))
							{
								Pass( "The Color matches the expected content.",log);
							}
							else
							{
								Fail( "The Color does not matches the expected content.",log);
							}
						}
						else
						{
							Fail( "The Checkout Navigation Tab - 2 is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(checkoutNavTab3))
						{
							Pass( "The Checkout Navigation Tab - 3 is displayed.");
							String col = checkoutNavTab3.getCssValue("color");
							Color expC = Color.fromString(col);
							String actCol = expC.asHex();
							log.add( "The Expected Color was : " + othtabCol);
							log.add( "The Actual Color is : " + col);
							if(actCol.contains(othtabCol))
							{
								Pass( "The Color matches the expected content.",log);
							}
							else
							{
								Fail( "The Color does not matches the expected content.",log);
							}
						}
						else
						{
							Fail( "The Checkout Navigation Tab - 3 is not displayed.");
						}
					}
					else
					{
						Fail( "The Current Title is not displayed.");
					}
				}
				else
				{
					Fail( "The Use As Entered button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1247 Issue ." + e.getMessage());
				Exception(" HBC - 1247 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1248 Guest Checkout - Verify that the current tabs should be highlighted in black, and the other tabs should be grayed out.*/
	public void HBC1248TabState()
	{
		ChildCreation(" HBC - 1248 Guest Checkout - Verify that the current tabs should be highlighted in black, and the other tabs should be grayed out.");
		if(shipPage==true)
		{
			try
			{
				String expcurrCol = HBCBasicfeature.getExcelVal("HBC1247", sheet, 1);
				String othtabCol = HBCBasicfeature.getExcelVal("HBC1247", sheet, 3);
				if(HBCBasicfeature.isElementPresent(checkoutNavTab1))
				{
					Pass( "The Checkout Navigation Tab - 1 is displayed.");
					String col = checkoutNavTab1.getCssValue("color");
					Color expC = Color.fromString(col);
					String actCol = expC.asHex();
					log.add( "The Expected Color was : " + othtabCol);
					log.add( "The Actual Color is : " + actCol);
					if(actCol.contains(othtabCol))
					{
						Pass( "The Color matches the expected content.",log);
					}
					else
					{
						Fail( "The Color does not matches the expected content.",log);
					}
				}
				else
				{
					Fail( "The Checkout Navigation Tab - 1 is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(checkoutNavTab2))
				{
					Pass( "The Checkout Navigation Tab - 2 is displayed.");
					String col = checkoutNavTab2.getCssValue("color");
					Color expC = Color.fromString(col);
					String actCol = expC.asHex();
					log.add( "The Expected Color was : " + expcurrCol);
					log.add( "The Actual Color is : " + actCol);
					if(expcurrCol.contains(actCol))
					{
						Pass( "The Color matches the expected content.",log);
					}
					else
					{
						Fail( "The Color does not matches the expected content.",log);
					}
				}
				else
				{
					Fail( "The Checkout Navigation Tab - 2 is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(checkoutNavTab3))
				{
					Pass( "The Checkout Navigation Tab - 3 is displayed.");
					String col = checkoutNavTab3.getCssValue("color");
					Color expC = Color.fromString(col);
					String actCol = expC.asHex();
					log.add( "The Expected Color was : " + othtabCol);
					log.add( "The Actual Color is : " + actCol);
					if(actCol.contains(othtabCol))
					{
						Pass( "The Color matches the expected content.",log);
					}
					else
					{
						Fail( "The Color does not matches the expected content.",log);
					}
				}
				else
				{
					Fail( "The Checkout Navigation Tab - 3 is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1248 Issue ." + e.getMessage());
				Exception(" HBC - 1248 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1249 Guest Checkout - Verify that "Shipping & Payment" tab should have "Shipping Information" & "Billing Information" section.*/
	public void HBC1249ShipBillingSections()
	{
		ChildCreation(" HBC - 1249 Guest Checkout - Verify that Shipping & Payment tab should have Shipping Information & Billing Information section.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipSec))
				{
					Pass( "The Shipping Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Shipping Section in the Shipping and Billing Tab is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypagePaySec))
				{
					Pass( "The Billing Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Billing Section in the Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1249 Issue ." + e.getMessage());
				Exception(" HBC - 1249 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1250 Guest Checkout - Shipping Information Section > Verify that Selected shipping address should be shown with "Edit" text link & "Create Address" text links below the shipping information section.*/
	public void HBC1250ShipSectionsEditCreateLinks()
	{
		ChildCreation(" HBC - 1250 Guest Checkout - Shipping Information Section > Verify that Selected shipping address should be shown with Edit text link & Create Address text links below the shipping information section.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					Pass( "The Create Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Create Link in the Shipping Section in the Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1250 Issue ." + e.getMessage());
				Exception(" HBC - 1250 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1251 Guest Checkout - Shipping Information Section > Verify that Selected Name(Customer Name) option should be shown in the dropdown below the shipping information section.*/
	/* HBC - 1252 Guest Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be shown with the prefilled shipping details on clicking the "Edit" link.*/
	/* HBC - 1255 Guest Checkout - Shipping Information Section > Verify that "X" close icon should be shown in the "Shipping Address" overlay/page.*/
	/* HBC - 1256 Guest Checkout - Shipping Information Section > Verify that "Ok" button & "Cancel" button should be shown in the "Shipping Address" overlay/page.*/
	/* HBC - 1257 Guest Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be closed on tapping the "Cancel" button.*/
	/* HBC - 1253 Guest Checkout - Shipping Information Section > Verify that clicking on "Cancel" button after making changes in the "Shipping Address" overlay/page should be displayed without any changes.*/
	public void HBC1251ShipSectionsShipToNMEditLinkDet()
	{
		ChildCreation(" HBC - 1251 Guest Checkout - Shipping Information Section > Verify that Selected Name(Customer Name) option should be shown in the dropdown below the shipping information section.");
		boolean addOverlayState = false, addOverlayCloseState = false;
		if(shipPage==true)
		{
			try
			{
				String sfn = HBCBasicfeature.getExcelVal("HBC1246", sheet, 1);
				String lsn = HBCBasicfeature.getExcelVal("HBC1246", sheet, 2);
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
				{
					Pass( " The Ship To Address Customer Drop Down is displayed.");
					Select sel = new Select(shipandPaypageShipShipToName);
					String selCust = sel.getFirstSelectedOption().getText();
					log.add( "The Expected Customer First Name was : " + sfn);
					log.add( "The Displayed Customer Name was : " + selCust);
					if(selCust.contains(sfn))
					{
						Pass( "The Entered Customer Name in the Addressess page is dispalyed in the drop down by default.",log);
					}
					else
					{
						Fail( "There is some mismatch in the expected and the actual customer name. " , log);
					}
					
					log.add( "The Expected Customer First Name was : " + lsn);
					log.add( "The Displayed Customer Name was : " + selCust);
					if(selCust.contains(lsn))
					{
						Pass( "The Entered Customer Name in the Addressess page is dispalyed in the drop down by default.",log);
					}
					else
					{
						Fail( "There is some mismatch in the expected and the actual customer name. " , log);
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
					{
						befAdd = shipandPaypageShipAddressDisp.getText();
					}
					
				}
				else
				{
					Fail( " The Ship To Address Customer Drop Down is not displayed..");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1251 Issue ." + e.getMessage());
				Exception(" HBC - 1251 Issue ." + e.getMessage());
			}
			
			/* HBC - 1252 Guest Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be shown with the prefilled shipping details on clicking the "Edit" link.*/
			ChildCreation(" HBC - 1251 Guest Checkout - Shipping Information Section > Verify that Selected Name(Customer Name) option should be shown in the dropdown below the shipping information section.");
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					jsclick(shipandPaypageShipEditLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						addOverlayState = true;
						Pass("The Edit Shipping Address Overlay is dispalyed.");
					}
					else
					{
						Fail("The Edit Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1252 Issue ." + e.getMessage());
				Exception(" HBC - 1252 Issue ." + e.getMessage());
			}
			
			/* HBC - 1255 Guest Checkout - Shipping Information Section > Verify that "X" close icon should be shown in the "Shipping Address" overlay/page.*/
			ChildCreation(" HBC - 1255 Guest Checkout - Shipping Information Section > Verify that X close icon should be shown in the Shipping Address overlay/page.");
			try
			{
				if(addOverlayState==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
					{
						Pass( "The Close Link in Edit overlay is displayed.");
					}
					else
					{
						Fail( "The Close Link in Edit overlay is not displayed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1255 Issue ." + e.getMessage());
				Exception(" HBC - 1255 Issue ." + e.getMessage());
			}
			
			/* HBC - 1256 Guest Checkout - Shipping Information Section > Verify that "Ok" button & "Cancel" button should be shown in the "Shipping Address" overlay/page..*/
			ChildCreation(" HBC - 1256 Guest Checkout - Shipping Information Section > Verify that Ok button & Cancel button should be shown in the Shipping Address overlay/page.");
			try
			{
				if(addOverlayState==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
					{
						Pass( "The Cancel Link in Edit overlay is displayed.");
					}
					else
					{
						Fail( "The Cancel Link in Edit overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
					{
						Pass( "The Ok button in Edit overlay is displayed.");
					}
					else
					{
						Fail( "The Ok button in Edit overlay is not displayed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1256 Issue ." + e.getMessage());
				Exception(" HBC - 1256 Issue ." + e.getMessage());
			}
			
			/* HBC - 1253 Guest Checkout - Shipping Information Section > Verify that clicking on "Cancel" button after making changes in the "Shipping Address" overlay/page should be displayed without any changes.*/
			ChildCreation(" HBC - 1253 Guest Checkout - Shipping Information Section > Verify that clicking on Cancel button after making changes in the Shipping Address overlay/page should be displayed without any changes.");
			try
			{
				if(addOverlayState==true)
				{
					String aftAdd = "";
					String fName = HBCBasicfeature.getExcelVal("HBC1253", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name Field in the Edit form is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(fName);
					}
					else
					{
						Fail( "The First Name in the Edit field is not displayed.");
					}
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
					{
						Pass( "The Cancel button in the Edit Overlay is displayed.");
						jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
						Thread.sleep(1000);
						if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
						{
							addOverlayCloseState = true;
							aftAdd = shipandPaypageShipAddressDisp.getText();
							log.add( "The address before modification is  : " + befAdd);
							log.add( "The address after modification is  : " + aftAdd);
							if((befAdd.isEmpty())||(aftAdd.isEmpty()))
							{
								Fail( "The user failed to get the before address and after address they are empty.",log);
							}
							else if(befAdd.equals(aftAdd))
							{
								Pass( " There is no modification done when cancel is clicked in the Address Verification Page.",log);
							}
							else
							{
								Fail( "There is mismatch in the before address and after address.",log);
							}
						}
						else
						{
							Fail("The Saved Address list is not displayed.");
						}
					}
					else
					{
						Fail( "The Cancel button in the Edit Overlay is displayed is not displayed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1253 Issue ." + e.getMessage());
				Exception(" HBC - 1253 Issue ." + e.getMessage());
			}
			
			/* HBC - 1257 Guest Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be closed on tapping the "Cancel" button.*/
			ChildCreation(" HBC - 1257 Guest Checkout - Shipping Information Section > Verify that Shipping Address overlay/page should be closed on tapping the Cancel button.");
			try
			{
				if(addOverlayCloseState==true)
				{
					Pass( "The Address Edit Overlay was closed when cancel button was clicked.");
				}
				else
				{
					Skip ( "The address edit overlay was not enabled in the first place to close.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1257 Issue ." + e.getMessage());
				Exception(" HBC - 1257 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1254 Guest Checkout - Shipping Information Section > Verify that clicking on "Ok" button after making changes in the "Shipping Address" overlay/page should be displayed with the modified changes.*/
	public void HBC1254AddressModifiySave()
	{
		ChildCreation(" HBC - 1254 Guest Checkout - Shipping Information Section > Verify that clicking on Ok button after making changes in the Shipping Address overlay/page should be displayed with the modified changes.");
		String befAdd = "", aftAdd = "";
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
				{
					befAdd = shipandPaypageShipAddressDisp.getText();
				}
					
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					jsclick(shipandPaypageShipEditLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Edit Shipping Address Overlay is dispalyed.");
						String fName = HBCBasicfeature.getExcelVal("HBC1254", sheet, 1);
						String LName = HBCBasicfeature.getExcelVal("HBC1254", sheet, 2);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name Field in the Edit form is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(fName);
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
							{
								Pass( "The Last Name Field in the Edit form is displayed.");
								shipandPaypageShipShipCreateEditAddresssLName.clear();
								shipandPaypageShipShipCreateEditAddresssLName.clear();
								shipandPaypageShipShipCreateEditAddresssLName.sendKeys(LName);
								Thread.sleep(1000);
								if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
								{
									Pass( "The Ok button in the Edit Overlay is displayed.");
									Thread.sleep(1000);
									boolean addOverlay  = false;
									int count = 0;
									do
									{
										WebDriverWait w1 = new WebDriverWait(driver, 5);
										try
										{
											count++;
											shipandPaypageShipShipCreateEditAddressOkBtn.click();
											w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
											addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
											addOverlay = true;
										}
										catch(Exception e)
										{
											addOverlay = false;
										}
									}while((addOverlay==false)&&(count<5));
									if(addOverlay==true)
									{
										wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
										addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
										if(addVerEnable==true)
										{
											Pass( "The Address Verification Overlay is enabled.");
											if(HBCBasicfeature.isElementPresent(addressuseasEntered))
											{
												Pass( "The Use As Entered button is displayed.");
												jsclick(addressuseasEntered);
												Thread.sleep(500);
												wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
												wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
												wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
												Thread.sleep(1000);
												aftAdd = shipandPaypageShipAddressDisp.getText();
												log.add( "The address before modification is  : " + befAdd);
												log.add( "The address after modification is  : " + aftAdd);
												if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddress))
												{
													Pass("The Edit Shipping Address Overlay is dispalyed.");
													if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
													{
														aftAdd = shipandPaypageShipAddressDisp.getText();
														log.add( "The address before modification is  : " + befAdd);
														log.add( "The address after modification is  : " + aftAdd);
														if((befAdd.isEmpty())||(aftAdd.isEmpty()))
														{
															Fail( "The user failed to get the before address and after address they are empty.",log);
														}
														else if(befAdd.equals(aftAdd))
														{
															Fail( " There is no modification done when Ok is clicked in the Address Verification Page.",log);
														}
														else
														{
															log.add( "The Expected Modified First Name was : " + fName);
															log.add( "The Expected Modified First Name was : " + aftAdd);
															if(aftAdd.contains(fName))
															{
																Pass( "The Updated First Name is displayed. ");
															}
															else
															{
																Fail( "There is mismatch in the before address and after address.",log);
															}
															
															log.add( "The Expected Modified Last Name was : " + fName);
															log.add( "The Expected Modified Last Name was : " + aftAdd);
															if(aftAdd.contains(LName))
															{
																Pass( "The Updated Last Name is displayed. ");
															}
															else
															{
																Fail( "There is mismatch in the before address and after address.",log);
															}
														}
														
														if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
														{
															sbefAdd = shipandPaypageShipAddressDisp.getText();
														}
													}
													else
													{
														Fail("The Saved Address list is not displayed.");
													}
												}
												else
												{
													Fail( "The User failed to load the Shippment and Payment Page.");
												}
											}
											else
											{
												Fail( " The Use As Entered Button is not displayed.");
											}
										}
										else
										{
											Fail( " The User failed to load the address Verification page.");
										}
									}
									else
									{
										Fail("The Edit Shipping Address Overlay is not dispalyed.");
									}
								}
								else
								{
									Fail( "The Cancel button in the Edit Overlay is displayed is not displayed.");
								}
							}
							else
							{
								Fail( "The First Name in the Edit field is not displayed.");
							}
						}
						else
						{
							Fail( "The First Name in the Edit field is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1254 Issue ." + e.getMessage());
				Exception(" HBC - 1254 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1258 Guest Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be closed on tapping the "X" icon.*/
	public void HBC1258EditAddressOverlayClose()
	{
		ChildCreation("HBC - 1258 Guest Checkout - Shipping Information Section > Verify that Shipping Address overlay/page should be closed on tapping the X icon.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					jsclick(shipandPaypageShipEditLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Edit Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Edit overlay is displayed.");
							jsclick(shipandPaypageShipShipCreateEditAddressCloseLink);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
							Thread.sleep(1000);
							if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
							{
								Pass( "The Address Edit Overlay is closed.");
							}
							else
							{
								Fail( "The Address Edit Overlay is not closed.");
							}
						}
						else
						{
							Fail( "The Close Link in Edit overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1258 Issue ." + e.getMessage());
				Exception(" HBC - 1258 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1259 Guest Checkout - Shipping Information Section > Verify that "Shipping Address" overlay/page should be as per the creative.*/
	public void HBC1259ShipPageCreateShipOverlay()
	{
		ChildCreation("HBC - 1259 Guest Checkout - Shipping Information Section > Verify that Shipping Address overlay/page should be as per the creative.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					jsclick(shipandPaypageShipCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The First Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Last Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Street Address is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The City Field is not displayed in the Shipping Address Page.");
						}

						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Country is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
						{
							Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Phone Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Close Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
						{
							Pass( "The Cancel Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Cancel Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
						{
							Pass( "The Ok button in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Ok button in Create overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1259 Issue ." + e.getMessage());
				Exception(" HBC - 1259 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1261 Guest Checkout - Verify that "Shipping Address" overlay/page should have the following text boxes : "First Name", "Last Name", "Street Address", and so on.*/
	/* HBC - 1264 Guest Checkout - Verify that required input fields needs to be displayed to add a new shipping address.*/
	public void HBC1261ShipPageCreateShipOverlayFields(String tc)
	{
		String tcId = "";
		if(tc=="HBC1261")
		{
			tcId = " HBC - 1261 Guest Checkout - Verify that Shipping Address overlay/page should have the following text boxes : First Name, Last Name, Street Address, and so on. ";
		}
		else
		{
			tcId = " HBC - 1264 Guest Checkout - Verify that required input fields needs to be displayed to add a new shipping address.";
		}
		ChildCreation(tcId);
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The City Field is not displayed in the Shipping Address Page.");
					}

					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Pass( "The State Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The State Field is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The PO Code Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The PO Code is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
					{
						Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Phone Code is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
					{
						Pass( "The Close Link in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Close Link in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
					{
						Pass( "The Cancel Link in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Cancel Link in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
					{
						Pass( "The Ok button in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Ok button in Create overlay is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1261 Issue ." + e.getMessage());
				Exception(" HBC - 1261 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1262 Guest Checkout -Verify that alert needs to be shown when the user leaves any field without entering the details.*/
	public void HBC1262ShipPageCreateAddressEmptyVal()
	{
		ChildCreation("HBC - 1262 Guest Checkout -Verify that alert needs to be shown when the user leaves any field without entering the details.");
		if(shipPage==true)
		{
			try
			{
				String[] alert = HBCBasicfeature.getExcelVal("HBC1262", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.click();
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						shipandPaypageShipShipCreateEditAddresssLName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBStaddressAlert,"style","block"));
						String stAddAlert = checkoutBStaddressAlert.getText();
						
						if(stAddAlert.equalsIgnoreCase(alert[2].toString()))
						{
							Pass( "The Street Address alert was as expected.",log);
						}
						else
						{
							Fail( "The Street Address alert was not as expected.",log);
						}
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						HBCBasicfeature.scrolldown(staddress, driver);
						shipandPaypageShipShipCreateEditAddresssCty.click();
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						shipandPaypageShipShipCreateEditAddresssStAddress.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBCityAlert,"style","block"));
						String cityAlert = checkoutBCityAlert.getText();
						
						if(cityAlert.equalsIgnoreCase(alert[3].toString()))
						{
							Pass( "The City alert was as expected.",log);
						}
						else
						{
							Fail( "The City alert was not as expected.",log);
						}
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						shipandPaypageShipShipCreateEditAddresssCty.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						String poAlert = checkoutBPOAlert.getText();
						log.add( "The actual alert was : " + poAlert);
						if(poAlert.equalsIgnoreCase(alert[5].toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert1,"style","block"));
						String ctAlert = checkoutSPHAlert1.getText();
						if(ctAlert.equalsIgnoreCase(alert[6].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1262 Issue ." + e.getMessage());
				Exception(" HBC - 1262 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1263 Guest Checkout - Verify that guest user should be able to add the new shipping address details in the "Shipping Address" overlay/page.*/
	/* HBC - 1290 Guest Checkout - Shipping Information Section > Verify that user can be able to add multiple shipping address in the "Shipping & Payment" section.*/
	/* HBC - 1292 Guest Checkout - Shipping Information Section > Verify that recently added address First Name field text should be shown by default in "Ship to" dropdown field.*/
	/* HBC - 1448 Verify that entered address should be updated in the shipping address and it should redirect to the checkout page on tapping the "Use Address as entered" button.*/
	public void HBC1263ShipPageCreateAddressAddShippingAdd()
	{
		ChildCreation(" HBC - 1263 Guest Checkout - Verify that guest user should be able to add the new shipping address details in the Shipping Address overlay/page." );
		boolean HBC1290 = false,HBC1448A = false,HBC1448B = false;
		String aftAdd = "", sfn = "", lsn = "";
		if(shipPage==true)
		{
			try
			{
				sfn = HBCBasicfeature.getExcelVal("HBC1263", sheet, 1);
				lsn = HBCBasicfeature.getExcelVal("HBC1263", sheet, 2);
				String stAdd = HBCBasicfeature.getExcelVal("HBC1263", sheet, 3);
				String cty = HBCBasicfeature.getExcelVal("HBC1263", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1263", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1263", sheet, 6);
				String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 7);
				String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 8);
				String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 9);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						sel.selectByValue("CA");
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Pass( "The State field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
						sel.selectByValue(prv);
						Thread.sleep(1000);
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					boolean addOverlay = ldAddressVerification();
					if(addOverlay==true)
					{
						wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
						addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
						if(addVerEnable==true)
						{
							Pass( "The Address Verification Overlay is enabled.");
							if(HBCBasicfeature.isElementPresent(addressuseasEntered))
							{
								Pass( "The Use As Entered button is displayed.");
								jsclick(addressuseasEntered);
								wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
								wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
								wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
								Thread.sleep(1000);
								aftAdd = shipandPaypageShipAddressDisp.getText();
								log.add( "The address before modification is  : " + befAdd);
								log.add( "The address after modification is  : " + aftAdd);
								if((sbefAdd.isEmpty())||(aftAdd.isEmpty()))
								{
									Fail( "The user failed to get the before address and after address they are empty.",log);
								}
								else if(sbefAdd.equals(aftAdd))
								{
									Fail( " The newly modified address is not dispalyed.",log);
								}
								else
								{
									HBC1290 = true;
									log.add( " There is update made to the address list.");
									log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
									log.add( " There is update made to the address list. The entered first name was : " + sfn);
									
									if(aftAdd.contains(sfn))
									{
										Pass( "The Added First Name is dispalyed.",log);
										HBC1448A = true;
									}
									else
									{
										Fail( "The Added First Name is not dispalyed.",log);
									}
									
									log.add( " There is update made to the address list.");
									log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
									log.add( " There is update made to the address list. The entered first name was : " + sfn);
									
									if(aftAdd.contains(lsn))
									{
										Pass( "The Added Last Name is dispalyed.",log);
										HBC1448B = true;
									}
									else
									{
										Fail( "The Added Last Name is not dispalyed.",log);
									}
								}
								
								if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
								{
									Pass( "The Selected Address Drop Down is displayed.");
									Select sel = new Select(shipandPaypageShipShipToName);
									int size = sel.getOptions().size();
									if(size>1)
									{
										HBC1290 = true;
										for( int i = 0; i<size; i++)
										{
											String addedAdress = sel.getOptions().get(i).getText();
											log.add(" The displayed address list was : " + addedAdress);
										}
										Pass( "The newly added address is added to the drop down.");
									}
									else
									{
										HBC1290 = false;
										Fail( "The newly added address is not added to the drop down.");
									}
								}
								else
								{
									Fail( "The Selected Address Drop Down is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Use As entered is not displayed.");
								if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
								{
									jsclick(addressVerificationEdit);
									wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
									Thread.sleep(1000);
									if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
									{
										Pass( "The Cancel Link in Create overlay is displayed.");
										jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
										Thread.sleep(1000);
										wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
										Thread.sleep(1000);
										if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
										{
											Pass( "The Address Edit Overlay is closed.");
										}
										else
										{
											Fail( "The Address Edit Overlay is not closed.");
										}
									}
									else
									{
										Fail( "The Cancel Link in Create overlay is not displayed.");
									}
								}
								else
								{
									Fail( "The Edit link in the Address Verification overlay is not displayed.");
								}
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail( "The Address Verification Overlay is not enabled.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1263 Issue ." + e.getMessage());
				Exception(" HBC - 1263 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
			
		/* HBC - 1290 Guest Checkout - Shipping Information Section > Verify that user can be able to add multiple shipping address in the "Shipping & Payment" section.*/
		ChildCreation(" HBC - 1290 Guest Checkout - Shipping Information Section > Verify that user can be able to add multiple shipping address in the Shipping & Payment section." );
		if(shipPage==true)
		{
			try
			{
				if(HBC1290==true)
				{
					Pass( "The User was able to create additional address.");
				}
				else
				{
					Fail( "The User was not able to create additional address.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1290 Issue ." + e.getMessage());
				Exception(" HBC - 1290 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
			
		/* HBC - 1448 Verify that entered address should be updated in the shipping address and it should redirect to the checkout page on tapping the "Use Address as entered" button.*/
		ChildCreation(" HBC - 1448 Verify that entered address should be updated in the shipping address and it should redirect to the checkout page on tapping the Use Address as entered button.");
		if(shipPage==true)
		{
			try
			{
				if((HBC1448A==true)&&(HBC1448B==true))
				{
					Pass( "The User was able to create additional address."); 
				}
				else
				{
					Fail( "The User was not able to create additional address.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1448 Issue ." + e.getMessage());
				Exception(" HBC - 1448 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
			
		/* HBC - 1292 Guest Checkout - Shipping Information Section > Verify that recently added address First Name field text should be shown by default in "Ship to" dropdown field.*/
		ChildCreation("HBC - 1292 Guest Checkout - Shipping Information Section > Verify that recently added address First Name field text should be shown by default in Ship to dropdown field.");
		if(shipPage==true)
		{
			try
			{
				if(addVerEnable==true)
				{
					if((befAdd.isEmpty())||(aftAdd.isEmpty()))
					{
						Fail( "The user failed to get the before address and after address they are empty.",log);
					}
					else if(befAdd.equals(aftAdd))
					{
						Fail( " The newly modified address is not dispalyed.",log);
					}
					else
					{
						HBC1290 = true;
						log.add( " There is update made to the address list.");
						log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
						log.add( " There is update made to the address list. The entered first name was : " + sfn);
						
						if(aftAdd.contains(sfn))
						{
							Pass( "The Added First Name is dispalyed.",log);
						}
						else
						{
							Fail( "The Added First Name is not dispalyed.",log);
						}
						
						log.add( " There is update made to the address list.");
						log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
						log.add( " There is update made to the address list. The entered first name was : " + sfn);
						
						if(aftAdd.contains(lsn))
						{
							Pass( "The Added Last Name is dispalyed.",log);
						}
						else
						{
							Fail( "The Added Last Name is not dispalyed.",log);
						}
					}
				}
				else
				{
					Fail( " No Address Verification overlay was enabled to add the address.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1292 Issue ." + e.getMessage());
				Exception(" HBC - 1292 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1431 Verify that "Address Verification" overlay should be as per the creative.*/
	/* HBC - 1432 Verify that Address Verification page should be shown after adding new shipping address or updating the existing address.*/
	/* HBC - 1433 Verify that suggested address should be shown in the address verification page for the entered address.*/
	/* HBC - 1436 Verify that "We found more than...." text should be shown in bold, when more than 1 address suggestion is displayed.*/
	/* HBC - 1443 Verify that user entered address should be displayed with Edit option with the button caption "Use Address as entered" in the address verification page as per the creative.*/
	/* HBC - 1449 Verify that partial entered address should be shown below the "Use Address as entered" button.*/
	/* HBC - 1450 Verify that "*Your address may be undeliverable" text should be shown below the "Use Address as entered" button.*/
	/* HBC - 1451 Verify that edit address page / overlay with prefilled address should be open on tapping the Edit option in address verification page.*/
	public void HBCShipPageAddShippingAddressVerification1()
	{
		ChildCreation(" HBC - 1431 Verify that Address Verification overlay should be as per the creative." );
		String sfn = "", lsn = "", actText = "", suggInfoTxt = "", stAdd = "", cty = "", prv = "", pocode = "";
		boolean HBC1432 = false, HBC1436 = false;
		if(shipPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipCreateAddLink));
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					jsclick(shipandPaypageShipCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						sfn = HBCBasicfeature.getExcelVal("HBC1431AddVer", sheet, 1);
						lsn = HBCBasicfeature.getExcelVal("HBC1431AddVer", sheet, 2);
						stAdd = HBCBasicfeature.getExcelVal("HBC1431AddVer", sheet, 3);
						cty = HBCBasicfeature.getExcelVal("HBC1431AddVer", sheet, 4);
						prv = HBCBasicfeature.getExcelVal("HBC1431AddVer", sheet, 5);
						pocode = HBCBasicfeature.getExcelVal("HBC1431AddVer", sheet, 6);
						String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1431AddVer", sheet, 7);
						String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1431AddVer", sheet, 8);
						String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1431AddVer", sheet, 9);
						String editTxt = HBCBasicfeature.getExcelNumericVal("HBC1431", sheet, 1);
						suggInfoTxt = HBCBasicfeature.getExcelNumericVal("HBC1431", sheet, 2);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address field is displayed.");
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
						}
						else
						{
							Fail( "The Street Address field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City field is displayed.");
							shipandPaypageShipShipCreateEditAddresssCty.clear();
							shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
						}
						else
						{
							Fail( "The City field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							sel.selectByValue("CA");
						}
						else
						{
							Fail( "The Country field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						boolean addOverlay = ldAddressVerification();
						if(addOverlay==true)
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								HBC1432 = true;
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
								{
									Pass( "The Address Verification Overlay is dispalyed.");
									if(HBCBasicfeature.isElementPresent(addressVerificationInfoText))
									{
										actText = addressVerificationInfoText.getText();
										if(actText.isEmpty())
										{
											Fail( "The address verification details text is not displayed and it seems to be empty.");
										}
										else
										{
											Pass( "The address verification details text is displayed.The displayed text is " + actText);
											if(actText.contains(suggInfoTxt))
											{
												HBC1436 = true;
											}
										}
									}
									else
									{
										Fail( "The Address Verification text is not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
									{
										Pass( "The Address Verification Edit link is displayed.");
										String actTxt = addressVerificationAddressEditLink.getText();
										log.add(" The expected text was : " + editTxt);
										log.add(" The actual text is : " + actTxt);
										if(actTxt.equalsIgnoreCase(editTxt))
										{
											Pass( "There is no mismatch in the expected and the actual text.",log);
										}
										else
										{
											Fail( "There is some mismatch in the expected and the actual text.",log);
										}
									}
									else
									{
										Fail( "The Address Verification Edit link is not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressuseasEntered))
									{
										Pass( "The Use as Entered Button is displayed.");
									}
									else
									{
										Fail( "The Use as Entered Button is not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressSuggestionList))
									{
										boolean addrSugg = addressSuggestionList.getAttribute("style").contains("block");
										if(addrSugg==true)
										{
											Pass( "The Address Suggestion List is enabled and the address is displayed.");
										}
										else
										{
											Skip(" The Address Suggestion List is not displayed.");
										}
									}
									else
									{
										Fail( "The Address Suggestion List is not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressSuggestionDeliveryWarning))
									{
										Pass( "The Address Suggestion delivery warning is displayed.");
									}
									else
									{
										Fail( "The Address Suggestion delivery warning is  not displayed.");
									}
									
									if(HBCBasicfeature.isElementPresent(addressSuggestionClose))
									{
										Pass( "The Address Suggestion Close icon is displayed.");
									}
									else
									{
										Fail( "The Address Suggestion Close icon is not displayed.");
									}
								}
								else
								{
									Fail( "The Address Verification overlay is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Create Shipping Address Link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1431 Issue ." + e.getMessage());
				Exception(" HBC - 1431 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1436 Verify that "We found more than...." text should be shown in bold, when more than 1 address suggestion is displayed.*/
		ChildCreation(" HBC - 1436 Verify that We found more than.... text should be shown in bold, when more than 1 address suggestion is displayed.");
		if(shipPage==true)
		{
			try
			{
				log.add( "The displayed address suggestion information is : " + actText);
				log.add( "The expected address suggestion information is : " + suggInfoTxt);
				if(HBC1436==true)
				{
					Pass( "The Address Verification page is displayed on entering the new Address and more than one address found info is displayed.",log);
				}
				else
				{
					Fail( " No Address Verification page is displayed on entering the new Address.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1436 Issue ." + e.getMessage());
				Exception(" HBC - 1436 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1432 Verify that Address Verification page should be shown after adding new shipping address or updating the existing address.*/
		ChildCreation("HBC - 1432 Verify that Address Verification page should be shown after adding new shipping address or updating the existing address.");
		if(shipPage==true)
		{
			try
			{
				if(HBC1432==true)
				{
					Pass( "The Address Verification page is displayed on entering the new Address.");
				}
				else
				{
					Fail( " No Address Verification page is displayed on entering the new Address.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1432 Issue ." + e.getMessage());
				Exception(" HBC - 1432 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1450 Verify that "*Your address may be undeliverable" text should be shown below the "Use Address as entered" button.*/
		ChildCreation("HBC - 1450 Verify that *Your address may be undeliverable text should be shown below the Use Address as entered button.");
		if(shipPage==true)
		{
			try
			{
				String warnText = HBCBasicfeature.getExcelVal("HBC1450", sheet, 2);
				if(HBCBasicfeature.isElementPresent(addressSuggestionDeliveryWarning))
				{
					Pass( "The Address Suggestion delivery warning is displayed.");
					String actTxt = addressSuggestionDeliveryWarning.getText();
					log.add( "The displayed warning text was : " + actTxt);
					log.add( "The Expected text is  : " + warnText);
					if(warnText.equalsIgnoreCase(actTxt))
					{
						Pass( "The expected and the actual text matches.",log);
					}
					else
					{
						Fail( "The expected and the actual text does not matches.",log);
					}
				}
				else
				{
					Fail( "The Address Suggestion delivery warning is  not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1450 Issue ." + e.getMessage());
				Exception(" HBC - 1450 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1446 Verify that "Use Suggested Address" button should not be shown, when more than 1 address suggestion is displayed.*/
		ChildCreation("HBC - 1446 Verify that Use Suggested Address button should not be shown, when more than 1 address suggestion is displayed.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressSuggestionList))
				{
					boolean addrSugg = addressSuggestionList.getAttribute("style").contains("block");
					if(addrSugg==true)
					{
						Pass( "The Address Suggestion List is enabled and the address is displayed.");
						if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
						{
							Fail( "The Use Suggested Address button is displayed when more than one address suggestion is displayed.");
						}
						else
						{
							Pass( "The Use Suggested Address button is not displayed when more than one address suggestion is displayed.");
						}
					}
					else
					{
						Skip(" The Address Suggestion List is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Suggestion List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1446 Issue ." + e.getMessage());
				Exception(" HBC - 1446 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1449 Verify that partial entered address should be shown below the "Use Address as entered" button.*/
		ChildCreation(" HBC - 1449 Verify that partial entered address should be shown below the Use Address as entered button.");
		boolean HBC1443 = false;
		if(shipPage==true)
		{
			boolean addFound = false;
			try
			{
				if(HBCBasicfeature.isListElementPresent(userEnteredAddress))
				{
					for(int k = 0; k<userEnteredAddress.size(); k++)
					{
						addFound = false;
						String addDet = userEnteredAddress.get(k).getText();
						if(addDet.contains(stAdd))
						{
							addFound = true;
							HBC1443 = true;
							break;
						}
					}
					
					if(addFound==true)
					{
						Pass( "The User Entered Address is displayed.");
					}
					else
					{
						Fail( "The User Entered Address is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Suggestion List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1449 Issue ." + e.getMessage());
				Exception(" HBC - 1449 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1443 Verify that user entered address should be displayed with Edit option with the button caption "Use Address as entered" in the address verification page as per the creative.*/
		ChildCreation(" HBC - 1443 Verify that user entered address should be displayed with Edit option with the button caption Use Address as entered in the address verification page as per the creative.");
		if(shipPage==true)
		{
			try
			{
				if(HBC1443==true)
				{
					Pass( "The User Entered Address is displayed in the Address Verification Page. ");
				}
				else
				{
					Fail( "The User Entered Address is not displayed in the Address Verification Page. ");
				}
				
				if(HBCBasicfeature.isElementPresent(addressuseasEntered))
				{
					Pass( "The Use as Entered Button is displayed.");
				}
				else
				{
					Fail( "The Use as Entered Button is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
				{
					Pass( "The Address Verification Edit link is displayed.");
				}
				else
				{
					Fail( "The Address Verification Edit link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1443 Issue ." + e.getMessage());
				Exception(" HBC - 1443 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
			
		/* HBC - 1433 Verify that suggested address should be shown in the address verification page for the entered address.*/
		ChildCreation("HBC - 1433 Verify that suggested address should be shown in the address verification page for the entered address.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressSuggestionList))
				{
					boolean addrSugg = addressSuggestionList.getAttribute("style").contains("block");
					if(addrSugg==true)
					{
						Pass( "The Address Suggestion List is enabled and the address is displayed.");
						if(HBCBasicfeature.isListElementPresent(addressSuggestionAddressList))
						{
							for(int i = 0; i<addressSuggestionAddressList.size(); i++)
							{
								String addrSuggested = addressSuggestionAddressList.get(i).getText();
								if(addrSuggested.isEmpty())
								{
									Fail( "The Suggested address is empty.");
								}
								else
								{
									Pass( " The Address Suggested is displayed. The displayed address is  : " + addrSuggested);
								}
							}
						}
						else
						{
							Fail( "The Address Suggestion is not displayed.");
						}
					}
					else
					{
						Skip(" The Address Suggestion List is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Suggestion List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1433 Issue ." + e.getMessage());
				Exception(" HBC - 1433 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1451 Verify that edit address page / overlay with prefilled address should be open on tapping the Edit option in address verification page.*/
		ChildCreation("HBC - 1451 Verify that edit address page / overlay with prefilled address should be open on tapping the Edit option in address verification page.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
				{
					Pass( "The Address Verification Edit link is displayed.");
					jsclick(addressVerificationAddressEditLink);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The First Name field is empty.");
						}
						else
						{
							Pass( "The First Name field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Last Name field is empty.");
						}
						else
						{
							Pass( "The Last Name field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Street Address field is empty.");
						}
						else
						{
							Pass( "The Street Address field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssCty.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The City field is empty.");
						}
						else
						{
							Pass( "The City field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						if(val.isEmpty())
						{
							Fail( "The Country field is empty.");
						}
						else
						{
							Pass( "The Country field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Pass( "The State field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
						String val = sel.getFirstSelectedOption().getText();
						if(val.isEmpty())
						{
							Fail( "The State field is empty.");
						}
						else
						{
							Pass( "The State field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The PO CODE field is empty.");
						}
						else
						{
							Pass( "The PO CODE field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Phone Number field 1 is empty.");
						}
						else
						{
							Pass( "The Phone Number field 1 field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Phone Number field 2 is empty.");
						}
						else
						{
							Pass( "The Phone Number field 2 field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						String val = shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value");
						if(val.isEmpty())
						{
							Fail( "The Phone Number field 3 is empty.");
						}
						else
						{
							Pass( "The Phone Number field 3 field is not empty when the Edit button in the Address Verification Page is clicked.The displayed value is  : " + val);
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Edit link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1451 Issue ." + e.getMessage());
				Exception(" HBC - 1451 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1453 Verify that updated address should be displayed in address verification page while selecting the "Ok" / "Continue" button in the edit shipping address page/overlay.*/
	/* HBC - 1434 Verify that "Use Address as entered" button alone should be displayed with the Edit option, when no address suggestion is shown.*/
	/* HBC - 1435 Verify that "Sorry we could not find...." text should be shown in bold, when no address suggestion is displayed.*/
	public void HBCShipPageAddShippingAddressNoMatchAddressVerification2()
	{
		ChildCreation(" HBC - 1453 Verify that updated address should be displayed in address verification page while selecting the Ok / Continue button in the edit shipping address page/overlay.");
		String stAdd = "";
		if(shipPage==true)
		{
			try
			{
				stAdd = HBCBasicfeature.getExcelVal("HBC1453", sheet, 3);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
				{
					Pass( "The Street Address field is displayed.");
					shipandPaypageShipShipCreateEditAddresssStAddress.clear();
					shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
				}
				else
				{
					Fail( "The Street Address field is not displayed.");
				}
				
				boolean addOverlay = ldAddressVerification();
				if(addOverlay==true)
				{
					wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
					addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
					if(addVerEnable==true)
					{
						Pass( "The Address Verification Overlay is enabled.");
						if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
						{
							Pass( "The Address Verification Overlay is dispalyed.");
							if(HBCBasicfeature.isListElementPresent(userEnteredAddress))
							{
								boolean addFound = false;
								for(int k = 0; k<userEnteredAddress.size(); k++)
								{
									addFound = false;
									String addDet = userEnteredAddress.get(k).getText();
									if(addDet.contains(stAdd))
									{
										addFound = true;
										break;
									}
								}
								
								if(addFound==true)
								{
									Pass( "The User Entered Address is displayed.");
								}
								else
								{
									Fail( "The User Entered Address is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Suggestion List is not displayed.");
							}
						}
						else
						{
							Fail( "The Address Verification overlay is not displayed.");
						}
					}
					else
					{
						Fail( "The Address Verification Overlay is not enabled.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1453 Issue ." + e.getMessage());
				Exception(" HBC - 1453 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1434 Verify that "Use Address as entered" button alone should be displayed with the Edit option, when no address suggestion is shown.*/
		ChildCreation(" HBC - 1434 Verify that Use Address as entered button alone should be displayed with the Edit option, when no address suggestion is shown.");
		if(shipPage==true)
		{
			try
			{
				String editTxt = HBCBasicfeature.getExcelNumericVal("HBC1431", sheet, 1);
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
						{
							Pass( "The Address Verification Edit link is displayed.");
							String actTxt = addressVerificationAddressEditLink.getText();
							log.add(" The expected text was : " + editTxt);
							log.add(" The actual text is : " + actTxt);
							if(actTxt.equalsIgnoreCase(editTxt))
							{
								Pass( "There is no mismatch in the expected and the actual text.",log);
							}
							else
							{
								Fail( "There is some mismatch in the expected and the actual text.",log);
							}
						}
						else
						{
							Fail( "The Address Verification Edit link is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(addressuseasEntered))
						{
							Pass( "The Use as Entered Button is displayed.");
						}
						else
						{
							Fail( "The Use as Entered Button is not displayed.");
						}
						
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1434 Issue ." + e.getMessage());
				Exception(" HBC - 1434 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1435 Verify that "Sorry we could not find...." text should be shown in bold, when no address suggestion is displayed.*/
		ChildCreation(" HBC - 1435 Verify that Sorry we could not find.... text should be shown in bold, when no address suggestion is displayed.");
		if(shipPage==true)
		{
			try
			{
				String actText = "";
				String suggInfoTxt = HBCBasicfeature.getExcelNumericVal("HBC1435", sheet, 1);
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(addressVerificationInfoText))
						{
							actText = addressVerificationInfoText.getText();
							if(actText.isEmpty())
							{
								Fail( "The address verification details text is not displayed and it seems to be empty.");
							}
							else if(actText.contains(suggInfoTxt))
							{
								Pass(" The expected and the actual text matches.",log);
							}
							else
							{
								Fail(" The expected and the actual text does not matches.",log);
							}
						}
						else
						{
							Fail( "The Address Verification text is not displayed.");
						}
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1435 Issue ." + e.getMessage());
				Exception(" HBC - 1435 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1437 Verify that "Sorry, we don't recognize...." text should be shown in bold, when the house or building number is invalid or not able to recognize.*/
	/* HBC - 1438 Verify that "Confirm your House/Building number" textbox should be shown, when the house or building number is invalid or not able to recognize.*/
	/* HBC - 1439 Verify that "Confirm Number" button should be shown near the "Confirm your House/Building number" textbox.*/
	/* HBC - 1440 Verify that user should navigates to checkout pages on tapping the "Confirm Number" button after entering the number in Confirm Number field.*/
	public void HBCShipPageAddShippingAddressBuildingNumberVerification3()
	{
		ChildCreation(" HBC - 1437 Verify that Sorry, we don't recognize.... text should be shown in bold, when the house or building number is invalid or not able to recognize." );
		String sfn = "", lsn = "", actText = "", suggInfoTxt = "", stAdd = "", cty = "", prv = "", pocode = "", expColor = "";
		if(shipPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipCreateAddLink));
				
				if(HBCBasicfeature.isElementPresent(addressVerificationAddressEditLink))
				{
					Pass( "The Address Suggestion Close icon is displayed.");
					jsclick(addressVerificationAddressEditLink);
					Thread.sleep(2000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					//Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						sfn = HBCBasicfeature.getExcelVal("HBC1437AddrVer", sheet, 2);
						lsn = HBCBasicfeature.getExcelVal("HBC1437AddrVer", sheet, 1);
						stAdd = HBCBasicfeature.getExcelVal("HBC1437AddrVer", sheet, 3);
						cty = HBCBasicfeature.getExcelVal("HBC1437AddrVer", sheet, 4);
						prv = HBCBasicfeature.getExcelVal("HBC1437AddrVer", sheet, 5);
						pocode = HBCBasicfeature.getExcelVal("HBC1437AddrVer", sheet, 6);
						suggInfoTxt = HBCBasicfeature.getExcelVal("HBC1437", sheet, 1);
						expColor = HBCBasicfeature.getExcelVal("HBC1437", sheet, 2);
						String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1437AddrVer", sheet, 7);
						String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1437AddrVer", sheet, 8);
						String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1437AddrVer", sheet, 9);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address field is displayed.");
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
						}
						else
						{
							Fail( "The Street Address field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City field is displayed.");
							shipandPaypageShipShipCreateEditAddresssCty.clear();
							shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
						}
						else
						{
							Fail( "The City field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							sel.selectByValue("CA");
						}
						else
						{
							Fail( "The Country field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						boolean addOverlay = ldAddressVerification();
						if(addOverlay==true)
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
								{
									Pass( "The Address Verification Overlay is dispalyed.");
									if(HBCBasicfeature.isElementPresent(addressVerificationInfoText))
									{
										actText = addressVerificationInfoText.getText();
										if(actText.isEmpty())
										{
											Fail( "The address verification details text is not displayed and it seems to be empty.");
										}
										else
										{
											Pass( "The address verification details text is displayed.The displayed text is " + actText);
											log.add( "The Expected value was : " + suggInfoTxt);
											log.add( "The actual value is : " + actText);
											if(actText.contains(suggInfoTxt))
											{
												Pass( "The Expected and the actual value matches.",log);
											}
											else
											{
												Fail( "The Expected and the actual value does not matches.",log);
											}
										}
									}
									else
									{
										Fail( "The Address Verification text is not displayed.");
									}
									
									String txtCol = addressVerificationInfoText.getCssValue("color");
									Color col = Color.fromString(txtCol);
									String actCol = col.asHex();
									log.add( "The expected color was : " + expColor);
									log.add( "The actual color is : " + actCol);
									if(expColor.equalsIgnoreCase(actCol))
									{
										Pass( "The Expected and the Actual color matches.",log);
									}
									else
									{
										Fail( "The Expected and the Actual color does not matches.",log);
									}
								}
								else
								{
									Fail( "The Address Verification overlay is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Address Suggestion Edit Link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1437 Issue ." + e.getMessage());
				Exception(" HBC - 1437 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1438 Verify that "Confirm your House/Building number" textbox should be shown, when the house or building number is invalid or not able to recognize.*/
		ChildCreation("HBC - 1438 Verify that Confirm your House/Building number textbox should be shown, when the house or building number is invalid or not able to recognize.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressVerificationbuildingTextbox))
				{
					Pass( "The Building Number Text Box is diplayed.");
				}
				else
				{
					Fail( "The Building Number Text Box is not diplayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1438 Issue ." + e.getMessage());
				Exception(" HBC - 1438 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1439 Verify that "Confirm Number" button should be shown near the "Confirm your House/Building number" textbox.*/
		ChildCreation(" HBC - 1439 Verify that Confirm Number button should be shown near the Confirm your House/Building number textbox.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
				{
					Pass( "The Confirm Building Number button is diplayed.");
				}
				else
				{
					Fail( "The Confirm Building Number button is not diplayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1439 Issue ." + e.getMessage());
				Exception(" HBC - 1439 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1440 Verify that user should navigates to checkout pages on tapping the "Confirm Number" button after entering the number in Confirm Number field.*/
		ChildCreation(" HBC - 1440 Verify that user should navigates to checkout pages on tapping the Confirm Number button after entering the number in Confirm Number field.");
		if(shipPage==true)
		{
			try
			{
				String buildNo = HBCBasicfeature.getExcelNumericVal("HBC1440", sheet, 1);
				if(HBCBasicfeature.isElementPresent(addressVerificationbuildingTextbox))
				{
					Pass( "The Building Number Text Box is diplayed.");
					addressVerificationbuildingTextbox.clear();
					addressVerificationbuildingTextbox.sendKeys(buildNo);
				}
				else
				{
					Fail( "The Building Number Text Box is not diplayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
				{
					Pass( "The Confirm Building Number button is diplayed.");
					jsclick(addressSuggestedBtn);
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
					wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
					wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
					Thread.sleep(1000);
					boolean shipPge = shipandPaypage.getAttribute("style").contains("block");
					if(shipPge==true)
					{
						Pass( "The User is navigated to the Ship Page.");
					}
					else
					{
						Fail( "The User is not navigated to the Ship Page.");
					}
				}
				else
				{
					Fail( "The Confirm Building Number button is not diplayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1440 Issue ." + e.getMessage());
				Exception(" HBC - 1440 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1444 Verify that "Use Suggested Address" button should be displayed above the suggested address.*/
	/* HBC - 1445 Verify that suggested address should be updated in the shipping address on tapping the "Use Suggested Address" button.*/
	public void HBCShipPageAddShippingSuggestedAddressVerification4()
	{
		ChildCreation(" HBC - 1444 Verify that Use Suggested Address button should be displayed above the suggested address." );
		String sfn = "", lsn = "",stAdd = "", cty = "", prv = "", pocode = "";
		if(shipPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipCreateAddLink));
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					jsclick(shipandPaypageShipCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						sfn = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 2);
						lsn = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 1);
						stAdd = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 3);
						cty = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 4);
						prv = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 5);
						pocode = HBCBasicfeature.getExcelVal("HBC1444AddrVer", sheet, 6);
						String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1444AddrVer", sheet, 7);
						String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1444AddrVer", sheet, 8);
						String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1444AddrVer", sheet, 9);
						String expVal = HBCBasicfeature.getExcelVal("HBC1444", sheet, 1);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address field is displayed.");
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
						}
						else
						{
							Fail( "The Street Address field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City field is displayed.");
							shipandPaypageShipShipCreateEditAddresssCty.clear();
							shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
						}
						else
						{
							Fail( "The City field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							sel.selectByValue("CA");
						}
						else
						{
							Fail( "The Country field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						boolean addOverlay = ldAddressVerification();
						if(addOverlay==true)
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
								{
									Pass( "The Address Verification Overlay is dispalyed.");
									if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
									{
										Pass( "The Use Suggested Address button is diplayed.");
										String actVal = addressSuggestedBtn.getAttribute("value");
										log.add( "The Expected value was : " + expVal);
										log.add( "The actual value is : " + actVal);
										if(expVal.equalsIgnoreCase(actVal))
										{
											Pass( "The Expected and the actual value matches.",log);
										}
										else
										{
											Fail( "The Expected and the actual value does not matches.",log);
										}
									}
									else
									{
										Fail( "The Confirm Building Number button is not diplayed.");
									}
								}
								else
								{
									Fail( "The Address Verification overlay is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Create Shipping Address Link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1444 Issue ." + e.getMessage());
				Exception(" HBC - 1444 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1445 Verify that suggested address should be updated in the shipping address on tapping the "Use Suggested Address" button.*/
		ChildCreation(" HBC - 1445 Verify that suggested address should be updated in the shipping address on tapping the Use Suggested Address button." );
		if(shipPage==true)
		{
			String currAdd = "";
			try
			{
				if(HBCBasicfeature.isListElementPresent(addressVerificationSuggestedAddress))
				{
					ArrayList<String> suggadd = new ArrayList<>();
					for(int i = 0; i<addressVerificationSuggestedAddress.size(); i++)
					{
						suggadd.add(addressVerificationSuggestedAddress.get(i).getText());
					}
					
					log.add( "The Suggested address is  : " + suggadd);
					if(HBCBasicfeature.isElementPresent(addressSuggestedBtn))
					{
						Pass( "The Use Suggested Address button is diplayed.");
						jsclick(addressSuggestedBtn);
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
						wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
						Thread.sleep(1000);
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
						{
							currAdd = shipandPaypageShipAddressDisp.getText();
							log.add( "The Current Updated address is  : " + currAdd);
						}
						boolean shipPge = shipandPaypage.getAttribute("style").contains("block");
						if(shipPge==true)
						{
							Pass( "The User is navigated to the Ship Page.");
							boolean addFound = false;
							for(int k = 0; k<suggadd.size(); k++)
							{
								String suggA = suggadd.get(k).toString();
								if(currAdd.contains(suggA))
								{
									addFound = true;
									break;
								}
								else
								{
									continue;
								}
							}
							
							if(addFound==true)
							{
								Pass( "The Suggested Address is updated and it is displayed in the Checkout page.",log);
							}
							else
							{
								Fail( "The Suggested Address is not updated and it is not displayed in the Checkout page.",log);
							}
						}
						else
						{
							Fail( "The User is not navigated to the Ship Page.");
						}
					}
					else
					{
						Fail( "The Confirm Building Number button is not diplayed.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1445 Issue ." + e.getMessage());
				Exception(" HBC - 1445 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1441 Verify that "Show all Potential Matches" text link should be shown below the "Confirm your House/Building number" textbox.*/
	/* HBC - 1442 Verify that all the matched suggested address should be shown on tapping "Show all Potential Matches" text link.*/
	/* HBC - 1452 Verify that user should be able to choose from suggested address in the address verification page.*/
	/* HBC - 1447 Verify that shipping address details should be updated and it should redirects to checkout page on tapping on any suggested address.*/
	public void HBCShipPageAddShippingPotentialAddressVerification5()
	{
		ChildCreation(" HBC - 1441 Verify that Show all Potential Matches text link should be shown below the Confirm your House/Building number textbox." );
		String sfn = "", lsn = "",stAdd = "", cty = "", prv = "", pocode = "";
		int sele = 0;
		if(shipPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipCreateAddLink));
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					Thread.sleep(1000);
					jsclick(shipandPaypageShipCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						sfn = HBCBasicfeature.getExcelVal("HBC1441AddrVer", sheet, 2);
						lsn = HBCBasicfeature.getExcelVal("HBC1441AddrVer", sheet, 1);
						stAdd = HBCBasicfeature.getExcelVal("HBC1441AddrVer", sheet, 3);
						cty = HBCBasicfeature.getExcelVal("HBC1441AddrVer", sheet, 4);
						prv = HBCBasicfeature.getExcelVal("HBC1441AddrVer", sheet, 5);
						pocode = HBCBasicfeature.getExcelVal("HBC1441AddrVer", sheet, 6);
						String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1441AddrVer", sheet, 7);
						String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1441AddrVer", sheet, 8);
						String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1441AddrVer", sheet, 9);
						String expVal = HBCBasicfeature.getExcelVal("HBC1441", sheet, 1);
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address field is displayed.");
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
						}
						else
						{
							Fail( "The Street Address field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City field is displayed.");
							shipandPaypageShipShipCreateEditAddresssCty.clear();
							shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
						}
						else
						{
							Fail( "The City field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							sel.selectByValue("CA");
						}
						else
						{
							Fail( "The Country field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
						{
							Pass( "The State field is displayed.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
						{
							Pass( "The Contact Number field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
						}
						else
						{
							Fail( "The Contact Number is not displayed.");
						}
						boolean addOverlay = ldAddressVerification();
						if(addOverlay==true)
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
								{
									Pass( "The Address Verification Overlay is dispalyed.");
									if(HBCBasicfeature.isElementPresent(addressVerificationPotentialMatchesText))
									{
										Pass( "The Potential Matches Link is displayed.");
										String actVal = addressVerificationPotentialMatchesText.getText();
										log.add( "The Expected value was : " + expVal);
										log.add( "The actual value is : " + actVal);
										if(expVal.equalsIgnoreCase(actVal))
										{
											Pass( "The Expected and the actual value matches.",log);
										}
										else
										{
											Fail( "The Expected and the actual value does not matches.",log);
										}
									}
									else
									{
										Fail( "The Confirm Building Number button is not diplayed.");
									}
								}
								else
								{
									Fail( "The Address Verification overlay is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail("The Create Shipping Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Create Shipping Address Link is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1441 Issue ." + e.getMessage());
				Exception(" HBC - 1441 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1442 Verify that all the matched suggested address should be shown on tapping "Show all Potential Matches" text link.*/
		ChildCreation(" HBC - 1442 Verify that all the matched suggested address should be shown on tapping Show all Potential Matches text link." );
		if(shipPage==true)
		{
			try
			{
				
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(addressVerificationPotentialMatchesText))
						{
							Pass( "The Potential Matches Link is displayed.");
							jsclick(addressVerificationPotentialMatchesText);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.attributeContains(addressVerificationPotentialMatchesAddress, "style", "block"));
							if(HBCBasicfeature.isListElementPresent(addressVerificationPotentialMatchesAddressList))
							{
								Pass( "The Address List is displayed.");
							}
							else
							{
								Fail( "The Address List is not displayed.");
							}
						}
						else
						{
							Fail( "The Confirm Building Number button is not diplayed.");
						}
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1442 Issue ." + e.getMessage());
				Exception(" HBC - 1442 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1452 Verify that user should be able to choose from suggested address in the address verification page.*/
		ChildCreation(" HBC - 1452 Verify that user should be able to choose from suggested address in the address verification page." );
		if(shipPage==true)
		{
			try
			{
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isListElementPresent(addressVerificationPotentialMatchesAddressList))
						{
							Pass( "The Address List is displayed.");
							Random r = new Random();
							sele = r.nextInt(addressVerificationPotentialMatchesAddressList.size());
							HBCBasicfeature.scrolldown(addressVerificationPotentialMatchesAddressList.get(sele), driver);
							Thread.sleep(1000);
							String addTxt = driver.findElement(By.xpath("(//*[@class='QAS_Pick']//tr)["+sele+"]")).getText();
							Pass( "The user was able to select the address from the list. The Selected address was : " + addTxt);
						}
						else
						{
							Fail( "The Address List is not displayed.");
						}
						
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1452 Issue ." + e.getMessage());
				Exception(" HBC - 1452 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
		
		/* HBC - 1447 Verify that shipping address details should be updated and it should redirects to checkout page on tapping on any suggested address.*/
		ChildCreation(" HBC - 1447 Verify that shipping address details should be updated and it should redirects to checkout page on tapping on any suggested address." );
		if(shipPage==true)
		{
			try
			{
				addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
				if(addVerEnable==true)
				{
					Pass( "The Address Verification Overlay is enabled.");
					if(HBCBasicfeature.isElementPresent(addressVerificationoverlay))
					{
						Pass( "The Address Verification Overlay is dispalyed.");
						if(HBCBasicfeature.isListElementPresent(addressVerificationPotentialMatchesAddressList))
						{
							Pass( "The Address List is displayed.");
							Random r = new Random();
							sele = r.nextInt(addressVerificationPotentialMatchesAddressList.size());
							HBCBasicfeature.scrolldown(addressVerificationPotentialMatchesAddressList.get(0), driver);
							Thread.sleep(1000);
							WebElement addTxt = driver.findElement(By.xpath("(//*[@class='QAS_Pick']//tr//td)[1]"));
							//String selAdd = addTxt.getText();
							Actions act = new Actions(driver);
							act.moveToElement(addTxt).click().build().perform();
							//jsclick(addTxt);
							Thread.sleep(2000);
							//wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
							wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
							wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
							Thread.sleep(1000);
							boolean shipPge = shipandPaypage.getAttribute("style").contains("block");
							if(shipPge==true)
							{
								Pass( "The User is navigated to the Ship Page.");
							}
							else
							{
								Fail( "The User is not navigated to the Ship Page.");
							}
						}
						else
						{
							Fail( "The Address List is not displayed.");
						}
						
					}
					else
					{
						Fail( "The Address Verification overlay is not displayed.");
					}
				}
				else
				{
					Fail( "The Address Verification Overlay is not enabled.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1447 Issue ." + e.getMessage());
				Exception(" HBC - 1447 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1266 Guest Checkout - Verify that user should be able to enter first name in the "First Name" field and it should accept maximum of 64 characters.*/
	public void HBC1266ShipPageCreateShipOverlayFNLenVal()
	{
		ChildCreation("HBC - 1266 Guest Checkout - Verify that user should be able to enter first name in the First Name field and it should accept maximum of 64 characters.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1177", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").isEmpty())
							{
								Fail( "The First Name is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").length()<=64)
							{
								Pass( "The first name was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The first name was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssFName.clear();
						}
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1266 Issue ." + e.getMessage());
				Exception(" HBC - 1266 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1267 Guest Checkout - Verify that user should be able to enter last name in the "Last Name" field and it should accept maximum of 64 characters.*/
	public void HBC1267ShipPageCreateShipOverlayLNLenVal()
	{
		ChildCreation(" HBC - 1267 Guest Checkout - Verify that user should be able to enter last name in the Last Name field and it should accept maximum of 64 characters.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1177", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").isEmpty())
							{
								Fail( "The Last Name is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").length()<=64)
							{
								Pass( "The Last Name  was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Last Name  was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssLName.clear();
						}
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1267 Issue ." + e.getMessage());
				Exception(" HBC - 1267 Issue ." + e.getMessage());
			}
		}
		else
		{
			 Skip( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1268 Guest Checkout - Verify that only characters should be accepted in "First Name" and "Last Name" field.*/
	public void HBC1268FNLNameNumValidation()
	{
		ChildCreation(" HBC - 1268 Guest Checkout - Verify that only characters should be accepted in First Name and Last Name field..");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1179", sheet, 1).split("\n");
				String stAdd = HBCBasicfeature.getExcelVal("HBC1263", sheet, 3);
				String cty = HBCBasicfeature.getExcelVal("HBC1263", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1263", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1263", sheet, 6);
				String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 7);
				String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 8);
				String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 9);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[0]);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Value from the excel file was : " + cellVal[1].toString());
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[1]);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Value from the excel file was : " + stAdd);
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.click();
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						log.add( "The Value from the excel file was : " + cty);
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						sel.selectByValue("CA");
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Pass( "The State field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
						sel.selectByValue(prv);
						Thread.sleep(1000);
					}
					else
					{
						Fail( "The State field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Value from the excel file was : " + pocode);
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						log.add( "The Value from the excel file was : " + cNum1);
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						log.add( "The Value from the excel file was : " + cNum2);
						shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.click();
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						log.add( "The Value from the excel file was : " + cNum3);
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					//WebDriverWait w1 = new WebDriverWait(driver, 10);
					boolean addOverlay = ldNoAddressVerification();
					/*do
					{
						try
						{
							shipandPaypageShipShipCreateEditAddressOkBtn.click();
							w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							addOverlay = true;
							if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
							{
								jsclick(addressVerificationEdit);
								wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
								Thread.sleep(1000);
							}
						}
						catch(Exception e)
						{
							addOverlay = false;
						}
					}while(addOverlay==true);
					*/
					
					if(addOverlay==true)
					{
						Pass( "The User is not navigated to the Address Verification page.");
					}
					else
					{
						Fail( "The User is navigated to the Address Verification page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1268 Issue ." + e.getMessage());
				Exception(" HBC - 1268 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1270 Guest Checkout - Verify that "Street Address" field should have two text boxes.*/
	public void HBC1270ShipPageCreateShipStreetAddressFields()
	{
		ChildCreation("HBC - 1270 Guest Checkout - Verify that Street Address field should have two text boxes.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress2))
					{
						Pass( "The 2 Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The 2 Street Address is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1270 Issue ." + e.getMessage());
				Exception(" HBC - 1270 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1271 Guest Checkout - Verify that user should be able to enter street address in the "Street Address" field and it should accept maximum of 70 characters in the 1st text box and 50 characters in the 2nd text box.*/
	public void HBC1271StAddFieldsLenVal()
	{
		ChildCreation(" HBC - 1271 Guest Checkout - Verify that user should be able to enter street address in the Street Address field and it should accept maximum of 70 characters in the 1st text box and 50 characters in the 2nd text box.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String[] st1 = HBCBasicfeature.getExcelVal("HBC1184", sheet, 1).split("\n");
					String[] st2 = HBCBasicfeature.getExcelVal("HBC1184", sheet, 2).split("\n");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						for(int i = 0; i<st1.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
							log.add( "The Entered value from the excel file is : " + st1[i].toString());
							log.add( "The Value from the excel file was : " + st1[i].toString() + " and is length is : " + st1[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(st1[i]);
							if(shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value").isEmpty())
							{
								Fail( "The Street Address is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value").length()<=70)
							{
								Pass( "The Street Address was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Street Address was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						}
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress2))
					{
						Pass( "The Street Address 2 field is displayed.");
						for(int i = 0; i<st2.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssStAddress2.clear();
							log.add( "The Entered value from the excel file is : " + st2[i].toString());
							log.add( "The Value from the excel file was : " + st2[i].toString() + " and is length is : " + st2[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssStAddress2.sendKeys(st2[i]);
							if(shipandPaypageShipShipCreateEditAddresssStAddress2.getAttribute("value").isEmpty())
							{
								Fail( "The Street Address 2 is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssStAddress2.getAttribute("value").length()<=50)
							{
								Pass( "The Street Address 2 was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Street Address 2 was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssStAddress2.clear();
						}
					}
					else
					{
						Fail( "The Street Address 2 field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1271 Issue ." + e.getMessage());
				Exception(" HBC - 1271 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1272 Guest Checkout - Verify that user should be able to enter characters in the "City" field.*/
	public void HBC1272CityEnterable()
	{
		ChildCreation(" HBC - 1272 Guest Checkout - Verify that user should be able to enter characters in the City field.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String cityy = HBCBasicfeature.getExcelVal("HBC1185", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.click();
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						log.add( "The Entered value from the excel file is : " + cityy);
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cityy);
						String ctyVal = city.getAttribute("value");
						
						if(ctyVal.isEmpty())
						{
							Fail( "The city field is empty.",log);
						}
						else
						{
							Pass( "The city name was enterable.",log);
						}
						shipandPaypageShipShipCreateEditAddresssCty.clear();
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1272 Issue ." + e.getMessage());
				Exception(" HBC - 1272 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1274 Guest Checkout - Verify that first name & last name fields accepts empty spaces in "Shipping Address" overlay/page.*/
	public void HBC1274FNLEmptySpaceValidation()
	{
		ChildCreation(" HBC - 1274 Guest Checkout - Verify that only characters should be accepted in First Name and Last Name field..");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1274", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[0]);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[1].toString());
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[1]);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}

					boolean addOverlay = ldAddressVerification();
					if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
					{
						jsclick(addressVerificationEdit);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
						Thread.sleep(1000);
					}
					if(addOverlay==true)
					{
						Pass( "The User is able to enter the First and Last Name with the Spaces and they are navigated to the Address Verification page.");
					}
					else
					{
						Fail( "The User is navigated to the Address Verification page when they enter the First name and Last name with the Spaces.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1274 Issue ." + e.getMessage());
				Exception(" HBC - 1274 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1275 Guest Checkout - Verify that "Country" field should have only one dropdown option.*/
	public void HBC1275ShipPageCreateShipStreetCountryDDSizes()
	{
		ChildCreation(" HBC - 1275 Guest Checkout - Verify that Country field should have only one dropdown option.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						int size = sel.getOptions().size();
						if(size==1)
						{
							Pass( "The Country Drop down has no more than one values.");
						}
						else
						{
							for(int i = 0; i<size; i++)
							{
								log.add( "The Displayed Country name is : " + sel.getOptions().get(i).getText());
							}
							Fail( "The Country Drop down has more than one values. The total value is : " + size +". The displayed countries in the list are : ", log);
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1275 Issue ." + e.getMessage());
				Exception(" HBC - 1275 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1276 Guest Checkout - Verify that "Country" field should should be selected in "Canada" option by default.*/
	public void HBC1276ShipPageCreateShipDefaultCountry()
	{
		ChildCreation(" HBC - 1276 Guest Checkout - Verify that Country field should should be selected in Canada option by default.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1276", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String txt = sel.getFirstSelectedOption().getText().replaceAll("\\s", "");
						log.add( "The Expected Value was : " + expVal);
						log.add( "The Actual Value is : " + txt);
						if(txt.isEmpty())
						{
							Fail( "The Country Selection seems to be empty.");
						}
						else if(txt.equalsIgnoreCase(expVal))
						{
							Pass( " The Default Selected country is as expected.",log);
						}
						else
						{
							Fail( "There is mismatch in the expected and the actual default country selection.",log);
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1276 Issue ." + e.getMessage());
				Exception(" HBC - 1276 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1277 Guest Checkout - Verify that "Province" dropdown field should be selected in "Select" option by default.*/
	public void HBC1277ShippingCADefaultStateSel()
	{
		ChildCreation(" HBC - 1277 Guest Checkout - Verify that Province dropdown field should be selected in Select option by default.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1277", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssState))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssState);
						String actVal = sel.getFirstSelectedOption().getText();
						log.add( "The Expected Value was : " + expVal);
						log.add( "The Actual Value is : " + actVal);
						if(expVal.equalsIgnoreCase(actVal))
						{
							Pass( "The Expected Default Value is selected.",log);
						}
						else
						{
							Fail( "The Expected Default Value is not selected.",log);
						}
					}
					else
					{
						Fail( "The State drop down is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1277 Issue ." + e.getMessage());
				Exception(" HBC - 1277 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1278 Guest Checkout - Billing Address Section > Verify that "Zip Code" field should accept alphanumeric characters.*/
	public void HBC1278ShippingZipAlphaNumericValidation()
	{
		ChildCreation(" HBC - 1278 Guest Checkout - Billing Address Section > Verify that Zip Code field should accept alphanumeric characters.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
					
					String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1206", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						String val = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value");
						log.add ( "The Value entered from the excel sheet was : " + cellVal);
						log.add ( "The Value in the field is : " + val);
						if(val.isEmpty())
						{
							Fail ( "The Field is empty.");
						}
						else if(val.equalsIgnoreCase(cellVal))
						{
							Pass( " There is no mismatch in the entered and the current value.",log);
						}
						else
						{
							Fail( " There is mismatch in the entered and the current value.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1278 Issue ." + e.getMessage());
				Exception(" HBC - 1278 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1279 Guest Checkout - Verify that "Postal Code" field should accept maximum of 7 characters.*/
	public void HBC1279ShippingCAPOLength()
	{
		ChildCreation(" HBC - 1279 Guest Checkout - Verify that Postal Code field should accept maximum of 7 characters.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1207", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code Area is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").isEmpty())
							{
								Fail( "The Zip Code field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").length()<=7)
							{
								Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						}
					}
					else
					{
						Fail( "The Postal Code Area is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1279 Issue ." + e.getMessage());
				Exception(" HBC - 1279 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1280 Guest Checkout - Verify that when the user entered less than 6 digits in the "Postal Code" field, then the corresponding alert message should be shown.*/
	public void HBC1280ShippingCAPOLengthValidationAlert()
	{
		ChildCreation(" HBC - 1280 Guest Checkout - Verify that when the user entered less than 6 digits in the Postal Code field, then the corresponding alert message should be shown.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1208", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					String alert = HBCBasicfeature.getExcelVal("HBC1208", sheet, 2);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The expected alert was : " + alert.toString());
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						String poAlert = checkoutBPOAlert.getText();
						log.add( "The actual alert was : " + poAlert);
						if(poAlert.equalsIgnoreCase(alert.toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1280 Issue ." + e.getMessage());
				Exception(" HBC - 1280 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1281 Guest Checkout - Verify that when the user entered alphabets or numbers in the "Postal Code" field, then the corresponding alert message should be shown.*/
	public void HBC1281ShippingCAPOAlphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1281 Guest Checkout - Verify that when the user entered alphabets or numbers in the Postal Code field, then the corresponding alert message should be shown.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1281", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					String alert = HBCBasicfeature.getExcelVal("HBC1281", sheet, 2);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The expected alert was : " + alert.toString());
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						String poAlert = checkoutBPOAlert.getText();
						log.add( "The actual alert was : " + poAlert);
						if(poAlert.equalsIgnoreCase(alert.toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1281 Issue ." + e.getMessage());
				Exception(" HBC - 1281 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1284 Guest Checkout - Verify that user should be able to enter phone number in the "Phone Number" field and it should accept maximum of 10 numbers.*/
	public void HBC1284ShippingCAPHNumLenValidation()
	{
		ChildCreation(" HBC - 1284 Guest Checkout - Verify that when the user entered alphabets or numbers in the Postal Code field, then the corresponding alert message should be shown.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Postal Code Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1284", sheet, 1).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").length()<=3)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Postal Code Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1284", sheet, 2).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").length()<=3)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1284", sheet, 3).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").length()<=4)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1284 Issue ." + e.getMessage());
				Exception(" HBC - 1284 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1285 Guest Checkout - Verify that corresponding alert message should be shown when the user entered alphabets or special characters in the "Phone Number" field.*/
	public void HBC1285ShippingCAPHAphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1285 Guest Checkout - Verify that corresponding alert message should be shown when the user entered alphabets or special characters in the Phone Number field.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1285", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					String[] alert = HBCBasicfeature.getExcelVal("HBC1285", sheet, 2).split("\n");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert1,"style","block"));
						String ctAlert = checkoutSPHAlert1.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[0].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert3,"style","block"));
						String ctAlert = checkoutSPHAlert3.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[1].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1285 Issue ." + e.getMessage());
				Exception(" HBC - 1285 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1286 Guest Checkout - Verify that when the "Phone Number" field is left blank and tapping on other text boxes, then the corresponding alert message should be shown.*/
	public void HBC1286ShippingCAPHEmptyValidationAlert()
	{
		ChildCreation(" HBC - 1286 Guest Checkout - Verify that when the Phone Number field is left blank and tapping on other text boxes, then the corresponding alert message should be shown.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
						}
					}
				
					String[] alert = HBCBasicfeature.getExcelVal("HBC1286", sheet, 1).split("\n");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert3,"style","block"));
						String ctAlert = checkoutSPHAlert3.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[0].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1286 Issue ." + e.getMessage());
				Exception(" HBC - 1286 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1287 Guest Checkout - Verify that alert must be shown on entering only space in any of the input fields and selecting "Ok" button.*/
	public void HBC1287ShipPageEmptyAddValidation()
	{
		ChildCreation(" HBC - 1287 Guest Checkout - Verify that alert must be shown on entering only space in any of the input fields and selecting Ok button.");
		if(shipPage==true)
		{
			try
			{
				Actions act = new Actions(driver);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssFName).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssStAddress).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssCty).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssPOcode).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssPH1).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssPH2).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssPH3).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).sendKeys(Keys.SPACE).build().perform();
						act.moveToElement(shipandPaypageShipShipCreateEditAddresssLName).sendKeys(Keys.TAB).build().perform();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					WebDriverWait w1 = new WebDriverWait(driver, 5);
					boolean addOverlay = true;
					do
					{
						try
						{
							shipandPaypageShipShipCreateEditAddressOkBtn.click();
							w1.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							addOverlay = true;
							if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
							{
								jsclick(addressVerificationEdit);
								wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
								Thread.sleep(1000);
							}
						}
						catch(Exception e)
						{
							addOverlay = false;
						}
					}while(addOverlay==true);
					
					if(addOverlay==false)
					{
						Pass( "The User is not navigated to the Address Verification page.");
					}
					else
					{
						Fail( "The User is navigated to the Address Verification page.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1287 Issue ." + e.getMessage());
				Exception(" HBC - 1287 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1288 Guest Checkout - Verify that on entering html tags in the fields should not be treated as strings.*/
	/* HBC - 1289 Guest Checkout - Verify that on entering the values to the fields where alert is displayed, the alert should be dismissed automatically.*/
	public void HBC1288ShipPageAddShippAddressHTMLValidation()
	{
		ChildCreation(" HBC - 1288 Guest Checkout - Verify that on entering html tags in the fields should not be treated as strings.");
		boolean HBC1289 = true;
		if(shipPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1287", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						String prevVal = shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal);
						boolean addOverlay = ldNoAddressVerification();
						if(addOverlay==true)
						{
							Pass( "The User is able to Html but it was not treated as strings.");
						}
						else
						{
							Fail( "The User is navigated to the Address Verification page when they enter the html tags.");
						}
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(prevVal);
						shipandPaypageShipShipCreateEditAddresssLName.click();
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						String prevVal = shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal);
						boolean addOverlay = ldNoAddressVerification();
						if(addOverlay==true)
						{
							Pass( "The User is able to Html but it was not treated as strings.");
						}
						else
						{
							Fail( "The User is navigated to the Address Verification page when they enter the html tags.");
						}
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(prevVal);
						shipandPaypageShipShipCreateEditAddresssStAddress.click();
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code field is displayed.");
						String prevVal = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						boolean addOverlay = ldNoAddressVerification();
						if(addOverlay==true)
						{
							Pass( "The User is able to Html but it was not treated as strings.");
						}
						else
						{
							Fail( "The User is navigated to the Address Verification page when they enter the html tags.");
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(prevVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						if(HBCBasicfeature.isElementPresent(checkoutBPOAlert))
						{
							Fail( "The PO Alert is not dismissed.");
							if(HBC1289 == true)
							{
								HBC1289 = false;
							}
						}
						else
						{
							Pass( "The PO Alert is dismissed.");
							if(HBC1289 == true)
							{
								HBC1289 = true;
							}
						}
					}
					else
					{
						Fail( "The Postal Code is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						String prevVal = shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value");
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal);
						boolean addOverlay = ldNoAddressVerification();
						if(addOverlay==true)
						{
							Pass( "The User is able to Html but it was not treated as strings.");
						}
						else
						{
							Fail( "The User is navigated to the Address Verification page when they enter the html tags.");
						}
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(prevVal);
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						if(HBCBasicfeature.isElementPresent(checkoutSPHAlert1))
						{
							Fail( "The Invalid Contact Number Alert is not dismissed.");
							if(HBC1289 == true)
							{
								HBC1289 = false;
							}
						}
						else
						{
							Pass( "The Invalid Contact Number Alert is dismissed.");
							if(HBC1289 == true)
							{
								HBC1289 = true;
							}
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1288 Issue ." + e.getMessage());
				Exception(" HBC - 1288 Issue ." + e.getMessage());
			}
			
			/* HBC - 1289 Guest Checkout - Verify that on entering the values to the fields where alert is displayed, the alert should be dismissed automatically.*/
			ChildCreation("HBC - 1289 Guest Checkout - Verify that on entering the values to the fields where alert is displayed, the alert should be dismissed automatically.");
			try
			{
				if(HBC1289==true)
				{
					Pass( "The Alert got dismissed when values are entered.");
				}
				else
				{
					Fail( "The Alert was not dismissed when values are entered.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1289 Issue ." + e.getMessage());
				Exception(" HBC - 1289 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1291 Guest Checkout - Shipping Information Section > Verify that "Ship to" dropdown field should be shown below the "Shipping Information" section.*/
	public void HBC1291ShippingPageShipToDD()
	{
		ChildCreation("HBC - 1291 Guest Checkout - Shipping Information Section > Verify that Ship to dropdown field should be shown below the Shipping Information section.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
				{
					Pass( " The Ship To Address Customer Drop Down is displayed.");
				}
				else
				{
					Fail( " The Ship To Address Customer Drop Down is not displayed..");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1291 Issue ." + e.getMessage());
				Exception(" HBC - 1291 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1293 Guest Checkout - Shipping Information Section > Verify that user can be able to change the shipping address by changing the dropdown option in the "Ship to" dropdown field. */
	public void HBC1293ShipPageSelectAddFromShipList()
	{
		ChildCreation("HBC - 1293 Guest Checkout - Shipping Information Section > Verify that user can be able to change the shipping address by changing the dropdown option in the Ship to dropdown field.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
				{
					Pass( "The Selected Address Drop Down is displayed.");
					Select sel = new Select(shipandPaypageShipShipToName);
					String currAdd = sel.getFirstSelectedOption().getText();
					int size = sel.getOptions().size();
					if(size>1)
					{
						Pass( "The newly added address is added to the drop down.");
						//Random r = new Random();
						//int sAddsel = r.nextInt(size);
						sel.getOptions().get(0).click();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "none"));
						Thread.sleep(1000);
						String newAdd = sel.getFirstSelectedOption().getText();
						log.add( "The Old address was : " + currAdd);
						log.add( "The Old address was : " + newAdd);
						if(newAdd.equals(currAdd))
						{
							Fail( "The User was not able to select the new Address from the drop down.");
						}
						else
						{
							Pass( "The User was able to select the new Address from the drop down.");
						}
					}
					else
					{
						Fail( "The address list less than or equal to one in the drop down.");
					}
				}
				else
				{
					Fail( "The Selected Address Drop Down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1293 Issue ." + e.getMessage());
				Exception(" HBC - 1293 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1294 Guest Checkout - Shipping Information Section > Verify that "Edit", "Create Address" text link should be underlined.*/
	public void HBC1294ShipSectionsEditCreateUnderLined()
	{
		ChildCreation(" HBC - 1294 Guest Checkout - Shipping Information Section > Verify that Edit, Create Address text link should be underlined.");
		if(shipPage==true)
		{
			try
			{
				String expCol = HBCBasicfeature.getExcelVal("HBC1294", sheet, 1);
				String expColl = HBCBasicfeature.getExcelVal("HBC1294", sheet, 2);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					String col = shipandPaypageShipEditLink.getCssValue("border-bottom").replaceAll("1px solid", "");
					String actCol = HBCBasicfeature.colorfinder(col.substring(12));
					log.add( "The Expected Color was : " + expCol);
					log.add( "The actual Color is : " + actCol);
					if(expCol.equals(actCol))
					{
						Pass(" The Edit Link is Underlined and the color matches the expected one.",log);
					}
					else
					{
						Fail(" The Edit Link is not Underlined or the color does not matches the expected one.",log);
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateAddLink))
				{
					Pass( "The Create Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					String col = shipandPaypageShipCreateAddLink.getCssValue("border-bottom").replaceAll("1px solid", "");
					String actCol = HBCBasicfeature.colorfinder(col.substring(12));
					log.add( "The Expected Color was : " + expColl);
					log.add( "The actual Color is : " + actCol);
					if(expColl.equals(actCol))
					{
						Pass(" The Create Address Link is Underlined and the color matches the expected one.",log);
					}
					else
					{
						Fail(" The Create Address Link is not Underlined or the color does not matches the expected one.",log);
					}
				}
				else
				{
					Fail( "The Create Link in the Shipping Section in the Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1294 Issue ." + e.getMessage());
				Exception(" HBC - 1294 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1301 Guest Checkout - Payment Information Section > Verify that Selected Billing address should be shown with "Edit" & "Create Address" text links below the Payment information section.*/
	public void HBC1301PaySectionsEditCreateLinks()
	{
		ChildCreation(" HBC - 1301 Guest Checkout - Payment Information Section > Verify that Selected Billing address should be shown with Edit & Create Address text links below the Payment information section.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayEditLink))
				{
					Pass( "The Edit Link in the Payment Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Edit Link in the Payment Section in Shipping and Billing Tab is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayCreateAddLink))
				{
					Pass( "The Create Link in the Payment Section in the Shipping and Billing Tab is displayed.");
				}
				else
				{
					Fail( "The Create Link in the Payment Section in the Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1301 Issue ." + e.getMessage());
				Exception(" HBC - 1301 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1302 Guest Checkout - Payment Information Section > Verify that "Billing Address" overlay/page should be shown with the prefilled billing details on clicking the Edit link.*/
	/* HBC - 1303 Guest Checkout - Payment Information Section > Verify that "X" close icon should be shown in the "Billing Address" overlay/page.*/
	/* HBC - 1304 Guest Checkout - Payment Information Section > Verify that "Ok" button & "Cancel" button should be shown in the "Billing Address" overlay/page.*/
	/* HBC - 1305 Guest Checkout - Payment Information Section > Verify that "Billing Address" overlay/page should be closed on tapping the "Cancel" button*/
	public void HBC302PayAddressEditView()
	{
		ChildCreation(" HBC - 1302 Guest Checkout - Payment Information Section > Verify that Billing Address overlay/page should be shown with the prefilled billing details on clicking the Edit link.");
		String cnty = "";
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
				{
					paybefAdd = shipandPaypagePayAddressDisp.getText();
				}
					
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					jsclick(shipandPaypagePayEditLink);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Payment Address Overlay is dispalyed.");
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The First Name Value is not displayed.");
							}
							else
							{
								Pass( " The First Name value is displayed. The displayed value is  : " + txt);
							}
						}
						else
						{
							Fail( "The First Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The Last Name Value is not displayed.");
							}
							else
							{
								Pass( " The Last Name value is displayed. The displayed value is  : " + txt);
							}
						}
						else
						{
							Fail( "The Last Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The Street Address Name Value is not displayed.");
							}
							else
							{
								Pass( " The Street Address value is displayed. The displayed value is  : " + txt);
							}
						}
						else
						{
							Fail( "The Street Address is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City Field is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssCty.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The City Name Value is not displayed.");
							}
							else
							{
								Pass( " The City Name value is displayed. The displayed value is  : " + txt);
							}
						}
						else
						{
							Fail( "The City Field is not displayed in the Shipping Address Page.");
						}

						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country is displayed in the Shipping Address Page.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							cnty = sel.getFirstSelectedOption().getText();
							if(cnty.isEmpty())
							{
								Fail( "The Country Name Value is not displayed.");
							}
							else
							{
								Pass( " The Country Name value is displayed. The displayed value is  : " + cnty);
							}
						}
						else
						{
							Fail( "The Country is not displayed in the Shipping Address Page.");
						}
						
						if(cnty.contains("Canada")||(cnty.contains("States")))
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
							{
								Pass( "The State Field is displayed in the Shipping Address Page.");
								Select sel = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
								String txt = sel.getFirstSelectedOption().getText();
								if(txt.isEmpty())
								{
									Fail( "The State Name Value is not displayed.");
								}
								else
								{
									Pass( " The State Name value is displayed. The displayed value is  : " + txt);
								}
							}
							else
							{
								Fail( "The State Field is not displayed in the Shipping Address Page.");
							}
						}
						else
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
							{
								Pass( "The State Field is displayed in the Shipping Address Page.");
								Select sel = new Select(shipandPaypageShipShipCreateEditAddresssPayUKState);
								String txt = sel.getFirstSelectedOption().getText();
								if(txt.isEmpty())
								{
									Fail( "The State Name Value is not displayed.");
								}
								else
								{
									Pass( " The State Name value is displayed. The displayed value is  : " + txt);
								}
							}
							else
							{
								Fail( "The State Field is not displayed in the Shipping Address Page.");
							}
						}
						
						if(cnty.contains("States"))
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
							{
								Pass( "The PO Code Field is displayed in the Shipping Address Page.");
								String txt = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value");
								if(txt.isEmpty())
								{
									Fail( "The First Name Value is not displayed.");
								}
								else
								{
									Pass( " The First Name value is displayed. The displayed value is  : " + txt);
								}
							}
							else
							{
								Fail( "The PO Code is not displayed in the Shipping Address Page.");
							}
						}
						else
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
							{
								Pass( "The PO Code Field is displayed in the Shipping Address Page.");
								String txt = shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value");
								if(txt.isEmpty())
								{
									Fail( "The First Name Value is not displayed.");
								}
								else
								{
									Pass( " The First Name value is displayed. The displayed value is  : " + txt);
								}
							}
							else
							{
								Fail( "The PO Code is not displayed in the Shipping Address Page.");
							}
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
						{
							Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
							String txt = shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value");
							if(txt.isEmpty())
							{
								Fail( "The Contact Number Value is not displayed.");
							}
							else
							{
								Pass( " The Contact Number value is displayed. The displayed value is  : " + txt);
							}
							
							String txt1 = shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value");
							if(txt1.isEmpty())
							{
								Fail( "The Contact Number Value is not displayed.");
							}
							else
							{
								Pass( " The Contact Number value is displayed. The displayed value is  : " + txt1);
							}
							
							String txt2 = shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value");
							if(txt2.isEmpty())
							{
								Fail( "The Contact Number Value is not displayed.");
							}
							else
							{
								Pass( " The Contact Number value is displayed. The displayed value is  : " + txt2);
							}
						}
						else
						{
							Fail( "The Phone Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Close Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
						{
							Pass( "The Cancel Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Cancel Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
						{
							Pass( "The Ok button in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Ok button in Create overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Payment Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1302 Issue ." + e.getMessage());
				Exception(" HBC - 1302 Issue ." + e.getMessage());
			}
			
			/* HBC - 1303 Guest Checkout - Payment Information Section > Verify that "X" close icon should be shown in the "Billing Address" overlay/page.*/
			ChildCreation(" HBC - 1303 Guest Checkout - Payment Information Section > Verify that X close icon should be shown in the Billing Address overlay/page.");
			try
			{
				if(shipPage==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Edit overlay is displayed.");
						}
						else
						{
							Fail( "The Close Link in Edit overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Payment Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Skip ( " The User was unable to select the address from the verification page in the checkout page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1303 Issue ." + e.getMessage());
				Exception(" HBC - 1303 Issue ." + e.getMessage());
			}
			
			/* HBC - 1304 Guest Checkout - Payment Information Section > Verify that "Ok" button & "Cancel" button should be shown in the "Billing Address" overlay/page.*/
			ChildCreation(" HBC - 1304 Guest Checkout - Payment Information Section > Verify that Ok button & Cancel button should be shown in the Billing Address overlay/page.");
			try
			{
				if(shipPage==true)
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
						{
							Pass( "The Cancel Link in Edit overlay is displayed.");
						}
						else
						{
							Fail( "The Cancel Link in Edit overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
						{
							Pass( "The Ok button in Edit overlay is displayed.");
						}
						else
						{
							Fail( "The Ok button in Edit overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Payment Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Skip( "The Address Edit Overlay is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1304 Issue ." + e.getMessage());
				Exception(" HBC - 1304 Issue ." + e.getMessage());
			}
			
			/* HBC - 1305 Guest Checkout - Payment Information Section > Verify that "Billing Address" overlay/page should be closed on tapping the "Cancel" button.*/
			ChildCreation(" HBC - 1305 Guest Checkout - Payment Information Section > Verify that Billing Address overlay/page should be closed on tapping the Cancel button.");
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
				{
					Pass( "The Cancel button in the Edit Overlay is displayed.");
					jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
					Thread.sleep(1000);
					if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Edit Address Overlay is not displayed.");
					}
					else
					{
						Fail("The Saved Address list is not displayed.");
					}
				}
				else
				{
					Fail( "The Cancel button in the Edit Overlay is displayed is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1305 Issue ." + e.getMessage());
				Exception(" HBC - 1305 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1306 Guest Checkout - Payment Information Section > Verify that "Billing Address" overlay/page should be closed on tapping the "X" icon. */
	public void HBC1306PayEditAddressOverlayClose()
	{
		ChildCreation(" HBC - 1306 Guest Checkout - Payment Information Section > Verify that Billing Address overlay/page should be closed on tapping the X icon.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					jsclick(shipandPaypagePayEditLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Edit Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Edit overlay is displayed.");
							jsclick(shipandPaypageShipShipCreateEditAddressCloseLink);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
							Thread.sleep(1000);
							if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
							{
								Pass( "The Address Edit Overlay is closed.");
							}
							else
							{
								Fail( "The Address Edit Overlay is not closed.");
							}
						}
						else
						{
							Fail( "The Close Link in Edit overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Edit Payment Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Payment Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1306 Issue ." + e.getMessage());
				Exception(" HBC - 1306 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1307 Guest Checkout - Payment Information Section > Verify that edited field details in the "Billing Address" overlay/page should get updated in "Billing Information" section on tapping the "Ok" button.*/
	public void HBC1307PayEditAddressUpdated()
	{
		ChildCreation(" HBC - 1307 Guest Checkout - Payment Information Section > Verify that edited field details in the Billing Address overlay/page should get updated in Billing Information section on tapping the Ok button.");
		String aftAdd = "";
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
				{
					paybefAdd = shipandPaypagePayAddressDisp.getText();
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipEditLink))
				{
					Pass( "The Edit Link in the Shipping Section in the Shipping and Billing Tab is displayed.");
					Thread.sleep(1000);
					jsclick(shipandPaypagePayEditLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					String sfn = HBCBasicfeature.getExcelVal("HBC1307", sheet, 1);
					String lsn = HBCBasicfeature.getExcelVal("HBC1307", sheet, 2);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssFName.click();
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							log.add( "The Value from the excel file was : " + sfn);
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
						}
						else
						{
							Fail( "The First Name field is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The First Name field is displayed.");
							shipandPaypageShipShipCreateEditAddresssLName.click();
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							log.add( "The Value from the excel file was : " + lsn);
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
						}
						else
						{
							Fail( "The Last Name field is not displayed.");
						}
						
						boolean addOverlay = ldAddressVerification();
						if(addOverlay==true)
						{
							wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
							addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
							if(addVerEnable==true)
							{
								Pass( "The Address Verification Overlay is enabled.");
								if(HBCBasicfeature.isElementPresent(addressuseasEntered))
								{
									Pass( "The Use As Entered button is displayed.");
									jsclick(addressuseasEntered);
									Thread.sleep(1000);
									wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
									wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
									Thread.sleep(1000);
									aftAdd = shipandPaypagePayAddressDisp.getText();
									log.add( "The address before modification is  : " + paybefAdd);
									log.add( "The address after modification is  : " + aftAdd);
									if((paybefAdd.isEmpty())||(aftAdd.isEmpty()))
									{
										Fail( "The user failed to get the before address and after address they are empty.",log);
									}
									else if(paybefAdd.equals(aftAdd))
									{
										Fail( " The newly modified address is not dispalyed.",log);
									}
									else
									{
										log.add( " There is update made to the address list.");
										log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
										log.add( " There is update made to the address list. The entered first name was : " + sfn);
										
										if(aftAdd.contains(sfn))
										{
											Pass( "The Added First Name is dispalyed.",log);
										}
										else
										{
											Fail( "The Added First Name is not dispalyed.",log);
										}
										
										log.add( " There is update made to the address list.");
										log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
										log.add( " There is update made to the address list. The entered first name was : " + sfn);
										
										if(aftAdd.contains(lsn))
										{
											Pass( "The Added Last Name is dispalyed.",log);
										}
										else
										{
											Fail( "The Added Last Name is not dispalyed.",log);
										}
									}
									
									if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
									{
										Pass( "The Selected Address Drop Down is displayed.");
										Select sel = new Select(shipandPaypageShipShipToName);
										int size = sel.getOptions().size();
										if(size>1)
										{
											for( int i = 0; i<size; i++)
											{
												String addedAdress = sel.getOptions().get(i).getText();
												log.add(" The displayed address list was : " + addedAdress);
											}
											Pass( "The newly added address is added to the drop down.");
										}
										else
										{
											Fail( "The newly added address is not added to the drop down.");
										}
									}
									else
									{
										Fail( "The Selected Address Drop Down is not displayed.");
									}
								}
								else
								{
									Fail( "The Address Use As entered is not displayed.");
									if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
									{
										jsclick(addressVerificationEdit);
										wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
										Thread.sleep(1000);
										if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
										{
											Pass( "The Cancel Link in Create overlay is displayed.");
											jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
											Thread.sleep(1000);
											wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
											Thread.sleep(1000);
											if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
											{
												Pass( "The Address Edit Overlay is closed.");
											}
											else
											{
												Fail( "The Address Edit Overlay is not closed.");
											}
										}
										else
										{
											Fail( "The Cancel Link in Create overlay is not displayed.");
										}
									}
									else
									{
										Fail( "The Edit link in the Address Verification overlay is not displayed.");
									}
								}
							}
							else
							{
								Fail( "The Address Verification Overlay is not enabled.");
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
						
					}
					else
					{
						Fail("The Edit Billing Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Payment Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1307 Issue ." + e.getMessage());
				Exception(" HBC - 1307 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}

	/* HBC - 1308 Guest Checkout - Billing Address Overlay/Page > Verify that "Billing Address" overlay/page should be as per the creative. */
	public void HBC1308ShipPayCreateShippingAddress()
	{
		ChildCreation(" HBC - 1308 Guest Checkout - Billing Address Overlay/Page > Verify that Billing Address overlay/page should be as per the creative.");
		String cnty = "";
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipAddressDisp))
				{
					paybefAdd = shipandPaypagePayAddressDisp.getText();
				}
				
				if(HBCBasicfeature.isElementPresent(shipandPaypagePayCreateAddLink))
				{
					jsclick(shipandPaypagePayCreateAddLink);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.attributeContains(shipandPaypageShipShipCreateEditAddress,"style","opacity: 1"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
					{
						Pass("The Create Shipping Address Overlay is dispalyed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
						{
							Pass( "The First Name is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The First Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
						{
							Pass( "The Last Name is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Last Name is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
						{
							Pass( "The Street Address is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Street Address is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
						{
							Pass( "The City Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The City Field is not displayed in the Shipping Address Page.");
						}

						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
						{
							Pass( "The Country is displayed in the Shipping Address Page.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
							cnty = sel.getFirstSelectedOption().getText();
						}
						else
						{
							Fail( "The Country is not displayed in the Shipping Address Page.");
						}
						
						if(cnty.contains("Canada")||(cnty.contains("States")))
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
							{
								Pass( "The State Field is displayed in the Shipping Address Page.");
							}
							else
							{
								Fail( "The State Field is not displayed in the Shipping Address Page.");
							}
						}
						else
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
							{
								Pass( "The State Field is displayed in the Shipping Address Page.");
							}
							else
							{
								Fail( "The State Field is not displayed in the Shipping Address Page.");
							}
						}
						
						if(cnty.contains("States"))
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
							{
								Pass( "The PO Code Field is displayed in the Shipping Address Page.");
							}
							else
							{
								Fail( "The PO Code is not displayed in the Shipping Address Page.");
							}
						}
						else
						{
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
							{
								Pass( "The PO Code Field is displayed in the Shipping Address Page.");
							}
							else
							{
								Fail( "The PO Code is not displayed in the Shipping Address Page.");
							}
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
						{
							Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The Phone Code is not displayed in the Shipping Address Page.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
						{
							Pass( "The Email Field in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Email Field in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditTermsandCond))
						{
							Pass( "The Terms and Condition in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Terms and Condition in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
						{
							Pass( "The Close Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Close Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
						{
							Pass( "The Cancel Link in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Cancel Link in Create overlay is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
						{
							Pass( "The Ok button in Create overlay is displayed.");
						}
						else
						{
							Fail( "The Ok button in Create overlay is not displayed.");
						}
					}
					else
					{
						Fail("The Create Billing Address Overlay is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Edit Link in the Shipping Section in Shipping and Billing Tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1308 Issue ." + e.getMessage());
				Exception(" HBC - 1308 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1310 Guest Checkout - Verify that "Billing Address" overlay/page should have the following text boxes : "First Name", "Last Name", "Street Address", and so on. */
	public void HBC1310ShipPayCreateShippingAddressFields(String tc)
	{
		String tcId = "";
		if(tcId=="HBC1310")
		{
			tcId = "HBC - 1310 Guest Checkout - Verify that Billing Address overlay/page should have the following text boxes : First Name, Last Name, Street Address, and so on..";
		}
		else
		{
			tcId = "HBC - 1313 Guest Checkout - Verify that required input fields needs to be displayed to add a new Billing address";
		}
		ChildCreation(tcId);
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The Last Name is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The City Field is not displayed in the Shipping Address Page.");
					}

					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						payDefSelCountr = sel.getFirstSelectedOption().getText();
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					else if(payDefSelCountr.contains("Kingdom"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					
					if(payDefSelCountr.contains("States"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
					{
						Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Phone Code is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
					{
						Pass( "The Email Field in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Email Field in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditTermsandCond))
					{
						Pass( "The Terms and Condition in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Terms and Condition in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCloseLink))
					{
						Pass( "The Close Link in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Close Link in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
					{
						Pass( "The Cancel Link in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Cancel Link in Create overlay is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressOkBtn))
					{
						Pass( "The Ok button in Create overlay is displayed.");
					}
					else
					{
						Fail( "The Ok button in Create overlay is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1310 Issue ." + e.getMessage());
				Exception(" HBC - 1310 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1311 Guest Checkout - Verify that alert should be shown when the user leaves any field without entering any details.*/
	/* HBC - 1346 Guest Checkout - Billing Address Overlay/Page > Verify that alert message should be shown, without entering any values in the input field clicking on each text boxes.*/
	public void HBC1311ShipPageCreateShippingAddressEmptyVal(String tc)
	{
		String tcId = "";
		if(tc.equals("HBC-1311"))
		{
			tcId = "HBC - 1311 Guest Checkout - Verify that alert should be shown when the user leaves any field without entering any details.";
		}
		else
		{
			tcId = "HBC - 1346 Guest Checkout - Billing Address Overlay/Page > Verify that alert message should be shown, without entering any values in the input field clicking on each text boxes.";
		}
		ChildCreation(tcId);
		if(shipPage==true)
		{
			try
			{
				String[] alert = HBCBasicfeature.getExcelVal("HBC1262", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.click();
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						shipandPaypageShipShipCreateEditAddresssLName.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBStaddressAlert,"style","block"));
						String stAddAlert = checkoutBStaddressAlert.getText();
						log.add( "The Expected alert was : " + alert[2].toString());
						log.add( "The actual alert is : " + stAddAlert);
						
						if(stAddAlert.equalsIgnoreCase(alert[2].toString()))
						{
							Pass( "The Street Address alert was as expected.",log);
						}
						else
						{
							Fail( "The Street Address alert was not as expected.",log);
						}
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						HBCBasicfeature.scrolldown(staddress, driver);
						shipandPaypageShipShipCreateEditAddresssCty.click();
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						shipandPaypageShipShipCreateEditAddresssStAddress.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBCityAlert,"style","block"));
						String cityAlert = checkoutBCityAlert.getText();
						log.add( "The Expected alert was : " + alert[3].toString());
						log.add( "The actual alert is : " + cityAlert);
						
						if(cityAlert.equalsIgnoreCase(alert[3].toString()))
						{
							Pass( "The City alert was as expected.",log);
						}
						else
						{
							Fail( "The City alert was not as expected.",log);
						}
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						sel.selectByValue("CA");
						payDefSelCountr = sel.getFirstSelectedOption().getText();
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					
					if(payDefSelCountr.contains("States"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.click();
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssCty.click();
							wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
							String poAlert = checkoutBPOAlert.getText();
							log.add( "The Expected alert was : " + alert[7].toString());
							log.add( "The actual alert is : " + poAlert);
							
							if(poAlert.equalsIgnoreCase(alert[7].toString()))
							{
								Pass( "The Zip Code alert was as expected.",log);
							}
							else
							{
								Fail( "The Zip Code alert was not as expected.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Pass( "The PO Code Field is displayed in the Shipping Address Page.");
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.click();
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
							shipandPaypageShipShipCreateEditAddresssPH1.click();
							wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
							String poAlert = checkoutBPOAlert.getText();
							log.add( "The Expected alert was : " + alert[7].toString());
							log.add( "The actual alert is : " + poAlert);
							
							if(poAlert.equalsIgnoreCase(alert[7].toString()))
							{
								Pass( "The Zip Code alert was as expected.",log);
							}
							else
							{
								Fail( "The Zip Code alert was not as expected.",log);
							}
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
						}
						else
						{
							Fail( "The PO Code is not displayed in the Shipping Address Page.");
						}
					}

					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert1,"style","block"));
						String ctAlert = checkoutSPHAlert1.getText();
						log.add( "The Expected alert was : " + alert[8].toString());
						log.add( "The actual alert is : " + ctAlert);
						if(ctAlert.equalsIgnoreCase(alert[8].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1311 Issue ." + e.getMessage());
				Exception(" HBC - 1311 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1312 Guest Checkout - Verify that guest user should be able to add the new Billing address details in the "Billing Address" overlay/page.*/
	public void HBC1312ShipPageCreateNewBillingAddress()
	{
		ChildCreation("HBC - 1312 Guest Checkout - Verify that guest user should be able to add the new Billing address details in the Billing Address overlay/page.");
		String aftAdd = "";
		if(shipPage==true)
		{
			try
			{
				String sfn = HBCBasicfeature.getExcelVal("HBC1312", sheet, 1);
				String lsn = HBCBasicfeature.getExcelVal("HBC1312", sheet, 2);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						log.add( "The Value from the excel file was : " + sfn);
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(sfn);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Value from the excel file was : " + lsn);
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(lsn);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					boolean addOverlay = ldAddressVerification();
					if(addOverlay==true)
					{
						wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
						addVerEnable = addressVerificationoverlay.getAttribute("style").contains("block");
						if(addVerEnable==true)
						{
							Pass( "The Address Verification Overlay is enabled.");
							if(HBCBasicfeature.isElementPresent(addressuseasEntered))
							{
								Pass( "The Use As Entered button is displayed.");
								jsclick(addressuseasEntered);
								wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 1"));
								wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "opacity: 0"));
								wait.until(ExpectedConditions.attributeContains(shipandPaypage, "style", "block"));
								Thread.sleep(1000);
								aftAdd = shipandPaypagePayAddressDisp.getText();
								log.add( "The address before modification is  : " + paybefAdd);
								log.add( "The address after modification is  : " + aftAdd);
								if((paybefAdd.isEmpty())||(aftAdd.isEmpty()))
								{
									Fail( "The user failed to get the before address and after address they are empty.",log);
								}
								else if(paybefAdd.equals(aftAdd))
								{
									Fail( " The newly modified address is not dispalyed.",log);
								}
								else
								{
									log.add( " There is update made to the address list.");
									log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
									log.add( " There is update made to the address list. The entered first name was : " + sfn);
									
									if(aftAdd.contains(sfn))
									{
										Pass( "The Added First Name is dispalyed.",log);
									}
									else
									{
										Fail( "The Added First Name is not dispalyed.",log);
									}
									
									log.add( " There is update made to the address list.");
									log.add( " There is update made to the address list The displayed new address is : " + aftAdd);
									log.add( " There is update made to the address list. The entered first name was : " + sfn);
									
									if(aftAdd.contains(lsn))
									{
										Pass( "The Added Last Name is dispalyed.",log);
									}
									else
									{
										Fail( "The Added Last Name is not dispalyed.",log);
									}
								}
								
								if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipToName))
								{
									Pass( "The Selected Address Drop Down is displayed.");
									Select sel = new Select(shipandPaypageShipShipToName);
									int size = sel.getOptions().size();
									if(size>1)
									{
										for( int i = 0; i<size; i++)
										{
											String addedAdress = sel.getOptions().get(i).getText();
											log.add(" The displayed address list was : " + addedAdress);
										}
										Pass( "The newly added address is added to the drop down.");
									}
									else
									{
										Fail( "The newly added address is not added to the drop down.");
									}
								}
								else
								{
									Fail( "The Selected Address Drop Down is not displayed.");
								}
							}
							else
							{
								Fail( "The Address Use As entered is not displayed.");
								if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
								{
									jsclick(addressVerificationEdit);
									wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
									Thread.sleep(1000);
									if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddressCancelBtn))
									{
										Pass( "The Cancel Link in Create overlay is displayed.");
										jsclick(shipandPaypageShipShipCreateEditAddressCancelBtn);
										Thread.sleep(1000);
										wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'dijitDialogFixed dijitDialog')][contains(@style,'opacity: 1')]")));
										Thread.sleep(1000);
										if(!HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
										{
											Pass( "The Address Edit Overlay is closed.");
										}
										else
										{
											Fail( "The Address Edit Overlay is not closed.");
										}
									}
									else
									{
										Fail( "The Cancel Link in Create overlay is not displayed.");
									}
								}
								else
								{
									Fail( "The Edit link in the Address Verification overlay is not displayed.");
								}
							}
						}
						else
						{
							Fail( "The Address Verification Overlay is not enabled.");
						}
					}
					else
					{
						Fail( "The Address Verification Overlay is not enabled.");
					}
					
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1312 Issue ." + e.getMessage());
				Exception(" HBC - 1312 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail("The Create Billing Address Overlay is not dispalyed.");
		}
	}
	
	/* HBC - 1315 Guest Checkout - Verify that user should be able to enter first name in the "First Name" field and it should accept maximum of 64 characters.*/
	public void HBC1315ShipPageCreatePayOverlayFNLenVal()
	{
		ChildCreation(" HBC - 1315 Guest Checkout - Verify that user should be able to enter first name in the First Name field and it should accept maximum of 64 characters.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1177", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssFName.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").isEmpty())
							{
								Fail( "The First Name is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssFName.getAttribute("value").length()<=64)
							{
								Pass( "The first name was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The first name was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssFName.clear();
						}
					}
					else
					{
						Fail( "The First Name is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1315 Issue ." + e.getMessage());
				Exception(" HBC - 1315 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1316 Guest Checkout - Verify that user should be able to enter last name in the "Last Name" field and it should accept maximum of 64 characters.*/
	public void HBC1316ShipPageCreatePayOverlayLNLenVal()
	{
		ChildCreation(" HBC - 1316 Guest Checkout - Verify that user should be able to enter last name in the Last Name field and it should accept maximum of 64 characters.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1177", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name is displayed in the Shipping Address Page.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssLName.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").isEmpty())
							{
								Fail( "The Last Name is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssLName.getAttribute("value").length()<=64)
							{
								Pass( "The Last Name  was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Last Name  was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssLName.clear();
						}
					}
					else
					{
						Fail( "The Last Name is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1316 Issue ." + e.getMessage());
				Exception(" HBC - 1316 Issue ." + e.getMessage());
			}
		}
		else
		{
			 Skip( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1317 Guest Checkout - Verify that only characters should be accepted in "First Name" and "Last Name" field.*/
	public void HBC1317FNLNameNumValidation()
	{
		ChildCreation(" HBC - 1317 Guest Checkout - Verify that only characters should be accepted in First Name and Last Name field..");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1179", sheet, 1).split("\n");
				String stAdd = HBCBasicfeature.getExcelVal("HBC1263", sheet, 3);
				String cty = HBCBasicfeature.getExcelVal("HBC1263", sheet, 4);
				String prv = HBCBasicfeature.getExcelVal("HBC1263", sheet, 5);
				String pocode = HBCBasicfeature.getExcelVal("HBC1263", sheet, 6);
				String cNum1 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 7);
				String cNum2 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 8);
				String cNum3 = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 9);
				String ema = HBCBasicfeature.getExcelNumericVal("HBC1263", sheet, 10);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssFName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssFName.click();
						shipandPaypageShipShipCreateEditAddresssFName.clear();
						log.add( "The Value from the excel file was : " + cellVal[0].toString());
						shipandPaypageShipShipCreateEditAddresssFName.sendKeys(cellVal[0]);
					}
					else
					{
						Fail( "The First Name field is not displayed.");
					}
					
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssLName))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssLName.click();
						shipandPaypageShipShipCreateEditAddresssLName.clear();
						log.add( "The Value from the excel file was : " + cellVal[1].toString());
						shipandPaypageShipShipCreateEditAddresssLName.sendKeys(cellVal[1]);
					}
					else
					{
						Fail( "The Last Name field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.click();
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						log.add( "The Value from the excel file was : " + stAdd);
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(stAdd);
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.click();
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						log.add( "The Value from the excel file was : " + cty);
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cty);
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						sel.selectByValue("CA");
						Thread.sleep(500);
						payDefSelCountr = sel.getFirstSelectedOption().getText();
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
							Select sel = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
							sel.selectByValue(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
							shipandPaypageShipShipCreateEditAddresssPayUKState.sendKeys(prv);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					
					if(payDefSelCountr.contains("States"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code field is displayed.");
							shipandPaypageShipShipCreateEditAddresssPOcode.click();
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							log.add( "The Value from the excel file was : " + pocode);
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(pocode);
						}
						else
						{
							Fail( "The Postal Code is not displayed.");
						}
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.click();
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
							log.add( "The Value from the excel file was : " + pocode);
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(pocode);
							Thread.sleep(1000);
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						log.add( "The Value from the excel file was : " + cNum1);
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cNum1);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						shipandPaypageShipShipCreateEditAddresssPH2.clear();
						log.add( "The Value from the excel file was : " + cNum2);
						shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cNum2);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.click();
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						log.add( "The Value from the excel file was : " + cNum3);
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cNum3);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
					{
						Pass( "The Contact Number field is displayed.");
						shipandPaypageShipShipCreateEditEmail.click();
						shipandPaypageShipShipCreateEditEmail.clear();
						log.add( "The Value from the excel file was : " + cNum3);
						shipandPaypageShipShipCreateEditEmail.sendKeys(ema);
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					//WebDriverWait w1 = new WebDriverWait(driver, 10);
					boolean addOverlay = ldNoAddressVerification();
					if(HBCBasicfeature.isElementPresent(addressVerificationEdit))
					{
						jsclick(addressVerificationEdit);
						wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "none"));
						Thread.sleep(1000);
					}
					
					if(addOverlay==true)
					{
						Pass( "The User is not navigated to the Address Verification page.");
					}
					else
					{
						Fail( "The User is navigated to the Address Verification page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1317 Issue ." + e.getMessage());
				Exception(" HBC - 1317 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1318 Guest Checkout - Verify that "Country" field should should be selected in "Canada" option by default.*/
	public void HBC1318ShipPageCreatePayDefaultCountry()
	{
		ChildCreation(" HBC - 1318 Guest Checkout - Verify that Country field should should be selected in Canada option by default.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1276", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String txt = sel.getFirstSelectedOption().getText().replaceAll("\\s", "");
						log.add( "The Expected Value was : " + expVal);
						log.add( "The Actual Value is : " + txt);
						if(txt.isEmpty())
						{
							Fail( "The Country Selection seems to be empty.");
						}
						else if(txt.equalsIgnoreCase(expVal))
						{
							Pass( " The Default Selected country is as expected.",log);
						}
						else
						{
							Fail( "There is mismatch in the expected and the actual default country selection.",log);
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1318 Issue ." + e.getMessage());
				Exception(" HBC - 1318 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1319 Guest Checkout - Billing Address Overlay/Page > Verify that "Province", "Postal Code" texboxes gets hidden and "State", "Zip Code" text boxes is shown on clicking "United States" option in the country dropdown.*/
	/* HBC - 1328 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to select state from the "State" drop down, when the country is selected in "United States".*/
	/* HBC - 1329 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter zip code details in the "Zip Code" field, when the country is selected in "United States".*/
	/* HBC - 1330 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter maximum of 5 numbers in the "Zip Code" field as per the classic site.*/
	/* HBC - 1331 Guest Checkout - Billing Address Overlay/Page > Verify that "Zip Code" field should accept numbers and special characters and the corresponding alert message should be shown as per the classic site.*/
	/* HBC - 1332 Guest Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 5 digits in the "Zip Code" field as per the classic site.*/
	public void HBC1319ShipPageCreateChangeCountry()
	{
		ChildCreation(" HBC - 1319 Guest Checkout - Billing Address Overlay/Page > Verify that Province, Postal Code texboxes gets hidden and State, Zip Code text boxes is shown on clicking United States option in the country dropdown.");
		boolean HBC1328 = false, HBC1329 = false, HBC1332 = false; 
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("States");
						if(cnty==false)
						{
							sel.selectByValue("US");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("States"))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							Fail( "The Province Drop Down box is displayed.");
						}
						else
						{
							Pass( "The Province Drop Down box is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Fail( "The Postal Code Field is displayed.");
						}
						else
						{
							Pass( "The Postal Code Field box is not displayed.");
							
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState))
						{
							Pass( "The State Drop Down box is displayed.");
							Select stSel = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
							String orignialTxt = stSel.getFirstSelectedOption().getText();
							log.add( "The Original State before selection was : " + orignialTxt);
							stSel.selectByValue("NY");
							String seleSt = stSel.getFirstSelectedOption().getText();
							log.add( "The State after selection was : " + seleSt);
							if(orignialTxt.equals(seleSt))
							{
								Fail( "The Original State before and after selection seems to be same.",log);
							}
							else
							{
								Pass( "The Original State before and after selection seems to be different.",log);
								HBC1328 = true;
							}
						}
						else
						{
							Fail( "The State Drop Down box is not displayed.");
						}
						
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
						{
							Pass( "The Postal Code Field box is displayed.");
							String zpval = HBCBasicfeature.getExcelNumericVal("HBC1202", sheet, 1);
							
							shipandPaypageShipShipCreateEditAddresssPOcode.click();
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							log.add( "The Entered value from the excel file is : " + zpval);
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(zpval);
							String zpVal = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value");
							
							if(zpVal.isEmpty())
							{
								Fail( "The Zip Code  field is empty.",log);
							}
							else if(zpval.equalsIgnoreCase(zpVal))
							{
								Pass( "The Zip Code was enterable and it matches.",log);
								HBC1329 = true;
							}
							else
							{
								Fail( "The Zip Code was enterable and it does not matches.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						}
						else
						{
							Fail( "The Province Drop Down box is not displayed.");
						}
					}
					else
					{
						Fail( "The User failed to load the Country United States.");
					}
					
					/*if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country is displayed in the Shipping Address Page.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						sel.selectByValue("CA");
						Thread.sleep(500);
						payDefSelCountr = sel.getFirstSelectedOption().getText();
					}*/
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1319 Issue ." + e.getMessage());
				Exception(" HBC - 1319 Issue ." + e.getMessage());
			}
			
			/* HBC - 1328 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to select state from the "State" drop down, when the country is selected in "United States".*/
			ChildCreation(" HBC - 1328 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to select state from the State drop down, when the country is selected in United States");
			try
			{
				log.add( "Please refer HBC - 1319 log for details.");
				if(HBC1328==true)
				{
					Pass( "The Original State before and after selection seems to be different.",log);
				}
				else
				{
					Fail( "The Original State before and after selection seems to be same.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1328 Issue ." + e.getMessage());
				Exception(" HBC - 1328 Issue ." + e.getMessage());
			}
			
			/* HBC - 1329 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter zip code details in the "Zip Code" field, when the country is selected in "United States".*/
			ChildCreation(" HBC - 1329 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter zip code details in the Zip Code field, when the country is selected in United States");
			try
			{
				log.add( "Please refer HBC - 1319 log for details.");
				if(HBC1329==true)
				{
					Pass( "The Zip Code was enterable and it matches.",log);
				}
				else
				{
					Fail( "The Zip Code was enterable and it does not matches.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1329 Issue ." + e.getMessage());
				Exception(" HBC - 1329 Issue ." + e.getMessage());
			}
			
			/* HBC - 1330 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter maximum of 5 numbers in the "Zip Code" field as per the classic site.*/
			ChildCreation("HBC - 1330 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter maximum of 5 numbers in the Zip Code field as per the classic site.");
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1203", sheet, 1).split("\n");
				Select sel;;
				if(HBCBasicfeature.isElementPresent(bcountrySelection))
				{
					sel = new Select(bcountrySelection);
					String val = sel.getFirstSelectedOption().getText();
					boolean cnty = val.contains("States");
					if(cnty==false)
					{
						sel.selectByValue("US");
						wait.until(ExpectedConditions.visibilityOf(bState));
					}
				}
				
				if(payDefSelCountr.contains("States"))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code Area is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal[i]);
							int currVal = shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").length();
							log.add( "The Current length in the PO Field is  : " + currVal);
							if(shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").isEmpty())
							{
								Fail( "The Zip Code field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPOcode.getAttribute("value").length()<=5)
							{
								Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						}
					}
					else
					{
						Fail( "The Province Drop Down box is not displayed.");
					}
				}
				else
				{
					Fail( "The User failed to load the Country United States.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1330 Issue ." + e.getMessage());
				Exception(" HBC - 1330 Issue ." + e.getMessage());
			}
			
			/* HBC - 1331 Guest Checkout - Billing Address Overlay/Page > Verify that "Zip Code" field should accept numbers and special characters and the corresponding alert message should be shown as per the classic site.*/
			ChildCreation(" HBC - 1331 Guest Checkout - Billing Address Overlay/Page > Verify that Zip Code field should accept numbers and special characters and the corresponding alert message should be shown as per the classic site.");
			try
			{
				String cellVal = HBCBasicfeature.getExcelVal("HBC1204", sheet, 1);
				String alert = HBCBasicfeature.getExcelVal("HBC1204", sheet, 2);

				if(payDefSelCountr.contains("States"))
				{
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPOcode))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPOcode.click();
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The expected alert was : " + alert.toString());
						shipandPaypageShipShipCreateEditAddresssPOcode.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						String poAlert = checkoutBPOAlert.getText();
						log.add( "The actual alert was : " + poAlert);
						if(poAlert.equalsIgnoreCase(alert.toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
							HBC1332 = true;
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPOcode.clear();
					}
					else
					{
						Fail( "The Province Drop Down box is not displayed.");
					}
				}
				else
				{
					Fail( "The User failed to select the drop down field.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1331 Issue ." + e.getMessage());
				Exception(" HBC - 1331 Issue ." + e.getMessage());
			}
			
			/* HBC - 1332 Guest Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 5 digits in the "Zip Code" field as per the classic site.*/
			ChildCreation(" HBC - 1332 Guest Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 5 digits in the Zip Code field as per the classic site.");
			try
			{
				log.add( "Please refer HBC - 1331 log for details.");
				if(HBC1332==true)
				{
					Pass( "The Zip Code alert was as expected.",log);
				}
				else
				{
					Fail( "The Zip Code alert was not as expected.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1332 Issue ." + e.getMessage());
				Exception(" HBC - 1332 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1320 Guest Checkout - Billing Address Overlay/Page > Verify that "Street Address" field should have two text boxes.*/
	public void HBC1320ShipPageCreatePayStreetAddressFields()
	{
		ChildCreation(" HBC - 1320 Guest Checkout - Billing Address Overlay/Page > Verify that Street Address field should have two text boxes.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Street Address is not displayed in the Shipping Address Page.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress2))
					{
						Pass( "The 2 Street Address is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The 2 Street Address is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1320 Issue ." + e.getMessage());
				Exception(" HBC - 1320 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1321 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter street address in the "Street Address" field.*/
	public void HBC1321StAddFieldsLenVal()
	{
		ChildCreation(" HBC - 1321 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter street address in the Street Address field.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String st1 = HBCBasicfeature.getExcelVal("HBC1321", sheet, 1);
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
						log.add( "The Entered value from the excel file is : " + st1);
						shipandPaypageShipShipCreateEditAddresssStAddress.sendKeys(st1);
						String curVal = shipandPaypageShipShipCreateEditAddresssStAddress.getAttribute("value");
						log.add( "The Current value in the field is : " + curVal);
						if(curVal.isEmpty())
						{
							Fail( "The Street Address is empty.");
						}
						else if(curVal.equals(st1))
						{
							Pass( "The Street Address was enterable and it is matches the exact content.",log);
						}
						else
						{
							Fail( "The Street Address was enterable but it is there is some mismatch.",log);
						}
						shipandPaypageShipShipCreateEditAddresssStAddress.clear();
					}
					else
					{
						Fail( "The Street Address field is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssStAddress2))
					{
						Pass( "The Street Address field is displayed.");
						shipandPaypageShipShipCreateEditAddresssStAddress2.clear();
						log.add( "The Entered value from the excel file is : " + st1);
						shipandPaypageShipShipCreateEditAddresssStAddress2.sendKeys(st1);
						String curVal = shipandPaypageShipShipCreateEditAddresssStAddress2.getAttribute("value");
						log.add( "The Current value in the field is : " + curVal);
						if(curVal.isEmpty())
						{
							Fail( "The Street Address is empty.");
						}
						else if(curVal.equals(st1))
						{
							Pass( "The Street Address was enterable and it is matches the exact content.",log);
						}
						else
						{
							Fail( "The Street Address was enterable but it is there is some mismatch.",log);
						}
						shipandPaypageShipShipCreateEditAddresssStAddress2.clear();
					}
					else
					{
						Fail( "The Street Address 2 field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1321 Issue ." + e.getMessage());
				Exception(" HBC - 1321 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1322 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter characters in the "City" field.*/
	public void HBC1322BillCityEnterable()
	{
		ChildCreation(" HBC - 1322 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter characters in the City field.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String cityy = HBCBasicfeature.getExcelVal("HBC1185", sheet, 1);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCty))
					{
						Pass( "The City field is displayed.");
						shipandPaypageShipShipCreateEditAddresssCty.click();
						shipandPaypageShipShipCreateEditAddresssCty.clear();
						log.add( "The Entered value from the excel file is : " + cityy);
						shipandPaypageShipShipCreateEditAddresssCty.sendKeys(cityy);
						String ctyVal = city.getAttribute("value");
						
						if(ctyVal.isEmpty())
						{
							Fail( "The city field is empty.",log);
						}
						else
						{
							Pass( "The city name was enterable.",log);
						}
						shipandPaypageShipShipCreateEditAddresssCty.clear();
					}
					else
					{
						Fail( "The City field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1322 Issue ." + e.getMessage());
				Exception(" HBC - 1322 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1324 Guest Checkout - Billing Address Overlay/Page > Verify that "Country" field should have a maximum of 3 dropdown options.*/
	public void HBC1324BillingCountryOptions()
	{
		ChildCreation(" HBC - 1324 Guest Checkout - Billing Address Overlay/Page > Verify that Country field should have a maximum of 3 dropdown options.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Pass( "The Country field is displayed.");
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						int cntysize = sel.getOptions().size();
						for( int i = 0; i<cntysize;i++)
						{
							String cnty = sel.getOptions().get(i).getText();
							log.add( "The Displayed country option is  : " + cnty);
						}
						if(cntysize==3)
						{
							Pass( "The Country Default Options is of 3 Sizes.",log);
						}
						else
						{
							Fail( "The Country Default Options is of not the expected 3.The displayed total country sizes are : " + cntysize, log);
						}
					}
					else
					{
						Fail( "The Country field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1324 Issue ." + e.getMessage());
				Exception(" HBC - 1324 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1325 Guest Checkout - Billing Address Overlay/Page > Verify that "Province" dropdown field should be shown, when the country is selected in Canada.*/
	public void HBC1325BillingCADefaultStateSel()
	{
		ChildCreation(" HBC - 1325 Guest Checkout - Billing Address Overlay/Page > Verify that Province dropdown field should be shown, when the country is selected in Canada.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1199", sheet, 1);
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					else
					{
						Fail( "The Country is not displayed in the Shipping Address Page.");
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayNonUsStateLabel))
						{
							Pass( "The Province Label is displayed.");
							boolean sel = shipandPaypageShipShipCreateEditAddresssPayNonUsStateLabel.getAttribute("style").contains("block");
							if(sel==true)
							{
								Pass( "The Province Drop Down is selected.");
								log.add( "The expected Value was : " + expVal);
								String txt = shipandPaypageShipShipCreateEditAddresssPayNonUsStateLabel.getText().replaceAll("\\s", "");
								log.add( "The Actual Value was : " + txt);
								if(txt.contains(expVal))
								{
									Pass( " The Expected and actual label matches.",log);
								}
								else
								{
									Fail( " The Expected and actual label does not matches.",log);
								}
							}
							else
							{
								Fail( " The Province Drop Dwon is not selected.");
							}
						}
						else
						{
							Fail( " The Province label is not displayed.");
						}
					}
					else
					{
						Fail( "The User failed to select the CANADA Country.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1325 Issue ." + e.getMessage());
				Exception(" HBC - 1325 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1326 Guest Checkout - Billing Address Overlay/Page > Verify that "Select" option should be selected by default in the "Province" dropdown field.*/
	public void HBC1326ShippingCADefaultStateSel()
	{
		ChildCreation(" HBC - 1326 Guest Checkout - Billing Address Overlay/Page > Verify that Province dropdown field should be shown, when the country is selected in Canada.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String expVal = HBCBasicfeature.getExcelVal("HBC1277", sheet, 1);
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(payDefSelCountr.contains("Canada")||(payDefSelCountr.contains("States")))
					{
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayState)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayNonUsStateLabel)))
						{
							Pass( "The Province Drop Down is displayed.");
							boolean sel = shipandPaypageShipShipCreateEditAddresssPayNonUsStateLabel.getAttribute("style").contains("block");
							if(sel==true)
							{
								Pass( "The Province Drop Down is selected.");
								Select sell = new Select(shipandPaypageShipShipCreateEditAddresssPayState);
								String actVal = sell.getFirstSelectedOption().getText();
								log.add( "The Expected Value was : " + expVal);
								log.add( "The Actual Value is : " + actVal);
								if(expVal.equalsIgnoreCase(actVal))
								{
									Pass( "The Expected Default Value is selected.",log);
								}
								else
								{
									Fail( "The Expected Default Value is not selected.",log);
								}
							}
							else
							{
								Fail( " The Province Drop Dwon is not selected.");
							}
						}
						else
						{
							Fail( " The Province label is not displayed.");
						}
					}
					else
					{
						Fail( "The User failed to select the CANADA Country.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1326 Issue ." + e.getMessage());
				Exception(" HBC - 1326 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1327 Guest Checkout - Billing Address Overlay/Page > Verify that "Province" dropdown field should be changed to textbox field, when the country is selected in "United Kingdom".*/
	public void HBC1327BillingUKStateText()
	{
		ChildCreation(" HBC - 1327 Guest Checkout - Billing Address Overlay/Page > Verify that Province dropdown field should be changed to textbox field, when the country is selected in United Kingdom");
		if(shipPage==true)
		{
			try
			{
				
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Kingdom");
						if(cnty==false)
						{
							sel.selectByValue("GB");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayUKState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(payDefSelCountr.contains("Kingdom"))
					{
						Pass( "The Desired Country was Selected.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPayUKState))
						{
							Pass( "The State Field is displayed in the Shipping Address Page.");
							jsclick(shipandPaypageShipShipCreateEditAddresssPayUKState);
							if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
							{
								Pass( "The Country is displayed in the Shipping Address Page.");
								Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
								sel.selectByValue("CA");
								Thread.sleep(500);
								payDefSelCountr = sel.getFirstSelectedOption().getText();
							}
						}
						else
						{
							Fail( "The State Field is not displayed in the Shipping Address Page.");
						}
					}
					else
					{
						Fail( "The Desired Country was not selected.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1327 Issue ." + e.getMessage());
				Exception(" HBC - 1327 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1333 Guest Checkout - Billing Address Overlay/Page > Verify that "Postal Code" field should accept alphanumeric characters.*/
	public void HBC1333ShippingCAPOAlphaNumericValidation()
	{
		ChildCreation("HBC - 1333 Guest Checkout - Billing Address Overlay/Page > Verify that Postal Code field should accept alphanumeric characters.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1209", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(payDefSelCountr.contains("Canada"))
					{
						Pass( "The Desired Country was Selected.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
						{
							Pass( "The Postal Code Field box is not displayed.");
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.click();
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal.toString());
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(cellVal);
							String val = shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value");
							log.add( "The Current value in the field is : " + val);
							if(val.isEmpty())
							{
								Fail( "The User was not able to enter any value in the Postal Code field of the shipping address.");
							}
							else if(val.equals(cellVal))
							{
								Pass( "The User able to enter value in the Postal Code field of the shipping address and it matches.",log);
							}
							else
							{
								Fail( "The User was able to enter value in the Postal Code field of the shipping address and it does not matches.",log);
							}
							
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
						}
						else
						{
							Fail( "The Postal Code Field is displayed.");
							
						}
					}
					else
					{
						Fail( "The Desired Country was not selected.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1333 Issue ." + e.getMessage());
				Exception(" HBC - 1333 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1334 Guest Checkout - Billing Address Overlay/Page > Verify that "Postal Code" field should accept maximum of 7 characters.*/
	public void HBC1334BillingCAPOLength()
	{
		ChildCreation(" HBC - 1334 Guest Checkout - Billing Address Overlay/Page > Verify that Postal Code field should accept maximum of 7 characters.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1207", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
					{
						Pass( "The Postal Code Area is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(cellVal[i]);
							String val = shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value");
							log.add( "The Current value in the field is : " + val);
							if(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value").isEmpty())
							{
								Fail( "The Zip Code field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.getAttribute("value").length()<=7)
							{
								Pass( "The Zip Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Zip Code was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
						}
					}
					else
					{
						Fail( "The Postal Code Area is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1334 Issue ." + e.getMessage());
				Exception(" HBC - 1334 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1335 Guest Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 6 digits in the "Postal Code" field.*/
	/* HBC - 1336 Guest Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered alphabets or numbers in the "Postal Code" field as per the classic site.*/
	public void HBC1335BillingCAPOLengthValidationAlert()
	{
		ChildCreation(" HBC - 1335 Guest Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered less than 6 digits in the Postal Code field.");
		boolean HBC1336 = false;
		if(shipPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1208", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						Select sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					String alert = HBCBasicfeature.getExcelVal("HBC1208", sheet, 2);
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode))
					{
						Pass( "The First Name field is displayed.");
						shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.click();
						shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The expected alert was : " + alert.toString());
						shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH1.click();
						wait.until(ExpectedConditions.attributeContains(checkoutBPOAlert,"style","block"));
						Thread.sleep(1000);
						String poAlert = checkoutBPOAlert.getText();
						log.add( "The actual alert was : " + poAlert);
						if(poAlert.equalsIgnoreCase(alert.toString()))
						{
							Pass( "The Zip Code alert was as expected.",log);
							HBC1336 = true;
						}
						else
						{
							Fail( "The Zip Code alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssNonUsUkPOcode.clear();
					}
					else
					{
						Fail( "The Postal Code field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1335 Issue ." + e.getMessage());
				Exception(" HBC - 1335 Issue ." + e.getMessage());
			}
			
			/* HBC - 1336 Guest Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered alphabets or numbers in the "Postal Code" field as per the classic site.*/
			ChildCreation(" HBC - 1336 Guest Checkout - Billing Address Overlay/Page > Verify that required alert message should be shown, when the user entered alphabets or numbers in the Postal Code field as per the classic site.");
			try
			{
				log.add( "Refer log 1335 for reports.");
				if(HBC1336==true)
				{
					Pass( "The Zip Code alert was as expected.",log);
				}
				else
				{
					Fail( "The Zip Code alert was not as expected.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1336 Issue ." + e.getMessage());
				Exception(" HBC - 1336 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1339 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter phone number in the "Phone Number" field and it should accept maximum of 10 numbers.*/
	/* HBC - 1342 Guest Checkout - Billing Address Overlay/Page > Verify that in the "Phone Number" field, the first two text boxes accepts 3 digits and the 3rd text box accepts 4 digits.*/
	public void HBC1339PayCAPHNumLenValidation(String tc)
	{
		String tcId = "";
		if(tc=="HBC-1339")
		{
			tcId = "HBC - 1339 Guest Checkout - Billing Address Overlay/Page > Verify that user should be able to enter phone number in the Phone Number field and it should accept maximum of 10 numbers.";
		}
		else
		{
			tcId = "HBC - 1342 Guest Checkout - Billing Address Overlay/Page > Verify that in the Phone Number field, the first two text boxes accepts 3 digits and the 3rd text box accepts 4 digits.";
		}
		
		ChildCreation(tcId);
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssPayState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
				
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Postal Code Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1284", sheet, 1).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH1.getAttribute("value").length()<=3)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH1.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2))
					{
						Pass( "The Postal Code Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1284", sheet, 2).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH2.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH2.getAttribute("value").length()<=3)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH2.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Contact Number Area is displayed.");
						String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1284", sheet, 3).split("\n");
						for(int i = 0; i<cellVal.length;i++)
						{
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cellVal[i]);
							if(shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").isEmpty())
							{
								Fail( "The Contact Number field is empty.");
							}
							else if(shipandPaypageShipShipCreateEditAddresssPH3.getAttribute("value").length()<=4)
							{
								Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
							}
							shipandPaypageShipShipCreateEditAddresssPH3.clear();
						}
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1339 Issue ." + e.getMessage());
				Exception(" HBC - 1339 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1340 Guest Checkout - Billing Address Overlay/Page > Verify that corresponding alert message should be shown, when the user entered alphabets or special characters in the "Phone Number" field.*/
	public void HBC1340PayCAPHAphaNumericValidationAlert()
	{
		ChildCreation(" HBC - 1340 Guest Checkout - Billing Address Overlay/Page > Verify that corresponding alert message should be shown, when the user entered alphabets or special characters in the Phone Number field.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = HBCBasicfeature.getExcelNumericVal("HBC1285", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
				
					String[] alert = HBCBasicfeature.getExcelVal("HBC1285", sheet, 2).split("\n");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						shipandPaypageShipShipCreateEditAddresssPH1.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert1,"style","block"));
						String ctAlert = checkoutSPHAlert1.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[0].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH1.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
					
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3))
					{
						Pass( "The Postal Code Area is displayed.");
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
						log.add( "The Entered value from the excel file is : " + cellVal.toString());
						log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());
						shipandPaypageShipShipCreateEditAddresssPH3.sendKeys(cellVal);
						shipandPaypageShipShipCreateEditAddresssPH2.click();
						wait.until(ExpectedConditions.attributeContains(checkoutSPHAlert3,"style","block"));
						String ctAlert = checkoutSPHAlert3.getText();
						
						if(ctAlert.equalsIgnoreCase(alert[1].toString()))
						{
							Pass( "The Phone Number alert was as expected.",log);
						}
						else
						{
							Fail( "The Phone Number alert was not as expected.",log);
						}
						shipandPaypageShipShipCreateEditAddresssPH3.clear();
					}
					else
					{
						Fail( "The Contact Number is not displayed.");
					}
				}
				else
				{
					Fail("The Create Shipping Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1340 Issue ." + e.getMessage());
				Exception(" HBC - 1340 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1341 Guest Checkout - Billing Address Overlay/Page > Verify that "Phone Number" field should have three text boxes as per the classic site.*/
	public void HBC1341ShipPageCreatePayPHTextBoxes()
	{
		ChildCreation(" HBC - 1341 Guest Checkout - Billing Address Overlay/Page > Verify that Phone Number field should have three text boxes as per the classic site.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH1)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH2)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH3)&&(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssPH4)))))
					{
						Pass( "The Phone Code Field is displayed in the Shipping Address Page.");
					}
					else
					{
						Fail( "The Phone Code is not displayed in the Shipping Address Page.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1341 Issue ." + e.getMessage());
				Exception(" HBC - 1341 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1343 Guest Checkout - Billing Address Overlay/Page > Verify that "ext." field should accept maximum of 5 digits.*/
	public void HBC1343BillingCAExtNumLenValidation()
	{
		ChildCreation(" HBC - 1343 Guest Checkout - Billing Address Section > Verify that ext. field should accept maximum of 5 digits.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1212", sheet, 1).split("\n");
					Select sel;
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditAddresssCountry))
					{
						sel = new Select(shipandPaypageShipShipCreateEditAddresssCountry);
						String val = sel.getFirstSelectedOption().getText();
						boolean cnty = val.contains("Canada");
						if(cnty==false)
						{
							sel.selectByValue("CA");
							wait.until(ExpectedConditions.visibilityOf(shipandPaypageShipShipCreateEditAddresssState));
							payDefSelCountr = sel.getFirstSelectedOption().getText();
						}
					}
					
					if(HBCBasicfeature.isElementPresent(bextCode))
					{
						Pass( "The Extension Code Area is displayed.");
						for(int i = 0; i<cellVal.length;i++)
						{
							bextCode.clear();
							log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
							log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
							bextCode.sendKeys(cellVal[i]);
							if(bextCode.getAttribute("value").isEmpty())
							{
								Fail( "The Extension Code field is empty.");
							}
							else if(bextCode.getAttribute("value").length()<=5)
							{
								Pass( "The Extension Code was enterable and it is within the specified character limit.",log);
							}
							else
							{
								Fail( "The Extension Code was enterable but it is not within the specified character limit.",log);
							}
							bextCode.clear();
						}
					}
					else
					{
						Fail( "The Extension Code is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1343 Issue ." + e.getMessage());
				Exception(" HBC - 1343 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1344 Guest Checkout - Billing Address Section > Verify that user should be able to enter email in the "Email:" field and it should accept both alphanumerics and characters.*/
	public void HBC1344ShipPageBillingValidEmail()
	{
		ChildCreation(" HBC - 1344 Guest Checkout - Billing Address Section > Verify that user should be able to enter email in the Email: field and it should accept both alphanumerics and characters.");
		if(shipPage==true)
		{
			try
			{
				String zpval = HBCBasicfeature.getExcelNumericVal("HBC1216", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditEmail))
					{
						Pass( "The Email Field is displayed.");
						shipandPaypageShipShipCreateEditEmail.click();
						shipandPaypageShipShipCreateEditEmail.clear();
						log.add( "The Entered value from the excel file is : " + zpval);
						shipandPaypageShipShipCreateEditEmail.sendKeys(zpval);
						String emVal = shipandPaypageShipShipCreateEditEmail.getAttribute("value");
						
						if(emVal.isEmpty())
						{
							Fail( "The Email Field field is empty.",log);
						}
						else if(zpval.equalsIgnoreCase(emVal))
						{
							Pass( "The Email Field was enterable and it matches.",log);
						}
						else
						{
							Fail( "The Email Field  was enterable and it does not matches.",log);
						}
						shipandPaypageShipShipCreateEditEmail.clear();
					}
					else
					{
						Fail( "The Email Field is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1344 Issue ." + e.getMessage());
				Exception(" HBC - 1344 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1347 Guest Checkout - Billing Address Overlay/Page > Verify that "I agree to receive electronic messages from Hudson's Bay..." check box should be unchecked by default.*/
	public void HBC1347BillingTermandConditionChkBoxState()
	{
		ChildCreation(" HBC - 1347 Guest Checkout - Billing Address Overlay/Page > Verify that I agree to receive electronic messages from Hudson's Bay... check box should be unchecked by default.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipCreateEditAddressOverlay))
				{
					Pass("The Create Shipping Address Overlay is dispalyed.");
					if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditTermsandCond))
					{
						Pass( "The Terms and Condition link is displayed.");
						if(HBCBasicfeature.isElementPresent(shipandPaypageShipShipCreateEditTermsandCondChkBox))
						{
							Pass( "The Checkbox is displayed.");
							boolean chkd = shipandPaypageShipShipCreateEditTermsandCondChkBox.getAttribute("value").contains("0");
							if(chkd==true)
							{
								Pass( "The Checkbox is in Unchecked Mode.");
							}
							else
							{
								Fail( "The Checkbox is not in Unchecked Mode.");
							}
						}
						else
						{
							Fail( "The Checkbox is not displayed.");
						}
					}
					else
					{
						Fail( "The Terms and Condition link is not displayed.");
					}
				}
				else
				{
					Fail("The Create Billing Address Overlay is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1347 Issue ." + e.getMessage());
				Exception(" HBC - 1347 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1348 Guest Checkout - Payment Information Section > Verify that "Pay by Credit Card", "Pay by PayPal" radio tab should be shown below the payment information section.*/
	public void HBC1348PaymentSectionsOptions()
	{
		ChildCreation("HBC - 1348 Guest Checkout - Payment Information Section > Verify that Pay by Credit Card, Pay by PayPal radio tab should be shown below the payment information section.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCard))
				{
					HBCBasicfeature.scrolldown(paymentOptionsCreditCard, driver);
					Pass( "The Credit Card payment Checkbox is displayed .");
				}
				else
				{
					Fail( "The Credit Card payment Checkbox is not displayed .");
				}
				
				if(HBCBasicfeature.isElementPresent(paymentOptionsPaypal))
				{
					Pass( "The Paypal payment Checkbox is displayed .");
				}
				else
				{
					Fail( "The Paypal payment Checkbox is not displayed .");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1348 Issue ." + e.getMessage());
				Exception(" HBC - 1348 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1349 Guest Checkout - Payment Information Section > Verify that on tapping the "Pay by Credit Card" radio tab the respective fields should be enabled.*/
	public void HBC1349PaymentSectionsCreditCardEnable()
	{
		ChildCreation(" HBC - 1349 Guest Checkout - Payment Information Section > Verify that on tapping the Pay by Credit Card radio tab the respective fields should be enabled.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCard))
				{
					Pass( "The Credit Card payment Checkbox is displayed .");
					jsclick(paymentOptionsCreditCardChkkBox);
					//paymentOptionsCreditCardChkkBox.click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(paymentOptionsCreditCardSelected));
					Thread.sleep(500);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
					{
						Pass( "The Credit Card option was selected.");
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecEnable, "style", "block"));
						Thread.sleep(1000);
						boolean chkd = paymentOptionsCreditCardSecEnable.getAttribute("style").contains("block");
						if(chkd==true)
						{
							Pass( "The Credit Card Section is Enabled.");
						}
						else
						{
							Fail( "The Credit Card Section is not Enabled.");
						}
					}
					else
					{
						Fail( "The Credit Card option was not selected.");
					}
				}
				else
				{
					Fail( "The Credit Card payment Checkbox is not displayed .");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1349 Issue ." + e.getMessage());
				Exception(" HBC - 1349 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1350 Guest Checkout - Payment Information Section > Verify that "Pay by card" section should display "Card type" dropdown field in the "Payment" column.*/
	public void HBC1350PaymentSectionsCreditCardFields()
	{
		ChildCreation(" HBC - 1350 HBC - 1350 Guest Checkout - Payment Information Section > Verify that Pay by card section should display Card type dropdown field in the Payment column.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
				{
					Pass( "The Credit Card option was selected.");
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
					{
						Pass( "The Credit Card Drop Down is displayed.");
					}
					else
					{
						Fail( "The Credit Card Drop Down is not displayed.");
					}
				}
				else
				{
					Fail( "The Credit Card option was not selected.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1350 Issue ." + e.getMessage());
				Exception(" HBC - 1350 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1352 Guest Checkout - Payment Information Section > Verify that user should be able to choose the option in the "Card type" dropdown field in the "Payment" column.*/
	public void HBC1352PaymentSectionsCreditCardSelection()
	{
		ChildCreation(" HBC - 1352 Guest Checkout - Payment Information Section > Verify that user should be able to choose the option in the Card type dropdown field in the Payment column.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
				{
					Pass( "The Credit Card option was selected.");
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
					{
						Pass( "The Credit Card Drop Down is displayed.");
						Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
						String actual = sel.getFirstSelectedOption().getText();
						log.add("The selected by default option is: "+actual);
						int size = sel.getOptions().size();
						if(size>1)
						{
							sel.getOptions().get(1).click();
							Thread.sleep(3000);
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
							Thread.sleep(1000);
							String current = sel.getFirstSelectedOption().getText();
							log.add("The selected by default option is: "+actual);
							log.add("The Current selected option  is: "+current);
							if(actual.equalsIgnoreCase(current))
							{
								Fail("user not able to choose the option in the 'Card type' dropdown field in the 'Payment' column",log);
							}
							else
							{
								Pass("user able to choose the option in the 'Card type' dropdown field in the 'Payment' column",log);
							}							
						}
						else
						{
							Fail("Drop down size is not more than 1");
						}
					}
					else
					{
						Fail( "The Credit Card Drop Down is not displayed.");
					}
				}
				else
				{
					Fail( "The Credit Card option was not selected.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1352 Issue ." + e.getMessage());
				Exception(" HBC - 1352 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1353 Guest Checkout - Payment Information Section > Verify that credit card images should be shown in the "Pay by credit card" section under "Payment" column.*/
	public void HBC1353PaymentSectionsCreditCardImages()
	{
		ChildCreation(" HBC - 1353 Guest Checkout - Payment Information Section > Verify that credit card images should be shown in the Pay by credit card section under Payment column.");
		if(shipPage==true)
		{
			try
			{
				int ctr = 0;
				if(HBCBasicfeature.isListElementPresent(paymentOptionsCreditCardImages))
				{
					Pass("Credit Card Images are displayed");
					int size = paymentOptionsCreditCardImages.size();
					for(int i=1;i<=size;i++)
					{
						Thread.sleep(200);
						boolean image = driver.findElement(By.xpath("//*[@id='CreditCardList']//li["+i+"]")).isDisplayed();
						if(image==true)
						{
							ctr++;
						}
					}
					if(size==ctr)
					{
						Pass("Credit card images shown in the 'Pay by credit card' section",log);
					}
					else
					{
						Fail("Credit card images not shown in the 'Pay by credit card' section",log);
					}
				}
				else
				{
					Fail("Credit Card Images are not displayed");
				}					
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1353 Issue ." + e.getMessage());
				Exception(" HBC - 1353 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1354 Guest Checkout - Payment Information Section > Verify that "Amount to pay by credit card: $XXX.XX" text should be shown in the "Pay by credit card" section under "Payment" column.*/
	public void HBC1354PaymentSectionsCreditCardAmountToPay()
	{
		ChildCreation(" HBC - 1354 Guest Checkout - Payment Information Section > Verify that Amount to pay by credit card: $XXX.XX text should be shown in the Pay by credit card section under Payment column.");
		if(shipPage==true)
		{
			try
			{
				String expctd = HBCBasicfeature.getExcelVal("HBC1354", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardAmountToPaylabel)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardAmountToPayPrice))
				{
					String actual = paymentOptionsCreditCardAmountToPaylabel.getText();
					String actualval = paymentOptionsCreditCardAmountToPayPrice.getText();
					Thread.sleep(500);
					log.add("The selected by default option is: "+actual);
					log.add("The Current selected option  is: "+expctd);
					log.add("The Price amount displayed is: "+actualval);
					Thread.sleep(500);
					if(actual.equalsIgnoreCase(expctd)&&(actualval.contains("$"))&&(actualval.matches(".*\\d+.*")))
					{
						Pass("'Amount to pay by credit card:  $XXX.XX' text shown in the 'Pay by credit card' section",log);
					}
					else
					{
						Fail("'Amount to pay by credit card: $XXX.XX 'text not shown in the 'Pay by credit card' section",log);
					}					
				}
				else
				{
					Fail("'Amount to pay by credit card: ' label and price not displayed");
				}				
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1354 Issue ." + e.getMessage());
				Exception(" HBC - 1354 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1355 Guest Checkout - Payment Information Section > Verify that "Card type" dropdown field should be selected in "Select" option by default in "Payment" column.*/
	public void HBC1355PaymentSectionsCreditCardDropdownDefault()
	{
		ChildCreation(" HBC - 1355 Guest Checkout - Payment Information Section > Verify that Card type dropdown field should be selected in Select option by default in Payment column.");
		if(shipPage==true)
		{
			try
			{
				String expctd = HBCBasicfeature.getExcelVal("HBC1355", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
				{
					Pass("Credit type drop down is displayed");
					Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
					String actual = sel.getFirstSelectedOption().getText();
					log.add("The selected by default option is: "+actual);
					log.add("The expected option  is: "+expctd);
					Thread.sleep(200);
					if(actual.equalsIgnoreCase(expctd))
					{
						Pass(" 'Card type' dropdown field selected in 'Select' option by default in 'Payment' column",log);
					}
					else
					{
						Fail("'Card type' dropdown field not selected in 'Select' option by default in 'Payment' column",log);
					}
				}
				else
				{
					Fail("'Card type' dropdown field not displayed");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1355 Issue ." + e.getMessage());
				Exception(" HBC - 1355 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1356 Guest Checkout - Payment Information Section > Verify that "Card number" textbox field should be shown on selecting "HBC Credit Card" option in the "Card type" dropdown field.*/
	public void HBC1356PaymentSectionsCreditCardNumberField()
	{
		ChildCreation(" HBC - 1356 Guest Checkout - Payment Information Section > Verify that Card number textbox field should be shown on selecting HBC Credit Card option in the Card type dropdown field.");
		if(shipPage==true)
		{
			try
			{
				String expctd = HBCBasicfeature.getExcelVal("HBC1356", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
				{
					Pass("Credit type drop down is displayed");
					Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
					String actual = sel.getFirstSelectedOption().getText();
					Thread.sleep(200);
					log.add("The selected by default option is: "+actual);
					sel.selectByValue("HBC");
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
					String current = sel.getFirstSelectedOption().getText();
					Thread.sleep(200);
					log.add("The Current selected option  is: "+current);
					if(current.equalsIgnoreCase(expctd))
					{
						Pass(" 'HBC Credit Card' option selected");
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
						{
							Pass("'Card number' textbox field shown on selecting 'HBC Credit Card' option ",log);
						}
						else
						{
							Fail("'Card number' textbox field not shown on selecting 'HBC Credit Card' option ",log);
						}						
					}
					else
					{
						Fail("user not selected 'HBC Credit Card' option in the 'Card type' dropdown field",log);
					}
				}
				else
				{
					Fail("Card type drop down is not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1356 Issue ." + e.getMessage());
				Exception(" HBC - 1356 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1357 Guest Checkout - Payment Information Section > Verify that all the "Card number" textbox fields should accept maximum of 16 digits.*/
	public void HBC1357PaymentSectionsCreditCardNumberFieldMaximum()
	{
		ChildCreation(" HBC - 1357 Guest Checkout - Payment Information Section > Verify that all the Card number textbox fields should accept maximum of 16 digits.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1357", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
				{
					Pass("'Card number' textbox field shown ");
					for(int i = 0; i<cellVal.length;i++)
					{
						paymentOptionsCreditCardNumberField.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						paymentOptionsCreditCardNumberField.sendKeys(cellVal[i]);
						Thread.sleep(500);
						if(paymentOptionsCreditCardNumberField.getAttribute("value").isEmpty())
						{
							Fail( "The 'Card number' textbox field is empty.");
						}
						else if(paymentOptionsCreditCardNumberField.getAttribute("value").length()<=16)
						{
							Pass( "The 'Card number' textbox field was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The 'Card number' textbox field was enterable but it is not within the specified character limit.",log);
						}
						paymentOptionsCreditCardNumberField.clear();
					}
				}
				else
				{
					Fail("'Card number' textbox field not shown ");
				}						
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1357 Issue ." + e.getMessage());
				Exception(" HBC - 1357 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1358 Guest Checkout - Payment Information Section > Verify that alert should be shown on navigating to other fields without entering characters in the "Card number" fields.*/
	public void HBC1358PaymentSectionsCreditCardNumberEmptyFieldAlert()
	{
		ChildCreation(" HBC - 1358 Guest Checkout - Payment Information Section > Verify that alert should be shown on navigating to other fields without entering characters in the Card number fields.");
		if(shipPage==true)
		{
			try
			{			
				String expctd = HBCBasicfeature.getExcelVal("HB1358", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
				{
					Pass("'Card number' textbox field shown ");					
					if(paymentOptionsCreditCardNumberField.getAttribute("value").isEmpty())
					{
						log.add("'Card number' textbox field is empty");
						paymentOptionsRewardCardField2.click();
						Thread.sleep(200);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardNumberFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberFieldAlert))
						{
							String actual = paymentOptionsCreditCardNumberFieldAlert.getText().trim();
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+expctd);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(expctd))
							{
								Pass("Alert shown on navigating to other fields without entering characters in the 'Card number' field",log);
							}
							else
							{
								Fail("Alert not shown on navigating to other fields without entering characters in the 'Card number' field",log);
							}			
						}
						else
						{
							Fail("Alert not displayed");
						}
					}
					else
					{
						Fail("Credit card number field not empty");
					}							
				}
				else
				{
					Fail("Credit card number field not displayed");
				}	
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1358 Issue ." + e.getMessage());
				Exception(" HBC - 1358 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1359 Guest Checkout - Payment Information Section > Verify that on selecting other than "HBC Credit Card" option in the "Card type" dropdown field, "Card number", "Security code", "Expiration" text boxes should be shown.*/
	public void HBC1359PaymentSectionsCreditCardFields() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1359 Guest Checkout - Payment Information Section > Verify that on selecting other than HBC Credit Card option in the Card type dropdown field, Card number, Security code, Expiration text boxes should be shown.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardCardSelDropDown))
				{
					Pass("Credit type drop down is displayed");
					HBCBasicfeature.scrolldown(paymentOptionsCreditCardCardSelDropDown, driver);
					Select sel = new Select(paymentOptionsCreditCardCardSelDropDown);
					String actual = sel.getFirstSelectedOption().getText();
					log.add("The selected by default option is: "+actual);
					Thread.sleep(200);
					int size = sel.getOptions().size();
					Random r  = new Random();
					boolean flag = false;
					String current = "";						
					do
					{
						int i = r.nextInt(size);
						if(i<=1)
						{
							flag = false;
						}
						else
						{
							sel.getOptions().get(i).click();
							Thread.sleep(3000);
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
							Thread.sleep(1000);
							current = sel.getFirstSelectedOption().getText();		
							Thread.sleep(200);
							if(!actual.equalsIgnoreCase(current))
							{
								flag = true;
								continue;
							}
						}
					}while(!flag == true);
					log.add("The Current selected option is: "+current);
					Thread.sleep(200);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
					{
						Pass("'Card number' textbox field shown on selecting other than'HBC Credit Card' option ",log);
					}
					else
					{
						Fail("'Card number' textbox field not shown on selecting other than 'HBC Credit Card' option ",log);
					}						

					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
					{
						Pass("'Card number' Security code field shown on selecting other than'HBC Credit Card' option ");
					}
					else
					{
						Fail("'Card number' Security code field not shown on selecting other than 'HBC Credit Card' option ");
					}						
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
					{
						Pass("'Card number'Expiration Month field shown on selecting other than'HBC Credit Card' option ");
					}
					else
					{
						Fail("'Card number' number'Expiration Year field not shown on selecting other than 'HBC Credit Card' option ");
					}			
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
					{
						Pass("'Card number' Security code field shown on selecting other than'HBC Credit Card' option ");
					}
					else
					{
						Fail("'Card number' Security code field not shown on selecting other than 'HBC Credit Card' option ");
					}			
				}
				else
				{
					Fail("Card type drop down is not displayed");
				}					
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1359 Issue ." + e.getMessage());
				Exception(" HBC - 1359 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}	
	
	/* HBC - 1360 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering the special characters or alphabets or invalid numbers in the "Card number" field and on tapping other fields.*/
	public void HBC1360PaymentSectionsCreditCardNumberInvalidFieldAlert() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1360 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering the special characters or alphabets or invalid numbers in the Card number field and on tapping other fields.");
		if(shipPage==true)
		{
			String val = HBCBasicfeature.getExcelVal("HB1360", sheet, 1);
			String valAlert = HBCBasicfeature.getExcelVal("HB1360", sheet, 2);
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberField))
				{						
					Pass("'Card number' textbox field shown ");	
					HBCBasicfeature.scrolldown(paymentOptionsCreditCardNumberField, driver);
					paymentOptionsCreditCardNumberField.clear();
					paymentOptionsCreditCardNumberField.sendKeys(val);
					paymentOptionsRewardCardField2.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardNumberFieldAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardNumberFieldAlert))
					{
						String actual = paymentOptionsCreditCardNumberFieldAlert.getText().trim();
						log.add("The actual alert displayed is: "+actual);
						log.add("The expected alert to be displayed is: "+valAlert);
						Thread.sleep(200);
						if(actual.equalsIgnoreCase(valAlert))
						{
							Pass("Alert shown on entering the special characters or alphabets or invalid numbers in the 'Card number' field",log);
						}
						else
						{
							Fail("Alert not shown on entering the special characters or alphabets or invalid numbers in the 'Card number' field",log);
						}			
					}
					else
					{
						Fail("Alert not displayed");
					}
				}
				else
				{
					Fail("Credit card number field not displayed");
				}							
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1360 Issue ." + e.getMessage());
				Exception(" HBC - 1360 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1363 Guest Checkout - Payment Information Section > Verify that user should be able to enter security code in the "Security Code" field.*/
	public void HBC1363PaymentSectionsCreditCardSecurityCode() 
	{
		ChildCreation(" HBC - 1363 Guest Checkout - Payment Information Section > Verify that user should be able to enter security code in the Security Code field.");
		if(shipPage==true)
		{
			try
			{
				String expctd = HBCBasicfeature.getExcelNumericVal("HBC1363", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
				{
					Pass("'Card number' Security code field displayed");
					paymentOptionsCreditCardSecurityCodeField.clear();
					paymentOptionsCreditCardSecurityCodeField.sendKeys(expctd);
					if(!paymentOptionsCreditCardSecurityCodeField.getAttribute("value").isEmpty())
					{
						String actual = paymentOptionsCreditCardSecurityCodeField.getAttribute("value");
						log.add("The actual displayed value is: "+actual);
						log.add("The expected value is: "+expctd);
						if(actual.equalsIgnoreCase(expctd))
						{
							Pass(" user able to enter security code in the 'Security Code' field",log);
						}
						else
						{
							Fail("user not able to enter security code in the 'Security Code' field",log);
						}			
					}
					else
					{
						Fail("'Security Code' textbox field is empty");
					}
				}
				else
				{
					Fail("'Security Code' Security code field not shown");
				}			
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1363 Issue ." + e.getMessage());
				Exception(" HBC - 1363 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1364 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering the special characters or alphabets or invalid numbers in the "Security Code" field and tapping on other fields.*/
	public void HBC1364PaymentSectionsCreditCardSecurityInvalidFieldAlert() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1364 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering the special characters or alphabets or invalid numbers in the Security Code field and tapping on other fields.");
		if(shipPage==true)
		{
			String val = HBCBasicfeature.getExcelVal("HB1364", sheet, 1);
			String valAlert = HBCBasicfeature.getExcelVal("HB1364", sheet, 2);
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
				{
					Pass("'Security Code' textbox field shown ");					
					paymentOptionsCreditCardSecurityCodeField.clear();
					paymentOptionsCreditCardSecurityCodeField.sendKeys(val);
					paymentOptionsCreditCardExpMonthField.click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecurityCodeFieldAlert, "style", "block"));
					Thread.sleep(500);
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeFieldAlert))
					{
						String actual = paymentOptionsCreditCardSecurityCodeFieldAlert.getText().trim();
						log.add("The actual alert displayed is: "+actual);
						log.add("The expected alert to be displayed is: "+valAlert);
						if(actual.equalsIgnoreCase(valAlert))
						{
							Pass("Alert shown on entering the special characters or alphabets or invalid numbers in the 'Security Code' field",log);
						}
						else
						{
							Fail("Alert not shown on entering the special characters or alphabets or invalid numbers in the 'Security Code' field",log);
						}			
					}
					else
					{
						Fail("Alert not displayed");
					}
				}
				else
				{
					Fail("Security Code field not displayed");
				}							
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1364 Issue ." + e.getMessage());
				Exception(" HBC - 1364 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}	
	
	/* HBC - 1365 Guest Checkout - Payment Information Section > Verify that alert should be shown on navigating to other fields without entering characters in the "Security Code" fields.*/
	public void HBC1365PaymentSectionsCreditCardSecurityEmptyFieldAlert()
	{
		ChildCreation(" HBC - 1365 Guest Checkout - Payment Information Section > Verify that alert should be shown on navigating to other fields without entering characters in the Security Code fields.");
		if(shipPage==true)
		{
			try
			{			
				String expctd = HBCBasicfeature.getExcelVal("HB1365", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
				{
					Pass("'Security Code' textbox field shown ");
					paymentOptionsCreditCardSecurityCodeField.click();
					if(paymentOptionsCreditCardSecurityCodeField.getAttribute("value").isEmpty())
					{
						log.add("'Security Code' textbox field is empty");
						paymentOptionsCreditCardExpMonthField.click();
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecurityCodeFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeFieldAlert))
						{
							String actual = paymentOptionsCreditCardSecurityCodeFieldAlert.getText().trim();
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+expctd);
							if(actual.equalsIgnoreCase(expctd))
							{
								Pass("Alert shown on navigating to other fields without entering characters in the 'Security Code' field",log);
							}
							else
							{
								Fail("Alert not shown on navigating to other fields without entering characters in the 'Security Code' field",log);
							}			
						}
						else
						{
							Fail("Alert not displayed");
						}
					}
					else
					{
						Fail("Security Code field not empty");
					}							
				}
				else
				{
					Fail("Security Code field not displayed");
				}	
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1365 Issue ." + e.getMessage());
				Exception(" HBC - 1365 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1366 Guest Checkout - Payment Information Section > Verify that "Security Code" fields, should accept maximum of 4 digits.*/
	public void HBC1366PaymentSectionsCreditCardSecurityCodeFieldMaximum()
	{
		ChildCreation(" HBC - 1366 Guest Checkout - Payment Information Section > Verify that Security Code fields, should accept maximum of 4 digits.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1366", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSecurityCodeField))
				{
					Pass("'Security code' textbox field shown ");
					for(int i = 0; i<cellVal.length;i++)
					{
						paymentOptionsCreditCardSecurityCodeField.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						paymentOptionsCreditCardSecurityCodeField.sendKeys(cellVal[i]);
						if(paymentOptionsCreditCardSecurityCodeField.getAttribute("value").isEmpty())
						{
							Fail( "The 'Security code' textbox field is empty.");
						}
						else if(paymentOptionsCreditCardSecurityCodeField.getAttribute("value").length()<=4)
						{
							Pass( "The 'Security code' textbox field was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The 'Security code' textbox field was enterable but it is not within the specified character limit.",log);
						}
						paymentOptionsCreditCardSecurityCodeField.clear();
					}
				}
				else
				{
					Fail("'Security code' textbox field not shown ");
				}						
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1366 Issue ." + e.getMessage());
				Exception(" HBC - 1366 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1367 Guest Checkout - Payment Information Section > Verify that user should be able to enter expiration date in the "expiration" field.*/
	public void HBC1367PaymentSectionsCreditCardExpirationMonthYear() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1367 Guest Checkout - Payment Information Section > Verify that user should be able to enter expiration date in the expiration field.");
		if(shipPage==true)
		{
			try
			{
				String expctdMonth = HBCBasicfeature.getExcelNumericVal("HBC1367", sheet, 1);
				String expctdYear = HBCBasicfeature.getExcelNumericVal("HBC1367", sheet, 2);

				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
				{
					Pass("Expiration Month field displayed");
					paymentOptionsCreditCardExpMonthField.clear();
					paymentOptionsCreditCardExpMonthField.sendKeys(expctdMonth);
					if(!paymentOptionsCreditCardExpMonthField.getAttribute("value").isEmpty())
					{
						String actual = paymentOptionsCreditCardExpMonthField.getAttribute("value");
						log.add("The actual displayed value is: "+actual);
						log.add("The expected value is: "+expctdMonth);
						if(actual.equalsIgnoreCase(expctdMonth))
						{
							Pass(" user able to enter Expiration date in the 'expiration' field",log);
						}
						else
						{
							Fail("user not able to enter Expiration date in the 'expiration' field",log);
						}
						paymentOptionsCreditCardExpMonthField.clear();
					}
					else
					{
						Fail("'Expiration month' textbox field is empty");
					}
				}
				else
				{
					Fail("'Expiration month' not displayed ");
				}
				
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
				{
					Pass("Expiration Year field displayed");
					paymentOptionsCreditCardExpYearField.clear();
					paymentOptionsCreditCardExpYearField.sendKeys(expctdYear);
					if(!paymentOptionsCreditCardExpYearField.getAttribute("value").isEmpty())
					{
						String actual = paymentOptionsCreditCardExpYearField.getAttribute("value");
						log.add("The actual displayed value is: "+actual);
						log.add("The expected value is: "+expctdYear);
						if(actual.equalsIgnoreCase(expctdYear))
						{
							Pass(" user able to enter Expiration Year in the 'expiration' field",log);
						}
						else
						{
							Fail("user not able to enter Expiration Year in the 'expiration' field",log);
						}
						paymentOptionsCreditCardExpYearField.clear();
					}
					else
					{
						Fail("'Expiration Year' textbox field is empty");
					}
				}
				else
				{
					Fail("'Expiration Year' not displayed ");
				}			
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1367 Issue ." + e.getMessage());
				Exception(" HBC - 1367 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1368 Guest Checkout - Payment Information Section > Verify that Expiration date format (MM / YYYY) should be shown below the "Expiration Date" field.*/
	public void HBC1368PaymentSectionsCreditCardExpirationMonthYearFormat() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1368 Guest Checkout - Payment Information Section > Verify that Expiration date format (MM / YYYY) should be shown below the Expiration Date field.");
		if(shipPage==true)
		{
			try
			{
				String[] expected = HBCBasicfeature.getExcelVal("HBC1368", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
				{
					Pass("Expiration Month & Year field displayed");
					String actualMonthTxt = paymentOptionsCreditCardExpMonthField.getAttribute("placeholder");
					String actualYearTxt = paymentOptionsCreditCardExpYearField.getAttribute("placeholder");
					log.add("Expected Month field format is: "+expected[0]);
					log.add("Actual Month field format is: "+actualMonthTxt);
					log.add("Expected Year field format is: "+expected[1]);
					log.add("Actual Year field format is: "+actualYearTxt);
					if(actualMonthTxt.equalsIgnoreCase(expected[0])&&actualYearTxt.equalsIgnoreCase(expected[1]))
					{
						Pass("Expiration date format (MM / YYYY) shown below the 'Expiration Date' field",log);
					}
					else
					{
						Fail("Expiration date format (MM / YYYY) not shown below the 'Expiration Date' field",log);
					}
				}
				else
				{
					Fail("Expiration Month & Year field is not displayed");
				}						
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1368 Issue ." + e.getMessage());
				Exception(" HBC - 1368 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1369 Guest Checkout - Payment Information Section > Verify that on entering the special characters or alphabets or invalid numbers in the "Expiration" field and tapping on other fields, alert should be shown*/
	public void HBC1369PaymentSectionsCreditCardExpirationInvalidFieldAlert() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1369 Guest Checkout - Payment Information Section > Verify that on entering the special characters or alphabets or invalid numbers in the Expiration field and tapping on other fields, alert should be shown.");
		if(shipPage==true)
		{
			String monthVal = HBCBasicfeature.getExcelVal("HB1369", sheet, 1);
			String monthAlert = HBCBasicfeature.getExcelVal("HB1369", sheet, 2);
			String yearVal = HBCBasicfeature.getExcelVal("HBC1369", sheet, 1);
			String yearAlert = HBCBasicfeature.getExcelVal("HBC1369", sheet, 2);
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
				{
					Pass("Expiration Month & Year field displayed");
					
					paymentOptionsCreditCardExpMonthField.clear();
					paymentOptionsCreditCardExpMonthField.sendKeys(monthVal);
					paymentOptionsCreditCardExpYearField.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpMonthFieldAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthFieldAlert))
					{
						String actual = paymentOptionsCreditCardExpMonthFieldAlert.getText().trim();
						log.add("The actual alert displayed is: "+actual);
						log.add("The expected alert to be displayed is: "+monthAlert);
						if(actual.equalsIgnoreCase(monthAlert))
						{
							Pass("Alert shown on entering the special characters or alphabets or invalid numbers in the 'Expiration Month' field",log);
						}
						else
						{
							Fail("Alert not shown on entering the special characters or alphabets or invalid numbers in the 'Expiration Month' field",log);
						}
						paymentOptionsCreditCardExpMonthField.clear();
					}
					else
					{
						Fail("Month alert not displayed");
					}
					
					paymentOptionsCreditCardExpYearField.clear();
					paymentOptionsCreditCardExpYearField.sendKeys(yearVal);
					paymentOptionsCreditCardExpMonthField.click();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpYearFieldAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearFieldAlert))
					{
						String actual = paymentOptionsCreditCardExpYearFieldAlert.getText().trim();
						log.add("The actual alert displayed is: "+actual);
						log.add("The expected alert to be displayed is: "+yearAlert);
						Thread.sleep(200);
						if(actual.equalsIgnoreCase(yearAlert))
						{
							Pass("Alert shown on entering the special characters or alphabets or invalid numbers in the 'Expiration Year' field",log);
						}
						else
						{
							Fail("Alert not shown on entering the special characters or alphabets or invalid numbers in the 'Expiration Year' field",log);
						}	
						paymentOptionsCreditCardExpYearField.clear();
					}
					else
					{
						Fail("Year alert not displayed");
					}					
				}
				else
				{
					Fail("Expiration month and year field not displayed");
				}							
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1369 Issue ." + e.getMessage());
				Exception(" HBC - 1369 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1370 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering less than the required digits in the "expiration" fields.*/
	public void HBC1370PaymentSectionsCreditCardExpirationLessDigitsFieldAlert() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1370 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering less than the required digits in the expiration fields.");
		if(shipPage==true)
		{
			String monthVal = HBCBasicfeature.getExcelNumericVal("HB1370", sheet, 1);
			String monthAlert = HBCBasicfeature.getExcelVal("HB1370", sheet, 2);
			String yearVal = HBCBasicfeature.getExcelNumericVal("HBC1370", sheet, 1);
			String yearAlert = HBCBasicfeature.getExcelVal("HBC1370", sheet, 2);

			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField)&&(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField)))
				{
					Pass("Expiration Month & Year field displayed");
					
					paymentOptionsCreditCardExpMonthField.clear();
					paymentOptionsCreditCardExpMonthField.sendKeys(monthVal);
					Thread.sleep(200);
					if(paymentOptionsCreditCardExpMonthField.getAttribute("value").length()<2)
					{
						log.add("Entered value is Less than 2");
						paymentOptionsCreditCardExpYearField.click();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpMonthFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthFieldAlert))
						{
							String actual = paymentOptionsCreditCardExpMonthFieldAlert.getText().trim();
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+monthAlert);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(monthAlert))
							{
								Pass("Alert shown  on entering less than the required digits in the 'Expiration Month' field",log);
							}
							else
							{
								Fail("Alert not shown on entering the less than the required digits in the 'Expiration Month' field",log);
							}
							paymentOptionsCreditCardExpMonthField.clear();
						}
						else
						{
							Fail("Month alert not displayed");
						}
					}
					else
					{
						Fail("Entered value length not less than 2");
					}
						
					paymentOptionsCreditCardExpYearField.clear();
					paymentOptionsCreditCardExpYearField.sendKeys(yearVal);
					Thread.sleep(200);
					if(paymentOptionsCreditCardExpYearField.getAttribute("value").length()<4)
					{
						log.add("Entered value is Less than 4");
						paymentOptionsCreditCardExpMonthField.click();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpYearFieldAlert, "style", "block"));
						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearFieldAlert))
						{
							String actual = paymentOptionsCreditCardExpYearFieldAlert.getText().trim();
							log.add("The actual alert displayed is: "+actual);
							log.add("The expected alert to be displayed is: "+yearAlert);
							Thread.sleep(200);
							if(actual.equalsIgnoreCase(yearAlert))
							{
								Pass("Alert shown  on entering less than the required digits in the 'Expiration Year' field",log);
							}
							else
							{
								Fail("Alert not shown on entering less than the required digits in the 'Expiration Year' field",log);
							}	
							paymentOptionsCreditCardExpYearField.clear();
						}
						else
						{
							Fail("Year alert not displayed");
						}					
					}
					else
					{
						Fail("Entered value length not less than 4");
					}						
				}
				else
				{
					Fail("Expiration month and year field not displayed");
				}							
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1370 Issue ." + e.getMessage());
				Exception(" HBC - 1370 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1371 Register Checkout - Payment Information Section >Verify that alert should be shown on navigating to other fields without entering characters in the "expiration" fields*/
	public void HBC1371PaymentSectionsCreditCardExpirationEmptyFieldAlert() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1371 Guest Checkout - Payment Information Section > Verify that alert should be shown on navigating to other fields without entering characters in the expiration fields.");
		if(shipPage==true)
		{
			String emptyMAlert = HBCBasicfeature.getExcelVal("HBC1371", sheet, 1);
			String emptyYAlert = HBCBasicfeature.getExcelVal("HBC1371", sheet, 2);

			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
				{
					Pass("Expiration Month & Year field displayed");						
					paymentOptionsCreditCardExpMonthField.clear();	
					paymentOptionsCreditCardExpYearField.clear();						
					if(paymentOptionsCreditCardExpMonthField.getAttribute("value").isEmpty()&&paymentOptionsCreditCardExpYearField.getAttribute("value").isEmpty())
					{
						Pass("Month & Year field is empty");
						paymentOptionsCreditCardExpMonthField.click();
						paymentOptionsCreditCardExpYearField.click();
						paymentOptionsCreditCardSecurityCodeField.click();					
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpMonthFieldAlert, "style", "block"));
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpYearFieldAlert, "style", "block"));

						if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthFieldAlert)&&HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearFieldAlert))
						{
							String actualMAlert = paymentOptionsCreditCardExpMonthFieldAlert.getText().trim();
							String actualYAlert = paymentOptionsCreditCardExpYearFieldAlert.getText().trim();
							log.add("The actual Month alert displayed is: "+actualMAlert);
							log.add("The expected Month alert to be displayed is: "+emptyMAlert);
							log.add("The actual Year alert displayed is: "+actualYAlert);
							log.add("The expected Year alert to be displayed is: "+emptyYAlert);
							Thread.sleep(500);
							if(actualMAlert.equalsIgnoreCase(emptyMAlert)&&actualYAlert.equalsIgnoreCase(emptyYAlert))
							{
								Pass("Alert shown on navigating to other fields without entering characters in the 'Expiration Month/Year' field",log);
							}
							else
							{
								Fail("Alert not shown on navigating to other fields without entering characters in the 'Expiration Month/Year' field",log);
							}
						}
						else
						{
							Fail("Respective alerts not displayed");
						}
					}
					else
					{
						Fail("Month & Year field is not empty");
					}						
				}
				else
				{
					Fail("Expiration month and year field not displayed");
				}							
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1371 Issue ." + e.getMessage());
				Exception(" HBC - 1371 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1372 Register Checkout - Payment Information Section > Verify that "Expiration" fields, should accept maximum of 2 digits in month field and maximum of 4 digits in year field*/
	public void HBC1372PaymentSectionsCreditCardExpirationFieldMaximum()
	{
		ChildCreation(" HBC - 1372 Guest Checkout - Payment Information Section > Verify that Expiration fields, should accept maximum of 2 digits in month field and maximum of 4 digits in year field.");
		if(shipPage==true)
		{
			try
			{
				String[] cellMVal = HBCBasicfeature.getExcelNumericVal("HBC1372", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpMonthField))
				{
					Pass("'Expiration month' textbox field shown ");
					for(int i = 0; i<cellMVal.length;i++)
					{
						paymentOptionsCreditCardExpMonthField.clear();
						log.add( "The Entered value from the excel file is : " + cellMVal[i].toString());
						log.add( "The Value from the excel file was : " + cellMVal[i].toString() + " and is length is : " + cellMVal[i].toString().length());
						paymentOptionsCreditCardExpMonthField.sendKeys(cellMVal[i]);
						if(paymentOptionsCreditCardExpMonthField.getAttribute("value").isEmpty())
						{
							Fail( "The 'Expiration month' textbox field is empty.");
						}
						else if(paymentOptionsCreditCardExpMonthField.getAttribute("value").length()<=2)
						{
							Pass( "The 'Expiration month' textbox field was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The 'Expiration month' textbox field was enterable but it is not within the specified character limit.",log);
						}
						paymentOptionsCreditCardExpMonthField.clear();
					}
				}
				else
				{
					Fail("'Expiration month' textbox field not shown ");
				}	
				
				String[] cellYVal = HBCBasicfeature.getExcelNumericVal("HBC1372", sheet, 2).split("\n");
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
				{
					Pass("'Expiration Year' textbox field shown ");
					for(int i = 0; i<cellYVal.length;i++)
					{
						paymentOptionsCreditCardExpYearField.clear();
						log.add( "The Entered value from the excel file is : " + cellYVal[i].toString());
						log.add( "The Value from the excel file was : " + cellYVal[i].toString() + " and is length is : " + cellYVal[i].toString().length());
						paymentOptionsCreditCardExpYearField.sendKeys(cellYVal[i]);
						if(paymentOptionsCreditCardExpYearField.getAttribute("value").isEmpty())
						{
							Fail( "The 'Expiration Year' textbox field is empty.");
						}
						else if(paymentOptionsCreditCardExpYearField.getAttribute("value").length()<=4)
						{
							Pass( "The 'Expiration Year' textbox field was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The 'Expiration Year' textbox field was enterable but it is not within the specified character limit.",log);
						}
						paymentOptionsCreditCardExpYearField.clear();
					}
				}
				else
				{
					Fail("'Expiration Year' textbox field not shown ");
				}						
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1372 Issue ." + e.getMessage());
				Exception(" HBC - 1372 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1373 Guest Checkout - Verify that alert should be shown on entering the expired year in the "expiration" field*/
	public void HBC1373PaymentSectionsCreditCardExpiredYear() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1373 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering the expired year in the 'expiration' field");
		if(shipPage==true)
		{
			try
			{
				String expYear = HBCBasicfeature.getExcelNumericVal("HBC1373", sheet, 1);
				String yearAlert = HBCBasicfeature.getExcelVal("HBC1370", sheet, 2);

		
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearField))
				{
					Pass("Expiration Year field displayed");
					paymentOptionsCreditCardExpYearField.clear();
					paymentOptionsCreditCardExpYearField.sendKeys(expYear);
					paymentOptionsCreditCardExpMonthField.click();	
					Thread.sleep(200);
					wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardExpYearFieldAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardExpYearFieldAlert))
					{
						String actual = paymentOptionsCreditCardExpYearFieldAlert.getText().trim();
						log.add("The actual alert displayed is: "+actual);
						log.add("The expected alert to be displayed is: "+yearAlert);
						Thread.sleep(200);
						if(actual.equalsIgnoreCase(yearAlert))
						{
							Pass("Alert shown  on entering expired year in the 'Expiration Year' field",log);
						}
						else
						{
							Fail("Alert not shown on entering expired year in the 'Expiration Year' field",log);
						}	
						paymentOptionsCreditCardExpYearField.clear();
					}
					else
					{
						Fail("Year alert not displayed");
					}
				}
				else
				{
					Fail("'Expiration Year'field not displayed ");
				}			
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1373 Issue ." + e.getMessage());
				Exception(" HBC - 1373 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1374 Guest Checkout - Payment Information Section > Verify that "Apply a Gift Card" tex link should be shown below the radio tabs.*/
	public void HBC1374PaymentApplyGiftCardLink()
	{
		ChildCreation(" HBC - 1374 Guest Checkout - Payment Information Section > Verify that Apply a Gift Card text link should be shown below the radio tabs.");
		if(shipPage==true)
		{
			try
			{
				String expText = HBCBasicfeature.getExcelVal("HBC1374", sheet, 1);
				log.add(" The Expected text was :  " + expText);
				if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardLabel))
				{
					Pass( "The Apply Gift card option is displayed .");
					String actText = paymentOptionsApplyGiftCardLabel.getText();
					actText = WordUtils.capitalizeFully(actText);
					log.add(" The actual text is :  " + actText);
					if(expText.equalsIgnoreCase(actText))
					{
						Pass( "The Expected and the actual value matches.",log);
					}
					else
					{
						Fail( "The Expected and the actual value does not matches.",log);
					}
				}
				else
				{
					Fail( "The Apply Gift card option is not displayed .");
				}		
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1374 Issue ." + e.getMessage());
				Exception(" HBC - 1374 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1375 Guest Checkout - Payment Information Section > Verify that on tapping "Apply a Gift Card" tex link, "Card number" field, "Pin" field, "Apply" text link should be enabled.*/
	public void HBC1375PaymentSectionsPaybygiftcard() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1375 Guest Checkout - Payment Information Section > Verify that on tapping Apply a Gift Card tex link, Card number field, Pin field, Apply text link should be enabled.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCard))
				{
					Pass("Apply Gift Card option is displayed");
					//paymentOptionsApplyGiftCard.click();
					jsclick(paymentOptionsApplyGiftCard);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
					Thread.sleep(500);
					if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardNumberField))
					{
						Pass("Pay by gift card' section display 'Card number' field");
					}
					else
					{
						Fail("Pay by gift card' section not display 'Card number' field");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardPinField))
					{
						Pass("Pay by gift card' section display 'Pin' text boxes field");
					}
					else
					{
						Fail("Pay by gift card' section not display 'Pin' text boxes field");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardApply))
					{
						Pass("Pay by gift card' section display 'Apply' button");
					}
					else
					{
						Fail("Pay by gift card' section not display 'Apply' button");
					}						
				}
				else
				{
					Fail("Apply Gift card option is not displayed");
				}					
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1375 Issue ." + e.getMessage());
				Exception(" HBC - 1375 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}	
	
	/* HBC - 1376 Guest Checkout - Payment Information Section > Verify that "Pay by gift card" section should display "Card number", "Pin" text boxes with "Apply" button.*/
	public void HBC1376PaymentSectionsPaybygiftcard() throws java.lang.Exception
	{
		ChildCreation(" HBC - 1376 Guest Checkout - Payment Information Section > Verify that Pay by gift card section should display Card number, Pin text boxes with Apply button.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCard))
				{
					if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardNumberField))
					{
						Pass("Pay by gift card' section display 'Card number' field");
					}
					else
					{
						Fail("Pay by gift card' section not display 'Card number' field");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardPinField))
					{
						Pass("Pay by gift card' section display 'Pin' text boxes field");
					}
					else
					{
						Fail("Pay by gift card' section not display 'Pin' text boxes field");
					}
					
					if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardApply))
					{
						Pass("Pay by gift card' section display 'Apply' button");
					}
					else
					{
						Fail("Pay by gift card' section not display 'Apply' button");
					}						
				}
				else
				{
					Fail("Apply Gift card option is not displayed");
				}					
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1376 Issue ." + e.getMessage());
				Exception(" HBC - 1376 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1377 Guest Checkout - Payment Information Section > Verify that "Pin" field accepts maximum of 4 digits in "Pay by gift card" section.*/
	public void HBC1377PaymentSectionsApplyGiftPinFieldMaximum()
	{
		ChildCreation(" HBC - 1377 Guest Checkout - Payment Information Section > Verify that Pin field accepts maximum of 4 digits in Pay by gift card section.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1377", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCardPinField))
				{
					Pass("'Pin' number field shown ");
					for(int i = 0; i<cellVal.length;i++)
					{
						paymentOptionsApplyGiftCardPinField.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						paymentOptionsApplyGiftCardPinField.sendKeys(cellVal[i]);
						if(paymentOptionsApplyGiftCardPinField.getAttribute("value").isEmpty())
						{
							Fail( "The 'Pin number' field is empty.");
						}
						else if(paymentOptionsApplyGiftCardPinField.getAttribute("value").length()<=4)
						{
							Pass( "The 'Pin number' field was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The 'Pin number' field was enterable but it is not within the specified character limit.",log);
						}
						paymentOptionsApplyGiftCardPinField.clear();
					}
				}
				else
				{
					Fail("'Pin number' field not shown ");
				}						
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1377 Issue ." + e.getMessage());
				Exception(" HBC - 1377 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1378 Guest Checkout - Payment Information Section > Verify that "Add Another Gift Card" text link should be shown in "Pay by gift card" section.*/
	public void HBC1378PaymentSectionsAddAnotherGiftCard()
	{
		ChildCreation(" HBC - 1378 Guest Checkout - Payment Information Section > Verify that Add Another Gift Card text link should be shown in Pay by gift card section.");
		if(shipPage==true)
		{
			try
			{
				String expctd = HBCBasicfeature.getExcelVal("HBC1378", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsAddAnotherGiftCard))
				{
					String actual = paymentOptionsAddAnotherGiftCard.getText();
					log.add("The actual text link is: "+ actual);
					log.add("The expected text link is: "+ expctd);
					
					if(actual.equalsIgnoreCase(expctd))
					{
						Pass("'Add Another Gift Card' text link shown in 'Pay by gift card' section",log);
					}
					else
					{
						Fail("'Add Another Gift Card' text link not shown in 'Pay by gift card' section",log);
					}					
				}
				else
				{
					Fail("'Add Another Gift Card' text link not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1378 Issue ." + e.getMessage());
				Exception(" HBC - 1378 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1379 Guest Checkout - Payment Information Section > Verify that on tapping "Add Another Gift Card" text link in "Pay by gift card" section the respective action should be done.*/
	public void HBC1379PaymentSectionsAddAnotherGiftCardClick()
	{
		ChildCreation(" HBC - 1379 Guest Checkout - Payment Information Section > Verify that on tapping Add Another Gift Card text link in Pay by gift card section the respective action should be done.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsAddAnotherGiftCard))
				{
					Pass("'Add Another Gift Card' text link is displayed");
					jsclick(paymentOptionsAddAnotherGiftCard);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(secondGiftCard));
					if(HBCBasicfeature.isElementPresent(secondGiftCard))
					{
						Pass( "The Second Gift Card is displayed.");
					}
					else
					{
						Fail( "The Second Gift Card is not displayed.");
					}
				}
				else
				{
					Fail("'Add Another Gift Card' text link is not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1379 Issue ." + e.getMessage());
				Exception(" HBC - 1379 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1380 Guest Checkout - Payment Information Section > Verify that "Pay by credit card" checkbox should be deselected automatically on tapping "Pay by PayPal" radio tab.*/
	public void HBC1380PaymentSectionsPaybycreditcardDeselect()
	{
		ChildCreation(" HBC - 1380 Guest Checkout - Payment Information Section > Verify that Pay by credit card checkbox should be deselected automatically on tapping Pay by PayPal radio tab.");
		if(shipPage==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected))
				{
					wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecEnable, "style", "block"));
					Pass( "The Credit Card option was selected.");
					if(HBCBasicfeature.isElementPresent(paymentOptionsPaypalChkkBox))
					{
						jsclick(paymentOptionsPaypalChkkBox);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						wait.until(ExpectedConditions.attributeContains(paymentOptionsCreditCardSecEnable, "style", "none"));
						if(!HBCBasicfeature.isElementPresent(paymentOptionsCreditCardSelected)&&(HBCBasicfeature.isElementPresent(paymentOptionsPaypalSelected)))
						{
							Pass("'Pay by credit card' checkbox deselected automatically on tapping 'Pay by PayPal' radio tab");
						}
						else
						{
							Fail("'Pay by credit card' checkbox not deselected automatically on tapping 'Pay by PayPal' radio tab");
						}
					}
					else
					{
						Fail("'Pay by paypal' option not displayed");
					}					
				}
				else
				{
					Fail("'Add Another Gift Card' text link is not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1380 Issue ." + e.getMessage());
				Exception(" HBC - 1380 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1398  Guest Checkout - Payment Information Section > Verify that "What is PayPal" text link should be shown in the paypal page near PayPal logo.*/
	public void HBC1398PaymentSectionsWhatIsPayPal()
	{ 
		ChildCreation(" HBC - 1398  Guest Checkout - Payment Information Section > Verify that What is PayPal text link should be shown in the paypal page near PayPal logo.");
		if(shipPage==true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC1398", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsPaypalSelected))
				{
					Pass( "The Pay By Paypal field is displayed .");
					if(HBCBasicfeature.isElementPresent(paymentOptionsWhatIspaypal))
					{
						HBCBasicfeature.scrollup(paymentOptionsWhatIspaypal, driver);
						String actual = paymentOptionsWhatIspaypal.getText();
						log.add("The Actual text link displayed is: "+actual);
						log.add("The Expected text link displayed is: "+expected);
						if(expected.equalsIgnoreCase(actual))
						{
							Pass("'What is paypal' text link  shown in 'Payment' column",log);
						}
						else
						{
							Fail("'What is paypal' text link not shown in 'Payment' column",log);
						}
					}
					else
					{
						Fail("'What is paypal' text link not displayed");
					}						
				}
				else
				{
					Fail( "The Pay By Paypal field is not displayed .");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1581 Issue ." + e.getMessage());
				Exception(" HBC - 1581 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
			
	/* HBC - 1397 Guest Checkout - Payment Information Section > Verify that in "Payment" column, on tapping the "What is PayPal" text link should redirect or paypal overlay/page should be shown.*/
	public void HBC1397PaymentSectionsWhatIsPayPalClick()
	{ 
		ChildCreation(" HBC - 1397 Guest Checkout - Payment Information Section > Verify that in Payment column, on tapping the What is PayPal text link should redirect or paypal overlay/page should be shown.");
		if(shipPage==true)
		{
			try
			{
				String payPalUrl = HBCBasicfeature.getExcelVal("HBC1397", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsPaypalSelected))
				{
					Pass( "The Pay By Paypal field is displayed .");
					if(HBCBasicfeature.isElementPresent(paymentOptionsWhatIspaypal))
					{
						HBCBasicfeature.scrollup(paymentOptionsWhatIspaypal, driver);
						String handle = driver.getWindowHandle();
						jsclick(paymentOptionsWhatIspaypal);
						Thread.sleep(2000);
						for(String newWin: driver.getWindowHandles())
						{
							if(!newWin.equals(handle))
							{
								driver.switchTo().window(newWin);
								Thread.sleep(1000);
								String url = driver.getCurrentUrl();
								if(url.contains(payPalUrl))
								{
									Pass("On selecting 'What is PayPal' text link paypal overlay shown");
								}
								else
								{
									Fail("On selecting 'What is PayPal' text link paypal overlay shown");
								}
								driver.close();		
								driver.switchTo().window(handle);
								Thread.sleep(1000);
							}						
						}									
					}
					else
					{
						Fail("'What is paypal' text link not displayed");
					}						
				}
				else
				{
					Fail( "The Pay By Paypal field is not displayed .");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1582 Issue ." + e.getMessage());
				Exception(" HBC - 1582 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1381 Guest Checkout - Payment Information Section > Verify that "HBC rewards card number:" field, "Promo Codes" field should be shown under gift card Section.*/
	public void HBC1381PaymentHBCrewards()
	{
		ChildCreation(" HBC - 1381 Guest Checkout - Payment Information Section > Verify that HBC rewards card number: field, Promo Codes field should be shown under gift card Section.");
		if(shipPage==true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC1381", sheet, 1);					
				if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardFieldLabel))
				{
					String actual = paymentOptionsRewardCardFieldLabel.getText();
					log.add("The actual HBC rewards label is: "+actual);
					log.add("The expected HBC rewards label is: "+expected);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass( "The 'HBC rewards card number' is displayed .",log);
					}
					else
					{
						Fail( "The 'HBC rewards card number' is not displayed .");
					}
				}
				else
				{
					Fail( "The 'HBC rewards card number' label is not displayed .");
				}						
				
				if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField1))
				{
					Pass( "The Reward card number field-1 is displayed .");
				}
				else
				{
					Fail( "The Reward card number field-1 is not displayed .");
				}
				
				if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
				{
					Pass( "The Reward card number field-2 is displayed .");
				}
				else
				{
					Fail( "The Reward card number field-2 is not displayed .");
				}
				
				if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
				{
					Pass( "The Add promo code field is displayed .");
				}
				else
				{
					Fail( "The Add promo code field is not displayed .");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1381 Issue ." + e.getMessage());
				Exception(" HBC - 1381 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1382 Guest Checkout - Payment Information Section > Verify that user should be able to enter card number in the "HBC rewards card number:" field and it should accept maximum of 9 digits.*/
	public void HBC1382PaymentHBCrewardValidation() throws Exception
	{
		ChildCreation(" HBC - 1382 Guest Checkout - Payment Information Section > Verify that user should be able to enter card number in the HBC rewards card number: field and it should accept maximum of 9 digits.");
		if(shipPage==true)
		{
			String[] cellVal = HBCBasicfeature.getExcelNumericVal("HBC1382", sheet, 1).split("\n");
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
				{
					Pass( "The Reward card number field-2 is displayed .");
					for(int i = 0; i<cellVal.length; i++)
					{
						paymentOptionsRewardCardField2.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						paymentOptionsRewardCardField2.sendKeys(cellVal[i]);
						if(paymentOptionsRewardCardField2.getAttribute("value").isEmpty())
						{
							Fail( "The 'HBC rewards card number:' field is empty.");
						}
						else if(paymentOptionsRewardCardField2.getAttribute("value").length()<=9)
						{
							Pass( "The  'HBC rewards card number:' field was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
						}
						paymentOptionsRewardCardField2.clear();	
					}
				}
				else
				{
					Fail("HBC Reward card field is not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1382 Issue ." + e.getMessage());
				Exception(" HBC - 1382 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1383 Guest Checkout - Payment Information Section > Verify that corresponding alert message should be shown, when the user entered less than 9 digits in the "HBC rewards card number" field*/
	/* HBC - 1384 Guest Checkout - Payment Information Section > Verify that alert message should be shown, when the user entered alphabets or special characters in the "HBC rewards card number" field.*/
	public void HBC1383PaymentHBCrewardlenValidation() throws Exception
	{
		ChildCreation(" HBC - 1383 Guest Checkout - Payment Information Section > Verify that corresponding alert message should be shown, when the user entered less than 9 digits in the HBC rewards card number field.");
		boolean HBC1384 = false;
		if(shipPage==true)
		{
			String[] cellVal = HBCBasicfeature.getExcelVal("HBC1383", sheet, 1).split("\n");
			String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1383", sheet, 2);
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
				{
					Pass( "The Reward card number field-2 is displayed .");	
					for(int i=0;i<cellVal.length;i++)
					{
						paymentOptionsRewardCardField2.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						paymentOptionsRewardCardField2.sendKeys(cellVal[i]);
						if(paymentOptionsRewardCardField2.getAttribute("value").length()<9)
						{
							paymentOptionsPromoCode.click();
							wait.until(ExpectedConditions.attributeContains(paymentOptionsRewardCardFieldAlert, "style", "block"));
							Thread.sleep(100);
							if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardFieldAlert))
							{
								String alert = paymentOptionsRewardCardFieldAlert.getText();
								log.add("The displayed alert is: "+alert);
								log.add("The Actual alert is: "+ ExpctdAlert);
								if(alert.equalsIgnoreCase(ExpctdAlert))
								{
									Pass("corresponding alert message shown, when the user entered less than 9 digits in the 'HBC rewards card number' field",log);
									HBC1384 = true;
								}
								else
								{
									Fail("corresponding alert message not shown, when the user entered less than 9 digits in the 'HBC rewards card number' field",log);
								}
								paymentOptionsRewardCardField2.clear();	
							}
							else
							{
								Fail("Alert is not displayed",log);
							}						
						}
						else
						{
							Fail( "The Reward card Number was enterable but it is not within the specified character limit.",log);
						}							
					}
				}
				else
				{
					Fail("HBC Reward card field is not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1383 Issue ." + e.getMessage());
				Exception(" HBC - 1383 Issue ." + e.getMessage());
			}
			
			ChildCreation(" HBC - 1384 Guest Checkout - Payment Information Section > Verify that alert message should be shown, when the user entered alphabets or special characters in the HBC rewards card number field.");
			if(HBC1384 == true)
			{
				Pass("corresponding alert message shown, when the user entered  alphabets or special characters in the 'HBC rewards card number' field",log);
			}
			else
			{
				Fail("corresponding alert message not shown, when the user entered alphabets or special characters in the 'HBC rewards card number' field",log);
			}				
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1385 Guest Checkout - Payment Information Section > Verify that "Apply" text link should be shown near the "Promo Codes" text box.*/
	public void HBC1385PaymentHBCrewardsApply() throws Exception
	{
		ChildCreation(" HBC - 1385 Guest Checkout - Payment Information Section > Verify that Apply text link should be shown near the Promo Codes text box.");
		if(shipPage==true)
		{
			try
			{
				String expctd = HBCBasicfeature.getExcelVal("HBC1385", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
				{
					Pass( "The Add promo code field is displayed .");
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApply))
					{
						String actual = paymentOptionsPromoCodeApply.getText();
						log.add("Actual text link displayed is: "+actual);
						log.add("Expected text link displayed is: "+expctd);
						if(actual.equalsIgnoreCase(expctd))
						{
							Pass(" 'Apply' text link shown near the 'Promo Codes' text box",log);
						}
						else
						{
							Fail("'Apply' text link not shown near the 'Promo Codes' text box",log);
						}
					}
					else
					{
						Fail("Promo code Apply link mnot displayed");
					}
				}
				else
				{
					Fail( "The Add promo code field is not displayed .");
				}
			}		
			catch(Exception e)
			{
				System.out.println(" HBC - 1385 Issue ." + e.getMessage());
				Exception(" HBC - 1385 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1386 Guest Checkout - Payment Information Section > Verify that alert should be shown on tapping the "Apply" text link near the "Promo Codes" text box without entering any characters.*/
	public void HBC1386PaymentHBCrewardsEmptyApply() throws Exception
	{
		ChildCreation(" HBC - 1386 Guest Checkout - Payment Information Section > Verify that alert should be shown on tapping the Apply text link near the Promo Codes text box without entering any characters.");
		if(shipPage==true)
		{
			try
			{					
				String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1386", sheet, 2);
				if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
				{
					Pass( "The Add promo code field is displayed .");
					//HBCBasicfeature.scrolldown(paymentOptionsPromoCode, driver);
					paymentOptionsPromoCode.clear();
					if(paymentOptionsPromoCode.getAttribute("value").isEmpty())
					{
						log.add("PromoCode box is empty");
						if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApply))
						{
							jsclick(paymentOptionsPromoCodeApply);
							wait.until(ExpectedConditions.attributeContains(paymentOptionsPromoCodeApplyAlert, "style", "block"));
							if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApplyAlert))
							{	
								String alert = paymentOptionsPromoCodeApplyAlert.getText();
								log.add("The displayed alert is: "+alert);
								log.add("The Actual alert is: "+ ExpctdAlert);
								if(alert.equalsIgnoreCase(ExpctdAlert))
								{
									Pass("corresponding alert message shown, when tapping the Apply text link near the 'Promo Codes' text box without entering any characters",log);
								}
								else
								{
									Fail("corresponding alert message not shown, when tapping the Apply text link near the 'Promo Codes' text box without entering any characters",log);
								}
							}
							else
							{
								Fail("Alert not displayed",log);
							}								
						}
						else
						{
							Fail("Promo code Apply link mnot displayed");
						}
					}
					else
					{
						Fail("The Add promo code field is not Empty");
					}
				}
				else
				{
					Fail( "The Add promo code field is not displayed .");
				}					
			}		
			catch(Exception e)
			{
				System.out.println(" HBC - 1386 Issue ." + e.getMessage());
				Exception(" HBC - 1386 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1387 Guest Checkout - Payment Information Section > Verify that user should be able to enter promo code in "Promo Codes" field and it should accept maximum of 20 digits.*/
	public void HBC1387PaymentHBCrewardLenPromocode()
	{	
		ChildCreation(" HBC - 1387 Guest Checkout - Payment Information Section > Verify that user should be able to enter promo code in Promo Codes field and it should accept maximum of 20 digits.");
		if(shipPage==true)
		{
			try
			{
				String[] cellVal = HBCBasicfeature.getExcelVal("HBC1387", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
				{
					Pass( "The Add Promo code field is displayed.");					
					for(int i = 0; i<cellVal.length;i++)
					{
						paymentOptionsPromoCode.clear();
						log.add( "The Entered value from the excel file is : " + cellVal[i].toString());
						log.add( "The Value from the excel file was : " + cellVal[i].toString() + " and is length is : " + cellVal[i].toString().length());
						paymentOptionsPromoCode.sendKeys(cellVal[i]);
						if(paymentOptionsPromoCode.getAttribute("value").isEmpty())
						{
							Fail( "The Contact Number field is empty.");
						}
						else if(paymentOptionsPromoCode.getAttribute("value").length()<=20)
						{
							Pass( "The Contact Number was enterable and it is within the specified character limit.",log);
						}
						else
						{
							Fail( "The Contact Number was enterable but it is not within the specified character limit.",log);
						}
						paymentOptionsPromoCode.clear();
					}
				}
				else
				{
					Fail("HBC Reward Promo code field is not displayed");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1387 Issue ." + e.getMessage());
				Exception(" HBC - 1387 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( "The User was unable to add the product and the user is not navigated to the checkout page.");
		}
	}
	
	/* HBC - 1388 Guest Checkout - Payment Information Section > Verify that alert should be shown on clicking the "Apply" link after entering the invalid numbers.*/
	/* HBC - 1390 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering less than the acceptable characters in the "Promo Codes" field.*/
	public void HBC1388PaymentHBCrewardInvalidPromocode() throws Exception
	{
		ChildCreation(" HBC - 1388 Guest Checkout - Payment Information Section > Verify that alert should be shown on clicking the Apply link after entering the invalid numbers.");
		boolean HBC1390 = false;
		if(shipPage==true)
		{
			String cellVal = HBCBasicfeature.getExcelVal("HBC1388", sheet, 1);
			String ExpctdAlert = HBCBasicfeature.getExcelVal("HBC1388", sheet, 2);
			try
			{
				if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
				{
					Pass( "The Add Promo code field is displayed .");							
					paymentOptionsPromoCode.clear();
					log.add( "The Entered value from the excel file is : " + cellVal.toString());
					log.add( "The Value from the excel file was : " + cellVal.toString() + " and is length is : " + cellVal.toString().length());						
					paymentOptionsPromoCode.sendKeys(cellVal);
					Thread.sleep(1000);
					jsclick(paymentOptionsPromoCodeApply);
					Thread.sleep(2000);
					wait.until(ExpectedConditions.attributeContains(paymentOptionsPromoCodeApplyInvalidAlert, "style", "block"));
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApplyInvalidAlert))
					{
						String alert = paymentOptionsPromoCodeApplyInvalidAlert.getText();
						log.add("The displayed alert is: "+alert);
						log.add("The Actual alert is: "+ ExpctdAlert);
						if(alert.equalsIgnoreCase(ExpctdAlert))
						{
							Pass("corresponding alert message shown, when clicking on 'Apply' link after entering the invalid numbers",log);
							HBC1390 = true;
						}
						else
						{
							Fail("corresponding alert message not shown, when clicking on 'Apply' link after entering the invalid numbers' field",log);
						}
						paymentOptionsPromoCode.clear();	
					}
					else
					{
						Fail("Alert is not displayed",log);
					}							
				}
				else
				{
					Fail("HBC Reward Promo code field is not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1388 Issue ." + e.getMessage());
				Exception(" HBC - 1388 Issue ." + e.getMessage());
			}
			
			ChildCreation(" HBC - 1390 Guest Checkout - Payment Information Section > Verify that alert should be shown on entering less than the acceptable characters in the Promo Codes field.");
			if(HBC1390 == true)
			{
				Pass("corresponding alert message shown, when the user entered less than the acceptable characters in the 'Promo Codes' field");
			}
			else
			{
				Fail("corresponding alert message not shown, when the user entered less than the acceptable characters in the 'Promo Codes' field");
			}				
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1391 Guest Checkout - Payment Information Section > Verify that "Shipping & Taxes" column should display "Order sub total", "Shipping amount", "PST", "GST/HST", "HBC Reward Points Earned", "Order total".*/
	public void HBC1391PaymentShippingandTaxes() throws Exception
	{
		ChildCreation(" HBC - 1391 Guest Checkout - Payment Information Section > Verify that Shipping & Taxes column should display Order sub total, Shipping amount, PST, GST/HST, HBC Reward Points Earned, Order total.");
		if(shipPage==true)
		{
			try
			{			
				if(HBCBasicfeature.isElementPresent(paymentOptionsEstimatedSubtotal))
				{
					Pass("Order Sub Total option displayed");
				}
				else
				{
					Fail("Order Sub Total option not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsShippingAmountlabel))
				{
					Pass("Order Shipping Amount option displayed");
				}
				else
				{
					Fail("Order Shipping Amount option not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsPSTLabel))
				{
					Pass("PST label option displayed");
				}
				else
				{
					Fail("PST label option not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsGSTHSTLabel))
				{
					Pass("GST/HST label option displayed");
				}
				else
				{
					Fail("GST/HST label option not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotalLabel))
				{
					Pass("Order Total option displayed");
				}
				else
				{
					Fail("Order Total option not displayed");
				}					
			}						
			catch(Exception e)
			{
				System.out.println(" HBC - 1391 Issue ." + e.getMessage());
				Exception(" HBC - 1391 Issue ." + e.getMessage());
			}			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1392 Guest Checkout - Payment Information Section > Verify that "Order Sub total" and its amount should be highlighted in black."*/
	public void HBC1392PaymentOrderTotalHighlight() throws Exception
	{
		ChildCreation(" HBC - 1392 Guest Checkout - Payment Information Section > Verify that Order Sub total and its amount should be highlighted in black.");
		if(shipPage==true)
		{
			try
			{	
				String expctdColor = HBCBasicfeature.getExcelVal("HBC1393", sheet, 2);
				if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotalLabel))
				{
					String actual = paymentOptionsOrderTotalLabel.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(actual);
					log.add("The actual color is: "+actualColor);
					log.add("The expected color is: "+expctdColor);
					if(actualColor.equalsIgnoreCase(expctdColor))
					{
						Pass("'Order total'  highlighted in black",log);
					}
					else
					{
						Fail("'Order total' not highlighted in black",log);
					}
				}
				else
				{
					Fail("Order Total label is not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotal))
				{
					String actual = paymentOptionsOrderTotal.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(actual);
					log.add("The actual color is: "+actualColor);
					log.add("The expected color is: "+expctdColor);
					if(actualColor.equalsIgnoreCase(expctdColor))
					{
						Pass("'Order total' amount highlighted in black",log);
					}
					else
					{
						Fail("'Order total'amount not highlighted in black",log);
					}
				}
				else
				{
					Fail("Order Total amount is not displayed");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1392 Issue ." + e.getMessage());
				Exception(" HBC - 1392 Issue ." + e.getMessage());
			}			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1393 Register Checkout - Payment Information Section >  Verify that "Shipping method" dropdown field should be shown under "Shipping & Taxes" column"*/
	public void HBC1393PaymentShippingMethodDropDown() throws Exception
	{
		ChildCreation(" HBC - 1393 Reg Checkout - Payment Information Section > Verify that 'Shipping method' dropdown field should be shown under 'Shipping & Taxes' column");
		if(shipPage==true)
		{
			try
			{	
				if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
				{
					Pass( "The Shipping Method dropdown is displayed .");						
				}
				else
				{
					Fail( "The Shipping Method dropdown is not displayed .");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1393 Issue ." + e.getMessage());
				Exception(" HBC - 1393 Issue ." + e.getMessage());
			}			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1394 Guest Checkout - Payment Information Section > Verify that "Standard Delivery" option hould be selected in default in "Shipping method" dropdown field.*/
	public void HBC1394PaymentShippingMethodDropDownDefault() throws Exception
	{
		ChildCreation(" HBC - 1394 Guest Checkout - Payment Information Section > Verify that Standard Delivery option hould be selected in default in Shipping method dropdown field.");
		if(shipPage==true)
		{
			try
			{	
				String expctd = HBCBasicfeature.getExcelVal("HBC1394", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
				{
					Pass( "The Shipping Method dropdown is displayed .");							
					Select sel = new Select(paymentOptionsShippingMethoddropdown);
					String actual = sel.getFirstSelectedOption().getText().trim();
					log.add("The selected by default option is: "+actual);
					log.add("The expected option  is: "+expctd);
					Thread.sleep(500);
					if(actual.equalsIgnoreCase(expctd))
					{
						Pass("'Standard Delivery' option selected in default in 'Shipping method' dropdown field",log);
					}
					else
					{
						Fail("'Standard Delivery' option not selected in default in 'Shipping method' dropdown field",log);
					}						
				}
				else
				{
					Fail( "The Shipping Method dropdown is not displayed .");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1394 Issue ." + e.getMessage());
				Exception(" HBC - 1394 Issue ." + e.getMessage());
			}			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1395 Guest Checkout - Payment Information Section > Verify that "Shipping method" dropdown field should display maximum of 3 options."*/
	public void HBC1395PaymentShippingMethodDropDownOptions() throws Exception
	{
		ChildCreation(" HBC - 1395 Guest Checkout - Payment Information Section > Verify that Shipping method dropdown field should display maximum of 3 options.");
		if(shipPage==true)
		{
			try
			{	
				if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
				{
					Select sel = new Select(paymentOptionsShippingMethoddropdown);
					int size = sel.getOptions().size();
					log.add("Displayed option size is: "+size);
					if(size==3)
					{
						Pass("Dropdown field has maximum 3 options", log);
						for(int i=0;i<size;i++)
						{
							sel.getOptions().get(i).click();
							Thread.sleep(1000);
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
							String option = sel.getFirstSelectedOption().getText();
							Pass("The 'Shipping method' dropdown field option is: "+option);
						}
						sel.getOptions().get(0).click();
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
					}
					else
					{
						Fail("'Shipping method' dropdown field doesn't has maximum of 3 options",log);
					}
				}
				else
				{
					Fail("DropDown field not displayed");
				}					
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1395 Issue ." + e.getMessage());
				Exception(" HBC - 1395 Issue ." + e.getMessage());
			}			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1396 Guest Checkout - Payment Information Section > Verify that prices in the "Shipping & taxes" column is also changed on changing the "Shipping method" dropdown option."*/
	public void HBC1396PaymentShippingMethodDropDownOptionChange() throws Exception
	{
		ChildCreation(" HBC - 1396 HBC - 1396 Guest Checkout - Payment Information Section > Verify that prices in the Shipping & taxes column is also changed on changing the Shipping method dropdown option.");
		if(shipPage==true)
		{
			try
			{	
				if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
				{
					Select sel = new Select(paymentOptionsShippingMethoddropdown);
					int size = sel.getOptions().size();
					log.add("Displayed option size is: "+size);
					String currentOption = sel.getFirstSelectedOption().getText().trim();
					String currentVal = paymentOptionsShippingAmountPrice.getText();
					Thread.sleep(200);
					if(size>1)
					{
						sel.getOptions().get(1).click();
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						Thread.sleep(1000);
						String option = sel.getFirstSelectedOption().getText().trim();
						String Val = paymentOptionsShippingAmountPrice.getText();	
						Thread.sleep(200);
						if(currentOption.equalsIgnoreCase(option)&&(currentVal.equalsIgnoreCase(Val)))
						{
							Fail("'Shipping & taxes' column is not changed on changing the 'Shipping method' dropdown option",log);
						}
						else
						{
							Pass("'Shipping & taxes' column is also changed on changing the 'Shipping method' dropdown option",log);
						}							
						sel.getOptions().get(0).click();
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
					}
					else
					{
						Fail("'Shipping method' dropdown field doesn't has more than 1 options",log);
					}
				}
				else
				{
					Fail("DropDown field not displayed");
				}					
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1396 Issue ." + e.getMessage());
				Exception(" HBC - 1396 Issue ." + e.getMessage());
			}			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1399 Guest Checkout - Verify that "Shipping & Payment" section, should be as per the creative.*/
	public void HBC1399ShippingPaymentCreative() throws Exception
	{
		ChildCreation(" HBC - 1399 Guest Checkout - Verify that Shipping & Payment section, should be as per the creative.");
		if(shipPage==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(checkoutNavTitle));
				if(HBCBasicfeature.isElementPresent(checkoutNavTitle))
				{
					Pass("Checkout Navigation Tab's are displayed");
				}
				else
				{
					Fail("	Checkout Navigation Tab's are  not displayed");
				}
				if(HBCBasicfeature.isElementPresent(shipandPaypageShipSec))
				{
					Pass("Shiping Information section displayed");
				}
				else
				{
					Fail("Shiping Information section  not displayed");
				}
				if(HBCBasicfeature.isElementPresent(shipandPaypagePaySec))
				{
					Pass("Payment Information section displayed");
				}
				else
				{
					Fail("Payment Information section  not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsCreditCard))
				{
					HBCBasicfeature.scrolldown(paymentOptionsCreditCard, driver);
					Pass( "The Credit Card payment Checkbox is displayed .");
				}
				else
				{
					Fail( "The Credit Card payment Checkbox is not displayed .");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsPaypal))
				{
					Pass( "The Paypal payment Checkbox is displayed .");
				}
				else
				{
					Fail( "The Paypal payment Checkbox is not displayed .");
				}	
				if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardFieldLabel))
				{	
					HBCBasicfeature.scrolldown(paymentOptionsRewardCardFieldLabel, driver);
					Pass( "The 'HBC rewards card number' is displayed .");						
				}
				else
				{
					Fail( "The 'HBC rewards card number' label is not displayed .");
				}		
				Thread.sleep(500);
				if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField1))
				{
					Pass( "The Reward card number field-1 is displayed .");
				}
				else
				{
					Fail( "The Reward card number field-1 is not displayed .");
				}
				
				if(HBCBasicfeature.isElementPresent(paymentOptionsRewardCardField2))
				{
					Pass( "The Reward card number field-2 is displayed .");
				}
				else
				{
					Fail( "The Reward card number field-2 is not displayed .");
				}
				
				if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
				{
					Pass( "The Add promo code field is displayed .");
				}
				else
				{
					Fail( "The Add promo code field is not displayed .");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsShippingMethoddropdown))
				{
					Pass( "The Shipping Method dropdown is displayed .");						
				}
				else
				{
					Fail( "The Shipping Method dropdown is not displayed .");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsEstimatedSubtotal))
				{
					Pass("Order Sub Total option displayed");
				}
				else
				{
					Fail("Order Sub Total option not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsShippingAmountlabel))
				{
					HBCBasicfeature.scrolldown(paymentOptionsShippingAmountlabel, driver);
					Pass("Order Shipping Amount option displayed");
				}
				else
				{
					Fail("Order Shipping Amount option not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsPSTLabel))
				{
					Pass("PST label option displayed");
				}
				else
				{
					Fail("PST label option not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsGSTHSTLabel))
				{
					Pass("GST/HST label option displayed");
				}
				else
				{
					Fail("GST/HST label option not displayed");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionsOrderTotalLabel))
				{
					Pass("Order Total option displayed");
				}
				else
				{
					Fail("Order Total option not displayed");
				}	
				if(HBCBasicfeature.isElementPresent(paymentOptionsApplyGiftCard))
				{
					Pass( "The Apply Gift card option is displayed .");						
				}
				else
				{
					Fail( "The Apply Gift card option is not displayed .");
				}
				if(HBCBasicfeature.isElementPresent(paymentOptionReviewOrder))
				{
					Pass( "The Review Order option is displayed .");						
				}
				else
				{
					Fail( "The Review Order option is not displayed .");
				}
				HBCBasicfeature.scrolldown(checkoutNavTitle, driver);
				Thread.sleep(500);
			}		
			catch (Exception e)
			{
				System.out.println(" HBC - 1399 Issue ." + e.getMessage());
				Exception(" HBC - 1399 Issue ." + e.getMessage());
			}	
		}
		else
		{
			Skip("Shipping & Payment page not displayed");
		}
	}
	
	/* HBC - 1400 Register Checkout - Verify that "Review & Submit" section should be as per the creative*/
	public void HBC1400ReviewAndSubmitCreative()
	{
		ChildCreation(" HBC - 1400 Guest Checkout - Verify that Review & Submit section, should be as per the creative.");
		if(shipPage==true)
		{
			try
			{
				reviewSub = AddPaymentReviewOrderClick();
				if(reviewSub==true)
				{
					wait.until(ExpectedConditions.visibilityOf(checkoutNavTabs));
					if(HBCBasicfeature.isElementPresent(checkoutNavTabs))
					{
						Pass("Checkout Navigation Tab's are displayed");
					}
					else
					{
						Fail("Checkout Navigation Tab's are  not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddress))
					{
						Pass("Review & Submit page Shipping Address section is displayed");
					}
					else
					{
						Fail("Review & Submit page Shipping Address section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethod))
					{
						Pass("Review & Submit page Shipping Method section is displayed");
					}
					else
					{
						Fail("Review & Submit page Shipping Method section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddress))
					{
						Pass("Review & Submit page Billing Address section is displayed");
					}
					else
					{
						Fail("Review & Submit page Billing Address section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethod))
					{
						Pass("Review & Submit page Billing Method section is displayed");
					}
					else
					{
						Fail("Review & Submit page Billing Method section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitCheckoutSummary))
					{
						HBCBasicfeature.scrolldown(ReviewandSubmitCheckoutSummary, driver);
						Pass("Review & Submit page checkout summary section is displayed");
					}
					else
					{
						Fail("Review & Submit page checkout summary section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitPlaceorderbutton))
					{
						Pass("Review & Submit page Placeorder button is displayed");
					}
					else
					{
						Fail("Review & Submit page Placeorder button is not displayed");
					}						
				}
				else
				{
					Fail("Review & Submit page not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1400 Issue ." + e.getMessage());
				Exception(" HBC - 1400 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1401 Guest Checkout - Verify that user can be able to navigate to the previous section(i.e."Shipping & Payment" section or "Addresses" section).*/
	/* HBC - 1402 Guest Checkout - Verify that user should be navigate to the "Shipping & Payment" section on clicking the "Shipping & Payment" link*/
	public void HBC1401ReviewAndSubmitToPreviousTabs()
	{
		ChildCreation(" HBC - 1401 Guest Checkout - Verify that user can be able to navigate to the previous section(i.e Shipping & Payment section or Addresses section).");
		boolean HBC1402 = false;
		if(reviewSub==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
				{
					Pass("Currently in Review and Submit page");
					if(HBCBasicfeature.isElementPresent(shippingandPayment))
					{
						//shippingandPayment.click(); 
						jsclick(shippingandPayment);
						wait.until(ExpectedConditions.attributeContains(shippingPaymentPage, "style", "visible"));
						if(HBCBasicfeature.isElementPresent(shippingPaymentPage))
						{
							Pass("Checkout shipping & payment page displayed");
							Pass("user can able to navigate to the previous sections");
							HBC1402 = true;
							shipPage = true;
							shipAddress = shipandPaypageShipAddressDisp.getText();
							billAddress = shipandPaypagePayAddressDisp.getText();								 
							Select sel = new Select(paymentOptionsShippingMethoddropdown);
							paymethod = sel.getFirstSelectedOption().getText();		
							sel = new Select(paymentOptionsShippingMethoddropdown);
							shipMethod = sel.getFirstSelectedOption().getText();
						}
						else
						{
							Fail("Checkout shipping & payment page not displayed");
							shipPage = false;
						}																
					}
					else
					{
						Fail("Shipping & Payment tab not displayed");
					}
				}
				else
				{
					Fail("Review and Submit page not displayed");
				}
			}			
			catch(Exception e)
			{
				shipPage = false;
				System.out.println(" HBC - 1401 Issue ." + e.getMessage());
				Exception(" HBC - 1401 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
			
		/* HBC - 1402 Guest Checkout - Verify that user should be navigate to the "Shipping & Payment" section on clicking the "Shipping & Payment" link*/
		ChildCreation(" HBC - 1402 Guest Checkout - Verify that user should be navigate to the Shipping & Payment section on clicking the Shipping & Payment link.");
		if(reviewSub==true)
		{
			if(HBC1402==true)
			{
				Pass("user can navigate to the 'Shipping & Payment' section on clicking the 'Shipping & Payment' link");	
			}
			else
			{
				Fail("user not able to navigate to the 'Shipping & Payment' section on clicking the 'Shipping & Payment' link");
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1403 Guest Checkout - Verify that "Shipping Address", "Shipping Method", "Billing Address", "Payment Method" columns should be shown along with "Edit" text links in the "Review & Submit" section.*/
	public void HBC1403ReviewAndSubmitColumswithEdit()
	{
		ChildCreation(" HBC - 1403 Guest Checkout - Verify that Shipping Address, Shipping Method, Billing Address, Payment Method columns should be shown along with Edit text links in the Review & Submit section.");
		if(shipPage==true)
		{
			try
			{
				reviewSub = AddPaymentReviewOrderClick();
				if(reviewSub==true)
				{						
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddress))
					{
						Pass("Review & Submit page Shipping Address section is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressEdit))
						{
							Pass("Edit link displayed for 'Shipping Address' section ");
						}
						else
						{
							Fail("Edit link not displayed for 'Shipping Address' section");
						}
					}
					else
					{
						Fail("Review & Submit page Shipping Address section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethod))
					{
						Pass("Review & Submit page Shipping Method section is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodEdit))
						{
							Pass("Edit link displayed for 'Shipping Method' section ");
						}
						else
						{
							Fail("Edit link not displayed for 'Shipping Method' section");
						}
					}
					else
					{
						Fail("Review & Submit page Shipping Method section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddress))
					{
						Pass("Review & Submit page Billing Address section is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressEdit))
						{
							Pass("Edit link displayed for 'Billing Address' section ");
						}
						else
						{
							Fail("Edit link not displayed for 'Billing Address' section");
						}
					}
					else
					{
						Fail("Review & Submit page Billing Address section is not displayed");
					}
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethod))
					{
						Pass("Review & Submit page Billing Method section is displayed");
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodEdit))
						{
							Pass("Edit link displayed for 'Payment Method' section ");
						}
						else
						{
							Fail("Edit link not displayed for 'Payment Method' section");
						}
					}
					else
					{
						Fail("Review & Submit page Billing Method section is not displayed");
					}						
				}
				else
				{
					Fail("Review & Submit page not displayed");
				}
			}			
			catch(Exception e)
			{
				System.out.println(" HBC - 1403 Issue ." + e.getMessage());
				Exception(" HBC - 1403 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1404 Guest Checkout - Verify that saved "Shipping Address", "Shipping Method", "Billing Address", "Payment Method" should be shown along with "Edit" text links in "Review & Submit" section.*/
	public void HBC1404ReviewAndSubmitSavedDetails()
	{
		ChildCreation(" HBC - 1404 Guest Checkout - Verify that saved Shipping Address, Shipping Method, Billing Address, Payment Method should be shown along with Edit text links in Review & Submit section.");
		if(reviewSub==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressDetails))
				{
					Pass("Review & Submit page Shipping Address Details section is displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressEdit))
					{
						Pass("Edit link displayed for 'Shipping Address' section ");
					}
					else
					{
						Fail("Edit link not displayed for 'Shipping Address' section");
					}
				}
				else
				{
					Fail("Review & Submit page Shipping Address Details section is not displayed");
				}	
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodSelected))
				{
					Pass("Review & Submit page Shipping Method section details is displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodEdit))
					{
						Pass("Edit link displayed for 'Shipping Method' section ");
					}
					else
					{
						Fail("Edit link not displayed for 'Shipping Method' section");
					}
				}
				else
				{
					Fail("Review & Submit page Shipping Method section details is not displayed");
				}
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressDetails))
				{
					Pass("Review & Submit page Billing Address section details is displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressEdit))
					{
						Pass("Edit link displayed for 'Billing Address' section ");
					}
					else
					{
						Fail("Edit link not displayed for 'Billing Address' section");
					}
				}
				else
				{
					Fail("Review & Submit page Billing Address section details is not displayed");
				}
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodDetails))
				{
					Pass("Review & Submit page Billing Method section Details is displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodEdit))
					{
						Pass("Edit link displayed for 'Payment Method' section ");
					}
					else
					{
						Fail("Edit link not displayed for 'Payment Method' section");
					}
				}
				else
				{
					Fail("Review & Submit page Billing Method section Details is not displayed");
				}						
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1404 Issue ." + e.getMessage());
				Exception(" HBC - 1404 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1405 Guest Checkout - Verify that page should navigates to "Addresses" section on tapping the "Edit" link near the Shipping Address text.*/
	public void HBC1405ReviewAndSubmitShippingEditToAddressSection()
	{
		ChildCreation(" HBC - 1405 Guest Checkout - Verify that page should navigates to Addresses section on tapping the Edit link near the Shipping Address text.");
		if(reviewSub==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressDetails))
				{
					Pass("Review & Submit page Shipping Address Details section is displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressEdit))
					{
						Pass("Edit link displayed for 'Shipping Address' section ");
						ReviewandSubmitShippingAddressEdit.click();
						wait.until(ExpectedConditions.visibilityOf(chkoutAddressPage));
						if(HBCBasicfeature.isElementPresent(chkoutAddressPage))
						{
							reviewSub = false;
							Pass("page navigates to 'Addresses' section on tapping the 'Edit' link near the Shipping Address text");
							if(HBCBasicfeature.isElementPresent(AddressSubmit))
							{
								HBCBasicfeature.scrolldown(AddressSubmit, driver);
								jsclick(AddressSubmit);
								try
								{
									wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
									if(HBCBasicfeature.isElementPresent(addressuseasEntered))
									{
										jsclick(addressuseasEntered);
										wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
										if(HBCBasicfeature.isElementPresent(shipandPaypage))
										{
											Pass("Shipping & Payment page displayed");
										}
										else
										{
											Fail( "The Shippging & Payment Page is not dispalyed.");
										}
									}
									else
									{
										Fail( "The Use as Entered button is not dispalyed.");
									}
								}
								catch(Exception e)
								{
									Pass(" No Address verification overlay displayed");
								}
								
								wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
								if(HBCBasicfeature.isElementPresent(shipandPaypage))
								{
									Pass("Shipping & payment page displayed");
									if(HBCBasicfeature.isElementPresent(shipandPaypageShipPayToName))
									{
										Pass("Billing Address dropdown displayed");
										/*Select sel = new Select(shipandPaypageShipPayToName);
										Thread.sleep(200);
										sel.selectByVisibleText("Abrams JJ");
										Thread.sleep(1000);*/
										wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
										Thread.sleep(1000);											
										reviewSub = AddPaymentReviewOrderClick();
										if(reviewSub==true)
										{
											wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
											if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
											{
												Pass("Review & Submit page displayed");
											}
											else
											{
												Fail("Review & Submit page is not displayed");
											}
										}
										else
										{
											Fail("Review & Submit page not displayed");
										}
									}
									else
									{
										Fail("Billing Address dropdown not displayed");
									}
								}
								else
								{
									Fail("Shipping & payment page not displayed");
								}
							}
							else
							{
								Fail("Address submit option not displayed");
							}
						}
						else
						{
							Fail("page not navigates to 'Addresses' section on tapping the 'Edit' link near the Shipping Address text");
						}
					}
					else
					{
						Fail("Edit link not displayed for 'Shipping Address' section");
					}
				}
				else
				{
					Fail("Review & Submit page Shipping Address Details section is not displayed");
				}	
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1405 Issue ." + e.getMessage());
				Exception(" HBC - 1405 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1406 Guest Checkout - Verify that page should navigates to "Shipping & Payment" section on tapping the "Edit" link near the Shipping Method text.*/
	public void HBC1406ReviewAndSubmitShippingMethodEditToShipPayment()
	{
		ChildCreation(" HBC - 1406 Guest Checkout - Verify that page should navigates to Shipping & Payment section on tapping the Edit link near the Shipping Method text.");
		if(reviewSub==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodSelected))
				{
					Pass("Review & Submit page Shipping Method Details section is displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodEdit))
					{
						Pass("Edit link displayed for 'Shipping Method' section ");
						jsclick(ReviewandSubmitShippingMethodEdit);
						wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
						if(HBCBasicfeature.isElementPresent(shipandPaypage))
						{
							reviewSub =false;
							Pass("page navigates to 'Shipping & Payment' section on tapping the 'Edit' link near the Shipping Method text");
							reviewSub = AddPaymentReviewOrderClick();
							if(reviewSub==true)
							{
								wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
								if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
								{
									Pass("Review & Submit page displayed");
								}
								else
								{
									Fail(" The Review and Submit Page is not displayed.");
								}
							}
							else
							{
								Fail("Review & Submit page not displayed");
							}
						}										
						else
						{
							Fail("Shipping & payment page not displayed on tapping the 'Edit' link near the Shipping Method text");
						}							
					}
					else
					{
						Fail("Edit link not displayed for 'Shipping Method' section");
					}
				}
				else
				{
					Fail("Review & Submit page Shipping Method Details section is not displayed");
				}	
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1406 Issue ." + e.getMessage());
				Exception(" HBC - 1406 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1414 Guest Checkout - Verify that details in the "Review & Submit" section must be displayed with the modified changes on making changes and selecting "Ok" button.*/
	/* HBC - 1407 Guest Checkout - Verify that page should navigates to "Addresses" section on tapping the "Edit" link near the Billing Address text.*/
	public void HBC1414ReviewAndSubmitAfterModification()
	{
		ChildCreation(" HBC - 1414 Guest Checkout - Verify that details in the Review & Submit section must be displayed with the modified changes on making changes and selecting Ok button.");
		boolean HBC1407 = false;
		if(reviewSub==true)
		{
			try
			{
				String name = HBCBasicfeature.getExcelNumericVal("HBC1414", sheet, 1);
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressDetails))
				{
					Pass("Shipping Address Details are displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressNameDetails))
					{
						String currentName = ReviewandSubmitBillingAddressNameDetails.getText();
						log.add("Phone Number displayed currently is: "+currentName);
						if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressEdit))
						{
							Pass("Edit link displayed for 'Billing Address' section ");
							jsclick(ReviewandSubmitBillingAddressEdit);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOf(chkoutAddressPage));
							if(HBCBasicfeature.isElementPresent(chkoutAddressPage))
							{
								reviewSubmit=false;
								HBC1407 = true;
								Pass("Address Page displayed");
								if(HBCBasicfeature.isElementPresent(firstName))
								{
									HBCBasicfeature.scrolldown(firstName, driver);
									firstName.clear();
									firstName.sendKeys(name);
									Thread.sleep(500);
									HBCBasicfeature.scrolldown(AddressSubmit, driver);
									jsclick(AddressSubmit);
									try
									{
										wait.until(ExpectedConditions.attributeContains(addressVerificationoverlay, "style", "block"));
										if(HBCBasicfeature.isElementPresent(addressuseasEntered))
										{
											jsclick(addressuseasEntered);
											wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
											if(HBCBasicfeature.isElementPresent(shipandPaypage))
											{
												Pass("Shipping & Payment page displayed");
											}
											else
											{
												Fail( "The Shipping & Payment page is not displayed.");
											}
										}
										else
										{
											Fail( "The Use as Entered Button is not displayed.");
										}
									}
									catch(Exception e)
									{
										Pass("No Address verification overlay displayed");
									}
									if(HBCBasicfeature.isElementPresent(shipandPaypage))
									{
										Pass("Shipping & payment page displayed");
										if(HBCBasicfeature.isElementPresent(shipandPaypageShipPayToName))
										{
											Pass("Billing Address dropdown displayed");
											/*Select sel = new Select(shipandPaypageShipPayToName);
											Thread.sleep(200);
											sel.selectByVisibleText("Bat JJ");
											Thread.sleep(1000);
											wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));*/
											Thread.sleep(1000);											
											boolean reviewSub = AddPaymentReviewOrderClick();
											if(reviewSub==true)
											{
												wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
												if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
												{
													Pass("Review & Submit page displayed");
													if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressNameDetails))
													{
														String UpdatedName = ReviewandSubmitBillingAddressNameDetails.getText();
														log.add("Phone Number displayed after update is: "+UpdatedName);
														Thread.sleep(500);
														if(currentName.equalsIgnoreCase(UpdatedName))
														{
															Fail("'Review & Submit' section not displayed with the modified changes on making changes and selecting 'Ok' button",log);
														}
														else
														{
															Pass("'Review & Submit' section displayed with the modified changes on making changes and selecting 'Ok' button",log);
														}
													}
													else
													{
														Fail("BillingAddress Name not displayed");
													}
												}
												else
												{
													Fail("Chceckout review & Submit tab not displayed");
												}
											}
											else
											{
												Fail("Chceckout review & Submit page not displayed");
											}
										}
										else
										{
											Fail("Payment  section Select address dropdown not displayed");
										}
									}
									else
									{
										Fail("Shipping & payment page not displayed");
									}
								}
								else
								{
									Fail("Addresss section Firstname field not displayed");
								}									
							}
							else
							{
								Fail("Addresss section page not displayed");
							}		
						}	
						else
						{
							Fail("Edit link not displayed for 'Billing Address' section");
						}	
					}
					else
					{
						Fail("BillingAddress Name not displayed");
					}
				}	
				else
				{
					Fail("BillingAddress Name not displayed");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1414 Issue ." + e.getMessage());
				Exception(" HBC - 1414 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
			
		/* HBC - 1407 Guest Checkout - Verify that page should navigates to "Addresses" section on tapping the "Edit" link near the Billing Address text.*/
		ChildCreation(" HBC - 1407 Guest Checkout - Verify that page should navigates to Addresses section on tapping the Edit link near the Billing Address text.");
		if(reviewSub==true)
		{
			if(HBC1407==true)
			{
				Pass("Page navigates to 'Addresses' section on tapping the 'Edit' link near the Billing Address text");
			}
			else
			{
				Fail("Page not navigates to 'Addresses' section on tapping the 'Edit' link near the Billing Address text");
			}			
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1408 Guest Checkout - Verify that page should navigates to "Shipping & Payment" section on tapping the "Edit" link near the Payment Method text.*/
	public void HBC1408ReviewAndSubmitPaymentMethodEditToShipPayment()
	{
		ChildCreation(" HBC - 1408 Guest Checkout - Verify that page should navigates to Shipping & Payment section on tapping the Edit link near the Payment Method text.");
		if(reviewSub==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodDetails))
				{
					Pass("Review & Submit page Payment Method Details section is displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodEdit))
					{
						Pass("Edit link displayed for 'Payment Method' section ");
						jsclick(ReviewandSubmitBillingMethodEdit);
						wait.until(ExpectedConditions.visibilityOf(shipandPaypage));
						if(HBCBasicfeature.isElementPresent(shipandPaypage))
						{
							reviewSub=false;
							Pass("page navigates to 'Shipping & Payment' section on tapping the 'Edit' link near the Payment Method text");
							reviewSub = AddPaymentReviewOrderClick();
							if(reviewSub==true)
							{
								wait.until(ExpectedConditions.visibilityOf(ReviewandSubmitPage));
								if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
								{
									Pass("Review & Submit page displayed");
								}
								else
								{
									Fail("Review & Submit page is not displayed");
								}
							}
							else
							{
								Fail("Review & Submit page not displayed");
							}
						}										
						else
						{
							Fail("Shipping & payment page not displayed on tapping the 'Edit' link near the Payment Method text");
						}							
					}
					else
					{
						Fail("Edit link not displayed for 'Payment Method' section");
					}
				}
				else
				{
					Fail("Review & Submit page Payment Method Details section is not displayed");
				}	
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1408 Issue ." + e.getMessage());
				Exception(" HBC - 1408 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1411 Guest Checkout - Verify that price must also be updated in the Review & Submit section on updating the product quantity in the shopping bag page*/
	public void HBC1411ReviewAndSubmitPriceChangewhenQuantityUpdate()
	{
		ChildCreation(" HBC - 1411 Guest Checkout - Verify that price must also be updated in the Review & Submit section on updating the product quantity in the shopping bag page.");
		if(reviewSub==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
				{
					Pass("Review & Submit page displayed");
					if(HBCBasicfeature.isElementPresent(ReviewandSubmitOrderTotal))
					{
						HBCBasicfeature.scrolldown(ReviewandSubmitOrderTotal, driver);
						String currentPrice = ReviewandSubmitOrderTotal.getText().replaceAll("$", "");
						Thread.sleep(500);
						log.add("Current order total price is: "+ currentPrice);
						HBCBasicfeature.scrollup(bagIcon, driver);
						jsclick(bagIcon);
						wait.until(ExpectedConditions.visibilityOf(shoppingBagPage));			
						if(HBCBasicfeature.isElementPresent(shoppingBagPage))
						{
							log.add("Shopping Bag page displayed");
							int qnty = Integer.parseInt(prodQuanty.getAttribute("value"));
							log.add("Product count is: "+qnty);
							if(qnty<5)
							{
								prodQuantyInc.click();	
								Thread.sleep(1000);
								wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
								if(HBCBasicfeature.isElementPresent(checkoutButton))
								{
									jsclick(checkoutButton);
									wait.until(ExpectedConditions.visibilityOf(guestContinueBtn));
									Thread.sleep(1000);
									jsclick(guestContinueBtn);
									Thread.sleep(1000);
									wait.until(ExpectedConditions.attributeContains(shippingPaymentPage, "style", "visible"));
									if(HBCBasicfeature.isElementPresent(shippingPaymentPage))
									{
										Pass("shipping & payment page displayed");
										reviewSub = AddPaymentReviewOrderClick();
										if(reviewSub==true)
										{
											if(HBCBasicfeature.isElementPresent(ReviewandSubmitPage))
											{
												Pass("Review & Submit page displayed");
												if(HBCBasicfeature.isElementPresent(ReviewandSubmitOrderTotal))
												{
													HBCBasicfeature.scrolldown(ReviewandSubmitOrderTotal, driver);
													String UpdatedPrice = ReviewandSubmitOrderTotal.getText().replaceAll("$", "");
													Thread.sleep(500);
													log.add("Updated order total price is: "+ UpdatedPrice);
													if(currentPrice==UpdatedPrice)
													{
														Fail("Price not updated in the 'Review & Submit' section on updating the product quantity in the shopping bag",log);
													}
													else
													{
														Pass("Price updated in the 'Review & Submit' section on updating the product quantity in the shopping bag",log);
													}
												}
												else
												{
													Fail("OrderTotal not displayed");
												}
											}
											else
											{
												Fail("Review & submit page not displayed");
											}
										}
										else
										{
											Fail("Review & submit page not displayed");
										}
									}
									else
									{
										Fail("shipping & payment page not displayed");
									}
								}
								else
								{
									Fail("Checkout button not displayed");
								}											
							}
							else
							{
								Fail("Cant able to add more tha 5 products",log);
							}
						}
						else
						{
							Fail("Shopping bag page not displayed");
						}
					}
					else
					{
						Fail("OrderTotal not displayed");
					}
				}
				else
				{
					Fail("Review & submit page not displayed");
				}						
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1411 Issue ." + e.getMessage());
				Exception(" HBC - 1411	 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1412 Guest Checkout - Verify that "Shipping Address", "Shipping Method", "Billing Address", "Payment Method" must be exactly same as when entered in the forms.*/
	public void HBC1412ReviewAndSubmitSavedDetailsCompare()
	{
		ChildCreation(" HBC - 1412 Guest Checkout - Verify that Shipping Address, Shipping Method, Billing Address, Payment Method must be exactly same as when entered in the forms.");
		if(reviewSubmit==true)
		{
			try
			{
				String actual = HBCBasicfeature.getExcelVal("HBCCard", sheet, 1);
				String cnumber = HBCBasicfeature.getExcelNumericVal("HBCCard", sheet, 2);
				String[] rsBillAddress , BillAddres, rspaymethod  ;
				String[] rsShipAddress , shipAddres;
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingAddressDetails))
				{
					Pass("Shipping Address Details are displayed");
					rsShipAddress = ReviewandSubmitShippingAddressDetails.getText().trim().split("\n");
					Thread.sleep(200);
					log.add("Entered details in form is: "+shipAddress);
					shipAddres = shipAddress.replaceAll(",", "").trim().split("\n");
					for(int i=0;i<2;i++)
					{							
						log.add("The current dispalyed Shipping Address Detail: "+rsShipAddress[i]);
						Thread.sleep(200);
						if(shipAddres[i].isEmpty()&&rsShipAddress[i].isEmpty())
						{
							Fail("Address details are empty",log);
						}
						else if(shipAddres[i].contains(rsShipAddress[i]))
						{
							Pass("'Shipping Address'detail exactly same as when entered in the forms",log);
						}
						else
						{
							Fail("Shipping Address' not same as when entered in the forms",log);
						}						
					}					
				}
				else
				{
					Fail("Review & Submit page Shipping Address section is not displayed");
				}
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitShippingMethodSelected))
				{
					Pass("Review & Submit page Shipping Method section details is displayed");
					String rsShippingMethod = ReviewandSubmitShippingMethodSelected.getText().trim();
					Thread.sleep(200);
					log.add("The current dispalyed Shipping Method Detail: "+rsShippingMethod);
					log.add("Entered Shipping Method details in form is: "+shipMethod);
					Thread.sleep(200);
					if(shipMethod.isEmpty()&&rsShippingMethod.isEmpty())
					{
						Fail("Shipping Mentod detail is empty",log);
					}
					else if(shipMethod.contains(rsShippingMethod))
					{
						Pass("'Shipping Method' exactly same as when entered in the forms",log);
					}
					else
					{
						Fail("Shipping Method' not same as when entered in the forms",log);
					}
				}
				else
				{
					Fail("Review & Submit page Shipping Method section details is not displayed");
				}
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingAddressDetails))
				{
					Pass("Review & Submit page Shipping Method section details is displayed");
					rsBillAddress = ReviewandSubmitBillingAddressDetails.getText().trim().split("\n");
					Thread.sleep(200);
					log.add("Entered Billing Address details in form is: "+billAddress);
					BillAddres = shipAddress.replaceAll(",", "").split("\n");
					Thread.sleep(200);
					for(int i=0;i<2;i++)
					{	
						log.add("The current dispalyed Billing Address Detail: "+rsBillAddress[i]);
						if(BillAddres[i].isEmpty()&&rsBillAddress[i].isEmpty())
						{
							Fail("Billing address detail is empty",log);
						}
						else if(BillAddres[i].contains(rsBillAddress[i]))
						{
							Pass("'Billing address' exactly same as when entered in the forms",log);
						}
						else
						{
							Fail("Billing address' not same as when entered in the forms",log);
						}
					}
				}
				else
				{
					Fail("Review & Submit page Billing Address section details is not displayed");
				}
				if(HBCBasicfeature.isElementPresent(ReviewandSubmitBillingMethodDetails))
				{
					Pass("Review & Submit page Billing Method section details is displayed");
					rspaymethod = ReviewandSubmitBillingMethodDetails.getText().trim().split("\n");
					Thread.sleep(200);
					log.add("Entered  Billing Method  details in form is: "+actual+" and "+cnumber);	
					String snum = rspaymethod[1].substring(12);
					String eNum = cnumber.substring(12);
					Thread.sleep(200);
					if(rspaymethod[0].isEmpty()&&rspaymethod[1].isEmpty())
					{
						Fail(" Billing Method detail is empty",log);
					}
					else if(rspaymethod[0].equalsIgnoreCase(actual)&&snum.contains(eNum))
					{
						Pass("' Billing Method ' exactly same as when entered in the forms",log);
					}
					else
					{
						Fail("' Billing Method ' not same as when entered in the forms",log);
					}
				}
				else
				{
					Fail("Review & Submit page Billing Method  section details is not displayed");
				}					
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1412 Issue ." + e.getMessage());
				Exception(" HBC - 1412 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1415 Guest Checkout - Verify that "HBC Reward Points Earned" text should be highlighted in bold.*/
	public void HBC1415ReviewHBCEarnedText()
	{
		ChildCreation("HBC - 1415 Guest Checkout - Verify that HBC Reward Points Earned text should be highlighted in bold");
		if(reviewSub==true)
		{
			try
			{
				String expCol = HBCBasicfeature.getExcelVal("HBC1415", sheet, 1);
				if(HBCBasicfeature.isElementPresent(reviewTabHBCText))
				{
					Pass( "The HBC Earned Point text is displayed.");
					String txtColor = reviewTabHBCText.getCssValue("color");
					Color colhex = Color.fromString(txtColor);
					String actColor = colhex.asHex();
					log.add( "The Expected Color was : " + expCol);
					log.add( "The Actuak Color is : " + actColor);
					if(expCol.equalsIgnoreCase(actColor))
					{
						Pass( "The HBC Reward pint text is highlighted in the expected color.",log);
					}
					else
					{
						Fail( "The HBC Reward pint text is not highlighted in the expected color.",log);
					}
				}
				else
				{
					Fail( "The HBC Earned Point text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1415 Issue ." + e.getMessage());
				Exception(" HBC - 1415 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1416 Guest Checkout - Verify that "Shipping Amount", "PST", "GST/HST" text should be shown in greyed out color.*/
	public void HBC1416ReviewSummaryContainerGreyedOut()
	{
		ChildCreation(" HBC - 1416 Guest Checkout - Verify that Shipping Amount, PST, GST/HST text should be shown in greyed out color.");
		if(reviewSub==true)
		{
			try
			{
				String expCol = HBCBasicfeature.getExcelVal("HBC1416", sheet, 1);
				if(HBCBasicfeature.isElementPresent(reviewshippingamounttxt))
				{
					Pass( "The Shipping Amount Label is displayed.");
					String shipCol = reviewshippingamounttxt.getCssValue("color");
					Color Col = Color.fromString(shipCol);
					String colhex = Col.asHex();
					log.add( "The Expected Color was : " + expCol);
					log.add( "The actual  Color is : " + colhex);
					if(expCol.equals(colhex))
					{
						Pass( "The Expected and the actual color matches.",log);
					}
					else
					{
						Fail( "The Expected and the actual color does not matches.",log);
					}
				}
				else
				{
					Fail( "The Shipping Amount text is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(reviewshippingamount))
				{
					Pass( "The Shipping Amount is displayed.");
					String shipCol = reviewshippingamount.getCssValue("color");
					Color Col = Color.fromString(shipCol);
					String colhex = Col.asHex();
					log.add( "The Expected Color was : " + expCol);
					log.add( "The actual  Color is : " + colhex);
					if(expCol.equals(colhex))
					{
						Pass( "The Expected and the actual color matches.",log);
					}
					else
					{
						Fail( "The Expected and the actual color does not matches.",log);
					}
				}
				else
				{
					Fail( "The Shipping Amount is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(reviewpsttxt))
				{
					Pass( "The PST Text is displayed.");
					String shipCol = reviewpsttxt.getCssValue("color");
					Color Col = Color.fromString(shipCol);
					String colhex = Col.asHex();
					log.add( "The Expected Color was : " + expCol);
					log.add( "The actual  Color is : " + colhex);
					if(expCol.equals(colhex))
					{
						Pass( "The Expected and the actual color matches.",log);
					}
					else
					{
						Fail( "The Expected and the actual color does not matches.",log);
					}
				}
				else
				{
					Fail( "The PST Text is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(reviewpstamount))
				{
					Pass( "The PST Amount is displayed.");
					String shipCol = reviewpstamount.getCssValue("color");
					Color Col = Color.fromString(shipCol);
					String colhex = Col.asHex();
					log.add( "The Expected Color was : " + expCol);
					log.add( "The actual  Color is : " + colhex);
					if(expCol.equals(colhex))
					{
						Pass( "The Expected and the actual color matches.",log);
					}
					else
					{
						Fail( "The Expected and the actual color does not matches.",log);
					}
				}
				else
				{
					Fail( "The PST Amount is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(reviewgsttxt))
				{
					Pass( "The Gst Text is displayed.");
					String shipCol = reviewgsttxt.getCssValue("color");
					Color Col = Color.fromString(shipCol);
					String colhex = Col.asHex();
					log.add( "The Expected Color was : " + expCol);
					log.add( "The actual  Color is : " + colhex);
					if(expCol.equals(colhex))
					{
						Pass( "The Expected and the actual color matches.",log);
					}
					else
					{
						Fail( "The Expected and the actual color does not matches.",log);
					}
				}
				else
				{
					Fail( "The GST Text is not displayed.");
				}
				
				if(HBCBasicfeature.isElementPresent(reviewgstamount))
				{
					Pass( "The Gst Amount is displayed.");
					String shipCol = reviewgstamount.getCssValue("color");
					Color Col = Color.fromString(shipCol);
					String colhex = Col.asHex();
					log.add( "The Expected Color was : " + expCol);
					log.add( "The actual  Color is : " + colhex);
					if(expCol.equals(colhex))
					{
						Pass( "The Expected and the actual color matches.",log);
					}
					else
					{
						Fail( "The Expected and the actual color does not matches.",log);
					}
				}
				else
				{
					Fail( "The GST Amount is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1416 Issue ." + e.getMessage());
				Exception(" HBC - 1416 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1417 Guest Checkout - Verify that Order details page must be displayed on tapping the "Place Order" button.*/
	public void HBC1417ReviewAndSubmitPlaceOrderToOrderConfrim()
	{
		ChildCreation(" HBC - 1417 Guest Checkout - Verify that Order details page must be displayed on tapping the Place Order button.");
		if(reviewSub==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(placeOrder))
				{
					HBCBasicfeature.scrolldown(placeOrder, driver);
					jsclick(placeOrder);
					wait.until(ExpectedConditions.attributeContains(confirmOrderPage, "style", "visible"));
					if(HBCBasicfeature.isElementPresent(confirmOrderPage))
					{
						Pass("Order details page displayed on tapping the 'Place Order' button");
						orderConfirm = true;
					}
					else
					{
						Fail("Order details page not displayed on tapping the 'Place Order' button");
					}						
				}
				else
				{
					Fail("Place order button not displayed");
				}					
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1417 Issue ." + e.getMessage());
				Exception(" HBC - 1417 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1418 Guest Checkout - Verify that "Order Confirmation" page, should be as per the creative*/
	public void HBC1418OrderConfirmationCreative()
	{
		ChildCreation(" HBC - 1418 Guest Checkout - Verify that Order Confirmation page, should be as per the creative");
		if(orderConfirm==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(confirmOrderPage))
				{
					Pass("Order details page displayed");
					if(HBCBasicfeature.isElementPresent(confirmThanksSection))
					{
						Pass("Order details Thanks for order section displayed");
					}
					else
					{
						Fail("Order details Thanks for order section not displayed");
					}
					if(HBCBasicfeature.isElementPresent(confirmShippingInfoSection))
					{
						Pass("Order details Shipping Info section displayed");
					}
					else
					{
						Fail("Order details Shipping Info section not displayed");
					}
					if(HBCBasicfeature.isElementPresent(confirmPaymentInfoSection))
					{
						Pass("Order details Payment Info section displayed");
					}
					else
					{
						Fail("Order details Payment Info section not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(confirmPaymentDetailSection))
					{
						Pass("Order details Payment Detail section displayed");
					}
					else
					{
						Fail("Order details Payment Detail section not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(confirmRewardDetailSection))
					{
						Pass("Order details Reward Detail section displayed");
					}
					else
					{
						Fail("Order details Reward Detail section not displayed");
					}		
					if(HBCBasicfeature.isElementPresent(confirmsubTotalSection))
					{
						Pass("Order details subTotal section displayed");
					}
					else
					{
						Fail("Order details subTotal section not displayed");
					}		
					if(HBCBasicfeature.isElementPresent(confirmPromotionSection))
					{
						Pass("Order details Promotion section displayed");
					}
					else
					{
						Fail("Order details Promotion section not displayed");
					}		
					if(HBCBasicfeature.isElementPresent(confirmShippingTaxesSection))
					{
						HBCBasicfeature.scrolldown(confirmShippingTaxesSection, driver);
						Pass("Order details ShippingTaxes section displayed");
					}
					else
					{
						Fail("Order details ShippingTaxes section not displayed");
					}	
					if(HBCBasicfeature.isElementPresent(confirmOrderTotalSection))
					{
						Pass("Order Total section displayed");
					}
					else
					{
						Fail("Order Total not displayed");
					}
					/*if(HBCBasicfeature.isElementPresent(signupEmails))
					{
						HBCBasicfeature.scrolldown(signupEmails, driver);
						Pass("SignUp for Emails section displayed");
					}
					else
					{
						Fail("SignUp for Emails section not displayed");
					}	*/
				}
				else
				{
					Fail("Order details page not displayed");
				}					
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1418 Issue ." + e.getMessage());
				Exception(" HBC - 1418 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1419 Guest Checkout - Verify that "Thank you for your order!. You will receive...." text should be shown in the "Order Confirmation" page and should be as per the creative.*/
	public void HBC1419OrderConfirmationThanks()
	{
		ChildCreation(" HBC - 1419 Guest Checkout - Verify that Thank you for your order!. You will receive.... text should be shown in the Order Confirmation page and should be as per the creative.");
		if(orderConfirm==true)
		{
			try
			{
				String[] expMsg = HBCBasicfeature.getExcelVal("HBC1419", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(confirmOrderPage))
				{
					Pass("Order details page displayed");
					if(HBCBasicfeature.isElementPresent(confirmThanksSection))
					{
						Pass("Order details Thanks for order section displayed");
						log.add( "The Expected value was : " + expMsg[0].toString());
						String val = confirmThanksSection.getText();
						log.add( "The actual value is : " + val);
						if(expMsg[0].toString().contains(val))
						{
							Pass( "The Expected and the actual color matches.",log);
						}
						else
						{
							Fail( "The Expected and the actual color does not matches.",log);
						}
					}
					else
					{
						Fail("Order details Thanks for order section not displayed");
					}
					
					if(HBCBasicfeature.isListElementPresent(revieworderDetailsText))
					{
						Pass(" Order details Thanks for order section displayed");
						log.add( "The Expected value was : " + expMsg[1].toString());
						String val = revieworderDetailsText.get(0).getText();
						log.add( "The actual value is : " + val);
						if(expMsg[1].toString().contains(val))
						{
							Pass( "The Expected and the actual color matches.",log);
						}
						else
						{
							Fail( "The Expected and the actual color does not matches.",log);
						}
						
						log.add( "The Expected value was : " + expMsg[2].toString());
						val = revieworderDetailsText.get(1).getText();
						log.add( "The actual value is : " + val);
						if(expMsg[2].toString().contains(val))
						{
							Pass( "The Expected and the actual color matches.",log);
						}
						else
						{
							Fail( "The Expected and the actual color does not matches.",log);
						}
					}
					else
					{
						Fail("Order details Thanks for order section not displayed");
					}
				}
				else
				{
					Fail("Order details page not displayed");
				}					
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1419 Issue ." + e.getMessage());
				Exception(" HBC - 1419 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1420 Guest Checkout - Verify that "Order Number", "Order Date" should be shown in the order confirmation page below the thank you text.*/
	public void HBC1420OrderConfirmationNumberDate()
	{
		ChildCreation(" HBC - 1420 Guest Checkout - Verify that Order Number, Order Date should be shown in the order confirmation page below the thank you text.");
		if(orderConfirm==true)
		{
			try
			{
				String[] expMsg = HBCBasicfeature.getExcelVal("HBC1419", sheet, 1).split("\n");
				if(HBCBasicfeature.isElementPresent(confirmOrderPage))
				{
					Pass("Order details page displayed");
					if(HBCBasicfeature.isListElementPresent(revieworderDetailsText))
					{
						Pass(" Order details Thanks for order section displayed");
						log.add( "The Expected value was : " + expMsg[1].toString());
						String val = revieworderDetailsText.get(0).getText();
						log.add( "The actual value is : " + val);
						if(expMsg[1].toString().contains(val))
						{
							Pass( "The Expected and the actual color matches.",log);
						}
						else
						{
							Fail( "The Expected and the actual color does not matches.",log);
						}
						
						log.add( "The Expected value was : " + expMsg[2].toString());
						val = revieworderDetailsText.get(1).getText();
						log.add( "The actual value is : " + val);
						if(expMsg[2].toString().contains(val))
						{
							Pass( "The Expected and the actual color matches.",log);
						}
						else
						{
							Fail( "The Expected and the actual color does not matches.",log);
						}
					}
					else
					{
						Fail("Order details Thanks for order section not displayed");
					}
				}
				else
				{
					Fail("Order details page not displayed");
				}					
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1420 Issue ." + e.getMessage());
				Exception(" HBC - 1420 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1422 Guest Checkout - Verify that shipping address are displayed in the "Order Confirmation" page */
	public void HBC1422OrderConfirmationShippingAddress()
	{
		ChildCreation(" HBC - 1422 Guest Checkout - Verify that shipping address are displayed in the Order Confirmation page");
		if(orderConfirm==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(confirmShippingInfoSection))
				{
					HBCBasicfeature.scrollup(confirmShippingInfoSection, driver);
					String shipping =  confirmShippingInfoSection.getText();
					log.add("shipping address displayed is: "+shipping);
					Pass("Shipping address shown in the order confirmation page",log);
				}
				else
				{
					Fail("Shipping address not shown in the order confirmation page",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1422 Issue ." + e.getMessage());
				Exception(" HBC - 1422 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1422 Guest Checkout - Verify that Billing address are displayed in the "Order Confirmation" page*/
	public void HBC1423OrderConfirmationBillingAddress()
	{
		ChildCreation(" HBC - 1422 Guest Checkout - Verify that Billing address are displayed in the Order Confirmation page.");
		if(orderConfirm==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(reviewBillinSummarySec))
				{
					HBCBasicfeature.scrollup(reviewBillinSummarySec, driver);
					String billing =  reviewBillinSummarySec.getText();
					log.add("Billing address displayed is: "+ billing);
					Pass("Billing address shown in the order confirmation page",log);
				}
				else
				{
					Fail(" Billing address not shown in the order confirmation page",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1423 Issue ." + e.getMessage());
				Exception(" HBC - 1423 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1424 Guest Checkout - Verify that payment details should be displayed in "Order Confirmation" page	*/
	public void HBC1424OrderConfirmationPaymentdetails()
	{
		ChildCreation(" HBC - 1424 Guest Checkout - Verify that payment details should be displayed in Order Confirmation page.");
		if(orderConfirm==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(confirmPaymentDetailSection))
				{
					HBCBasicfeature.scrollup(confirmPaymentDetailSection, driver);
					String shipping =  confirmPaymentDetailSection.getText();
					log.add("Payment details displayed is: "+shipping);
					Pass("Payment details shown in the order confirmation page",log);
				}
				else
				{
					Fail("Payment details not shown in the order confirmation page",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1424 Issue ." + e.getMessage());
				Exception(" HBC - 1424 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HBC - 1425 Guest Checkout - Verify that "Order Confirmation" Page has the "Create Account" and "Print" buttons.*/
	public void HBC1425OrderConfirmationCreateAccountBtns()
	{
		ChildCreation(" HBC - 1425 Guest Checkout - Verify that Order Confirmation Page has the Create Account and Print buttons..");
		if(orderConfirm==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(reviewOrderCreateAccount))
				{
					Pass(" The Create Account Button is in the order confirmation page",log);
				}
				else
				{
					Fail("The Create Account Button is not in the order confirmation page",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1425 Issue ." + e.getMessage());
				Exception(" HBC - 1425 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
	
	/* HNC - 1426 Guest Checkout - Verify that "Register account" page should be displayed on selecting "Create Account" button.*/
	public void HBC1426CreateAccountNavigation()
	{
		ChildCreation("HBC - 1426 Guest Checkout - Verify that Register account page should be displayed on selecting Create Account button ");
		if(orderConfirm==true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(reviewOrderCreateAccount))
				{
					Pass(" The Create Account Button is in the order confirmation page",log);
					jsclick(reviewOrderCreateAccount);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(reviewOrderCreateAccountPage));
					if(HBCBasicfeature.isElementPresent(reviewOrderCreateAccountPage))
					{
						Pass( "The User is navigated to the Create Account Page.");
					}
					else
					{
						Fail( "The User is not navigated to the Create Account Page.");
					}
				}
				else
				{
					Fail("The Create Account Button is not in the order confirmation page",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" HBC - 1426 Issue ." + e.getMessage());
				Exception(" HBC - 1426 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip ( " The User was unable to select the address from the verification page in the checkout page.");
		}
	}
}

