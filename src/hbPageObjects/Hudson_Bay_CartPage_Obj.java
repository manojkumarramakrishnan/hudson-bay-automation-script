package hbPageObjects;

import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_CartPage;

public class Hudson_Bay_CartPage_Obj extends Hudson_Bay_CartPage implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_CartPage_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
		
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath=""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+pdppageATBb+"")
	WebElement pdppageATB;	
	
	@FindBy(xpath = ""+PDPProdNamee+"")
	WebElement PDPProdName;		
	
	@FindBy(xpath = ""+homepagee+"")
	WebElement homepage;	

	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;	
	
	@FindBy(xpath = ""+bagIconCountt+"")
	WebElement bagIconCount;	
	
	@FindBy(xpath = ""+PDPATBoverlayViewBagg+"")
	WebElement PDPATBoverlayViewBag;		
	
	@FindBy(xpath = ""+pdppageATBOverlayy+"")
	WebElement pdppageATBOverlay;	
	
	@FindBy(xpath = ""+pdpSuccessIdd+"")
	WebElement pdpSuccessId;	
	
	@FindBy(xpath = ""+shoppingBagPageDomm+"")
	WebElement shoppingBagPageDom;	
	
	@FindBy(xpath = ""+shoppingBagProdd+"")
	WebElement shoppingBagProd;	
	
	@FindBy(xpath = ""+shoppingBagPageProdQuickWindoww+"")
	WebElement shoppingBagPageProdQuickWindow;	
	
	@FindBy(xpath = ""+shoppingBagPageProdQuickWindowClosee+"")
	WebElement shoppingBagPageProdQuickWindowClose;	
		
	@FindBy(xpath = ""+shoppingBagPageTitlee+"")
	WebElement shoppingBagPageTitle;	
	
	@FindBy(xpath = ""+shoppingBagPageShopNoww+"")
	WebElement shoppingBagPageShopNow;	
		
	@FindBy(xpath = ""+shoppingBagPageProdTitlee+"")
	WebElement shoppingBagPageProdTitle;	
	
	@FindBy(xpath = ""+shoppingBagPageProdImagee+"")
	WebElement shoppingBagPageProdImage;
	
	@FindBy(xpath = ""+shoppingBagPageProdAvaill+"")
	WebElement shoppingBagPageProdAvail;
	
	@FindBy(xpath = ""+shoppingBagPageProdColorr+"")
	WebElement shoppingBagPageProdColor;
	
	@FindBy(xpath = ""+shoppingBagPageProdSizee+"")
	WebElement shoppingBagPageProdSize;
	
	@FindBy(xpath = ""+shoppingBagPageProdRemovee+"")
	WebElement shoppingBagPageProdRemove;

	@FindBy(xpath = ""+shoppingBagPageProdRegularPricee+"")
	WebElement shoppingBagPageProdRegularPrice;	
	
	@FindBy(xpath = ""+shoppingBagPageProdRegSalePricee+"")
	WebElement shoppingBagPageProdRegSalePrice;	
	
	@FindBy(xpath = ""+shoppingBagPageProdRegSaleSalePricee+"")
	WebElement shoppingBagPageProdRegSaleSalePrice;	
	
	@FindBy(xpath = ""+loadingGaugeDomm+"")
	WebElement loadingGaugeDom;
	
	@FindBy(xpath = ""+loadingBarrr+"")
	WebElement loadingbar;
	
	@FindBy(xpath = ""+progressBarWidgett+"")
	WebElement progressBarWidget;
	
	@FindBy(xpath = ""+prodQuantyy+"")
	WebElement prodQuanty;	
	
	@FindBy(xpath = ""+prodQuantityContt+"")
	WebElement prodQuantityCont;	
	
	@FindBy(xpath = ""+prodQuantyIncc+"")
	WebElement prodQuantyInc;	
	
	@FindBy(xpath = ""+prodQuantyDecc+"")
	WebElement prodQuantyDec;		
	
	@FindBy(xpath = ""+shoppingCartCheckoutt+"")
	WebElement shoppingCartCheckout;

	@FindBy(xpath = ""+shoppingBagPaypall+"")
	WebElement shoppingBagPaypal;
	
	@FindBy(xpath = ""+shoppingCartCheckoutt2+"")
	WebElement shoppingCartCheckout2;
	
	@FindBy(xpath = ""+shippingPaymentPagee+"")
	WebElement shippingPaymentPage;	
	
	@FindBy(xpath = ""+payPalLogoo+"")
	WebElement payPalLogo;	
	
	@FindBy(xpath = ""+payPalLoadingSpinnerr+"")
	WebElement payPalLoadingSpinner;	
	
	@FindBy(xpath = ""+payPalPageCancell+"")
	WebElement payPalPageCancel;	

	@FindBy(xpath = ""+shoppingBagPaypall2+"")
	WebElement shoppingBagPaypal2;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodee+"")
	WebElement paymentOptionsPromoCode;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodeApplyy+"")
	WebElement paymentOptionsPromoCodeApply;
	
	@FindBy(xpath = ""+paymentOptionsPromoCodeApplyInvalidAlertt+"")
	WebElement paymentOptionsPromoCodeApplyInvalidAlert;	
	
	@FindBy(xpath = ""+shoppingBagPostalCodee+"")
	WebElement shoppingBagPostalCode;
	
	@FindBy(xpath = ""+shoppingBagZipcodeEmptyAlertt+"")
	WebElement shoppingBagZipcodeEmptyAlert;
	
	@FindBy(xpath = ""+shoppingBagZipcodeInvalidAlertt+"")
	WebElement shoppingBagZipcodeInvalidAlert;	
	
	@FindBy(xpath = ""+shoppingBagShippingMethodDropdownn+"")
	WebElement shoppingBagShippingMethodDropdown;
	
	@FindBy(xpath = ""+shoppingBagShippingMethodApplyy+"")
	WebElement shoppingBagShippingMethodApply;	
	
	@FindBy(xpath = ""+shoppingBagShippingAmountTaxSectionn+"")
	WebElement shoppingBagShippingAmountTaxSection;
	
	@FindBy(xpath = ""+shoppingBageEstimatedShippingg+"")
	WebElement shoppingBageEstimatedShipping;
	
	@FindBy(xpath = ""+shoppingBageEstimatedShippingPricee+"")
	WebElement shoppingBageEstimatedShippingPrice;
	
	@FindBy(xpath = ""+shoppingBageEstimatedTaxx+"")
	WebElement shoppingBageEstimatedTax;
	
	@FindBy(xpath = ""+shoppingBageEstimatedTaxPricee+"")
	WebElement shoppingBageEstimatedTaxPrice;	
	
	@FindBy(xpath = ""+shoppingBagTotalAmountSectionn+"")
	WebElement shoppingBagTotalAmountSection;	
	
	@FindBy(xpath = ""+pancakeStaticWelcomee+"")
	WebElement pancakeStaticWelcome;	
	
	@FindBy(xpath = ""+signInPagee+"")
	WebElement signInPage;
	
	@FindBy(xpath = ""+emailFieldd+"")
	WebElement emailField;
	
	@FindBy(xpath = ""+pwdFieldd+"")
	WebElement pwdField;
	
	@FindBy(xpath = ""+signInButtonn+"")
	WebElement signInButton;
	
	@FindBy(xpath = ""+myAccountPagee+"")
	WebElement myAccountPage;
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	@FindBy(how = How.XPATH,using = ""+shoppingBagPageProdRemovee+"")
	List<WebElement> shoppingBagPageProdRemoveList;
	
	//Footer:
					
			@FindBy(xpath = ""+footerContainerr+"")
			WebElement footerContainer;	
			
			@FindBy(xpath = ""+footerTopp+"")
			WebElement footerTop;	
			
			@FindBy(xpath = ""+footerTopTitlee+"")
			WebElement footerTopTitle;
			
			@FindBy(xpath = ""+footerSubTopp+"")
			WebElement footerSubTop;
			
			@FindBy(xpath = ""+footerSubTopCalll+"")
			WebElement footerSubTopCall;
			
			@FindBy(xpath = ""+footerSubTopEmaill+"")
			WebElement footerSubTopEmail;
			
			@FindBy(xpath = ""+footerMenuListt+"")
			WebElement footerMenuList;
			
			@FindBy(xpath = ""+footerBottomm+"")
			WebElement footerBottom;
			
			@FindBy(xpath = ""+footerSocialIconn+"")
			WebElement footerSocialIcon;
			
			@FindBy(xpath = ""+footerBottomCopyrightt+"")
			WebElement footerBottomCopyright;
	
	
	boolean homepge = false, cartPage = false, prodAdded = false;
	
	String PLPtitle = "", PDPtitle = "";	

	
	/*SignIn To HomePage*/
	public boolean signin() throws java.lang.Exception
	{
		String email = HBCBasicfeature.getExcelVal("login", sheet, 3);
		String pwd = HBCBasicfeature.getExcelVal("login", sheet, 4);
		try
		{			
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(header));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{
					try
					{
						HBCBasicfeature.jsclick(hamburger, driver);
						Thread.sleep(2000);
						if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
						{
							flag = true;
							break;
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
				while(flag!=true);					
				if(HBCBasicfeature.isElementPresent(pancakeStaticWelcome))
				{
					HBCBasicfeature.scrolldown(pancakeStaticWelcome, driver);
					Thread.sleep(500);
					pancakeStaticWelcome.click();
					wait.until(ExpectedConditions.attributeContains(signInPage, "style", "visible"));
					if(HBCBasicfeature.isElementPresent(emailField))
					{	
						emailField.sendKeys(email);								
						if(HBCBasicfeature.isElementPresent(pwdField))
						{	
							pwdField.sendKeys(pwd);									
							if(HBCBasicfeature.isElementPresent(signInButton))
							{
								signInButton.click();
								Thread.sleep(500);
								wait.until(ExpectedConditions.visibilityOf(myAccountPage));			
								if(HBCBasicfeature.isElementPresent(myAccountPage))
								{	
									if(HBCBasicfeature.isElementPresent(logo))
									{										
										logo.click();
										Thread.sleep(500);
										wait.until(ExpectedConditions.visibilityOf(homepage));
										if(HBCBasicfeature.isElementPresent(homepage))
										{
											homepge = true;
										}
										else
										{
											homepge = false;
										}
									}
									else
									{
										Fail("MyProfile section displayed");
									}								
								}
								else
								{
									Fail("MyAccount page not displayed",log);
								}
							}
							else
							{
								Fail("SignIn button not displayed");
							}
						}
						else
						{
							Fail("Password Field not displayed");
						}
					}
					else
					{
						Fail("Email Field not displayed");
					}
				}
				else
				{
					Fail("Pancake WelcomesignIn option not displayed");
				}
			}
			else
			{
				Fail("Hamburger menuu not displayed");
			}				
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}	
		return homepge;
	}
	
	/*PDP Page loaded*/
	public boolean pdpLoaded() throws java.lang.Exception
	{
		String Key = HBCBasicfeature.getExcelVal("keyword", sheet, 1);
		WebElement product = null ;
		String product_BrandTitle = "";
		boolean pdploaded = false;
		try
		{			
			do
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(Key);
						searchBox.sendKeys(Keys.ENTER);
						boolean pgLoad = false;
						do
						{				
							try
							{
								Thread.sleep(1000);
								if(HBCBasicfeature.isListElementPresent(plpItemContList))		
								{
									pgLoad=true;	
									break;		
								}
							}
							catch (Exception e)
							{
								pgLoad=false;
								driver.navigate().refresh();	
								continue;
							}
						}							
						while(!pgLoad==true); 							
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							Thread.sleep(1000);
							if(HBCBasicfeature.isListElementPresent(plpItemContList))
							{							
								Thread.sleep(500);
								int size = plpItemContList.size();
								Random r  = new Random();
								int i = r.nextInt(size);
								Thread.sleep(500);
								product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
								wait.until(ExpectedConditions.visibilityOf(product));
								HBCBasicfeature.scrolldown(product, driver);	
								product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+(i+1)+"]")).getText();
								String product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+(i+1)+"]")).getText();
								PLPtitle = product_BrandTitle +" "+ product_Title;
								Thread.sleep(1000);
								product.click();
								wait.until(ExpectedConditions.visibilityOf(PDPPage));
								if(HBCBasicfeature.isElementPresent(PDPPage))
								{
									Thread.sleep(1000);
									PDPtitle = PDPProdName.getText();
									if(PDPtitle.equalsIgnoreCase(PLPtitle))
									{
										if(HBCBasicfeature.isElementPresent(pdppageATB))
										{
											 pdploaded = true;
											 break;
										}
										else
										{
											 pdploaded = false;
											 continue;
										}
									}
									else
									{
										Fail("PDP and PLP title not matches",log);
									}	
								}
								else
								{
									Fail("PDP Page not displayed");
								}									
							}
							else
							{
								Fail("PLP products are not dispalyed");
							}
						}
						else
						{
							Fail("PLP page not dispalyed");
						}						
					}
					else
					{
						Fail("Search Box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}			
			}
			while( pdploaded == true);
		}		
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}	
		return pdploaded;
	}
	
	// Add to Bag
	public boolean clickaddtoBag(int prevVal) throws InterruptedException
	{
		boolean prdadded = false;
		try
		{
			HBCBasicfeature.jsclick(pdppageATB,driver);
			wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
			prdadded = false;
			Thread.sleep(1000);
			do
			{
				int currVal = Integer.parseInt(bagIconCount.getText());
				wait.until(ExpectedConditions.attributeContains(pdppageATBOverlay, "style", "block"));
				if(HBCBasicfeature.isElementPresent(pdppageATBOverlay))
				{
					if(currVal>prevVal)
					{
						prdadded = true;
						break;
					}
					else
					{
						HBCBasicfeature.jsclick(pdppageATB,driver);
						wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
						Thread.sleep(1000);
						continue;
					}
				}
				else
				{
					HBCBasicfeature.jsclick(pdppageATB,driver);
					wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
					Thread.sleep(1000);
					continue;
				}
			}while(prdadded != true);
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		return prdadded;
	}

	/*HUDSONBAY-982 Verify that on selecting the "Bag" icon from the header, shopping bag page should be shown*/
	public void cartBagIconClick() 
	{
		ChildCreation("HUDSONBAY-982 Verify that on selecting the 'Bag' icon from the header, shopping bag page should be shown");		
		try
		{
			homepge = signin();
			if(homepge == true)
			{
				Pass("Home page displayed");
				if(HBCBasicfeature.isElementPresent(bagIcon))
				{
					Pass("'Bag' icon displayed in the header");
					HBCBasicfeature.jsclick(bagIcon, driver);
					wait.until(ExpectedConditions.attributeContains(shoppingBagPageDom, "style", "visible"));
					if(HBCBasicfeature.isElementPresent(shoppingBagPageDom))
					{
						Pass("On selecting the 'Bag' icon from the header, shopping bag page shown");
						cartPage = true;
					}
					else
					{
						Fail("On selecting the 'Bag' icon from the header, shopping bag page not shown");
					}
				}
				else
				{
					Fail("'Bag' icon not displayed in the header");
				}
			}
			else
			{
				Skip("Home Page not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println("HBC-982" +e.getMessage());
			Exception("HBC-982 There is something wrong. Please Check." + e.getMessage());
		}	
	}
		
	/*HUDSONBAY-985 Verify that "Continue Shopping" or "Shop Now" button should be shown when the shopping bag page is empty*/
	public void cartEmptyShopNowButton() 
	{
		ChildCreation("HUDSONBAY-985 Verify that 'Continue Shopping' or 'Shop Now' button should be shown when the shopping bag page is empty");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagPageShopNow))
				{
					Pass(" 'Shop Now' button shown when the shopping bag page is empty");
				}
				else
				{
					Fail(" 'Shop Now' button not shown when the shopping bag page is empty");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-985" +e.getMessage());
				Exception("HBC-985 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
		
	/*HUDSONBAY-986 Verify that home page or respective page should be shown on tapping the "Continue Shopping" or "Shop Now" button*/
	public void cartEmptyShopNowButtonClick() 
	{
		ChildCreation("HUDSONBAY-986 Verify that home page or respective page should be shown on tapping the'Continue Shopping' or 'Shop Now' button");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagPageShopNow))
				{
					Pass(" 'Shop Now' button displayed");
					HBCBasicfeature.jsclick(shoppingBagPageShopNow, driver);
					wait.until(ExpectedConditions.visibilityOf(homepage));
					if(HBCBasicfeature.isElementPresent(homepage))
					{
						Pass("Home page or respective page shown on tapping the'Continue Shopping' or 'Shop Now' button");
						cartPage = false;
					}
					else
					{
						Fail("Home page or respective page not shown on tapping the'Continue Shopping' or 'Shop Now' button");
					}					
				}
				else
				{
					Fail(" 'Shop Now' button not shown when the shopping bag page is empty");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-986" +e.getMessage());
				Exception("HBC-986 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-996 Verify that user added products should be shown in the "Shopping Bag" page for registered user*/
	/*HUDSONBAY-983 Verify that total number of products that are added to the cart should be displayed in the Shopping bag page*/
	public void cartAddedProductsInBag() 
	{
		ChildCreation("HUDSONBAY-996 Verify that user added products should be shown in the 'Shopping Bag' page for registered user");
		boolean pdpPage = false, addBag = false, HBC983 = false;
		try
		{
			int Val = Integer.parseInt(bagIconCount.getText());
			pdpPage = pdpLoaded();
			if(pdpPage==true)
			{
				Pass("PDP page displayed for the product");
				addBag = clickaddtoBag(Val);
				if(addBag==true)
				{
					Pass("Product Added to shopping bag");		
					wait.until(ExpectedConditions.attributeContains(pdppageATBOverlay, "style", "block"));
					if(HBCBasicfeature.isElementPresent(PDPATBoverlayViewBag))
					{
						HBCBasicfeature.jsclick(PDPATBoverlayViewBag, driver);
						wait.until(ExpectedConditions.attributeContains(shoppingBagPageDom, "style", "visible"));
						if(HBCBasicfeature.isElementPresent(shoppingBagPageDom))
						{
							cartPage = true;
							Pass("Shopping bag page shown");
							if(HBCBasicfeature.isElementPresent(shoppingBagProd))
							{
								Pass("Product Displayed in Shopping Bag page");
								if(HBCBasicfeature.isElementPresent(shoppingBagPageProdTitle))
								{
									String actualTitle = shoppingBagPageProdTitle.getText();
									log.add("Displayed Prod Title in shopping bag is: "+actualTitle);
									log.add("Selected Product Title is: "+PDPtitle);
									if(PDPtitle.replaceAll("\\s+","").contains(actualTitle.replaceAll("\\s+",""))||PLPtitle.replaceAll("\\s+","").contains(actualTitle.replaceAll("\\s+","")))		
									{
										Pass("User added products shown in the 'Shopping Bag' page for registered user",log);
										HBC983 = true;
									}
									else
									{
										Fail("User added products not shown in the 'Shopping Bag' page for registered user",log);
									}
								}
								else
								{
									Fail("'Product Title' not displayed for the product in the 'Shopping Bag' page");
								}
							}
							else
							{
								Fail("Product not Displayed in Shopping Bag page");
							}								
						}
						else
						{
							Fail("Shopping bag page not shown");
						}
					}
					else
					{
						Fail("ATB success overlay 'Viewbag' button not displayed");
					}
				}
				else
				{
					Fail( "The User failed to add the product to bag.");
				}
			}
			else
			{
				Fail( "The User failed to add the product.");
			}		
		}
		catch (Exception e)
		{
			System.out.println("HBC-996" +e.getMessage());
			Exception("HBC-996 There is something wrong. Please Check." + e.getMessage());
		}	
		
		ChildCreation("HUDSONBAY-983 Verify that total number of products that are added to the cart should be displayed in the Shopping bag page");
		if(HBC983 == true)
		{
			Pass("Total number of products that are added to the cart displayed in the Shopping bag page");
		}
		else
		{
			Fail(" Products that are added to the cart not displayed in the Shopping bag page");
		}		
	}
		
	
	/*HUDSONBAY-981 Verify that "Shopping Bag" page should be shown as per the creative*/
	public void cartPageCreative() 
	{
		ChildCreation("HUDSONBAY-981 Verify that 'Shopping Bag' page should be shown as per the creative");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagPageTitle))
				{
					Pass("'SHOPPING BAG' title is displayed below header");
				}
				else
				{
					Fail("'SHOPPING BAG' title is not displayed below header");
				}				
				if(HBCBasicfeature.isElementPresent(bagIconCount))
				{
					int count = Integer.parseInt(bagIconCount.getText());
					if(count>0)
					{
						if(HBCBasicfeature.isElementPresent(shoppingCartCheckout))
						{
							Pass("Checkout button displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Checkout button not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingBagPaypal))
						{
							Pass("PayPal button displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("PayPal button not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingBagProd))
						{
							Pass("Added product displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Added product not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
						{
							Pass("Promo code field displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Promo code field not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApply))
						{
							Pass("Promo code Apply link displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Promo code Apply link not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingBagPostalCode))
						{
							Pass("Postal code field displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Postal code field not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingBagShippingMethodDropdown))
						{
							HBCBasicfeature.scrolldown(shoppingBagShippingMethodDropdown, driver);
							Pass("Shipping Method drop down displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Shipping Method drop down not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingBagShippingMethodApply))
						{
							Pass("Shipping Method drop down Apply link displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Shipping Method drop down Apply link not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingBagShippingAmountTaxSection))
						{
							HBCBasicfeature.scrolldown(shoppingBagShippingAmountTaxSection, driver);
							Pass("Estimated Shipping Amount and tax section displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Estimated Shipping Amount and tax section not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingBagTotalAmountSection))
						{
							HBCBasicfeature.scrolldown(shoppingBagTotalAmountSection, driver);
							Pass("Estimated Order Total section displayed in 'Shopping Bag' page ");
						}
						else
						{
							Fail("Estimated Order Total section not displayed in 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingCartCheckout2))
						{
							Pass("Checkout button displayed in Footer of 'Shopping Bag' page ");
						}
						else
						{
							Fail("Checkout button not displayed in footer of 'Shopping Bag' page");
						}
						if(HBCBasicfeature.isElementPresent(shoppingBagPaypal2))
						{
							Pass("PayPal button displayed in footer of 'Shopping Bag' page ");
						}
						else
						{
							Fail("PayPal button not displayed in footer of 'Shopping Bag' page");
						}
					}
					else
					{
						Skip("Product not displayed in ShoppingBag page");
					}
				}
				else
				{
					Fail("Bag prod count not displayed in ShoppingBag");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-981" +e.getMessage());
				Exception("HBC-981 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-984 Verify that "SHOPPING BAG" title should be shown in the header of the cart page*/
	public void cartShoppingBagtitle() throws java.lang.Exception 
	{
		ChildCreation("HUDSONBAY-984 Verify that 'SHOPPING BAG' title should be shown in the header of the cart page");
		if(cartPage == true)
		{
			String expected = HBCBasicfeature.getExcelVal("HBC984", sheet, 1);
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagPageTitle))
				{
					HBCBasicfeature.scrollup(shoppingBagPageTitle, driver);
					String actual = shoppingBagPageTitle.getText();
					log.add("Actual String displayed is: "+actual);
					log.add("Expected String displayed is: "+expected);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("'SHOPPING BAG' title is displayed below header",log);	
					}
					else
					{
						Fail("'SHOPPING BAG' title is displayed below header, but string not matches",log);
					}					 
				}
				else
				{
					Fail("'SHOPPING BAG' title is not displayed below header");
				}	
			}
			catch (Exception e)
			{
				System.out.println("HBC-984" +e.getMessage());
				Exception("HBC-984 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-989 Verify that header should be displayed in thetop side of the shopping bag page.*/
	public void cartPageHeader() 
	{
		ChildCreation("HUDSONBAY-989 Verify that header should be displayed in thetop side of the shopping bag page.");
		if(cartPage == true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));			
				if(HBCBasicfeature.isElementPresent(header))
				{
					if(HBCBasicfeature.isElementPresent(hamburger))
					{
						Pass("Hamburger menu displayed in the header");
					}
					else
					{
						Fail("Hamburger menu not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(logo))
					{
						Pass("Logo displayed in the header");
					}
					else
					{
						Fail("Logo not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(searchIcon))
					{
						Pass("Search Icon displayed in the header");
					}
					else
					{
						Fail("Search Icon not displayed in the header");
					}
					if(HBCBasicfeature.isElementPresent(bagIcon))
					{
						Pass("Shopping bag Icon displayed in the header");
					}
					else
					{
						Fail("Shopping bag Icon not displayed in the header");
					}
				}
				else
				{
					Fail("Header not displayed in the page");
				}					
			}
			catch (Exception e)
			{
				System.out.println("HBC-989" +e.getMessage());
				Exception("HBC-989 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
			
	/*HUDSONBAY-990 Verify that Footer should be displayed in the bottom side of the shopping bag page.*/
	public void cartPageFooter() 
	{
		ChildCreation("HUDSONBAY-990 Verify that Footer should be displayed in the bottom side of the shopping bag page.");
		if(cartPage == true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(footerContainer));
				if(HBCBasicfeature.isElementPresent(footerTop))
				{
					Pass("Footer top panel is displayed");	
					HBCBasicfeature.scrolldown(footerContainer, driver);
					wait.until(ExpectedConditions.visibilityOf(footerTopTitle));
					if(HBCBasicfeature.isElementPresent(footerTopTitle))
					{
						Pass("Footer top panel title is displayed");	
					}
					else
					{
						Fail("Footer top panel title is not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerSubTop));
					if(HBCBasicfeature.isElementPresent(footerSubTop))
					{
						Pass("Footer sub top panel is displayed");	
						wait.until(ExpectedConditions.visibilityOf(footerSubTopCall));
						if(HBCBasicfeature.isElementPresent(footerSubTopCall))
						{
							Pass("Footer call option is displayed");		
						}
						else
						{
							Fail("Footer Call option is not displayed");						
						}
						wait.until(ExpectedConditions.visibilityOf(footerSubTopEmail));
						if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
						{
							Pass("Footer Email option is displayed");	
						}
						else
						{
							Fail("Footer email option is not displayed");						
						}
					}
					else
					{
						Fail("Footer sub top panel is not displayed");						
					}
				}
				else
				{
					Fail("Footer top panel is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					Pass("Footer Menu List option is displayed");
				}
				else
				{
					Fail("Footer menu list is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerBottom));
				if(HBCBasicfeature.isElementPresent(footerBottom))
				{
					Pass("Footer bottom panel is displayed");
					wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
					if(HBCBasicfeature.isElementPresent(footerSocialIcon))
					{
						Pass("Footer social icon options are displayed");
					}
					else
					{
						Fail("Footer social icon options are not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{
						log.add("Footer displayed as per the creative and fit to the screen");
						Pass("Footer copyright section is displayed",log);	
					}
					else
					{
						log.add("Footer is not displayed as per the creative and fit to the screen");
						Fail("Footer copyright section is not displayed",log);	
					}
				}
				else
				{
					Fail("Footer bottom panel is not displayed");						
				}	
				HBCBasicfeature.scrollup(header, driver);
			}
			catch (Exception e)
			{
				System.out.println("HBC-990" +e.getMessage());
				Exception("HBC-990 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-987 Verify that "Product Image", "Product Title", "Price", "color", "size" , "In stock", "Qty" update, "Edit" button should be displayed for the product in the "Shopping Bag" page*/
	public void cartProductDetails() 
	{
		ChildCreation("HUDSONBAY-987 Verify that 'Product Image', 'Product Title', 'Price', 'color', 'size' , 'In stock', 'Qty' update, 'Edit' button should be displayed for the product in the 'Shopping Bag' page");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagProd))
				{
					Pass("Product Displayed in Shopping Bag page");
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdTitle))
					{
						log.add("Displayed Prod Title is: "+shoppingBagPageProdTitle.getText());
						Pass("'Product Title' displayed for the product in the 'Shopping Bag' page",log);
					}
					else
					{
						Fail("'Product Title' not displayed for the product in the 'Shopping Bag' page");
					}
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdImage))
					{
						int res = HBCBasicfeature.imageBroken(shoppingBagPageProdImage, log);
						if(res==200)
						{
							Pass("'Product Image' displayed for the product in the 'Shopping Bag' page",log);
						}
						else
						{
							Fail("'Product Image' displayed but image broken or not displayed",log);
						}
					}
					else
					{
						Fail("'Product Image' not displayed for the product in the 'Shopping Bag' page");
					}
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdColor))
					{
						log.add("Displayed Prod Color is: "+shoppingBagPageProdColor.getText());
						Pass("'Product color' displayed for the product in the 'Shopping Bag' page",log);
					}
					else
					{
						Fail("'Product color' not displayed for the product in the 'Shopping Bag' page");
					}
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdSize))
					{
						log.add("Displayed Prod size is: "+shoppingBagPageProdSize.getText());
						Pass("'Product size' displayed for the product in the 'Shopping Bag' page",log);
					}
					else
					{
						Fail("'Product size' not displayed for the product in the 'Shopping Bag' page");
					}
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRegularPrice)||HBCBasicfeature.isElementPresent(shoppingBagPageProdRegSalePrice))
					{
						Pass("'Product Price' displayed for the product in the 'Shopping Bag' page");
					}
					else
					{
						Fail("'Product Price' not displayed for the product in the 'Shopping Bag' page");
					}
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdAvail))
					{
						log.add("Displayed Prod Availability is: "+shoppingBagPageProdAvail.getText());
						Pass("'Product In stock' displayed for the product in the 'Shopping Bag' page",log);
					}
					else
					{
						Fail("'Product In stock' not displayed for the product in the 'Shopping Bag' page");
					}
					if(HBCBasicfeature.isElementPresent(prodQuantyInc))
					{
						Pass("Product Quantity Increment icon displayed for the product in the 'Shopping Bag' page");
					}
					else
					{
						Fail("Product Quantity Increment icon not displayed for the product in the 'Shopping Bag' page");
					}
					if(HBCBasicfeature.isElementPresent(prodQuanty))
					{
						String count = prodQuanty.getAttribute("value");
						int val = Integer.parseInt(count);
						log.add("Selected product quantity count is: "+val);
						Pass("Product Quantity displayed for the product in the 'Shopping Bag' page",log);
						if(val>1)
						{
							if(HBCBasicfeature.isElementPresent(prodQuantyDec))
							{
								Pass("Product Quantity Decrement icon displayed for the product in the 'Shopping Bag' page");
							}
							else
							{
								Fail("Product Quantity Decrement icon not displayed for the product in the 'Shopping Bag' page");
							}
						}
						else
						{
							Pass("Product Quantity Decrement icon not displayed when quantity is not more than 1");
						}
					}
					else
					{
						Fail("Product Quantity not displayed for the product in the 'Shopping Bag' page");
					}					
				}
				else
				{
					Fail("Product not Displayed in Shopping Bag page");
				}
								
			}
			catch (Exception e)
			{
				System.out.println("HBC-987" +e.getMessage());
				Exception("HBC-987 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-988 Verify that "X" icon should be shown for each product in the shopping bag page*/
	public void cartProductRemoveIcon() 
	{
		ChildCreation("HUDSONBAY-988 Verify that 'X' icon should be shown for each product in the shopping bag page");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagProd))
				{
					Pass("Product Displayed in Shopping Bag page");
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRemove))
					{
						Pass(" 'X' icon shown for each product in the shopping bag page");
					}
					else
					{
						Fail("'X' icon not shown for each product in the shopping bag page");
					}
				}
				else
				{
					Fail("Product not Displayed in Shopping Bag page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-988" +e.getMessage());
				Exception("HBC-988 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-994 Verify that selected quantity should be displayed in the "Shopping Bag" page*/
	public void cartProductQuantity() 
	{
		ChildCreation("HUDSONBAY-994 Verify that selected quantity should be displayed in the 'Shopping Bag' page");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(prodQuanty))
				{
					String count = prodQuanty.getAttribute("value");
					int val = Integer.parseInt(count);
					log.add("Selected product quantity count is: "+val);
					if(val==1)
					{
						Pass("Selected Product Quantity displayed for the product in the 'Shopping Bag' page",log);
					}
					else
					{
						Fail("Selected Product Quantity not displayed for the product in the 'Shopping Bag' page",log);
					}
				}
				else
				{
					Fail("Product Quantity not displayed for the product in the 'Shopping Bag' page");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-994" +e.getMessage());
				Exception("HBC-994 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-997 Verify that difference of the reg and sale price must be displayed*/
	/*HUDSONBAY-1002 Verify that Sale price, Actual Price should be shown by the font color mentioned in the creative*/
	public void cartProdPriceDifference() throws java.lang.Exception 
	{
		ChildCreation("HUDSONBAY-997 Verify that difference of the reg and sale price must be displayed");
		boolean regsale = false;
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRegularPrice)||HBCBasicfeature.isElementPresent(shoppingBagPageProdRegSalePrice))
				{
					Pass("'Product Price' displayed for the product in the 'Shopping Bag' page");
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRegularPrice))
					{
						Pass("Regular price alone displayed for the product");						
						String val = shoppingBagPageProdRegularPrice.getText();
						Pass("Displayed Regular price is: "+val);
					}
					else
					{
						if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRegSalePrice))
						{
							Pass("Regular and Sale price displayed for the product");
							regsale = true;
							if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRegSalePrice))
							{
								String val = shoppingBagPageProdRegSalePrice.getText();
								Pass("Displayed Regular price is: "+val);
							}
							else
							{
								Fail("Regular price not displayed for the product");
							}
							if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRegSaleSalePrice))
							{
								String val = shoppingBagPageProdRegSaleSalePrice.getText();
								Pass("Displayed Sale price is: "+val);
							}
							else
							{
								Fail("Sale price not displayed for the product");
							}							
						}
						else
						{
							Fail("Regular and Sale price not displayed for the product");
						}
					}
				}
				else
				{
					Fail("'Product Price' not displayed for the product in the 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-997" +e.getMessage());
				Exception("HBC-997 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-1002 Verify that Sale price, Actual Price should be shown by the font color mentioned in the creative");
			if(regsale==true)
			{
				String expectedRegColor = HBCBasicfeature.getExcelVal("HBC1002", sheet, 1);
				String expectedSaleColor = HBCBasicfeature.getExcelVal("HBC1002", sheet, 2);

				if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRegSalePrice))
				{
					String color = shoppingBagPageProdRegSalePrice.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(color);
					log.add("Actual color displayed is: "+actualColor);
					log.add("Expected color displayed is: "+expectedRegColor);
					if(actualColor.equalsIgnoreCase(expectedRegColor))
					{
						Pass("Regular Price displayed as per the creative",log);
					}
					else
					{
						Fail("Regular Price not displayed as per the creative",log);
					}
				}
				else
				{
					Fail("Regular price not displayed for the product");
				}
				if(HBCBasicfeature.isElementPresent(shoppingBagPageProdRegSaleSalePrice))
				{
					String color = shoppingBagPageProdRegSaleSalePrice.getCssValue("color");
					String actualColor = HBCBasicfeature.colorfinder(color);
					log.add("Actual color displayed is: "+actualColor);
					log.add("Expected color displayed is: "+expectedSaleColor);
					if(actualColor.equalsIgnoreCase(expectedSaleColor))
					{
						Pass("Sale Price displayed as per the creative",log);
					}
					else
					{
						Fail("Sale Price not displayed as per the creative",log);
					}
				}
				else
				{
					Fail("Sale price not displayed for the product");
				}					
			}
			else
			{
				Skip("Respective product has not Regular and sale price");
			}			
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1004 Verify that user must be able to increase or decrease the quantity by selecting the quantity from the quantity options*/
	public void cartProdIncrementDecrement() 
	{
		ChildCreation("HUDSONBAY-1004 Verify that user must be able to increase or decrease the quantity by selecting the quantity from the quantity options");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(prodQuanty))
				{
					int count = Integer.parseInt(prodQuanty.getAttribute("value"));
					log.add("Before Increment Product quantity count is: "+count);
					if(HBCBasicfeature.isElementPresent(prodQuantyInc))
					{
						Pass("Product Quantity Increment icon displayed");
						Thread.sleep(500);
						prodQuantyInc.click();
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						Thread.sleep(2000);
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "none"));						
						int aftrcount = Integer.parseInt(prodQuanty.getAttribute("value"));
						log.add("After Increment Product quantity count is: "+aftrcount);
						if(count!=aftrcount)
						{
							Pass("User able to increase the quantity by selecting the quantity Increment from the quantity options",log);
						}
						else
						{
							Fail("User not able to increase the quantity by selecting the quantity Increment from the quantity options",log);
						}
					}
					else
					{
						Fail("quantity Increment icon not displayed");
					}
					if(HBCBasicfeature.isElementPresent(prodQuantyDec))
					{
						Pass("Product Quantity Decrement icon displayed");
						Thread.sleep(500);
						prodQuantyDec.click();
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "none"));						
						int aftrcount = Integer.parseInt(prodQuanty.getAttribute("value"));
						log.add("After Decrement Product quantity count is: "+aftrcount);
						if(count==aftrcount)
						{
							Pass("User able to decrease the quantity by selecting the quantity Decrement from the quantity options",log);
						}
						else
						{
							Fail("User not able to decrease the quantity by selecting the quantity Decrement from the quantity options",log);
						}						
					}
					else
					{
						Fail("quantity Decrement icon not displayed");
					}						
				}
				else
				{
					Fail("Product Quantity not displayed for the product in the 'Shopping Bag' page");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1004" +e.getMessage());
				Exception("HBC-1004 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1003 Verify that user should be able to select the quantity in the "Quantity" options.*/
	public void cartQuanitySelection() 
	{
		ChildCreation("HUDSONBAY-1003 Verify that user should be able to select the quantity in the 'Quantity' options.");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(prodQuanty))
				{
					int count = Integer.parseInt(prodQuanty.getAttribute("value"));
					log.add("Before select the quantity, count is: "+count);
					if(HBCBasicfeature.isElementPresent(prodQuantityCont))
					{
						prodQuantityCont.click();
						prodQuantityCont.sendKeys(Keys.DELETE);
						prodQuantityCont.sendKeys("4");
						prodQuantityCont.sendKeys(Keys.ENTER);
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "none"));						
						Thread.sleep(1000);
						int aftrcount = Integer.parseInt(prodQuanty.getAttribute("value"));
						log.add("Updated Product quantity count is: "+aftrcount);
						if(count!=aftrcount)
						{
							Pass("User able to select the quantity in the 'Quantity' options",log);							
						}
						else
						{
							Fail("User not able to select the quantity in the 'Quantity' options",log);
						}						
					}
					else
					{
						Fail("Product qunatity count conatiner not displayed");
					}
				}
				else
				{
					Fail("Product qunatity count not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1003" +e.getMessage());
				Exception("HBC-1003 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1005 Verify that maximum of 5 should be updated in the "Quantity" field*/
	public void cartQuantityMaximum() 
	{
		ChildCreation("HUDSONBAY-1005 Verify that maximum of 5 should be updated in the 'Quantity' field");
		if(cartPage == true)
		{
			try
			{				
				int count = 0;
				if(HBCBasicfeature.isElementPresent(prodQuantyInc))
				{
					Pass("Product Quantity Increment icon displayed");
					boolean flag = false;
					do
					{
						try
						{
							prodQuantyInc.click();
							wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
							Thread.sleep(1000);
							wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "none"));						
						}
						catch(Exception e)
						{
							count = Integer.parseInt(prodQuanty.getAttribute("value"));								
							if(count==5)
							{
								flag = true;
								break;
							}
						}
						count = Integer.parseInt(prodQuanty.getAttribute("value"));		
					}
					while(count!=6);
					Thread.sleep(1000);
					log.add("Maximum count dispalyed is: "+count);	
					if(flag==true&&count==5)
					{
						Pass("Maximum of 5 updated in the 'Quantity' field",log);
					}
					else
					{
						Fail("More than 5 products updated in the 'Quantity' field",log);
					}
					if(count==5)
					{
						prodQuantityCont.clear();						
						wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
						Thread.sleep(1000);
						wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "none"));						
						count = Integer.parseInt(prodQuanty.getAttribute("value"));
						log.add("Updated Product quantity count is: "+count);
						if(count==1)
						{
							Pass("Quantity count changed to one",log);							
						}
						else
						{
							Fail("Quantity count not changed to one",log);
						}						
					}
					else
					{
						Fail("Count is greater than 5");
					}
				}
				else
				{
					count = Integer.parseInt(prodQuanty.getAttribute("value"));								
					if(count==5)
					{
						log.add("Since the quantity is 5, product increment icon is disabled");
						Pass("Maximum of quantity 5 updated in the 'Quantity' field",log);
					}
					else
					{
						Fail("Product Quantity Increment icon not displayed");
					}
				}						
			}
			catch (Exception e)
			{
				System.out.println("HBC-1005" +e.getMessage());
				Exception("HBC-1005 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-999 Verify that on tapping the "Product Image" or "Product Title" the respective PDP page or pop up should be shown*/
	public void cartProdImageTitleClick() 
	{
		ChildCreation("HUDSONBAY-999 Verify that on tapping the 'Product Image' or 'Product Title' the respective PDP page or pop up should be shown");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagProd))
				{
					Pass("Product Displayed in Shopping Bag page");
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdTitle))
					{
						Pass("'Product Title' displayed for the product in the 'Shopping Bag' page");
						shoppingBagPageProdTitle.click();
						wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@id='quick_view_iframe']")));
						wait.until(ExpectedConditions.attributeContains(shoppingBagPageProdQuickWindow, "style", "visible"));
						if(HBCBasicfeature.isElementPresent(shoppingBagPageProdQuickWindow))
						{
							Pass(" on tapping the 'Product Title' the respective pop up shown");							
						}
						else
						{
							Fail("on tapping the 'Product Title' the respective pop up not shown");
						}
						driver.switchTo().defaultContent();
						if(HBCBasicfeature.isElementPresent(shoppingBagPageProdQuickWindowClose))
						{
							HBCBasicfeature.jsclick(shoppingBagPageProdQuickWindowClose, driver);
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@skpagename='skhbcCartQuickInfo']")));
							Pass("Product Quick Window closed");
						}
						else
						{
							Fail("Product Quick Window close icon not displayed");
						}
					}
					else
					{
						Fail("'Product Title' not displayed for the product in the 'Shopping Bag' page");
					}
					if(HBCBasicfeature.isElementPresent(shoppingBagPageProdImage))
					{
						Pass("'Product Image' displayed for the product in the 'Shopping Bag' page");
						shoppingBagPageProdImage.click();
						wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@id='quick_view_iframe']")));
						wait.until(ExpectedConditions.attributeContains(shoppingBagPageProdQuickWindow, "style", "visible"));
						if(HBCBasicfeature.isElementPresent(shoppingBagPageProdQuickWindow))
						{
							Pass(" on tapping the 'Product Image' the respective pop up shown");							
						}
						else
						{
							Fail("on tapping the 'Product Image' the respective pop up not shown");
						}
						driver.switchTo().defaultContent();
						if(HBCBasicfeature.isElementPresent(shoppingBagPageProdQuickWindowClose))
						{
							HBCBasicfeature.jsclick(shoppingBagPageProdQuickWindowClose, driver);
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@skpagename='skhbcCartQuickInfo']")));
							Pass("Product Quick Window closed");
						}
						else
						{
							Fail("Product Quick Window close icon not displayed");
						}
					}
					else
					{
						Fail("'Product Image' not displayed for the product in the 'Shopping Bag' page");
					}
				}
				else
				{
					Fail("Product not displayed in the 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-999" +e.getMessage());
				Exception("HBC-999 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}

	/*HUDSONBAY-1001 Verify that "Promo Code" field should be shown with the "Apply" text link*/
	public void cartPromoCodeField() 
	{
		ChildCreation("HUDSONBAY-1001 Verify that 'Promo Code' field should be shown with the 'Apply' text link");
		if(cartPage == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC1001", sheet, 1);
				if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
				{
					Pass("'Promo Code' field displayed");
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApply))
					{
						String actual = paymentOptionsPromoCodeApply.getText();
						log.add("Actual text link is: "+actual);
						log.add("Expected text link is: "+expected);
						if(actual.equalsIgnoreCase(expected))
						{
							Pass(" 'Promo Code' field shown with the 'Apply' text link",log);
						}
						else
						{
							Fail("'Promo Code' field not shown with the 'Apply' text link",log);
						}
					}
				}
				else
				{
					Fail("'Promo Code' field not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1001" +e.getMessage());
				Exception("HBC-1001 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1009 Verify that on entering the invalid promo code, alert needs to be displayed*/
	public void cartInvalidPromoAlert() 
	{
		ChildCreation("HUDSONBAY-1009 Verify that on entering the invalid promo code, alert needs to be displayed");
		if(cartPage == true)
		{
			try
			{
				String promo = HBCBasicfeature.getExcelVal("HBC1009", sheet, 1);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1009", sheet, 2);
				if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCode))
				{
					Pass("'Promo Code' field displayed");
					paymentOptionsPromoCode.clear();
					paymentOptionsPromoCode.sendKeys(promo);
					paymentOptionsPromoCodeApply.click();
					wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
					wait.until(ExpectedConditions.attributeContains(paymentOptionsPromoCodeApplyInvalidAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(paymentOptionsPromoCodeApplyInvalidAlert))
					{
						String actualAlert = paymentOptionsPromoCodeApplyInvalidAlert.getText();
						log.add("Actual alert displayed is: "+actualAlert);
						log.add("Expected alert displayed is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("On entering the invalid promo code, respective alert displayed",log);
						}
						else
						{
							Fail("On entering the invalid promo code, respective alert not displayed",log);
						}
					}
					else
					{
						Fail("Alert not displayed");
					}
				}
				else
				{
					Fail("'Promo Code' field not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1009" +e.getMessage());
				Exception("HBC-1009 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1013 Verify that "Estimated Shipping and taxes" section should be shown below the promo codes*/
	public void cartEstimatedShippingandTaxes() 
	{
		ChildCreation("HUDSONBAY-1013 Verify that 'Estimated Shipping and taxes' section should be shown below the promo codes");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagShippingAmountTaxSection))
				{
					HBCBasicfeature.scrolldown(shoppingBagShippingAmountTaxSection, driver);
					Pass("Estimated Shipping Amount and tax section displayed in 'Shopping Bag' page ");
				}
				else
				{
					Fail("Estimated Shipping Amount and tax section not displayed in 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1013" +e.getMessage());
				Exception("HBC-1013 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1014 Verify that "Zip Code" field, "Shipping Method" field should be shown with the "Apply" text link in the "Estimated Shipping and taxes" section*/
	public void cartZipShippingMethodFields() 
	{
		ChildCreation("HUDSONBAY-1014 Verify that 'Zip Code' field, 'Shipping Method' field should be shown with the 'Apply' text link in the 'Estimated Shipping and taxes' section");
		if(cartPage == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC1001", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shoppingBagPostalCode))
				{
					Pass("Postal code field displayed in 'Shopping Bag' page ");
				}
				else
				{
					Fail("Postal code field not displayed in 'Shopping Bag' page");
				}
				if(HBCBasicfeature.isElementPresent(shoppingBagShippingMethodDropdown))
				{
					Pass("Shipping Method drop down displayed in 'Shopping Bag' page ");
				}
				else
				{
					Fail("Shipping Method drop down not displayed in 'Shopping Bag' page");
				}
				if(HBCBasicfeature.isElementPresent(shoppingBagShippingMethodApply))
				{
					String actual = shoppingBagShippingMethodApply.getText();
					log.add("Actual text link is: "+actual);
					log.add("Expected text link is: "+expected);
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("Shipping Method drop down Apply link displayed in 'Shopping Bag' page ",log);
					}
					else
					{
						Fail("Apply link displayed but text not matches",log);
					}
				}
				else
				{
					Fail("Shipping Method drop down Apply link not displayed in 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1014" +e.getMessage());
				Exception("HBC-1014 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1017 Verify that alert should be shown on tapping the apply button without entering the "Zip Code"*/
	public void cartEmptyZipcodeAlert() 
	{
		ChildCreation("HUDSONBAY-1017 Verify that alert should be shown on tapping the apply button without entering the 'Zip Code'");
		if(cartPage == true)
		{
			try
			{
				Actions act = new Actions(driver);
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1017", sheet, 2);
				driver.navigate().refresh();
				wait.until(ExpectedConditions.elementToBeClickable(shoppingBagPostalCode));
				if(HBCBasicfeature.isElementPresent(shoppingBagPostalCode))
				{
					HBCBasicfeature.scrolldown(shoppingBagPostalCode, driver);
					Pass("Postal code field displayed in 'Shopping Bag' page ");
					act.moveToElement(shoppingBagPostalCode).click().build().perform();
					Thread.sleep(1500);
					wait.until(ExpectedConditions.elementToBeClickable(shoppingBagShippingMethodDropdown));
					act.moveToElement(shoppingBagShippingMethodDropdown).click().build().perform();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(shoppingBagZipcodeEmptyAlert));
					wait.until(ExpectedConditions.attributeContains(shoppingBagZipcodeEmptyAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(shoppingBagZipcodeEmptyAlert))
					{
						Pass("Alert displayed");
						String actualAlert = shoppingBagZipcodeEmptyAlert.getText();
						log.add("Actual alert displayed is: "+actualAlert);
						log.add("Expected alert displayed is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("on tapping the apply button without entering the 'Zip Code' respective alert displayed",log);
						}
						else
						{
							Fail("respective alert not displayed",log);
						}
					}
					else
					{
						Fail("on tapping the apply button without entering the 'Zip Code' respective alert not displayed");
					}
				}
				else
				{
					Fail("Postal code field not displayed in 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1017" +e.getMessage());
				Exception("HBC-1017 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1018 Verify that alert should be shown on entering the invalid "Zip Code"*/
	public void cartInvalidZipcodeAlert() 
	{
		ChildCreation("HUDSONBAY-1018 Verify that alert should be shown on entering the invalid 'Zip Code'");
		if(cartPage == true)
		{
			try
			{
				String expectedAlert = HBCBasicfeature.getExcelVal("HBC1017", sheet, 3);
				String zip = HBCBasicfeature.getExcelVal("HBC1017", sheet, 1);
				if(HBCBasicfeature.isElementPresent(shoppingBagPostalCode))
				{
					Pass("Postal code field displayed in 'Shopping Bag' page ");
					shoppingBagPostalCode.clear();
					shoppingBagPostalCode.sendKeys(zip);
					

					shoppingBagShippingMethodApply.click();
					wait.until(ExpectedConditions.attributeContains(shoppingBagZipcodeInvalidAlert, "style", "block"));
					if(HBCBasicfeature.isElementPresent(shoppingBagZipcodeInvalidAlert))
					{
						Pass("Alert displayed");
						String actualAlert = shoppingBagZipcodeInvalidAlert.getText();
						log.add("Actual alert displayed is: "+actualAlert);
						log.add("Expected alert displayed is: "+expectedAlert);
						if(actualAlert.equalsIgnoreCase(expectedAlert))
						{
							Pass("on tapping the apply button on entering the invalid 'Zip Code' respective alert displayed",log);
						}
						else
						{
							Fail("respective alert not displayed",log);
						}
					}
					else
					{
						Fail("on tapping the apply button on entering the invalid 'Zip Code' respective alert not displayed");
					}
				}
				else
				{
					Fail("Postal code field not displayed in 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1018" +e.getMessage());
				Exception("HBC-1018 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1015 Verify that "Standard Delivery" option should be shown in default in the "Shipping Method" dropdown field*/
	public void cartDefaultDeliveryOption() throws java.lang.Exception 
	{
		ChildCreation("HUDSONBAY-1015 Verify that 'Standard Delivery' option should be shown in default in the 'Shipping Method' dropdown field");
		if(cartPage == true)
		{
			String expected  = HBCBasicfeature.getExcelVal("HBC1015", sheet, 1);
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagShippingMethodDropdown))
				{
					Pass("Shipping Method drop down displayed in 'Shopping Bag' page ");
					Select sel  = new Select(shoppingBagShippingMethodDropdown);
					String actual = sel.getFirstSelectedOption().getText();
					Thread.sleep(200);
					log.add("Actual option shown in default is: "+actual);
					log.add("Expected option shown in default is: "+expected);
					if(actual.contains(expected))
					{
						Pass("'Standard Delivery' option shown in default in the 'Shipping Method' dropdown field",log);
					}
					else
					{
						Fail("'Standard Delivery' option not shown in default in the 'Shipping Method' dropdown field",log);
					}					
				}
				else
				{
					Fail("Shipping Method drop down not displayed in 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1015" +e.getMessage());
				Exception("HBC-1015 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Page not displayed");
		}
	}
	
	/*HUDSONBAY-1016 Verify that price should be changed on changing the "Shipping Method" option*/
	public void cartShippingMethodChange() throws java.lang.Exception 
	{
		ChildCreation("HUDSONBAY-1016 Verify that price should be changed on changing the 'Shipping Method' option");
		if(cartPage == true)
		{
			String bfreVal = "";
			String zip = HBCBasicfeature.getExcelVal("HBC1016", sheet, 1);
			try
			{
				Random r = new Random();
				if(HBCBasicfeature.isElementPresent(shoppingBagTotalAmountSection))
				{
					HBCBasicfeature.scrolldown(shoppingBagTotalAmountSection, driver);
					Pass("Estimated Order Total section displayed in 'Shopping Bag' page ");
					bfreVal = shoppingBagTotalAmountSection.getText();
				}
				else
				{
					Fail("Estimated Order Total section not displayed in 'Shopping Bag' page");
				}
				if(HBCBasicfeature.isElementPresent(shoppingBagShippingMethodDropdown))
				{
					Pass("Shipping Method drop down displayed in 'Shopping Bag' page ");
					Select sel  = new Select(shoppingBagShippingMethodDropdown);
					int size = sel.getOptions().size();
					int  i  = r.nextInt(size);
					if(i==0)
					{
						i=i+1;
					}
					sel.getOptions().get(i).click();
					Thread.sleep(500);
					shoppingBagPostalCode.clear();
					shoppingBagPostalCode.sendKeys(zip);
					shoppingBagShippingMethodApply.click();
					Thread.sleep(1000);
					driver.navigate().refresh();
					if(HBCBasicfeature.isElementPresent(shoppingBagTotalAmountSection))
					{						
						String aftrVal = shoppingBagTotalAmountSection.getText();
						log.add("Before changing the 'Shipping Method' option the price is: "+bfreVal);
						log.add("After changing the 'Shipping Method' option the price is: "+aftrVal);
						if(bfreVal!=aftrVal)
						{
							Pass(" Price changed on changing the 'Shipping Method' option",log);
						}
						else
						{
							Fail("Price not changed on changing the 'Shipping Method' option",log);							
						}
					}
					else
					{
						Fail("Total amount section not displayed");
					}
				}
				else
				{
					Fail("Shipping Method drop down  not displayed in 'Shopping Bag' page ");
				}				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1016" +e.getMessage());
				Exception("HBC-1016 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1019 Verify that "Discounts", "Estimated Shipping Amount", "Estimated Tax" should be shown below the "Shipping Method" dropdown field*/
	public void cartProductEstimatedShippingandTaxes() 
	{
		ChildCreation("HUDSONBAY-1019 Verify that 'Discounts', 'Estimated Shipping Amount', 'Estimated Tax' should be shown below the 'Shipping Method' dropdown field");
		if(cartPage == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC1019", sheet, 1);
				String expectedd = HBCBasicfeature.getExcelVal("HBC1019", sheet, 2);
				if(HBCBasicfeature.isElementPresent(shoppingBagShippingAmountTaxSection))
				{
					HBCBasicfeature.scrolldown(shoppingBagShippingAmountTaxSection, driver);
					if(HBCBasicfeature.isElementPresent(shoppingBageEstimatedShipping)&&HBCBasicfeature.isElementPresent(shoppingBageEstimatedShippingPrice))
					{
						String actual = shoppingBageEstimatedShipping.getText();
						log.add("Actual string displayed is: "+actual);
						log.add("Expected string displayed is: "+expected);
						log.add("Estimated shipping amount displayed is: "+shoppingBageEstimatedShippingPrice.getText());
						if(actual.equalsIgnoreCase(expected)&&!shoppingBageEstimatedShippingPrice.getText().isEmpty())
						{
							Pass("'Estimated Shipping Amount' shown below the 'Shipping Method' dropdown field ",log);
						}
						else
						{
							Fail("'Estimated Shipping Amount' string not matches or amount not displayed",log);
						}
					}
					else
					{
						Fail("'Estimated Shipping Amount'section not displayed");
					}
					if(HBCBasicfeature.isElementPresent(shoppingBageEstimatedTax)&&HBCBasicfeature.isElementPresent(shoppingBageEstimatedTaxPrice))
					{
						String actual = shoppingBageEstimatedTax.getText();
						log.add("Actual string displayed is: "+actual);
						log.add("Expected string displayed is: "+expectedd);
						log.add("Estimated shipping Tax displayed is: "+shoppingBageEstimatedTaxPrice.getText());
						if(actual.equalsIgnoreCase(expectedd)&&!shoppingBageEstimatedTaxPrice.getText().isEmpty())
						{
							Pass("'Estimated Tax' shown below the 'Shipping Method' dropdown field ",log);
						}
						else
						{
							Fail("'Estimated Tax' string not matches or amount not displayed",log);
						}
					}
					else
					{
						Fail("'Estimated Tax' section not displayed");
					}					
				}
				else
				{
					Fail("Estimated Shipping Amount and tax section not displayed in 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1019" +e.getMessage());
				Exception("HBC-1019 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1065 Verify that product count in the the cart icon in the header is updating when update the quantity from shopping bag.*/
	/*HUDSONBAY-1735 Verify that Product price should be updated on changing the quantity in Cart Page*/

	public void cartQuantityPriceUpdates() throws InterruptedException 
	{
		ChildCreation("HUDSONBAY-1065 Verify that product count in the the cart icon in the header is updating when update the quantity from shopping bag.");
		if(cartPage == true)
		{
			boolean HBC1735 = false;
			int bfreCount = 0; String bfrePrice = "";
			try
			{
				if(HBCBasicfeature.isElementPresent(bagIconCount)&&HBCBasicfeature.isElementPresent(shoppingBagTotalAmountSection))
				{
					bfreCount = Integer.parseInt(bagIconCount.getText());
					bfrePrice = shoppingBagTotalAmountSection.getText();
				}				
				if(HBCBasicfeature.isElementPresent(prodQuantyInc))
				{
					Pass("Product Quantity Increment icon displayed");
					prodQuantyInc.click();
					wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
					Thread.sleep(2000);
					wait.until(ExpectedConditions.attributeContains(progressBarWidget, "style", "none"));						
					int aftrcount = Integer.parseInt(bagIconCount.getText());
					log.add("After Increment Product quantity count is: "+aftrcount);
					if(bfreCount!=aftrcount)
					{
						Pass("Product count in the the cart icon in the header is updating when update the quantity from shopping bag",log);
						HBC1735 = true;
					}
					else
					{
						Fail("Product count in the the cart icon in the header is not updating when update the quantity from shopping bag",log);
					}
				}
				else
				{
					Fail("quantity Increment icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1065" +e.getMessage());
				Exception("HBC-1065 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-1735 Verify that Product price should be updated on changing the quantity in Cart Page");
			if(HBC1735 == true)
			{
				if(HBCBasicfeature.isElementPresent(shoppingBagTotalAmountSection))
				{
					String aftrPrice = shoppingBagTotalAmountSection.getText();
					log.add("Before updating the quantity the count is: "+bfrePrice);
					log.add("After updating the quantity the count is: "+aftrPrice);
					Thread.sleep(200);
					if(bfrePrice!=aftrPrice)
					{
						Pass("Product price updated on changing the quantity in Cart Page",log);
					}
					else
					{
						Fail("Product price not updated on changing the quantity in Cart Page",log);
					}
				}
				else
				{
					Fail("Order total section not displayed");
				}
			}
			else
			{
				Fail("Product quantity not updated.pls chk HBC1065");
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1058 Verify that product count in the shopping bag & the cart icon in the header should match with each other..*/
	public void cartShoppingBagCount() 
	{
		ChildCreation("HUDSONBAY-1058 Verify that product count in the shopping bag & the cart icon in the header should match with each other");
		if(cartPage == true)
		{
			try
			{
				if(HBCBasicfeature.isElementPresent(bagIconCount))
				{
					int bagCount = Integer.parseInt(bagIconCount.getText());
					log.add("Product count in shoppingBag page: "+bagCount);
					if(HBCBasicfeature.isElementPresent(prodQuanty))
					{
						int count = Integer.parseInt(prodQuanty.getAttribute("value"));
						log.add("Product quantity count is: "+count);
						if(bagCount==count)
						{
							Pass("Product count in the shopping bag & the cart icon in the header match with each other",log);
						}
						else
						{
							Fail("Product count in the shopping bag & the cart icon in the header not match with each other",log);
						}
					}
					else
					{
						Fail("Product Quantity not displayed");
					}
				}
				else
				{
					Fail("Shopping Bag count not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1955" +e.getMessage());
				Exception("HBC-1955 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1021 Verify that "Proceed to Checkout", "Checkout with Paypal" button should be shown*/
	public void cartCheckoutPaypal() throws Exception 
	{
		ChildCreation("HUDSONBAY-1021 Verify that 'Proceed to Checkout', 'Checkout with Paypal' button should be shown");
		if(cartPage == true)
		{
			String chkout = HBCBasicfeature.getExcelVal("HBC1021", sheet, 1);
			try
			{
				if(HBCBasicfeature.isElementPresent(shoppingCartCheckout2))
				{
					String actual = shoppingCartCheckout2.getText();
					log.add("Actual checkout button text is: "+actual);
					log.add("Expected checkout button text is: "+chkout);
					Pass("Checkout button displayed in Footer of 'Shopping Bag' page ",log);
				}
				else
				{
					Fail("Checkout button not displayed in footer of 'Shopping Bag' page",log);
				}
				if(HBCBasicfeature.isElementPresent(shoppingBagPaypal2))
				{
					Pass("PayPal button displayed in footer of 'Shopping Bag' page ");
				}
				else
				{
					Fail("PayPal button not displayed in footer of 'Shopping Bag' page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1021" +e.getMessage());
				Exception("HBC-1021 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1022 Verify that on tapping "Proceed to Checkout", "Checkout with Paypal" button it should navigate to respective page*/
	public void cartPageNavigation() 
	{
		ChildCreation("HUDSONBAY-1022 Verify that on tapping 'Proceed to Checkout', 'Checkout with Paypal' button it should navigate to respective page");
		if(cartPage == true)
		{
			try
			{
				Thread.sleep(1000);
				if(HBCBasicfeature.isElementPresent(shoppingCartCheckout))
				{
					shoppingCartCheckout.click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(shippingPaymentPage, "style", "visible"));
					if(HBCBasicfeature.isElementPresent(shippingPaymentPage))
					{
						Pass("on tapping 'Proceed to Checkout' button it navigates to respective page");
						cartPage = false;
					}
					else
					{
						Fail("on tapping 'Proceed to Checkout' button not navigates to respective page");
					}
					wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
					HBCBasicfeature.jsclick(bagIcon, driver);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(shoppingBagPageDom, "style", "visible"));
					if(HBCBasicfeature.isElementPresent(shoppingBagPageDom))
					{
						Pass("On selecting the 'Bag' icon from the header, shopping bag page shown");
						cartPage = true;
					}
					else
					{
						Fail("On selecting the 'Bag' icon from the header, shopping bag page not shown");
					}
				}
				else
				{
					Fail("Checkout button not displayed in footer of 'Shopping Bag' page",log);
				}
				
				if(cartPage == true)
				{
					if(HBCBasicfeature.isElementPresent(shoppingBagPaypal))
					{
						Pass("PayPal button displayed in footer of 'Shopping Bag' page ");
						shoppingBagPaypal.click();
						wait.until(ExpectedConditions.attributeContains(payPalLoadingSpinner, "style", "none"));
						wait.until(ExpectedConditions.visibilityOf(payPalLogo));
						if(HBCBasicfeature.isElementPresent(payPalLogo))
						{
							Pass("on tapping 'PayPal' button it navigates to respective page");
							cartPage = false;
						}
						else
						{
							Fail("on tapping 'PayPal' button not navigates to respective page");
						}						
						if(HBCBasicfeature.isElementPresent(payPalPageCancel))
						{
							HBCBasicfeature.scrolldown(payPalPageCancel,driver);
							payPalPageCancel.click();
							wait.until(ExpectedConditions.attributeContains(shoppingBagPageDom, "style", "visible"));
							if(HBCBasicfeature.isElementPresent(shoppingBagPageDom))
							{
								Pass("On selecting the Paypal page cancel text link, shopping bag page shown");
								cartPage = true;
							}
							else
							{
								Fail("On selecting the Paypal page cancel text link, shopping bag page not shown");
							}
						}
						else
						{
							Fail("Paypal page cancel text link not displayed");
						}
					}
					else
					{
						Fail("PayPal button not displayed in footer of 'Shopping Bag' page");
					}
				}
				
			}
			catch (Exception e)
			{
				System.out.println("HBC-1022" +e.getMessage());
				Exception("HBC-1022 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-991 Verify that on tapping the "X" icon above each product, the respective product should be removed from the shopping bag page*/
	/*HUDSONBAY-1738 Verify that on removing the product from Bag then count should be updated.*/

	
	public void cartRemoveProduct() 
	{
		ChildCreation("HUDSONBAY-991 Verify that on tapping the X icon above each product, the respective product should be removed from the shopping bag page");
		if(cartPage == true)
		{
			boolean HBC1738 = false;
			int bagCount = 0;
			try
			{
				if(HBCBasicfeature.isElementPresent(bagIconCount))
				{
					bagCount = Integer.parseInt(bagIconCount.getText());
				}
				if(HBCBasicfeature.isElementPresent(shoppingBagProd))
				{
					Pass("Products displayed in the shopping bag page");
					if(HBCBasicfeature.isListElementPresent(shoppingBagPageProdRemoveList))
					{
						int size = shoppingBagPageProdRemoveList.size();
						int i = 1;
						do
						{
							try
							{
								WebElement remove = driver.findElement(By.xpath("(//*[@class='sk_prodDesCont']//*[@class='remove'])["+i+"]")); 
								if(HBCBasicfeature.isElementPresent(remove))
								{
									HBCBasicfeature.jsclick(remove, driver);
									wait.until(ExpectedConditions.attributeContains(loadingGaugeDom, "style", "none"));
									Thread.sleep(1000);
								}
								try
								{
									size = shoppingBagPageProdRemoveList.size();
								}
								catch(Exception e)
								{
									if(HBCBasicfeature.isElementPresent(shoppingBagPageShopNow))
									{							
										break;
									}
								}
							}
							catch(Exception e)
							{
								if(HBCBasicfeature.isElementPresent(shoppingBagPageShopNow))
								{							
									break;
								}
							}
						}
						while(size!=0);
						if(HBCBasicfeature.isElementPresent(shoppingBagPageShopNow))
						{
							HBC1738 = true;
							Pass("on tapping the 'X' icon above each product, the respective product removed from the shopping bag page");
						}
						else
						{
							HBC1738 = false;
							Fail("on tapping the 'X' icon above each product, the respective product not removed from the shopping bag page");
						}					
					}
					else
					{
						Fail("Product X icon not displayed");
					}
				}
				else
				{
					Skip("Products not displayed in the shopping bag");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-991" +e.getMessage());
				Exception("HBC-991 There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-1738 Verify that on removing the product from Bag then count should be updated.");
			if(HBC1738==true)
			{
				if(!HBCBasicfeature.isElementPresent(shoppingBagProd))
				{
					Pass("Product not displayed in shoppingBag page");
					if(HBCBasicfeature.isElementPresent(bagIconCount))
					{
						int aftrCount = Integer.parseInt(bagIconCount.getText());
						log.add("Before count is: "+bagCount);
						log.add("After count is: "+aftrCount);
						if(aftrCount!=bagCount)
						{
							Pass("On removing the product from Bag then count updated",log);
						}
						else
						{
							Fail("On removing the product from Bag then count not updated",log);
						}
					}
					else
					{
						Fail("Shoppingbag count not displayed");
					}
				}
				else
				{
					Fail("Product available in shopping bag");
				}
			}
			else
			{
				Skip("Products not removed from Shoppingbag page..pls check HBC-991 ");
			}
			
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
	
	/*HUDSONBAY-1006 Verify that if there is only one product in the bag and when that product is removed then the message, "Your shopping bag is empty" must be displayed with "Shop Now" or "Continue Shopping" button*/
	public void cartEmptyBagButton() 
	{
		ChildCreation("HUDSONBAY-1006 Verify that if there is only one product in the bag and when that product is removed then the message, 'Your shopping bag is empty' must be displayed with 'Shop Now' or 'Continue Shopping' button");
		if(cartPage == true)
		{
			try
			{
				String expected = HBCBasicfeature.getExcelVal("HBC1006", sheet, 1);
						
				if(!HBCBasicfeature.isElementPresent(shoppingBagProd))
				{
					if(HBCBasicfeature.isElementPresent(shoppingBagPageShopNow))
					{
						String actual = shoppingBagPageShopNow.getText();
						log.add("Actual button string is: "+actual);
						log.add("Expected button string is: "+expected);
						Pass(" 'Shop Now' button shown when the shopping bag page is empty",log);
					}
					else
					{
						Fail(" 'Shop Now' button not shown when the shopping bag page is empty",log);
					}
				}
				else
				{
					Fail("Product available in the shopping bag page");
				}
			}
			catch (Exception e)
			{
				System.out.println("HBC-1006" +e.getMessage());
				Exception("HBC-1006 There is something wrong. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip("Shopping Bag Page not displayed");
		}
	}
}


