package hbPageObjects;

import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbPages.Hudson_Bay_Footer;

public class Hudson_Bay_Footer_Obj extends Hudson_Bay_Footer implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_Footer_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+signupEmailss+"")
	WebElement signupEmails;
	
	@FindBy(xpath = ""+homepagee+"")
	WebElement homepage;
	
	@FindBy(xpath = ""+footerContainerr+"")
	WebElement footerContainer;	
	
	@FindBy(xpath = ""+footerTopp+"")
	WebElement footerTop;	
	
	@FindBy(xpath = ""+footerTopTitlee+"")
	WebElement footerTopTitle;
	
	@FindBy(xpath = ""+footerSubTopp+"")
	WebElement footerSubTop;
	
	@FindBy(xpath = ""+footerSubTopCalll+"")
	WebElement footerSubTopCall;
	
	@FindBy(xpath = ""+footerSubTopEmaill+"")
	WebElement footerSubTopEmail;
	
	@FindBy(xpath = ""+footerMenuListt+"")
	WebElement footerMenuList;
	
	@FindBy(xpath = "//*[@class='footer-menu-list'][1]")
	WebElement footerMenuShopHBC;	
			
	@FindBy(xpath = "(//*[@class='sk_footerAccordIcon'])[1]")
	WebElement footerAccordShopHBC;	
	
	@FindBy(xpath = "//*[@class='footer-menu-list'][2]")
	WebElement footerMenuStoreCorp;	
	
	@FindBy(xpath = "(//*[@class='sk_footerAccordIcon'])[2]")
	WebElement footerAccordStoreCorp;	
	
	@FindBy(xpath = "//*[@class='footer-menu-list'][3]")
	WebElement footerMenuCustomerPolicy;	
	
	@FindBy(xpath = "(//*[@class='sk_footerAccordIcon'])[3]")
	WebElement footerAccordCustomerPolicy;	
	
	@FindBy(xpath = "//*[@class='footer-menu-list'][4]")
	WebElement footerHBCRewards;	
	
	@FindBy(xpath = "(//*[@class='sk_footerAccordIcon'])[4]")
	WebElement footerAccordHBCRewards;
	
	@FindBy(xpath = ""+footerBottomm+"")
	WebElement footerBottom;
	
	@FindBy(xpath = ""+footerSocialIconn+"")
	WebElement footerSocialIcon;
	
	@FindBy(xpath = ""+footerBottomCopyrightt+"")
	WebElement footerBottomCopyright;
	
	@FindBy(xpath = ""+footerMenuListActivee+"")
	WebElement footerMenuListActive;
	
	@FindBy(xpath = ""+footerMenuWrapperr+"")
	WebElement footerMenuWrapper;	
	
	@FindBy(xpath = ""+footerMenuWrapperDomm+"")
	WebElement footerMenuWrapperDom;		
	
	@FindBy(xpath = ""+footerSocialFBb+"")
	WebElement footerSocialFB;
	
	@FindBy(xpath = ""+footerSocialPinn+"")
	WebElement footerSocialPin;
	
	@FindBy(xpath = ""+footerSocialInstaa+"")
	WebElement footerSocialInsta;
	
	@FindBy(xpath = ""+footerSocialTwitterr+"")
	WebElement footerSocialTwitter;
	
	@FindBy(xpath = ""+footerSocialYouu+"")
	WebElement footerSocialYou;
		
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+footerSectionsDomm+"")
	WebElement footerSectionsDom;
	
	@FindBy(xpath = ""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+homepageDomm+"")
	WebElement homepageDom;
	
	@FindBy(xpath = ""+otherlanguagee+"")
	WebElement otherlanguage;
	
	
	//Lists:
		
	@FindBy(how = How.XPATH,using = ""+MenuListss+"")
	List<WebElement> MenuLists;
	
	@FindBy(how = How.XPATH,using = ""+socialIconsListss+"")
	List<WebElement> socialIconsLists;	
	
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	boolean creative = false, labie = false;
	
	
	public void creative()
	{
		try
		{
			HBCBasicfeature.scrolldown(footerContainer, driver);
			/*wait.until(ExpectedConditions.visibilityOf(signupEmails));
			if(HBCBasicfeature.isElementPresent(signupEmails))
			{
				Pass("Signup for daily Emails option is displayed");	
			}
			else
			{
				Fail("Signup for daily Emails option is not displayed");						
			}			*/
			wait.until(ExpectedConditions.visibilityOf(footerTop));
			if(HBCBasicfeature.isElementPresent(footerTop))
			{
				Pass("Footer top panel is displayed");	
				wait.until(ExpectedConditions.visibilityOf(footerTopTitle));
				if(HBCBasicfeature.isElementPresent(footerTopTitle))
				{
					Pass("Footer top panel title is displayed");	
				}
				else
				{
					Fail("Footer top panel title is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerSubTop));
				if(HBCBasicfeature.isElementPresent(footerSubTop))
				{
					Pass("Footer sub top panel is displayed");	
					wait.until(ExpectedConditions.visibilityOf(footerSubTopCall));
					if(HBCBasicfeature.isElementPresent(footerSubTopCall))
					{
						Pass("Footer call option is displayed");		
					}
					else
					{
						Fail("Footer Call option is not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerSubTopEmail));
					if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
					{
						Pass("Footer Email option is displayed");	
					}
					else
					{
						Fail("Footer email option is not displayed");						
					}
				}
				else
				{
					Fail("Footer sub top panel is not displayed");						
				}
				creative = true;
			}
			else
			{
				Fail("Footer top panel is not displayed");						
			}
			wait.until(ExpectedConditions.visibilityOf(footerMenuList));
			if(HBCBasicfeature.isElementPresent(footerMenuList))
			{
				Pass("Footer Menu List option is displayed");
			}
			else
			{
				Fail("Footer menu list is not displayed");						
			}
			wait.until(ExpectedConditions.visibilityOf(footerBottom));
			if(HBCBasicfeature.isElementPresent(footerBottom))
			{
				Pass("Footer bottom panel is displayed");
				wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
				if(HBCBasicfeature.isElementPresent(footerSocialIcon))
				{
					Pass("Footer social icon options are displayed");
				}
				else
				{
					Fail("Footer social icon options are not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
				if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
				{
					log.add("Footer displayed as per the creative and fit to the screen");
					Pass("Footer copyright section is displayed",log);	
					creative = true;
				}
				else
				{
					log.add("Footer is not displayed as per the creative and fit to the screen");
					Fail("Footer copyright section is not displayed",log);	
				}
			}
			else
			{
				Fail("Footer bottom panel is not displayed");						
			}							
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}	
	}
	
	/*HUDSONBAY-94 Verify that Footer Should be displayed as per the creative and fit to the screen*/
	public void footerCreative()
		{
			ChildCreation("HUDSONBAY-94 Verify that Footer Should be displayed as per the creative and fit to the screen");
			try
			{
				creative();
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}	
	
	/*HUDSONBAY-97 Verify that sign up for emails button should be shown as per the creative in all the pages of footer.*/
	public void signupEmails() throws java.lang.Exception
		{
			String expected = HBCBasicfeature.getExcelVal("HB97", sheet, 1);	
			ChildCreation("HUDSONBAY-97 Verify that sign up for emails button should be shown as per the creative in all the pages of footer.");
			boolean childFlag = false;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(signupEmails));
				if(HBCBasicfeature.isElementPresent(signupEmails))
				{
					String actual = signupEmails.getText();		
					if(actual.equalsIgnoreCase(expected))
					{
						childFlag = true;
						Pass("Signup for daily Emails button displayed as per the creative");	
					}					
					else
					{
						Fail("Signup for daily Emails button not displayed as per the creative");						
					}
				}
				else
				{
					Fail("Signup for daily Emails button not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-237 Verify that sign up for daily mails section should fit to the screen as per the creative");
			if(childFlag)
			{
				Pass("Signup for daily Emails section displayed as per the creative");
			}
			else
			{
				Fail("Signup for daily Emails section not displayed as per the creative");
			}			
		}
	
	/*HUDSONBAY-99 Verify that "We are here to help section" should be shown as per the creative in all the pages of footer.*/
	public void hereToHelp() throws Exception
		{
			String expected = HBCBasicfeature.getExcelVal("HB99", sheet, 1);				
			ChildCreation("HUDSONBAY-99 Verify that 'We are here to help section' should be shown as per the creative in all the pages of footer.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(footerTopTitle));
				if(HBCBasicfeature.isElementPresent(footerTopTitle))
				{
					String actual = footerTopTitle.getText();
					if(actual.equalsIgnoreCase(expected))
					{
						Pass("'We are here to help'section shown as per the creative");
					}
					else
					{
						Fail("'We are here to help'section not shown as per the creative");
					}
				}
				else
				{
					Fail("'We are here to help' button is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-102 Verify that Shop HBC should be shown as per the creative in all the pages of footer.*/
	public void menuShopHBC() throws Exception
		{		
			String expected = HBCBasicfeature.getExcelVal("HB102", sheet, 1);				
			ChildCreation("HUDSONBAY-102 Verify that Shop HBC should be shown as per the creative in all the pages of footer.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					wait.until(ExpectedConditions.visibilityOf(footerMenuShopHBC));
					if(HBCBasicfeature.isElementPresent(footerMenuShopHBC))
					{
						String actual = footerMenuShopHBC.getText();
						/*System.out.println(actual);
						System.out.println(expected);*/
						Thread.sleep(500);
						if(actual.equalsIgnoreCase(expected))
						{
							log.add("ShopHBC menu title is "+actual+" displayed");
							if(HBCBasicfeature.isElementPresent(footerAccordShopHBC))
							{
								log.add("ShopHBC accordian is displayed");
								Pass("Shop HBC shown as per the creative",log);
							}
							else
							{
								Fail("Shop HBC not displayed as per the creative");
							}
						}
						else
						{
							Fail("Shop HBC menu title is not displayed");
						}
					}
					else
					{
						Fail("ShopHBC menu is not displayed");
					}					
				}
				else
				{
					Fail("Footer Menu list is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-103 Verify that Shop HBC section should be expanded on selecting it & collapse on closing it from footer.*/
	public void shopHBCClick()
		{
			ChildCreation("HUDSONBAY-103 Verify that Shop HBC section should be expanded on selecting it & collapse on closing it from footer.");
			try
			{
				Actions action = new Actions(driver);	
				wait.until(ExpectedConditions.visibilityOf(footerMenuShopHBC));
				if(HBCBasicfeature.isElementPresent(footerMenuShopHBC))
				{
					action.moveToElement(footerMenuShopHBC).click().build().perform();
					wait.until(ExpectedConditions.attributeContains(footerMenuWrapper, "style", "block"));
					wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
					{
						log.add("on selecting ShopHBC then the section is expanded");
						action.moveToElement(footerAccordShopHBC).click().build().perform();
						wait.until(ExpectedConditions.attributeContains(footerMenuWrapper, "style", "none"));
						if(!HBCBasicfeature.isElementPresent(footerMenuListActive))
						{
							log.add("on selecting ShopHBC then the section is collapsed");
							Pass("ShopHBC section expanded on selecting it & collapse on closing it from footer",log);
						}
						else
						{
							Fail("ShopHBC section not expanded on selecting it & not collapse on closing it from footer");
						}
					}
					else
					{
						Fail("ShopHBC section not expanded");
					}
				}
				else
				{
					Fail("Footer menu ShopHBC is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
		
	/*HUDSONBAY-104 Verify that Shop HBC section should consists of these links as per the requirement*/
	public void shopHBCSection() throws Exception
		{		
			String[] linkOptions = HBCBasicfeature.getExcelVal("HB104", sheet, 1).split("\n");
			ChildCreation("HUDSONBAY-104 Verify that Shop HBC section should consists of these links as per the requirement");
			try
			{
				Actions action = new Actions(driver);					
				wait.until(ExpectedConditions.visibilityOf(footerMenuShopHBC));
				if(HBCBasicfeature.isElementPresent(footerMenuShopHBC))
				{
					action.moveToElement(footerMenuShopHBC).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
					{
						boolean optionfound = true;
						int size = MenuLists.size();
						for(int i=0;i<size;i++)
						{
							String link_text = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]")).getText();
							for(int j=0;j<linkOptions.length;j++)
							{
								if(link_text.equalsIgnoreCase(linkOptions[j]))
								{
									log.add("ShopHBC contains the link :"+link_text);
									optionfound = true;
									break;
								}
								else
								{
									optionfound = false;
									continue;
								}		
							}
						}
						if(optionfound == true)
						{
							Pass("ShopHBC section consists of these links as per the requirement",log);
							action.moveToElement(footerAccordShopHBC).click().build().perform();
						}
						else
						{
							Fail("ShopHBC section not consists of these links as per the requirement",log);
							action.moveToElement(footerAccordShopHBC).click().build().perform();
						}
					}
					else
					{
						Fail("ShopHBC section not expanded");
					}
				}
				else
				{
					Fail("ShopHBC section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-105 Verify that on selecting the links from Speciality Shops should takes to its corresponding pages as expected.*/
	public void shopHBCSectionLinkSelection() throws Exception
		{		
			String[] linkOptions = HBCBasicfeature.getExcelVal("HB104", sheet, 1).split("\n");
			ChildCreation("HUDSONBAY-105 Verify that on selecting the links from Speciality Shops should takes to its corresponding pages as expected.");
			try
			{
				Actions action = new Actions(driver);					
				wait.until(ExpectedConditions.visibilityOf(footerMenuShopHBC));
				if(HBCBasicfeature.isElementPresent(footerMenuShopHBC))
				{
					Pass("Speciality Shops Section displayed");
					action.moveToElement(footerAccordShopHBC).click().build().perform();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(footerMenuWrapper, "style", "block"));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapperDom))
					{
						int size = MenuLists.size();
						for(int i=0;i<size;i++)
						{
							boolean flag = false;
							if(HBCBasicfeature.isElementPresent(footerMenuListActive))
							{
								Pass("Speciality Shops Section in opened state");
							}
							else
							{
								action.moveToElement(footerAccordShopHBC).click().build().perform();
								Thread.sleep(500);
								wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
								Pass("Speciality Shops Section in opened state");
							}
							WebElement link = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]"));
							String link_text = link.getText();
							Thread.sleep(500);
							for(int j=0;j<linkOptions.length;j++)
							{
								if(link_text.equalsIgnoreCase(linkOptions[j]))
								{
									Pass("Speciality Shops section contains the link :"+link_text);
									wait.until(ExpectedConditions.visibilityOf(link));
									HBCBasicfeature.scrolldown(link, driver);
									Thread.sleep(1000);
									action.moveToElement(link).click().build().perform();
									wait.until(ExpectedConditions.visibilityOf(plpPage));
									if(HBCBasicfeature.isElementPresent(plpPage))
									{
										Pass("The expected "+link_text+"  plp page shown on selecting the "+link_text+" link");
										Thread.sleep(1000);										
										do
										{		
											logo.click();
											wait.until(ExpectedConditions.visibilityOf(homepage));
											if(HBCBasicfeature.isElementPresent(homepage))
											{
												flag = true;
											}
											else
											{
												continue;
											}											
										}
										while(flag!=true);	
										break;
									}								
									else
									{
										Fail("The expected plp page not shown on selecting the "+link_text+" link");
										Thread.sleep(500);
										logo.click();
										wait.until(ExpectedConditions.visibilityOf(homepage));
									}								
								}
								else
								{
									continue;
								}		
							}
						}
					/*	WebElement activeAccordClose = driver.findElement(By.xpath("//*[@class='footer-menu-list active']//*[@class='sk_footerAccordIcon']"));
						action.moveToElement(activeAccordClose).click().build().perform();			*/
					}
					else
					{
						Fail("Speciality Shops section not expanded");
					}
				}
				else
				{
					Fail("Speciality Shops section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HUDSONBAY-105"+e.getMessage());
				Exception("HUDSONBAY-105 There is something wrong. Please Check." + e.getMessage());
			}				
		}
		
	/*HUDSONBAY-106 Verify that Store & corporate section should be shown as per the creative in all the pages of footer.*/
	public void menuStoreCorp() throws Exception
		{		
			String expected = HBCBasicfeature.getExcelVal("HB106", sheet, 1);				
			ChildCreation("HUDSONBAY-106 Verify that Store & corporate section should be shown as per the creative in all the pages of footer.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					wait.until(ExpectedConditions.visibilityOf(footerMenuStoreCorp));
					if(HBCBasicfeature.isElementPresent(footerMenuStoreCorp))
					{
						String actual = footerMenuStoreCorp.getText();
						Thread.sleep(1000);
						if(actual.equalsIgnoreCase(expected))
						{
							log.add("Store & corporate menu title " +actual+ " is displayed ");
							if(HBCBasicfeature.isElementPresent(footerAccordShopHBC))
							{
								log.add("Store & corporate section accordian is displayed");
								Pass("Store & corporate section shown as per the creative",log);
							}
							else
							{
								Fail("Store & corporate section not displayed as per the creative");
							}
						}
						else
						{
							Fail("Store & corporate section title is not displayed");
						}
					}
					else
					{
						Fail("Store & corporate section is not displayed");
					}					
				}
				else
				{
					Fail("Footer Menu list is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-107 Verify that Store & corporate section should be expanded on selecting it & collapse on closing it from footer..*/
	public void storeCorplick()
		{
			ChildCreation("HUDSONBAY-107 Verify that Store & corporate section should be expanded on selecting it & collapse on closing it from footer.");
			try
			{
				Actions action = new Actions(driver);	
				wait.until(ExpectedConditions.visibilityOf(footerMenuStoreCorp));
				if(HBCBasicfeature.isElementPresent(footerMenuStoreCorp))
				{
					action.moveToElement(footerMenuStoreCorp).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
					{
						log.add("on selecting Store & corporate then the section is expanded");
						Thread.sleep(1000);
						action.moveToElement(footerAccordStoreCorp).click().build().perform();
						Thread.sleep(1000);
						if(!HBCBasicfeature.isElementPresent(footerMenuListActive))
						{
							log.add("on selecting Store & corporate then the section is collapsed");
							Pass("Store & corporate section expanded on selecting it & collapse on closing it from footer",log);
						}
						else
						{
							Fail("Store & corporate section not expanded on selecting it & not collapse on closing it from footer");
						}
					}
					else
					{
						Fail("Store & corporate section not expanded");
					}
				}
				else
				{
					Fail("Footer menu Store & corporate is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-108 Verify that Store & corporate section should consists of these links as per the requirement*/
	public void storeCorpSection() throws Exception
		{		
			String links = HBCBasicfeature.getExcelVal("HBC108", sheet, 1);	
			String[] linkOptions = links.split("\n");
			ChildCreation("HUDSONBAY-108 Verify that Store & corporate section should consists of these links as per the requirement");
			try
			{
				Actions action = new Actions(driver);					
				wait.until(ExpectedConditions.visibilityOf(footerMenuStoreCorp));
				if(HBCBasicfeature.isElementPresent(footerMenuStoreCorp))
				{
					action.moveToElement(footerMenuStoreCorp).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
					{
						boolean optionfound = true;
						int size = MenuLists.size();
						for(int i=0;i<size;i++)
						{
							String link_text = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]")).getText();
							for(int j=0;j<linkOptions.length;j++)
							{
								if(link_text.equalsIgnoreCase(linkOptions[j]))
								{
									log.add("Store & Corporate section contains the link :"+link_text);
									optionfound = true;
									break;
								}
								else
								{
									optionfound = false;
									continue;
								}		
							}
						}
						if(optionfound == true)
						{
							Pass("Store & Corporate section consists of these links as per the requirement",log);
							action.moveToElement(footerAccordStoreCorp).click().build().perform();
						}
						else
						{
							Fail("Store & Corporate section section not consists of these links as per the requirement",log);
							action.moveToElement(footerAccordStoreCorp).click().build().perform();
						}
					}
					else
					{
						Fail("Store & Corporate section not expanded");
					}
				}
				else
				{
					Fail("Store & Corporate section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-109 Verify that on selecting the links from Store & corporate section should takes to its corresponding pages as expected.*/
	public void storeCorpSectionLinksSelection() throws Exception
		{		
			String[] storeCorp = HBCBasicfeature.getExcelVal("HBC108", sheet, 2).split("\n");
			String[] linkOptions = HBCBasicfeature.getExcelVal("HBC108", sheet, 1).split("\n");
			ChildCreation("HUDSONBAY-109 Verify that on selecting the links from Store & corporate section should takes to its corresponding pages as expected.");
			try
			{
				Actions action = new Actions(driver);					
				wait.until(ExpectedConditions.visibilityOf(footerMenuStoreCorp));
				if(HBCBasicfeature.isElementPresent(footerMenuStoreCorp))
				{
					Pass("Store & corporate Section displayed");
					action.moveToElement(footerAccordStoreCorp).click().build().perform();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(footerMenuWrapperDom, "style", "block"));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapperDom))
					{
						int size = MenuLists.size();
						for(int i=0;i<size;i++)
						{
							boolean flag = false;
							Thread.sleep(500);
							if(HBCBasicfeature.isElementPresent(footerMenuListActive))
							{
								Pass("Store & corporate Section in opened state");
							}
							else
							{
								action.moveToElement(footerAccordStoreCorp).click().build().perform();
								wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
								Pass("Store & corporate Section in opened state");
							}
							WebElement link = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]"));
							HBCBasicfeature.scrolldown(link, driver);
							String link_text = link.getText();
							Thread.sleep(500);
							for(int j=0;j<linkOptions.length;j++)
							{
								if(link_text.equalsIgnoreCase(linkOptions[j]))
								{
									Pass("Store & Corporate section contains the link :"+link_text);
									try
									{
										WebElement Outlink = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]//a[@target='_blank']"));
										if(HBCBasicfeature.isElementPresent(Outlink))
										{
											String handle = driver.getWindowHandle();
											HBCBasicfeature.scrolldown(Outlink, driver);
											Outlink.click();
											Thread.sleep(2000);
											boolean linkfound = false;
											for(String newWin: driver.getWindowHandles())
											{
												if(!newWin.equals(handle))
												{
													driver.switchTo().window(newWin);
													Thread.sleep(1000);
													String url = driver.getCurrentUrl();
													Thread.sleep(1000);
													if(url.contains(storeCorp[j]))
													{
														Pass("The expected "+link_text+"  page shown on selecting the "+link_text+" link and the URL is " + driver.getCurrentUrl());
														Pass("The URL Contains the Searched Keyword.");
														linkfound = true;
														break;
													}
													else
													{
														linkfound = false;
														continue;
													}							
												}									
											 }								
											if(linkfound == true)
											{
												driver.close();								
												driver.switchTo().window(handle);
												Pass("The "+link_text+" link was clicked and the tab is closed successfully.");
												Thread.sleep(1000);
												break;
											}
											else
											{
												Fail("The expected page shown on selecting the "+link_text+" link and the URL is  " + driver.getCurrentUrl());
												Fail("The URL not Contains the Searched Keyword.");
												driver.close();
												driver.switchTo().window(handle);
												Thread.sleep(1000);
											}								
										}
										action.moveToElement(footerAccordStoreCorp).click().build().perform();																		
									}
									catch(Exception e)
									{
										Thread.sleep(1000);
										HBCBasicfeature.scrolldown(link, driver);
										action.moveToElement(link).click().build().perform();
										wait.until(ExpectedConditions.attributeContains(footerSectionsDom, "style", "visible"));
										Thread.sleep(1000);
										String url = driver.getCurrentUrl();
										if(url.contains(storeCorp[j]))
										{
											Pass("The expected "+link_text+"  page shown on selecting the "+link_text+" link and the URL is " + driver.getCurrentUrl());
											Pass("The URL Contains the Searched Keyword.");
											do
											{		
												logo.click();
												wait.until(ExpectedConditions.visibilityOf(homepage));
												if(HBCBasicfeature.isElementPresent(homepage))
												{
													flag = true;
												}
												else
												{
													continue;
												}											
											}
											while(flag!=true);	
											break;											
										}
										else
										{
											Fail("The expected page not shown on selecting the "+link_text+" link and the URL is  " + driver.getCurrentUrl());
											Fail("The URL not Contains the Searched Keyword.");
											logo.click();		
											wait.until(ExpectedConditions.visibilityOf(homepage));
										}								
									}
								}
								else
								{
									continue;
								}		
							}
						}
						driver.navigate().refresh();
					}
					else
					{
						Fail("Store & Corporate section not expanded");
					}
				}
				else
				{
					Fail("Store & Corporate section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HUDSONBAY-109" +e.getMessage());
				Exception("HUDSONBAY-109 There is something wrong. Please Check." + e.getMessage());
			}				
		}
		
	/*HUDSONBAY-111 Verify that customer care & policy section should be shown as per the creative in all the pages of footer.*/
	public void menuCustomerPolicy() throws Exception
		{		
			String expected = HBCBasicfeature.getExcelVal("HB111", sheet, 1);				
			ChildCreation("HUDSONBAY-111 Verify that customer care & policy section should be shown as per the creative in all the pages of footer.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					wait.until(ExpectedConditions.visibilityOf(footerMenuCustomerPolicy));
					if(HBCBasicfeature.isElementPresent(footerMenuCustomerPolicy))
					{
						String actual = footerMenuCustomerPolicy.getText();
						Thread.sleep(1000);
						if(actual.equalsIgnoreCase(expected))
						{
							log.add("Customer care & Policy menu title" +actual+ " is displayed ");
							if(HBCBasicfeature.isElementPresent(footerAccordCustomerPolicy))
							{
								log.add("Customer care & Policy section accordian is displayed");
								Pass("Customer care & Policy section shown as per the creative",log);
							}
							else
							{
								Fail("Customer care & Policy section not displayed as per the creative");
							}
						}
						else
						{
							Fail("Customer care & Policy section title is not displayed");
						}
					}
					else
					{
						Fail("Customer care & Policy section is not displayed");
					}					
				}
				else
				{
					Fail("Footer Menu list is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-112 Verify that customer care & policy section should be expanded on selecting it & collapse on closing it from footer.*/
	public void CustomerPolicylick()
		{
			ChildCreation("HUDSONBAY-112 Verify that customer care & policy section should be expanded on selecting it & collapse on closing it from footer.");
			try
			{
				Actions action = new Actions(driver);	
				wait.until(ExpectedConditions.visibilityOf(footerMenuCustomerPolicy));
				if(HBCBasicfeature.isElementPresent(footerMenuCustomerPolicy))
				{
					action.moveToElement(footerMenuCustomerPolicy).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
					{
						log.add("on selecting customer care & policy then the section is expanded");
						Thread.sleep(1000);
						action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
						Thread.sleep(1000);
						if(!HBCBasicfeature.isElementPresent(footerMenuListActive))
						{
							log.add("on selecting customer care & policy then the section is collapsed");
							Pass("Customer care & policy section expanded on selecting it & collapse on closing it from footer",log);
						}
						else
						{
							Fail("Customer care & policy section not expanded on selecting it & not collapse on closing it from footer");
						}
					}
					else
					{
						Fail("Customer care & policy section not expanded");
					}
				}
				else
				{
					Fail("Footer menu Customer care & policy section is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-113 Verify that customer care & policy section should consists of these links as per the requirement*/
	public void CustomerPolicySection() throws Exception
		{		
			String links = HBCBasicfeature.getExcelVal("HBC113", sheet, 1);	
			String[] linkOptions = links.split("\n");
			ChildCreation("HUDSONBAY-113 Verify that customer care & policy section should consists of these links as per the requirement");
			try
			{
				Actions action = new Actions(driver);					
				wait.until(ExpectedConditions.visibilityOf(footerMenuCustomerPolicy));
				if(HBCBasicfeature.isElementPresent(footerMenuCustomerPolicy))
				{
					action.moveToElement(footerMenuCustomerPolicy).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
					{
						boolean optionfound = true;
						int size = MenuLists.size();
						for(int i=0;i<size;i++)
						{
							String link_text = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]")).getText();
							for(int j=0;j<linkOptions.length;j++)
							{
								if(link_text.equalsIgnoreCase(linkOptions[j]))
								{
									log.add("Customer care & policy section contains the link :"+link_text);
									optionfound = true;
									break;
								}
								else
								{
									optionfound = false;
									continue;
								}		
							}
						}
						if(optionfound == true)
						{
							Pass("Customer care & policy section consists of these links as per the requirement",log);
							action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
						}
						else
						{
							Fail("Customer care & policy section section not consists of these links as per the requirement",log);
							action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
						}
					}
					else
					{
						Fail("Customer care & policy section not expanded");
					}
				}
				else
				{
					Fail("Customer care & policy section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-114 Verify that on selecting the links from customer care & policy section should takes to its corresponding pages as expected.*/
	public void CustomerPolicySectionLinkSelection() throws Exception
		{		
			String[] linkOptions = HBCBasicfeature.getExcelVal("HBC113", sheet, 1).split("\n");
			String[] CustomerURL = HBCBasicfeature.getExcelVal("HBC113", sheet, 2).split("\n");
			ChildCreation("HUDSONBAY-114 Verify that on selecting the links from customer care & policy section should takes to its corresponding pages as expected.");
			try
			{
				Actions action = new Actions(driver);					
				wait.until(ExpectedConditions.visibilityOf(footerMenuCustomerPolicy));
				if(HBCBasicfeature.isElementPresent(footerMenuCustomerPolicy))
				{
					Pass("customer care & policy section displayed");
					action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(footerMenuWrapperDom, "style", "block"));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapperDom))
					{
						int size = MenuLists.size();
						for(int i=0;i<size;i++)
						{
							boolean flag = false;
							Thread.sleep(500);
							if(HBCBasicfeature.isElementPresent(footerMenuListActive))
							{
								Pass("customer care & policy Section in opened state");
							}
							else
							{
								action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
								wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
								Pass("customer care & policy Section in opened state");
							}
							Thread.sleep(500);
							WebElement link = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]"));
							HBCBasicfeature.scrolldown(link, driver);
							String link_text = link.getText();
							Thread.sleep(500);
							for(int j=0;j<linkOptions.length;j++)
							{
								if(link_text.equalsIgnoreCase(linkOptions[j]))
								{
									Pass("Customer care & policy section contains the link :"+link_text);
									HBCBasicfeature.scrolldown(link, driver);
									action.moveToElement(link).click().build().perform();
									wait.until(ExpectedConditions.attributeContains(footerSectionsDom, "style", "visible"));
									Thread.sleep(1000);
									String url = driver.getCurrentUrl();
									if(url.contains(CustomerURL[j]))
									{
										Pass("The expected "+link_text+"  page shown on selecting the "+link_text+" link and the URL is " + driver.getCurrentUrl());
										Pass("The URL Contains the Searched Keyword.");
										do
										{		
											logo.click();
											wait.until(ExpectedConditions.visibilityOf(homepage));
											if(HBCBasicfeature.isElementPresent(homepage))
											{
												flag = true;
											}
											else
											{
												continue;
											}											
										}
										while(flag!=true);	
										break;					
									}
									else
									{
										Fail("The expected page not shown on selecting the "+link_text+" link and the URL is  " + driver.getCurrentUrl());
										Fail("The URL not Contains the Searched Keyword.");
										logo.click();
										wait.until(ExpectedConditions.visibilityOf(homepage));
									}								
								}
								else
								{
									continue;
								}		
							}
						}
						driver.navigate().refresh();
					}
					else
					{
						Fail("Customer care & policy section not expanded");
					}
				}
				else
				{
					Fail("Customer care & policy section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HUDSONBAY-114"+e.getMessage());
				Exception("HUDSONBAY-114 There is something wrong. Please Check." + e.getMessage());
			}				
		}

	/*HUDSONBAY-115 Verify that HBC Rewards section should be shown as per the creative in all the pages of footer.*/
	public void menuHBCReward() throws Exception
		{		
			String expected = HBCBasicfeature.getExcelVal("HB115", sheet, 1);				
			ChildCreation("HUDSONBAY-115 Verify that HBC Rewards section should be shown as per the creative in all the pages of footer.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					wait.until(ExpectedConditions.visibilityOf(footerHBCRewards));
					if(HBCBasicfeature.isElementPresent(footerHBCRewards))
					{
						String actual = footerHBCRewards.getText();
						Thread.sleep(1000);
						if(actual.equalsIgnoreCase(expected))
						{
							log.add("HBC Rewards menu title" +actual+ " is displayed ");
							if(HBCBasicfeature.isElementPresent(footerAccordHBCRewards))
							{
								log.add("HBC Rewards section accordian is displayed");
								Pass("HBC Rewards section shown as per the creative",log);
							}
							else
							{
								Fail("HBC Rewards section not displayed as per the creative");
							}
						}
						else
						{
							Fail("HBC Rewards section title is not displayed");
						}
					}
					else
					{
						Fail("HBC Rewards section is not displayed");
					}					
				}
				else
				{
					Fail("Footer Menu list is not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-116 Verify that HBC Rewards section should be expanded on selecting it & collapse on closing it from footer.*/
	public void HBCRewardsClick()
		{
			ChildCreation("HUDSONBAY-116 Verify that HBC Rewards section should be expanded on selecting it & collapse on closing it from footer.");
			try
			{
				Actions action = new Actions(driver);	
				wait.until(ExpectedConditions.visibilityOf(footerHBCRewards));
				if(HBCBasicfeature.isElementPresent(footerHBCRewards))
				{
					action.moveToElement(footerHBCRewards).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
					{
						log.add("on selecting HBC Rewards then the section is expanded");
						Thread.sleep(1000);
						action.moveToElement(footerAccordHBCRewards).click().build().perform();
						Thread.sleep(1000);
						if(!HBCBasicfeature.isElementPresent(footerMenuListActive))
						{
							log.add("on selecting HBC Rewards then the section is collapsed");
							Pass("HBC Rewards section expanded on selecting it & collapse on closing it from footer",log);
						}
						else
						{
							Fail("HBC Rewards section not expanded on selecting it & not collapse on closing it from footer");
						}
					}
					else
					{
						Fail("HBC Rewards section not expanded");
					}
				}
				else
				{
					Fail("Footer menu HBC Rewards section is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
	
	/*HUDSONBAY-117 Verify that HBC Rewards section should consists of these links as per the requirement*/
	public void HBCRewardsSection() throws Exception
		{		
			String links = HBCBasicfeature.getExcelVal("HBC117", sheet, 1);	
			String[] linkOptions = links.split("\n");
			ChildCreation("HUDSONBAY-117 Verify that HBC Rewards section should consists of these links as per the requirement");
			try
			{
				Actions action = new Actions(driver);					
				wait.until(ExpectedConditions.visibilityOf(footerHBCRewards));
				if(HBCBasicfeature.isElementPresent(footerHBCRewards))
				{
					action.moveToElement(footerHBCRewards).click().build().perform();
					wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
					{
						boolean optionfound = true;
						int size = MenuLists.size();
						for(int i=0;i<size;i++)
						{
							String link_text = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]")).getText();
							Thread.sleep(500);
							for(int j=0;j<linkOptions.length;j++)
							{
								if(link_text.equalsIgnoreCase(linkOptions[j]))
								{
									log.add("HBC Rewards section contains the link :"+link_text);
									optionfound = true;
									break;
								}
								else
								{
									optionfound = false;
									continue;
								}		
							}
						}
						if(optionfound == true)
						{
							Pass("HBC Rewards section consists of these links as per the requirement",log);
							action.moveToElement(footerAccordHBCRewards).click().build().perform();
						}
						else
						{
							Fail("HBC Rewards section section not consists of these links as per the requirement",log);
							action.moveToElement(footerAccordHBCRewards).click().build().perform();
						}
					}
					else
					{
						Fail("HBC Rewards section not expanded");
					}
				}
				else
				{
					Fail("HBC Rewards section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-118 Verify that on selecting the links from HBC Rewards section should takes to its corresponding pages as expected.*/
	public void HBCRewardsSectionLinksSelection() throws Exception
		{		
			String[] linkOptions = HBCBasicfeature.getExcelVal("HBC117", sheet, 1).split("\n");
			String[] HBCRewardURL = HBCBasicfeature.getExcelVal("HBC117", sheet, 2).split("\n");
			ChildCreation("HUDSONBAY-118 Verify that on selecting the links from HBC Rewards section should takes to its corresponding pages as expected");
			try
			{
				Actions action = new Actions(driver);					
				wait.until(ExpectedConditions.visibilityOf(footerHBCRewards));
				if(HBCBasicfeature.isElementPresent(footerHBCRewards))
				{
					Pass("HBC Rewards section displayed");
					action.moveToElement(footerAccordHBCRewards).click().build().perform();
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(footerMenuWrapperDom, "style", "block"));
					if(HBCBasicfeature.isElementPresent(footerMenuWrapperDom))
					{
						int size = MenuLists.size();
						for(int i=0;i<size;i++)
						{
							if(HBCBasicfeature.isElementPresent(footerMenuListActive))
							{
								Pass("HBC Rewards section in opened state");
							}
							else
							{
								action.moveToElement(footerAccordHBCRewards).click().build().perform();
								wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
								Pass("HBC Rewards section in opened state");
							}
							WebElement link= driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]"));
							String link_text = link.getText();
							Thread.sleep(500);
							for(int j=0;j<linkOptions.length;j++)
							{
								if(link_text.equalsIgnoreCase(linkOptions[j]))
								{
									Pass("HBC Rewards section contains the link :"+link_text);
									String handle = driver.getWindowHandle();
									HBCBasicfeature.scrolldown(link, driver);
									link.click();
									Thread.sleep(2000);
									boolean linkfound = false;
									for(String newWin: driver.getWindowHandles())
									{
										if(!newWin.equals(handle))
										{
											driver.switchTo().window(newWin);
											Thread.sleep(1000);
											String url = driver.getCurrentUrl();
											Thread.sleep(1000);
											if(url.contains(HBCRewardURL[j]))
											{
												Pass("The expected "+link_text+"  page shown on selecting the "+link_text+" link and the URL is " + driver.getCurrentUrl());
												Pass("The URL Contains the Searched Keyword.");
												linkfound = true;
												break;
											}
											else
											{
												linkfound = false;
												continue;
											}							
										}									
									 }								
									if(linkfound == true)
									{
										driver.close();								
										driver.switchTo().window(handle);
										Pass("The "+link_text+" link was clicked and the tab is closed successfully.");
										Thread.sleep(1000);
										break;
									}
									else
									{
										Fail("The expected page shown on selecting the "+link_text+" link and the URL is  " + driver.getCurrentUrl());
										Fail("The URL not Contains the Searched Keyword.");
										driver.close();
										driver.switchTo().window(handle);
										Thread.sleep(1000);
									}								
								}
								else
								{
									continue;
								}		
							}
						}
						action.moveToElement(footerAccordHBCRewards).click().build().perform();
					}
					else
					{
						Fail("HBC Rewards section not expanded");
					}
				}
				else
				{
					Fail("HBC Rewards section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println("HUDSONBAY-118"+e.getMessage());
				Exception("HUDSONBAY-118 There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-119 Verify that social share section should be shown as per the creative in all the pages of footer.*/
	public void socialShareSection()
		{
			boolean childFlag = false;
			ChildCreation("HUDSONBAY-119 Verify that social share section should be shown as per the creative in all the pages of footer.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
				if(HBCBasicfeature.isElementPresent(footerSocialIcon))
				{
					childFlag = true;
					Pass("Social share section shown as per the creative in all the pages of footer");
				}
				else
				{
					Fail("Social share section not shown as per the creative in all the pages of footer");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-144 Verify that social share section should be shown as per the creative in all the pages of footer.");
			if(childFlag)
			{
				Pass("Social share section shown as per the creative in all the pages of footer");
			}
			else
			{
				Fail("Social share section not shown as per the creative in all the pages of footer");
			}
			
		}
	
	/*HUDSONBAY-120 Verify that social share section should consists of these share icons links.Facebook, Pinterest, Instagram, Twitter, YouTube*/
	public void socialShareIcons()
		{
			ChildCreation("HUDSONBAY-120 Verify that social share section should consists of these share icons links.Facebook, Pinterest, Instagram, Twitter, YouTube");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
				if(HBCBasicfeature.isListElementPresent(socialIconsLists))
				{
					if(HBCBasicfeature.isElementPresent(footerSocialFB))
					{
						Pass("Social Icon Facebook displayed");
					}
					else
					{
						Fail("Social Icon Facebook not displayed");
					}
					if(HBCBasicfeature.isElementPresent(footerSocialPin))
					{
						Pass("Social Icon Pinterest displayed");
					}
					else
					{
						Fail("Social Icon Pinterest not displayed");
					}
					if(HBCBasicfeature.isElementPresent(footerSocialInsta))
					{
						Pass("Social Icon Instagram displayed");
					}
					else
					{
						Fail("Social Icon Instagram not displayed");
					}
					if(HBCBasicfeature.isElementPresent(footerSocialTwitter))
					{
						Pass("Social Icon Twitter displayed");
					}
					else
					{
						Fail("Social Icon Twitter not displayed");
					}
					if(HBCBasicfeature.isElementPresent(footerSocialYou))
					{
						Pass("Social Icon Youtube displayed");
					}
					else
					{
						Fail("Social Icon Youtube not displayed");
					}
				}
				else
				{
					Fail("Social share Icons not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-121 Verify that expected facebook page should be shown on selecting the facebook share icon from the footer.*/
	public void shareFB()
		{
			ChildCreation("HUDSONBAY-121 Verify that expected facebook page should be shown on selecting the facebook share icon from the footer.");
			try
			{
				String social = HBCBasicfeature.getExcelVal("HB121", sheet, 1);
				boolean linkfound = true;
				wait.until(ExpectedConditions.visibilityOf(footerSocialFB));
				if(HBCBasicfeature.isElementPresent(footerSocialFB))
				{	
					String handle = driver.getWindowHandle();
					Thread.sleep(2000);
					footerSocialFB.click();
					Thread.sleep(2000);
					for(String newWin: driver.getWindowHandles())
					{
						if(!newWin.equals(handle))
						{
							driver.switchTo().window(newWin);
							Thread.sleep(2000);
							String url = driver.getCurrentUrl();
							Thread.sleep(1000);
							if(url.contains(social))
							{
								Pass("The expected facebook page shown on selecting the facebook share icon is " + driver.getCurrentUrl());
								Pass("The URL Contains the Searched Keyword.");
								linkfound = true;
								break;
							}
							else
							{
								linkfound = false;
								continue;
							}							
						}									
					 }								
					if(linkfound == true)
					{
						driver.close();								
						driver.switchTo().window(handle);
						Pass("The facebook icon was clicked and the tab is closed successfully.");
						Thread.sleep(2000);
					}
					else
					{
						Fail("The expected facebook page shown on selecting the facebook share icon is  " + driver.getCurrentUrl());
						Fail("The URL not Contains the Searched Keyword.");
						driver.close();
						driver.switchTo().window(handle);
						Thread.sleep(2000);
					}					
				}
				else
				{
					Fail("Facebook icon is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-122 Verify that expected Pinterest page should be shown on selecting the Pinterest share icon from the footer.*/
	public void sharePIN()
		{
			ChildCreation("HUDSONBAY-122 Verify that expected Pinterest page should be shown on selecting the Pinterest share icon from the footer.");
			try
			{
				String social = HBCBasicfeature.getExcelVal("HB122", sheet, 1);
				boolean linkfound = true;
				wait.until(ExpectedConditions.visibilityOf(footerSocialPin));
				if(HBCBasicfeature.isElementPresent(footerSocialPin))
				{	
					String handle = driver.getWindowHandle();
					Thread.sleep(1000);
					footerSocialPin.click();
					Thread.sleep(1000);
					for(String newWin: driver.getWindowHandles())
					{
						if(!newWin.equals(handle))
						{
							driver.switchTo().window(newWin);
							Thread.sleep(2000);
							String url = driver.getCurrentUrl();
							Thread.sleep(1000);
							if(url.contains(social))
							{
								Pass("The expected Pinterest page shown on selecting the Pinterest share icon is " + driver.getCurrentUrl());
								Pass("The URL Contains the Searched Keyword.");
								linkfound = true;
								break;
							}
							else
							{
								linkfound = false;
								continue;
							}							
						}									
					 }								
					if(linkfound == true)
					{
						driver.close();								
						driver.switchTo().window(handle);
						Pass("The Pinterest icon was clicked and the tab is closed successfully.");
						Thread.sleep(1000);
					}
					else
					{
						Fail("The expected Pinterest page shown on selecting the Pinterest share icon is  " + driver.getCurrentUrl());
						Fail("The URL not Contains the Searched Keyword.");
						driver.close();
						driver.switchTo().window(handle);
						Thread.sleep(1000);
					}					
				}
				else
				{
					Fail("Pinterest icon is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-123 Verify that expected Instagram page should be shown on selecting the Instagram share icon from the footer.*/
	public void shareInsta()
		{
			ChildCreation("HUDSONBAY-123 Verify that expected Instagram page should be shown on selecting the Instagram share icon from the footer.");
			try
			{
				String social = HBCBasicfeature.getExcelVal("HB123", sheet, 1);
				boolean linkfound = true;
				wait.until(ExpectedConditions.visibilityOf(footerSocialInsta));
				if(HBCBasicfeature.isElementPresent(footerSocialInsta))
				{	
					String handle = driver.getWindowHandle();
					Thread.sleep(1000);
					footerSocialInsta.click();
					Thread.sleep(1000);
					for(String newWin: driver.getWindowHandles())
					{
						if(!newWin.equals(handle))
						{
							driver.switchTo().window(newWin);
							Thread.sleep(2000);
							String url = driver.getCurrentUrl();
							Thread.sleep(1000);
							if(url.contains(social))
							{
								Pass("The expected Instagram page shown on selecting the Instagram share icon is " + driver.getCurrentUrl());
								Pass("The URL Contains the Searched Keyword.");
								linkfound = true;
								break;
							}
							else
							{
								linkfound = false;
								continue;
							}							
						}									
					 }								
					if(linkfound == true)
					{
						driver.close();								
						driver.switchTo().window(handle);
						Pass("The Instagram icon was clicked and the tab is closed successfully.");
						Thread.sleep(1000);
					}
					else
					{
						Fail("The expected Instagram page shown on selecting the Instagram share icon is  " + driver.getCurrentUrl());
						Fail("The URL not Contains the Searched Keyword.");
						driver.close();
						driver.switchTo().window(handle);
						Thread.sleep(1000);
					}					
				}
				else
				{
					Fail("Instagram icon is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-124 Verify that expected Twitter page should be shown on selecting the Twitter share icon from the footer.*/
	public void shareTwitter()
		{
			ChildCreation("HUDSONBAY-124 Verify that expected Twitter page should be shown on selecting the Twitter share icon from the footer.");
			try
			{
				String social = HBCBasicfeature.getExcelVal("HB124", sheet, 1);
				boolean linkfound = true;
				wait.until(ExpectedConditions.visibilityOf(footerSocialTwitter));
				if(HBCBasicfeature.isElementPresent(footerSocialTwitter))
				{	
					String handle = driver.getWindowHandle();
					Thread.sleep(1000);
					footerSocialTwitter.click();
					Thread.sleep(1000);
					for(String newWin: driver.getWindowHandles())
					{
						if(!newWin.equals(handle))
						{
							driver.switchTo().window(newWin);
							Thread.sleep(2000);
							String url = driver.getCurrentUrl();
							Thread.sleep(1000);
							if(url.contains(social))
							{
								Pass("The expected Twitter page shown on selecting the Twitter share icon is " + driver.getCurrentUrl());
								Pass("The URL Contains the Searched Keyword.");
								linkfound = true;
								break;
							}
							else
							{
								linkfound = false;
								continue;
							}							
						}									
					 }								
					if(linkfound == true)
					{
						driver.close();								
						driver.switchTo().window(handle);
						Pass("The Twitter icon was clicked and the tab is closed successfully.");
						Thread.sleep(1000);
					}
					else
					{
						Fail("The expected Twitter page shown on selecting the Twitter share icon is  " + driver.getCurrentUrl());
						Fail("The URL not Contains the Searched Keyword.");
						driver.close();
						driver.switchTo().window(handle);
						Thread.sleep(1000);
					}					
				}
				else
				{
					Fail("Twitter icon is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
	
	/*HUDSONBAY-125 Verify that expected YouTube page should be shown on selecting the YouTube share icon from the footer.*/
	public void shareYoutube()
		{
			ChildCreation("HUDSONBAY-125 Verify that expected YouTube page should be shown on selecting the YouTube share icon from the footer.");
			try
			{
				String social = HBCBasicfeature.getExcelVal("HB125", sheet, 1);
				boolean linkfound = true;
				wait.until(ExpectedConditions.visibilityOf(footerSocialYou));
				if(HBCBasicfeature.isElementPresent(footerSocialYou))
				{	
					String handle = driver.getWindowHandle();
					Thread.sleep(1000);
					footerSocialYou.click();
					Thread.sleep(1000);
					for(String newWin: driver.getWindowHandles())
					{
						if(!newWin.equals(handle))
						{
							driver.switchTo().window(newWin);
							Thread.sleep(2000);
							String url = driver.getCurrentUrl();
							Thread.sleep(1000);
							if(url.contains(social))
							{
								Pass("The expected YouTube page shown on selecting the YouTube share icon is " + driver.getCurrentUrl());
								Pass("The URL Contains the Searched Keyword.");
								linkfound = true;
								break;
							}
							else
							{
								linkfound = false;
								continue;
							}							
						}									
					 }								
					if(linkfound == true)
					{
						driver.close();								
						driver.switchTo().window(handle);
						Pass("The YouTube icon was clicked and the tab is closed successfully.");
						Thread.sleep(1000);
					}
					else
					{
						Fail("The expected YouTube page shown on selecting the YouTube share icon is  " + driver.getCurrentUrl());
						Fail("The URL not Contains the Searched Keyword.");
						driver.close();
						driver.switchTo().window(handle);
						Thread.sleep(1000);
					}					
				}
				else
				{
					Fail("YouTube icon is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}	
	
	/*HUDSONBAY-126 Verify that Hudson Bay copyrights reserved text should be shown as per the creative in all the pages of footer.*/
	public void footerCopyright()
		{
			ChildCreation("HUDSONBAY-126 Verify that Hudson Bay copyrights reserved text should be shown as per the creative in all the pages of footer.");
			try
			{
				String actual = HBCBasicfeature.getExcelVal("HB126", sheet, 1).replaceAll("\\s+","");
				/*System.out.println(actual.length());*/
				log.add(actual);
				wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
				if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
				{
					String expected = footerBottomCopyright.getText().replaceAll("\\s+","");
					/*System.out.println(expected.length());
					System.out.println(actual);*/
					log.add(expected);
					Thread.sleep(500);
					if(expected.equalsIgnoreCase(actual))
					{
						Pass("Hudson Bay copyrights reserved text shown as per the creative in all the pages of footer",log);
					}
					else
					{
						Fail("Hudson Bay copyrights reserved text are not shown as per the creative in all the pages of footer",log);
					}
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}
		
	/*HUDSONBAY-96 Verify that Footer should be static in all the pages.*/
	public void footerStatic() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-96 Verify that Footer should be static in all the pages.");
			String key = HBCBasicfeature.getExcelVal("HB96", sheet, 1);
			try
			{
				HBCBasicfeature.scrollup(searchIcon, driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));		
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					boolean flag = false;
					do
					{
						try
						{
							searchIcon.click();
							wait.until(ExpectedConditions.visibilityOf(searchBox));
							searchBox.clear();
							searchBox.sendKeys(key);
							searchBox.sendKeys(Keys.ENTER);
							try
							{
								wait.until(ExpectedConditions.visibilityOf(plpPage));
								if(HBCBasicfeature.isElementPresent(plpPage))
								{
									flag=true;
									break;
								}
								else
								{
									continue;
								}		
							}
							catch(Exception e)
							{
								continue;
							}												
						}
						catch(Exception e)
						{
							continue;
						}
					}
					while(flag!=true);				
					if(HBCBasicfeature.isElementPresent(plpPage))
					{
						HBCBasicfeature.scrolldown(footerBottomCopyright, driver);
						creative();
						log.add("Footer static in PLP pages");						
						if(HBCBasicfeature.isListElementPresent(plpItemContList))
						{
							int size = plpItemContList.size();
							Random r  = new Random();
							int i = r.nextInt(size);
							WebElement product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
							wait.until(ExpectedConditions.visibilityOf(product));
							HBCBasicfeature.scrolldown(product, driver);
							if(HBCBasicfeature.isElementPresent(product))
							{		
								WebElement product_Clk = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])["+(i+1)+"]"));		
								product_Clk.click();
								wait.until(ExpectedConditions.visibilityOf(PDPPage));
								if(HBCBasicfeature.isElementPresent(PDPPage))
								{
									HBCBasicfeature.scrolldown(footerBottomCopyright, driver);
									creative();
									log.add("Footer static in PDP pages");
									if(creative == true)
									{
										Pass("Footer static in all the pages",log);
									}
									else
									{
										Fail("Footer not static in all the pages",log);
									}
								}
								else
								{
									Fail("PDP Page not displayed");
								}											
							}
							else
							{
								Fail("Selected product is not dispalyed");
							}	
						}						
						else
						{
							Fail("PLP products are not displayed");
						}
					}
					else
					{
						Fail("PLP page not displayed");
					}					
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	public boolean french()
	{
		try
		{			
			String act_Url = driver.getCurrentUrl();
			Actions action = new Actions(driver);
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(hamburger));	
			if(HBCBasicfeature.isElementPresent(hamburger))
			{
				boolean flag = false;
				do
				{
					try
					{
						action.moveToElement(hamburger).click().build().perform();
						Thread.sleep(500);
						if(HBCBasicfeature.isElementPresent(otherlanguage))
						{
							flag = true;
							break;
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
				while(flag!=true);					
				wait.until(ExpectedConditions.visibilityOf(otherlanguage));			
				if(HBCBasicfeature.isElementPresent(otherlanguage))
				{
					HBCBasicfeature.scrolldown(otherlanguage, driver);
					Thread.sleep(500);
					HBCBasicfeature.jsclick(otherlanguage, driver);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(homepageDom));
					String Changed_Url = driver.getCurrentUrl();
					if(act_Url!=Changed_Url)
					{
						labie = true;
					}
					else
					{
						Fail("switching the Language option respective site not displayed",log);
					}
				}
				else
				{
					Fail("Other language option not displayed");
				}
			}
			else
			{
				Fail("Hamburger menuu not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}
		return labie;			
	}
		
	/*HUDSONBAY-127 Verify that  Magasins Sp�cialis�s should be shown as per the creative in all the pages of footer.*/
	public void menuMagasinsSp�cialis�s()
		{		
			ChildCreation("HUDSONBAY-127 Verify that  Magasins Sp�cialis�s should be shown as per the creative in all the pages of footer.");
			try
			{
				labie = french();
				if(labie == true)
				{
					Pass("HBC french site displayed");
					String expected = HBCBasicfeature.getExcelVal("HB102", sheet, 2);				
					wait.until(ExpectedConditions.visibilityOf(footerMenuList));
					if(HBCBasicfeature.isElementPresent(footerMenuList))
					{
						HBCBasicfeature.scrolldown(footerMenuList, driver);
						wait.until(ExpectedConditions.visibilityOf(footerMenuShopHBC));
						if(HBCBasicfeature.isElementPresent(footerMenuShopHBC))
						{
							String actual = footerMenuShopHBC.getText();
							/*System.out.println(actual);
							System.out.println(expected);*/
							Thread.sleep(500);
							if(actual.equalsIgnoreCase(expected))
							{
								log.add("Magasins Sp�cialis�s menu title is "+actual);
								if(HBCBasicfeature.isElementPresent(footerAccordShopHBC))
								{
									log.add("Magasins Sp�cialis�s accordian is displayed");
									Pass("Magasins Sp�cialis�s shown as per the creative",log);
								}
								else
								{
									Fail("Magasins Sp�cialis�s not displayed as per the creative");
								}
							}
							else
							{
								Fail("Magasins Sp�cialis�s menu title is not displayed");
							}
						}
						else
						{
							Fail("Magasins Sp�cialis�s menu is not displayed");
						}					
					}
					else
					{
						Fail("Footer Menu list is not displayed");
					}	
				}
				else
				{
					Skip("HBC french site not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
		}

	/*HUDSONBAY-128 Verify that  Magasins Sp�cialis�s section should be expanded on selecting it & collapse on closing it from footer.*/
	public void MagasinsSp�cialis�sClick()
		{
			ChildCreation("HUDSONBAY-128 Verify that Magasins Sp�cialis�s section should be expanded on selecting it & collapse on closing it from footer.");
			if(labie == true)
			{
				try
				{
					Actions action = new Actions(driver);	
					wait.until(ExpectedConditions.visibilityOf(footerMenuShopHBC));
					if(HBCBasicfeature.isElementPresent(footerMenuShopHBC))
					{
						action.moveToElement(footerMenuShopHBC).click().build().perform();
						wait.until(ExpectedConditions.attributeContains(footerMenuWrapper, "style", "block"));
						wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
						{
							log.add("on selecting Magasins Sp�cialis�s then the section is expanded");
							action.moveToElement(footerAccordShopHBC).click().build().perform();
							wait.until(ExpectedConditions.attributeContains(footerMenuWrapper, "style", "none"));
							if(!HBCBasicfeature.isElementPresent(footerMenuListActive))
							{
								log.add("on selecting Magasins Sp�cialis�s then the section is collapsed");
								Pass("Magasins Sp�cialis�s section expanded on selecting it & collapse on closing it from footer",log);
							}
							else
							{
								Fail("Magasins Sp�cialis�s section not expanded on selecting it & not collapse on closing it from footer");
							}
						}
						else
						{
							Fail("Magasins Sp�cialis�s section not expanded");
						}
					}
					else
					{
						Fail("Footer menu Magasins Sp�cialis�s is not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}				
			}
			else
			{
				Skip("HBC french site not displayed");
			}			
		}
		
	/*HUDSONBAY-129 Verify that Magasins Sp�cialis�s section should consists of these links as per the requirement*/
	public void MagasinsSp�cialis�Section()
		{		
			ChildCreation("HUDSONBAY-129 Verify that Shop HBC section should consists of these links as per the requirement");
			if(labie == true)
			{
				try
				{
					String[] linkOptions = HBCBasicfeature.getExcelVal("HB104", sheet, 2).split("\n");
					Actions action = new Actions(driver);					
					wait.until(ExpectedConditions.visibilityOf(footerMenuShopHBC));
					if(HBCBasicfeature.isElementPresent(footerMenuShopHBC))
					{
						action.moveToElement(footerMenuShopHBC).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
						{
							boolean optionfound = true;
							int size = MenuLists.size();
							for(int i=0;i<size;i++)
							{
								String link_text = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]")).getText();
								for(int j=0;j<linkOptions.length;j++)
								{
									if(link_text.equalsIgnoreCase(linkOptions[j]))
									{
										log.add("Magasins Sp�cialis� contains the link :"+link_text);
										optionfound = true;
										break;
									}
									else
									{
										optionfound = false;
										continue;
									}		
								}
							}
							if(optionfound == true)
							{
								Pass("Magasins Sp�cialis� section consists of these links as per the requirement",log);
								action.moveToElement(footerAccordShopHBC).click().build().perform();
							}
							else
							{
								Fail("Magasins Sp�cialis� section not consists of these links as per the requirement",log);
								action.moveToElement(footerAccordShopHBC).click().build().perform();
							}
						}
						else
						{
							Fail("Magasins Sp�cialis� section not expanded");
						}
					}
					else
					{
						Fail("Magasins Sp�cialis� section not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("HBC french site not displayed");
			}
		}
	
	/*HUDSONBAY-130 Verify that on selecting the links from  Magasins Sp�cialis�s should takes to its corresponding pages as expected.*/
	public void MagasinsSp�cialis�sSectionLinkSelection() 
		{		
			ChildCreation("HUDSONBAY-130 Verify that on selecting the links from Magasins Sp�cialis�s should takes to its corresponding pages as expected.");
			if(labie == true)
			{
				try
				{
					Actions action = new Actions(driver);	
					String[] linkOptions = HBCBasicfeature.getExcelVal("HB104", sheet, 2).split("\n");
					wait.until(ExpectedConditions.visibilityOf(footerMenuShopHBC));
					if(HBCBasicfeature.isElementPresent(footerMenuShopHBC))
					{
						Pass("Magasins Sp�cialis�s Section displayed");
						action.moveToElement(footerAccordShopHBC).click().build().perform();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(footerMenuWrapper, "style", "block"));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapperDom))
						{
							int size = MenuLists.size();
							for(int i=0;i<size;i++)
							{
								boolean flag = false;
								if(HBCBasicfeature.isElementPresent(footerMenuListActive))
								{
									Pass("Magasins Sp�cialis�s Section in opened state");
								}
								else
								{
									action.moveToElement(footerAccordShopHBC).click().build().perform();
									Thread.sleep(500);
									wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
									Pass("Magasins Sp�cialis�s Section in opened state");
								}
								WebElement link = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]"));
								String link_text = link.getText();
								Thread.sleep(500);
								for(int j=0;j<linkOptions.length;j++)
								{
									if(link_text.equalsIgnoreCase(linkOptions[j]))
									{
										Pass("Magasins Sp�cialis�s section contains the link :"+link_text);
										wait.until(ExpectedConditions.visibilityOf(link));
										HBCBasicfeature.scrolldown(link, driver);
										Thread.sleep(1000);
										action.moveToElement(link).click().build().perform();
										wait.until(ExpectedConditions.visibilityOf(plpPage));
										if(HBCBasicfeature.isElementPresent(plpPage))
										{
											Pass("The expected "+link_text+"  plp page shown on selecting the "+link_text+" link");
											Thread.sleep(1000);										
											do
											{		
												logo.click();
												wait.until(ExpectedConditions.visibilityOf(homepage));
												if(HBCBasicfeature.isElementPresent(homepage))
												{
													flag = true;
												}
												else
												{
													continue;
												}											
											}
											while(flag!=true);	
											break;
										}								
										else
										{
											Fail("The expected plp page not shown on selecting the "+link_text+" link");
											Thread.sleep(500);
											logo.click();
											wait.until(ExpectedConditions.visibilityOf(homepage));
										}								
									}
									else
									{
										continue;
									}		
								}
							}
						/*	WebElement activeAccordClose = driver.findElement(By.xpath("//*[@class='footer-menu-list active']//*[@class='sk_footerAccordIcon']"));
							action.moveToElement(activeAccordClose).click().build().perform();			*/
						}
						else
						{
							Fail("Magasins Sp�cialis�s section not expanded");
						}
					}
					else
					{
						Fail("Magasins Sp�cialis�s section not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-130"+e.getMessage());
					Exception("HUDSONBAY-130 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("HBC french site not displayed");
			}
		}
	
	/*HUDSONBAY-131 Verify that magasins + entreprises section should be shown as per the creative in all the pages of footer.*/
	public void menuMagasinsEntreprises() throws Exception
		{		
			ChildCreation("HUDSONBAY-131 Verify that magasins + entreprises section should be shown as per the creative in all the pages of footer.");
			if(labie == true)
			{
				try
				{
					String expected = HBCBasicfeature.getExcelVal("HB106", sheet, 2);				
					wait.until(ExpectedConditions.visibilityOf(footerMenuList));
					if(HBCBasicfeature.isElementPresent(footerMenuList))
					{
						wait.until(ExpectedConditions.visibilityOf(footerMenuStoreCorp));
						if(HBCBasicfeature.isElementPresent(footerMenuStoreCorp))
						{
							String actual = footerMenuStoreCorp.getText();
							Thread.sleep(1000);
							if(actual.equalsIgnoreCase(expected))
							{
								log.add("magasins + entreprises menu title " +actual+ " is displayed ");
								if(HBCBasicfeature.isElementPresent(footerAccordShopHBC))
								{
									log.add("magasins + entreprises section accordian is displayed");
									Pass("magasins + entreprises section shown as per the creative",log);
								}
								else
								{
									Fail("magasins + entreprises section not displayed as per the creative");
								}
							}
							else
							{
								Fail("magasins + entreprises section title is not displayed");
							}
						}
						else
						{
							Fail("magasins + entreprises section is not displayed");
						}					
					}
					else
					{
						Fail("Footer Menu list is not displayed");
					}				
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("HBC french site not displayed");
			}
		}
	
	/*HUDSONBAY-132 Verify that magasins + entreprises section should be expanded on selecting it & collapse on closing it from footer..*/
	public void MagasinsEntreprisesClick()
		{
			ChildCreation("HUDSONBAY-132 Verify that magasins + entreprises section should be expanded on selecting it & collapse on closing it from footer.");
			if(labie == true)
			{
				try
				{
					Actions action = new Actions(driver);	
					wait.until(ExpectedConditions.visibilityOf(footerMenuStoreCorp));
					if(HBCBasicfeature.isElementPresent(footerMenuStoreCorp))
					{
						action.moveToElement(footerMenuStoreCorp).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
						{
							log.add("on selecting magasins + entreprises then the section is expanded");
							Thread.sleep(1000);
							action.moveToElement(footerAccordStoreCorp).click().build().perform();
							Thread.sleep(1000);
							if(!HBCBasicfeature.isElementPresent(footerMenuListActive))
							{
								log.add("on selecting magasins + entreprises then the section is collapsed");
								Pass("magasins + entreprises section expanded on selecting it & collapse on closing it from footer",log);
							}
							else
							{
								Fail("magasins + entreprises section not expanded on selecting it & not collapse on closing it from footer");
							}
						}
						else
						{
							Fail("magasins + entreprises section not expanded");
						}
					}
					else
					{
						Fail("Footer menu magasins + entreprises  is not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("HBC french site not displayed");
			}				
		}
	
	/*HUDSONBAY-133 Verify that magasins + entreprises section should consists of these links as per the requirement*/
	public void MagasinsEntreprisesSection() throws Exception
		{			
			ChildCreation("HUDSONBAY-133 Verify that magasins + entreprises section should consists of these links as per the requirement");
			if(labie == true)
			{
				try
				{
					Actions action = new Actions(driver);	
					String links = HBCBasicfeature.getExcelVal("HBC108", sheet, 3);	
					String[] linkOptions = links.split("\n");
					wait.until(ExpectedConditions.visibilityOf(footerMenuStoreCorp));
					if(HBCBasicfeature.isElementPresent(footerMenuStoreCorp))
					{
						action.moveToElement(footerMenuStoreCorp).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
						{
							boolean optionfound = true;
							int size = MenuLists.size();
							for(int i=0;i<size;i++)
							{
								String link_text = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]")).getText();
								for(int j=0;j<linkOptions.length;j++)
								{
									if(link_text.equalsIgnoreCase(linkOptions[j]))
									{
										log.add("magasins + entreprises section contains the link :"+link_text);
										optionfound = true;
										break;
									}
									else
									{
										optionfound = false;
										continue;
									}		
								}
							}
							if(optionfound == true)
							{
								Pass("magasins + entreprises section consists of these links as per the requirement",log);
								action.moveToElement(footerAccordStoreCorp).click().build().perform();
							}
							else
							{
								Fail("magasins + entreprises section section not consists of these links as per the requirement",log);
								action.moveToElement(footerAccordStoreCorp).click().build().perform();
							}
						}
						else
						{
							Fail("magasins + entreprises section not expanded");
						}
					}
					else
					{
						Fail("magasins + entreprises section not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("HBC french site not displayed");
			}			
		}
	
	/*HUDSONBAY-134/135 Verify that on selecting the links from magasins + entreprises  section should takes to its corresponding pages as expected.*/
	public void MagasinsEntreprisesSectionLinksSelection() throws Exception
		{				
			ChildCreation("HUDSONBAY-134 Verify that on selecting the links from magasins + entreprises section should takes to its corresponding pages as expected.");
			if(labie == true)
			{
				boolean HBC135 = false;
				try
				{
					Actions action = new Actions(driver);	
					String[] storeCorp = HBCBasicfeature.getExcelVal("HBC108", sheet, 4).split("\n");
					String[] linkOptions = HBCBasicfeature.getExcelVal("HBC108", sheet, 3).split("\n");
					wait.until(ExpectedConditions.visibilityOf(footerMenuStoreCorp));
					if(HBCBasicfeature.isElementPresent(footerMenuStoreCorp))
					{
						Pass("magasins + entreprises Section displayed");
						action.moveToElement(footerAccordStoreCorp).click().build().perform();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(footerMenuWrapperDom, "style", "block"));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapperDom))
						{
							int size = MenuLists.size();
							for(int i=0;i<size;i++)
							{
								boolean flag = false;
								Thread.sleep(500);
								if(HBCBasicfeature.isElementPresent(footerMenuListActive))
								{
									Pass("magasins + entreprises Section in opened state");
								}
								else
								{
									action.moveToElement(footerAccordStoreCorp).click().build().perform();
									wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
									Pass("magasins + entreprises Section in opened state");
								}
								WebElement link = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]"));
								HBCBasicfeature.scrolldown(link, driver);
								String link_text = link.getText();
								Thread.sleep(500);
								for(int j=0;j<linkOptions.length;j++)
								{
									if(link_text.equalsIgnoreCase(linkOptions[j]))
									{
										Pass("magasins + entreprises section contains the link :"+link_text);
										try
										{
											WebElement Outlink = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]//a[@target='_blank']"));
											if(HBCBasicfeature.isElementPresent(Outlink))
											{
												String handle = driver.getWindowHandle();
												HBCBasicfeature.scrolldown(Outlink, driver);
												Outlink.click();
												Thread.sleep(2000);
												boolean linkfound = false;
												for(String newWin: driver.getWindowHandles())
												{
													if(!newWin.equals(handle))
													{
														driver.switchTo().window(newWin);
														Thread.sleep(1000);
														String url = driver.getCurrentUrl();
														Thread.sleep(1000);
														if(url.contains(storeCorp[j]))
														{
															Pass("The expected "+link_text+"  page shown on selecting the "+link_text+" link and the URL is " + driver.getCurrentUrl());
															Pass("The URL Contains the Searched Keyword.");
															HBC135 = true;
															linkfound = true;
															break;
														}
														else
														{
															linkfound = false;
															continue;
														}							
													}									
												 }								
												if(linkfound == true)
												{
													driver.close();								
													driver.switchTo().window(handle);
													Pass("The "+link_text+" link was clicked and the tab is closed successfully.");
													HBC135 = true;
													Thread.sleep(1000);
													break;
												}
												else
												{
													Fail("The expected page shown on selecting the "+link_text+" link and the URL is  " + driver.getCurrentUrl());
													Fail("The URL not Contains the Searched Keyword.");
													HBC135 = false;
													driver.close();
													driver.switchTo().window(handle);
													Thread.sleep(1000);
												}								
											}
											action.moveToElement(footerAccordStoreCorp).click().build().perform();																		
										}
										catch(Exception e)
										{
											Thread.sleep(1000);
											HBCBasicfeature.scrolldown(link, driver);
											action.moveToElement(link).click().build().perform();
											//wait.until(ExpectedConditions.attributeContains(footerSectionsDom, "style", "visible"));
											Thread.sleep(3000);
											String url = driver.getCurrentUrl();
											if(url.contains(storeCorp[j]))
											{
												Pass("The expected "+link_text+"  page shown on selecting the "+link_text+" link and the URL is " + driver.getCurrentUrl());
												Pass("The URL Contains the Searched Keyword.");
												HBC135 = true;
												do
												{		
													logo.click();
													wait.until(ExpectedConditions.visibilityOf(homepage));
													if(HBCBasicfeature.isElementPresent(homepage))
													{
														flag = true;
													}
													else
													{
														continue;
													}											
												}
												while(flag!=true);	
												break;											
											}
											else
											{
												Fail("The expected page not shown on selecting the "+link_text+" link and the URL is  " + driver.getCurrentUrl());
												Fail("The URL not Contains the Searched Keyword.");
												HBC135 = false;
												logo.click();		
												wait.until(ExpectedConditions.visibilityOf(homepage));
											}								
										}
									}
									else
									{
										continue;
									}		
								}
							}
							driver.navigate().refresh();
						}
						else
						{
							Fail("magasins + entreprises section not expanded");
						}
					}
					else
					{
						Fail("magasins + entreprises section not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-134" +e.getMessage());
					Exception("HUDSONBAY-134 There is something wrong. Please Check." + e.getMessage());
				}	
				
				ChildCreation("HUDSONBAY-135 Verify that on selecting the links from magasins + entreprises section should takes to its corresponding pages as expected.");
				if(HBC135 == true)
				{
					Pass("on selecting the links from magasins + entreprises section takes to its corresponding pages as expected");
				}
				else
				{
					Fail("on selecting the links from magasins + entreprises section not takes to its corresponding pages as expected");
				}				
			}
			else
			{
				Skip("HBC french site not displayed");
			}				
		}

	/*HUDSONBAY-136 Verify that service � la client�le et politique section should be shown as per the creative in all the pages of footer.*/
	public void menuservice�laclient�leetpolitique() 
		{		
			ChildCreation("HUDSONBAY-136 Verify that service � la client�le et politique section should be shown as per the creative in all the pages of footer.");
			if(labie == true)
			{
				try
				{
					String expected = HBCBasicfeature.getExcelVal("HB111", sheet, 2);				
					wait.until(ExpectedConditions.visibilityOf(footerMenuList));
					if(HBCBasicfeature.isElementPresent(footerMenuList))
					{
						wait.until(ExpectedConditions.visibilityOf(footerMenuCustomerPolicy));
						if(HBCBasicfeature.isElementPresent(footerMenuCustomerPolicy))
						{
							String actual = footerMenuCustomerPolicy.getText();
							Thread.sleep(1000);
							if(actual.equalsIgnoreCase(expected))
							{
								log.add("service � la client�le et politique menu title" +actual+ " is displayed ");
								if(HBCBasicfeature.isElementPresent(footerAccordCustomerPolicy))
								{
									log.add("service � la client�le et politiquesection accordian is displayed");
									Pass("service � la client�le et politique section shown as per the creative",log);
								}
								else
								{
									Fail("service � la client�le et politique section not displayed as per the creative");
								}
							}
							else
							{
								Fail("service � la client�le et politique section title is not displayed");
							}
						}
						else
						{
							Fail("service � la client�le et politique section is not displayed");
						}					
					}
					else
					{
						Fail("Footer Menu list is not displayed");
					}				
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("HBC french site not displayed");
			}	
		}
	
	/*HUDSONBAY-137 Verify that service � la client�le et politique section should be expanded on selecting it & collapse on closing it from footer.*/
	public void service�laclient�leetpolitiqueClick()
		{
			ChildCreation("HUDSONBAY-137 Verify that service � la client�le et politique section should be expanded on selecting it & collapse on closing it from footer.");
			if(labie == true)
			{
				try
				{
					Actions action = new Actions(driver);	
					wait.until(ExpectedConditions.visibilityOf(footerMenuCustomerPolicy));
					if(HBCBasicfeature.isElementPresent(footerMenuCustomerPolicy))
					{
						action.moveToElement(footerMenuCustomerPolicy).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
						{
							log.add("on selecting Service � la client�le et politique then the section is expanded");
							Thread.sleep(1000);
							action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
							Thread.sleep(1000);
							if(!HBCBasicfeature.isElementPresent(footerMenuListActive))
							{
								log.add("on selecting service � la client�le et politique then the section is collapsed");
								Pass("service � la client�le et politique section expanded on selecting it & collapse on closing it from footer",log);
							}
							else
							{
								Fail("service � la client�le et politique section not expanded on selecting it & not collapse on closing it from footer");
							}
						}
						else
						{
							Fail("service � la client�le et politique section not expanded");
						}
					}
					else
					{
						Fail("Footer menu service � la client�le et politique section is not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("HBC french site not displayed");
			}				
		}
	
	/*HUDSONBAY-138 Verify that service � la client�le et politique section should consists of these links as per the requirement*/
	public void service�laclient�leetpolitiqueSection() throws Exception
		{		
			ChildCreation("HUDSONBAY-138 Verify that service � la client�le et politique section should consists of these links as per the requirement");
			if(labie == true)
			{				
				try
				{
					String links = HBCBasicfeature.getExcelVal("HBC113", sheet, 3);	
					String[] linkOptions = links.split("\n");
					Actions action = new Actions(driver);					
					wait.until(ExpectedConditions.visibilityOf(footerMenuCustomerPolicy));
					if(HBCBasicfeature.isElementPresent(footerMenuCustomerPolicy))
					{
						action.moveToElement(footerMenuCustomerPolicy).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
						{
							boolean optionfound = true;
							int size = MenuLists.size();
							for(int i=0;i<size;i++)
							{
								String link_text = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]")).getText();
								for(int j=0;j<linkOptions.length;j++)
								{
									if(link_text.equalsIgnoreCase(linkOptions[j]))
									{
										log.add("service � la client�le et politique section contains the link :"+link_text);
										optionfound = true;
										break;
									}
									else
									{
										optionfound = false;
										continue;
									}		
								}
							}
							if(optionfound == true)
							{
								Pass("service � la client�le et politique section consists of these links as per the requirement",log);
								action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
							}
							else
							{
								Fail("service � la client�le et politique section section not consists of these links as per the requirement",log);
								action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
							}
						}
						else
						{
							Fail("service � la client�le et politique section not expanded");
						}
					}
					else
					{
						Fail("service � la client�le et politique section not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("HBC french site not displayed");
			}	
		}
	
	/*HUDSONBAY-139 Verify that on selecting the links from service � la client�le et politique section should takes to its corresponding pages as expected.*/
	public void service�laclient�leetpolitiqueSectionLinkSelection() throws Exception
		{			
			ChildCreation("HUDSONBAY-139 Verify that on selecting the links from service � la client�le et politique section should takes to its corresponding pages as expected.");
			if(labie == true)
			{			
				try
				{
					String[] linkOptions = HBCBasicfeature.getExcelVal("HBC113", sheet, 3).split("\n");
					String[] CustomerURL = HBCBasicfeature.getExcelVal("HBC113", sheet, 2).split("\n");
					Actions action = new Actions(driver);					
					wait.until(ExpectedConditions.visibilityOf(footerMenuCustomerPolicy));
					if(HBCBasicfeature.isElementPresent(footerMenuCustomerPolicy))
					{
						Pass("service � la client�le et politique section displayed");
						action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(footerMenuWrapperDom, "style", "block"));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapperDom))
						{
							int size = MenuLists.size();
							for(int i=0;i<size;i++)
							{
								boolean flag = false;
								Thread.sleep(500);
								if(HBCBasicfeature.isElementPresent(footerMenuListActive))
								{
									Pass("service � la client�le et politique Section in opened state");
								}
								else
								{
									action.moveToElement(footerAccordCustomerPolicy).click().build().perform();
									wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
									Pass("service � la client�le et politique Section in opened state");
								}
								Thread.sleep(500);
								WebElement link = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]"));
								HBCBasicfeature.scrolldown(link, driver);
								String link_text = link.getText();
								Thread.sleep(500);
								for(int j=0;j<linkOptions.length;j++)
								{
									if(link_text.equalsIgnoreCase(linkOptions[j]))
									{
										Pass("service � la client�le et politique section contains the link :"+link_text);
										HBCBasicfeature.scrolldown(link, driver);
										action.moveToElement(link).click().build().perform();
										//wait.until(ExpectedConditions.attributeContains(footerSectionsDom, "style", "visible"));
										Thread.sleep(3000);
										String url = driver.getCurrentUrl();
										if(url.contains(CustomerURL[j]))
										{
											Pass("The expected "+link_text+"  page shown on selecting the "+link_text+" link and the URL is " + driver.getCurrentUrl());
											Pass("The URL Contains the Searched Keyword.");
											do
											{		
												logo.click();
												wait.until(ExpectedConditions.visibilityOf(homepage));
												if(HBCBasicfeature.isElementPresent(homepage))
												{
													flag = true;
												}
												else
												{
													continue;
												}											
											}
											while(flag!=true);	
											break;					
										}
										else
										{
											Fail("The expected page not shown on selecting the "+link_text+" link and the URL is  " + driver.getCurrentUrl());
											Fail("The URL not Contains the Searched Keyword.");
											logo.click();
											wait.until(ExpectedConditions.visibilityOf(homepage));
										}								
									}
									else
									{
										continue;
									}		
								}
							}
							driver.navigate().refresh();
						}
						else
						{
							Fail("service � la client�le et politique section not expanded");
						}
					}
					else
					{
						Fail("service � la client�le et politique section not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-139"+e.getMessage());
					Exception("HUDSONBAY-139 There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("HBC french site not displayed");
			}	
		}
	
	/*HUDSONBAY-140 Verify that r�compenses hbc section should be shown as per the creative in all the pages of footer.*/
	public void menur�compenseshbc() throws Exception
		{		
			ChildCreation("HUDSONBAY-140 Verify that r�compenses hbc section should be shown as per the creative in all the pages of footer.");
			if(labie == true)
			{
				try
				{
					String expected = HBCBasicfeature.getExcelVal("HB115", sheet, 2);				
					wait.until(ExpectedConditions.visibilityOf(footerMenuList));
					if(HBCBasicfeature.isElementPresent(footerMenuList))
					{
						wait.until(ExpectedConditions.visibilityOf(footerHBCRewards));
						if(HBCBasicfeature.isElementPresent(footerHBCRewards))
						{
							String actual = footerHBCRewards.getText();
							Thread.sleep(1000);
							if(actual.equalsIgnoreCase(expected))
							{
								log.add("r�compenses hbc menu title" +actual+ " is displayed ");
								if(HBCBasicfeature.isElementPresent(footerAccordHBCRewards))
								{
									log.add("r�compenses hbc section accordian is displayed");
									Pass("r�compenses hbc section shown as per the creative",log);
								}
								else
								{
									Fail("r�compenses hbc section not displayed as per the creative");
								}
							}
							else
							{
								Fail("r�compenses hbc section title is not displayed");
							}
						}
						else
						{
							Fail("r�compenses hbc section is not displayed");
						}					
					}
					else
					{
						Fail("Footer Menu list is not displayed");
					}				
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("HBC french site not displayed");
			}	
		}
	
	/*HUDSONBAY-141 Verify that r�compenses hbc  section should be expanded on selecting it & collapse on closing it from footer.*/
	public void r�compenseshbcClick()
		{
			ChildCreation("HUDSONBAY-141 Verify that r�compenses hbc  section should be expanded on selecting it & collapse on closing it from footer.");
			if(labie == true)
			{
				try
				{
					Actions action = new Actions(driver);	
					wait.until(ExpectedConditions.visibilityOf(footerHBCRewards));
					if(HBCBasicfeature.isElementPresent(footerHBCRewards))
					{
						action.moveToElement(footerHBCRewards).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
						{
							log.add("on selecting r�compenses hbc  then the section is expanded");
							Thread.sleep(1000);
							action.moveToElement(footerAccordHBCRewards).click().build().perform();
							Thread.sleep(1000);
							if(!HBCBasicfeature.isElementPresent(footerMenuListActive))
							{
								log.add("on selecting r�compenses hbc  then the section is collapsed");
								Pass("r�compenses hbc  section expanded on selecting it & collapse on closing it from footer",log);
							}
							else
							{
								Fail("r�compenses hbc  section not expanded on selecting it & not collapse on closing it from footer");
							}
						}
						else
						{
							Fail("r�compenses hbc  section not expanded");
						}
					}
					else
					{
						Fail("Footer menu r�compenses hbc  section is not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("HBC french site not displayed");
			}			
		}
	
	/*HUDSONBAY-142 Verify that r�compenses hbc section should consists of these links as per the requirement*/
	public void r�compenseshbcSection() throws Exception
		{				
			ChildCreation("HUDSONBAY-142 Verify that r�compenses hbc section should consists of these links as per the requirement");
			if( labie == true)
			{
				try
				{
					Actions action = new Actions(driver);			
					String links = HBCBasicfeature.getExcelVal("HBC117", sheet, 3);	
					String[] linkOptions = links.split("\n");
					wait.until(ExpectedConditions.visibilityOf(footerHBCRewards));
					if(HBCBasicfeature.isElementPresent(footerHBCRewards))
					{
						action.moveToElement(footerHBCRewards).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapper))
						{
							boolean optionfound = true;
							int size = MenuLists.size();
							for(int i=0;i<size;i++)
							{
								String link_text = driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]")).getText();
								Thread.sleep(500);
								for(int j=0;j<linkOptions.length;j++)
								{
									if(link_text.equalsIgnoreCase(linkOptions[j]))
									{
										log.add("r�compenses hbc section contains the link :"+link_text);
										optionfound = true;
										break;
									}
									else
									{
										optionfound = false;
										continue;
									}		
								}
							}
							if(optionfound == true)
							{
								Pass("r�compenses hbc section consists of these links as per the requirement",log);
								action.moveToElement(footerAccordHBCRewards).click().build().perform();
							}
							else
							{
								Fail("r�compenses hbc section section not consists of these links as per the requirement",log);
								action.moveToElement(footerAccordHBCRewards).click().build().perform();
							}
						}
						else
						{
							Fail("r�compenses hbc section not expanded");
						}
					}
					else
					{
						Fail("r�compenses hbc section not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("HBC french site not displayed");
			}	
		}
	
	/*HUDSONBAY-143 Verify that on selecting the links from r�compenses hbc section should takes to its corresponding pages as expected.*/
	public void r�compenseshbcSectionLinksSelection() throws Exception
		{					
			ChildCreation("HUDSONBAY-143 Verify that on selecting the links from r�compenses hbc section should takes to its corresponding pages as expected");
			if(labie == true)
			{
				try
				{
					Actions action = new Actions(driver);
					String[] linkOptions = HBCBasicfeature.getExcelVal("HBC117", sheet, 3).split("\n");
					String[] HBCRewardURL = HBCBasicfeature.getExcelVal("HBC117", sheet, 4).split("\n");
					wait.until(ExpectedConditions.visibilityOf(footerHBCRewards));
					if(HBCBasicfeature.isElementPresent(footerHBCRewards))
					{
						Pass("HBC Rewards section displayed");
						action.moveToElement(footerAccordHBCRewards).click().build().perform();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(footerMenuWrapperDom, "style", "block"));
						if(HBCBasicfeature.isElementPresent(footerMenuWrapperDom))
						{
							int size = MenuLists.size();
							for(int i=0;i<size;i++)
							{
								if(HBCBasicfeature.isElementPresent(footerMenuListActive))
								{
									Pass("r�compenses hbc section in opened state");
								}
								else
								{
									action.moveToElement(footerAccordHBCRewards).click().build().perform();
									wait.until(ExpectedConditions.visibilityOf(footerMenuListActive));
									Pass("HBC Rewards section in opened state");
								}
								WebElement link= driver.findElement(By.xpath("(//*[@class='footer-menu-list active']//*[@class='link-data'])["+(i+1)+"]"));
								String link_text = link.getText();
								Thread.sleep(500);
								for(int j=0;j<linkOptions.length;j++)
								{
									if(link_text.equalsIgnoreCase(linkOptions[j]))
									{
										Pass("r�compenses hbc section contains the link :"+link_text);
										String handle = driver.getWindowHandle();
										HBCBasicfeature.scrolldown(link, driver);
										link.click();
										Thread.sleep(2000);
										boolean linkfound = false;
										for(String newWin: driver.getWindowHandles())
										{
											if(!newWin.equals(handle))
											{
												driver.switchTo().window(newWin);
												Thread.sleep(1000);
												String url = driver.getCurrentUrl();
												Thread.sleep(1000);
												if(url.contains(HBCRewardURL[j]))
												{
													Pass("The expected "+link_text+"  page shown on selecting the "+link_text+" link and the URL is " + driver.getCurrentUrl());
													Pass("The URL Contains the Searched Keyword.");
													linkfound = true;
													break;
												}
												else
												{
													linkfound = false;
													continue;
												}							
											}									
										 }								
										if(linkfound == true)
										{
											driver.close();								
											driver.switchTo().window(handle);
											Pass("The "+link_text+" link was clicked and the tab is closed successfully.");
											Thread.sleep(1000);
											break;
										}
										else
										{
											Fail("The expected page shown on selecting the "+link_text+" link and the URL is  " + driver.getCurrentUrl());
											Fail("The URL not Contains the Searched Keyword.");
											driver.close();
											driver.switchTo().window(handle);
											Thread.sleep(1000);
										}								
									}
									else
									{
										continue;
									}		
								}
							}
							action.moveToElement(footerAccordHBCRewards).click().build().perform();
						}
						else
						{
							Fail("r�compenses hbc section not expanded");
						}
					}
					else
					{
						Fail("r�compenses hbc section not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println("HUDSONBAY-143"+e.getMessage());
					Exception("HUDSONBAY-143 There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("HBC french site not displayed");
			}
		}
	
}

