package hbPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.HBCBasicfeature;
import hbConfig.HBCInterface;
import hbConfig.HBConstants;
import hbPages.Hudson_Bay_PDP;

public class Hudson_Bay_PDP_Obj extends Hudson_Bay_PDP implements HBCInterface
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
		
	public Hudson_Bay_PDP_Obj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}	
	
	//HUDSONBAY-255 To be changed
		@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id_2_0']")
		WebElement pancakemenuShoe;
		
		@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id__1_151']")
		WebElement pancakemenushoeNewArrival;
		
		@FindBy(xpath = "//*[@id='sk_mobCategoryItemCont_id__2_152']")
		WebElement pancakesubmenuShoeWomenNewArrival;
		
		//To be changed
	
	@FindBy(xpath=""+hamburgerr+"")
	WebElement hamburger;
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+logoo+"")
	WebElement logo;
	
	@FindBy(xpath = ""+maskk+"")
	WebElement mask;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchBoxx+"")
	WebElement searchBox;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
		
	@FindBy(xpath=""+plpPagee+"")
	WebElement plpPage;
	
	@FindBy(xpath = ""+pdpPagee+"")
	WebElement PDPPage;
	
	@FindBy(xpath = ""+PDPProdNamee+"")
	WebElement PDPProdName;		

	@FindBy(xpath = ""+pdpPageProductImagee+"")
	WebElement pdpPageProductImage;		
	
	@FindBy(xpath = ""+pdpImageSelectedd+"")
	WebElement pdpImageSelected;		
	
	@FindBy(xpath = ""+pdpPageSwatchess+"")
	WebElement pdpPageSwatchesContainer;
	
	@FindBy(xpath = ""+pdpPageActiveColorr+"")
	WebElement pdpPageActiveColor;	
	
	
	@FindBy(xpath = ""+pdppageSizee+"")
	WebElement pdppageSize;
	
	@FindBy(xpath = ""+	pdppageSizeInfoo+"")
	WebElement 	pdppageSizeInfo;
	
	@FindBy(xpath = ""+	pdppageSizeInfoExpandd+"")
	WebElement 	pdppageSizeInfoExpand;
	
	@FindBy(xpath = ""+	pdppageSizeInfoExpandDOMM+"")
	WebElement 	pdppageSizeInfoExpandDOM;
	
	@FindBy(xpath = ""+	pdppageSizeContainerr+"")
	WebElement 	pdppageSizeContainer;
	
	@FindBy(xpath = ""+	pdppageQuantityContainerr+"")
	WebElement 	pdppageQuantityContainer;
	
	@FindBy(xpath = ""+	pdppageQuantityPluss+"")
	WebElement 	pdppageQuantityPlus;
	
	@FindBy(xpath = ""+	pdppageQuantityMinuss+"")
	WebElement 	pdppageQuantityMinus;
	
	@FindBy(xpath = ""+	pdppageQuantityTextt+"")
	WebElement 	pdppageQuantityText;
	
	@FindBy(xpath = ""+	pdppageDetailss+"")
	WebElement 	pdppageDetails;
	
	@FindBy(xpath = ""+	pdppageDetailsRewardd+"")
	WebElement 	pdppageDetailsReward;	
	
	@FindBy(xpath = ""+	pdppageDetailsExpandd+"")
	WebElement 	pdppageDetailsExpand;
	
	@FindBy(xpath = ""+	pdppageDetailsExpandDOMM+"")
	WebElement 	pdppageDetailsExpandDOM;
	
	@FindBy(xpath = ""+	pdppageAccordsExpandDoMm+"")
	WebElement 	pdppageAccordsExpandDoM;
	
	@FindBy(xpath = ""+	pdppageAccordd+"")
	WebElement 	pdppageAccord;
		
	@FindBy(xpath = ""+	pdppageDetailsExpandClss+"")
	WebElement 	pdppageDetailsExpandCls;
	
	@FindBy(xpath = ""+	pdppageSizeInfoExpandClss+"")
	WebElement 	pdppageSizeInfoExpandCls;
		
	@FindBy(xpath = ""+	pdppagePriceContainerr+"")
	WebElement 	pdppagePriceContainer;
	
	@FindBy(xpath = ""+	pdppagePriceSalee+"")
	WebElement 	pdppagePriceSale;

	@FindBy(xpath = ""+	pdppagePriceRegularr+"")
	WebElement 	pdppagePriceRegular;

	@FindBy(xpath = ""+	pdppageATBb+"")
	WebElement 	pdppageATB;
	
	@FindBy(xpath = ""+	ATBAlertt+"")
	WebElement 	ATBAlert;
	
	@FindBy(xpath = ""+	ATBAlertOkk+"")
	WebElement 	ATBAlertOk;
	
	@FindBy(xpath = ""+	pdppageATBOverlayy+"")
	WebElement 	pdppageATBOverlay;
	
	@FindBy(xpath = ""+	pdpcheckOutBtnn+"")
	WebElement 	pdpcheckOutBtn;	
	
	@FindBy(xpath = ""+	shoppingCartPagee+"")
	WebElement 	shoppingCartPage;	
	
	@FindBy(xpath = ""+	pdppageZoomIconn+"")
	WebElement 	pdppageZoomIcon;
	
	@FindBy(xpath = ""+	pdppageZoomContainerr+"")
	WebElement 	pdppageZoomContainer;
	
	@FindBy(xpath = ""+	pdppageZoomMaskk+"")
	WebElement 	pdppageZoomMask;

	@FindBy(xpath = ""+	bagIconCountt+"")
	WebElement 	bagIconCount;
	
	@FindBy(xpath = ""+pdppageATBOverlayClosee+"")
	WebElement 	pdppageATBOverlayClose;
	
	@FindBy(xpath = ""+pdppageShareIconss+"")
	WebElement 	pdppageShareIcons;
	
	@FindBy(xpath = ""+pdppageShareFBb+"")
	WebElement 	pdppageShareFB;
	
	@FindBy(xpath = ""+pdppageShareTwitterr+"")
	WebElement 	pdppageShareTwitter;
	
	@FindBy(xpath = ""+pdppageShareGpluss+"")
	WebElement 	pdppageShareGplus;
	
	@FindBy(xpath = ""+pdppageSizeSelectedDefaultt+"")
	WebElement 	pdppageSizeSelectedDefault;
	
	@FindBy(xpath = ""+pdppageSwatchColorSelectedd+"")
	WebElement 	pdppageSwatchColorSelected;
	
	@FindBy(xpath = ""+pdppageAccordsExpandd+"")
	WebElement 	pdppageAccordsExpand;
	
	@FindBy(xpath = ""+pdppageNoRatingReviewContnrr+"")
	WebElement 	pdppageNoRatingReviewContnr;
	
	@FindBy(xpath = ""+pdppageRatingReviewContnrr+"")
	WebElement 	pdppageRatingReviewContnr;
	
	@FindBy(xpath = ""+pdppageRatingCountt+"")
	WebElement 	pdppageRatingCount;
	
	@FindBy(xpath = ""+pdppageReviewCountt+"")
	WebElement 	pdppageReviewCount;
	
	@FindBy(xpath = ""+pdpPageSwatchSelectedColorr+"")
	WebElement 	pdpPageSwatchSelectedColor;
	
	@FindBy(xpath = ""+pdppageIframeVideoo+"")
	WebElement 	pdppageIframeVideo;	
	
	@FindBy(xpath = ""+pdppageIframeRelatedVideoo+"")
	WebElement 	pdppageIframeRelatedVideo;	
			
	@FindBy(xpath = ""+pdppageVideoo+"")
	WebElement 	pdppageVideo;
	
	@FindBy(xpath = ""+pdppageVideoContainerr+"")
	WebElement 	pdppageVideoContainer;	
		
	@FindBy(xpath = ""+videoPlayPausee+"")
	WebElement 	videoPlayPause;
	
	@FindBy(xpath = ""+videoSocialSharee+"")
	WebElement 	videoSocialShare;
	
	@FindBy(xpath = ""+videoEmailSharee+"")
	WebElement 	videoEmailShare;
	
	@FindBy(xpath = ""+videoEmbedSharee+"")
	WebElement 	videoEmbedShare;
	
	@FindBy(xpath = ""+videoLinkSharee+"")
	WebElement 	videoLinkShare;
	
	@FindBy(xpath = ""+videoTwitterSharee+"")
	WebElement 	videoTwitterShare;
	
	@FindBy(xpath = ""+videoFacebookSharee+"")
	WebElement 	videoFacebookShare;	
	
	@FindBy(xpath = ""+videoFullScreenn+"")
	WebElement 	videoFullScreen;
	
	@FindBy(xpath = ""+relatedVideoss+"")
	WebElement 	relatedVideos;	
	
	@FindBy(xpath = ""+relatedVideoplaypausee+"")
	WebElement 	relatedVideoplaypause;
	
	@FindBy(xpath = ""+callToOrderr+"")
	WebElement 	callToOrder;	
	
	@FindBy(xpath = ""+collectionProductContianerr+"")
	WebElement 	collectionProductContianer;
	
	@FindBy(xpath = ""+pdpWishListIconn+"")
	WebElement 	pdpWishListIcon;	
	
	@FindBy(how = How.XPATH,using  = ""+collectionProductListt+"")
	List<WebElement> collectionProductList;	
	
	@FindBy(how = How.XPATH,using  = ""+relatedVideoss+"")
	List<WebElement> relatedVideosList;	
	
	@FindBy(how = How.XPATH,using  = ""+pdppageDescriptionAccordss+"")
	List<WebElement> pdppageDescriptionAccords;	
	
	@FindBy(how = How.XPATH,using  = ""+pdppageSwatchColorSelectedd+"")
	List<WebElement> pdppageSwatchListColorSelected;	
	
	@FindBy(how = How.XPATH,using  = ""+pdppageSizeSelectedd+"")
	List<WebElement> pdppageSizeListSelected;
	
	@FindBy(how = How.XPATH,using  = ""+pdppageSizeContainerListt+"")
	List<WebElement> pdppageSizeContainerList;
	
	@FindBy(how = How.XPATH,using  = ""+pdpPageSwatchesListt+"")
	List<WebElement> pdpPageSwatchesList;
	
	@FindBy(how = How.XPATH,using  = ""+shoppingBagPageProdTitlee+"")
	List<WebElement> shoppingBagPageProdTitle;
	
	
	//Footer:
		@FindBy(xpath = ""+signupEmailss+"")
		WebElement signupEmails;
	
		@FindBy(xpath = ""+footerContainerr+"")
		WebElement footerContainer;	
		
		@FindBy(xpath = ""+footerTopp+"")
		WebElement footerTop;	
		
		@FindBy(xpath = ""+footerTopTitlee+"")
		WebElement footerTopTitle;
		
		@FindBy(xpath = ""+footerSubTopp+"")
		WebElement footerSubTop;
		
		@FindBy(xpath = ""+footerSubTopCalll+"")
		WebElement footerSubTopCall;
		
		@FindBy(xpath = ""+footerSubTopEmaill+"")
		WebElement footerSubTopEmail;
		
		@FindBy(xpath = ""+footerMenuListt+"")
		WebElement footerMenuList;
		
		@FindBy(xpath = ""+footerBottomm+"")
		WebElement footerBottom;
		
		@FindBy(xpath = ""+footerSocialIconn+"")
		WebElement footerSocialIcon;
		
		@FindBy(xpath = ""+footerBottomCopyrightt+"")
		WebElement footerBottomCopyright;
		
		@FindBy(xpath = ""+loadingbarr+"")
		WebElement loadingbar;
		
		
	@FindBy(how = How.XPATH,using = ""+plpItemContListt+"")
	List<WebElement> plpItemContList;
	
	String PLPtitle = "";	
	String pdpUrl = "";
	
	boolean swatches = false;
	boolean sizeGuide = false;
	boolean sizeContainer = false;
	boolean regsale = false;

		
	public void subcatToPdp()
		{
			ChildCreation("PDP Page");	
			String Key = "shoe";
			WebElement product = null ;
			try
			{		
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(Key);
						searchBox.sendKeys(Keys.ENTER);
					}												
				}			
				boolean pgLoad = false;
				do
				{				
					try
					{
						Thread.sleep(1000);
						if(HBCBasicfeature.isElementPresent(plpPage))		
						{
							pgLoad=true;	
							break;		
						}
					}
					catch (Exception e)
					{
						pgLoad=false;
						driver.navigate().refresh();	
						continue;
					}
				}							
				while(!pgLoad==true); 							
				if(HBCBasicfeature.isElementPresent(plpPage))
				{
					Pass("Respective PLP page displayed");
					if(HBCBasicfeature.isListElementPresent(plpItemContList))
					{
						int i =0;
						boolean swat = false;								
						do
						{				
							try
							{
								Thread.sleep(500);
								int size = plpItemContList.size();
								Random r  = new Random();
								i = r.nextInt(size);
								product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]"));
								wait.until(ExpectedConditions.visibilityOf(product));
								HBCBasicfeature.scrolldown(product, driver);	
								//Selcting product with color swatches
								List<WebElement> swatchProd = driver.findElements(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]//*[@class='sk_prdswatch']"));
								int ssize = swatchProd.size();
								if(ssize>1)		
								{
									//Selcting product with rating 
									WebElement rating = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+(i+1)+"]//*[@class='skMob_productDetails']//*[@class='skMob_rating_reviews ']"));
									if(HBCBasicfeature.isElementPresent(rating))
									{
										swat=true;	
										break;		
									}
								}
							}
							catch (Exception e)
							{
								swat=false;
								continue;
							}
						}
						while(!swat==true);									
						if(HBCBasicfeature.isElementPresent(product))
						{
							String product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+(i+1)+"]")).getText();
							String product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+(i+1)+"]")).getText();
							PLPtitle = product_BrandTitle +" "+ product_Title;
							log.add("The selected product title is: "+PLPtitle);
							Thread.sleep(1000);
							product.click();
							wait.until(ExpectedConditions.visibilityOf(PDPPage));
							if(HBCBasicfeature.isElementPresent(PDPPage))
							{
								Thread.sleep(1000);
								String PDPtitle = PDPProdName.getText();
								log.add("The PDP page title is: "+PDPtitle);
								if(PDPtitle.equalsIgnoreCase(PLPtitle))
								{
									Pass("Selecting the product in the PLP page,it navigated to the corresponding PDP page",log);
								}
								else
								{
									Fail("Selecting the product in the PLP page,not navigated to the corresponding PDP page",log);
								}	
							}
							else
							{
								Fail("PDP Page not displayed");
							}
						}
						else
						{
							Fail("Selected product not displayed");
						}					
					}
					else
					{
						Fail("PLP products are not dispalyed");
					}
				}
				else
				{
					Fail("Selecting the sub-categories in the pancake menu,the selected category product list page not displayed");
				}							
						
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}				
		}
		
	/*HUDSONBAY-471 Verify that header should be displayed in PDP as per the creative.*/
	public void pdpHeader()
		{
			ChildCreation("HUDSONBAY-471 Verify that header should be displayed in PDP as per the creative.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				if(HBCBasicfeature.isElementPresent(header))
				{
					wait.until(ExpectedConditions.visibilityOf(hamburger));
					if(HBCBasicfeature.isElementPresent(hamburger))
					{
						Pass("Hamburger menu displayed in the header");
					}
					else
					{
						Fail("Hamburger menu not displayed in the header");
					}
					wait.until(ExpectedConditions.visibilityOf(logo));
					if(HBCBasicfeature.isElementPresent(logo))
					{
						Pass("Logo displayed in the header");
					}
					else
					{
						Fail("Logo not displayed in the header");
					}
					wait.until(ExpectedConditions.visibilityOf(bagIcon));
					if(HBCBasicfeature.isElementPresent(bagIcon))
					{
						Pass("Shoppin Bag icon displayed in the header");
					}
					else
					{
						Fail("Shoppin Bag icon not displayed in the header");
					}
				}
				else
				{
					Fail("Header not displayed in the home page");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-470 Verify that user should able to scroll the PDP page vertically*/
	public void pdpVertical_Scroll()
		{
			ChildCreation("HUDSONBAY-470 Verify that user should able to scroll the PDP page vertically");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				if(HBCBasicfeature.isElementPresent(header))
				{
					HBCBasicfeature.scrolldown(footerContainer, driver);
					wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{
						Pass("User can able to scroll the PDP page vertically");
					}
					else
					{
						Fail("User not able to scroll the PDP page vertically ");
					}					
				}
				else
				{
					Fail("Header not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-472 Verify that Footer should be displayed in PDP as per the creative.*/
	public void pdpFooter()
		{
			ChildCreation("HUDSONBAY-472 Verify that Footer should be displayed in PDP as per the creative.");
			try
			{
				/*wait.until(ExpectedConditions.visibilityOf(signupEmails));
				if(HBCBasicfeature.isElementPresent(signupEmails))
				{
					Pass("Signup for daily Emails option is displayed");	
				}
				else
				{
					Fail("Signup for daily Emails option is not displayed");						
				}			*/
				wait.until(ExpectedConditions.visibilityOf(footerTop));
				if(HBCBasicfeature.isElementPresent(footerTop))
				{
					Pass("Footer top panel is displayed");	
					wait.until(ExpectedConditions.visibilityOf(footerTopTitle));
					if(HBCBasicfeature.isElementPresent(footerTopTitle))
					{
						Pass("Footer top panel title is displayed");	
					}
					else
					{
						Fail("Footer top panel title is not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerSubTop));
					if(HBCBasicfeature.isElementPresent(footerSubTop))
					{
						Pass("Footer sub top panel is displayed");	
						wait.until(ExpectedConditions.visibilityOf(footerSubTopCall));
						if(HBCBasicfeature.isElementPresent(footerSubTopCall))
						{
							Pass("Footer call option is displayed");		
						}
						else
						{
							Fail("Footer Call option is not displayed");						
						}
						wait.until(ExpectedConditions.visibilityOf(footerSubTopEmail));
						if(HBCBasicfeature.isElementPresent(footerSubTopEmail))
						{
							Pass("Footer Email option is displayed");	
						}
						else
						{
							Fail("Footer email option is not displayed");						
						}
					}
					else
					{
						Fail("Footer sub top panel is not displayed");						
					}
				}
				else
				{
					Fail("Footer top panel is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerMenuList));
				if(HBCBasicfeature.isElementPresent(footerMenuList))
				{
					Pass("Footer Menu List option is displayed");
				}
				else
				{
					Fail("Footer menu list is not displayed");						
				}
				wait.until(ExpectedConditions.visibilityOf(footerBottom));
				if(HBCBasicfeature.isElementPresent(footerBottom))
				{
					Pass("Footer bottom panel is displayed");
					wait.until(ExpectedConditions.visibilityOf(footerSocialIcon));
					if(HBCBasicfeature.isElementPresent(footerSocialIcon))
					{
						Pass("Footer social icon options are displayed");
					}
					else
					{
						Fail("Footer social icon options are not displayed");						
					}
					wait.until(ExpectedConditions.visibilityOf(footerBottomCopyright));
					if(HBCBasicfeature.isElementPresent(footerBottomCopyright))
					{
						log.add("Footer displayed as per the creative and fit to the screen");
						Pass("Footer copyright section is displayed",log);										
					}
					else
					{
						log.add("Footer is not displayed as per the creative and fit to the screen");
						Fail("Footer copyright section is not displayed",log);	
					}
				}
				else
				{
					Fail("Footer bottom panel is not displayed");						
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-473 Verify that product title should be displayed as per the classic site*/
	public void pdpProduct_Title()
		{
			ChildCreation("HUDSONBAY-473 Verify that product title should be displayed as per the classic site");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(PDPProdName));
				HBCBasicfeature.scrollup(PDPProdName, driver);
				if(HBCBasicfeature.isElementPresent(PDPProdName))
				{
					String PDPtitle = PDPProdName.getText();
					log.add("The PDP page title is: "+PDPtitle);
					log.add("The Title from PLP page is: "+PLPtitle);
					if(PDPtitle.equalsIgnoreCase(PLPtitle))
					{
						Pass("Product title displayed as per the classic site",log);
					}
					else
					{
						Fail("Product title displayed as per the classic site",log);
					}					
				}
				else
				{
					Fail("Product title not displayed in the PDP page");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-528 Verify that "Favorite" icon should be displayed in the PDP page*/
	/*HUDSONBAY-529 Verify that 'Favorite' icon should be displayed in the top right corner of the PDP page*/
	public void pdpProduct_FavIcon(String tc)
		{
			if(tc == "HBC528")
			{
				ChildCreation("HUDSONBAY-528 Verify that 'Favorite' icon should be displayed in the PDP page");
			}
			else
			{
				ChildCreation("HUDSONBAY-528 Verify that 'Favorite' icon should be displayed in the top right corner of the PDP page");
			}
			try
			{
				if(HBCBasicfeature.isElementPresent(pdpWishListIcon))
				{
					Pass("'Favorite' icon  displayed in the PDP page");
				}
				else
				{
					Fail("'Favorite' icon not displayed in the PDP page");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-474 Verify that Product image should be displayed above the product title*/
	public void pdpProduct_Image()
		{
			ChildCreation("HUDSONBAY-474 Verify that Product image should be displayed above the product title");
			int status;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdpPageProductImage));
				HBCBasicfeature.scrollup(pdpPageProductImage, driver);
				if(HBCBasicfeature.isElementPresent(pdpPageProductImage))
				{
					Pass("The image container is present for the selected product.");
					WebElement imageContainer = driver.findElement(By.xpath("//*[@id='id_pdpMasterImageCont']"));
					List<WebElement> img = imageContainer.findElements(By.tagName("img"));
					log.add("Total Images present for the Product is: "+img.size());
					for(WebElement image : img)
					{
						status = HBCBasicfeature.imageBroken(image,log);
						if(status==200)
						{
							Pass("The displayed image for the product is Valid Image and it is not broken.", log);
						}
						else
						{
							Fail("The displayed image for the product is Invaalid Image and it is broken.", log);
						}
					}
				}
				else
				{
					Fail("The image container is not found for the selected product.");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-523 Verify that by default the first image should be displayed.*/
	public void pdpProduct_ImageDefault()
		{
			ChildCreation("HUDSONBAY-523 Verify that by default the first image should be displayed.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdpImageSelected));
				if(HBCBasicfeature.isElementPresent(pdpImageSelected))
				{
					String position = pdpImageSelected.getAttribute("index");
					int pos = Integer.parseInt(position);
					log.add("The selected image position is: "+position);
					if(pos==1)
					{
						Pass("By default the first image displayed",log);
					}
					else
					{
						Fail("By default the first image not displayed",log);
					}					
				}
				else
				{
					Fail("Image not selected by defalut");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-524 Verify that carousel swiping should not occurs, only alternate image selection should works properly. */
	public void pdpProduct_ImageAlternate()
		{
			ChildCreation("HUDSONBAY-524 Verify that Carousel swiping should not occurs, only alternate image selection should works properly. ");
			try
			{
				String position = pdpImageSelected.getAttribute("index");
				int pos = Integer.parseInt(position);
				log.add("Default selected Image position is: "+pos);				
				List<WebElement> altImage = driver.findElements(By.xpath("//*[@class='swiper-slide'][contains(@style,'block')]"));
				int size = altImage.size();
				if(size>0)
				{	
					for(int i=1;i<size;i++)
					{						
						WebElement altImage_clk = driver.findElement(By.xpath("//*[@class='swiper-slide'][contains(@style,'block')]["+i+"]"));
						altImage_clk.click();
						String displayPic = driver.findElement(By.xpath("//*[@id='id_pdpthumbImageCont']//*[@class='swiper-slide swiper-no-swiping swiper-slide-active']//img")).getAttribute("index");
						int dpic = Integer.parseInt(displayPic);
						log.add("After selected alternate image, selected image position is: "+dpic);
						String currentPos = pdpImageSelected.getAttribute("index");
						int Cpos = Integer.parseInt(currentPos);
						log.add("Alternate image position is: "+Cpos);
						if(dpic==Cpos)
						{
							Pass("Alternate image selection works properly",log);
						}
						else
						{
							Fail("Alternate image selection not works properly",log);
						}						
					}
				}
				else
				{
					Skip("Alternate images are not displayd for the selected product");
				}		
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
			
	/*HUDSONBAY-569 Verify that on selecting product with no rating & reviews, then its product page will be shown.*/
	public void pdp_RatingReview()
		{
			ChildCreation("HUDSONBAY-569 Verify that on selecting product with no rating & reviews, then its product page will be shown.");
			try
			{
				if(HBCBasicfeature.isElementPresent(pdppageNoRatingReviewContnr))
				{
					WebElement writeReview = driver.findElement(By.xpath("//*[@class='sk_writeReview']"));
					if(HBCBasicfeature.isElementPresent(writeReview))						
					{
						log.add("Write a review link displayed");
						Pass("no rating & reviews, then its product page will be shown");
					}
					else
					{
						Fail("no rating & reviews, then its product page will be shown");
					}					
				}
				else
				{
					if(HBCBasicfeature.isElementPresent(pdppageRatingReviewContnr))
					{
						log.add("Rating & review container displayed");		
						if(HBCBasicfeature.isElementPresent(pdppageRatingCount))
						{
							log.add("Rating count displayed");
							String rat = pdppageRatingCount.getAttribute("aria-label").substring(0,2).trim();
							int rating = Integer.parseInt(rat);
							log.add("Rating is: "+rating);
							if(rating>0)
							{
								Pass("Rating is displayed for the product",log);
							}
							else
							{
								Fail("Rating is not displayed "+rating);
							}
						}
						else
						{
							Skip("Rating Count not displayed for this product");
						}
						if(HBCBasicfeature.isElementPresent(pdppageReviewCount))
						{
							log.add("Review count displayed");
							String review = pdppageReviewCount.getText();
							log.add("Review  is: "+review);
							if(review.contains("(No reviews)"))
							{
								Pass("If no reviews PDP page is displayed",log);
							}
							else
							{
								Pass("Product has reviews and the review is: "+review);
							}
						}
						else
						{
							Skip("Review Count not displayed for this product");
						}					
					}
					else
					{
						Fail("rating and review container not displayed");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-857 Verify that color/size arrangements should be shown as per the creative and it should match with the classic site.*/
	public void pdpColor_Creative() throws Exception
		{
			ChildCreation("HUDSONBAY-857 Verify that color/size arrangements should be shown as per the creative and it should match with the classic site");
			ArrayList<String> streamResp = new ArrayList<>();
			ArrayList<String> siteResp = new ArrayList<>();
			ArrayList<Integer> result = new ArrayList<Integer>();
			Random r = new Random();
			String Key = HBCBasicfeature.getExcelVal("HBC857", sheet, 1);
			String id = "";
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(Key);
						searchBox.sendKeys(Keys.ENTER);
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							int plpsize = plpItemContList.size();
							int i =0;
							if(plpsize>=1)
							{													
								/*String plpStream = "http://hbcstage.skavaone.com/skavastream/core/v5/thebay/search?campaignId=1&domain=www.thebay.com&storeid=10701&catalogid=10652&language=-24&offset=0&search="+Key+"";
								URL plpcurl = new URL(plpStream);
								conn1 = (HttpURLConnection) plpcurl.openConnection();
								if(conn1.getResponseCode()==200)
								{
									String classicPLPResponse = IOUtils.toString(new URL(plpStream));*/
									String Stream = HBConstants.plpStream;
									String plpStream = Stream.replace("searchKey", Key);
									driver.navigate().to(plpStream);
									String response = driver.findElement(By.xpath("/html/body/pre")).getText();
									Thread.sleep(1000);
									driver.navigate().back();								
									JSONObject jObj1 = new JSONObject(response);
									JSONObject classicResobj = jObj1.getJSONObject("children");
									JSONArray classicResArray = classicResobj.getJSONArray("products");
									int plsize = classicResArray.length();
									i = r.nextInt(plsize);
									if(i==0)
									{
										i=i+1;
									}									
									pdpUrl = classicResArray.getJSONObject(i).getString("link");
									System.out.println(pdpUrl); 	
									id = classicResArray.getJSONObject(i).getString("identifier");
									System.out.println(id); 	
								/*}
								else
								{
									Fail("Not getting response from stream");
								}*/
								String pdpStream = HBConstants.pdpStream.replace("pdpUrl", pdpUrl);
								/*URL curl = new URL(pdpStream);
								conn = (HttpURLConnection) curl.openConnection();
								if(conn.getResponseCode()==200)
								{
									String classicResponse = IOUtils.toString(new URL(pdpStream));*/
								
										driver.navigate().to(pdpStream);
										String response1 = driver.findElement(By.xpath("/html/body/pre")).getText();
										Thread.sleep(1000);
										driver.navigate().back();		
										
									  JSONObject jObj = new JSONObject(response1);
									  JSONObject classicResultsObj = jObj.getJSONObject("properties").getJSONObject("skuprops");
									  JSONArray classicResultArray = classicResultsObj.getJSONArray("color");
									 // System.out.println(classicResultArray.length());
									  for(int j=0; j<classicResultArray.length();j++)
									  {
										  String prtname = classicResultArray.getJSONObject(j).getString("name").toLowerCase();									  
										  streamResp.add(prtname);
									  }
								/*}
								else
								{
									Fail("There is something wrong with the keyword sent to the URL. " + conn.getResponseMessage());
									System.out.println("HTTP Response Failure." + conn.getResponseMessage());
								}*/
								WebElement product_Clk = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])//*[@identifier='"+id+"']"));		
								HBCBasicfeature.scrolldown(product_Clk, driver);
								Thread.sleep(500);
								product_Clk.click();
								wait.until(ExpectedConditions.visibilityOf(PDPPage));
								wait.until(ExpectedConditions.visibilityOf(pdpPageSwatchesContainer));
								HBCBasicfeature.scrollup(pdpPageSwatchesContainer, driver);
								if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
								{
									int size = pdpPageSwatchesList.size();
									for(int k=1;k<=size;k++)
									{
										String colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)["+k+"]")).getAttribute("swatchcolor").toLowerCase();	
										siteResp.add(colorSwatch);
									}									
								}
								else
								{
									Fail("Size container not displayed",log);
								}
								
								for (String temp : streamResp)
								{
									result.add(siteResp.contains(temp) ? 1 : 0);
									System.out.println(result);
									log.add("Compared result is: "+result);
								}
								
								if(!result.contains(0))
							  	{
							  		log.add("The colors available for the product (Response from classicSite): " + streamResp.toString());
							  		log.add("The colors available for the product (Response from MobileSite): " + siteResp.toString());
							  		Pass("All the colors that are available for the product displayed as per the classic site",log);
							  	}
							  	else
							  	{
							  		log.add("The colors available for the product (Response from classicSite): " + streamResp.toString());
							  		log.add("The colors available for the product (Response from MobileSite): " + siteResp.toString());
							  		Fail("All the colors that are not available for the product displayed as per the classic site.",log);
							  	}								
							}
							else
							{
								Fail("No products displayed for this keyword");
							}								
						}
						else
						{
							Fail("PLP page not displayed");
						}						
					}
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
				
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-492 Verify that color swatche's Tile should be shown, when the respective product has less than 15 colors*/
	public void pdpColorSwatches_Tile()
		{
			ChildCreation("HUDSONBAY-492 Verify that color swatches Tile should be shown, when the respective product has less than 15 colors");
			try
			{
				if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
				{
					int size = pdpPageSwatchesList.size();
					log.add("Total swatches displayed is: "+size);
					if(size<=15)
					{
						Pass("Color swatches Tile shown, when the respective product has less than 15 colors");
						swatches = true;
					}
					else
					{
						Fail("Color swatches Tile not shown, when the respective product has less than 15 colors");
					}
				}
				else
				{
					Fail("Color swatches Tile not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-491 Verify that color Swatche's should be displayed below the product title*/
	public void pdpColorSwatches()
		{
			ChildCreation("HUDSONBAY-491 Verify that color swatches should be displayed below the product title");
			if(swatches==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
					{
						int size = pdpPageSwatchesList.size();
						for(int i=1;i<=size;i++)
						{
							String colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)["+i+"]")).getAttribute("swatchcolor");
							log.add("The color of swatch "+i+" is: "+colorSwatch);
						}
						Pass("Color swatches displayed below the product title",log);
					}
					else
					{
						Fail("Color swatches Tile not displayed");
					}				
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}
			}
			else
			{
				Skip("Colour swatches not displayed for this product");
			}
		}
	
	/*HUDSONBAY-859 Verify that selected color/size string should be shown as color:XXXXX, size:XXXXXX */
	public void pdpColorSize()
		{
			ChildCreation("HUDSONBAY-859 Verify that selected color/size string should be shown as color:XXXXX, size:XXXXXX ");
			try
			{
				if(HBCBasicfeature.isElementPresent(pdppageSize))
				{
					
					String txt = pdppageSize.getText().substring(0, 6);
					if(txt.equalsIgnoreCase("SIZE :"))
					{
						log.add("Size text displayed"+txt);
						String size = pdppageSize.getText().substring(7,8);
						if(size.matches(".*\\d+.*"))
						{
							Pass("Selected Size string displayed as "+txt.concat(size),log);
						}
						else
						{
							Fail("Selected Size string not displayed as size:XXXXXX "+txt.concat(size),log);
						}
					}
					else
					{
						Fail("Size text not matched");
					}					
				}
				else
				{
					Fail("Size option not availble");
				}
				
				if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
				{
					
					String txt = pdpPageActiveColor.getText().substring(0, 15);
					if(txt.equalsIgnoreCase("SELECT COLOUR :"))
					{
						log.add("Selected colour text displayed"+txt);
						String displayedColor = pdpPageSwatchSelectedColor.getText();		
						Pass("Selected color string displayed as "+txt.concat(displayedColor),log);
					}
					else
					{
						Fail("Selected color text not matched");
					}					
				}
				else
				{
					Fail("Color option not availble");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-495 Verify that on changing the color the image should be updated as per the color is selected.*/
	public void pdpChangeColor_Swatches()
		{
			ChildCreation("HUDSONBAY-495 Verify that on changing the color the image should be updated as per the color is selected.");
			if(swatches==true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
					{
						int size = pdpPageSwatchesList.size();
						String activeColor = pdpPageProductImage.getAttribute("imgclrname");
						log.add("The current color of product is: "+activeColor);
						String activeUrl = pdpPageProductImage.getAttribute("src");
						log.add("The current Url of product is: "+activeUrl);
						for(int i=1;i<size;i++)
						{
							WebElement colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)["+(i+1)+"]"));
							colorSwatch.click();
							Thread.sleep(500);
							String CurrentColor = pdpPageProductImage.getAttribute("imgclrname");
							log.add("After changed The color of product is: "+CurrentColor);
							String CurrentUrl = pdpPageProductImage.getAttribute("src");
							log.add("After changed The color of product is: "+CurrentUrl);
							if(activeColor.equalsIgnoreCase(CurrentColor))
							{
								Fail("Changing the color, respective color not changed",log);
							}
							else
							{
								Pass("Respective color changed",log);
								if(activeUrl.equalsIgnoreCase(CurrentUrl))
								{
									Fail("On changing the color, the image not updated",log);
								}
								else
								{
									Pass("On changing the color, the image gets updated",log);
								}
							}
						}
					}
					else
					{
						Fail("PDP swatches container not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("Colour swatches not displayed for this product");
			}
		}
	
	/*HUDSONBAY-498 Verify that "Size Guide" text link should be displayed below the color swatches for some of the products*/
	public void pdpSize_Guide() throws Exception
		{
			ChildCreation("HUDSONBAY-498 Verify that 'Size Guide' text link should be displayed below the color swatches for some of the products");
			String expcText = HBCBasicfeature.getExcelVal("HB498", sheet, 1);
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageSize));
				if(HBCBasicfeature.isElementPresent(pdppageSizeInfo))
				{
					String actualTxt = pdppageSizeInfo.getText();
					if(actualTxt.equalsIgnoreCase(expcText))
					{
						sizeGuide = true;
						Pass("'Size Guide' text link displayed below the color swatches for some of the products");
					}
					else
					{
						Fail("'Size Guide' text link not displayed");
					}
				}
				else
				{
					Skip("'Size Guide' text link not present for this product");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-500 Verify that Size Details page/overlay should be shown on tapping the "Size Guide" text link*/
	public void pdpSize_GuidePage()
		{
			ChildCreation("HUDSONBAY-500 Verify that Size Details page/overlay should be shown on tapping the 'Size Guide' text link");
			if(sizeGuide == true)
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageSizeInfo))
					{
						HBCBasicfeature.scrolldown(pdppageSizeInfo, driver);
						pdppageSizeInfo.click();
						Thread.sleep(500);
						wait.until(ExpectedConditions.visibilityOf(pdppageSizeInfoExpand));
						if(HBCBasicfeature.isElementPresent(pdppageSizeInfoExpand))
						{					
							wait.until(ExpectedConditions.visibilityOf(pdppageSizeInfoExpandDOM));
							if(HBCBasicfeature.isElementPresent(pdppageSizeInfoExpandDOM))
							{
								Pass("Size Details page/overlay displayed on tapping the 'Size Guide' text link");
								pdppageSizeInfoExpandCls.click();
							}
							else
							{
								Fail("Size Details page/overlay not displayed on tapping the 'Size Guide' text link");
							}
						}
						else
						{
							Fail("PDP Sizing info not expaned");
						}
					}
					else
					{
						Fail("Size Guide not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("Size Guide not displayed for this product");
			}
		}
	
	/*HUDSONBAY-503 Verify that size option should be displayed below the color swatches.*/
	public void pdpSize_SelectSize()
		{
			ChildCreation("HUDSONBAY-503 Verify that size option should be displayed below the color swatches.");
				try
				{
					wait.until(ExpectedConditions.visibilityOf(pdppageSize));
					HBCBasicfeature.scrollup(pdppageSize, driver);
					if(HBCBasicfeature.isElementPresent(pdppageSizeContainer))
					{
						sizeContainer = true;
						int size = pdppageSizeContainerList.size();
						for(int i=1;i<=size;i++)
						{
							String ssize = driver.findElement(By.xpath("//*[contains(@class,'pdpSizeSelect pdpSelectEle')]["+i+"]")).getText();
							log.add("The displayed size is: "+ssize);
						}
						Pass("size option displayed below the color swatches",log);
					}
					else
					{
						Fail("Size container not displayed",log);
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}			
		}
	
	/*HUDSONBAY-504 Verify that by default the " first size" option should be displayed & selected in the "Size" option*/
	public void pdpSize_SelectedDefault()
		{
			ChildCreation("HUDSONBAY-504 Verify that by default the ' first size ' option should be displayed & selected in the 'Size' option");
			if(sizeContainer == true)
			{
				try
				{				
					if(HBCBasicfeature.isElementPresent(pdppageSizeContainer))
					{
						Thread.sleep(1000);
						String selectedSize = pdppageSizeSelectedDefault.getText();
						log.add("The actual seleceted value is: "+selectedSize);
						Thread.sleep(500);
						String firstSize = driver.findElement(By.xpath("//*[contains(@class,'pdpSizeSelect pdpSelectEle')][1]")).getText();
						log.add("The First size value is: "+firstSize);
						if(firstSize.equalsIgnoreCase(selectedSize))
						{
							Pass("' First size ' option displayed & selected in the 'Size' option",log);
						}
						else
						{
							Fail("' First size ' option not displayed & selected in the 'Size' option",log);
						}					
					}
					else
					{
						Fail("Size container not displayed",log);
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}	
			}
			else
			{
				Skip("Size Container not displayed for this product");
			}
		}
		
	/*HUDSONBAY-505 Verify that all the sizes that are available for the product should be displayed as per the classic site.*/
	public void pdpSize_Creative() throws Exception
		{
			ChildCreation("HUDSONBAY-505 Verify that all the sizes that are available for the product should be displayed as per the classic site");
			/*HttpURLConnection conn = null;
			HttpURLConnection conn1 = null;
*/
			ArrayList<String> streamResp = new ArrayList<>();
			ArrayList<String> siteResp = new ArrayList<>();
			ArrayList<Integer> result = new ArrayList<Integer>();
			Random r = new Random();
			String Key = HBCBasicfeature.getExcelVal("HBC505", sheet, 1);					
			String id = "";
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(Key);
						searchBox.sendKeys(Keys.ENTER);
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{
							int plpsize = plpItemContList.size();
							int i =0;
							if(plpsize>=1)
							{													
								String plpStream = HBConstants.plpStream.replace("searchKey", Key);
								/*URL plpcurl = new URL(plpStream);
								conn1 = (HttpURLConnection) plpcurl.openConnection();
								if(conn1.getResponseCode()==200)
								{
									String classicPLPResponse = IOUtils.toString(new URL(plpStream));*/
								
									driver.navigate().to(plpStream);
									String classicPLPResponse = driver.findElement(By.xpath("/html/body/pre")).getText();
									Thread.sleep(1000);
									driver.navigate().back();
									
									JSONObject jObj1 = new JSONObject(classicPLPResponse);
									JSONObject classicResobj = jObj1.getJSONObject("children");
									JSONArray classicResArray = classicResobj.getJSONArray("products");
									int plsize = classicResArray.length();
									i = r.nextInt(plsize);
									if(i==0)
									{
										i=i+1;
									}									
									pdpUrl = classicResArray.getJSONObject(i).getString("link");
									System.out.println(pdpUrl); 	
									id = classicResArray.getJSONObject(i).getString("identifier");
									System.out.println(id); 	
								/*}
								else
								{
									Fail("Not getting response from stream");
								}*/
								String pdpStream = HBConstants.pdpStream.replace("pdpUrl", pdpUrl);
								/*URL curl = new URL(pdpStream);
								conn = (HttpURLConnection) curl.openConnection();
								if(conn.getResponseCode()==200)
								{
									String classicResponse = IOUtils.toString(new URL(pdpStream));*/
								
										driver.navigate().to(pdpStream);
										String classicResponse = driver.findElement(By.xpath("/html/body/pre")).getText();
										Thread.sleep(1000);
										driver.navigate().back();
											  
									  JSONObject jObj = new JSONObject(classicResponse);
									  JSONObject classicResultsObj = jObj.getJSONObject("properties").getJSONObject("skuprops");
									  JSONArray classicResultArray = classicResultsObj.getJSONArray("size1");
									 // System.out.println(classicResultArray.length());
									  for(int j=0; j<classicResultArray.length();j++)
									  {
										  String prtname = classicResultArray.getJSONObject(j).getString("name");										  
										  streamResp.add(prtname);
									  }
								/*}
								else
								{
									Fail("There is something wrong with the keyword sent to the URL. " + conn.getResponseMessage());
									System.out.println("HTTP Response Failure." + conn.getResponseMessage());
								}*/
								WebElement product_Clk = driver.findElement(By.xpath("(//*[contains(@class,'skMob_productListItemOuterCont')]//*[@class='skMob_productImgDiv'])//*[@identifier='"+id+"']"));		
								HBCBasicfeature.scrolldown(product_Clk, driver);
								Thread.sleep(500);
								product_Clk.click();
								wait.until(ExpectedConditions.visibilityOf(PDPPage));
								wait.until(ExpectedConditions.visibilityOf(pdppageSize));
								HBCBasicfeature.scrollup(pdppageSize, driver);
								if(HBCBasicfeature.isElementPresent(pdppageSizeContainer))
								{
									int size = pdppageSizeContainerList.size();
									for(int k=1;k<=size;k++)
									{
										String sizeName = driver.findElement(By.xpath("//*[contains(@class,'pdpSizeSelect pdpSelectEle')]["+k+"]")).getText();
										siteResp.add(sizeName);
									}									
								}
								else
								{
									Fail("Size container not displayed",log);
								}
								
								for (String temp : streamResp)
								{
									result.add(siteResp.contains(temp) ? 1 : 0);
									System.out.println(result);
									log.add("Compared result is: "+result);
								}
								
								if(!result.contains(0))
							  	{
							  		log.add("The sizes available for the product (Response from classicSite): " + streamResp.toString());
							  		log.add("The sizes available for the product (Response from MobileSite): " + siteResp.toString());
							  		Pass("All the sizes that are available for the product displayed as per the classic site",log);
							  	}
							  	else
							  	{
							  		log.add("The sizes available for the product (Response from classicSite): " + streamResp.toString());
							  		log.add("The sizes available for the product (Response from MobileSite): " + siteResp.toString());
							  		Fail("All the sizes that are not available for the product displayed as per the classic site.",log);
							  	}								
							}
							else
							{
								Fail("No products displayed for this keyword");
							}								
						}
						else
						{
							Fail("PLP page not displayed");
						}						
					}
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}								
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-541 Verify that Size option should be highlighted in Black color on selecting it and gray color if D-selected /normal.*/
	public void pdpSize_Color()
		{
			ChildCreation("HUDSONBAY-541 Verify that Size option should be highlighted in Black color on selecting it and gray color if deselected/normal.");
			String colour  = "#000000";
			String colour2 = "#e7e8e8";
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageSizeContainer));
				HBCBasicfeature.scrollup(pdppageSizeContainer, driver);
				if(HBCBasicfeature.isElementPresent(pdppageSizeContainer))
				{
					int size = pdppageSizeContainerList.size();
					String color = driver.findElement(By.xpath("//*[contains(@class,'pdpSizeSelect pdpSelectEle')][1]")).getCssValue("border");
					String bgColor = HBCBasicfeature.colorfinder(color.substring(12));	
					log.add("The Highlighted color on selecting is: "+bgColor);
					if(bgColor.contains(colour))
					{
						Pass("Selected Size option highlighted in Black color",log);
					}
					else
					{
						Fail("Selected Size option not highlighted in Black color",log);
					}
					if(size>1)
					{
						log.add("More than one Size is displayed");
						boolean clr = false;
						for(int i =2;i<=size;i++)
						{
							String de_color = driver.findElement(By.xpath("//*[contains(@class,'pdpSizeSelect pdpSelectEle')]["+i+"]")).getCssValue("background-color");
							String bg_DColor = HBCBasicfeature.colorfinder(de_color);	
							log.add("The Highlighted color on Deselecting/normal is: "+bg_DColor);
							if(bg_DColor.contains(colour2))
							{
								clr = true;
							}							
						}
						if(clr==true)
						{
							Pass("Size option highlighted in gray color if deselected/normal",log);
						}						
					}
					else
					{
						log.add("Single Size option is displayed, so can't check deselected/normal options");
					}
				}
				else
				{
					Fail("PDP Size container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-545 Verify that User should not be able to select multiple size or color*/
	public void pdp_selectMultipleSizeColor()
		{
			ChildCreation("HUDSONBAY-545 Verify that User should not be able to select multiple size or color");
			try
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(pdppageSizeContainer));
					HBCBasicfeature.scrollup(pdppageSizeContainer, driver);
					if(HBCBasicfeature.isElementPresent(pdppageSizeContainer))
					{
						int size = pdppageSizeContainerList.size();
						if(size>1)
						{
							for(int i =2;i<=size;i++)
							{
								WebElement sizeSelct = driver.findElement(By.xpath("//*[contains(@class,'pdpSizeSelect pdpSelectEle')]["+i+"]"));
								sizeSelct.click();
							}
							int val = pdppageSizeListSelected.size();
							log.add("The selected count of size is: "+val);
							Thread.sleep(500);
							if(val==1)
							{
								Pass("User not able to select multiple size",log);
							}
							else
							{
								Fail("User able to select multiple size",log);
							}
						}
						else
						{
							Skip("Product doesn't have more than one size "+size);
						}
					}
					else
					{
						Fail("Size container not displayed");
					}			
				}			
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}
				
				// Checking Color Swatche's
				
				try
				{					
					wait.until(ExpectedConditions.visibilityOf(pdppageSizeContainer));
					if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
					{
						int size = pdpPageSwatchesList.size();
						if(size>1)
						{
							for(int i=1;i<size;i++)
							{
								WebElement colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)["+(i+1)+"]"));
								colorSwatch.click();
							}
							int val = pdppageSwatchListColorSelected.size();
							log.add("The selected count of Color Swatches is: "+val);
							Thread.sleep(500);
							if(val==1)
							{
								Pass("User not able to select multiple colors",log);
							}
							else
							{
								Fail("User able to select multiple colors",log);
							}
						}
						else
						{
							Skip("Product doesn't have more than one colors: "+size);
						}
					}
					else
					{
						Skip("Color Swatches container not displayed");
					}
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something wrong. Please Check." + e.getMessage());
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-510 Verify that Price of the product should be displayed as per the classic site.*/
	public void pdpPage_price()
		{
			ChildCreation("HUDSONBAY-510 Verify that Price of the product should be displayed as per the classic site.");
/*			HttpURLConnection conn = null;
*/			ArrayList<String> streamRespType = new ArrayList<>();
			ArrayList<String> streamRespPrice = new ArrayList<>();
			String Sprice = "";
			try
			{
				String pdpStream = HBConstants.pdpStream.replace( "pdpUrl", pdpUrl);
				/*URL curl = new URL(pdpStream);
				conn = (HttpURLConnection) curl.openConnection();
				if(conn.getResponseCode()==200)
				{
					String classicResponse = IOUtils.toString(new URL(pdpStream));*/
					
						driver.navigate().to(pdpStream);
						String classicResponse = driver.findElement(By.xpath("/html/body/pre")).getText();	
						Thread.sleep(1000);
						driver.navigate().back();
									  
					  JSONObject jObj = new JSONObject(classicResponse);
					  JSONObject classicResultsObj = jObj.getJSONObject("properties").getJSONObject("buyinfo").getJSONObject("pricing");
					  JSONArray classicResultArray = classicResultsObj.getJSONArray("prices");
					  //  System.out.println(classicResultArray.length());
					  for(int j=0; j<classicResultArray.length();j++)
					  {
						  String saleType = classicResultArray.getJSONObject(j).getString("type");
						  streamRespType.add(saleType);
						  String price = classicResultArray.getJSONObject(j).getString("value");
						  streamRespPrice.add(price);
					  }
				/*}
				else
				{
					Fail("There is something wrong with the keyword sent to the URL. " + conn.getResponseMessage());
					System.out.println("HTTP Response Failure." + conn.getResponseMessage());
				}*/
				
				wait.until(ExpectedConditions.visibilityOf(pdppagePriceContainer));
				if(HBCBasicfeature.isElementPresent(pdppagePriceContainer))
				{
					if(streamRespType.size()>1)
					{
						regsale=true;
						log.add("Regular price and sale price present in response");
						String salePrice = pdppagePriceSale.getText();
						String RegPrice = pdppagePriceRegular.getText();
						log.add("The Regular price is: "+RegPrice+" and The Sale price is: "+salePrice);
						if((streamRespPrice.contains(RegPrice))&&streamRespPrice.contains(salePrice))
						{
							Pass("Price of the product displayed as per the classic site",log);
						}
						else
						{
							Fail("Price of the product not displayed as per the classic site",log);
						}						
					}
					else
					{
						log.add("Sale price alone present in response");
						Sprice = driver.findElement(By.xpath("//*[@class='skmob_priceValue ']")).getText();
						log.add("The Regular price is: "+Sprice);
						if(streamRespPrice.contains(Sprice))
						{
							Pass("Price of the product displayed as per the classic site",log);
						}
						else
						{
							Fail("Price of the product not displayed as per the classic site",log);
						}
					}
				}
				else
				{
					Fail("Product price container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-557 Verify that 'Sale' price, 'Actual' price should be shown below the product image");
			if(regsale==true)
			{
				
				Pass("'Sale' price, 'Actual' price displayed below the product image",log);
			}
			else
			{
				log.add("The Sale price is: "+Sprice);
				Skip(" This product not has'Sale' price & 'Actual' price");
			}
		}
			
	/*HUDSONBAY-511 Verify that Quantity increment(+) or decrement(-) buttons should be displayed below the product price.*/
	public void pdpQuantityContainer()
		{
			ChildCreation("HUDSONBAY-511 Verify that Quantity increment(+) or decrement(-) buttons should be displayed below the product price.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageQuantityContainer));
				if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
				{
					log.add("The Product Quantity container displayed");
					wait.until(ExpectedConditions.visibilityOf(pdppageQuantityMinus));
					if(HBCBasicfeature.isElementPresent(pdppageQuantityMinus))
					{
						Pass(" Quantity decrement(-) button displayed below the product price",log);
					}
					else
					{
						Fail("Quantity decrement(-) button not displayed below the product price",log);
					}
					wait.until(ExpectedConditions.visibilityOf(pdppageQuantityPlus));
					if(HBCBasicfeature.isElementPresent(pdppageQuantityPlus))
					{
						Pass(" Quantity increment(+) button displayed below the product price",log);
					}
					else
					{
						Fail(" Quantity increment(+) button not displayed below the product price",log);
					}
				}
				else
				{
					Fail("The Product Quantity container not displayed");
				}			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-546 Verify that By default the qty 1 will be selected*/
	public void PDPQuantity_default()
		{
			ChildCreation("HUDSONBAY-546 Verify that By default the qty 1 will be selected");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageQuantityContainer));
				if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
				{
					String val = pdppageQuantityText.getAttribute("value");
					log.add("The current Quantity is: "+val);
					if(val.equals("1"))
					{
						Pass("By default the qty 1 selected",log);
					}
					else
					{
						Fail("By default the qty 1 is not selected",log);
					}
				}
				else
				{
					Fail("Product Quantity Container is not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-512 Verify that Quantity should be incremented on selecting the Quantity increment button("+").*/
	public void pdpQuantityIncrement()
		{
			ChildCreation("HUDSONBAY-512 Verify that Quantity should be incremented on selecting the Quantity increment button("+").");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageQuantityContainer));
				if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
				{
					String val = pdppageQuantityText.getAttribute("value");
					log.add("The current Quantity is: "+val);
					if(HBCBasicfeature.isElementPresent(pdppageQuantityPlus))
					{
						pdppageQuantityPlus.click();
						String valPlus = pdppageQuantityText.getAttribute("value");
						log.add("The updated Quantity is: "+valPlus);

						if(val.equals(valPlus))
						{
							Fail(" Quantity not incremented on selecting the Quantity increment button("+")",log);
						}
						else
						{
							Pass("Quantity incremented on selecting the Quantity increment button("+")",log);
						}
					}
					else
					{
						Fail("Quantity increment button("+") is displayed");
					}
				}
				else
				{
					Fail("Quantity Container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-513 Verify that Quantity should be decremented on selecting the Quantity decrement button("-").*/
	public void pdpQuantityDecrement()
		{
			ChildCreation("HUDSONBAY-513 Verify that Quantity should be decremented on selecting the Quantity decrement button('-').");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageQuantityContainer));
				if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
				{
					String val = pdppageQuantityText.getAttribute("value");
					log.add("The current Quantity is: "+val);
					if(HBCBasicfeature.isElementPresent(pdppageQuantityMinus))
					{
						pdppageQuantityMinus.click();
						String valPlus = pdppageQuantityText.getAttribute("value");
						log.add("The updated Quantity is: "+valPlus);

						if(val.equals(valPlus))
						{
							Fail(" Quantity not decremented on selecting the Quantity decrement button('-')",log);
						}
						else
						{
							Pass("Quantity decremented on selecting the Quantity decrement button('-')",log);
						}
					}
					else
					{
						Fail("Quantity decrement button('-') is displayed");
					}
				}
				else
				{
					Fail("Quantity Container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-514 Verify that User can able to add/update maximum of 5 products based on the client requirements and as per the classic site*/
	public void pdpMaximumQuantity()
		{
			ChildCreation("HUDSONBAY-514 Verify that User can able to add/update maximum of 5 products based on the client requirements and as per the classic site");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageQuantityContainer));
				if(HBCBasicfeature.isElementPresent(pdppageQuantityContainer))
				{
					if(HBCBasicfeature.isElementPresent(pdppageQuantityPlus))
					{							
						int size = 6;
						for(int i = 1;i<=size;i++)
						{
							try
							{
								pdppageQuantityPlus.click();
							}
							catch(Exception e)
							{
								String valPlus = pdppageQuantityText.getAttribute("value").trim();
								int k = Integer.parseInt(valPlus);
								log.add("The updated Quantity is: "+valPlus);
								if(k==5)
								{
									Pass("User can able to add/update maximum of 5 products based on the client requirements and as per the classic site",log);
									break;
								}
								else
								{
									Fail("User not able to add/update maximum of 5 products based on the client requirements and as per the classic site",log);
								}			
							}
						}									
					}
					else
					{
						Fail("Quantity plus option not displayed");
					}						
				}
				else
				{
					Fail("PDP Quantity container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/*HUDSONBAY-493 Verify that color swatche's tiles should be shown, when the respective product has more than 15 colors*/
	public void pdpColorSwatches_MoreTiles()
		{
			ChildCreation("HUDSONBAY-493 Verify that color swatches tiles should be shown, when the respective product has more than 15 colors");
			String key = "CLINIQUE Chubby Stick Moisturizing Lip";
			boolean keyFound = false;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(key);
						searchBox.sendKeys(Keys.ENTER);
						wait.until(ExpectedConditions.visibilityOf(plpPage));
						if(HBCBasicfeature.isElementPresent(plpPage))
						{							
							int plpsize = plpItemContList.size();
							int i =0;
							if(plpsize>=1)
							{									
								for(i=1;i<=plpsize;i++)
								{										
									String product_BrandTitle = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_brandTitle'])["+i+"]")).getText();
									String product_Title = driver.findElement(By.xpath("(//*[@class='skMob_productDetails']//*[@class='skMob_productTitle'])["+i+"]")).getText();
									String PLPtitle = product_BrandTitle +" "+ product_Title;
									Thread.sleep(500);
									if(PLPtitle.contains(key))
									{
										keyFound = true;
										break;
									}	
									else
									{
										continue;
									}
								}									
								if(keyFound==true)
								{
									WebElement product = driver.findElement(By.xpath("//*[contains(@class,'skMob_productListItemOuterCont')]["+i+"]"));		
									product.click();
									wait.until(ExpectedConditions.visibilityOf(pdpPageSwatchesContainer));
									if(HBCBasicfeature.isElementPresent(pdpPageSwatchesContainer))
									{
										log.add("Color swatches Tile displayed");
										int size = pdpPageSwatchesList.size();
										log.add("Color swatches size is: "+size);
										if(size>15)
										{
											Pass("Color swatches Tiles shown, when the respective product has more than 15 colors",log);
										}
										else
										{
											Fail("Color swatches Tiles not shown, when the respective product has more than 15 colors",log);
										}
									}
									else
									{
										Fail("Color swatches Tiles not displayed");
									}
								}
								else
								{
									Fail("No products matched");
								}
							}
							else
							{
								Fail("No products displayed for this keyword");
							}								
						}
						else
						{
							Fail("PLP page not displayed");
						}						
					}
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-494 Verify that name of the color that is selected in the Tile should be displayed*/
	public void pdpColorSwatches_MoreTilesColor()
		{
			ChildCreation("HUDSONBAY-494 Verify that name of the color that is selected in the Tile should be displayed");
			try
			{
				Random r = new Random();
				int size = pdpPageSwatchesList.size();
				int i = r.nextInt(size);
				WebElement colorSwatch = driver.findElement(By.xpath("(//*[@class='pdpAvailColors']//img)["+(i+1)+"]"));
				colorSwatch.click();
				Thread.sleep(500);
				String selectedColor = colorSwatch.getAttribute("aria-label");
				log.add("The actual Tile color is: "+selectedColor);
				String displayedColor = pdpPageSwatchSelectedColor.getText();
				log.add("The Displayed Selected Tile color is: "+displayedColor);
				if(selectedColor.equalsIgnoreCase(displayedColor))
				{
					Pass("Name of the color that is selected in the Tile is displayed",log);
				}
				else
				{
					Fail("Name of the color that is selected in the Tile is not displayed",log);
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-525 Verify that on tapping the product image, image should be zoomed*/
	public void pdp_ProductZoom()
		{
			ChildCreation("HUDSONBAY-525 Verify that on tapping the product image, image should be zoomed");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageZoomIcon));
				HBCBasicfeature.scrollup(pdppageZoomIcon, driver);
				if(HBCBasicfeature.isElementPresent(pdppageZoomIcon))
				{
					log.add("Zoom Icon displayed");
					pdppageZoomIcon.click();
					wait.until(ExpectedConditions.visibilityOf(pdppageZoomContainer));
					if(HBCBasicfeature.isElementPresent(pdppageZoomContainer))
					{
						Pass("on tapping the product image, image gets zoomed",log);
					}
					else
					{
						Fail("on tapping the product image, image not zoomed",log);
					}
				}
				else
				{
					Fail("Zoom Icon not displayed in the PDP");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-779 Verify that user should not able to scroll the background, when the product image is in zoomed*/
	public void pdp_productNoScroll()
		{
			ChildCreation("HUDSONBAY-779 Verify that user should not able to scroll the background, when the product image is in zoomed");
			try
			{
				try
				{
					if(HBCBasicfeature.isElementPresent(pdppageZoomContainer))
					{
						log.add("Product image is zoomed");
						HBCBasicfeature.scrolldown(pdppageATB, driver);
						pdppageATB.click();
						Fail("User able to scroll the background, when the product image is in zoomed",log);
					}
				}
				catch (Exception e)
				{
					Pass("User not able to scroll the background, when the product image is in zoomed",log);
				}	
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-526 Verify that on tapping outside the product image, the image should get zoomed out*/
	public void pdp_ZoomClick()
		{
			ChildCreation("HUDSONBAY-526 Verify that on tapping outside the product image, the image should get zoomed out");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageZoomContainer));
				if(HBCBasicfeature.isElementPresent(pdppageZoomContainer))
				{
					HBCBasicfeature.jsclick(pdppageZoomMask, driver);			
					if(HBCBasicfeature.isElementPresent(pdppageZoomContainer))
					{
						Fail("on tapping outside the product image, the image not get zoomed out");
					}
					else
					{
						Pass("on tapping outside the product image, the image get zoomed out");
					}
				}
				else
				{
					Fail("Image not Zoomed in");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}	
	
	/*HUDSONBAY-537 Verify that social icons such as "Facebook", "Google+", "Twitter" and so on should be shown for hitting the like for the product*/
	public void pdp_SocialIcons()
		{
			ChildCreation("HUDSONBAY-537 Verify that social icons such as Facebook, Google+, Twitter and so on should be shown for hitting the like for the product");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageShareIcons));
				HBCBasicfeature.scrolldown(pdppageShareIcons, driver);
				if(HBCBasicfeature.isElementPresent(pdppageShareIcons))
				{
					if(HBCBasicfeature.isElementPresent(pdppageShareTwitter))
					{
						Pass("Social Icon Twitter displayed");
					}
					else
					{
						Fail("Social Icon Twitter not displayed");
					}
					if(HBCBasicfeature.isElementPresent(pdppageShareGplus))
					{
						Pass("Social Icon Google+ displayed");
					}
					else
					{
						Fail("Social Icon Google+ not displayed");
					}
					if(HBCBasicfeature.isElementPresent(pdppageShareFB))
					{
						Pass("Social Icon FB+ displayed");
					}
					else
					{
						Fail("Social Icon FB not displayed");
					}
				}
				else
				{
					Fail("PDP share Icons container not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-548/549 Verify that accordions should be shown below the "Add to Bag" button for some of the products*/
	public void pdp_Accordians()
		{
			boolean flag = false;
			ChildCreation("HUDSONBAY-548 Verify that accordions should be shown below the 'Add to Bag' button for some of the products");
			try
			{
				wait.until(ExpectedConditions.visibilityOfAllElements(pdppageDescriptionAccords));
				HBCBasicfeature.scrollup(pdppageATB, driver);
				if(HBCBasicfeature.isListElementPresent(pdppageDescriptionAccords))
				{
					int size = pdppageDescriptionAccords.size();
					log.add("Accordians present below the Add to Bag button is: "+size);
					for(int i=1;i<=size;i++)
					{
						String prodDesc = driver.findElement(By.xpath("(//*[contains(@class,'pdp_Description')]//h1)["+i+"]")).getText();
						Thread.sleep(200);
						boolean prodDescAccord = driver.findElement(By.xpath("//*[contains(@class,'pdp_Description')]["+i+"]//*[@class='pdp_DetailsLink']")).isDisplayed();
						if(prodDescAccord==true)
						{
							flag = true;
							Pass("Accordion shown below the 'Add to Bag' button is: "+prodDesc,log);
						}
						else
						{
							Fail("Accordian not displayed",log);
						}
					}					
				}
				else
				{
					Fail("Product Accordians not displayed below the ATB button");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			ChildCreation("HUDSONBAY-549 Verify that Details, Sizing Info, Shipping Info, Customer Care accordions should be shown for some of the products");
			if(flag==true)
			{
				Pass("Details, Sizing Info, Shipping Info, Customer Care accordions displayed for some of the products",log);
			}
			else
			{
				Fail("Details, Sizing Info, Shipping Info, Customer Care accordions not displayed for some of the products",log);
			}
		}
	
	/* HUDSONBAY-863 Verify that section arrangements should be shown as per the creative and it should match with the classic site*/
	public void pdp_sectionArrangements() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-863 Verify that section arrangements should be shown as per the creative and it should match with the classic site");
			ArrayList<String> actualAccord = new ArrayList<>();
			ArrayList<String> expectedAccord = new ArrayList<>();
			String accordnames = HBCBasicfeature.getExcelVal("HB863", sheet, 1);		
			boolean flag = false;
			String[] split = accordnames.split("\n");
			int size1 = split.length;
			for(int j = 0;j<size1;j++)
			{
				actualAccord.add(split[j]);
			}			
			try
			{
				if(HBCBasicfeature.isListElementPresent(pdppageDescriptionAccords))
				{
					int size = pdppageDescriptionAccords.size();
					for(int i=1;i<=size;i++)
					{
						String prodDesc = driver.findElement(By.xpath("(//*[contains(@class,'pdp_Description')]//h1)["+i+"]")).getText();
						expectedAccord.add(prodDesc);
					}
					int actualSize = actualAccord.size();
					int expSize = expectedAccord.size();
					Thread.sleep(500);
					if(actualSize==expSize)
					{
						for(int k = 0;k<actualSize;k++)
						{
							flag = false;
							if(actualAccord.get(k).equalsIgnoreCase(expectedAccord.get(k)))
							{
								flag = true;
								log.add("The section "+expectedAccord.get(k)+" arranged as per the creative");
							}
							else
							{
								log.add("The section "+expectedAccord.get(k)+" not arranged as per the creative");
							}
						}
						if(flag==true)
						{
							Pass("Section arrangements  shown as per the creative and it match with the classic site",log);
						}
						else
						{
							Fail("Section arrangements  not shown as per the creative and not match with the classic site",log);
						}
					}
					else
					{
						Fail("Displayed sections size are not equal");
					}
				}
				else
				{
					Fail("Accordians are not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-551 Verify that all the accordion in the PDP page should be in collapsed state by default*/
	public void pdp_AccordCollpsed()
		{
			ChildCreation("HUDSONBAY-551 Verify that all the accordion in the PDP page should be in collapsed state by default");
			try
			{
				wait.until(ExpectedConditions.visibilityOfAllElements(pdppageDescriptionAccords));
				if(HBCBasicfeature.isListElementPresent(pdppageDescriptionAccords))
				{
					int size = pdppageDescriptionAccords.size();
					log.add("Accordians present below the Add to Bag button is: "+size);
					for(int i=1;i<=size;i++)
					{
						String prodDesc = driver.findElement(By.xpath("(//*[contains(@class,'pdp_Description')]//h1)["+i+"]")).getText();
						WebElement ExpandDomm = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+i+"]"));
						try
						{
							wait.until(ExpectedConditions.attributeContains(ExpandDomm, "style", "none"));
							Pass("Accordion in the PDP page not in collapsed state by default for: "+prodDesc,log);
						}
						catch (Exception e)		
						{
							Fail("Accordion in the PDP page not in collapsed state by default for: "+prodDesc,log);
						}
					}
				}
				else
				{
					Fail("Product Accordians not displayed in the PDP page");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-552/782 Verify that on tapping any accordion it should be should be enabled and all the details should be displayed as per the classic site*/
	public void pdp_AccordExpand()
		{
			ChildCreation("HUDSONBAY-552 Verify that on tapping any accordion it should be should be enabled and all the details should be displayed as per the classic site");
			boolean flag = false;
			try
			{
				System.out.println("AccordExpand case starts");
				WebDriverWait w1 = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.visibilityOfAllElements(pdppageDescriptionAccords));
				if(HBCBasicfeature.isListElementPresent(pdppageDescriptionAccords))
				{
					int size = pdppageDescriptionAccords.size();
					log.add("Accordians present below the Add to Bag button is: "+size);
					for(int i=1;i<=size;i++)
					{
						WebElement prodDesc = driver.findElement(By.xpath("(//*[contains(@class,'pdp_Description')]//h1)["+i+"]"));
						WebElement prodDescAccord = driver.findElement(By.xpath("(//*[@class='pdp_DetailsLink'])["+i+"]"));
						WebElement ExpandDomm = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+i+"]"));
						Thread.sleep(500);
						String accordName = prodDesc.getText();
						log.add("The selected Accordian is: "+accordName);
						wait.until(ExpectedConditions.visibilityOf(prodDesc));					
						if(HBCBasicfeature.isElementPresent(prodDesc))
						{
							HBCBasicfeature.jsclick(prodDesc, driver);
							try
							{
								w1.until(ExpectedConditions.attributeContains(ExpandDomm, "style", "block"));
							}
							catch(Exception e)
							{
								wait.until(ExpectedConditions.attributeContains(ExpandDomm, "style", "none"));
								HBCBasicfeature.jsclick(prodDesc, driver);
								Thread.sleep(1500);
							}							
						}
						wait.until(ExpectedConditions.attributeContains(ExpandDomm, "style", "block"));								
						if(accordName.contains("Sizing Info"))//Since Sizing info has Image file
						{
							Pass(" on tapping any accordion all the details are displayed as per the classic site",log);
							Thread.sleep(1000);
							HBCBasicfeature.jsclick(prodDescAccord, driver);
							wait.until(ExpectedConditions.attributeContains(ExpandDomm, "style", "none"));
							continue;
						}
						Thread.sleep(1500);
						String content = pdppageAccordsExpand.getText();
						log.add("The product description is: "+content);
						boolean result =content.isEmpty();
						if(result==false)
						{
							Pass(" on tapping any accordion all the details are displayed as per the classic site",log);
							flag=true;
							Thread.sleep(1500);
							HBCBasicfeature.jsclick(prodDescAccord, driver);
							wait.until(ExpectedConditions.attributeContains(ExpandDomm, "style", "none"));	
						}
						else	
						{
							Fail(" on tapping any accordion all the details are not displayed as per the classic site",log);
							Thread.sleep(1500);
							HBCBasicfeature.jsclick(prodDescAccord, driver);
							wait.until(ExpectedConditions.attributeContains(ExpandDomm, "style", "none"));								
						}						
					}
				}
				else
				{
					Fail("Product Accordians not displayed in the PDP page");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-782 Verify that blank sections should not be shown for the Products in the PDP Page");
			if(flag==true)
			{
				Pass("Blank sections not shown for the Products in the PDP Page");
			}
			else
			{
				Fail("Blank sections  shown for the Products in the PDP Page");
			}
		}
		
	/*HUDSONBAY-507/476/508 Verify that Product description should be displayed below the size drop down*/
	public void pdpProductDesc_ExpandCollpase() throws InterruptedException
		{
			ChildCreation("HUDSONBAY-507 Verfiy that Product description should be displayed below the size drop down");
			boolean opt = false;
			try
			{
				WebDriverWait w1 = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.visibilityOf(pdppageDetails));
				HBCBasicfeature.scrolldown(pdppageDetails, driver);
				if(HBCBasicfeature.isElementPresent(pdppageDetails))
				{
					try
					{
						w1.until(ExpectedConditions.visibilityOf(pdppageDetailsExpand));
						if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
						{
							opt = true;
							Pass("Product description displayed below the size drop down");			
						}						
					}
					catch(Exception e)
					{
						pdppageDetails.click();
						if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
						{							
							opt = true;
							Pass("Product description displayed below the size drop down");			
						}					
					}
				}
				else
				{
					Fail("Product description not displayed below the size drop down");
				}
			}			
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}
			
			ChildCreation("HUDSONBAY-476 Verify that HBC Rewards points should be displayed below the ADD to BAG button under details section.");
			if(opt = true)
			{
				if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
				{
					if(HBCBasicfeature.isElementPresent(pdppageDetailsReward))
					{
						log.add("Rewards point displayed is: "+pdppageDetailsReward.getText());
						Pass("HBC Rewards points displayed below the ADD to BAG button under details section",log);
					}
					else
					{
						Fail("HBC Rewards points not displayed below the ADD to BAG button under details section",log);
					}
				}
				else
				{
					Fail("Details section not in expand state");
				}
			}
			else
			{
				Skip("PDP Details section not expanded");
			}	
			
			ChildCreation("HUDSONBAY-508 Verfiy that user should be able to do expand or collapse the product description.");
			if(opt==true)
			{				
				if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
				{
					Pass("User able to do expand the product description");
					pdppageDetailsExpandCls.click();
					Thread.sleep(1000);
					if(HBCBasicfeature.isElementPresent(pdppageDetailsExpand))
					{
						Fail("User not able to do collapse the product description");
					}
					else
					{
						Pass("User able to do collapse the product description");
					}
				}
				else
				{
					Fail("User not able to do expand the product description");
				}
			}
			else
			{
				Fail("Details section not expanded");
			}					
			
		}
		
	/*HUDSONBAY-475/509 Verify that product description in the mobile site must match with the classic site.*/
	public void pdpProdDesc_Creative()
		{
			ChildCreation("HUDSONBAY-509 Verify that product description in the mobile site must match with the classic site.");
			boolean HBC475 = false;
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageDetails));
				if(HBCBasicfeature.isElementPresent(pdppageDetails))
				{
					pdppageDetails.click();
					Thread.sleep(500);
					if(HBCBasicfeature.isElementPresent(pdppageAccordsExpand))
					{
						String pdp = pdppageDetailsExpandDOM.getText();
						log.add("The product description is: "+pdp);
						boolean result =pdp.isEmpty();
						if(result==false)
						{
							Pass("Product description in the mobile site matches with the classic site.",log);
							HBC475 = true;
						}
						else	
						{
							Fail("Product description in the mobile site not matches with the classic site.",log);
						}
					}
					else
					{
						Fail("Product description section not expaned");
					}
				}
				else
				{
					Fail("Details section not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-475 Verify that product details should be displayed below the ADD to BAG button under details section. ");
			if(HBC475 == true)
			{
				Pass(" Product details displayed below the ADD to BAG button under details section");
			}
			else
			{
				Fail("Product details not displayed below the ADD to BAG button under details section");
			}		
		}
			
	/*HUDSONBAY-553 Verify that accordion should be closed on tapping the opened state accordion again or tapping the collapsed state accordion*/
	public void pdp_AccordCls()
		{
			ChildCreation("HUDSONBAY-553 Verify that accordion should be closed on tapping the opened state accordion again or tapping the collapsed state accordion");
			try
			{
				System.out.println("AccordExpand case ends");
				if(HBCBasicfeature.isElementPresent(pdppageAccordsExpand))
				{
					log.add("The accordian details is in opened state");
					pdppageDetailsExpandCls.click();
					Thread.sleep(500);
					if(HBCBasicfeature.isElementPresent(pdppageAccordsExpand))
					{
						Fail("Accordion not closed on tapping the opened state accordion again",log);
					}
					else
					{
						Pass("Accordion closed on tapping the opened state accordion again",log);
					}
				}
				else
				{
					Fail("Accordian Details is not in opened state");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
		
	/* HUDSONBAY-515 Verify that Add to bag button should be displayed as per the creative*/
	public void pdp_AddToBag() throws Exception
		{
			ChildCreation("HUDSONBAY-515 Verify that Add to bag button should be displayed as per the creative");
			String color = HBCBasicfeature.getExcelVal("HBC706", sheet, 2);
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageATB));
				HBCBasicfeature.scrolldown(pdppageATB, driver);
				if(HBCBasicfeature.isElementPresent(pdppageATB))
				{
					log.add("Add To Bag button displayed");
					String Colour = pdppageATB.getCssValue("background");					
					String bgColor = HBCBasicfeature.colorfinder(Colour.substring(0,12));					
					if(bgColor.contains(color))
					{
						Pass("Add to bag button displayed as per the creative",log);
					}
					else
					{
						Fail("Add to bag button not displayed as per the creative",log);
					}
				}
				else
				{
					Fail("Add to bag button not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/* HUDSONBAY-516 Verify that Added to Bag success overlay should be shown on selecting the Add to bag button.*/
	public void pdp_AddToBagClick()
		{
			ChildCreation("HUDSONBAY-516 Verify that Added to Bag success overlay should be shown on selecting the Add to bag button.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageATB));
				if(HBCBasicfeature.isElementPresent(pdppageATB))
				{
					HBCBasicfeature.scrollup(pdppageATB, driver);
					log.add("Add To Bag button displayed");
					pdppageATB.click();
					wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
					Thread.sleep(1500);
					try
					{
						wait.until(ExpectedConditions.visibilityOf(ATBAlert));
						if(HBCBasicfeature.isElementPresent(ATBAlert))
						{
							log.add("Limited quantities alert displayed");
							int val = 0;
							ATBAlertOk.click();
							do
							{
								pdppageQuantityMinus.click();	
								Thread.sleep(200);
								String valPlus = pdppageQuantityText.getAttribute("value");
								val = Integer.parseInt(valPlus);
							}
							while(val!=1);
							pdppageATB.click();
						}
					}
					catch (Exception e)
					{
						log.add("Limited quantities alert not displayed");						
					}					
					wait.until(ExpectedConditions.attributeContains(pdppageATBOverlay, "style", "block"));
					if(HBCBasicfeature.isElementPresent(pdppageATBOverlay))
					{
						Pass("Added to Bag success overlay shown on selecting the Add to bag button",log);
						pdppageATBOverlayClose.click();
					}
					else
					{
						Fail("Added to Bag success overlay not shown on selecting the Add to bag button",log);
					}
					/*wait.until(ExpectedConditions.visibilityOf(pdppageATB));
					if(HBCBasicfeature.isElementPresent(pdppageATB))
					{
						HBCBasicfeature.scrollup(pdppageATB, driver);
						log.add("Add To Bag button displayed");
						pdppageATB.click();
						wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
						Thread.sleep(1500);
						try
						{
							wait.until(ExpectedConditions.visibilityOf(ATBAlert));
							if(HBCBasicfeature.isElementPresent(ATBAlert))
							{
								log.add("Limited quantities alert displayed");
								int val = 0;
								ATBAlertOk.click();
								do
								{
									pdppageQuantityMinus.click();	
									Thread.sleep(200);
									String valPlus = pdppageQuantityText.getAttribute("value");
									val = Integer.parseInt(valPlus);
								}
								while(val!=1);
								pdppageATB.click();
							}
						}
						catch (Exception e)
						{
							log.add("Limited quantities alert not displayed");						
						}					
						wait.until(ExpectedConditions.attributeContains(pdppageATBOverlay, "style", "block"));
						if(HBCBasicfeature.isElementPresent(pdppageATBOverlay))
						{
							Pass("Added to Bag success overlay shown on selecting the Add to bag button",log);
							pdppageATBOverlayClose.click();
						}
						else
						{
							Fail("Added to Bag success overlay not shown on selecting the Add to bag button",log);
						}
				}
				else
				{
					Fail("Add to bag button not displayed");
				}*/
			}
			else
			{
				Fail("Add to bag button not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-547 Verify that on adding the product to the bag, the qty count should be increased in the header*/
	/*HUDSONBAY-518 Verify that 'Shopping Bag' page should be displayed on tapping the 'Checkout' button*/
	/*HUDSONBAY-535 Verify that while adding the same product multiple times,the items should be added as an individual product*/

	public void pdp_QuantityCount()
		{
			ChildCreation("HUDSONBAY-547 Verify that on adding the product to the bag, the qty count should be increased in the header");
			boolean HBC518 = false, HBC535 = false;
			try
			{
				HBCBasicfeature.scrollup(bagIcon, driver);
				String prev_Count = bagIconCount.getText(); //getting value of Bag count
				int pCount = Integer.parseInt(prev_Count);
				log.add("The Previous Qty count is: "+pCount);
				pdppageATB.click();
				wait.until(ExpectedConditions.attributeContains(loadingbar, "style", "none"));
				wait.until(ExpectedConditions.attributeContains(pdppageATBOverlay, "style", "block"));
				if(HBCBasicfeature.isElementPresent(pdppageATBOverlay))
				{
					Pass("ATB success overlay displayed");
					if(HBCBasicfeature.isElementPresent(pdpcheckOutBtn))
					{
						HBCBasicfeature.jsclick(pdpcheckOutBtn, driver);
						wait.until(ExpectedConditions.visibilityOf(shoppingCartPage));
						if(HBCBasicfeature.isElementPresent(shoppingCartPage))
						{
							HBC518 = true;
						}
						else
						{
							HBC518 = false;
						}
					}
					
					String current_Count = bagIconCount.getText();
					int cCount  = Integer.parseInt(current_Count);
					log.add("The Current Qty count is: "+cCount);
					if(cCount>pCount)
					{
						Pass("On adding the product to the bag, the qty count increased in the header",log);
					}
					else
					{
						Fail("On adding the product to the bag, the qty count not increased in the header",log);
					}
				}
				else
				{
					Fail("ATB Overlay not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}	
			
			ChildCreation("HUDSONBAY-518 Verify that 'Shopping Bag' page should be displayed on tapping the 'Checkout' button");
			if(HBC518 == true)
			{
				Pass("'Shopping Bag' page displayed on tapping the 'Checkout' button");
				 HBC535 = true;
			}
			else
			{
				Fail("'Shopping Bag' page not displayed on tapping the 'Checkout' button");
			}
			
			ChildCreation("HUDSONBAY-535 Verify that while adding the same product multiple times,the items should be added as an individual product");
			if(HBC535 == true)
			{
				if(HBCBasicfeature.isListElementPresent(shoppingBagPageProdTitle))
				{
					int size = shoppingBagPageProdTitle.size();
					if(size == 2)
					{
						String firstTitle  = driver.findElement(By.xpath("(//*[@class='item-info']//*[@class='pro-name'])[1]")).getText();
						String SecondTitle  = driver.findElement(By.xpath("(//*[@class='item-info']//*[@class='pro-name'])[2]")).getText();
						log.add("First product title is: "+firstTitle);
						log.add("Second product title is: "+SecondTitle);
						if(firstTitle.equalsIgnoreCase(SecondTitle))
						{
							Pass(" Adding the same product multiple times,the items added as an individual product",log);
						}
						else
						{
							Fail("Adding the same product multiple times,the items not added as an individual product",log);
						}	
					}
					else
					{
						Fail("More than two products displayed");
					}
				}
				else
				{
					Fail("Products title not displayed");
				}
			}
			else
			{
				Skip("'Shopping Bag' page not displayed on tapping the 'Checkout' button");
			}
		}
		
	/*HUDSONBAY-560 Verify that on selecting VIDEO for the product then the popup will be shown with the video player.*/
	public void pdp_videoProduct() throws Exception
		{
			ChildCreation("HUDSONBAY-560 Verify that on selecting VIDEO for the product then the popup will be shown with the video player.");
			String key = HBCBasicfeature.getExcelVal("HB560", sheet, 1);
			try
			{
				HBCBasicfeature.scrollup(searchIcon, driver);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(key);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(PDPPage));
						if(HBCBasicfeature.isElementPresent(PDPPage))
						{
							driver.switchTo().defaultContent();
							HBCBasicfeature.scrolldown(pdppageDetails, driver);
							if(HBCBasicfeature.isElementPresent(pdppageDetails))
							{
								pdppageDetails.click();
								driver.switchTo().frame(pdppageIframeVideo);
								Thread.sleep(500);
								wait.until(ExpectedConditions.visibilityOf(pdppageVideo));
								HBCBasicfeature.scrolldown(pdppageVideo, driver);
								if(HBCBasicfeature.isElementPresent(pdppageVideo))
								{
									log.add("Video player displayed");
									Thread.sleep(500);
									if(HBCBasicfeature.isElementPresent(videoPlayPause))
									{					
										HBCBasicfeature.jsclick(videoPlayPause, driver);
										Pass("On selecting VIDEO for the product then the video playing in the video player",log);
									}
									else
									{
										Fail("On selecting VIDEO for the product then the video not playing in the video player",log);
									}
								}			
								else
								{
									Fail("Video not displayed for the product");
								}
							}
							else
							{
								Fail("PDP page details section not displayed");
							}
						}
						else
						{
							Fail("PDP page not displayed");
						}
					}
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-561 Verify that on selecting VIDEO share from the video for the product then the corresponding share option sohuld be shown*/
	public void pdp_videoShare()
		{
			ChildCreation("HUDSONBAY-561 Verify that on selecting VIDEO share from the video for the product then the corresponding share option sohuld be shown");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pdppageVideo));
				if(HBCBasicfeature.isElementPresent(pdppageVideo))
				{
					videoPlayPause.click();
					if(HBCBasicfeature.isElementPresent(videoSocialShare))
					{
						videoSocialShare.click();
						wait.until(ExpectedConditions.visibilityOf(videoEmailShare));
						if(HBCBasicfeature.isElementPresent(videoEmailShare))
						{
							Pass("Email share option displayed");
						}
						else
						{
							Fail("Email share option not displayed");
						}
						if(HBCBasicfeature.isElementPresent(videoEmbedShare))
						{
							Pass("Embed share option displayed");
						}
						else
						{
							Fail("Embed share option not displayed");
						}
						if(HBCBasicfeature.isElementPresent(videoLinkShare))
						{
							Pass("Link share option displayed");
						}
						else
						{
							Fail("Link share option not displayed");
						}
						if(HBCBasicfeature.isElementPresent(videoTwitterShare))
						{
							Pass("Twitter share option displayed");
						}
						else
						{
							Fail("Link share option not displayed");
						}
						if(HBCBasicfeature.isElementPresent(videoFacebookShare))
						{
							Pass("Facebook share option displayed");
						}
						else
						{
							Fail("Facebook share option not displayed");
						}						
					}
					else
					{
						Fail("Social share icon not displayed");
					}
				}
				else
				{
					Fail("Video container not displayed");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-562 Verify that on selecting full screen option from the VIDEO of the product then the fullscreen video player should be shown. */
	public void pdp_videoFullScreen()
		{
			ChildCreation("HUDSONBAY-562 Verify that on selecting full screen option from the VIDEO of the product then the fullscreen video player should be shown.");
			try
			{
				if(HBCBasicfeature.isElementPresent(pdppageVideo))
				{
					if(HBCBasicfeature.isElementPresent(videoFullScreen))
					{
						videoFullScreen.click();
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(pdppageVideoContainer, "mode", "fullscreen"));
						Pass("On selecting Full screen option from the VIDEO of the product then the fullscreen video player shown");	
						videoFullScreen.click();
					}
					else
					{
						Fail("Full screen option not displayed");
					}
				}
				else
				{
					Fail("Video container not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-563 Verify that on selecting related video links below the video of the product then the new video should be shown.*/
	public void pdp_relatedVideo()
		{
			ChildCreation("HUDSONBAY-563 Verify that on selecting related video links below the video of the product then the new video should be shown.");
			try
			{
				driver.switchTo().defaultContent();
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(pdppageIframeRelatedVideo));
				if(HBCBasicfeature.isListElementPresent(relatedVideosList))
				{
					int size = relatedVideosList.size();
					for(int i=1;i<=size;i++)
					{
						WebElement relatedVidz = driver.findElement(By.xpath("//*[@id='player']["+i+"]//*[@class='ytp-icon ytp-icon-large-play-button-hover']"));
						Thread.sleep(500);
						relatedVidz.click();
						Pass("On selecting related video links below the video of the product then the new video shown");
						driver.switchTo().defaultContent();
					}
				}
				else
				{
					Skip("Related videos not displayed for the product");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	/*HUDSONBAY-565 Verify that on selecting "CALL TO PRODUCT" option for the product then the popup will be shown with the call option.*/
	public void pdp_callToProduct() throws Exception
		{
			ChildCreation("HUDSONBAY-565 Verify that on selecting 'CALL TO PRODUCT' option for the product then the popup will be shown with the call option.");
			try
			{
				wait.until(ExpectedConditions.visibilityOf(PDPPage));
				if(HBCBasicfeature.isElementPresent(PDPPage))
				{	
					log.add("PDP page displayed");
					HBCBasicfeature.scrolldown(callToOrder, driver);
					if(HBCBasicfeature.isElementPresent(callToOrder))
					{
						Thread.sleep(500);
						String number = callToOrder.getText().replace("-", "").trim();	
						if(number.matches(".*\\d+.*"))
						{
							log.add("Call To Order number is: "+number);
							Pass("on selecting 'CALL TO PRODUCT' option for the product then the popup shown with the call option",log);
						}
						else
						{
							Fail("on selecting 'CALL TO PRODUCT' option for the product no popup shown",log);
						}
					}
					else
					{
						Fail("Call to order number not displayed");
					}							
				}
				else
				{
					Fail("PDP page not displayed");
				}					
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-855 Verify that Add to Bag button should not be shown for Call to order products*/
	public void pdp_callToProduct_noATB() throws java.lang.Exception
		{
			ChildCreation("HUDSONBAY-855 Verify that Add to Bag button should not be shown for Call to order products");
			String key = HBCBasicfeature.getExcelVal("HB855", sheet, 1);
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					HBCBasicfeature.scrollup(searchIcon, driver);
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(key);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(PDPPage));
						if(HBCBasicfeature.isElementPresent(PDPPage))
						{
							log.add("PDP Page displayed");
							if(HBCBasicfeature.isElementPresent(pdppageATB))
							{
								Fail("Add to Bag button shown for Call to order products",log);
							}
							else
							{
								Pass("Add to Bag button not shown for Call to order products",log);
							}
						}
						else
						{
							Fail("PDP page not displayed");
						}
					}
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	/*HUDSONBAY-568 Verify that on selecting "Variable" product (price varying on sizes) then the product with various range of products will be shown.*/
	public void pdp_priceVaryProduct() throws java.lang.Exception
	{
		ChildCreation("HUDSONBAY-568 Verify that on selecting 'Variable' product (price varying on sizes) then the product with various range of products will be shown.");
		String key = HBCBasicfeature.getExcelVal("HB568", sheet, 1);
		ArrayList<String> reg = null;
		List<List<String>> regSale = new ArrayList<List<String>>();

		try
		{
			wait.until(ExpectedConditions.visibilityOf(searchIcon));
			if(HBCBasicfeature.isElementPresent(searchIcon))
			{
				HBCBasicfeature.scrollup(searchIcon, driver);
				searchIcon.click();
				wait.until(ExpectedConditions.visibilityOf(searchBox));
				if(HBCBasicfeature.isElementPresent(searchBox))
				{
					searchBox.sendKeys(key);
					searchBox.sendKeys(Keys.ENTER);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(PDPPage));
					if(HBCBasicfeature.isElementPresent(PDPPage))
					{
						log.add("PDP Page displayed");
						wait.until(ExpectedConditions.visibilityOf(pdppageSizeContainer));
						HBCBasicfeature.scrollup(pdppageSizeContainer, driver);
						if(HBCBasicfeature.isElementPresent(pdppageSizeContainer))
						{
							int size = pdppageSizeContainerList.size();
							if(size>1)
							{
								for(int i=1;i<=size;i++)
								{
									reg = new ArrayList<>();
									WebElement ssize = driver.findElement(By.xpath("//*[contains(@class,'pdpSizeSelect pdpSelectEle')]["+i+"]"));
									ssize.click();
									if(HBCBasicfeature.isElementPresent(pdppagePriceContainer))
									{	
										reg.add(pdppagePriceRegular.getText());
										//reg.add(pdppagePriceSale.getText()); Need to enable when both reg and sale prices are displayed
										regSale.add(reg);
									}
								}
								int ssize =  regSale.size();
								for(int j=0;j<ssize;j++)
								{
									log.add("Displayed price values are: "+regSale.get(j));
									for(int k=0;k<ssize;k++)
									{
										if(j==k)
										{
											continue;
										}
										else
										{
											if(regSale.get(j).equals(regSale.get(k)))
											{
												Fail("on selecting 'Variable' product (price varying on sizes) then the product with various range of products not shown",log);
											}
											else
											{
												Pass("on selecting 'Variable' product (price varying on sizes) then the product with various range of products are shown",log);													
											}
										}											
									}
								}
							}
							else
							{
								Skip("More than one size not available for this product");
							}
						}								
						else
						{
							Fail("PDP Size container not displayed for the product");
						}
					}
					else
					{
						Fail("PDP page not displayed");
					}
				}
				else
				{
					Fail("Search box not displayed");
				}
			}
			else
			{
				Fail("Search Icon not displayed");
			}							
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}			
	}
	
	/*HUDSONBAY-566 Verify that on selecting "Collective/ Master" product then the product with price range and its sub products will be shown.*/
	public void pdp_CollectiveProduct() throws Exception
		{
			ChildCreation("HUDSONBAY-566 Verify that on selecting 'Collective/ Master' product then the product with price range and its sub products will be shown.");
			String key = HBCBasicfeature.getExcelNumericVal("HB566", sheet, 1);
			try
			{
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				if(HBCBasicfeature.isElementPresent(searchIcon))
				{
					HBCBasicfeature.scrollup(searchIcon, driver);
					searchIcon.click();
					wait.until(ExpectedConditions.visibilityOf(searchBox));
					if(HBCBasicfeature.isElementPresent(searchBox))
					{
						searchBox.sendKeys(key);
						searchBox.sendKeys(Keys.ENTER);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(PDPPage));
						if(HBCBasicfeature.isElementPresent(PDPPage))
						{
							log.add("PDP Page displayed");
							if(HBCBasicfeature.isElementPresent(collectionProductContianer))
							{
								HBCBasicfeature.scrolldown(collectionProductContianer, driver);
								int size = collectionProductList.size();
								int ctr= 0;
								for(int i=1;i<=size;i++)
								{
									log.add("The sub product "+i+" details are below");
									WebElement prod = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]"));
									HBCBasicfeature.scrolldown(prod, driver);
									String prodTitle = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='skMob_pdpBundleItemTitle']")).getText();
									log.add("The Product title is: "+prodTitle);
									Thread.sleep(500);
									String prodColor = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='pdpColorContainer']//*[@class='selectedColor']")).getText();
									log.add("The Product Color is: "+prodColor);
									Thread.sleep(500);
									String prodSize = driver.findElement(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@id='pdpSizeContainer']//div")).getAttribute("value");
									log.add("The Product Size is: "+prodSize);
									Thread.sleep(500);
									List<WebElement> priceSize = driver.findElements(By.xpath("//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdpPriceContent']//*[@class='skmob_priceValue ']"));
									int pSize = priceSize.size();
									for(int j=1;j<=pSize;j++)
									{
										String price = driver.findElement(By.xpath("(//*[@class='skMob_pdpBundleItemCont']["+i+"]//*[@class='pdpPriceContent']//*[@class='skmob_priceValue '])["+j+"]")).getText();
										log.add("Displayed price values are "+price);
									}
									ctr++;
								}	
								if(ctr==size)
								{
									Pass("On selecting 'Collective/ Master' product then the product with price range and its sub products are shown",log);
								}
								else
								{
									Fail("On selecting 'Collective/ Master' product then the product with price range and its sub products are not shown",log);
								}
							}
							else
							{
								Fail("Collection Product container not displayed");
							}								
						}
						else
						{
							Fail("PDP page not displayed");
						}
					}
					else
					{
						Fail("Search box not displayed");
					}
				}
				else
				{
					Fail("Search Icon not displayed");
				}				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong. Please Check." + e.getMessage());
			}			
		}
	
	
	
	
	}
