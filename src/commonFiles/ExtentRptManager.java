package commonFiles;

import java.io.File;
import java.util.ArrayList;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.NetworkMode;

import hbConfig.HBConstants;

public class ExtentRptManager 
{
	public static ExtentReports extent;
	public static ExtentTest parent,child;
	public ArrayList<String> log = new ArrayList<>();
	
	@BeforeSuite
	public static ExtentReports completeReport()
	{
	 if(extent == null)
	 {
		 String rptPath = HBConstants.repFileLoc;
		 extent = new ExtentReports(rptPath, false, NetworkMode.ONLINE);
		 extent.loadConfig(new File(HBConstants.extentRepConfigfile));
	 }
	 return extent;
	}
	
	
}
