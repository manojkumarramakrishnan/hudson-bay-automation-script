package commonFiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import hbConfig.HBConstants;

public class HBCBasicfeature 
{
	public static WebDriver driver;
	public static WebDriverWait wait;
	public ExtentTest parent;
	public ExtentTest child;
	public static HSSFWorkbook workbook;
	public static HSSFSheet sheet;
	public static File BNExcelSrc;
	
	
	// Driver Initialization
	public HBCBasicfeature(WebDriver driver)
	{
		HBCBasicfeature.driver = driver;
	}
	
	//Mobile Emulation Setup  
	public static WebDriver setMobileView()
	{
		WebDriver driver = null;
		String chromeexePath = HBConstants.chromeexePath;
		System.setProperty("webdriver.chrome.driver",chromeexePath);
	    Map<String, String> mobileEmulation = new HashMap<String, String>();
		mobileEmulation.put("deviceName", "Apple iPhone 6");
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		//capabilities.setCapability("chrome.switches", Arrays.asList("--disable-extensions"));
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		if(driver==null)
		 {
		//Nexus	Dimension d = new Dimension(375,725);
			Dimension d = new Dimension(392,760);
			driver = new ChromeDriver(capabilities);
			driver.manage().window().setSize(d);
			driver.manage().deleteAllCookies();
			int count = 1;
			do
			{
				try
				{
					driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
					driver.get(HBConstants.mobileSiteURL);
					//loginCred();
					//wait = new WebDriverWait(driver, 15);
					break;
				}
				catch (Exception e)
				{
					driver.navigate().refresh();
					//Child.log(LogStatus.FATAL, "The browser is but there is something wrong with the URL. Please Check." + e.getMessage());
					//ExtentRptManager.completeReport().endTest(Child);
					System.out.print("There is something wrong with the URL Please Check." + e.getMessage());
					count++;
				}
			}while(count<5);
			
			/*try
			{
				driver.get(HBConstants.mobileSiteURL);
				//loginCred();
				wait = new WebDriverWait(driver, 15);
			}
			catch (Exception e)
			{
				//Child.log(LogStatus.FATAL, "The browser is but there is something wrong with the URL. Please Check." + e.getMessage());
				//ExtentRptManager.completeReport().endTest(Child);
				System.out.print("There is something wrong with the URL Please Check." + e.getMessage());
			}*/
		}
		return driver;
	}
	
	public static WebDriver Wait()
	{
		wait = new WebDriverWait(driver, 30);
		return driver;
	}
	
	/*public static WebDriver setMobileView()
	{
		WebDriver driver = null;
		String chromeexePath = "E:\\ChromeDriver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",chromeexePath);
	    Map<String, String> mobileEmulation = new HashMap<String, String>();
	    //mobileEmulation.put("deviceName", "Google Nexus 5");
		mobileEmulation.put("deviceName", "Apple iPhone 6");
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		//capabilities.setCapability("chrome.switches", Arrays.asList("--disable-extensions"));
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		if(driver==null)
		 {
			//Dimension d = new Dimension(375,725);
			Dimension d = new Dimension(392,760);
			driver = new ChromeDriver(capabilities);
			driver.manage().window().setSize(d);
			driver.manage().deleteAllCookies();
			try
			{
				driver.get(HBConstants.mobileSiteURL);
				wait = new WebDriverWait(driver, 60);
			}
			catch (Exception e)
			{
				System.out.print("There is something wrong with the URL Please Check." + e.getMessage());
			}
		}
		return driver;
	}
	*/
	/* Excel File Setup */
	public static HSSFWorkbook excelsetUp() throws IOException
	{
		File BNExcelSrc = new File(HBConstants.commonExcelFile);
		FileInputStream fis = new FileInputStream(BNExcelSrc);
		HSSFWorkbook workbook = new HSSFWorkbook(fis);
		return workbook;
	}
	
	/* Excel File Setup */
	public static HSSFSheet excelsetUp(String sheetName) throws IOException
	{
		File BNExcelSrc = new File(HBConstants.commonExcelFile);
		FileInputStream fis = new FileInputStream(BNExcelSrc);
		workbook = new HSSFWorkbook(fis);
		sheet = workbook.getSheet(sheetName);
		return sheet;
	}
	
	// Check If Element Present
	public static boolean isElementPresent(WebElement element)
	{
		try
		{
			element.isDisplayed();
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	// Check If List Element Present	
	public static boolean isListElementPresent(List<WebElement> element)
	{
		try
		{
			if(element.size()>0)
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	}
		
	
	//JS Script to Click Element
	public static void jsclick(WebElement element, WebDriver driver)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);	
	}
	
	// Color Code Converter FOR BORDER
	public static String colorfinder(String csvalue)
	{
		String ColorName = csvalue.replace("1px solid", "");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		return hexCode;
	}
	
	// Scroll Down to Element
	public static void scrolldown(WebElement element,WebDriver driver)
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);",element);
	}
	
	// Scroll Up to Element
	public static void scrollup(WebElement element,WebDriver driver)
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);",element);
	}
	
	// Check Broken Image 
	public static int imageBroken(WebElement image,ArrayList<String> log) throws ClientProtocolException, IOException
	{
		HttpClient client = HttpClientBuilder.create().build();
		/*System.out.println(image);
		System.out.println(image.getAttribute("src"));*/
		HttpGet get = new HttpGet(image.getAttribute("src"));
		/*String user = "bnuser";
		String pass = "bn123";
		String authStr = user + ":" + pass;
		byte[] authEncBytes = Base64.encodeBase64(authStr.getBytes());
		String utf8 = new String(authEncBytes,"UTF-8");
		get.setHeader("authorization", "Basic " + utf8)*/;
		HttpResponse response = client.execute(get);  
		log.add("The Verified Image URL : " + image.getAttribute("src") + " and its Response Code is : " + response.getStatusLine().getStatusCode());
		return response.getStatusLine().getStatusCode();
	}
	
	//Write in Excel File
	public static void writeInExcel(HSSFSheet sheet, String id,int row, int cellVal) throws Exception
	{
		try
		{
			File BNExcelSrc = new File(HBConstants.commonExcelFile);
			HSSFCell val = sheet.getRow(row).createCell(cellVal);
			val.setCellType(Cell.CELL_TYPE_STRING);
			val.setCellValue(id);
			System.out.println(id);
			FileOutputStream fos = new FileOutputStream(BNExcelSrc);
			FileInputStream fis = new FileInputStream(BNExcelSrc);
			workbook = new HSSFWorkbook(fis);
			workbook.write(fos);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	// Get Value from the Excel Sheet
	public static String getExcelVal(String tcId, HSSFSheet sheet, int cellVal) throws Exception
	{
		String searchvalue =""; 
		try
		{
			int brmkey = sheet.getLastRowNum();
			for(int i = 0; i <= brmkey; i++)
			{
				String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
				if(cellCont.equals(tcId))
				{
					searchvalue = sheet.getRow(i).getCell(cellVal).getStringCellValue();
					break;
				}
				else
				{
					continue;
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		return searchvalue;
	}
	
	// Get Numeric Value from the Excel Sheet
	public static String getExcelNumericVal(String tcId, HSSFSheet sheet, int cellVal) throws Exception
		{
			String searchvalue =""; 
			try
			{
				int brmkey = sheet.getLastRowNum();
				for(int i = 0; i < brmkey; i++)
				{
					String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
					if(cellCont.equals(tcId))
					{
						DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
						HSSFCell cell = sheet.getRow(i).getCell(cellVal);
						cell.setCellType(Cell.CELL_TYPE_STRING);
						searchvalue = formatter.formatCellValue(cell).toString();
					}
					else
					{
						continue;
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
			return searchvalue;
		}
	
	// Get the Link Status Code
	public static int linkBroken(WebElement link,ArrayList<String> log) throws ClientProtocolException, IOException
	{
		String urllink = "";
		if(link.getAttribute("href")!=null)
		{
			  /*urllink = link.getAttribute("href");
			  //System.out.println(urllink);
			  HttpClient Client = HttpClientBuilder.create().build();
			  //HttpGet Request = new HttpGet(link.getAttribute("href"));
			  HttpGet Request = new HttpGet(urllink);
			  HttpResponse Response = Client.execute(Request);
			  System.out.println(Response.getStatusLine().getStatusCode());
			  log.add("The Current Product URL : " + urllink + " and it response is : " + Response.getStatusLine().getStatusCode());
			  return Response.getStatusLine().getStatusCode();*/
			
			  urllink = link.getAttribute("href");
			  HttpClient Client = HttpClientBuilder.create().build();
			  String user = "bnuser";
			  String pass = "bn123";
			  String authStr = user + ":" + pass;
			  byte[] authEncBytes = Base64.encodeBase64(authStr.getBytes());
			  String utf8 = new String(authEncBytes,"UTF-8");
			  HttpGet get = new HttpGet(urllink);
			  get.setHeader("authorization", "Basic " + utf8);
			  HttpResponse response = Client.execute(get);  
			  //HttpGet Request = new HttpGet(link.getAttribute("href"));
			  //System.out.println(response.getStatusLine().getStatusCode());
			  log.add("The Current Product URL : " + urllink + " and it response is : " + response.getStatusLine().getStatusCode());
			  return response.getStatusLine().getStatusCode();
		 }
		else
		{
			log.add("The Current Product URL : " + urllink);
			System.out.println(urllink);
			return 404;
		}
	}
}
