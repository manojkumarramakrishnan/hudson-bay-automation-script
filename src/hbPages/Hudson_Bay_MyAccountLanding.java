package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.junit.AfterClass;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_MyAccountLanding_Obj;
import commonFiles.ExtentRptManager;

public class Hudson_Bay_MyAccountLanding extends ExtentRptManager
{
	  public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	
	 @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	
	  @Test
	  public void Hudson_Bay_911_MyAccountlanding() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("MyAccount");
		  Hudson_Bay_MyAccountLanding_Obj MyAccount = new Hudson_Bay_MyAccountLanding_Obj(driver,sheet);
		
		  MyAccount.signin();
		  MyAccount.MyAccountpageCreative();
		  MyAccount.MyAccountpageSections();
		  MyAccount.MyAccountpageDefaultView();
		  MyAccount.MyAccountpageWelcomeString();
		  MyAccount.MyAccountpageSignOutLink();
		  MyAccount.MyAccountpageEditProfile();
		  MyAccount.MyAccountpageSeparationSectionLine();
		  MyAccount.MyAccountpageEditProfilePage();
		  MyAccount.MyAccountpageToMyAddressBook();		  
		  MyAccount.MyAccountpageToNotificationPreferences();
		  MyAccount.MyAccountpageToMyOrders();
		  MyAccount.MyAccountpageToMyWishList();
		  MyAccount.MyAccountpageSignOut();
		  
		  
	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterClass
		public void afterClass() 
		{
		  driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}
	 	
	  @AfterTest
	  public void afterTest() 
	  {
		driver.close();
	  }

}


