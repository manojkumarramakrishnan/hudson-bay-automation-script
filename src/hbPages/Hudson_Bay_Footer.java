package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.ExtentRptManager;
import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_Footer_Obj;

public class Hudson_Bay_Footer extends ExtentRptManager
{
	  public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	
	 @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	
	  @Test
	  public void Hudson_Bay_27_Footer_Implementation() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("Footer");
		  Hudson_Bay_Footer_Obj footer = new Hudson_Bay_Footer_Obj(driver,sheet);
		 
		  footer.footerCreative();
		  footer.hereToHelp();
		  footer.menuShopHBC();
		  footer.shopHBCClick();
		  footer.shopHBCSection();
		  footer.shopHBCSectionLinkSelection();
		  footer.menuStoreCorp();
		  footer.storeCorplick();
		  footer.storeCorpSection();
		  footer.storeCorpSectionLinksSelection();
		  footer.menuCustomerPolicy();
		  footer.CustomerPolicylick();
		  footer.CustomerPolicySection();
		  footer.CustomerPolicySectionLinkSelection();
		  footer.menuHBCReward();
		  footer.HBCRewardsClick();
		  footer.HBCRewardsSection();	
		  footer.HBCRewardsSectionLinksSelection();
		  footer.socialShareSection();
		  footer.socialShareIcons();
		  footer.shareFB();
		  footer.sharePIN();
		  footer.shareInsta();
		  footer.shareTwitter();
		  footer.shareYoutube();
		  footer.footerCopyright();		  		  
		  footer.footerStatic();
		  
		  //French Site cases
		  footer.menuMagasinsSp�cialis�s();
		  footer.MagasinsSp�cialis�sClick();
		  footer.MagasinsSp�cialis�Section();
		  footer.MagasinsSp�cialis�sSectionLinkSelection();
		  footer.menuMagasinsEntreprises();
		  footer.MagasinsEntreprisesClick();
		  footer.MagasinsEntreprisesSection();
		  footer.MagasinsEntreprisesSectionLinksSelection();
		  footer.menuservice�laclient�leetpolitique();
		  footer.service�laclient�leetpolitiqueClick();
		  footer.service�laclient�leetpolitiqueSection();
		  footer.service�laclient�leetpolitiqueSectionLinkSelection();
		  footer.menur�compenseshbc();
		  footer.r�compenseshbcClick();
		  footer.r�compenseshbcSection();
		  footer.r�compenseshbcSectionLinksSelection();
		  
		  
		  //footer.signupEmails();

	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterTest
		public void afterClass() 
		{
		  driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}

}


