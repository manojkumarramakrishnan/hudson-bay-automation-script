package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_PDP_Obj;
import commonFiles.ExtentRptManager;

public class Hudson_Bay_PDP extends ExtentRptManager
{
	public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	  	
	 @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	
	  @Test
	  public void Hudson_Bay_39_PDP() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("PDP");
		  Hudson_Bay_PDP_Obj pdp = new Hudson_Bay_PDP_Obj(driver,sheet);
		  
		  pdp.subcatToPdp();
		  pdp.pdpHeader();
		  pdp.pdpVertical_Scroll();
		  pdp.pdpFooter();
		  pdp.pdpProduct_Title();
		  pdp.pdpProduct_FavIcon("HBC528");
		  pdp.pdpProduct_FavIcon("HBC529");
		  pdp.pdpProduct_Image();
		  pdp.pdpProduct_ImageDefault();
		  pdp.pdpProduct_ImageAlternate();		  
		  pdp.pdp_RatingReview();			  
		  pdp.pdpColorSwatches_Tile();
		  pdp.pdpColorSwatches();		  
		  pdp.pdpChangeColor_Swatches();
		  pdp.pdpColorSize();
		  pdp.pdpColor_Creative();	
		  pdp.pdpColorSwatches_MoreTiles();
		  pdp.pdpColorSwatches_MoreTilesColor();
		  pdp.pdpSize_Creative(); 
		  pdp.pdpSize_Guide();
		  pdp.pdpSize_GuidePage();
		  pdp.pdpSize_SelectSize();
		  pdp.pdpSize_SelectedDefault();		 
		  pdp.pdpSize_Color();
		  pdp.pdp_selectMultipleSizeColor();
		  pdp.pdpPage_price();
		  pdp.pdpQuantityContainer();
		  pdp.PDPQuantity_default();
		  pdp.pdpQuantityIncrement();
		  pdp.pdpQuantityDecrement();		
		  pdp.pdpMaximumQuantity();		 
		  //pdp.pdp_SocialIcons();
		  pdp.pdp_Accordians();		  
		  pdp.pdp_sectionArrangements();
		  
		  pdp.pdp_AccordExpand();
		  pdp.pdp_AccordCollpsed();
		  pdp.pdpProductDesc_ExpandCollpase();
		  pdp.pdpProdDesc_Creative();
		  pdp.pdp_AccordCls();
		  
		  pdp.pdp_AddToBag();
		  pdp.pdp_AddToBagClick();
		  pdp.pdp_QuantityCount();		  
		  pdp.pdp_callToProduct_noATB();
		  pdp.pdp_callToProduct();
		  pdp.pdp_priceVaryProduct();
		  pdp.pdp_CollectiveProduct();
		  pdp.pdp_videoProduct();
		  /*pdp.pdp_videoShare();
		  pdp.pdp_videoFullScreen();*/
		  pdp.pdp_relatedVideo();


		  
		  /*Removed features
		   	pdp.pdp_ProductZoom();
		  	pdp.pdp_productNoScroll();
		  	pdp.pdp_ZoomClick();*/	 
		  	
		  
		  
		  
		  	  
	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterTest
		public void afterClass() 
		{
		  driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}

}


