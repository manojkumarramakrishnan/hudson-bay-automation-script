package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_CartPage_Obj;
import commonFiles.ExtentRptManager;

public class Hudson_Bay_CartPage extends ExtentRptManager
{
	  public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	
	 @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	
	  @Test
	  public void Hudson_Bay_895_CartPage() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("CartPage");
		  Hudson_Bay_CartPage_Obj cart = new Hudson_Bay_CartPage_Obj(driver,sheet);
		
		  cart.cartBagIconClick();
		  cart.cartEmptyShopNowButton();
		  cart.cartEmptyShopNowButtonClick();
		  cart.cartAddedProductsInBag();
		  cart.cartPageCreative();
		  cart.cartShoppingBagtitle();
		  cart.cartPageHeader();
		  cart.cartPageFooter();
		  cart.cartPageNavigation();
		  cart.cartProductDetails();
		  cart.cartProductRemoveIcon();
		  cart.cartProductQuantity();
		  cart.cartProdPriceDifference();
		  cart.cartProdIncrementDecrement();
		  cart.cartQuanitySelection();
		  cart.cartQuantityMaximum();
		  cart.cartProdImageTitleClick();
		  cart.cartPromoCodeField();
		  cart.cartInvalidPromoAlert();
		  cart.cartEstimatedShippingandTaxes();
		  cart.cartZipShippingMethodFields(); 
		  cart.cartEmptyZipcodeAlert();
		  cart.cartInvalidZipcodeAlert();
		  cart.cartDefaultDeliveryOption();
		  cart.cartShippingMethodChange();
		  cart.cartProductEstimatedShippingandTaxes();
		  cart.cartQuantityPriceUpdates();
		  cart.cartShoppingBagCount();
		  cart.cartCheckoutPaypal();
		  cart.cartRemoveProduct();	
		  cart.cartEmptyBagButton();
		  
		  
	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterClass
		public void afterClass() 
		{
			driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}
	 	
	 

}


