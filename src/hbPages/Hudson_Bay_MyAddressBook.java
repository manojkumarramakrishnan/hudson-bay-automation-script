package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_MyAddressBook_Obj;
import commonFiles.ExtentRptManager;

public class Hudson_Bay_MyAddressBook extends ExtentRptManager
{
	  public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	
	 @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	
	  @Test
	  public void Hudson_Bay_913_MyAddressBook() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("MyAccount");
		  Hudson_Bay_MyAddressBook_Obj MyAddress = new Hudson_Bay_MyAddressBook_Obj(driver,sheet);
		
		  MyAddress.signin();
		  MyAddress.MyAddressBookpageCreative("HBC2114");
		  MyAddress.MyAddressBookpageCreative("HBC2072");
		  MyAddress.MyAddressBookBackArrow("HBC2115");
		  MyAddress.MyAddressBookBackArrow("HBC2073");
		  MyAddress.MyAddressBookBackArrowClick();
		  MyAddress.MyAddressBookAddNewButton();
		  MyAddress.MyAddressBookRemoveButton();
		  MyAddress.MyAddressBookAddNewClick();
		  MyAddress.MyAddressBookAddNewAddressCreative("HBC2131");
		  MyAddress.MyAddressBookAddNewAddressCreative("HBC2089");
		  MyAddress.MyAddressBookEmptyValidation();
		  MyAddress.MyAddressBookEnterDetails("HBC2135");
		  MyAddress.MyAddressBookEnterDetails("HBC2093");
		  MyAddress.MyAddressBookFirstLastNameCharValidation();
		  MyAddress.MyAddressBookFirstLastNameValidationAlert();
		  MyAddress.MyAddressBookStreetAddressFields();
		  MyAddress.MyAddressBookCityField();
		  MyAddress.MyAddressBookCityFieldAlert();
		  MyAddress.MyAddressBookCityFieldExceedLimitAlert();
		  MyAddress.MyAddressBookCountryFields();
		  MyAddress.MyAddressBookProvinceField();
		  MyAddress.MyAddressBookProvinceFieldSelectDefault();
		  MyAddress.MyAddressBookProvincetextBoxUK(); 
		  MyAddress.MyAddressBookProvinceDropDownUS();
		  MyAddress.MyAddressBookZipCodeUS();
		  MyAddress.MyAddressBookZipCodeValidation();
		  MyAddress.MyAddressZipCodeFieldAlert();
		  MyAddress.MyAddressZipCodeFieldMinAlert();
		  MyAddress.MyAddressPostalAlphaNumericValidation();
		  MyAddress.MyAddressPostalMaxCharacters();
		  MyAddress.MyAddressBookPostalCodeFieldAlert();
		  MyAddress.MyAddressPhoneNumberField(); 
		  MyAddress.MyAddressPhoneNumberExtField();
		  MyAddress.MyAddressBookSubmitButtonCreative();
		  MyAddress.MyAddressBookCancelButtonCreative();
		  MyAddress.MyAddressBookAddNewAddressCancel();
		  MyAddress.MyAddressBackAccountClickNoUpdate();
		  MyAddress.MyAddressBookUpdateClick();
		  MyAddress.MyAddressNoDelete(); 
		  MyAddress.MyAddressBookDeleteAddress(); 
		  
	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterTest
		public void afterClass() 
		{
		  driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}


}


