package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_sortFilter_Obj;
import commonFiles.ExtentRptManager;

public class Hudson_Bay_SortFilter extends ExtentRptManager
{
	public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	  
	
	 @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	
	  @Test
	  public void Hudson_Bay_621_sort() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("Sort");
		  Hudson_Bay_sortFilter_Obj sort = new Hudson_Bay_sortFilter_Obj(driver,sheet);
		 
		  sort.plp();
		  sort.plpSort_Refine();
		  sort.plpRefine_HeaderDisabled();
		  sort.plpRefine_Apply();
		  sort.plpRefine_ClearAll();
		  sort.plpRefine_close();
		  sort.filterOptions();
		  sort.sortOptionSelected();
		  sort.sortOptionChange();
		  sort.sortOptionTick();
		  sort.sortOptionBold();
		  sort.sortOptionUnselected();
		  sort.sort_not_selected(); 
		  sort.sortApply();
		  sort.selectedSortName();		  
		  sort.upDownArrow();
		  sort.DownArrowClosedState();
		  sort.sort_Filter_Scroll();
		  sort.colorDifference();
		  sort.UpArrowOpenedState();
		  sort.FilterViewAll();
		  sort.filterCountEmpty();
		  sort.FilterViewAllDisabled();
		  sort.Filter_not_selected();
		  sort.Filter_ProductCount();
		  sort.filterClearAll();
		  sort.frenchStrings();		  
		  sort.plpRefine_CloseClick();		
		  
		  //sort.ClearAll_no_close();(N/A)

		  
		  
		  
		  	  
	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterTest
		public void afterClass() 
		{
		  driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}

}


