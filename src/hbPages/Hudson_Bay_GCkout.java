package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.ExtentRptManager;
import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_GC_Obj;

public class Hudson_Bay_GCkout extends ExtentRptManager
{
  public static ExtentReports extent = ExtentRptManager.completeReport();
  public static ExtentTest parent,child;
  public ArrayList<String> log = new ArrayList<>();
  WebDriver driver;
  HSSFSheet sheet;
  
	
  @BeforeClass
  public void beforeClass() 
  {
	  driver = HBCBasicfeature.setMobileView();
  }
	
  @Test
  public void Hudson_Bay_871_Guest_Checkout() throws Exception 
  {		  
	  HSSFSheet sheet = HBCBasicfeature.excelsetUp("GuestCheckout");
	  Hudson_Bay_GC_Obj GC = new Hudson_Bay_GC_Obj(driver,sheet);
	  
	  GC.HBC1172AddCartItems();
	  GC.HBC1173GuestAddressPage();
	  // Comment Below //
	  GC.HBC1174GuestCheckoutEle();
	  GC.HBC1175Addressfields();
	  GC.HBC1182StAddFields();
	  GC.HBC1219BillingTermandCondition();
	  GC.HBC1220EmailDisclaimerDefText();
	  GC.HBC1221SameEmailDefaultChecked();
	  GC.HBC1218AddressfieldsEmptyValidation();
	  GC.HBC1181FNLLNPlaceHolderText();
	  GC.HBC1183AllFieldsPlaceHolderText();
	  GC.HBC1177FNameEnterMax();
	  GC.HBC1178LNameEnterMax();
	  GC.HBC1179FNLNameNumValidation();
	  GC.HBC1180FNLNameNumSplCharValidation();
	  GC.HBC1184StAddFieldsLenVal();
	  GC.HBC1185CityEnterable();
	  GC.HBC1186CityyValidation();
	  GC.HBC1197BillingCountryOptions();
	  GC.HBC1198BillingCAStates();
	  GC.HBC1199BillingCADefaultStateSel();
	  GC.HBC1187CountrySelection();
	  GC.HBC1207BillingCAPOLength();
	  GC.HBC1208BillingCAPOLengthValidationAlert();
	  GC.HBC1209BillingCAPOAlphaNumericValidation();
	  GC.HBC1210BillingCAPHNumLenValidation();
	  GC.HBC1211BillingCAPHAphaNumericValidationAlert();
	  GC.HBC1212BillingCAExtNumLenValidation();
	  GC.HBC1213BillingCARewardFldLenValidation();
	  GC.HBC1214BillingCARewardLenValidationAlert();
	  GC.HBC1215BillingCARewardAlphaNumericValidationAlert();
	  GC.HBC1217BillingCAEmailValidationAlert();
	  GC.HBC1216BillingValidEmail();
	  GC.HBC1189UKUSCountrySelectionChkdBoxState();
	  GC.HBC1188UKCountrySelection();
	  GC.HBC1200BillingUKStateText();
	  GC.HBC1201BillingUSCountrySel();
	  GC.HBC1190USCountryFields();
	  GC.HBC1202BillingUSZipCode();
	  GC.HBC1205BillUSPOLessLenValidation();
	  GC.HBC1203BillingUSZipLength();
	  GC.HBC1204BillinUSPOValidation();
	  GC.HBC1206BillinUSAlphaNumericValidation();
	  GC.HBC1224ShippingAddressExpand();
	  GC.HBC1195ShippingAddressFields();
	  GC.HBC1225ShippingAddressFields();
	  GC.HBC1231StAddFields();
	  GC.HBC1196ShippingAddressFieldsTabFocus();
	  GC.HBC1244ShippingAddressfieldsEmptyValidation();
	  GC.HBC1226ShippingAddressFNLenVal();
	  GC.HBC1227ShippingAddressLNLenVal();
	  GC.HBC1230FNLNameTitleCase();
	  GC.HBC1228ShippingAddressFVLNCharVal();
	  GC.HBC1229FNLNameNumSplCharValidation();
	  GC.HBC1232StAddFieldsLenVal();
	  GC.HBC1233CityEnterable();
	  GC.HBC1234CityyValidation();
	  //GC.HBC1235ShippingDisplayedCountry();
	  GC.HBC1236ShippingCADefaultStateSel();
	  GC.HBC1237ShippingCAPOAlphaNumericValidation();
	  GC.HBC1238ShippingCAPOLength();
	  GC.HBC1239ShippingCAPOLengthValidationAlert();
	  GC.HBC1240ShippingCAPOAlphaNumericValidation();
	  GC.HBC1241ShippingCAPHNumLenValidation();
	  GC.HBC1243ShippingCAEmptyPHValidationAlert();
	  GC.HBC1242ShippingCAPHAphaNumericValidationAlert();
	  GC.HBC1245BillShipHtmlValidation();
	  GC.HBC1223BillingAddressNoUpdate();
	  GC.HBC1194ShippingAddAddress(); 
	  
	  // Comment Above //
	  
	  GC.HBC1246BillingAddAddAddressVerification();
	  GC.HBC1247PayAndShipPgTabCreative();
	  GC.HBC1399ShippingPaymentCreative();
	  GC.HBC1248TabState();
	  GC.HBC1249ShipBillingSections();
	  GC.HBC1250ShipSectionsEditCreateLinks();
	  GC.HBC1294ShipSectionsEditCreateUnderLined();
	  GC.HBC1251ShipSectionsShipToNMEditLinkDet();
	  GC.HBC1258EditAddressOverlayClose();
	  GC.HBC1254AddressModifiySave();
	  GC.HBC1259ShipPageCreateShipOverlay();
	  GC.HBC1261ShipPageCreateShipOverlayFields("HBC1261");
	  GC.HBC1261ShipPageCreateShipOverlayFields("HBC1264");
	  GC.HBC1270ShipPageCreateShipStreetAddressFields();
	  GC.HBC1275ShipPageCreateShipStreetCountryDDSizes();
	  GC.HBC1276ShipPageCreateShipDefaultCountry();
	  GC.HBC1277ShippingCADefaultStateSel();
	  GC.HBC1287ShipPageEmptyAddValidation();
	  GC.HBC1262ShipPageCreateAddressEmptyVal();
	  GC.HBC1266ShipPageCreateShipOverlayFNLenVal();
	  GC.HBC1267ShipPageCreateShipOverlayLNLenVal();
	  
	  GC.HBC1268FNLNameNumValidation();
	  GC.HBC1274FNLEmptySpaceValidation();
	  GC.HBC1288ShipPageAddShippAddressHTMLValidation();
	  GC.HBC1271StAddFieldsLenVal();
	  GC.HBC1272CityEnterable();
	  GC.HBC1279ShippingCAPOLength();
	  GC.HBC1278ShippingZipAlphaNumericValidation();
	  GC.HBC1280ShippingCAPOLengthValidationAlert();
	  GC.HBC1281ShippingCAPOAlphaNumericValidationAlert();
	  GC.HBC1286ShippingCAPHEmptyValidationAlert();
	  GC.HBC1284ShippingCAPHNumLenValidation();
	  GC.HBC1285ShippingCAPHAphaNumericValidationAlert();
	  GC.HBC1263ShipPageCreateAddressAddShippingAdd();
	  GC.HBC1291ShippingPageShipToDD();
	  GC.HBC1293ShipPageSelectAddFromShipList(); 
	  
	  GC.HBCShipPageAddShippingAddressVerification1();
	  GC.HBCShipPageAddShippingAddressNoMatchAddressVerification2();
	  GC.HBCShipPageAddShippingAddressBuildingNumberVerification3();
	  GC.HBCShipPageAddShippingSuggestedAddressVerification4();
	  GC.HBCShipPageAddShippingPotentialAddressVerification5(); 
	  
	  // Payment Sections
	  GC.HBC1301PaySectionsEditCreateLinks();
	  GC.HBC302PayAddressEditView();
	  GC.HBC1306PayEditAddressOverlayClose();
	  GC.HBC1307PayEditAddressUpdated();
	  GC.HBC1308ShipPayCreateShippingAddress();
	  GC.HBC1310ShipPayCreateShippingAddressFields("HBC-1310");
	  GC.HBC1310ShipPayCreateShippingAddressFields("HBC-1313");
	  GC.HBC1320ShipPageCreatePayStreetAddressFields();
	  GC.HBC1341ShipPageCreatePayPHTextBoxes();
	  GC.HBC1315ShipPageCreatePayOverlayFNLenVal();
	  GC.HBC1316ShipPageCreatePayOverlayLNLenVal();
	  
	  GC.HBC1321StAddFieldsLenVal();
	  GC.HBC1322BillCityEnterable();
	  GC.HBC1318ShipPageCreatePayDefaultCountry();
	  GC.HBC1324BillingCountryOptions();
	  GC.HBC1325BillingCADefaultStateSel();
	  GC.HBC1326ShippingCADefaultStateSel();
	  GC.HBC1333ShippingCAPOAlphaNumericValidation();
	  GC.HBC1335BillingCAPOLengthValidationAlert();
	  GC.HBC1334BillingCAPOLength();
	  GC.HBC1343BillingCAExtNumLenValidation();
	  GC.HBC1344ShipPageBillingValidEmail();
	  GC.HBC1319ShipPageCreateChangeCountry();
	  GC.HBC1327BillingUKStateText();
	  GC.HBC1339PayCAPHNumLenValidation("HBC-1339");
	  GC.HBC1339PayCAPHNumLenValidation("HBC-1342");
	  GC.HBC1340PayCAPHAphaNumericValidationAlert();
	  GC.HBC1347BillingTermandConditionChkBoxState();
	  GC.HBC1311ShipPageCreateShippingAddressEmptyVal("HBC-1311");
	  GC.HBC1311ShipPageCreateShippingAddressEmptyVal("HBC-1346");
	  GC.HBC1317FNLNameNumValidation();
	  GC.HBC1312ShipPageCreateNewBillingAddress(); 
	  
	  
	  // Paypal Credit Card Sections
	  GC.HBC1348PaymentSectionsOptions();
	  GC.HBC1381PaymentHBCrewards();
	  GC.HBC1382PaymentHBCrewardValidation();
	  GC.HBC1383PaymentHBCrewardlenValidation();
	  GC.HBC1385PaymentHBCrewardsApply();
	  GC.HBC1386PaymentHBCrewardsEmptyApply();
	  GC.HBC1388PaymentHBCrewardInvalidPromocode();
	  GC.HBC1387PaymentHBCrewardLenPromocode();
	  GC.HBC1391PaymentShippingandTaxes();
	  GC.HBC1392PaymentOrderTotalHighlight();
	  GC.HBC1393PaymentShippingMethodDropDown();
	  GC.HBC1394PaymentShippingMethodDropDownDefault();
	  GC.HBC1395PaymentShippingMethodDropDownOptions();
	  GC.HBC1396PaymentShippingMethodDropDownOptionChange();
	  GC.HBC1349PaymentSectionsCreditCardEnable();
	  GC.HBC1355PaymentSectionsCreditCardDropdownDefault();
	  GC.HBC1350PaymentSectionsCreditCardFields();
	  GC.HBC1353PaymentSectionsCreditCardImages();
	  GC.HBC1354PaymentSectionsCreditCardAmountToPay();
	  GC.HBC1352PaymentSectionsCreditCardSelection();
	  GC.HBC1356PaymentSectionsCreditCardNumberField();
	  GC.HBC1359PaymentSectionsCreditCardFields();
	  GC.HBC1358PaymentSectionsCreditCardNumberEmptyFieldAlert();
	  GC.HBC1360PaymentSectionsCreditCardNumberInvalidFieldAlert();
	  GC.HBC1357PaymentSectionsCreditCardNumberFieldMaximum();
	  GC.HBC1365PaymentSectionsCreditCardSecurityEmptyFieldAlert();
	  GC.HBC1366PaymentSectionsCreditCardSecurityCodeFieldMaximum();
	  GC.HBC1364PaymentSectionsCreditCardSecurityInvalidFieldAlert();
	  GC.HBC1363PaymentSectionsCreditCardSecurityCode();
	  GC.HBC1368PaymentSectionsCreditCardExpirationMonthYearFormat();
	  GC.HBC1367PaymentSectionsCreditCardExpirationMonthYear();
	  GC.HBC1371PaymentSectionsCreditCardExpirationEmptyFieldAlert();
	  GC.HBC1372PaymentSectionsCreditCardExpirationFieldMaximum();
	  GC.HBC1369PaymentSectionsCreditCardExpirationInvalidFieldAlert();
	  GC.HBC1370PaymentSectionsCreditCardExpirationLessDigitsFieldAlert();
	  GC.HBC1373PaymentSectionsCreditCardExpiredYear();
	  GC.HBC1374PaymentApplyGiftCardLink();
	  GC.HBC1375PaymentSectionsPaybygiftcard();
	  GC.HBC1376PaymentSectionsPaybygiftcard();
	  GC.HBC1377PaymentSectionsApplyGiftPinFieldMaximum();
	  GC.HBC1378PaymentSectionsAddAnotherGiftCard();
	  GC.HBC1379PaymentSectionsAddAnotherGiftCardClick();
	  GC.HBC1380PaymentSectionsPaybycreditcardDeselect();
	  GC.HBC1398PaymentSectionsWhatIsPayPal();
	  GC.HBC1397PaymentSectionsWhatIsPayPalClick();
	  
	  // Review Order Page
	  GC.HBC1400ReviewAndSubmitCreative();
	  GC.HBC1415ReviewHBCEarnedText();
	  GC.HBC1416ReviewSummaryContainerGreyedOut();
	  GC.HBC1401ReviewAndSubmitToPreviousTabs();
	  GC.HBC1403ReviewAndSubmitColumswithEdit();
	  GC.HBC1404ReviewAndSubmitSavedDetails();
	  GC.HBC1412ReviewAndSubmitSavedDetailsCompare();
	  GC.HBC1414ReviewAndSubmitAfterModification();
	  GC.HBC1405ReviewAndSubmitShippingEditToAddressSection();
	  GC.HBC1406ReviewAndSubmitShippingMethodEditToShipPayment();
	  GC.HBC1408ReviewAndSubmitPaymentMethodEditToShipPayment();
	  GC.HBC1411ReviewAndSubmitPriceChangewhenQuantityUpdate();
	  
	  // Place Orders
	  /*GC.HBC1417ReviewAndSubmitPlaceOrderToOrderConfrim();
	  GC.HBC1418OrderConfirmationCreative();
	  GC.HBC1419OrderConfirmationThanks();
	  GC.HBC1420OrderConfirmationNumberDate();
	  GC.HBC1422OrderConfirmationShippingAddress();
	  GC.HBC1423OrderConfirmationBillingAddress();
	  GC.HBC1424OrderConfirmationPaymentdetails();
	  GC.HBC1425OrderConfirmationCreateAccountBtns();
	  GC.HBC1426CreateAccountNavigation();*/
  }
	  
	 	
    @AfterSuite
	public void closeReporter() 
	{
		extent.close();
	}
	
	@BeforeMethod
	public void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	@AfterTest
	public void afterClass() 
	{
	  driver.close();
	}
	
	public void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}

}


