package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_search_Obj;
import commonFiles.ExtentRptManager;

public class Hudson_Bay_Search extends ExtentRptManager
{
	  public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	
	 @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	 
	  @Test
	  public void Hudson_Bay_252_Search() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("Search");
		  Hudson_Bay_search_Obj search = new Hudson_Bay_search_Obj(driver,sheet);
		 
		  search.searchCreative();
		  search.searchInlineText();
		  search.searchXEnabled();
		  search.searchCancel();
		  search.searchEnableAfterPancakeClick(); // Need to remove double hamburger click once issue fixed
		  search.searchKeyword(); //Search suggestion & Stream comparison
		  search.searchClear();
		  search.searchSuggestionSelection();
		  search.searchStringMaintain();
		  search.searchKeywordHistory();
		  search.searchKeywordHistoryCount();
		  search.searchMinimumChar();
		  search.searchHtmlTags();
		  search.searchPageItemCount();		 		  
		  search.search_Sort_Filter();
		  search.search_Sort_separationLine();
		  search.sort_Filter_Count();		  
		  search.searchPage_Details();
		  search.brokenImage();
		  //search.product_rating();
		  search.product_noRating();
		  search.procuct_RegularSalePrice();
		  search.productSplOffer();
		  search.searchPage_Swatches();
		  search.searchPage_SwatchMore();
		  search.swatches_to_PDP();
		  search.SearchPageVerticalScroll();
		  search.searchPageLazyLoad();
		  search.Search_back_To_topCreative();
		  search.search_back_To_topScroll();
		  search.search_back_To_topRightSide();
		  search.search_back_To_top();
		  search.search_back_To_topDisabled();
		  
		  search.Filter_ProductCount();
		  search.filterClearAll();
		  search.Filter_not_selected();
		  search.searchPage_TO_PDP();
		  
		  search.searchRefinePage();
		  search.searchRefine_HeaderDisabled();
		  search.searchRefine_close();
		  search.searchRefine_ClearAll();
		  search.filterOptions();
		  search.sortOption_MultipleSelection();
		  search.sortOptionBlueTick();
		  search.sortOptionBold();
		  search.sortOptionUnselected();
		  search.upDownArrow();
		  search.downArrowClosedState();
		  search.colorDifference();
		  search.upArrowOpenedState();
		  search.FilterViewAll();
		  search.FilterViewAllDisabled();	  
		  search.searchRefine_CloseClick();		 
		 
		  
		  	  
	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterTest
		public void afterClass() 
		{
		  driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}

}


