package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_PDPChanel_Obj;
import commonFiles.ExtentRptManager;

public class Hudson_Bay_PDPChanel extends ExtentRptManager
{
	public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	  	
	 @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	
	  @Test
	  public void Hudson_Bay_642_PDP_Chanel() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("PDP");
		  Hudson_Bay_PDPChanel_Obj pdpChanel = new Hudson_Bay_PDPChanel_Obj(driver,sheet);
		  
		  pdpChanel.ChanelLandingPageCreative();
		  pdpChanel.ChanelPromoBanner();
		  pdpChanel.ChanelCarousels();
		  pdpChanel.ChanelLogo();
		  pdpChanel.ChanelDescription();
		  pdpChanel.ChanelHeroBanners();
		  pdpChanel.ChanelAccordians();
		  pdpChanel.ChanelAccordianNotOpenStateDefault();
		  pdpChanel.ChanelOpenCloseAccordian();
		  pdpChanel.ChanelOpenAccordianSubCatPLP();
		  pdpChanel.ChanelPLPCreative();
		  pdpChanel.ChanelItemCount();
		  pdpChanel.ChanelPLPScroll();
		  pdpChanel.ChanelPLPLazyLoad();
		  pdpChanel.ChanelPLPSortFilter();
		  pdpChanel.ChanelPLPToPDP();
		  pdpChanel.ChanelPDPScroll();
		  pdpChanel.ChanelPDPHeader();
		  pdpChanel.ChanelPDPFooter();
		  pdpChanel.ChanelPDPImage(); 
		  pdpChanel.ChanelPDPDefaultImage();
		  pdpChanel.ChanelPDPFavIcon();
		  pdpChanel.ChanelPDPProdName();
		  pdpChanel.ChanelPDPAccordians();
		  pdpChanel.ChanelPDPDetailsAccordian();
		  pdpChanel.ChanelAccordDefaultState();
		  pdpChanel.ChanelPDPWebID();
		  pdpChanel.ChanelPDP_HBCRewards();
		  pdpChanel.ChanelAccordClose();		  
		  pdpChanel.ChanelPDPPrice(); 
		  pdpChanel.ChanelPDPSwatches();
		  pdpChanel.ChanelPDPColorChange();
		  pdpChanel.ChanelPDPSelectedColor();
		  pdpChanel.ChanelPDPSizeSwatches();
		  pdpChanel.ChanelPDPSizeString();		  
		  pdpChanel.ChanelColorSwatchesDiff();
		  pdpChanel.ChanelpdpQuantityContainer();
		  pdpChanel.ChanelpdpQuantityIncrement();
		  pdpChanel.ChanelpdpQuantityDecrement();
		  pdpChanel.ChanelPDPMaximumQuantity();
		  pdpChanel.ChanelPDPAddToBag();
		  pdpChanel.Chanelpdp_AddToBagClick();
		  
		  
		  //pdpChanel.Chanelpdp_SocialIcons(); 
		  
		  
		  

		  
		  
		  
	
		  
		  	  
	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterTest
		public void afterClass() 
		{
		  driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}


}


