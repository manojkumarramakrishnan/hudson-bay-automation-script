package hbPages;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.HBCBasicfeature;
import hbPageObjects.Hudson_Bay_RegCheckout_Obj;
import commonFiles.ExtentRptManager;

public class Hudson_Bay_RegCheckout extends ExtentRptManager
{
	  public static ExtentReports extent = ExtentRptManager.completeReport();
	  public static ExtentTest parent,child;
	  public ArrayList<String> log = new ArrayList<>();
	  WebDriver driver;
	  HSSFSheet sheet;
	  
	  @BeforeClass
	  public void beforeTest() 
	  {
		  driver = HBCBasicfeature.setMobileView();
	  }
	
	  @Test
	  public void Hudson_Bay_872_CheckoutRegisteredUser() throws Exception 
	  {		  
		  HSSFSheet sheet = HBCBasicfeature.excelsetUp("regchkout");
		  Hudson_Bay_RegCheckout_Obj regchkout = new Hudson_Bay_RegCheckout_Obj(driver,sheet);
		 
		  //*****Shipping Information****	
		  regchkout.signinToChkoutPage();
		  regchkout.ShippingPaymentCreative();
		  regchkout.checkoutPage_Tabs();
		  regchkout.checkoutCurrentSelectedTab();
		  regchkout.ShipBillingSections();
		  regchkout.editShippingAlert();
		  regchkout.editBillingAlert();
		  regchkout.ShippingPageShipToDD();
		  regchkout.ShipSectionsEditCreateLinks();
		  regchkout.ShipSectionsAddressHighlight();		  
		  regchkout.ShipSectionsShipEditLinkOverlay();
		  regchkout.ShipPageCreateShipOverlay();
		  regchkout.ShipPageCreateShipOverlayFields("HBC1470");
		  regchkout.ShipPageCreateShipOverlayFields("HBC1473");
		  regchkout.ShipPageCreateShipOverlayMandatoryFields();
		  regchkout.ShipPageCreateShipOverlayFNLenVal();
		  regchkout.ShipPageCreateShipOverlayLNLenVal();
		  regchkout.FNLNameNumValidation();
		  regchkout.StreetAddFields();
		  regchkout. StAddFieldsLenVal();
		  regchkout.CityEnterable();
		 //regchkout.CityyValidation(); Rework once city alert is displayed
		  regchkout.FNLNameNumSpaceValidation();
		  regchkout.ShipPageCreateShipStreetCountryDDSizes();
		  regchkout.ShipPageCreateShipStreetDefaultCountry();
		  regchkout.ShippingCADefaultStateSel();
		  regchkout.ShippingZipAlphaNumericValidation();
		  regchkout.ShippingCAPOLength();
		  regchkout.ShippingCAPOLengthValidationAlert();
		  regchkout.ShippingCAPOAlphaNumericValidationAlert();
		  regchkout.ShippingCAPHNumLenValidation();
		  regchkout.ShippingCAPHAphaNumericValidationAlert();
		  regchkout.ShippingCAPHEmptyValidationAlert();
		  regchkout.ShipPageEmptyAddValidation();
		  regchkout.ShippingAddressFieldsTabFocus();
		  regchkout.ShipPageAddShippAddressHTMLValidation();
		  regchkout.ShipPageCreateAddressAddShippingAdd();
		  regchkout.ShipSectionsShipToNMEditLinkDet();
		  regchkout.ShipPageSelectAddFromShipList();
		  regchkout.HBCShipPageAddShippingAddressVerification1();
		  regchkout.HBCShipPageAddShippingAddressNoMatchAddressVerification2();
		  regchkout.HBCShipPageAddShippingAddressBuildingNumberVerification3();
		  regchkout.HBCShipPageAddShippingSuggestedAddressVerification4();
		  regchkout.HBCShipPageAddShippingPotentialAddressVerification5();
		  
		  
		 
		 //*****Payment Information***
		  regchkout.PaySectionsEditCreateLinks();
		  regchkout.PaymentSectionsAddressHighlight();
		  regchkout.PayPageCreateAddressAddShippingAdd();
		  regchkout.PayAddressEditView();
		  regchkout.PayEditAddressOverlayClose();
		  regchkout.PayCreateShippingAddress();
		  regchkout.PayCreateShippingAddressFields("HBC1522");
		  regchkout.PayCreateShippingAddressFields("HBC1525");
		  regchkout.PaymentPageCreateShipOverlayMandatoryFields();
		  regchkout.PaymentAddressFieldsTabFocus();
		  regchkout.PayFNameEnterMax();
		  regchkout.PayLNameEnterMax();
		  regchkout.PayFNLNameNumValidation();
		  regchkout.ShipPageCreatePayDefaultCountry();
		  regchkout.ShipPageCreatePayStreetAddressFields();
		  regchkout.PayStAddFieldsLenVal();
		  regchkout.PayBillCityEnterable();
		 //regchkout.PayCityyValidation(); Rework once city alert is displayed
		  regchkout.PayBillingCountryOptions();
		  regchkout.PayBillingCADefaultStateSel();
		  regchkout.PayShippingCADefaultStateSel();
		  regchkout.PayBillingUKStateText();
		  regchkout.PayShipPageCreateChangeCountry();
		  regchkout.ShippingCAPOAlphaNumericValidation();
		  regchkout.PayBillingCAPOLength();
		  regchkout.BillingCAPOLengthValidationAlert();
		  regchkout.PayCAPHNumLenValidation("HBC-1551");
		  regchkout.PayCAPHNumLenValidation("HBC-1554");
		  regchkout.ShipPageCreatePayPHTextBoxes();
		  regchkout.PayCAPHAphaNumericValidationAlert();
		  regchkout.BillingCAExtNumLenValidation();
		  regchkout.ShipPageBillingValidEmail();
		 //regchkout.BillingCAEmailValidationAlert(); Rework once Email alert is displayed
		  regchkout.BillingTermandConditionChkBoxState();
		  
		  //*****Payment Sections****
		  regchkout.PaymentSectionsOptions();
		  regchkout.PaymentShippingTaxesOptions();
		  regchkout.PaymentHBCrewards();
		  regchkout.PaymentHBCrewardValidation();
		  regchkout.PaymentHBCrewardlenValidation();
		  regchkout.PaymentHBCrewardsApply();
		  regchkout.PaymentHBCrewardsEmptyApply();
		  regchkout.PaymentHBCrewardInvalidPromocode();
		  regchkout.PaymentHBCrewardLenPromocode();
		  regchkout.PaymentShippingandTaxes();
		  regchkout.PaymentOrderTotalHighlight();
		  regchkout.PaymentShippingMethodDropDown();
		  regchkout.PaymentShippingMethodDropDownDefault();
		  regchkout.PaymentShippingMethodDropDownOptions();
		  regchkout.PaymentShippingMethodDropDownOptionChange(); 
		  regchkout.PaymentSectionsPaybyCardPaybycreditcardSelected();
		  regchkout.PaymentSectionsCreditCardFieldDropDown();
		  regchkout.PaymentSectionsCreditCardTypeSymbol();
		  regchkout.PaymentSectionsCreditCardDropdownDefault();
		  regchkout.PaymentSectionsCreditCardDropdownSelectOptions();
		  regchkout.PaymentSectionsCreditCardImages();
		  regchkout.PaymentSectionsCreditCardAmountToPay();
		  regchkout.PaymentSectionsCreditCardNumberField();
		  regchkout.PaymentSectionsCreditCardNumberFieldMaximum();
		  regchkout.PaymentSectionsCreditCardNumberEmptyFieldAlert();
		  regchkout.PaymentSectionsCreditCardNumberInvalidFieldAlert();
		  regchkout.PaymentSectionsCreditCardFields();
		  regchkout.PaymentSectionsCreditCardSecurityCode(); 
		  regchkout.PaymentSectionsCreditCardSecurityCodeFieldMaximum();
		  regchkout.PaymentSectionsCreditCardSecurityEmptyFieldAlert();
		  regchkout.PaymentSectionsCreditCardSecurityInvalidFieldAlert();
		  regchkout.PaymentSectionsCreditCardExpirationMonthYear();
		  regchkout.PaymentSectionsCreditCardExpirationMonthYearFormat();
		  regchkout.PaymentSectionsCreditCardExpirationInvalidFieldAlert();
		  regchkout.PaymentSectionsCreditCardExpirationEmptyFieldAlert();
		  regchkout.PaymentSectionsCreditCardExpirationLessDigitsFieldAlert();
		  regchkout.PaymentSectionsCreditCardExpirationFieldMaximum();
		  regchkout.PaymentSectionsCreditCardExpiredYear();	
		  regchkout.PaymentSectionsPayByGiftCardUnselected();
		  regchkout.PaymentSectionsPaybygiftcard();		 
		  regchkout.PaymentSectionsApplyGiftPinFieldMaximum();
		  regchkout.PaymentSectionsAddAnotherGiftCard();
		  regchkout.PaymentSectionsAddAnotherGiftCardClick();
		  regchkout.PaymentSectionsPaybycreditcardDeselect();
		  regchkout.PaymentSectionPaypalSelectedCreditCardUnselected();
		  regchkout.PaymentSectionsWhatIsPayPal();
		  regchkout.PaymentSectionsWhatIsPayPalClick(); 
		  
		  regchkout.removeAddress("Spider");

		  		  
		  //*****Review and Submit tab*****	
		  
		  regchkout.signOut();
		  regchkout.signin2();
		  regchkout.ReviewAndSubmitCreative();
		  regchkout.ReviewAndSubmitToPreviousTabs();
		  regchkout.ReviewAndSubmitColumswithEdit();
		  regchkout.ReviewAndSubmitSavedDetails();		  
		  regchkout.ReviewAndSubmitSavedDetailsCompare();
		  regchkout.ReviewAndSubmitAfterModification();
		  regchkout.ReviewAndSubmitShippingEditToAddressSection();
		  regchkout.ReviewAndSubmitShippingMethodEditToShipPayment();
		  regchkout.ReviewAndSubmitPaymentMethodEditToShipPayment();
		  regchkout.ReviewAndSubmitPriceChangewhenQuantityUpdate();	
		  
		  regchkout.removeAddress("Super");

		  
		  /*****OrderConfirmation Page****Don't run in Production*/
		  
		  /*regchkout.ReviewAndSubmitPlaceOrderToOrderConfrim();
		  regchkout.OrderConfirmationCreative();
		  regchkout.OrderConfirmationOrderNumber();
		  regchkout.OrderConfirmationShippingAddress();
		  regchkout.OrderConfirmationPaymentdetails();*/
		  
	  }
	  
	  @AfterSuite
		public void closeReporter() 
		{
			extent.close();
		}
		
		@BeforeMethod
		public void beforeMethod(Method name)
		{
			parent = extent.startTest(name.getName().replace("_", " "));
		}
		
		@AfterMethod
		public void afterMethod(Method name)
		{
			extent.endTest(parent);
			extent.flush();
		}
		
		@AfterTest
		public void afterClass() 
		{
		  driver.close();
		}
		
		public void ChildCreation(String name)
		{
			child = extent.startTest(name);
			parent.appendChild(child);
		}
		
		public static void EndTest()
		{
			extent.endTest(child);
		}
		
		public void Createlog(String val)
		{
			child.log(LogStatus.INFO, val);
		}
		
		public void Pass(String val)
		{
			child.log(LogStatus.PASS, val);
			EndTest();
		}
		
		public void Skip(String val)
		{
			child.log(LogStatus.SKIP, val);
			EndTest();
		}
		
		public void Pass(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.PASS, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Fail(String val)
		{
			child.log(LogStatus.FAIL, val);
			EndTest();
		}
		
		public void Fail(String val,ArrayList<String> logval)
		{
			child.log(LogStatus.FAIL, val);
			for(String lval:logval)
			{
				Createlog(lval);
			}
			logval.removeAll(logval);
			EndTest();
		}
		
		public static void Exception(String val)
		{
			child.log(LogStatus.FATAL,val);
			EndTest();
		}

}


